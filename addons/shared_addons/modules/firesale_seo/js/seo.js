$(function() {

	if( $('#meta_description').size() > 0 ) {

		// Description
		var obj   = $('#meta_description');
		var count = 160;
		var rem   = ( count - obj.val().length );
		$('label[for=meta_description]').append('<br /><div class="count"><span>' + rem + '</span> de ' + count + ' caracteres restantes</div>');
		obj.bind('keyup keydown blur focus change update paste delete', function() {
			var rem   = ( count - obj.val().length );
			var color = ( rem <= 10 ? 'C40000' : ( rem <= 40 ? 'E8E800' : '66CC00' ) );
			$('.count span').css('color', '#' + color).text(rem);
		}).focus().blur();

		// Keywords
		$('#meta_keywords').tagsInput();

	}

});
<?php defined('BASEPATH') OR exit('No direct script access allowed');

	// General
    $lang['firesale:wish:title']               = 'Wishlists';

    // Tabs
    $lang['firesale:wish:tabs:general']        = 'General';
    $lang['firesale:wish:tabs:items']          = 'Items';

    // Lists
	$lang['firesale:wish:add']                 = 'Create New Wishlist';
	$lang['firesale:wish:add:success']         = 'Wishlist Created Successfully';
	$lang['firesale:wish:add:error']           = 'Error Creating Wishlist';
	$lang['firesale:wish:edit']                = 'Edit Wishlist';
	$lang['firesale:wish:edit:success']        = 'Wishlist Edited Successfully';
	$lang['firesale:wish:edit:error']          = 'Error Editing Wishlist';
	$lang['firesale:wish:delete']              = 'Delete Wishlist';
	$lang['firesale:wish:delete:success']      = 'Wishlist Deleted Successfully';
	$lang['firesale:wish:delete:error']        = 'Error Deleting Wishlist';
	$lang['firesale:wish:none']                = 'No Wishlists Found, Click <a href="%s">Here</a> To Create One';
	$lang['firesale:wish:back']                = 'Back to Wishlists';

	// Items
	$lang['firesale:wish:item:add']            = 'Add Item to %s';
	$lang['firesale:wish:item:add:short']      = 'Add Item';
	$lang['firesale:wish:item:add:success']    = 'Item Added Successfully';
	$lang['firesale:wish:item:add:error']      = 'Error Adding Item';
	$lang['firesale:wish:item:edit']           = 'Edit Item';
	$lang['firesale:wish:item:edit:short']     = 'Update Items';
	$lang['firesale:wish:item:edit:success']   = 'Item Edited Successfully';
	$lang['firesale:wish:item:edit:error']     = 'Error Editing Item';
	$lang['firesale:wish:item:delete']         = 'Remove from %s';
	$lang['firesale:wish:item:delete:short']   = 'Remove Items';
	$lang['firesale:wish:item:delete:success'] = 'Item Removed Successfully';
	$lang['firesale:wish:item:delete:error']   = 'Error Removing Item';
	$lang['firesale:wish:item:none']           = 'No Items Found, Click <a href="%s">Here</a> To Find Some';

	// Defaults
	$lang['firesale:wish:default:name']        = 'Default Wishlist';
	$lang['firesale:wish:default:desc']        = 'Your Default Wishlist';

    // Labels
	$lang['firesale:wish:label:title']         = 'Name';
	$lang['firesale:wish:label:user']          = 'User';
	$lang['firesale:wish:label:status']        = 'Status';
	$lang['firesale:wish:label:privacy']       = 'Privacy';
	$lang['firesale:wish:label:public']        = 'Public';
	$lang['firesale:wish:label:private']       = 'Private';
	$lang['firesale:wish:label:link']          = 'Viewable with Link';
	$lang['firesale:wish:label:desc']          = 'Description';
	$lang['firesale:wish:label:product']       = 'Product';
	$lang['firesale:wish:label:quantity']      = 'Quantity';
	$lang['firesale:wish:label:items']         = 'Items';
	$lang['firesale:wish:label:value']         = 'Value';

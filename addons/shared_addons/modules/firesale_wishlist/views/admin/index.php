
    <section class="title">
        <h4><?php echo lang('firesale:wish:title'); ?></h4>
    </section>

    <section class="item">
        <div class="content">
        <?php if ($total > 0 ): ?>

            <?php echo form_open($this->uri->uri_string(), 'id="wish" class="crud"'); ?>
				<div class="scroll-table">
                    <table id="wish" class="sortable">
                        <thead>
                            <tr>
                                <th style="width: 15px"><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
                                <th><?php echo lang('firesale:wish:label:title'); ?></th>
                                <th class="center"><?php echo lang('firesale:wish:label:user'); ?></th>
                                <th class="center"><?php echo lang('firesale:wish:label:items'); ?></th>
                                <th class="center"><?php echo lang('firesale:wish:label:value'); ?></th>
                                <th class="center"><?php echo lang('firesale:wish:label:status'); ?></th>
                                <th class="center"><?php echo lang('firesale:wish:label:privacy'); ?></th>
                                <th style="width: 125px">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ( $entries as $list ): ?>
                            <tr id="wish_<?php echo $list['id']; ?>">
                                <td><input type="checkbox" name="action_to[]" value="<?php echo $list['id']; ?>"  /></td>
                                <td><?php echo $list['title']; ?></td>
                                <td><?php echo $list['created_by']['display_name']; ?></td>
                                <td class="center"><?php echo $list['total']; ?></td>
                                <td class="center"><?php echo $list['price']; ?></td>
                                <td class="center"><?php echo $list['status']['val']; ?></td>
                                <td class="center"><?php echo $list['privacy']['val']; ?></td>
                                <td><center>
                                    <?php echo anchor('admin/firesale_wishlist/edit/'.$list['id'], lang('global:edit'), 'class="btn blue small"'); ?>
                                    <?php echo anchor('admin/firesale_wishlist/delete/'.$list['id'], lang('global:delete'), 'class="btn red small"'); ?>
                                </center></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
				</div>
                <div class="table_action_buttons">
                    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )); ?>
                    <?php echo $pagination; ?>
                </div>

            <?php echo form_close(); ?>
        <?php else: ?>
            <div class="no_data"><?php echo sprintf(lang('firesale:wish:none'), 'admin/firesale_wishlist/create'); ?></div>
        <?php endif;?>
        </div>
    </section>

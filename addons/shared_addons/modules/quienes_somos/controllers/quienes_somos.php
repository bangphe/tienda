<?php

class Quienes_somos extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $models = array(
            'quienes_somos_m'
        );
        $this->load->model($models);
    }

    function index() {

        /*
         * Modulo
         */
        $result = $this->quienes_somos_m->get_all();
        $post = array();

        if (count($result) > 0) {
            $post = $result[0];
        }

        $this->template
                ->title($this->module_details['name'])
                ->set_breadcrumb('Quienes Somos')
                ->set('datos', $post)
                ->build('quienes_somos_front');
    }


}

?>

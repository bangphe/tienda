<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Module_Reports extends Module
{

    public $version = '1.2';
	public $table_widget = 'rg_widgets';
	public $table_user = 'rg_widgets_usuarios';
	
    public function info() {
        return array(
            'name' => array(
                'es' => 'Reportes',
                'en' => 'Reports',
            ),
            'description' => array(
                'es' => 'Modulo de reportes. @Luis Fernando Salazar',
                'en' => 'Reports module @Luis Fernando Salazar',
            ),
            'frontend' => false,
            'backend' => true,
            'menu' => 'content',
            'sections' => array(
                'mywidgets' => array(
                    'name' => 'Mis Widgets',
                    'uri' => 'admin/reports',
                ),
                'widgets' => array(
                    'name' => 'Widgets',
                    'uri' => 'admin/reports/modulosDeWidgets/1',
                ),
            )
        );
    }

    public function install()
    {
        // Widgets
		$this->dbforge->drop_table($this->table_widget);

        $data = array(
            'idwidge' => array(
                'type' => 'SMALLINT',
                'constraint' => '4',
                'auto_increment' => true,
				'unsigned' => TRUE,
            ),
            'codwidge' => array(
                'type' => 'VARCHAR',
                'constraint' => '10',
                'null' => true
            ),
            'nomwidge' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => true
            ),
            'nummodulo' => array(
                'type' => 'TINYINT',
                'constraint' => '2',
				'unsigned' => TRUE,
				'default' => '0',
            ),
			'undsancho' => array(
                'type' => 'TINYINT',
                'constraint' => '2',
                'default' => '0',
				'unsigned' => TRUE,
            ),
			'tipografico' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'default' => '0',
				'unsigned' => TRUE,
            ),
			'tipotabla' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'default' => '0',
				'unsigned' => TRUE,
            ),
			'minrefresco' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'default' => '0',
				'unsigned' => TRUE,
            ),
			'consultasql' => array(
                'type' => 'TEXT',
                'constraint' => '',
				'null' => true,
            ),
			'tituloejex' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => true
            ),
			'tituloejey' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => true
            ),
			'descripcion' => array(
                'type' => 'MEDIUMTEXT',
                'constraint' => '',
                'null' => true
            ),
        );

        $this->dbforge->add_field($data);
        $this->dbforge->add_key('idwidge', true);

        if (!$this->dbforge->create_table($this->table_widget)) {
            return false;
        }
		
		// Relación widget usuario
		$this->dbforge->drop_table($this->table_user);

        $data = array(
            'idwiusu' => array(
                'type' => 'INT',
                'constraint' => '10',
                'auto_increment' => true,
				'unsigned' => TRUE,
            ),
            'idusuar' => array(
                'type' => 'INT',
                'constraint' => '10',
				'unsigned' => TRUE,
                'default' => '0',
            ),
            'idwidge' => array(
                'type' => 'SMALLINT',
                'constraint' => '4',
                'unsigned' => TRUE,
                'default' => '0',
            ),
            'posicion' => array(
                'type' => 'SMALLINT',
                'constraint' => '4',
				'unsigned' => TRUE,
				'default' => '0',
            ),
			'undsancho' => array(
                'type' => 'TINYINT',
                'constraint' => '2',
                'default' => '0',
				'unsigned' => TRUE,
            ),
			'tipografico' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'default' => '0',
				'unsigned' => TRUE,
            ),
			'var1' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'default' => '0',
				'unsigned' => TRUE,
            ),
			'var2' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'default' => '0',
				'unsigned' => TRUE,
            ),
            'divisor' => array(
                'type' => 'INT',
                'constraint' => '20',
                'default' => '0',
                'unsigned' => TRUE,
            ),
        );

        $this->dbforge->add_field($data);
        $this->dbforge->add_key('idwiusu', true);

        if (!$this->dbforge->create_table($this->table_user)) {
            return false;
        }
		
        return TRUE;
    }

    public function uninstall() {
        $this->dbforge->drop_table($this->table_widget);
		$this->dbforge->drop_table($this->table_user);
        return true;
    }

    public function upgrade($old_version) {
        return true;
    }

    public function help() {
        return "Pagina inicial del sitio desarrollada a la medida.";
    }

}

<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
* This file is part of FireSale, a PHP based eCommerce system built for
* PyroCMS.
*
* Copyright (c) 2013 Moltin Ltd.
* http://github.com/firesale/firesale
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @package firesale/search
* @author FireSale <support@getfiresale.org>
* @copyright 2013 Moltin Ltd.
* @version master
* @link http://github.com/firesale/firesale
*
*/

    $lang['firesale:sections:search'] 		  = 'Buscar';
    $lang['firesale:sections:search_results'] = 'Resultado de la busqueda con "%s"';
    $lang['firesale:search:products_match']	  = 'Productos encontrados con el criterio de busqueda';
    $lang['firesale:search:products_none']	  = 'No productos encontrados con el criterio de busqueda';

    // Dashboard
    $lang['firesale:elements:search_terms'] = 'Términos de búsqueda populares';
    $lang['firesale:search:no_terms']		= 'No hay términos de su búsqueda';

    // Search Frontend
    $lang['firesale:search:label_search'] 			= 'Buscar';
    $lang['firesale:search:label_nothing_found'] 	= 'No se encontro nada!';

    // Routes
    $lang['firesale:routes:search'] = 'Buscar';

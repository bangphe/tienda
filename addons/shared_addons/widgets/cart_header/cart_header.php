<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Widget_Cart_Header extends Widgets
{
    // The widget title,  this is displayed in the admin interface
    public $title = array(
        'en' => 'Cart Header (Widget)',
        'es' => 'Cart header (Widget)'
    );
    public $description = array(
        'en' => 'Cart Header (Widget)',
        'es' => 'Cart Header (Widget)'
    );
 
    // The author's name
    public $author = 'Luis Fernando Salazar Buitrago';
 
    // The authors website for the widget
    public $website = 'www.imaginamos.com';
 
    //current version of your widget
    public $version = '1.0';
	
    public function run()
    {
    	$this->load->library('fs_cart');
        $this->load->model('products_m');
        $this->load->model('currency_m');
        $this->load->helper('general');

         $currency = ( $this->session->userdata('currency') ? $this->session->userdata('currency') : NULL );
        $currency = $this->pyrocache->model('currency_m', 'get', array($currency), $this->firesale->cache_time);

        // Variables
        $data           = new stdClass;
        $data->products = array();

        // Loop products in cart
        foreach ( $this->fs_cart->contents() as $id => $item ) {

            $product = $this->pyrocache->model('products_m', 'get_product', array($item['id']), $this->firesale->cache_time);

            if ($product !== FALSE) {

                $product['quantity'] = $item['qty'];
                $product['name']     = $item['name'];
                $product['price']    = $item['price'];
                $product['ship']     = $item['ship'];
                $product['subtotal'] = $this->currency_m->format_string($item['subtotal'], $currency, false);
                $product['rowid']    = $item['rowid'];

                $data->products[] = $product;

            }

        }

        // Calculate prices
        $data->tax   = $this->currency_m->format_string($this->fs_cart->tax(), $currency, false);
        $data->sub   = $this->currency_m->format_string($this->fs_cart->subtotal(), $currency, false);
        $data->total = $this->currency_m->format_string($this->fs_cart->total(), $currency, false);
        $data->count = $this->fs_cart->total_items() ? $this->fs_cart->total_items() : '&#48;';

        // Fix helper variables
        $data->products = reassign_helper_vars($data->products);
        
        /*echo "<pre> ";
        print_r($data->products);
        echo "</pre> ";
        exit();*/


        
        // Retrun data
        return array($data);

       /* $data = $this->db->get($this->db->select('phone')->dbprefix.'contact_us')->row();
    	return array('data' => $data->phone);*/
    }
}
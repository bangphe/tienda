<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Plugin_Firesale_discount_codes extends Plugin
{
    public function code_applied()
    {
        if ($code = $this->session->userdata('discount_code'))
            return $code;

        return FALSE;
    }
}

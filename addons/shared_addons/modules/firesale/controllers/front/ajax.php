<?php defined('BASEPATH') or exit('No direct script access allowed');

class ajax extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load required items
        $this->lang->load('firesale');
        $this->load->helper('general');
        $this->load->model('imaginamos_m');

        // Add initial items
        $this->data = new stdClass();

        // Ensure request was made
        if ( ! $this->input->is_ajax_request() ) { show_404(); }
    }

    // check the variation exists based on options
    public function variation_check()
    {
        if ($this->input->post()) {
            $input   = $this->input->post();
            $options = $input['options'][0];
            sort($options);

            $stream = $this->streams->streams->get_stream('firesale_product_variations', 'firesale_product_variations');
            $product = $this->modifier_m->variation_exists($options, $stream->id);

            if ($product !== false) {
                $product = cache('products_m/get_product', $product, null, 1);

                $data = array(
                    'code'            => $product['code'],
                    'stock'           => $product['stock'],
                    'stock_status'    => $product['stock_status'],
                    'rrp_rounded'     => $product['rrp_rounded'],
                    'rrp_formatted'   => $product['rrp_formatted'],
                    'price_rounded'   => $product['price_rounded'],
                    'price_formatted' => $product['price_formatted'],
                    'diff_rounded'    => $product['diff_rounded'],
                    'diff_formatted'  => $product['diff_formatted']
                );

                echo json_encode($data);
                exit;

            }
        }
    }

	public function change_modifier()
	{
		$this->load->model('firesale/products_m');
		
		$data['product'] = array();
		
		$first = false;
		// quitamos el prefijo de la base de datos por que esta generando error
		$this->db->set_dbprefix(null);
		foreach($_POST['modifiers_id'] AS $item => $value)
		{
			if(is_numeric($value))
			{
				if(!$first)
				{
					$first = true;
					$this->db->where('a.row_id', $value);
				}
				else
				{
					$this->db->join('default_firesale_product_variations_firesale_products AS a'.$item, 'a.firesale_products_id = a'.$item.'.firesale_products_id', 'left')
					->where('a'.$item.'.row_id', $value);
				}
			}
		}
		
		$prod_mod = $this->db
	        ->get('default_firesale_product_variations_firesale_products AS a')->result();
		// volvemos a colocar el prefijo de la base de datos
		$this->db->set_dbprefix(SITE_REF.'_');
		if(!empty($prod_mod))
		{
			$prod_mod = $prod_mod[0];
			$data['product'] = cache('products_m/get_product', $prod_mod->firesale_products_id);
		}
		
		$this->template->set_layout(FALSE);
		$this->template->build('change_modifier', $data);
	}

	public function change_deparment()
	{
		$id_deparment = $_POST['id_select'];
		$name_select = $_POST['name_select'];
		if(!empty($id_deparment))
		{
			/*$this->db->select('code, name, deparment')
				->from('firesale_city')
				->where('deparment', $id_deparment);

			// traemos los datos
			$citys = $this->db->get()->result();
			*/
			$citys = $this->imaginamos_m->sqlFormSelect('firesale_city', 'id', 'name', FALSE, 'deparment', $id_deparment);
			echo form_dropdown($name_select, $citys,  set_value($name_select), '');
		}
	}

	
}

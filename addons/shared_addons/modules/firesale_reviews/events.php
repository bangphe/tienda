<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Events_Firesale_reviews
{

    protected $ci;

    public function __construct()
    {

        $this->ci =& get_instance();

        // register the events
        Events::register('product_delete',     array($this, 'product_deleted'));
        Events::register('clear_cache',     array($this, 'clear_cache'));
        Events::register('firesale_dashboard', array($this, 'firesale_dashboard_reviews'));

    }

    public function product_deleted($data)
    {

        /* Coming soon, possibly
        echo '<pre>';
            print_r($data);
        echo '</pre>';
        exit();*/

    }

    public function clear_cache()
    {
        $this->ci->pyrocache->delete_all('reviews_m');
    }

    public function firesale_dashboard_reviews()
    {
        $this->ci->load->model('firesale_reviews/reviews_m');

        // Set query paramaters
        $params = array(
            'stream'    => 'firesale_reviews',
            'namespace' => 'firesale_reviews',
            'limit'     => 6,
            'order_by'  => 'id',
            'sort'      => 'asc'
        );
        
        // Get entries      
        $reviews = $this->ci->streams->entries->get_entries($params);
        $fields  = $this->ci->pyrocache->model('reviews_m', 'max_values', array(), $this->ci->firesale->cache_time);

        // Add rating values
        $this->ci->reviews_m->assign_values($reviews['entries'], $fields);

        // Build and return data
        return array(
            'id'      => 'reviews',
            'title'   => lang('firesale:reviews:latest_title'),
            'content' => $this->ci->parser->parse('firesale_reviews/admin/dashboard_latest', $reviews, true),
            'assets'  => array(
                array('type' => 'css', 'file' => 'firesale_reviews::dashboard_latest.css')
            )
        );
    }

}

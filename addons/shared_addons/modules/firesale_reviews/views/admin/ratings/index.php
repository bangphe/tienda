
    <section class="title">
        <h4><?php echo lang('firesale:reviews:ratings'); ?></h4>
    </section>

    <section class="item">
    	<div class="content">
        	<div class="scroll-table">
                <table>
                    <thead>
                        <tr>
                            <th style="width: 15px"><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
                            <th>Title</th>
                            <th>Max Rating</th>
                            <th>Default Rating</th>
                            <th>Required</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach( $fields as $key => $field ): ?>
                        <tr>
                            <td><input type="checkbox" name="action_to[]" value="<?php echo $field->field_id; ?>"  /></td>
                            <td><?php echo $field->field_name; ?></td>
                            <td><?php echo $field->field_max; ?></td>
                            <td><?php echo $field->field_data['default_value']; ?></td>
                            <td><?php echo ucwords($field->is_required); ?></td>
                            <td style="width: 120px">
                                <a href="<?php echo site_url(); ?>admin/firesale_reviews/ratings/edit/<?php echo $field->field_id; ?>" class="btn blue small">Edit</a>
                            <?php if( $key != 0 ): ?>
                                <a href="<?php echo site_url(); ?>admin/firesale_reviews/ratings/delete/<?php echo $field->field_id; ?>" class="btn red small">Delete</a>
                            <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
    		</div>
            <br />
            <div class="table_action_buttons">
                <a href="<?php echo site_url(); ?>admin/firesale_reviews/ratings/create" class="btn blue">Create New</a>
                <?php $this->load->view('admin/partials/buttons', array('buttons' => array('btn red') )); ?>
            </div>
		</div>
    </section>

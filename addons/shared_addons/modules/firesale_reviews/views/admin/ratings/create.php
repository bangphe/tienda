
    <?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>

    <?php if( isset($id) && $type == 'edit' ): ?>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <?php endif; ?>

        <section class="title">
            <h4><?php echo lang('firesale:reviews:ratings:'.$type); ?></h4>
        </section>

        <section class="item form_inputs">
        	<div class="content">
            	<div class="inline-form">
                    <fieldset>
                        <ul>
                            <li class="<?php echo alternator('even', ''); ?>">
                                <label for="title">Title <span>*</span></label>
                                <div class="input">
                                    <input type="text" value="<?php echo ( isset($input['title']) ? $input['title'] : '' ); ?>" id="title" name="title" />
                                </div>
                                <br class="clear">
                            </li>
                            <li class="<?php echo alternator('even', ''); ?>">
                                <label for="is_required">Is required?</label>
                                <div class="input">
                                	<label class="radio">
                                    	<input type="checkbox" <?php echo ( isset($input['is_required']) && $input['is_required'] == 'yes' ? 'checked="checked" ' : '' ); ?>id="is_required" name="is_required" value="yes" style="min-width: 20px !important; width: 20px" />
                                    </label>
                                </div>
                                <br class="clear">
                            </li>
                            <li class="<?php echo alternator('even', ''); ?>">
                                <label for="maximum">Maximum Rating <span>*</span></label>
                                <div class="input">
                                    <input type="text" value="<?php echo ( isset($input['maximum']) ? $input['maximum'] : '' ); ?>" id="maximum" name="maximum" style="min-width: 50px !important; width: 50px" />
                                </div>
                                <br class="clear">
                            </li>
                            <li class="<?php echo alternator('even', ''); ?>">
                                <label for="default">Default Rating <span>*</span></label>
                                <div class="input">
                                    <input type="text" value="<?php echo ( isset($input['default']) ? $input['default'] : '' ); ?>" id="default" name="default" style="min-width: 50px !important; width: 50px" />
                                </div>
                                <br class="clear">
                            </li>
                        </ul>
                    </fieldset>
                </div>
                <div class="buttons">
                    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
                </div>
            </div>
        </section>

    <?php echo form_close(); ?>

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Module_Quienes_Somos extends Module {

    public $version = '1.1';

    public function info() {
        return array(
            'name' => array(
                'es' => 'Quienes Somos',
                'en' => ''
            ),
            'description' => array(
                'es' => 'Página de contenido; texto, imagen y video',
                'en' => ''
            ),
            'frontend' => true,
            'backend' => true,
            'menu' => 'content',
        );
    }

    public function install() {

        $this->dbforge->drop_table('quienes_somos');

        $field = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true
            ),
            'titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
            'imagen' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
            'video' => array(
                'type' => 'TEXT',
                'null' => true
            ),
            'texto' => array(
                'type' => 'LONGTEXT',
                'null' => true
            )
        );

        $this->dbforge->add_field($field);
        $this->dbforge->add_key('id', true);

        if (!$this->dbforge->create_table('quienes_somos')) {
            return false;
        }

        $data = array(
            'titulo' => '',
            'imagen' => '',
            'video' => '',
            'texto' => ''
        );

        $this->db->insert('quienes_somos', $data);

        $dir = UPLOADSFOLDER . 'quienes_somos';

        if (!is_dir($dir)) {
            @mkdir($dir, '0777');
            chmod($dir, '0777');
        }

        return true;
    }

    public function uninstall() {
        $this->dbforge->drop_table('quienes_somos');
        @rmdir(UPLOADSFOLDER . 'quienes_somos');
        return true;
    }

    public function upgrade($old_version) {
        return true;
    }

    public function help() {
        return "Página de contenido; texto, imagen y video";
    }

}
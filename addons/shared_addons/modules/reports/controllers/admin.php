<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author 	Luis Fernando Salazar
 * @package 	PyroCMS
 * @subpackage 	Modulo de reportes
 * @category 	Modulos
 * @license 	Apache License v2.0
 */

class Admin extends Admin_Controller {

    var $modulosWidgets = array(1 => 'Compras', 2 => 'Ctas. Por Pagar', 3=> 'Inv. Mat. Prima', 4 => 'Producción', 5 => 'Inv. Prod. Term.', 6 => 'Aseg. Calidad', 7 => 'Gestión Comercial', 8 => 'Ctas. por Cobrar', 9 => 'Contabilidad', 10 => 'Gestión Humana', 11 => 'Mantenimiento');

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('reportesmodel'));
        $this->load->library(array('sinergia'));
    }

    public function index()
    {
		$consultaSql = "SELECT us.idwidge, us.idwiusu, us.posicion, us.undsancho, us.tipografico, wi.tipotabla, wi.nomwidge, wi.minrefresco, wi.tituloejex, wi.tituloejey, IF(consultasql LIKE '%0 AS ordenserie%', 0, 1) AS multiserie, wi.descripcion, us.divisor FROM default_rg_widgets_usuarios us LEFT JOIN default_rg_widgets wi USING(idwidge) WHERE idusuar = '".$this->current_user->id."' ORDER BY posicion";
        $datosVista['widgetsUsuario'] = $this->db->query($consultaSql);
        $datosVista['url_buscar'] = 'index.php?reportesgerenciales/cambiarFechaSession';
        
        $fechaactual = $this->session->userdata('fechaactual');
        list($datosVista['anoactual'], $datosVista['mesactual'], $datosVista['diaactual']) = explode('-', $fechaactual);
        
        $datosVista['buscarPor1'] = array(2015 => '2015', 2014 => '2014' , 2013 => '2013', 2012 => '2012', 2011 => '2011');
        $datosVista['buscarPor2'] = array(1 => 'Enero' , 2 =>'Febrero', 3=> 'Marzo', 4 => 'Abril', 5=> 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre');
        $datosVista['buscarPor3'] = array(1 => '1' , 2 => '2', 3 => '3', 4 => '4', 5 => '5', 6 => '6', 7 => '7', 8 => '8', 9 => '9', 10 => '10', 11 => '11', 12 => '12', 13 => '13', 14 => '14');

        $datosVista['modulosMenu'] = $this->modulosWidgets;

        $this->template
            ->append_css('module::bootstrap.min.css')
            ->append_css('module::bootstrap-responsive.min.css')
            ->append_css('module::datepicker.css')
            ->append_css('module::estilos.css')
            ->append_css('module::nvd3/src/nv.d3.css')
            ->append_css('module::jquery.multiselect.css')
            ->append_css('module::jquery.dataTables.css')
            ->append_css('module::jquery.contextMenu.css')
            ->append_css('module::jquery.pnotify.default.css')
            ->append_js('module::jquery.contextMenu.js')
            ->append_js('module::bootstrap.min.js')
            ->append_js('module::bootstrap-datepicker.js')
            ->append_js('module::reportes_gerenciales.js')
            ->append_js('module::bootbox.js')
            ->append_js('module::jquery.ui.touch-punch.min.js')
            ->append_js('module::jquery.pnotify.min.js')
            ->append_js('module::nvd3/lib/d3.v2.js')
            ->append_js('module::nvd3/nv.d3.min.js')
            ->append_js('module::nvd3/src/tooltip.js')
            ->append_js('module::nvd3/src/utils.js')
            ->append_js('module::nvd3/src/models/legend.js')
            ->append_js('module::nvd3/src/models/axis.js')
            ->append_js('module::nvd3/src/models/scatter.js')
            ->append_js('module::nvd3/src/models/line.js')
            ->append_js('module::nvd3/src/models/lineChart.js')
            ->append_js('module::nvd3/src/models/pie.js')
            ->append_js('module::nvd3/src/models/pieChart.js')
            ->append_js('module::nvd3/src/models/multiBar.js')
            ->append_js('module::nvd3/src/models/multiBarChart.js')
            ->append_js('module::jquery.multiselect.min.js')
            ->append_js('module::jquery.dataTables.min.js')
            ->append_js('module::jquery.numberformattable.js')
            ->append_js('module::rgbcolor.js')
            ->append_js('module::StackBlur.js')
            ->append_js('module::canvg.js')
            ->append_js('module::reportes_gerenciales_ajax.js')
            ->append_js('module::masonry.min.js')
            ->build('admin/vistaUsuarioWidgets', $datosVista);
    }

	public function cambiarFechaSession()
    {
        $this->session->set_userdata(array('fechaactual' => $_POST['fechaGeneral']));
        $statusJson = "";
        $msgJson = "";
        echo json_encode(array('status' => $statusJson, 'msg' => $msgJson)); 
    }
    
    public function modulosDeWidgets($numModulo)
    {
        $consultaSql = "SELECT idwidge, 0 AS idwiusu, 0 AS posicion, undsancho, tipografico, tipotabla, nomwidge, minrefresco, tituloejex, tituloejey, IF(consultasql LIKE '%0 AS ordenserie%', 0, 1) AS multiserie, wi.descripcion, '1' AS divisor FROM default_rg_widgets wi WHERE nummodulo = '".$numModulo."'";
        $datosVista['widgetsUsuario'] = $this->db->query($consultaSql);
        $datosVista['modulosWidgets'] = TRUE;
        
        $datosVista['modulosWidgets'] = TRUE;
        $datosVista['tituloModulo'] = $this->modulosWidgets[$numModulo];
        $datosVista['modulosMenu'] = $this->modulosWidgets;
        $datosVista['numModulo'] = $numModulo;
        
        // configuramos titulos botones etc de la vista inicio.php y fin.php
        $datosVista['Titulo'] = 'Tablero de Comando';

        $this->template
            ->append_css('module::bootstrap.min.css')
            ->append_css('module::bootstrap-responsive.min.css')
            ->append_css('module::datepicker.css')
            ->append_css('module::estilos.css')
            ->append_css('module::nvd3/src/nv.d3.css')
            ->append_css('module::jquery.multiselect.css')
            ->append_css('module::jquery.dataTables.css')
            ->append_css('module::jquery.contextMenu.css')
            ->append_css('module::jquery.pnotify.default.css')
            ->append_js('module::jquery.contextMenu.js')
            ->append_js('module::bootstrap.min.js')
            ->append_js('module::bootstrap-datepicker.js')
            ->append_js('module::reportes_gerenciales.js')
            ->append_js('module::bootbox.js')
            ->append_js('module::jquery.ui.touch-punch.min.js')
            ->append_js('module::jquery.pnotify.min.js')
            ->append_js('module::nvd3/lib/d3.v2.js')
            ->append_js('module::nvd3/nv.d3.min.js')
            ->append_js('module::nvd3/src/tooltip.js')
            ->append_js('module::nvd3/src/utils.js')
            ->append_js('module::nvd3/src/models/legend.js')
            ->append_js('module::nvd3/src/models/axis.js')
            ->append_js('module::nvd3/src/models/scatter.js')
            ->append_js('module::nvd3/src/models/line.js')
            ->append_js('module::nvd3/src/models/lineChart.js')
            ->append_js('module::nvd3/src/models/pie.js')
            ->append_js('module::nvd3/src/models/pieChart.js')
            ->append_js('module::nvd3/src/models/multiBar.js')
            ->append_js('module::nvd3/src/models/multiBarChart.js')
            ->append_js('module::jquery.multiselect.min.js')
            ->append_js('module::jquery.dataTables.min.js')
            ->append_js('module::jquery.numberformattable.js')
            ->append_js('module::rgbcolor.js')
            ->append_js('module::StackBlur.js')
            ->append_js('module::canvg.js')
            ->append_js('module::reportes_gerenciales_ajax.js')
            ->append_js('module::masonry.min.js')
            ->build('admin/vistaUsuarioWidgets', $datosVista);
    }
    
    public function creacionDeWidgets($fechaWidget = null)
    {       
        $consultaSql = 'SELECT consultasql, tituloejex, tituloejey, divisor FROM default_rg_widgets wi LEFT JOIN default_rg_widgets_usuarios USING(idwidge) WHERE idwidge = "'.$_POST['idWidge'].'"';
        $datosSql = $this->db->query($consultaSql);
        if(!empty($datosSql))
        {
            foreach ($datosSql->result() AS $datosArray)
            {
                $consultaSql = $datosArray->consultasql;
                $divisor = $datosArray->divisor;
            }
        }
        $datosArray = $this->reportesmodel->reemplazarAnoString($consultaSql, $fechaWidget);
        $consultaSql = $datosArray['string'];
        $datosGrafica = $this->sinergia->sqlQuery($consultaSql);
        
        echo '<div class="datosGrafico">';
        switch ($_POST['tipoGrafico'])
        {
            case '0':
                // mostramos los datos de la grafica cuando es tabla para poder cambiar de tabla a otro grafico
                if ($_POST['multiSerie'] == 1)
                {
                    echo json_encode($this->reportesmodel->generarArrayMultiBarChar($datosGrafica, $divisor));
                }
                else
                {
                    echo json_encode($this->reportesmodel->generarArrayBarrasSimple($datosGrafica, $divisor));
                }
                break;
            case '1':
            case '5':
                echo json_encode($this->reportesmodel->generarArrayBarrasSimple($datosGrafica, $divisor));
                break;
            case '2':
            case '3':
            case '4':
                echo json_encode($this->reportesmodel->generarArrayMultiBarChar($datosGrafica, $divisor));
                break;
        }
        echo '</div>';
        
        echo '<div class="datosTabla">';
        if ($_POST['tipoTabla'] > 0)
        {
            echo json_encode($this->reportesmodel->tablaGraficoArray($datosGrafica, $_POST['multiSerie'], $divisor));
        }
        echo '</div>';
        
        // determinamos si mostramos la fecha dinamica
        echo '<div class="mostrarFechaDinamica">';
        echo ($datosArray['fechaDinam'] == true ? '1' : '0');
        echo '</div>';
        
        // colocamos el divisor por el cual estan creados los datos del grafico
        echo '<div class="divisorWidget">';
        echo ($divisor > 0 ? $divisor : 1);
        echo '</div>';
    }

    public function sortableWidget()
    {
        $statusJson = '';
        $msgJson = '';
        $widgetsArrayViejo = $_POST['widgetsArrayViejo'];
        $widgetsArrayNuevo = $_POST['widgetsArrayNuevo'];
        
        $widgetsArrayViejo = array_unique($widgetsArrayViejo);  // quitamos los repetidos
        $widgetsArrayViejo = array_values($widgetsArrayViejo);  // ordenamos el array de 0 a n
        $widgetsArrayNuevo = array_unique($widgetsArrayNuevo);  // quitamos los repetidos
        $widgetsArrayNuevo = array_values($widgetsArrayNuevo);  // ordenamos el array de 0 a n
        
        //var_dump($widgetsArrayViejo);
        //var_dump($widgetsArrayNuevo);
        
        $consultaSql = 'SELECT idwiusu FROM default_rg_widgets_usuarios WHERE idusuar = "'.$this->current_user->id.'" ORDER BY posicion';
        $datosSql = $this->db->query($consultaSql);
        $hayCambios = FALSE;
        if(!empty($datosSql))
        {
            if($datosSql->num_rows() == count($widgetsArrayViejo))
            {
                $i = 0;
                foreach($datosSql->result() AS $datosArray)
                {
                    //echo $datosArray->idwiusu ."!=". $widgetsArrayViejo[$i].'<br/>';
                    if($datosArray->idwiusu != $widgetsArrayViejo[$i])
                    {
                        $hayCambios = TRUE;
                        break;
                    }
                    $i++;
                }
            }
            else
            {
                $hayCambios = TRUE;
            }
            if ($hayCambios)
            {
                $statusJson = "error";
                $msgJson = "El reordenamiento no fue posible los widgets tienen cambios sin actualizar. Presione Aceptar para actualizar los datos.";
                echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
                return;
            }
            
            // colocamos todos los lotes en 0 antes de reordenarlos en la máquina
            //$consultaSql = "UPDATE default_rg_widgets_usuarios SET posicion='0' WHERE idusuar='".$this->current_user->id."'";
            //$this->db->query($consultaSql);
            
            // ponemos el orden de los lotes en la máquina
            $i = 1;
            foreach($widgetsArrayNuevo as $fila => $idWiusu)
            {
                $consultaSql = "UPDATE default_rg_widgets_usuarios SET posicion='".$i."' WHERE idwiusu='".$idWiusu."'";
                $this->db->query($consultaSql);
                $statusJson = "success";
                $msgJson = "El campo se ha cambiado con éxito.";
                $i++;
            }
            if($statusJson == '' || $msgJson == '')
            {
                $statusJson = "error";
                $msgJson = "Ocurrió un error. Por favor inténtelo nuevamente.";
            }
            echo json_encode(array('status' => $statusJson, 'msg' => $msgJson)); 
        }
    }
    
    public function cambioTipoGraficoWidget()
    {
        $statusJson = "";
        $msgJson = "Grafico editado con exito.";
        $consultaSql = "UPDATE default_rg_widgets_usuarios SET tipografico='".$_POST['tipoGraficoNuevo']."' WHERE idwiusu='".$_POST['idWiusu']."'";
        
        if(!$this->db->query($consultaSql))
        {
            $statusJson = "error";
            $msgJson = "Ocurrió un error. Por favor inténtelo nuevamente.";
        }
        echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
    }
    
    public function cambioTamanoWidget()
    {
        $statusJson = "";
        $msgJson = "Tamaño cambiado con exito.";
        $consultaSql = "UPDATE default_rg_widgets_usuarios SET undsancho='".$_POST['undsAnchoNuevo']."' WHERE idwiusu='".$_POST['idWiusu']."'";
        
        if(!$this->db->query($consultaSql))
        {
            $statusJson = "error";
            $msgJson = "Ocurrió un error. Por favor inténtelo nuevamente.";
        }
        echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
    }
    
    public function cambioDivisorWidget()
    {
        $statusJson = "";
        $msgJson = "Divisor cambiado con exito.";
        $consultaSql = "UPDATE default_rg_widgets_usuarios SET divisor='".$_POST['divisorWidget']."' WHERE idwiusu='".$_POST['idWiusu']."'";
        
        if(!$this->db->query($consultaSql))
        {
            $statusJson = "error";
            $msgJson = "Ocurrió un error. Por favor inténtelo nuevamente.";
        }
        echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
    }
    
    public function eliminarWidget()
    {
        $statusJson = "";
        $msgJson = "Widget eliminado con exito.";
        // borramos el widget de la base de datos del usuario
        if(!$this->db->query('DELETE FROM default_rg_widgets_usuarios WHERE idwiusu="'.$_POST['idWiusu'].'"'))
        {
            $statusJson = "error";
            $msgJson = "Ocurrió un error. Por favor inténtelo nuevamente.";
        }
        echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
    }
    
    public function agregarWidget()
    {
        $statusJson = "";
        $msgJson = "Widget agregado a la vista con exito.";
        
        $consultaSql = "SELECT COUNT(idwiusu) AS numreg FROM default_rg_widgets_usuarios WHERE idwidge ='".$_POST['idWidge']."'";
        $BD_numreg = $this->db->query($consultaSql)->row()->numreg;
        if($BD_numreg > 0)
        {
            $statusJson = "error";
            $msgJson = "El widget se encuentra actualmente en su vista";
            echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
            return;
        }
        
        $consultaSql = "SELECT IFNULL(MAX(posicion) + 1, 0) AS numreg FROM default_rg_widgets_usuarios WHERE idusuar='".$this->current_user->id."'";
        $posicionNueva = $this->db->query($consultaSql)->row()->numreg;
        $query = 'INSERT INTO default_rg_widgets_usuarios (idusuar, idwidge, posicion, undsancho, tipografico, var1, var2) SELECT '.$this->current_user->id.' AS idusuar, idwidge, "'.$posicionNueva.'" AS posicion, "'.$_POST['undsAncho'].'" AS undsancho, "'.$_POST['tipoGrafico'].'" AS tipografico, 0 AS var1, 0 AS var2 FROM default_rg_widgets WHERE idwidge="'.$_POST['idWidge'].'"';
        if(!$this->db->query($query))
        {
            $statusJson = "error";
            $msgJson = "Ocurrió un error. Por favor inténtelo nuevamente.";
        }
        echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
    }
    
    public function graficoAImg()
    {
        //var_dump($_POST);
        $datosImagen =  $_POST['datosImagenGrafico'];
        $datosImagen = str_replace("[removed]", "data:image/jpeg;base64,", $datosImagen);
        //var_dump($_POST);
        //echo '<img src="'.$datosImagen.'"/>';
        
        header("Content-type: image/jpeg");
        header('Content-Disposition: attachment; filename="grafico.jpeg"');
        //nombre_imagen.gif es el nombre de la imagen tras la descarga 
        readfile($datosImagen); // se debe activar en el servidor
        //echo '<img src="'.$datosImagen.'"/>'; 
    }
    public function exportarExcel($idWidget, $datosTabla, $multiSerie)
    {
        $consultaSql = "SELECT wi.consultasql AS consulta, wi.tituloejex, wi.tituloejey, wu.tipografico, wi.tipotabla FROM default_rg_widgets wi LEFT JOIN default_rg_widgets_usuarios wu USING(idwidge) WHERE wu.idusuar=".$this->current_user->id." AND wu.idwidge=".$idWidget;
        $resultSql = $this->db->query($consultaSql);
        $consultaSql = $resultSql->row()->consulta;
        $tituloEjeX = $resultSql->row()->tituloejex;
        $tituloEjeY = $resultSql->row()->tituloejey;
        $tipoGrafico = $resultSql->row()->tipografico;
        $tipoTabla = $resultSql->row()->tipotabla;
        //$resultSql = $this->db->query($consultaSql);
        
        $datosGrafica = $this->sinergia->sqlQuery($consultaSql);
        $datosTabla = $this->reportesmodel->tablaGraficoArray($datosGrafica, $multiSerie);
        
        // sacamos los datos de la grafica
        switch ($tipoGrafico)
        {
            case '0':
                // mostramos los datos de la grafica cuando es tabla para poder cambiar de tabla a otro grafico
                if ($multiSerie == 1)
                {
                    $datosGrafica = $this->reportesmodel->generarArrayMultiBarChar($datosGrafica);
                }
                else
                {
                    $datosGrafica = $this->reportesmodel->generarArrayBarrasSimple($datosGrafica);
                }
                break;
            case '1':
            case '5':
                $datosGrafica = $this->reportesmodel->generarArrayBarrasSimple($datosGrafica);
                break;
            case '2':
            case '3':
            case '4':
                $datosGrafica = $this->reportesmodel->generarArrayMultiBarChar($datosGrafica);
                break;
        }
        
        switch($tipoTabla)
        {
            case '1':
            case '2':
                if($multiSerie == 1)
                {
                    $nuevaTabla = $this->reportesmodel->tablaSimpleMultiSerie($datosTabla, $datosGrafica, $tituloEjeX, $tituloEjeY, $tipoGrafico);
                }
                else
                {
                    if($tipoTabla < 3)
                    {
                        $nuevaTabla = $this->reportesmodel->tablaSimpleUnaSerie($datosTabla, $datosGrafica, $tituloEjeX, $tituloEjeY, $tipoGrafico);
                    }
                }
                break;
            case '3':
                if($multiSerie == 1)
                {
                    $nuevaTabla = $this->reportesmodel->tablaVariacionesMultiSerie($datosTabla, $datosGrafica, $tituloEjeX, $tituloEjeY, $tipoGrafico);
                }
                break;
            case '4':
                if($multiSerie == 1)
                {
                    $nuevaTabla = $this->reportesmodel->tablaCumplimientoMultiSerie($datosTabla, $datosGrafica, $tituloEjeX, $tituloEjeY, $tipoGrafico);
                }
                break;
        }
        
        // iniciamos la librería PHPExcel
        $this->load->library('excel');
        
        // creamos el objeto PHPExcel
        $objPHPExcel = new PHPExcel();
        
        // CAMBIAR setTitle POR EL NOMBRE DEL WIDGET
        $objPHPExcel->getProperties()->setTitle("widgets")->setDescription("Detalle widget, generado por Sinergia Soluciones Tecnológicas S.A.S.");
 
        $objPHPExcel->setActiveSheetIndex(0);
        
        // llenamos los títulos con los nombres de campo
        //$fields = $resultSql->list_fields();
        $fields = $nuevaTabla['titulos'];
        $col = 0;
        // creamos un array con los nombres de las columnas, ya que la función getStyle solo recibe la dirección de la celda en el formato A1
        $cols=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        
        // llenamos los encabezados del archivo y les damos formato
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $objPHPExcel->getActiveSheet()->getStyle($cols[$col].'1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle($cols[$col].'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $col++;
        }
 
        // llenamos los datos de la tabla, iniciamos $row en 2 ya que en la primera fila están los títulos
        $row = 2;
        foreach($nuevaTabla['datos'] as $data)
        {
            $col = 0;
            foreach ($data as $field)
            {
                //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $field);
                $col++;
            }
 
            $row++;
        }
 
        // auto-ajustamos las columnas del archivo
        foreach(range('A','Z') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // enviamos los encabezados para obligar al navegador a que descargue el archivo
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Widget_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        // forzamos al usuario para que descargue el archivo sin escribirlo en el servidor
        $objWriter->save('php://output');
    }
    
    public function pdfPanelPrincipal()
    {
        $consultaSql = "SELECT us.idwidge, us.idwiusu, us.posicion, us.undsancho, us.tipografico, wi.tipotabla, wi.nomwidge, wi.minrefresco, wi.tituloejex, wi.tituloejey, IF(consultasql LIKE '%0 AS ordenserie%', 0, 1) AS multiserie, wi.descripcion FROM default_rg_widgets_usuarios us LEFT JOIN default_rg_widgets wi USING(idwidge) WHERE idusuar = '".$this->current_user->id."' ORDER BY posicion";
        $datosVista['widgetsUsuario'] = $this->db->query($consultaSql);
        
        $datosVista['modulosMenu'] = $this->modulosWidgets;
        $datosVista['pdfVista'] = '1'; // determinamos si el servidor crea los pdf para enviar por correo
        $datosVista['pdfVista'] = '1';
        
        $this->template->build('admin/vistaUsuarioWidgets', $datosVista);
    }
    
    public function serverGraficoAImg()
    {
        $datosImagen =  $_POST['datosImagenGrafico'];
        $idRegistro = $_POST['idRegistro'];
        $datosImagen = str_replace("[removed]", "data:image/jpeg;base64,", $datosImagen);
        
        $data = $datosImagen;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        
        // si la carpeta con el id del usuario no existe se crea
        $rutaCarpeta = $this->sinergia->rutaProyecto.'/temp/'.$this->current_user->id.'/';
        $this->sinergia->crearCarpetaRuta($rutaCarpeta);
        
        file_put_contents('temp/'.$this->current_user->id.'/'.$idRegistro.'.jpeg', $data);
        echo json_encode(array('status' => '', 'msg' => 'imagen creada'));
    }
    
    public function serverTablaAArchivo()
    {
        $htmlTabla =  $_POST['htmlTabla'];
        $htmlTabla = str_replace("styyyyleeee=", "style=",$htmlTabla);
        $idRegistro = $_POST['idRegistro'];
        
        // si la carpeta con el id del usuario no existe se crea
        $rutaCarpeta = $this->sinergia->rutaProyecto.'/temp/'.$this->current_user->id.'/';
        $this->sinergia->crearCarpetaRuta($rutaCarpeta);
        
        
        $rutaArchivo = 'temp/'.$this->current_user->id.'/'.$idRegistro.'.html';
        unlink($rutaArchivo);
        // creamos el archivo
        $file=fopen($rutaArchivo,'a+') or die("Problemas al crear el archivo");
        //vamos añadiendo el contenido
        fwrite($file, $htmlTabla);
        fclose($file);
        
        echo json_encode(array('status' => '', 'msg' => 'tabla creada'));
    }
    
    public function creacionPdfPanelPrincipal()
    {
        $consultaSql = "SELECT us.idwidge, us.idwiusu, us.posicion, us.undsancho, us.tipografico, wi.tipotabla, wi.nomwidge, wi.minrefresco, wi.tituloejex, wi.tituloejey, IF(consultasql LIKE '%0 AS ordenserie%', 0, 1) AS multiserie, wi.descripcion FROM default_rg_widgets_usuarios us LEFT JOIN default_rg_widgets wi USING(idwidge) WHERE idusuar = '".$this->current_user->id."' ORDER BY posicion";
        $widgetsUsuario = $this->db->query($consultaSql);
        
        $hmtlToPdf = '
                            <style>
                                body{font-family:"Droid Sans",Helvetica,Arial,sans-serif;color:#353535;font-size:14px}
                                th,.textocentrado,.td-center{text-align:center}
                                .css-colnum{text-align:right}
                                .widget{margin:15px 0 15px 0;border:1px #000 solid;display:inline-block}
                                .tabla-widget{border:none !important}
                                .widget-header{padding:5px 7px;font-size:14px;color:#fff;border-bottom:1px #ccc solid;font-weight:bold;background:#19618E}
                                .graficoWidgets{height:370px;margin:0 auto}
                                .graficoWidgets img{margin:0 auto}
                                .widget-table{padding:0 5px}
                                .widget-table .table th,.widget-table .table td{font-size:11px}
                                .table-bordered th,.table-bordered td{border:1px solid #000}
                                /*.table-bordered td{border:1px solid #ddd}
                                .table-bordered td:first-child{border-color:#ddd #ddd #ddd #000}
                                .table-bordered td:last-child{border-color:#ddd #000 #ddd #ddd}
                                .table-bordered tr:last-child td{border-bottom:1px solid #000;}*/
                                .table{border-collapse:collapse;border-spacing:0px;width:100%}
                                .tablasWidgets{min-height:370px}
                                .css-porcProgress{display:block;float:right;width:60px;padding-left:5px;font-size:12px;font-weight:bold;text-align:right}
                                .progress{height:20px;margin-bottom:20px;overflow:hidden;background:#ddd;margin:0px 65px 0px 0px;width:60px}
                                .bar{float:left;display:block;height:100%}
                                .progress-info .bar{background:#4bb1cf}
                                .progress-danger .bar{background:#dd514c}
                                .progress-warning .bar{background:#faa732}
                                .progress-success .bar{background:#5eb95e}
                                .centrarPie{padding-left: 195px}
                            </style>';
        
        $hmtlToPdf .= '
                    <page backtop="15mm" backbottom="15mm" backleft="0mm" backright="0mm">
                        <page_header>
                            <table style="width:100%;border:none;">
                                <tr>
                                    <td style="text-align:left;width:50%;font-size:10px"><img src="'.base_url().'\images/Logo_amer.gif" alt="" height="57"/></td>
                                    <td style="text-align:center;width:0%;font-weight:bold"></td>
                                    <td style="text-align:right;width:50%;"><img src="'.base_url().'\images/logo-ini.jpg" alt="" width="170" height="57"/></td>
                                </tr>
                            </table>
                        </page_header>
                        <page_footer>
                            <table style="width:100%;font-size:10px;vertical-align:text-top;border:none;">
                                <tr>
                                    <td style="text-align:left;width:40%">Impreso por:  Sinergia ERP v3.0</td>
                                    <td style="text-align:center;width:20%"><a href="http://www.sinergia.com.co" target="_blank">http://www.sinergia.com.co</a></td>
                                    <td style="text-align:right;width:40%">Página [[page_cu]] de [[page_nb]]</td>
                                </tr>
                            </table>
                        </page_footer>';
        
        foreach ($widgetsUsuario->result() AS $idArray => $datosArray)
        {
            $idWiusu = $datosArray->idwiusu;
            $nomWidge = $datosArray->nomwidge;
            $tipoGrafico = $datosArray->tipografico;
            
            $anchoWidget = 662;  // usamos un ancho fijo para llenar el ancho total de la pagina
            
            if($tipoGrafico != 0)
            {
                $hmtlToPdf .= '
                            <div class="widget" style="width:'.$anchoWidget.'px">
                                <div class="widget-header">'.$nomWidge.'</div>
                                <div class="graficoWidgets '.($tipoGrafico == 5 ? 'centrarPie' : '').'">
                                    <img src="'.base_url().'temp/'.$this->current_user->id.'/'.$idWiusu.'.jpeg" alt="" />
                                </div>
                            </div>';
            }
            else
            {
                
                $archivoTabla = fopen('temp/'.$this->current_user->id.'/'.$idWiusu.'.html', 'r') or exit("no se abrio el archivo!");
                //Output a line of the file until the end is reached
                $hmtlToPdf .= '<div class="widget tabla-widget" style="width:'.$anchoWidget.'px">
                                <div class="widget-header">'.$nomWidge.'</div>
                                    <div class="tablasWidgets">';
                while(!feof($archivoTabla))
                {
                    $hmtlToPdf .= fgets($archivoTabla);
                }
                $hmtlToPdf .= '</div>
                            </div>';
                fclose($archivoTabla);
                
            }
        }
        
        $hmtlToPdf .= '</page>';
        
        //echo $hmtlToPdf;
        
        $pdf_nombreArchivo  = 'Sinergia Reportes.pdf';
        $this->load->library('htmltopdf');
        $this->htmltopdf->convert_html_to_pdf($hmtlToPdf, $pdf_nombreArchivo,array(216,279));
         
    }

}


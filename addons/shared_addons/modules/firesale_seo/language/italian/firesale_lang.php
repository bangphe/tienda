<?php defined('BASEPATH') or exit('No direct script access allowed');

    // General
    $lang['firesale:seo:title']    = 'Ottimizzazione per Motori di ricerca';
    $lang['firesale:tab_metadata'] = 'Meta Data';

    // Labels
    $lang['firesale:seo:label_title']    = 'Meta Titolo';
    $lang['firesale:seo:label_desc']     = 'Meta Descrizione';
    $lang['firesale:seo:label_keywords'] = 'Meta Keywords';

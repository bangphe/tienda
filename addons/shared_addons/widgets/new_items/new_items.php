<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Widget_New_Items extends Widgets
{
    // The widget title,  this is displayed in the admin interface
    public $title = array(
        'en' => 'New Items (Widget)',
        'es' => 'New Items (Widget)'
    );
    public $description = array(
        'en' => 'New Items (Widget)',
        'es' => 'New Items (Widget)'
    );
 
    // The author's name
    public $author = 'Christian España';
 
    // The authors website for the widget
    public $website = 'www.imaginamos.com';
 
    //current version of your widget
    public $version = '1.0';
	
    public function run()
    {
    	
    }
}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @author  Kevin vargas
 */
class Home_Banner_Two_m extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->_table = $this->db->dbprefix . 'home_banner_two';
    }

}
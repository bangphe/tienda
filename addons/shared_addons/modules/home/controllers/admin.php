<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Brayan Acebo
 */
class Admin extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('home');
        $this->template->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));
        $models = array(
            'home_pautas_m',
            'home_slider_m',
            'productos_m',
            'home_banner_m',
            'home_banner_two_m',
            'firesale/products_m',
            'home_others_m'
        );
        $this->load->model($models);
    }

    // -----------------------------------------------------------------

    public function index()
    {
    	// Banner
        $banner = $this->home_banner_m->get_all();

        // Banner 2
        $banner_two = $this->home_banner_two_m->get_all();
		
        // Pautas
        $pautas = $this->home_pautas_m
                ->get_all();
		
        // Productos
        /*$productos = $this->productos_m
                ->order_by('title', 'ASC')
                ->get_all();
		echo "<pre> ";
		print_r($productos);
		echo "</pre> ";*/
				
			
		$productos = cache('products_m/get_products', array(), 0, 0);
		
		if(!empty($productos))
		{
			foreach ($productos AS $item => $value)
	        {
	            $pro = $this->db->query("SELECT * FROM default_firesale_products WHERE id=".$value['id'])->row();
				if(!empty($pro))
				{
					$productos[$item]['title'] = $pro->title;
				}
	        }
		}
		
		/*echo "<pre> ";
		print_r($productos);
		echo "</pre> ";*/
				
        // Slider de productos destacados
        $slider = $this->home_slider_m
                ->order_by('orden', 'ASC')
                ->get_all();

        $others = $this->home_others_m->get_all();

        $items = array();
		if(!empty($slider))
		{
	        foreach ($slider as $item)
	        {
	            $pro = $this->db->query("SELECT * FROM default_firesale_products WHERE id=$item->producto_id")->row();
				if(!empty($pro))
				{
		            $items[] = array(
		                'item_id' => $item->id,
		                'producto_id' => $pro->id,
		                'titulo' => $pro->title,
		                'descripcion' => substr(strip_tags($pro->description), 0, 100) . '...',
		                'precio' => $pro->price,
		                'orden' => $item->orden
		            );
				}
	        }
		}

        $this->template
                ->set('pautas', $pautas)
                ->set('productos', $productos)
                ->set('items', $items)
				->set('banner', $banner)
                ->set('banner_two', $banner_two)
                ->set('others', $others)
                ->build('admin/home_back');
    }

    /*
     * Pautas
     */

    public function nueva_pauta()
    {
        $this->template->build('admin/nueva_pauta_back');
    }

    // -----------------------------------------------------------------

    public function crear_pauta() {

        $this->form_validation->set_rules('link', 'Link', 'valid_url');

        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();
            $data = array(
                'link' => $post->link
            );

            $config['upload_path'] = './' . UPLOAD_PATH . '/home_pautas';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2050;
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);

            // imagen
            $img = $_FILES['imagen']['name'];

            if (!empty($img)) {
                if ($this->upload->do_upload('imagen')) {
                    $datos = array('upload_data' => $this->upload->data());
					$path = UPLOAD_PATH . 'home_pautas/' . $datos['upload_data']['file_name'];
                    $img = array('imagen' => $path);
                    $data = array_merge($data, $img);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    return $this->index();
                }
            }

            if ($this->home_pautas_m->insert($data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/home/');
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
                return $this->nueva_pauta();
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            return $this->nueva_pauta();
        }
    }

    // -----------------------------------------------------------------

    public function editar_pauta($id = null) {

        $id or redirect('admin/home');

        $pauta = $this->home_pautas_m->get($id);

        $this->template
                ->set('pauta', $pauta)
                ->build('admin/editar_pauta_back');
    }

    // -----------------------------------------------------------------

    public function actualizar_pauta() {

        $this->form_validation->set_rules('link', 'Link', 'valid_url');

        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();

            $data = array(
                'link' => $post->link
            );

            $config['upload_path'] = './' . UPLOAD_PATH . '/home_pautas';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2050;
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);

            // imagen uno
            $img = $_FILES['imagen']['name'];

            if (!empty($img)) {
                if ($this->upload->do_upload('imagen')) {
                    $datos = array('upload_data' => $this->upload->data());
					$path = UPLOAD_PATH . 'home_pautas/' . $datos['upload_data']['file_name'];
                    $img = array('imagen' => $path);
                    $data = array_merge($data, $img);
                    $obj = $this->db->where('id', $post->id)->get('home_pautas')->row();
					@unlink($obj->imagen);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    return $this->index();
                }
            }

            if ($this->home_pautas_m->update($post->id, $data)) {
                $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                redirect('admin/home/');
            } else {
                $this->session->set_flashdata('success', lang('home:error_message'));
                return $this->editar_pauta();
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            return $this->editar_pauta();
        }
    }

    // -----------------------------------------------------------------

    public function eliminar_pauta($id = null) {

        $id or redirect('admin/home/');

        $obj = $this->db->where('id', $id)->get('home_pautas')->row();

        if ($this->home_pautas_m->delete($id)) {
            @unlink($obj->imagen);
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/');
    }

    /*
     * Crear Destacado
     */

    public function crear_destacado() {

        $post = (object) $this->input->post();

        $valid = $this->home_slider_m->get_by('producto_id', $post->producto_id);

        if (count($valid) > 0) {
            $this->session->set_flashdata('error', 'Este producto ya fue destacado');
            redirect('admin/home#page-slider');
        }

        $this->form_validation->set_rules('producto_id', 'Producto', 'required');

        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();
            $data = array(
                'producto_id' => $post->producto_id
            );

            if ($this->home_slider_m->insert($data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/home#page-slider');
            } else {
                $this->session->set_flashdata('error', lang('home:error_message'));
                redirect('admin/home#page-slider');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/home#page-slider');
        }
    }

    // -----------------------------------------------------------------

    public function eliminar_destacado($id = null) {

        $id or redirect('admin/home#page-slider');

        if ($this->home_slider_m->delete($id)) {
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home#page-slider');
    }

    // -----------------------------------------------------------------

    public function orden() {

        $post = (object) $this->input->post();

        $slider = $this->home_slider_m->get($post->orden_nuevo);

        $data = array(
            'orden' => $slider->orden
        );

        if ($this->home_slider_m->update($post->id, $data)) {
            $orden = array('orden' => $post->orden_actual);
            $this->home_slider_m->update($slider->id, $orden);
            $this->session->set_flashdata('success', 'El registro se actualizo con éxito.');
        }
        
        $this->session->set_flashdata('error', 'No se logro actualizar el registro, inténtelo nuevamente');
        redirect('admin/home#page-slider');
    }
	
	/*
     * Banner (slider)
     */
	
	public function edit_banner($idItem = null)
    {
        $this->form_validation->set_rules('title', 'Título', 'max_length[180]|trim');
        $this->form_validation->set_rules('text', 'Texto', 'max_length[300]|trim');
        $this->form_validation->set_rules('link', 'Link', 'max_length[420]|valid_url');
		
		if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion
		{
			if(validation_errors() == "")
			{
				$this->session->set_flashdata('error', validation_errors());
			}
			if(!empty($idItem))  // si se envia un dato por la URL se hace lo siguiente (Edita)
			{
				$idItem or redirect('admin/home/');
				
				$titulo = 'Editar Banner';
		        $banner = $this->home_banner_m->get($idItem);
				 
		        $this->template
		                ->set('banner', $banner)
						->set('titulo', $titulo)
		                ->build('admin/edit_banner_back');
			}
			else 
			{
				$titulo = 'Crear Banner';
			 	$this->template
					 	->set('titulo', $titulo)
					 	->build('admin/edit_banner_back');
			}
		}
		else // si el formulario ha sido enviado con éxito se procede
		{
			$action = $_POST['btnAction'];
			unset($_POST['btnAction']);
			if($idItem != null)  // si se envia un dato por la URL se hace lo siguiente (Edita)
			{
	            $data = $_POST;
				
	            $config['upload_path'] = './' . UPLOAD_PATH . '/home_banner';
	            $config['allowed_types'] = 'gif|jpg|png|jpeg';
	            $config['max_size'] = 2050;
	            $config['encrypt_name'] = true;
	
	            $this->load->library('upload', $config);
	
	            // imagen uno
	            $img = $_FILES['image']['name'];
	
	            if (!empty($img)) {
	                if ($this->upload->do_upload('image')) {
	                    $datos = array('upload_data' => $this->upload->data());
	                    $path = UPLOAD_PATH . 'home_banner/' . $datos['upload_data']['file_name'];
	                    $img = array('image' => $path);
	                    $data = array_merge($data, $img);
	                    $obj = $this->db->where('id', $idItem)->get('home_banner')->row();
	                    @unlink($obj->imagen);
	                }
	                else
	                {
	                    $this->session->set_flashdata('error', $this->upload->display_errors());
	                    header('Location: '.$_SERVER['REQUEST_URI']);
	                }
	            }
	
	            if ($this->home_banner_m->update($idItem, $data))
	            {
	                $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
					if($action == 'save')
					{
						header('Location: '.$_SERVER['REQUEST_URI']);
					}
					else
					{
						redirect('admin/home/');
					}
	            }
	            else
	            {
	                $this->session->set_flashdata('error', lang('home:error_message'));
	                header('Location: '.$_SERVER['REQUEST_URI']);
	            }
			}
			else
			{
	            $data = $_POST;
	
	            $config['upload_path'] = './' . UPLOAD_PATH . '/home_banner';
	            $config['allowed_types'] = 'gif|jpg|png|jpeg';
	            $config['max_size'] = 2050;
	            $config['encrypt_name'] = true;
	
	            $this->load->library('upload', $config);
	
	            // imagen uno
	            $img = $_FILES['image']['name'];
	
	            if (!empty($img)) {
	                if ($this->upload->do_upload('image')) {
	                    $datos = array('upload_data' => $this->upload->data());
	                    $path = UPLOAD_PATH . 'home_banner/' . $datos['upload_data']['file_name'];
	                    $img = array('image' => $path);
	                    $data = array_merge($data, $img);
	                }
	                else
	                {
	                    $this->session->set_flashdata('error', $this->upload->display_errors());
	                    header('Location: '.$_SERVER['REQUEST_URI']);
	                }
	            }
	
	            if ($this->home_banner_m->insert($data))
	            {
	                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
					redirect('admin/home/');
	            }
	            else
	            {
	                $this->session->set_flashdata('error', lang('home:error_message'));
	                header('Location: '.$_SERVER['REQUEST_URI']);
	            }
			}
		}
    }

    public function delete_banner($id = null) {

        $id or redirect('admin/home/');

        $obj = $this->db->where('id', $id)->get($this->db->dbprefix.'home_banner')->row();

        if ($this->home_banner_m->delete($id)) {
            @unlink($obj->image);
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/');
    }

    /*
     * Banner
     */
    
    public function edit_banner_two($idItem = null)
    {
        $this->form_validation->set_rules('title', 'Título', 'max_length[180]|trim');
        $this->form_validation->set_rules('text', 'Texto', 'max_length[300]|trim');
        $this->form_validation->set_rules('link', 'Link', 'max_length[420]|valid_url');
        
        if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion
        {
            if(validation_errors() == "")
            {
                $this->session->set_flashdata('error', validation_errors());
            }
            if(!empty($idItem))  // si se envia un dato por la URL se hace lo siguiente (Edita)
            {
                $idItem or redirect('admin/home/');
                
                $titulo = 'Editar Banner ';
                $banner_two = $this->home_banner_two_m->get($idItem);
                 
                $this->template
                        ->set('banner_two', $banner_two)
                        ->set('titulo', $titulo)
                        ->build('admin/edit_banner_two_back');
            }
            else 
            {
                $titulo = 'Crear Banner';
                $this->template
                        ->set('titulo', $titulo)
                        ->build('admin/edit_banner_two_back');
            }
        }
        else // si el formulario ha sido enviado con éxito se procede
        {
            $action = $_POST['btnAction'];
            unset($_POST['btnAction']);
            if($idItem != null)  // si se envia un dato por la URL se hace lo siguiente (Edita)
            {
                $data = $_POST;
                
                $config['upload_path'] = './' . UPLOAD_PATH . '/home_banner_two';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 2050;
                $config['encrypt_name'] = true;
    
                $this->load->library('upload', $config);
    
                // imagen uno
                $img = $_FILES['image']['name'];
    
                if (!empty($img)) {
                    if ($this->upload->do_upload('image')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'home_banner_two/' . $datos['upload_data']['file_name'];
                        $img = array('image' => $path);
                        $data = array_merge($data, $img);
                        $obj = $this->db->where('id', $idItem)->get('home_banner_two')->row();
                        @unlink($obj->imagen);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: '.$_SERVER['REQUEST_URI']);
                    }
                }
    
                if ($this->home_banner_two_m->update($idItem, $data))
                {
                    $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                    if($action == 'save')
                    {
                        header('Location: '.$_SERVER['REQUEST_URI']);
                    }
                    else
                    {
                        redirect('admin/home/');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', lang('home:error_message'));
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }
            }
            else
            {
                $data = $_POST;
    
                $config['upload_path'] = './' . UPLOAD_PATH . '/home_banner_two';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 2050;
                $config['encrypt_name'] = true;
    
                $this->load->library('upload', $config);
    
                // imagen uno
                $img = $_FILES['image']['name'];
    
                if (!empty($img)) {
                    if ($this->upload->do_upload('image')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'home_banner_two/' . $datos['upload_data']['file_name'];
                        $img = array('image' => $path);
                        $data = array_merge($data, $img);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: '.$_SERVER['REQUEST_URI']);
                    }
                }
    
                if ($this->home_banner_two_m->insert($data))
                {
                    $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                    redirect('admin/home/');
                }
                else
                {
                    $this->session->set_flashdata('error', lang('home:error_message'));
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }
            }
        }
    }

    public function delete_banner_two($id = null) {

        $id or redirect('admin/home/');

        $obj = $this->db->where('id', $id)->get('home_banner_two')->row();

        if ($this->home_banner_two_m->delete($id)) {
            @unlink($obj->image);
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/');
    }

    // otros
    public function edit_others_img($idItem = null)
    {
        $this->form_validation->set_rules('image', 'Imagen', 'max_length[180]|trim');
        
        if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion
        {
            if(validation_errors() == "")
            {
                $this->session->set_flashdata('error', validation_errors());
            }
            if(!empty($idItem))  // si se envia un dato por la URL se hace lo siguiente (Edita)
            {
                $idItem or redirect('admin/home/');
                
                $titulo = 'Editar Imagen ';
                $item = $this->home_others_m->get($idItem);
                 
                $this->template
                        ->set('item', $item)
                        ->set('titulo', $titulo)
                        ->build('admin/edit_others_img');
            }
            else 
            {
                $titulo = 'Crear Imagen';
                $this->template
                        ->set('titulo', $titulo)
                        ->build('admin/edit_others_img');
            }
        }
        else // si el formulario ha sido enviado con éxito se procede
        {
            $action = $_POST['btnAction'];
            unset($_POST['btnAction']);
            if($idItem != null)  // si se envia un dato por la URL se hace lo siguiente (Edita)
            {
                $data = $_POST;
                $data['type'] = '1';
                
                $config['upload_path'] = './' . UPLOAD_PATH . '/home_others';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 2050;
                $config['encrypt_name'] = true;
    
                $this->load->library('upload', $config);
    
                // imagen uno
                $img = $_FILES['image']['name'];
    
                if (!empty($img)) {
                    if ($this->upload->do_upload('image')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'home_others/' . $datos['upload_data']['file_name'];
                        $img = array('content' => $path);
                        $data = array_merge($data, $img);
                        $obj = $this->db->where('id', $idItem)->get('home_others')->row();
                        @unlink($obj->imagen);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: '.$_SERVER['REQUEST_URI']);
                    }
                }
    
                if ($this->home_others_m->update($idItem, $data))
                {
                    $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                    if($action == 'save')
                    {
                        header('Location: '.$_SERVER['REQUEST_URI']);
                    }
                    else
                    {
                        redirect('admin/home/');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', lang('home:error_message'));
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }
            }
            else
            {
                $data = $_POST;
                $data['type'] = '1';
    
                $config['upload_path'] = './' . UPLOAD_PATH . '/home_others';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 2050;
                $config['encrypt_name'] = true;
    
                $this->load->library('upload', $config);
    
                // imagen uno
                $img = $_FILES['image']['name'];
    
                if (!empty($img)) {
                    if ($this->upload->do_upload('image')) {
                        $datos = array('upload_data' => $this->upload->data());
                        $path = UPLOAD_PATH . 'home_others/' . $datos['upload_data']['file_name'];
                        $img = array('content' => $path);
                        $data = array_merge($data, $img);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        header('Location: '.$_SERVER['REQUEST_URI']);
                    }
                }
    
                if ($this->home_others_m->insert($data))
                {
                    $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                    redirect('admin/home/');
                }
                else
                {
                    $this->session->set_flashdata('error', lang('home:error_message'));
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }
            }
        }
    }

    public function edit_others_vid($idItem = null)
    {
        $this->form_validation->set_rules('video', 'Video', 'max_length[180]|trim');
        
        if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion
        {
            if(validation_errors() == "")
            {
                $this->session->set_flashdata('error', validation_errors());
            }
            if(!empty($idItem))  // si se envia un dato por la URL se hace lo siguiente (Edita)
            {
                $idItem or redirect('admin/home/');
                
                $titulo = 'Editar Video ';
                $item = $this->home_others_m->get($idItem);
                 
                $this->template
                        ->set('item', $item)
                        ->set('titulo', $titulo)
                        ->build('admin/edit_others_vid');
            }
            else 
            {
                $titulo = 'Crear Video';
                $this->template
                        ->set('titulo', $titulo)
                        ->build('admin/edit_others_vid');
            }
        }
        else // si el formulario ha sido enviado con éxito se procede
        {
            $action = $_POST['btnAction'];
            unset($_POST['btnAction']);
            if($idItem != null)  // si se envia un dato por la URL se hace lo siguiente (Edita)
            {
                $data = $_POST;
                $data['type'] = '2';
                
                // video
                $video = $data['content'];

                if (!empty($video)) {
                    $_video = array('content' => $video);
                    $data = array_merge($data, $_video);
                } else {
                    $this->session->set_flashdata('error', 'Debe pegar el link del video');
                    redirect('admin/gallery/new_gallery');
                }
    
                if ($this->home_others_m->update($idItem, $data))
                {
                    $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                    if($action == 'save')
                    {
                        header('Location: '.$_SERVER['REQUEST_URI']);
                    }
                    else
                    {
                        redirect('admin/home/');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', lang('home:error_message'));
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }
            }
            else
            {
                $data = $_POST;
                $data['type'] = '2';
                
                // video
                $video = $data['content'];

                if (!empty($video)) {
                    $_video = array('content' => $video);
                    $data = array_merge($data, $_video);
                } else {
                    $this->session->set_flashdata('error', 'Debe pegar el link del video');
                    redirect('admin/gallery/new_gallery');
                }
    
                if ($this->home_others_m->insert($data))
                {
                    $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
                    if($action == 'save')
                    {
                        header('Location: '.$_SERVER['REQUEST_URI']);
                    }
                    else
                    {
                        redirect('admin/home/');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', lang('home:error_message'));
                    header('Location: '.$_SERVER['REQUEST_URI']);
                }
            }
        }
    }

    public function delete_others($id = null) {

        $id or redirect('admin/home/');

        $obj = $this->db->where('id', $id)->get('home_others')->row();

        if ($this->home_others_m->delete($id)) {
            @unlink($obj->image);
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/home/');
    }
    
}
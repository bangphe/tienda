
<?php if( empty($entries) ): ?>
    <div class="no_data"><?php echo lang('firesale:reviews:none'); ?>
<?php else: ?>
    <table>
        <thead>
            <th class="author"><?php echo lang('firesale:label_author'); ?></th>
            <th class="product"><?php echo lang('firesale:label_product'); ?></th>
            <th class="status"><?php echo lang('firesale:label_status'); ?></th>
            <th class="rating"><?php echo sprintf(lang('firesale:label_rating'), '0.0 - 5.0'); ?></th>
        </thead>
        <tbody>
<?php foreach( $entries AS $entry ): ?>
            <tr>
                <td><?php echo $entry['created_by']['display_name']; ?></td>
                <td><?php echo $entry['product']['title']; ?></td>
                <td><?php echo $entry['status']['value']; ?></td>
                <td class="rating"><span class="stars"><span style="width: <?php echo $entry['width']; ?>"></span></span><?php echo $entry['rated']; ?></li></td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>

    <a href="/admin/firesale_reviews" class="btn blue"><span><?php echo lang('firesale:label_viewmore'); ?></span></a>

<?php endif; ?>

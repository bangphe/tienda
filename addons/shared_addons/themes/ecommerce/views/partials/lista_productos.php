<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6 no-js" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7 no-js" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="en"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<!-- html5.js for IE less than 9 -->
<!--[if lt IE 9]>  <script src="assets/js/lib/html5.js"></script>  <![endif]-->
<html>
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta http-equiv="content-language" content="es" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title>Imaginamos.com / e-commerce</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="copyright" content="imaginamos.com" />
  <meta name="date" content="2014" />
  <meta name="author" content="diseño web: imaginamos.com" />
  <meta name="robots" content="All" />
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <link rel="author" type="text/plain" href="humans.txt" />
  <!--Style's-->
  <link href="assets/css/tienda.css" rel="stylesheet" type="text/css" />
  <!--Librerias-->
  <script src="assets/js/lib/modernizr.min.js"></script>
</head>

<body>

<!-- <section id="preload">
    <div id="preload-container">
      <div id="dot"></div>
        <div class="step" id="s1"></div>
        <div class="step" id="s2"></div>
        <div class="step" id="s3"></div>
    </div>
</section>
 -->
<?php include("nav_responsive.php"); ?>  

<section class="content_all">

  <?php include("header.php"); ?> 

  <!--Contenidos Sitio-->
  <section class="barra_int"></section>
  <section class="cont_home">   

      <!--Miga de pan-->
      <div class="bg_miga">
        <ul class="marco miga_pan">
          <a href=""><i class="fa fa-angle-double-right"></i>Consestetur adipiscing</a>
          <a href=""><i class="fa fa-angle-double-right"></i>Usted, está aquí!</a>
        </ul>
          <div class="clear"></div>
      </div>    
      <!--Fin Miga de pan-->

      <div class="clear"></div>

      <!--Titulo-->
      <div class="tittles marco">
        <h1>CHAQUETAS PARA HOMBRE</h1>
        <span>39 Items</span>
      </div>
      <!--Fin Titulo-->

      <div class="clear"></div>
      
       <!--Contenidos Sitio-->
      <section class="cont_produt marco">
        <!--Filtrador de busqueda-->
        <section class="row buscador_cat">
          <article class="large-3 medium-3 small-12 columns"><strong>Búsqueda por Categoría</strong></article>
          <article class="large-9 medium-9 small-12 columns filtros_produc">
             <div class="controls elem_blancos">
                <div class="large-9 medium-9 small-12 columns filter_for_tipo">
                  <label>Filtrar por:</label>                
                  <select class="filtro_select ">
                    <option value="0">Talla </option>
                    <option value=".category-1">XL</option>
                    <option value=".category-3">L</option>
                    <option value=".category-3">S</option>
                    <option value=".category-2">m</option>
                  </select>

                  <select class="filtro_select">
                    <option value="0">Color </option>                  
                    <option value=".category-3">Cafe</option>
                    <option value=".category-1">Azul</option>
                    <option value=".category-1">Negro</option>
                    <option value=".category-2">verde</option>
                    <option value=".category-1">Blanco</option>
                    <option value=".category-3">Rojo</option>
                  </select>

                  <select class="filtro_select">
                    <option value="0">Estilo </option>                  
                    <option value=".category-2">Cafe</option>
                    <option value=".category-1">Azul</option>
                    <option value=".category-1">Negro</option>
                    <option value=".category-3">verde</option>
                    <option value=".category-2">Blanco</option>
                    <option value=".category-2">Rojo</option>
                  </select>

                  <select class="filtro_select">
                    <option value="0">Precio </option>
                    <option value=".category-1">$ 0 - $ 50.000</option>
                    <option value=".category-3">$ 50.000 - $ 150.000</option>
                    <option value=".category-2">$ 150.000 - $ 250.000</option>
                  </select>
                </div>
                <div class="large-3 medium-3 small-12 columns filter_for_orden">
                   <label>Ordenar por:</label>
                   <select class="filtro_orden">
                      <option value="myorder:asc">Recomendadas</option>
                      <option value="myorder:desc">Más compradas</option>
                   </select>
                </div>

                <div class="large-3 medium-3 small-12 columns filter_responsive">
                    <select class="filtro_select item1">
                      <option value="0">Categoria </option>                  
                      <option value=".category-3">Categoria</option>
                      <option value=".category-1">Categoria</option>
                      <option value=".category-3">Categoria</option>
                      <option value=".category-2">Categoria</option>
                      <option value=".category-1">Categoria</option>
                      <option value=".category-3">Categoria</option>
                    </select>
                    <select class="filtro_select item2">
                      <option value="0">Subcategoria </option>                  
                      <option value=".category-3">Subcategoria</option>
                      <option value=".category-1">Subcategoria</option>
                      <option value=".category-3">Subcategoria</option>
                      <option value=".category-2">Subcategoria</option>
                      <option value=".category-1">Subcategoria</option>
                      <option value=".category-3">Subcategoria</option>
                    </select>
                </div>

              </div>
          </article>
        </section>
        <!--Fin Filtrador de busqueda-->


        <!--TODOS LOS PRODUCTOS-->
        <section class="all_product_categ">
          <!--Categorias-->
          <article class="large-3 medium-3 small-12 columns relative list_categ_lis">
            <ul class="list_cat">
              <li>
                <a href="javascript:void(0)" class="active_acord">Consequat nulla</a>
                <div class="conten_subCagtegori">
                    <a href="" title="">Adipiscing elit</a>
                    <a href="" title="">Consectetur adipiscing</a>
                    <a href="" title="">Lorem ipsum dolor</a>
                      <div class="clear"></div>
                </div>
              </li>
              <li>
                <a href="javascript:void(0)">Consequat nulla</a>
                <div class="conten_subCagtegori">
                    <a href="" title="">Adipiscing elit</a>
                    <a href="" title="">Consectetur adipiscing</a>
                    <a href="" title="">Lorem ipsum dolor</a>
                      <div class="clear"></div>
                </div>
              </li>
              <li>
                <a href="javascript:void(0)">Consequat nulla</a>
                <div class="conten_subCagtegori">
                    <a href="" title="">Adipiscing elit</a>
                    <a href="" title="">Consectetur adipiscing</a>
                    <a href="" title="">Lorem ipsum dolor</a>
                      <div class="clear"></div>
                </div>
              </li>
              <li>
                <a href="javascript:void(0)">Consequat nulla</a>
                <div class="conten_subCagtegori">
                    <a href="" title="">Adipiscing elit</a>
                    <a href="" title="">Consectetur adipiscing</a>
                    <a href="" title="">Lorem ipsum dolor</a>
                      <div class="clear"></div>
                </div>
              </li>
              <li>
                <a href="">Consequat nulla</a>
              </li>

               <div class="clear"></div>
            </ul>

            <div class="newsll elem_blancos">
              <h3>Suscríbete a nuestro</h3>
              <h1>NEWSLETTER</h1>
              <p>Recibe descuentos, promociones exclusivas y se el primero en descubrir nuestras nuevas ofertas.</p>
              <form method="get" class="modal_all">
                 <input type="text" name="" value="Correo Electrónico">
                 <a href="#modal_nwslletter" data-effect="mfp-move-horizontal" class="btn_gris block">SUSCRIBIRME</a>  
              </form>
            </div>
          </article>
          <!--Fin Categorias-->

          <!--Productos-->
          <article class="large-9 medium-9 small-12 columns relative lista_products">             
              <div class="destacados container">

                <!--Productos uno-->
                  <li class="large-4 medium-6 small-6 columns mix category-1" data-myorder="1">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/1.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a>
                  </li>
                  <li class="large-4 medium-6 small-6 columns mix category-1" data-myorder="2">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <span class="etiq_new">NUEVO</span>
                          <img src="assets/images/productos/2.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a>
                  </li>
                  <li class="large-4 medium-6 small-6 columns mix category-1" data-myorder="3">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>                    
                          <img src="assets/images/productos/3.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a>
                  </li>
                  <li class="large-4 medium-6 small-6 columns mix category-1" data-myorder="4">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/5.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a>
                  </li>
                  <li class="large-4 medium-6 small-6 columns mix category-1" data-myorder="5" data-wow-delay="0.6s">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/6.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a> 
                  </li>
                  <li class="large-4 medium-6 small-6 columns mix category-1" data-myorder="6">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/7.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a> 
                  </li>
                <!--Fin Productos uno-->  

                <!--Productos dos-->
                  <li class="large-4 medium-6 small-6 columns mix category-2" data-myorder="1">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/7.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a> 
                  </li> 
                  <li class="large-4 medium-6 small-6 columns mix category-2" data-myorder="2">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/5.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a>
                  </li>
                   <li class="large-4 medium-6 small-6 columns mix category-2" data-myorder="3">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/1.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a>
                  </li>
                  <li class="large-4 medium-6 small-6 columns mix category-2" data-myorder="4">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <span class="etiq_new">NUEVO</span>
                          <img src="assets/images/productos/2.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a>
                  </li>                  
                  <li class="large-4 medium-6 small-6 columns mix category-2" data-myorder="5">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/7.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a> 
                  </li> 
                <!--Fin Productos dos-->  

                <!--Productos tres-->  
                  <li class="large-4 medium-6 small-6 columns mix category-3" data-myorder="1">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <span class="etiq_new">NUEVO</span>
                          <img src="assets/images/productos/2.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a>
                  </li>                  
                  <li class="large-4 medium-6 small-6 columns mix category-3" data-myorder="2">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/7.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a> 
                  </li> 
                  <li class="large-4 medium-6 small-6 columns mix category-3" data-myorder="3">
                    <a href="">
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          <img src="assets/images/productos/5.jpg" alt="">
                        </div>
                        <div class="color">2 Colores</div>
                        <p>Reloj consectetur sagittis tempor molestie</p>  
                        <div class="precio">$120.000</div>
                         <div class="clear"></div>
                        <a href="detalle.php" title="">
                        <div class="btn_border">
                          Detalle
                        </div>  
                        </a>
                      </div>
                    </a>
                  </li>
                <!--Fin Productos tres-->  

                <div class="gap"></div>
                <div class="gap"></div>
              </div>
          </article>
          <!-- Fin Productos-->

        </section>  
        <!--FIN TODOS LOS PRODUCTOS-->          

      </section>
        <!--Fin Contenidos Sitio-->

  </section>
  <!--Fin Contenidos Sitio-->

    <div class="clear"></div>

  <?php include("footer.php"); ?> 

</section><!--Content_all-->

</body>
</html>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class front extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('more_products_m');
    }

    public function remove_recent($id = NULL)
    {

        // Get products from store
        $products = $this->more_products_m->get_current();

        // Find key
        $key = array_search($id, $products);

        // Found it?
        if( $key !== FALSE ) {

            // Remove it
            unset($products[$key]);

            // Shuffle array order
            shuffle($products);

            // Set it back into store
            $this->more_products_m->save_current($products);

        }

        // Redirect
        redirect($_SERVER['HTTP_REFERER']);
    }
}

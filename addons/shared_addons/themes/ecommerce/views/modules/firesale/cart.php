  <div class="clear"></div>

      <!--Titulo-->

      <div class="tittles marco">

        <h1>CARRITO DE COMPRAS </h1>

      </div>

      <!--Fin Titulo-->

      <div class="clear"></div>

      <!--bg Carrito-->      

      <section class="bg_carrito">

          <!--Contenidos Sitio-->

          <section class="cont_produt marco">

            <!--Lista Carrito-->

            <article class="large-8 medium-8 small-12 columns ">

              <div class="caja_borde">

                 <ul class="list_carrito">

                    {{ contents }}  

                    <li>
                       <a href="{{ firesale:url route='cart' }}/remove/{{ rowid }}" class="delete_prd" type="">x</a>
                       <div class="large-2 medium-2 small-4 columns">
                         {{ if image }}
                          <a href="{{ if parent }}{{ firesale:url route='product' id=parent }}{{ else }}{{ firesale:url route='product' id=id }}{{ endif }}" title="View {{ name }}"><img src="{{ url:site }}files/large/{{ image }}" alt=" {{ name }}"/></a>
                          {{ else }}
                          <div class="cart-left">{{ theme:image file="no-image.png" alt=title }}</div>
                          {{ endif }}
                       </div>
                       <div class="detalle_prod_carrito large-10 medium-10 small-8 columns">
                           <div class="nom_prod large-6 medium-6 small-12 columns">
                              <h2>{{ name }}</h2>
                              <h4>{{ code }}</h4>
                              <h4> Cantidad: {{ qty }}</h4>
                           </div>
                           <div class="nom_prod2 large-6 medium-6 small-12 columns">
                              {{ if orig_price_formatted }}
                                <span>Precio Original:</span> <s><h1>{{ orig_price_formatted }}</h1></s>
                                <span>Precio de descuento:</span> <h1>{{ price }}</h1>
                              {{ else }}
                                <h1>{{ price }}</h1>
                              {{ endif }}
                              <h3>Subtotal {{ subtotal }}</h3>
                              {{ if options }}
                              <div class="options">
                                  <span class="title">Opciones</span><br>
                                  {{ options }}
                                    <span>{{ title }} : {{ value }}</span><br/>
                                  {{ /options }}
                              </div>
                              {{ endif }}
                           </div>
                       </div>

                       <div class="large-10 medium-10 small-12 columns">

                        {{ firesale:modifier_form_cart type="select" product=id_modifier id_encript=rowid }}

                       </div> 

                        <div class="clear"></div>

                    </li>

                     {{ /contents }}  

                </ul>

                   <div class="clear"></div>

              </div>

            </article>

            <!--Fin Lista Carrito-->

            <!--Lista de pago-->

            <article class="large-4 medium-4 small-12 columns padding_l">

              <div class="caja_borde">

                 <ul class="tabla_pagos">

                   <li>

                     <div class="large-8 medium-8 small-12 columns">

                          Cantidad de Artículos

                     </div>

                     <div class="large-4 medium-4 small-12 columns valor">

                          {{ firesale:cart }}

                            {{ count }}

                          {{ /firesale:cart }}

                     </div>

                   </li>

                    <form method="post" action="{{ firesale:url route='cart' }}/update" class="cart-view">

                      <li>

                       <div class="large-8 medium-8 small-12 columns">

                            Sub Total

                       </div>

                       <div class="large-4 medium-4 small-12 columns valor price-ship">

                            {{ subtotal }}

                       </div>

                     </li>

                     <li>

                       <div class="large-8 medium-8 small-12 columns">

                            Iva

                       </div>

                       <div class="large-4 medium-4 small-12 columns valor price-ship">

                            {{ tax }}

                       </div>

                     </li>

                      <div class="valor_comp">

                        <div class="large-6 medium-6 small-12 columns">TOTAL</div>

                        <div class="large-6 medium-6 small-12 columns valor_t">{{ total }}</div> 

                      </div>

                      <div class="clear"></div>                    

                      <input type="submit" name="btnAction" value="PAGAR AHORA" class="btn_gris" />

                    </form>

                   <!--<div class="term_compra">



                     <p>*Envío calculado para <span>Colombia</span>, <span>Bogotá D.C.</span>, <span>Despacho Estándar</span> <a href="" title="">Cambiar destino</a></p>                  



                   </div> -->      

                 </ul>

              </div>

            </article>

            

            <!--Fin Lista de pago--> 

             <div class="clear"></div>    

            <!--Codigos Descuentos-->

            <article class="large-12 medium-12 small-12 columns">


              <div class="codigo_dest">

                 {{ if firesale_discount_codes:code_applied }}

                  <a href="{{ firesale:url route='discounts' }}firesale_discount_codes/remove_discount" class="large-3 medium-3 small-12 columns btn_border_blan" id="remove-code">

                    X

                    Remover Código de Descuento

                  </a>

                  {{ else }}

                <form action="{{ firesale:url route='discounts' }}firesale_discount_codes/apply_discount" method="post" class="large-7 medium-7 small-12 columns elem_negros">

                   <label class="large-3 medium-3 small-12 columns">Código de descuento</label>

                   <div class="large-5 medium-5 small-12 columns relative">

                      <input name="code" id="appendedInputButton" type="text"  placeholder="Ingresar código" >

                   </div>                   

                   <div class="large-3 medium-3 small-12 columns">

                        <input type='submit' value="APLICAR" class="btn_border_blan" />

                   </div>

                </form>

                {{ endif }}

                <div class="bg_btn_deseos large-5 medium-5 small-12 columns">

                    <div class="large-6 medium-6 small-12 columns modal_all">

                      <a href="#wishlistModal" data-effect="mfp-move-horizontal" class="btn_border_blan">AÑADIR A MI LISTA DE DESEOS</a>               

                    </div>

                    <div class="large-6 medium-6 small-12 columns">

                      <a href="{{ url:site}}store" class="btn_blanco">SEGUIR COMPRANDO</a>                       

                    </div>

                </div>

            </article>

            <!--Fin Codigos Descuentos-->  

               <div class="clear"></div>

          </section>

          <!--Fin Contenidos Sitio-->      

      </section>

      <!--Fin bg Carrito-->      

	<div class="wrap">
	{{ if lists.entries }}
		<ul>
		{{ lists.entries }}
			<li><a href="{{ firesale:url route='wishlist' }}/view/{{ id }}">{{ title }} - {{ helper:number_format value=total }} Item{{ if total != 1 }}s{{ endif }} - worth {{ price }}</a></li>
		{{ /lists.entries }}
		</ul>
	{{ else }}
		<div class="alert yellow"><?php echo sprintf(lang('firesale:wish:none'), "{{ firesale:url route='wishlist' }}/create"); ?></div>
	{{ endif }}
	</div>

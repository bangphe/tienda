<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Plugin_Firesale_More_Products extends Plugin {

    private $product;
    private $stream;
    private $count = 0;

    public function __construct() {

        // Load libraries
        $this->lang->load('firesale_more_products/firesale');
        $this->load->model('firesale/categories_m');
        $this->load->model('firesale/products_m');
        $this->load->model('more_products_m');
    }

    public function recently_viewed() {

        $current_product = $this->attribute('id', null);

        // Get products
        $products = $this->more_products_m->get_current();
		
        // Se organiza de forma random 4 vistas recientes
        //$products = array_rand($products, 4);
        // cortamos el array hasta 4, ya que array_rand devuelve valores erroneos
        if(count($products) > 5)
		{
			shuffle($products);
			$products = array_slice($products, 0, 5);
		}
		
        // Check for results
        if ($products) {

            // Loop and get info
            foreach ($products AS $key => $product) {
                $products[$key] = $this->pyrocache->model('products_m', 'get_product', array($product), $this->firesale->cache_time);
                // unset current product if passed in
                if (!empty($current_product) && $products[$key]['id'] == $current_product) {
                    unset($products[$key]);
                }
            }
			
            // Return
            return array(array('products' => $products));
        }

        // Nothing?
        return array('products' => FALSE);
    }

    public function also_bought($product = NULL, $limit = 10) {
        if ($this->attributes()) {
            // Variables
            $product = $this->attribute('product');
            $limit = $this->attribute('limit', 10);
        }

        if (isset($product)) {
            // Get products
            $products = $this->pyrocache->model('more_products_m', 'also_bought', array($product, $limit), $this->firesale->cache_time);
            // Check for results
            if ($products) {

                // Loop and get info
                foreach ($products AS $key => $prod) {
                    $product = $this->pyrocache->model('products_m', 'get_product', array($prod['product_id']), $this->firesale->cache_time);
                    $products[$key] = array_merge($prod, $product);
                }

                // Return
                return array(array('products' => $products));
            }
        }


        // Nothing?
        return array('products' => FALSE);
    }

    public function related_products($product = NULL, $limit = 10, $use_cat = NULL) {
        if ($this->attributes()) {
            // Variables
            $product = $this->attribute('product');
            $limit = $this->attribute('limit', 10);
            $use_cat = (bool) $this->attribute('use_cat', false);
        }

        if (isset($product)) {
            // Get product
            $product = (array) $this->pyrocache->model('products_m', 'get_product', array($product), $this->firesale->cache_time);


            // Check product
            if ($product) {
                $products = $this->pyrocache->model('more_products_m', 'related_products', array($product, $use_cat, $limit), $this->firesale->cache_time);

                if (!empty($products)) {
                    return array(array('products' => $products));
                }
            }
        }

        // Nothing?
        return array('products' => FALSE);
    }

    public function also_bought_cart($limit = 10) {
        //use this when you wish to return products that have also been bought with items in users cart

        if ($this->attributes()) {
            // Variables
            $limit = $this->attribute('limit', 10);
        }

        $products = array();
        $tmp = array();
        foreach ($this->fs_cart->contents() as $item) {
            $product = $this->also_bought($item['id']);
            //for each of the also bought products returns
            foreach ($product as $prod) {
                //is the also bought product not in the temp array?
                if (!in_array($prod['id'], $tmp)) {
                    //if prod id and item id don't match
                    if ($prod['id'] != $item['id']) {
                        //add to tmp and the array
                        $tmp[] = $prod['id'];
                        $products[] = $prod;
                    }
                }
            }
        }

        if ($products) {
            shuffle($products);
            $products = array_slice($products, 0, $limit);
            return array(array('products' => $products));
        }

        return array('products' => FALSE);
    }

}

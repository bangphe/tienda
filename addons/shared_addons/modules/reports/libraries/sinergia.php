<?php
/**
* 
*/
class sinergia
{
	// guardamos en variables los mensajes de codeigniter para poder personalizarlos
	var $required_message = "required %s";
	var $numeric_message = "numeric %s";
	var $valid_email_message = "valid_email %s";
	var $matches_message = "matches %s";
	var $min_length_message = "min_length %s(%s)";
	var $max_length_message = "max_length %s(%s)";
	
	var $rutaProyecto = '';
	
	
	
	function __construct()
	{
		$this->CI = & get_instance();  // creamos una instancia para llamar los datos del codeigniter
		
		$this->rutaProyecto = dirname(dirname(__DIR__));
	}
	
	function cambioTamaPantalla($anchoFunc, $altoFunc)
	{
		$datosArray = array();
		
		// determinamos la cantidad de filas que se van a mostrar por cada tabla
		$numFilas = 10;  // variable para calcular el numero de paginas por tablas, si no se consigue la resolucion de la pantalla por defecto son 10
		$numLinks = 2;  // variable para calcular el numero de links en la paginación, si no se consigue la resolucion de la pantalla por defecto son 2
		$anchoPantalla = $anchoFunc;
		$altoPantalla = $altoFunc;
		// a la altura total disponible debemos restarle la altura usada por los demás elementos (header, menu, footer) teniendo en cuenta
		// que para resoluciones mayores a 768px el menú se esconde
		$alturaUsada = 460;
		// tenemos en cuenta que si la resolución es inferior a 640px, por CSS los botones se esconden y se coloca una barra de desplazamiento para habilitarlos
		if ($anchoPantalla <= 640)
		{
			$alturaUsada -= 58;
		}
		$numFilas = (($altoPantalla - $alturaUsada) / 25);  // la altura de cada elemento es 25px
		$numFilas = floor($numFilas);  // redondeamos hacia abajo para que nos quede un entero
		$numFilas = ($numFilas < 5 ? 5 : $numFilas);  // si da menos de 5 registros por tabla, lo dejamos en 5
		
		// determinamos el numero de links en la paginación que se van a mostrar
		$anchoBoton = 50;  // dado que el ancho de los botones es variable de acuerdo a la cantidad de digitos que tengan, tomamos el ancho con 5 digitos (hasta 99999 páginas)
		$numLinks = floor(($anchoPantalla - (38*2) - ($anchoBoton*5)) / $anchoBoton / 2);
		$numLinks = ($numLinks < 1 ? 1 : $numLinks);  // si da menos de 2 paginas, lo dejamos en 2
		$datosArray['numFilas'] = $numFilas;
		$datosArray['numLinks'] = $numLinks;
		return $datosArray;
	}
	
	// pasar un query sql de codeigniter a excel
	function to_excel($datosSql, $filename='exceloutput')
	{
	     $encabezadoExcel = '';  // just creating the var for field headers to append to below
	     $datosExcel = '';  // just creating the var for field data to append to below
	     
	     $obj = & get_instance();
	     
	     if ($datosSql->num_rows() == 0)
	     {
	          echo '<p>La tabla no tiene datos.</p>';
	     }
	     else 
	     {
	     	foreach($datosSql->result_array() as $row => $value)
		 	{
				$encabezadoExcel = '';
		 		//echo $row.'----'.$value.'<br/>';
				foreach($value as $row2 => $value2)
				{
					//echo $row2.'----'.$value2.'<br/>';
					$encabezadoExcel .= $row2."\t";
				}
		 	}
	     
          	foreach ($datosSql->result() as $row)
          	{
            	$lineaExcel = '';
               	foreach($row as $value)
               	{                                            
                	if ((!isset($value)) OR ($value == ""))
                	{
                    	$value = "\t";
                    }
					else
					{
                    	$value = str_replace('"', '""', $value);
                        $value = '"' . $value . '"' . "\t";
                    }
                    $lineaExcel .= $value;
               }
               $datosExcel .= trim($lineaExcel)."\n";
          	}
	          
	     	$datosExcel = str_replace("\r","",$datosExcel);
		  	
          	// pasar los datos de utf-8 a ansi
          	$datosExcel = iconv('UTF-8', 'ISO-8859-1', $datosExcel);
          	$encabezadoExcel = iconv('UTF-8', 'ISO-8859-1', $encabezadoExcel);
          	
          	//header("Content-type: application/x-msdownload; charset=utf-8"");
          	//header("Content-type: application/vnd.ms-excel; charset=utf-8");  //for Excel 5/2003 formats
          	header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");  // for Excel 2007
          	//header('Content-type: text/csv; charset=ISO-8859-1');
          	header("Content-Disposition: attachment; filename=$filename.xls");
          	echo "$encabezadoExcel\n$datosExcel";
	     }
	
	}
	
	function totalDatosTabla($DB_tabla, $buscar_BD = null, $campoBuscar = null)
	{
		if($buscar_BD != null && $campoBuscar != null)
		{
			$this->CI->db->where($campoBuscar, $buscar_BD);
		}
		$datosSql = $this->CI->db->get($DB_tabla);  // Hacemos una consulta a la BD
		return  $datosSql->num_rows();  // Tomamos cuantos filas existen en la consulta y la retormamos
	}
	
	// funcion para guardar los cambios que se hacen en los modulos en la tabla de cambios
	function guardarCambios($idRegistro, $codRegCam, $nomRegCam, $cambiosRegCam, $DB_tabla)
	{
		if(!empty($cambiosRegCam))
		{
			// Guardamos en la BD cambios lo siguiente
			$this->CI->db->set('fecharegca', 'NOW()', FALSE);
			$this->CI->db->insert('ts_regcambios_gen', array(
				'id' => $idRegistro,
				'idusuar' => $this->CI->session->userdata('iduserlog'),
				'tabla' => $DB_tabla,
				'cod' => $codRegCam,
				'nom' => $nomRegCam,
				'tipocambio' => 2
			));
			
			$idRegistro = $this->CI->db->insert_id();  // guardamos el id que acabamos de guardar
			
			$consultaSql = $this->crearStringInsertCambios($idRegistro, $cambiosRegCam);
			$this->CI->db->query($consultaSql);
		}
	}
	
	function crearStringInsertCambios($idRegistro, $cambiosRegCam)
	{
		$consultaSql = "INSERT INTO ts_regcambios_det VALUES ";
		$j = 0;
		$total = count($cambiosRegCam) + 1;
		if(!empty($cambiosRegCam))
		{
			foreach($cambiosRegCam AS $i => $datos)
			{
				if($j != 0 && $j != $total)
				{
					$consultaSql .= " , ";
				}
				$consultaSql .= "(null, ".$idRegistro." , '".$datos['nombre']."', '".$datos['anterior']."', '".$datos['nuevo']."')";
				$j++;
			}
			return $consultaSql;
		}
		return '';
	}
	
	// Funcion para comparar cambios que se hacen en la edicion de cualquier modulo, para guardar en la BD de cambios
	function cambiosEditor()
	{
		$datosArray = array();  // inicializamos un array donde vamos a mandar los datos a editar a la base de datos
		$datosArray['cambios'] = "";
		// el array $_POST queda en memoria y lo podemos usar asi no lo mandemos por la funcion
		if(isset($_POST['enviar']))
		{
			unset($_POST['enviar']);  // tenemos que borrar el dato del boton submit por que sale en nuestro post
		}
		if(isset($_POST['reclave']))
		{
			unset($_POST['reclave']);
		}
		
		$i = 0;  // iniciamos un contador para imprimir lo que separa los valores
		//var_dump($_POST);

		foreach($_POST as $key => $value)
		{
			if($value !== $this->CI->session->userdata($key) || $key == 'clave')
			{
				if($key == 'clave')
				{
					$value = $this->encriptarTexto($value);
				}
				// debemos usar el operador !==, pues si la cadena se encuentra en la posición 0,
				// en PHP la declaración (0 != false) se evalúa a false
				if(strpos($key,'fecha') !== false)
				{
					$value = $this->formatoFechaSQL($value);
					$_POST[$key] = $value;
				}
				$datosArray[$key] = $value;  // guardamos el nombre de la tabla de la bd y el valor
				$cambiosRegCam[$i]['nombre'] = $key;
				$cambiosRegCam[$i]['anterior'] = $this->CI->session->userdata($key);
				$cambiosRegCam[$i]['nuevo'] = $value;
				$datosArray['cambios'] = $cambiosRegCam;
				$i++;
			}
			$this->CI->session->unset_userdata($key);  // con esto borramos todas las sesiones para no guardar nada en memoria
		}
		return $datosArray;
	}
	
	// función para convertir segundoa a un string tipo 0h 0m
	function convertir_seg_a_hm($totalSegundos)
	{
		$totalHoras = floor($totalSegundos/3600);
		$totalMinutos = floor(($totalSegundos/60)%60);
		$hm = $totalHoras."h ".$totalMinutos."m";
		return $hm;
	}
	
    function strtoupperCorregido($textoMinuscula)
    {
        return strtoupper(strtr($textoMinuscula, 'áéíóúñ','ÁÉÍÓÚÑ' ));
    }
    
	// -------------------------------------------------------------
	// funcion encriptar en AES
	function addpadding($cadenaEncrip, $tamañoBloque = 32)
	{
		$longEncrip = strlen($cadenaEncrip);
		$padEncrip = $tamañoBloque - ($longEncrip % $tamañoBloque);
		$cadenaEncrip .= str_repeat(chr($padEncrip), $padEncrip);
		return $cadenaEncrip;
	}

	function strippadding($cadenaEncrip)
	{
		$slastEncrip = ord(substr($cadenaEncrip, -1));
		$slastcEncrip = chr($slastEncrip);
		$pcheckEncrip = substr($cadenaEncrip, -$slastEncrip);
		if(preg_match("/$slastcEncrip{".$slastEncrip."}/", $cadenaEncrip))
		{
			$cadenaEncrip = substr($cadenaEncrip, 0, strlen($cadenaEncrip)-$slastEncrip);
			return $cadenaEncrip;
		}
		else
		{
			return false;
		}
	}

	function encriptarTexto($cadenaEncrip = "")
	{
		$llaveEncrip = base64_decode("fjEdnnLggmbjIwwngYl8eQrXoAiIsabellajZjneNba=");
		$ivEncrip = base64_decode("w9ry6SlcxGJzhl4Ym3532JFbeRgm20121222ty9weAi=");

		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $llaveEncrip, $this->addpadding($cadenaEncrip), MCRYPT_MODE_CBC, $ivEncrip));
	}

	function desencriptarTexto($cadenaEncrip = "")
	{
		$llaveEncrip = base64_decode("fjEdnnLggmbjIwwngYl8eQrXoAiIsabellajZjneNba=");
		$ivEncrip = base64_decode("w9ry6SlcxGJzhl4Ym3532JFbeRgm20121222ty9weAi=");

		$cadenaEncrip = base64_decode($cadenaEncrip);

		return $this->strippadding(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $llaveEncrip, $cadenaEncrip, MCRYPT_MODE_CBC, $ivEncrip));
	}
	// Termina funcion encriptar en AES
	// ------------------------------------------------------------------
	
	// mandamos un array en lo posible si es un $_POST y le quitamos el boton del submit y y la reclave si es un usuario a registrarse
	function postArray()
	{
		$datosArray = array();  // creamos un array para guardar los datos
		// si contiene el dato del boton submit lo borramos del array
		// el array $_POST queda en memoria y lo podemos usar asi no lo mandemos por la funcion
		if(isset($_POST['enviar']))
		{
			unset($_POST['enviar']);
		}
		// si contiene la reclave, la borramos (no la vamos a guardar en la BD)
		if(isset($_POST['reclave']))
		{
			unset($_POST['reclave']);
		}
		// si contiene la clave anterior, la borramos (no la vamos a guardar en la BD)
		if(isset($_POST['claveAnt']))
		{
			unset($_POST['claveAnt']);
		}
		// recorremos el dato para guardarlos en el array
		foreach($_POST as $key => $value)
		{
			if($key == 'clave')
			{
				$datosArray[$key] = $this->encriptarTexto($value);
			}
			// debemos usar el operador !==, pues si la cadena se encuentra en la posición 0,
			// en PHP la declaración (0 != false) se evalúa a false
			else if(strpos($key,'fecha') !== false)
			{
				$datosArray[$key] = $this->formatoFechaSQL($value);
			}
			else
			{
				$datosArray[$key] = $value;
			}
			
		}
		return $datosArray;
	}
	
	function sqlInsertarRegCambios($datosASql, $DB_tabla, $codRegCam, $nomRegCam, $conRegCambios = true)
	{
		$this->CI->db->insert($DB_tabla, $datosASql);//es como tener INSERT INTO $nameTable (name, lastName, twitter) values ('$name'....);
		//echo $this->CI->db->last_query();  // para imprimir el ultimo query
		$BD_id_insert = $this->CI->db->insert_id();  // guardamos el id que acabamos de guardar
		
		if($BD_id_insert > 0 && $conRegCambios)
		{
			$estaLogeado = $this->CI->session->userdata('iduserlog');
			// guardamos en la tabla de registros cambiados los siguientes datos
			$regCambiados = array(
				'idusuar' => (!empty($estaLogeado) ? $this->CI->session->userdata('iduserlog') : '1'),
				'tabla' => $DB_tabla,
				'id' => $BD_id_insert,
				'cod' => $codRegCam,
				'nom' => $nomRegCam,
				'tipocambio' => 1
			);
			
			$this->CI->db->set('fecharegca', 'NOW()', FALSE);
			$this->CI->db->insert('ts_regcambios_gen', $regCambiados);
		}
		return $BD_id_insert;  // la funcion $this->db->insert_id() trae el id autonumerico con el cual se guardo el registro en la bsae de datos
	}
	
	// funcion que recibe un array y el nombre de la tabla para guardarlo en la BD el $origen es el modulo
	function sqlInsertar($datosASql, $DB_tabla, $controlador = null)
	{
		$this->CI->db->insert($DB_tabla, $datosASql);//es como tener INSERT INTO $nameTable (name, lastName, twitter) values ('$name'....);
		$BD_id_insert = $this->CI->db->insert_id();  // guardamos el id que acabamos de guardar
		
		return $BD_id_insert;  // la funcion $this->db->insert_id() trae el id autonumerico con el cual se guardo el registro en la bsae de datos
	}
	
	// sql para llenar el select $tablaBD es el nombre de la tabla, $codTabla es el nombre del campo del codigo o id, $nomTabla es el nombre de la tabla de los datos que vamos a mostrar
	function sqlFormSelect($DB_tabla, $BD_codCampo, $BD_campo, $nomForanea = null, $idForanea = null)
	{
		if($nomForanea != null && $idForanea != null)
		{
			$this->CI->db->where($nomForanea, $idForanea);
		}
		$datosSql = $this->CI->db->get($DB_tabla);
		if($datosSql->num_rows() > 0)
		{
			$datosArray = array();
			foreach ($datosSql->result_array() as $row)
			{
				$datosArray[$row[$BD_codCampo]] = $row[$BD_campo];
			}
			return $datosArray;
		}
		else
		{
			return FALSE;
		}
	}
	
	// para llenar los sql form con un query
	function sqlFormSelectQuery($consultaSql, $BD_codCampo, $BD_campo, $blanco = null)
	{
		$datosSql = $this->CI->db->query($consultaSql);
		$datosArray = array();
		if($blanco != null)
		{
			$datosArray = array('' => '');
		}
		if($datosSql != '')
		{
			if($datosSql->num_rows() > 0)
			{
				$datosArray = array();
				foreach ($datosSql->result_array() as $row)
				{
					$datosArray[$row[$BD_codCampo]] = $row[$BD_campo];
				}
				return $datosArray;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
		
	}
	
	function guardarIgualando($idRegistro, $datosASql, $DB_tabla, $BD_campo)
	{
		$this->CI->db->where($BD_campo, $idRegistro);  // esta sentencia es como si pusieramos
		$this->CI->db->update($DB_tabla, $datosASql);  // UPDATE users SET sitioWeb = $sitioWeb WHERE id = $id
		return TRUE;
	}
	
	function sqlQuery($consultaSql)
	{
		$datosArray = array();  // creamos un array donde vamos a guardar todos los resultados de la consulta a la BD
		$datosSql = $this->CI->db->query($consultaSql);  // si queremos mandar un query a la base de datos
		if ($datosSql->num_rows() > 0)
		{
			foreach ($datosSql->result() as $row => $value)
			{
				foreach ($value as $datos => $value2)
				{
					$datosArray[$row][$datos] = $value2;
				}
			}
			return $datosArray;
		}
	}
	
	function deleteUsuId($DB_tabla, $idRegistro, $BD_campo, $codRegCam, $nomRegCam)
	{
		$this->CI->db->where($BD_campo, $idRegistro);  // esta sentencia es como si pusieramos
		$this->CI->db->delete($DB_tabla);  // DELETE FROM users WHERE id = $id
		
		$regEliminado = array(
			'idusuar' => $this->CI->session->userdata('iduserlog'),
			'tabla' => $DB_tabla,
			'id' => $idRegistro,
			'cod' => $codRegCam,
			'nom' => $nomRegCam,
			'tipocambio' => 3
		);
		
		if($this->CI->db->_error_message())
		{
			return FALSE;
		}
		else 
		{
			$this->CI->db->set('fecharegca', 'NOW()', FALSE);
			$this->CI->db->insert('ts_regcambios_gen', $regEliminado);
			//echo $this->db->affected_rows();  // Nose que hace TAREA A INVESTIGAR igual no se necesita utilizar para esta funcion
			return TRUE;
		}
		
	}
	
	function sincronizarContactosGoogle($datosASql, $DB_tabla, $BD_campo, $nomRegCam)
	{
		if (!$this->CI->db->select($BD_campo)->from($DB_tabla)->where($BD_campo, $datosASql[$BD_campo])->count_all_results())
		{
			$this->CI->db->insert($DB_tabla, $datosASql);//es como tener INSERT INTO $nameTable (name, lastName, twitter) values ('$name'....);
			$id_insert = $this->CI->db->insert_id();  // guardamos el id que acabamos de guardar
			
			// guardamos en la tabla de registros cambiados los siguientes datos
			$regCambiados = array(
				'idusuar' => $this->CI->session->userdata('iduserlog'),
				'tabla' => $DB_tabla,
				'id' => $id_insert,
				'tipocambio' => 1,
				'nom' => $nomRegCam,
				'cambio' => 'Sincronizacion Google contacts'
			);
			
			$this->CI->db->set('fecharegca', 'NOW()', FALSE);
			$this->CI->db->insert('tm_regcambios', $regCambiados);
			
			return $id_insert;  // la funcion $this->db->insert_id() trae el id autonumerico con el cual se guardo el registro en la bsae de datos
		}
		else {
			return false;
		}
	}
	
	function totalCustomersBusqueda($buscar_BD, $campoBuscar, $DB_tabla, $prom)
	{
		if($prom == true)
		{
			$this->CI->db->where('prom_prod !=',0);
		}
		$this->CI->db->like($campoBuscar, $buscar_BD);  // buscaremos donde en id sea igual o parecido
		$datosSql = $this->CI->db->get($DB_tabla);  // Hacemos una consulta a la BD
		return  $datosSql->num_rows();  // Tomamos cuantos filas existen en la consulta y la retormamos
	}
	
	function totalCustomersQuery($consultaSql, $count = false)
	{
		if($count)
		{
			$datosSql = $this->CI->db->query($consultaSql);
			$numRegistros = 0;
			if($datosSql->num_rows() > 0)
			{
				$numRegistros = $datosSql->row()->numreg;
			}
			return $numRegistros;
		}
		else
		{
			$datosSql = $this->CI->db->query($consultaSql);
			return  $datosSql->num_rows();  // Devolvemos la cantidad de datos del query
		}
		
	}
	
	function ordenarDatosBuscador($datosArray)
	{
		$datosArray2 = array();
		foreach ($datosArray as $row => $value)
		{
			$datosArray2[$value[1]] = $value[0];
		}
		return $datosArray2;
	}
	
	function paginationCodeigniter()
	{
		$configPagination = array();
		$configPagination['per_page'] = $this->CI->session->userdata('filasPorPagina');  // numero de items que se mostraran por pagina
		$configPagination['num_links'] = $this->CI->session->userdata('numLinksTablas');  // numero de links mostrados en la paginación
		$configPagination['full_tag_open'] = '<ul>';
		$configPagination['full_tag_close'] = '</ul>';
		$configPagination['first_tag_open'] = '<li>';
		$configPagination['first_tag_close'] = '</li>';
		$configPagination['last_tag_open'] = '<li>';
		$configPagination['last_tag_close'] = '</li>';
		$configPagination['next_tag_open'] = '<li>';
		$configPagination['next_tag_close'] = '</li>';
		$configPagination['prev_tag_open'] = '<li>';
		$configPagination['prev_tag_close'] = '</li>';
		$configPagination['num_tag_open'] = '<li>';
		$configPagination['num_tag_close'] = '</li>';
		$configPagination['cur_tag_open'] = '<li class="active"><span>';
		$configPagination['cur_tag_close'] = '</span></li>';
		$configPagination['first_link'] = 'Primero';  // le cambiamos el nombre de la paginacion a la opcion ir a la primera pagina
		$configPagination['last_link'] = 'Último';  // le cambiamos el nombre de la paginacion a la opcion ir a la ultima pagina
		$configPagination['next_link'] = '<i class="icon-chevron-right"></i>';
		$configPagination['prev_link'] = '<i class="icon-chevron-left"></i>';
		
		return $configPagination;
	}
	
	public function mensajeCodeignierValidacionPersonalizado($cadenaExpl)
	{
		$msgCodig = '';
		$datosArray2 = array();
		$datosArray = explode("</p>", $cadenaExpl);
		foreach ($datosArray as $value)
		{
			if(strpos($value, 'required') !== false)
			{
				if(!isset($newArray['required']))
				{
					$datosArray2['required'] = 'Por favor digite un valor para los siguientes campos: ';
				}
				$valor = explode("<p>required ", $value);
				$datosArray2['required'] .= $valor[1].", ";
			}
			if(strpos($value, 'numeric') !== false)
			{
				if(!isset($newArray['numeric']))
				{
					$datosArray2['numeric'] = 'Los siguientes campos deben ser numéricos: ';
				}
				$valor = explode("<p>numeric ", $value);
				$datosArray2['numeric'] .= "(".$valor[1]."), ";
			}
			if(strpos($value, 'valid_email') !== false)
			{
				if(!isset($newArray['valid_email']))
				{
					$datosArray2['valid_email'] = 'Los siguientes campos deben contener un correo válido: ';
				}
				$valor = explode("<p>valid_email ", $value);
				$datosArray2['valid_email'] .= "(".$valor[1]."), ";
			}
			if(strpos($value, 'matches') !== false)
			{
				if(!isset($newArray['matches']))
				{
					$datosArray2['matches'] = 'Los siguientes campos deben coincidir: ';
				}
				$valor = explode("<p>matches ", $value);
				$datosArray2['matches'] .= "(".$valor[1]."), ";
			}
			if(strpos($value, 'min_length') !== false)
			{
				if(!isset($newArray['min_length']))
				{
					$datosArray2['min_length'] = 'Los siguientes campos deben tener un mínimo de caracteres: ';
				}
				$valor = explode("<p>min_length ", $value);
				$datosArray2['min_length'] .= "(".$valor[1]."), ";
			}
			if(strpos($value, 'max_length') !== false)
			{
				if(!isset($newArray['required']))
				{
					$datosArray2['required'] = 'Los siguientes campos deben tener un máximo de caracteres: ';
				}
				$valor = explode("<p>max_length ", $value);
				$datosArray2['max_length'] .= "".$valor[1].", ";
			}
		}
		foreach ($datosArray2 as $value)
		{
			// cambiamos la última coma por un punto
			$value = substr($value,0,strlen($value) - 2).'.';
			$msgCodig .= $value."</br></br>";
		}
		return $msgCodig;
	}
	
	function menuPrincipal()
	{
		$datosVista = array();
		$BD_buscarporquery = '';
		$BD_buscarporquery2 = '';
		if($this->CI->session->userdata('iduabogalog') != 0)
		{
			$BD_buscarporquery = " AND idaboga = ".$this->CI->session->userdata('iduabogalog');
			$BD_buscarporquery2 = " WHERE idaboga = ".$this->CI->session->userdata('iduabogalog');
		}
		else
		{
			$BD_buscarporquery = ' AND idempre = "'.$this->CI->session->userdata('idusuempre').'"';
		}
		//contamos cuantos registros de procesos hay para mandarlos a la vista
		$consultaSql = 'SELECT idproab FROM ab_procesos LEFT JOIN ab_abogados USING(idaboga) WHERE idempre="'.$this->CI->session->userdata('idusuempre').'"'.$BD_buscarporquery;
		$datosVista['ProcesosNot'] = $this->totalCustomersQuery($consultaSql);
		//contamos cuantos registros de clientes para mandarlos a la vista
		$consultaSql = 'SELECT idclien FROM tm_clientes cli LEFT JOIN ab_abogados USING(idaboga) WHERE cli.idestad = "1" '.$BD_buscarporquery;
		$datosVista['ClientesNot'] = $this->totalCustomersQuery($consultaSql);
		//contamos cuantos registros de contrapartes para mandarlos a la vista
		$consultaSql = 'SELECT idcptab FROM ab_contrapartes con LEFT JOIN ab_abogados USING(idaboga) WHERE con.idestad = "1" '.$BD_buscarporquery;
		$datosVista['ContrapartesNot'] = $this->totalCustomersQuery($consultaSql);
		// sacamos los permisos de la BD
		$PermisosUsuar = $this->CI->session->userdata('permisosUsuar');
		$PermisosUsuar = explode("&",$PermisosUsuar);
		$PermisosUsuar = $this->permisosUsuariosAbogados($PermisosUsuar);
		$datosVista['PermisosUsuar'] = $PermisosUsuar;
		return $datosVista;
	}
	
	public function permisosUsuariosAbogados($permisosArray)
	{
		$datosArray = array();
		$modulosArray = array('tmclie', 'tmusua', 'ababog', 'abcont', 'abproc', 'abdesj', 'abjuri', 'abagen');  // el orden esta en la BD
		$i = 0;
		
		foreach ($permisosArray as $row => $value)
		{
			$datosArray[$modulosArray[$i]] = $value;
			$i++;
		}
		return $datosArray;
	}
	
	public function permisoUsuarioModulo($codModulo)
	{
		// mandamos los permisos del usuario
		$PermisosUsuar = $this->CI->session->userdata('permisosUsuar');
		$PermisosUsuar = explode("&",$PermisosUsuar);
		$PermisosUsuar =  $this->permisosUsuariosAbogados($PermisosUsuar);
		if ($PermisosUsuar[$codModulo] != 2)  // si tiene el permiso del modulo lo deja ver (nota el id del modulo menos uno (-1) ya que es un array)
		{
			return FALSE;  // activa permisos de solo lectura
		}
		else 
		{
			return TRUE;
		}
	}
	
	function ultimoID($idRegistro, $DB_tabla)
	{
		$datosArray = $this->CI->db->query('SELECT MAX('.$idRegistro.') AS id FROM '.$DB_tabla);
		return $datosArray->row()->id;
	}
	
	public function crearCarpeta($nomCarpeta, $id_carpeta, $thumbCarpeta = false)
	{
		$rutaArchivo = $this->rutaProyecto.'/anexos/'.$nomCarpeta.'/'.$id_carpeta.'/';
		if (!file_exists($rutaArchivo)) 
		{
			mkdir($rutaArchivo,0777);
		}
		if($thumbCarpeta)
		{
			$rutaArchivo .= 'thumb/';
			if (!file_exists($rutaArchivo))
			{
				mkdir($rutaArchivo,0777);
			}
		}
	}
	
	public function crearCarpetaRuta($rutaCarpeta)
	{
		if (!file_exists($rutaCarpeta)) 
		{
			mkdir($rutaCarpeta,0777);
		}
	}
	
	public function filtrarSoloArchivosCarpeta($archivosArray)
	{
		$nombresExcluir = array('.', '..', 'thumb', 'thumbs.db');
		$numRegistros = count($archivosArray);
		for ($i = 0; $i < $numRegistros; $i++)
		{
			foreach ($nombresExcluir as $nombreExcluir)
			{
				if(strtolower($archivosArray[$i]) == $nombreExcluir)
				{
					unset($archivosArray[$i]);
					break;
				}
			}
		}
		return $archivosArray;
	}
	
	public function borrarCarpeta($nomCarpeta)
	{
		if (!file_exists($nomCarpeta))
		{
			return true;
		}
    	if (!is_dir($nomCarpeta))
		{
			return unlink($nomCarpeta);
		}
    	foreach (scandir($nomCarpeta) as $objExluy) 
    	{
	        if (!($objExluy == '.' || $objExluy == '..'))
			{
				$this->borrarCarpeta($carpeta.DIRECTORY_SEPARATOR.$objExluy);
			}
    	}
    	rmdir($nomCarpeta);
	}
	
	public function renombrarArchivo($nomCarpeta, $id_carpeta, $nombreArchivo, $nuevoNombreArchivo, $thumbCarpeta = false)
	{
		$extArchivo = $this->extensionArchivo($nombreArchivo);
		$rutaNombre = $this->rutaProyecto.'/anexos/'.$nomCarpeta.'/'.$id_carpeta.'/'.$nombreArchivo;
		$nuevaRutaNombre = $this->rutaProyecto.'/anexos/'.$nomCarpeta.'/'.$id_carpeta.'/'.$nuevoNombreArchivo.'.'.$extArchivo;
		rename ($rutaNombre, $nuevaRutaNombre);
		if($thumbCarpeta)
		{
			$rutaNombre = $this->rutaProyecto.'/anexos/'.$nomCarpeta.'/'.$id_carpeta.'/thumb/'.$nombreArchivo;
			$nuevaRutaNombre = $this->rutaProyecto.'/anexos/'.$nomCarpeta.'/'.$id_carpeta.'/thumb/'.$nuevoNombreArchivo.'.'.$extArchivo;
			rename ($rutaNombre, $nuevaRutaNombre);
		}
		return $nuevoNombreArchivo.'.'.$extArchivo;
	}
	
	public function extensionArchivo($nomArchivo)
	{
		$trozosNombre = explode(".", $nomArchivo); 
		$extArchivo = end($trozosNombre);
		$extArchivo = strtolower($extArchivo);
		return $extArchivo; 
	}
	
	public function columnaConDecimalesDB($datosArray, $nomColumna)
	{
		$es_decimal = FALSE;
		if($datosArray != '')
		{
			foreach ($datosArray->result() as $row)
			{
				$numDecimal = $row->{$nomColumna};  // ejemplo: $row->{$nomcol} es igual que decir $row->cantprod
				$numEntero = round($numDecimal);
				if($numDecimal != $numEntero)
				{
					$es_decimal = TRUE;
				}
			}
		}
		return $es_decimal;
	}
	
	// funcion para eliminar los espacios sobrantes de una cadena como espacios dobles ,tabulaciones (\t), retornos de carro (\r) y nuevas líneas (\n) y los sustituimos por un espacio en blanco
	public function quitarEspacios($cadenaTexto)
	{
		$cadenaTexto = trim($cadenaTexto);
        $cadenaTexto = preg_replace('/\s(?=\s)/', '', $cadenaTexto);
        $cadenaTexto = preg_replace('/[\n\r\t]/', ' ', $cadenaTexto);
        return $cadenaTexto;
	}
	
	public function quitarTildes($cadenaTexto)
	{
		$charAcentuados = array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$charNoAcentuados = array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
		$textoSinAcentos = str_replace($charAcentuados, $charNoAcentuados ,$cadenaTexto);
		return $textoSinAcentos;
	}
	
	public function formatoFechaSQL($cadenaTexto)
	{
		// partimos de la base de que las fechas han sido validadas previamente por javascript y por lo tanto se recibe
		// una cadena con el formato "dd-MM-yyyy"
		$cadenaTexto = explode(' ', $cadenaTexto);
		$cadenaTexto[0] = substr($cadenaTexto[0], 6).'-'.substr($cadenaTexto[0], 3, 2).'-'.substr($cadenaTexto[0], 0, 2);
		$cadenaCompleta = $cadenaTexto[0];
		if(isset($cadenaTexto[1]))
		{
			$cadenaCompleta = $cadenaTexto[0] .' '. $cadenaTexto[1];
		}
		return $cadenaCompleta;
	} 
	
	public function hackUsuId($idRegistro, $BD_campoId, $DB_tabla, $BD_foraneaId, $BD_campoForanea, $es_admin, $BD_tablaJoin, $BD_idJoin, $BD_idempre)
	{
		if($this->CI->session->userdata('iduserlog') == 1)
		{
			return TRUE;
		}
		if($es_admin)
		{
			$consultaSql = "SELECT COUNT(*) AS numreg FROM ".$DB_tabla." LEFT JOIN ".$BD_tablaJoin." USING(".$BD_idJoin.") WHERE ".$BD_campoId." = '".$idRegistro."' AND idempre = '".$BD_idempre."'";
		}
		else
		{
			$consultaSql = "SELECT COUNT(*) AS numreg FROM ".$DB_tabla." WHERE ".$BD_campoId." = '".$idRegistro."' AND ".$BD_campoForanea." = '".$BD_foraneaId."'";
		}
		$datosSql = $this->CI->db->query($consultaSql);
		$numRegistros = $datosSql->row()->numreg;
		if($numRegistros > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function mesIntToString($mesInt)
	{
		$datosArray = array( 1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril',
                 5 => 'Mayo', 6 => 'Junio',  7 => 'Julio', 8 => 'Agosto',
                 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre',
                 12 => 'Diciembre');
		return $datosArray[$mesInt];
	}
	
	public function generarStringAleatorio($length = 10)
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++)
	    {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    return $randomString;
	}
	
	
	public function dias_transcurridos($fecha_inicio, $fecha_final)
	{
		$diferenciaDias	= (strtotime($fecha_inicio)-strtotime($fecha_final))/86400;
		$diferenciaDias 	= abs($diferenciaDias);
		$diferenciaDias = floor($diferenciaDias);
		return $diferenciaDias;
	}
}

?>
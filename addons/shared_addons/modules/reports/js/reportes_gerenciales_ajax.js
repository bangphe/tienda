$(document).ready(function()
{
    var base_url = $('#baseurl').html();  // seleccionamos la base url de un div
    var pdfVista = $('#pdfVista').html();  // seleccionamos la base url de un div
    var esMovil = $('#esMovil').html();  // seleccionamos si es un dispositivo movil o no
    var esIOS = $('#esIOS').html();  // seleccionamos si es un dispositivo con sistema operativo IOS
    var invertirLados = ((esIOS == '1' && (window.orientation == 90 || window.orientation == 270)) ? true : false);
    var anchoPantTotalOri = (!invertirLados ? screen.width : screen.height);
    var altoPantTotalOri = (!invertirLados ? screen.height : screen.width);
    var anchoPantOri = window.innerWidth;
    var altoPantOri = window.innerHeight;
    var idResize;
    var widgetsArray = new Array();
    var datosLineasArray = new Array();
    var timerWidget = new Array();
    var imgTimerWidget = new Array();
    // creamos un conteo para crear el pdf del widget
    var conteoWidgetsPdf = 0;
    var conteoRecorridoWidgetsPdf = 0;
    var msgNodatos = "No hay datos";
    var fechaWidgetAnt = '';
    var idAntWidget = 0;
    
    organizarWidgets(anchoPantOri);
    
    $(window).resize(function()
	{
	    clearTimeout(idResize);
	    idResize = setTimeout(doneResizing, 500);  // 500 milisegundos
	});
	
	// mensaje en la creacion del pdf
	if(pdfVista != 0)
	{
		bootbox.alert('Creando el Pdf por favor espere. este proceso puede tardar un momento');
	}
	
	function doneResizing()
	{
		invertirLados = ((esIOS == '1' && (window.orientation == 90 || window.orientation == 270)) ? true : false);
		anchoPantTotal = (!invertirLados ? screen.width : screen.height);
		altoPantTotal = (!invertirLados ? screen.height : screen.width);
		anchoPantalla = window.innerWidth;
		altoPantalla = window.innerHeight;
		
		// si se trata de un dispositivo movil solo capturamos la rotación, pues cuando se abre el teclado, se cambia el area visible del navegador
		// Nota: debemos tener en cuenta que la función attr siempre devuelve strings
		esModal = ($("#sinergiaModal").attr("aria-hidden")== 'false' ? true : false);
		
		
		if (esMovil == 1)
		{
			//alert('Prueba');
			//alert(anchoPantTotalOri+' x '+altoPantTotalOri+' vs '+anchoPantTotal+' x '+altoPantTotal);  // en el iphone siempre están quedando repetidos
			// Nota: no podemos utilizar el evento onorientationchange pues no está disponible para los dispositivos iOS, por lo tanto determinamos si
			// hubo rotación validando si se invierten el ancho total y el alto total de la pantalla
			if(!(anchoPantTotal == altoPantTotalOri && altoPantTotal == anchoPantTotalOri))
			{
				anchoPantOri = anchoPantalla;
				altoPantOri = altoPantalla;
				anchoPantTotalOri = anchoPantTotal;
				altoPantTotalOri = altoPantTotal;
				return;
			}
			else
			{
				//alert(anchoPantTotalOri+' x '+altoPantTotalOri+' vs '+anchoPantTotal+' x '+altoPantTotal);
				//alert('rotaste la pantalla');
				//alert(window.innerHeight);  // el iphone 5 carga 300px en modo horiz y 504px en vertical
			}
		}
		
		//alert(anchoPantalla);
		organizarWidgets(anchoPantalla);
		
		anchoPantOri = anchoPantalla;
		altoPantOri = altoPantalla;
		anchoPantTotalOri = anchoPantTotal;
		altoPantTotalOri = altoPantTotal;
	}
	
	// menú contextual
	$.contextMenu
    ({
        selector: '.css-menu-opcioneswidget',
        trigger: 'left', 
        build: function($trigger, e) {
        	//console.log(e);
        	//console.log($trigger);
        	//console.log($trigger.parents().eq(1).find(".multiserie").html());
        	return {
		        callback: function(key, options) 
		        {
		            var m = "clicked: " + key;
		            switch(key)
		            {
						case "TablaWidget":
			                cambiarTipoGrafico($(this), 0);
			                break;
			            case "BarrasWidget":
			            	cambiarTipoGrafico($(this), 1);
			            	break;
			            case "MultiBarrasGroupedWidget":
			            	cambiarTipoGrafico($(this), 2);
			            	break;
			            case "MultiBarrasStackedWidget":
			                cambiarTipoGrafico($(this), 3);
			                break;
			            case "LineasWidget":
			                cambiarTipoGrafico($(this), 4);
			                break;
			            case "PieWidget":
			            	cambiarTipoGrafico($(this), 5);
			            	break;
			            case "redimTam_1":
			            	cambiarTamanoWidget($(this), 1);
			            	break;
			            case "redimTam_2":
			            	cambiarTamanoWidget($(this), 2);
			            	break;
			            case "redimTam_3":
			            	cambiarTamanoWidget($(this), 3);
			            	break;
			            case "redimTam_4":
			            	cambiarTamanoWidget($(this), 4);
			            	break;
			            case "redimTam_5":
			            	cambiarTamanoWidget($(this), 5);
			            	break;
			            case "redimTam_6":
			            	cambiarTamanoWidget($(this), 6);
			            	break;
			            case "redimTam_7":
			            	cambiarTamanoWidget($(this), 7);
			            	break;
			            case "redimTam_8":
			            	cambiarTamanoWidget($(this), 8);
			            	break;
			            case "redimTam_9":
			            	cambiarTamanoWidget($(this), 9);
			            	break;
			            case "redimTam_10":
			            	cambiarTamanoWidget($(this), 10);
			            	break;
			            case "divisorWidg_1":
			            	cambiarDivisorWidget($(this), 1);
			            	break;
			            case "divisorWidg_10":
			            	cambiarDivisorWidget($(this), 10);
			            	break;
			            case "divisorWidg_100":
			            	cambiarDivisorWidget($(this), 100);
			            	break;
			            case "divisorWidg_1000":
			            	cambiarDivisorWidget($(this), 1000);
			            	break;
			            case "divisorWidg_10000":
			            	cambiarDivisorWidget($(this), 10000);
			            	break;
			            case "divisorWidg_100000":
			            	cambiarDivisorWidget($(this), 100000);
			            	break;
			            case "divisorWidg_1000000":
			            	cambiarDivisorWidget($(this), 1000000);
			            	break;
			            case "EliminarWidget":
			            	eliminarWidget($(this));
			                break;
			            case "AgregarWidget":
			            	agregarWidget($(this));
			                break;
		            }
		        },
		        items:
		        {
		  			"TipoGraficaWidget": 
		            {
		                name: "Tipo de Gráfico", className: "cm-submenu", icon: "editar",
		                items: 
		                {
		                    "TablaWidget": {name: "Tabla", icon: "tabla"},
		                    "BarrasWidget": {name: "Barras", icon: "barras" + (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? "-des" : ""), disabled: (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? true : false)},
		                    "MultiBarrasGroupedWidget": {name: "Barras Agrupadas", icon: "barras" + (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? "" : "-des"), disabled: (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? false : true)},
		                    "MultiBarrasStackedWidget": {name: "Barras Apiladas", icon: "barras-apiladas"  + (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? "" : "-des"), disabled: (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? false : true)},
		                    "LineasWidget": {name: "Líneas", icon: "lineas"  + (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? "" : "-des"), disabled: (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? false : true)},
		                    "PieWidget": {name: "Torta", icon: "torta" + (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? "-des" : ""), disabled: (parseFloat($trigger.parents().eq(1).find(".multiserie").html()) == 1 ? true : false)},
		                }
		            },
		            "AnchoWidget": 
		            {
		                name: "Ancho del Widget", className: "cm-submenu", icon: "redimensionar",
		                items: 
		                {
		                    "redimTam_1": {name: "1x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 1 ? "ok" : "vacio")},
		                    "redimTam_2": {name: "2x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 2 ? "ok" : "vacio")},
		                    "redimTam_3": {name: "3x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 3 ? "ok" : "vacio")},
		                    "redimTam_4": {name: "4x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 4 ? "ok" : "vacio")},
		                    "redimTam_5": {name: "5x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 5 ? "ok" : "vacio")},
		                    "redimTam_6": {name: "6x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 6 ? "ok" : "vacio")},
		                    "redimTam_7": {name: "7x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 7 ? "ok" : "vacio")},
		                    "redimTam_8": {name: "8x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 8 ? "ok" : "vacio")},
		                    "redimTam_9": {name: "9x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 9 ? "ok" : "vacio")},
		                    "redimTam_10": {name: "10x", icon: (parseFloat($trigger.parents().eq(1).find(".undsancho").html()) == 10 ? "ok" : "vacio")},
		                }
		            },
		            "divisorWidget": 
		            {
		                name: "Divisor del Widget", className: "cm-submenu " + (parseFloat($trigger.parents().eq(1).find(".idwiusu").html()) != 0 ? "" : "cm-oculto"), icon: "redimensionar",
		                items: 
		                {
		                    "divisorWidg_1": {name: "/1", icon: (parseFloat($trigger.parents().eq(1).find(".divisorWidget").html()) == 1 ? "ok" : "vacio")},
		                    "divisorWidg_10": {name: "/10", icon: (parseFloat($trigger.parents().eq(1).find(".divisorWidget").html()) == 10 ? "ok" : "vacio")},
		                    "divisorWidg_100": {name: "/100", icon: (parseFloat($trigger.parents().eq(1).find(".divisorWidget").html()) == 100 ? "ok" : "vacio")},
		                    "divisorWidg_1000": {name: "/1000", icon: (parseFloat($trigger.parents().eq(1).find(".divisorWidget").html()) == 1000 ? "ok" : "vacio")},
		                    "divisorWidg_10000": {name: "/1000", icon: (parseFloat($trigger.parents().eq(1).find(".divisorWidget").html()) == 10000 ? "ok" : "vacio")},
		                    "divisorWidg_100000": {name: "/10000", icon: (parseFloat($trigger.parents().eq(1).find(".divisorWidget").html()) == 100000 ? "ok" : "vacio")},
		                    "divisorWidg_1000000": {name: "/1000000", icon: (parseFloat($trigger.parents().eq(1).find(".divisorWidget").html()) == 1000000 ? "ok" : "vacio")},
		                }
		            },
		            "sep1": "---------",
		            "EliminarWidget": {name: "Eliminar Widget", className: (parseFloat($trigger.parents().eq(1).find(".idwiusu").html()) != 0 ? "" : "cm-oculto"), icon: "eliminar", disabled: (parseFloat($trigger.parents().eq(1).find(".idwiusu").html()) != 0 ? false : true)},
		            "AgregarWidget": {name: "Agregar Widget", className: (parseFloat($trigger.parents().eq(1).find(".idwiusu").html()) != 0 ? "cm-oculto" : ""), icon: "agregar", disabled: (parseFloat($trigger.parents().eq(1).find(".idwiusu").html()) != 0 ? true : false)},
		        }
	        };
        }
    });
	
	
	$(document).on("submit", "#cambiarFechaSession", function(event)
	{
		event.preventDefault();
		$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			dataType: "json",
			data:  ({fechaGeneral: $("#fechaGeneralSession").val()}),
			beforeSend: function ()
			{
			    //$('#contenido').html('<img src="'+base_url+'uploads/default/spinner.gif" width="28" height="28"/>');
			},
			success: function(data)
			{
				if(data.status == 'error')
				{
					simple_notice(titulo="Error",mensaje="Error al cambiar la fecha. Por favor intentelo más tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
				}
				else
				{
					$(".periodoWidgetInput").val($("#fechaGeneralSession").val());
					refrescarTodosWidgets();
				}
			},
			error: function(err)
	        {
	        	simple_notice(titulo="Error",mensaje="Error al cambiar la fecha. Por favor intentelo más tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
	        }
		});
	});
	
	$(document).on("change", "#fechaWidget", function(event)
	{
		event.stopPropagation();
		event.preventDefault();
		widgetElement = $(this).parents().eq(4);
		idWidge = widgetElement.find(".idwidge").html();
		idWiusu = widgetElement.find(".idwiusu").html();
		
		if(fechaWidgetAnt != $(this).val() || idAntWidget != (parseFloat(idWiusu) != 0 ? idWiusu : idWidge))
		{
			fechaWidgetAnt = $(this).val();
			idAntWidget = (parseFloat(idWiusu) != 0 ? idWiusu : idWidge);
			posicion = widgetElement.find(".posicion").html();
			undsAncho = widgetElement.find(".undsancho").html();
			tipoGrafico = widgetElement.find(".tipografico").html();
			tipoTabla = widgetElement.find(".tipotabla").html();
			multiSerie = widgetElement.find(".multiserie").html();
			minRefresco = widgetElement.find(".minrefresco").html();
			nomWidge = widgetElement.find(".nomwidge").html();
			tituloejeX = widgetElement.find(".tituloejex").html();
			tituloejeY = widgetElement.find(".tituloejey").html();
			refrescoWidgetMin(widgetElement, idWidge, idWiusu, posicion, undsAncho, tipoGrafico, tipoTabla, minRefresco, nomWidge, tituloejeX, tituloejeY, multiSerie);
		}
	});
	
	function refrescarWidget(widgetElement, tipoGrafico, idRegistro, contenedor, tituloejeX, tituloejeY, classNameTabla)
	{
		switch(parseFloat(tipoGrafico))
		{
			case 0:
				refrescarTabla(widgetElement, contenedor, classNameTabla);
				break;
			case 1:
				graficaBarras(idRegistro, tituloejeX, tituloejeY, contenedor+" svg");
				break;
			case 2:
				graficaMultiBarChar(idRegistro, tituloejeX, tituloejeY, false, contenedor+" svg");
				break;
			case 3:
				graficaMultiBarChar(idRegistro, tituloejeX, tituloejeY, true, contenedor+" svg");
				break;
			case 4:
				graficaSimpleLineChar(idRegistro, tituloejeX, tituloejeY, contenedor+" svg");
				break;
			case 5:
				graficaPie(idRegistro, contenedor+" svg");
				break;
		}
		
	}
	
	function refrescarTabla(widgetElement, divTabla, classNameTabla)
	{
		idWidge = widgetElement.find(".idwidge").html();
		idWiusu = widgetElement.find(".idwiusu").html();
		tipoGrafico = widgetElement.find(".tipografico").html();
		tipoTabla = widgetElement.find(".tipotabla").html();
		tituloejeX = widgetElement.find(".tituloejex").html();
		tituloejeY = widgetElement.find(".tituloejey").html();
		multiSerie = widgetElement.find(".multiserie").html();
		nomWidge = widgetElement.find(".nomwidge").html();
		
		switch(tipoTabla)
		{
			case '1':
			case '2':
				if(multiSerie == 1)
				{
					tablaSimpleMultiSerie((parseFloat(idWiusu) != 0 ? idWiusu : idWidge), divTabla, tituloejeX, tituloejeY, tipoTabla, classNameTabla);
				}
				else
				{
					if(tipoTabla < 3)
					{
						tablaSimpleUnaSerie((parseFloat(idWiusu) != 0 ? idWiusu : idWidge), divTabla, tituloejeX, tituloejeY, tipoTabla, classNameTabla);
					}
				}
				break;
			case '3':
				if(multiSerie == 1)
				{
					tablaVariacionesMultiSerie((parseFloat(idWiusu) != 0 ? idWiusu : idWidge), divTabla, tituloejeX, tituloejeY, tipoTabla, classNameTabla);
				}
				break;
			case '4':
				if(multiSerie == 1)
				{
					tablaCumplimientoMultiSerie((parseFloat(idWiusu) != 0 ? idWiusu : idWidge), divTabla, tituloejeX, tituloejeY, tipoTabla, classNameTabla);
				}
				break;
		}
		tablaDinamica();
	}
	
	function tablaDinamica()
	{
		/*$('.tablaDinamica').dataTable(
		{
			"bPaginate": false,
	        "bLengthChange": false,
	        "bFilter": false,
	        "bSort": true,
	        "bInfo": false,
	        "bAutoWidth": false
		});*/
	}
	
	function cambiarTipoGrafico(element, tipoGraficoNuevo)
	{
		widgetElement = element.parents().eq(1);
		idWidge = widgetElement.find(".idwidge").html();
		idWiusu = widgetElement.find(".idwiusu").html();
		tituloejeX = widgetElement.find(".tituloejex").html();
		tituloejeY = widgetElement.find(".tituloejey").html();
		tipoGrafico = widgetElement.find(".tipografico").html();
		
		if(tipoGrafico == tipoGraficoNuevo)
		{
			simple_notice(titulo="Información",mensaje="El widget ya tiene este tipo gráfico.",tiempo=2000,estilo="info",clasepersonal="",icono="icon-info-sign");
			return;
		}
		//console.log(idWiusu + " ---- " + tipoGraficoNuevo);
		if(parseFloat(idWiusu) != 0)
		{
			$.ajax
			({
				url: base_url+'admin/reports/cambioTipoGraficoWidget',
				type: "POST",
				dataType: "json",
				data:  ({idWiusu: idWiusu, tipoGraficoNuevo: tipoGraficoNuevo}),
				beforeSend: function () 
				{
					widgetElement.find(".css-loaderWidget").html('<img src="'+base_url+'uploads/default/spinner.gif" width="32" height="32"/>');
					widgetElement.find(".controlesWidget").addClass("hide");
				},
				success: function(data)
				{
					widgetElement.find(".css-loaderWidget").html("");
					if(data.status == 'error')
					{
						simple_notice(titulo="Error",mensaje="Error al cambiar el tipo de grafico. Por favor intentelo más tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
					}
					else
					{
						if(widgetElement.find('.datosWidget'+idWiusu)[0])
						{
							$("#graficoContenedor"+idWiusu+" svg").empty();
							// como la tabla borra el svg lo volvemos a crear si cambiamos de tabla a otro grafico
							if(!$("#graficoContenedor"+idWiusu+" svg")[0] && tipoGrafico == 0)
							{
								$("#graficoContenedor"+idWiusu).empty();
								$("#graficoContenedor"+idWiusu).append("<svg></svg>").width(widgetElement.width() - 24);
							}
							refrescarWidget(widgetElement, tipoGraficoNuevo, idWiusu, "#graficoContenedor"+idWiusu, tituloejeX, tituloejeY, "");
							widgetElement.find(".controlesWidget").removeClass("hide");
							widgetElement.find(".tipografico").html(tipoGraficoNuevo);
						}
					}
				},
				error: function()
				{
		            simple_notice(titulo="Error",mensaje="Error al cambiar el tipo de grafico. Por favor intentelo más tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
		        }
			});
		}
		else
		{
			if(widgetElement.find('.datosWidget'+idWidge)[0])
			{
				$("#graficoContenedor"+idWidge+" svg").empty();
				// como la tabla borra el svg lo volvemos a crear si cambiamos de tabla a otro grafico
				if(!$("#graficoContenedor"+idWidge+" svg")[0] && tipoGrafico == 0)
				{
					$("#graficoContenedor"+idWidge).empty();
					$("#graficoContenedor"+idWidge).append("<svg></svg>").width(widgetElement.width() - 24);
				}
				refrescarWidget(widgetElement, tipoGraficoNuevo, idWidge, "#graficoContenedor"+idWidge, tituloejeX, tituloejeY, "");
				widgetElement.find(".controlesWidget").removeClass("hide");
				widgetElement.find(".tipografico").html(tipoGraficoNuevo);
			}
		}
	}
	
	function cambiarTamanoWidget(element, undsAnchoNuevo)
	{
		widgetElement = element.parents().eq(1);
		idWidge = widgetElement.find(".idwidge").html();
		idWiusu = widgetElement.find(".idwiusu").html();
		undsAncho = widgetElement.find(".undsancho").html();
		tipoGrafico = widgetElement.find(".tipografico").html();
		tituloejeX = widgetElement.find(".tituloejex").html();
		tituloejeY = widgetElement.find(".tituloejey").html();
		//console.log(undsAncho + " - ---- - " + idWiusu + " ------ " + undsAnchoNuevo);
		
		if(undsAnchoNuevo != undsAncho)
		{
			if(parseFloat(idWiusu) != 0)
			{
				$.ajax
				({
					url: base_url+'admin/reports/cambioTamanoWidget',
					type: "POST",
					dataType: "json",
					data:  ({idWiusu: idWiusu, undsAnchoNuevo: undsAnchoNuevo}),
					beforeSend: function () 
					{
						widgetElement.find(".css-loaderWidget").html('<img src="'+base_url+'uploads/default/spinner.gif" width="32" height="32"/>');
						widgetElement.find(".controlesWidget").addClass("hide");
					},
					success: function(data)
					{
						widgetElement.find(".css-loaderWidget").html("");
						if(data.status == 'error')
						{
							simple_notice(titulo="Error",mensaje="Error al cambiar el tamaño del Widget. Por favor intentelo más tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
						}
						else
						{
							widgetElement.find(".undsancho").html(undsAnchoNuevo);
							tamanoNuevo = (300*undsAnchoNuevo) + (18*(undsAnchoNuevo-1)) + (undsAnchoNuevo - (undsAnchoNuevo == 1 ? 1 : 2));
							widgetElement.width(tamanoNuevo);
							$("#graficoContenedor"+idWiusu+" svg").width(tamanoNuevo-24);
							$("#graficoContenedor"+idWiusu+" svg").empty();
							// la tabla se redimensiona sola, por eso no necesitamos refrescarla
							if(tipoGrafico != 0)
							{
								refrescarWidget(widgetElement, tipoGrafico, idWiusu, "#graficoContenedor"+idWiusu, tituloejeX, tituloejeY, "");
							}
							widgetElement.find(".controlesWidget").removeClass("hide");
							doneResizing();
						}
					},
					error: function()
					{
			            simple_notice(titulo="Error",mensaje="Error al cambiar el tamaño del Widget. Por favor intentelo más tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
			        }
				});
			}
			else
			{
				widgetElement.find(".undsancho").html(undsAnchoNuevo);
				tamanoNuevo = (300*undsAnchoNuevo) + (18*(undsAnchoNuevo-1)) + (undsAnchoNuevo - (undsAnchoNuevo == 1 ? 1 : 2));
				widgetElement.width(tamanoNuevo);
				$("#graficoContenedor"+idWidge+" svg").width(tamanoNuevo-24);
				$("#graficoContenedor"+idWidge+" svg").empty();
				// la tabla se redimensiona sola, por eso no necesitamos refrescarla
				if(tipoGrafico != 0)
				{
					refrescarWidget(widgetElement, tipoGrafico, idWidge, "#graficoContenedor"+idWidge, tituloejeX, tituloejeY, "");
				}
				widgetElement.find(".controlesWidget").removeClass("hide");
				doneResizing();
			}
		}
	}
	
	function cambiarDivisorWidget(element, divisorWidgetNuevo)
	{
		widgetElement = element.parents().eq(1);
		idWidge = widgetElement.find(".idwidge").html();
		idWiusu = widgetElement.find(".idwiusu").html();
		posicion = widgetElement.find(".posicion").html();
		undsAncho = widgetElement.find(".undsancho").html();
		tipoGrafico = widgetElement.find(".tipografico").html();
		tipoTabla = widgetElement.find(".tipotabla").html();
		multiSerie = widgetElement.find(".multiserie").html();
		minRefresco = widgetElement.find(".minrefresco").html();
		tituloejeX = widgetElement.find(".tituloejex").html();
		tituloejeY = widgetElement.find(".tituloejey").html();
		divisorWidget = widgetElement.find(".divisorWidget").html();
		//console.log(undsAncho + " - ---- - " + idWiusu + " ------ " + undsAnchoNuevo);
		
		if(divisorWidgetNuevo != divisorWidget)
		{
			if(parseFloat(idWiusu) != 0)
			{
				$.ajax
				({
					url: base_url+'admin/reports/cambioDivisorWidget',
					type: "POST",
					dataType: "json",
					data:  ({idWiusu: idWiusu, divisorWidget: divisorWidgetNuevo}),
					beforeSend: function () 
					{
						widgetElement.find(".css-loaderWidget").html('<img src="'+base_url+'uploads/default/spinner.gif" width="32" height="32"/>');
						widgetElement.find(".controlesWidget").addClass("hide");
					},
					success: function(data)
					{
						widgetElement.find(".css-loaderWidget").html("");
						if(data.status == 'error')
						{
							simple_notice(titulo="Error",mensaje="Error al cambiar el divisor del Widget. Por favor intentelo más tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
						}
						else
						{
							refrescoWidgetMin(widgetElement, idWidge, idWiusu, posicion, undsAncho, tipoGrafico, tipoTabla, minRefresco, nomWidge, tituloejeX, tituloejeY, multiSerie);
							widgetElement.find(".controlesWidget").removeClass("hide");
							widgetElement.find(".divisorWidget").html(divisorWidgetNuevo);
						}
					},
					error: function()
					{
			            simple_notice(titulo="Error",mensaje="Error al cambiar el divisor del Widget. Por favor intentelo más tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
			        }
				});
			}
		}
	}
	
	function agregarWidget(element)
	{
		widgetElement = element.parents().eq(1);
		idWidge = widgetElement.find(".idwidge").html();
		undsAncho = widgetElement.find(".undsancho").html();
		tipoGrafico = widgetElement.find(".tipografico").html();
		bootbox.confirm("¿Está seguro de que desea agregar el Widget a la vista?", function(result)
		{
			if(result)
			{
				$.ajax
				({
					url: base_url+'admin/reports/agregarWidget',
					type: "POST",
					dataType: "json",
					data:  ({idWidge: idWidge, undsAncho: undsAncho, tipoGrafico: tipoGrafico}),
					beforeSend: function () 
					{
						//widgetElement.find(".css-loaderWidget").html('<img src="'+base_url+'uploads/default/spinner.gif" width="32" height="32"/>');
					},
					success: function(data)
					{
						//widgetElement.find(".css-loaderWidget").html("");
						if(data.status == 'error')
						{
							simple_notice(titulo="Error",mensaje=data.msg,tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
						}
						else
						{
							simple_notice(titulo="Éxito",mensaje=data.msg,tiempo=2000,estilo="success",clasepersonal="",icono="icon-ok-sign");
						}
					},
					error: function()
					{
						simple_notice(titulo="Error",mensaje="Error al agregar el Widget. Por favor intentelo mas tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
			        }
				});
			}
		});
	}
	
	function eliminarWidget(element)
	{
		widgetElement = element.parents().eq(1);
		idWiusu = widgetElement.find(".idwiusu").html();
		bootbox.confirm("¿Está seguro de que desea eliminar el Widget de esta vista?", function(result)
		{
			if(result)
			{
				$.ajax
				({
					url: base_url+'admin/reports/eliminarWidget',
					type: "POST",
					dataType: "json",
					data:  ({idWiusu: idWiusu}),
					beforeSend: function () 
					{
						widgetElement.find(".css-loaderWidget").html('<img src="'+base_url+'uploads/default/spinner.gif" width="32" height="32"/>');
					},
					success: function(data)
					{
						widgetElement.find(".css-loaderWidget").html("");
						if(data.status == 'error')
						{
							simple_notice(titulo="Error",mensaje=data.msg,tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
						}
						else
						{
							widgetElement.fadeOut('slow', function()
						    {
								widgetElement.remove();
							});
							doneResizing();
						}
					},
					error: function()
					{
						simple_notice(titulo="Error",mensaje="Error al eliminar el Widget. Por favor intentelo mas tarde",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
			        }
				});
			}
		});
	}
	
	function organizarWidgets(anchoPantalla)
	{
		anchoWidgets = 300 + (9*2) + 2;
		totalWidgetsFila = Math.floor((anchoPantalla-25)/anchoWidgets);
		conteoFila = 0;
		maxWidgetFila = 0;
		cantWidgets = $(".css-widget").length - 1;
		$(".css-widget").each(function (contadorEach, element)
		{
			widgetElement = $(this);
			undsAncho = Math.floor(widgetElement.find(".undsancho").html());
			if((conteoFila + undsAncho) <= totalWidgetsFila)
			{
				conteoFila += undsAncho;
				if(cantWidgets == contadorEach)
				{
					maxWidgetFila = Math.max(maxWidgetFila, conteoFila);
				}
			}
			else
			{
				maxWidgetFila = Math.max(maxWidgetFila, conteoFila);
				conteoFila = undsAncho;
			}
		});
		anchoTotalWidgetsMaxFila = (300*maxWidgetFila) + (18*(maxWidgetFila-1)) + (maxWidgetFila - (maxWidgetFila == 1 ? 1 : 2));
		margen = (anchoPantalla - 10 - anchoTotalWidgetsMaxFila)/2 - 15;
		if(margen > 5)
		{
			$("#css-grupoWidgets").css("padding-left" , margen);
		}
		else
		{
			$("#css-grupoWidgets").css("padding-left" , 5);
		}
	}
    
    // creamos los widgets
    function refrescarTodosWidgets()
    {
	    $(".css-widget").each(function (contadorEach, element)
		{
			widgetElement = $(this);
			idWidge = widgetElement.find(".idwidge").html();
			idWiusu = widgetElement.find(".idwiusu").html();
			posicion = widgetElement.find(".posicion").html();
			undsAncho = widgetElement.find(".undsancho").html();
			tipoGrafico = widgetElement.find(".tipografico").html();
			tipoTabla = widgetElement.find(".tipotabla").html();
			multiSerie = widgetElement.find(".multiserie").html();
			minRefresco = widgetElement.find(".minrefresco").html();
			nomWidge = widgetElement.find(".nomwidge").html();
			tituloejeX = widgetElement.find(".tituloejex").html();
			tituloejeY = widgetElement.find(".tituloejey").html();
			refrescoWidgetMin(widgetElement, idWidge, idWiusu, posicion, undsAncho, tipoGrafico, tipoTabla, minRefresco, nomWidge, tituloejeX, tituloejeY, multiSerie);
			conteoWidgetsPdf++;
		});
	}
	
	refrescarTodosWidgets();
	
	function refrescoWidgetMin(widgetElement, idWidge, idWiusu, posicion, undsAncho, tipoGrafico, tipoTabla, minRefresco, nomWidge, tituloejeX, tituloejeY, multiSerie)
	{
		cargarWidgetsAjax(widgetElement, idWidge, idWiusu, posicion, undsAncho, tipoGrafico, tipoTabla, minRefresco, nomWidge, tituloejeX, tituloejeY, multiSerie);
		
		if(parseFloat(minRefresco) > 0 && parseFloat(pdfVista) == 0)
		{
			tiempoRefresco = 60000 * minRefresco;
			
			timerWidget[(parseFloat(idWiusu) != 0 ? idWiusu : idWidge)] = new timerRefrescoWidget(function()
			{
		    	console.log("refresco del widget: " + nomWidge);
				// cargamos los datos que se pueden cambiar de nuevo
				posicion = widgetElement.find(".posicion").html();
				undsAncho = widgetElement.find(".undsancho").html();
				tipoGrafico = widgetElement.find(".tipografico").html();
				tipoTabla = widgetElement.find(".tipotabla").html();
				refrescoWidgetMin(widgetElement, idWidge, idWiusu, posicion, undsAncho, tipoGrafico, tipoTabla, minRefresco, nomWidge, tituloejeX, tituloejeY, multiSerie);
				if(parseFloat($("#css-body-vistaAmpliada").find(".idWidget").html()) == (parseFloat(idWiusu) != 0 ? idWiusu : idWidge))
				{
					if(!$("#css-body-vistaAmpliada").hasClass('hide'))
				    {
				    	console.log("refresco vista ampliada del widget: " + nomWidge);
				    	vistaAmpliada(widgetElement, "#css-tabla-vistaAmpliada");
				    }
				}
			}, tiempoRefresco);
		}
	}
	
	function cargarWidgetsAjax(widgetElement, idWidge, idWiusu, posicion, undsAncho, tipoGrafico, tipoTabla, minRefresco, nomWidge, tituloejeX, tituloejeY, multiSerie)
	{
		$.ajax({
			url: base_url+'admin/reports/creacionDeWidgets/' + (widgetElement.find('.mostrarFechaDinamica').html() != undefined && widgetElement.find('.mostrarFechaDinamica').html() == 1 ? widgetElement.find("#fechaWidget").val() : ''),
			type: 'POST',
			dataType: 'html',
			data:  ({idWidge: idWidge, idWiusu: idWiusu, posicion: posicion, undsAncho: undsAncho, tipoGrafico: tipoGrafico, tipoTabla: tipoTabla, minRefresco: minRefresco, multiSerie: multiSerie}),
			//async: false,
			beforeSend: function () 
			{
			    widgetElement.find(".css-loaderWidget").html('<img src="'+base_url+'uploads/default/spinner.gif" width="32" height="32"/>');
			    if(!$("#css-body-vistaAmpliada").hasClass('hide'))
			    {
			    	widgetElement.find(".controlesWidget").addClass("hide");
			    }
			},
			success: function(data)
			{
				widgetElement.find(".css-loaderWidget").html("");
				$('.datosWidget'+(parseFloat(idWiusu) != 0 ? idWiusu : idWidge)).html(data);
				if(widgetElement.find('.datosWidget'+(parseFloat(idWiusu) != 0 ? idWiusu : idWidge))[0])
				{
					if($('.datosWidget'+(parseFloat(idWiusu) != 0 ? idWiusu : idWidge)).find('.mostrarFechaDinamica').html() != 1)
					{
						widgetElement.find('.fechaWidgetDiv').addClass('hide');
					}
					refrescarWidget(widgetElement, tipoGrafico, (parseFloat(idWiusu) != 0 ? idWiusu : idWidge), "#graficoContenedor"+(parseFloat(idWiusu) != 0 ? idWiusu : idWidge), tituloejeX, tituloejeY, "");
				}
				widgetElement.find(".controlesWidget").removeClass("hide");
				if(parseFloat(pdfVista) != 0)
				{
					// si es para pdf se borran las barras de progreso de la tabla
					if(parseFloat(tipoTabla) == 4)
					{
						$(".progress").remove();
						widgetElement.find(".table td:last-child").css('text-align', 'right');
					}
					// si una tabla le cambiamos las propiedades del tamaño para el pdf
					if(parseFloat(tipoGrafico) == 0)
					{
						$(".table").css('width', '100%');
						// contamos cuantos titulos hay en la tabla para ponerles un tamaño en porcentaje y que se acomoden en el pdf
						contadorTitulos = 0;
						widgetElement.find(".table thead tr th").each(function (contadorEach, element)
						{
							contadorTitulos++;
						});
						anchoTd = (100/contadorTitulos)+'%';
						widgetElement.find(".table th").css('width', anchoTd);
						widgetElement.find(".table td").css('width', anchoTd);
					}
					tiempoParaCrearImg(idWiusu, idWidge, tipoGrafico, tipoTabla);
				}
			},
			error: function()
			{
	            console.log("Error al cargar el grafico " + nomWidge);
	        }
		});
	}
	
	function tiempoParaCrearImg(idWiusu, idWidge, tipoGrafico, tipoTabla)
	{
		linkCreacionPdf = base_url+'admin/reports/creacionPdfPanelPrincipal/';
		//nota: se crean con 2 segundos (2000 milisegundos) de diferencia para que el nvd3 alcanze a cargar bien las graficas
		if(parseFloat(tipoGrafico) != 0)
		{
			imgTimerWidget[(parseFloat(idWiusu) != 0 ? idWiusu : idWidge)] = new timerRefrescoWidget(function()
			{
		    	htmlGrafica = $.trim($("#graficoContenedor"+(parseFloat(idWiusu) != 0 ? idWiusu : idWidge)).html());
				//console.log(htmlGrafica);
				anchoGrafico = ($("#"+(parseFloat(idWiusu) != 0 ? idWiusu : idWidge)).width() - (tipoGrafico == 5 ? 30 : 0)) + "px";
				altoGrafico = "350px"; // el alto de los widgets es de 350 por ahora
				
				datosImagenGrafico = crearImagenWidgetGrafica(htmlGrafica, tipoGrafico, anchoGrafico, altoGrafico);
				$.ajax({
					url: base_url+'admin/reports/serverGraficoAImg',
					type: 'POST',
					dataType: "json",
					data: ({datosImagenGrafico: datosImagenGrafico, idRegistro: (parseFloat(idWiusu) != 0 ? idWiusu : idWidge)}),
					success: function(data)
					{
						console.log(data.msg);
						conteoRecorridoWidgetsPdf++;
						if(conteoWidgetsPdf == conteoRecorridoWidgetsPdf)
						{
							location.replace(linkCreacionPdf);
						}
					},
					error: function(err)
				    {
				    	console.log("Ocurrió un error. Por favor inténtelo de nuevo.");
				    	conteoRecorridoWidgetsPdf++;
				    	if(conteoWidgetsPdf == conteoRecorridoWidgetsPdf)
						{
							location.replace(linkCreacionPdf);
						}
				    }
				});
			}, 2000);
		}
		else
		{
			imgTimerWidget[(parseFloat(idWiusu) != 0 ? idWiusu : idWidge)] = new timerRefrescoWidget(function()
			{
		    	htmlTabla = $.trim($("#graficoContenedor"+(parseFloat(idWiusu) != 0 ? idWiusu : idWidge)).html());
		    	htmlTabla = replaceAll(htmlTabla, 'style=', 'styyyyleeee=');
		    	$.ajax({
					url: base_url+'admin/reports/serverTablaAArchivo',
					type: 'POST',
					dataType: "json",
					data: ({htmlTabla: htmlTabla, idRegistro: (parseFloat(idWiusu) != 0 ? idWiusu : idWidge)}),
					success: function(data)
					{
						console.log(data.msg);
						conteoRecorridoWidgetsPdf++;
						if(conteoWidgetsPdf == conteoRecorridoWidgetsPdf)
						{
							location.replace(linkCreacionPdf);
						}
					},
					error: function(err)
				    {
				    	console.log("Ocurrió un error. Por favor inténtelo de nuevo.");
				    	conteoRecorridoWidgetsPdf++;
				    	if(conteoWidgetsPdf == conteoRecorridoWidgetsPdf)
						{
							location.replace(linkCreacionPdf);
						}
				    }
				});
			}, 2000);
		}
	}
	
	function graficaPie(idRegistro, divGrafico)
	{
		// Grafica de pie
		nv.addGraph(function()
		{
	    	var width = 270,
	        	height = 400;
			
	    	var chart = nv.models.pieChart()
	        	.x(function(d) { return d.key })
	        	.y(function(d) { return d.y })
	        	//.showLabels(false)
	        	//.values(function(d) { return d })
	        	.color(d3.scale.category20().range())
	        	.width(width)
	        	.height(height);
			
	      	d3.select(divGrafico)
	        	.datum(pieData(idRegistro))
	        	.transition().duration((parseFloat(pdfVista) == 0 ? 1200 : 0))
	        	.attr('width', width)
	        	.attr('height', height)
	         	.call(chart);
			
	    	chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });
	    	
	    	// para evitar que salga un mensaje en ingles, cambiamos el mensaje
	    	if($(".nv-noData").length > 0)
			{
				$(".nv-noData").html(msgNodatos);
			}
	    	return chart;
		});
	}
	
	// datos grafica pie
	function pieData(idRegistro)
	{
		grafica = new Array();
		data = [];
		dataGrafica = $('.datosWidget'+idRegistro+' .datosGrafico').text();
		dataGrafica = JSON.parse(dataGrafica);
		
		$.each( dataGrafica, function( nombre, datos )
		{
			data.push({'key': datos['nomcategoria'], 'y': datos['valor']});
		});
		grafica = {
			key: "Pie",
			values: data,
	    };
	    //console.log(data);
		return data;
	}
	
	// grafica barras
	function graficaBarras(idRegistro, tituloejeX, tituloejeY, divGrafico)
	{
		nv.addGraph(function()
		{
			var chart = nv.models.discreteBarChart()
		    	.x(function(d) { return d.label })
		      	.y(function(d) { return Math.round(d.value) })
		      	.staggerLabels(true)
		      	.tooltips(true)
		      	.showValues(true)
		      	.valueFormat(d3.format('.d'))
		      	.tooltipContent(function(nomGrafico, codigo, valor, array)
		      	{
		      		//console.log(array);
		      		return '<h3>' + array['point']['nomcategoria'] + '</h3>' + '<p>' + valor + '</p>';
           		});
        	
        	chart.xAxis.axisLabel(tituloejeX);
        	
        	chart.yAxis
            	.axisLabel(tituloejeY)
            	.tickFormat(d3.format('.d'));
            	
        	chart.margin({left: 90,bottom:90});
        	
		  	d3.select(divGrafico)
		      	.datum([datosBarras(idRegistro)])
		    	.transition().duration((parseFloat(pdfVista) == 0 ? 500 : 0))
		      	.call(chart);
			
		  	nv.utils.windowResize(chart.update);
			
			// para evitar que salga un mensaje en ingles, cambiamos el mensaje
	    	if($(".nv-noData").length > 0)
			{
				$(".nv-noData").html(msgNodatos);
			}
			
		  	return chart;
		});
	}
	
	function datosBarras(idRegistro)
	{		
		grafica = new Array();
		data = [];
		
		dataGrafica = $('.datosWidget'+idRegistro+' .datosGrafico').text();
		dataGrafica = JSON.parse(dataGrafica);
		
		$.each( dataGrafica, function( nombre, datosJason )
		{
			data.push({'label': nombre, 'value': datosJason['valor'], 'nomcategoria': datosJason['nomcategoria']});
		});
		
		grafica = {
			key: "Barra",
			values: data,
	    };
	    //console.log(grafica);
		return grafica;
	}
	
	// grafico multi bar char
	function graficaMultiBarChar(idRegistro, tituloejeX, tituloejeY, stacked, divGrafico)
	{
		var chart = nv.models.multiBarChart();
		
		chart.stacked(stacked)
			.showControls(false)
			.showLegend(true)
			//.rotateLabels(90)
			.staggerLabels(true)
			.reduceXTicks(false)
			.tooltipContent(function(nomGrafico, codigo, valor, array)
	      	{
	      		return '<h3>' + array['point']['name'] + '</h3>' + '<p>' + valor + ' Hojas en ' + nomGrafico + '</p>';
       		});
		
		chart.xAxis.axisLabel(tituloejeX);
        	
    	chart.yAxis
        	.axisLabel(tituloejeY)
        	.tickFormat(d3.format('.d'));
		
		chart.margin({left: 90,bottom:90});
		
		d3.select(divGrafico)
			.datum(datosMultiBarChar(idRegistro))
			.transition()
			.duration((parseFloat(pdfVista) == 0 ? 500 : 0))
			.call(chart);
		$('.nv-x .nv-axislabel').attr('y',50);
		
		// para evitar que salga un mensaje en ingles, cambiamos el mensaje
    	if($(".nv-noData").length > 0)
		{
			$(".nv-noData").html(msgNodatos);
		}
	}
	
	function datosMultiBarChar(idRegistro)
	{
		grafica = new Array();
		datosSerie = new Array();
		maxSerie = 0;
		data = [];
		i = 0;
		var colores = [ "#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf" ];
		datosJson = $('.datosWidget'+idRegistro+' .datosGrafico').text();
		datosJson = JSON.parse(datosJson);
		// Sacamos los datos traidos por json encode
		$.each( datosJson, function(nombre, datos )
		{
			if(nombre == 'datosArray')
			{
				dataGrafica = datos;
				//console.log(dataGrafica);
			}
			else
			{
				$.each( datos, function(nombre2, datos2 )
				{
					datosSerie[nombre2] = datos2;
					maxSerie = Math.max(maxSerie,nombre2);
					data[nombre2] = new Array();
				});
			}
		});
		
		$.each( dataGrafica, function( nombre, datos )
		{
			for (var j=1; j<=maxSerie; j++)
			{
				data[j][datos['orden']] = ({x: nombre, y: Math.round(datos['valores'][j]['valor']), name : datos['nomcategoria']});
			}
		});
		
		$.each( data, function( tipo, datos )
		{
			if(datos != undefined)
			{
				array = {
					key: datosSerie[i+1],
					color: colores[i],
					values: datos,
			    };
				grafica[i] = array;
				i++;
			}
		});
	    //console.log(grafica);
		return grafica;
	}
	
	// grafica lineas
	function graficaSimpleLineChar(idRegistro, tituloejeX, tituloejeY, divGrafico)
	{
		// recojemos el array de la vista, para organizar los datos
		datosJson = $('.datosWidget'+idRegistro+' .datosGrafico').text();
		datosJson = JSON.parse(datosJson);
		
		$.each( datosJson, function(nombre, datos )
		{
			if(nombre == 'datosArray')
			{
				dataGrafica = datos;
				//console.log(dataGrafica);
			}
		});
		
		// organizamos los datos en un array
		datosLineasArray['.datosGrafico'+idRegistro] = new Array();
		//console.log(dataGrafica);
		$.each( dataGrafica, function( nombre, datos )
		{
			//datosArray[datos['orden']] = nombre;
			datosLineasArray['.datosGrafico'+idRegistro][datos['orden']] = datos['nomcategoria'];
		});
		
		// grafica de lineas
		nv.addGraph(function()
		{
			var chart = nv.models
				.lineChart();
			
			chart.xAxis
		    	.axisLabel(tituloejeX)
		    	.tickFormat(function(d, i)
		    	{
		    		//console.log(datosArray[d]);
				    return datosLineasArray['.datosGrafico'+idRegistro][d];
				});
			
		  	chart.yAxis
		    	.axisLabel(tituloejeY)
		    	.tickFormat(d3.format('f'));
			
		  	d3.select(divGrafico)
		    	.datum(lineasData(idRegistro))
		    	.transition().duration((parseFloat(pdfVista) == 0 ? 500 : 0))
		    	.call(chart);
			
			nv.utils.windowResize(chart.update);
			
		  	return chart;
		});
	}
	
	function lineasData(idRegistro)
	{
		grafica = new Array();
		datosSerie = new Array();
		i = 0;
		maxSerie = 0;
		data = [];
		datosJson = $('.datosWidget'+idRegistro+' .datosGrafico').text();
		datosJson = JSON.parse(datosJson);
		$.each( datosJson, function(nombre, datos )
		{
			if(nombre == 'datosArray')
			{
				dataGrafica = datos;
				//console.log(dataGrafica);
			}
			else
			{
				$.each( datos, function(nombre2, datos2 )
				{
					datosSerie[nombre2] = datos2;
					maxSerie = Math.max(maxSerie,nombre2);
					data[nombre2] = new Array();
				});
			}
		});
		$.each( dataGrafica, function( nombre, datos )
		{
			for (var j=1; j<=maxSerie; j++)
			{
				if(data[j] == undefined)
				{
					data[j] = new Array();
				}
				data[j][datos['orden']] = ({x: datos['orden'], y: Math.round(datos['valores'][j]['valor']), name : datos['nomcategoria']});
			}
		});
		$.each( data, function( tipo, datos )
		{
			if(datos != undefined)
			{
				array = {
					key: datosSerie[i+1],
					values: datos,
			    };
				grafica[i] = array;
				i++;
			}
		});
		//console.log(grafica);
		// para evitar que salga un mensaje en ingles, cambiamos el mensaje
    	if($(".nv-noData").length > 0)
		{
			$(".nv-noData").html(msgNodatos);
		}
		return grafica;
	}
	
	// vista ampliada
	$(document).on("click", ".zoomWidget", function(event)
	{
		event.preventDefault();
		if($("#css-body-vistaAmpliada").hasClass('hide'))
		{
			$("#css-grupoWidgets").fadeOut( "slow" , function()
		    {
				$("#css-grupoWidgets").addClass("hide");
			});
			$("#css-body-vistaAmpliada").fadeIn("slow", function()
		    {
				$("#css-body-vistaAmpliada").removeClass("hide");
			});
			$("#vistaWidgetsUsuario").addClass("hide");
			$("#VistaAmpliadaWidgetUsuario").removeClass("hide");
		}
		widgetElement = $(this).parents().eq(1);
		vistaAmpliada(widgetElement, "#css-tabla-vistaAmpliada");
		// pausamos el refresco de todos los widgets
		idWidge = widgetElement.find(".idwidge").html();
		idWiusu = widgetElement.find(".idwiusu").html();
		idWidget = (parseFloat(idWiusu) != 0 ? idWiusu : idWidge);
		// colocamos el id del widget que vemos
		$("#css-body-vistaAmpliada").find(".idWidget").html(idWidget);
		$.each( timerWidget, function(idRegistro, widgetTimer)
		{
			if(widgetTimer != undefined)
			{
				if( idWidget != idRegistro)
				{
					widgetTimer.pause();
				}
			}
		});
	});
	
	function vistaAmpliada(widgetElement, divTabla)
	{
		idWidge = widgetElement.find(".idwidge").html();
		idWiusu = widgetElement.find(".idwiusu").html();
		tipoGrafico = widgetElement.find(".tipografico").html();
		tipoTabla = widgetElement.find(".tipotabla").html();
		tituloejeX = widgetElement.find(".tituloejex").html();
		tituloejeY = widgetElement.find(".tituloejey").html();
		multiSerie = widgetElement.find(".multiserie").html();
		nomWidge = widgetElement.find(".nomwidge").html();
		
		if(tipoGrafico==0)
		{
		    $("#reporteAPdf").addClass("hide");
		    
		}
		else
		{
		    $("#reporteAPdf").removeClass("hide");
            
		}
		if(tipoTabla == 0)
		{
			$("#reporteAXls").addClass("hide");
		}
		else
		{
			$("#reporteAXls").removeClass("hide");
			$("#reporteAXls").attr("href", base_url+"admin/reports/exportarExcel/"+idWidge+"/"+tipoGrafico+"/"+multiSerie);
		}
			
		
		$("#tituloVistaAmpliada").html(nomWidge);
		//console.log(idWiusu + ' ----- ' + tipoGrafico + ' ---- ' + tituloejeX + ' ------ ' + tituloejeY);
		$("#graficoContenedorVistaAmpliada svg").empty();
		$(divTabla).empty();
		// si el grafico esta escondido lo mostramos
		if($("#graficoContenedorVistaAmpliada").hasClass('hide'))
		{
			$("#graficoContenedorVistaAmpliada").removeClass("hide");
		}
		
		// organizamos la altura de la grafica, si no es tabla o pie
		if(tipoGrafico != 0 && tipoGrafico != 5)
		{
			altoPantalla = window.innerHeight;
			altoGraficoVistaAmpliada = altoPantalla * 0.60;
			if(altoGraficoVistaAmpliada < 350)
			{
				altoGraficoVistaAmpliada = 350;
			}
			//console.log(altoPantalla + "----" + altoGraficoVistaAmpliada);
			$("#graficoContenedorVistaAmpliada").height(altoGraficoVistaAmpliada);
			$("#graficoContenedorVistaAmpliada svg").height(altoGraficoVistaAmpliada);
			$("#graficoContenedorVistaAmpliada").width("100%");
			
			$("#canvas").attr('width', window.innerWidth);
			$("#canvas").attr('height', altoGraficoVistaAmpliada);
		}
		// el pie no se redimenciona segun la altura, por eso ponemos una altura fija
		if(tipoGrafico == 5)
		{
			$("#graficoContenedorVistaAmpliada").height(370);
			$("#graficoContenedorVistaAmpliada svg").height(370);
			$("#graficoContenedorVistaAmpliada").width(270);
			
			$("#canvas").attr('width', 270);
			$("#canvas").attr('height', 370);
		}
		
		if(tipoGrafico == 0)
		{
			if(!$("#graficoContenedorVistaAmpliada").hasClass('hide'))
			{
				$("#graficoContenedorVistaAmpliada").addClass("hide");
			}
		}
		else
		{
			refrescarWidget(widgetElement, tipoGrafico, (parseFloat(idWiusu) != 0 ? idWiusu : idWidge), "#graficoContenedorVistaAmpliada", tituloejeX, tituloejeY, "");
		}
		
		refrescarTabla(widgetElement, divTabla, "tablaDinamica");
	}
	
	$(document).on("click", "#volverVistaWidgetsUsuario", function(event)
	{
		event.preventDefault();
		$("#css-grupoWidgets").fadeIn("slow", function()
	    {
			$("#css-grupoWidgets").removeClass("hide");
		});
		$("#css-body-vistaAmpliada").fadeOut("slow", function()
	    {
			$("#css-body-vistaAmpliada").addClass("hide");
		});
		$("#vistaWidgetsUsuario").removeClass("hide");
		$("#VistaAmpliadaWidgetUsuario").addClass("hide");
		idWidget = parseFloat($("#css-body-vistaAmpliada").find(".idWidget").html());
		$.each( timerWidget, function(idRegistro, widgetTimer)
		{
			if(widgetTimer != undefined)
			{
				if(idWidget != idRegistro)
				{
					widgetTimer.resume();
				}
			}
		});
	});
	
	// funcion para ingresar un string dentro de otro
	String.prototype.splice = function( idx, rem, s ) {
	    return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
	};
	
	$(document).on("click", "#reporteAPdf", function(event)
	{
		htmlGrafica = $.trim($("#graficoContenedorVistaAmpliada").html());
		idWidge = $("#css-body-vistaAmpliada .idWidget").html();
		tipoGrafica = $("#"+idWidge+" .tipografico").html();
		
		datosImagenGrafico = crearImagenWidgetGrafica(htmlGrafica, tipoGrafica);
		
		$("#datosImagenGrafico").html(datosImagenGrafico).text;
		$("#formGraficoToPdf").submit();
	});
	
	function crearImagenWidgetGrafica(htmlGrafica, tipoGrafica, anchoGrafica, altoGrafica)
	{
		switch(parseFloat(tipoGrafica))
		{
			case 1:
				// estilo de barra simple
				style = '<style type="text/css">rect {   stroke-opacity: 0;    transition: fill-opacity 250ms linear;   -moz-transition: fill-opacity 250ms linear;   -webkit-transition: fill-opacity 250ms linear; }  text {   font-weight: bold;   fill: rgba(0,0,0,1);   stroke: rgba(0,0,0,0); }</style>';
				break;
			case 4:
				// estilo de lineas
				style = '<style type="text/css">.nv-line{  fill: none;  stroke-width: 1.5px; } .nv-thin-line {  stroke-width: 1px; }  .nv-area {  stroke: none; } .nv-point {  fill-opacity: 0;  stroke-opacity: 0; } .nv-point {  fill-opacity: .5 !important;  stroke-opacity: .5 !important; }  .nv-point {  transition: stroke-width 250ms linear, stroke-opacity 250ms linear;  -moz-transition: stroke-width 250ms linear, stroke-opacity 250ms linear;  -webkit-transition: stroke-width 250ms linear, stroke-opacity 250ms linear; } .nv-indexLine {  cursor: ew-resize; }</style>';
				break;
			case 5:
				// estilo de pie
				style = '<style type="text/css">svg {background-color:#fff;} path {   stroke-opacity: 0;   transition: fill-opacity 250ms linear, stroke-width 250ms linear, stroke-opacity 250ms linear;   -moz-transition: fill-opacity 250ms linear, stroke-width 250ms linear, stroke-opacity 250ms linear;   -webkit-transition: fill-opacity 250ms linear, stroke-width 250ms linear, stroke-opacity 250ms linear;  }  text {   stroke: #000;   stroke-width: 0; }  path {   stroke: #fff;   stroke-width: 1px;   stroke-opacity: 1; }  .nv-label {   pointer-events: none; } rect {   fill-opacity: 0;   stroke-opacity: 0; }</style>';
			break;
			default:
				// los multi bar chart quedan mejor sin css y las tablas no lo tienen
				style = '';
		}
		
		// agregamos un elemento rect con fondo blanco como primer elemento del SVG para evitar que se exporte con fondo negro
		style += '<rect style="fill: white; fill-opacity: 100;" width="100%" height="100%"></rect>';
		
		if(tipoGrafica != 0)
		{
			posicionStilo = htmlGrafica.indexOf(">") + 1;
			htmlGrafica = htmlGrafica.splice(posicionStilo, 0, style );
			
			//console.log(htmlGrafica);
			if(anchoGrafica != undefined && altoGrafica != undefined)
			{
				$("#canvas").attr('width', anchoGrafica);
				$("#canvas").attr('height', altoGrafica);
			}
			
			canvg('canvas', htmlGrafica, { ignoreMouse: true, ignoreAnimation: true , ignoreDimensions: true});
			
			// the canvas calls to output a png
			var canvas = document.getElementById("canvas");
			var datosImagenGrafico = canvas.toDataURL("image/jpeg");
			console.log(datosImagenGrafico);
			return datosImagenGrafico;
			//document.write('<img src="'+img+'"/>');
		}
		return "";
	}
	
	function tablaSimpleMultiSerie(idRegistro, divTabla, tituloejeX, tituloejeY, tipoTabla, classTable)
	{
		datosTabla = $('.datosWidget'+idRegistro+' .datosTabla').text();
		if(datosTabla.length > 0)
		{
			datosSerie = new Array();
			valoresSerie = new Array();
			maxSerie = 0;
			// guardamos los datos de la tabla
			datosTabla = JSON.parse(datosTabla);
			
			datosJson = $('.datosWidget'+idRegistro+' .datosGrafico').text();
			datosJson = JSON.parse(datosJson);
			
			// creamos el array de titulo
			datosSerie.push('codigo');
			datosSerie.push(tituloejeX);
			
			$.each( datosJson, function(nombre, datos )
			{
				if(nombre != 'datosArray')
				{
					$.each( datos, function(ordenSerie, datos2 )
					{
						datosSerie.push(datos2);
						valoresSerie[ordenSerie] = 0;
						maxSerie = Math.max(maxSerie, ordenSerie);
					});
				}
			});
			datosSerie.push('total');
			if(tipoTabla == 2)
			{
				datosSerie.push('%');
			}
			
			//console.log(datosTabla);
			
			var tbl=document.createElement('table');
			tbl.style.width='100%';
			tbl.setAttribute('border','1');
			tbl.className = "table table-bordered table-striped table-condensed "+ classTable;
			
			// ponemos los titulos
			var thead=document.createElement('thead');
			var tr=document.createElement('tr');
			$.each( datosSerie, function( row, titulo )
			{
		        tr.appendChild(agregarTd(titulo.toUpperCase(), 'th', false));
			});
	        thead.appendChild(tr);
	        tbl.appendChild(thead);
	        
	        // sacamos el total primero para hacer la tabla de participación y verificamos si ponemos decimales o no dependiendo si tienen
	        var total = 0;
	        var numDecim = false;
	        $.each( datosTabla, function( row, datos )
			{
				var totalFila = 0;
				$.each( datos['valores'], function( ordenSerie, datos2 )
				{
					totalFila += parseFloat(datos2['valor']);
					if(!numDecim)
					{
						numDecim = enteroODecimal(datos2['valor']);
					}
				});
				total += totalFila;
			});
	        
	        // creamos el cuerpo
			var tbdy=document.createElement('tbody');
			
			$.each( datosTabla, function( row, datos )
			{
				//console.log(row);
				//console.log(datos);
		        var tr = document.createElement('tr');
				tr.appendChild(agregarTd(row, 'td', false));
				tr.appendChild(agregarTd(datos["nomcategoria"], 'td', false));
				
				// llenamos los datos de la serie
				var totalFila = 0;
				$.each( datos['valores'], function( ordenSerie, datos2 )
				{
					tr.appendChild(agregarTd(numberFormat(datos2['valor'], numDecim), 'td', true));
					totalFila += parseFloat(datos2['valor']);
					valoresSerie[ordenSerie] += parseFloat(datos2['valor']);
				});
				tr.appendChild(agregarTd(numberFormat(totalFila, numDecim), 'td', true));
				if(tipoTabla == 2)
				{
					tr.appendChild(agregarTd(numberFormat((parseFloat(totalFila)/total)*100, true) + " %", 'td', true));
				}
				//total += totalFila;
				tbdy.appendChild(tr);
			});
			tbl.appendChild(tbdy);
			
			// agregamos los titulos y el total
			var tfoot=document.createElement('tfoot');
			var tr=document.createElement('tr');
			tr.appendChild(agregarTd("", 'td', false, true));
			tr.appendChild(agregarTd("Total", 'td', false, true));
			

			$.each( valoresSerie, function( ordenSerie, datos2 )
			{
				if(datos2 != undefined)
				{
					tr.appendChild(agregarTd(numberFormat(datos2, numDecim), 'td', true, true));
				}
			});
			tr.appendChild(agregarTd(numberFormat(total, numDecim), 'td', true, true));
			if(tipoTabla == 2)
			{
				tr.appendChild(agregarTd("100%", 'td', true, true));
			}
			tfoot.appendChild(tr);
			tbl.appendChild(tfoot);
			$(divTabla).html(tbl);
		}
	}
	
	function tablaSimpleUnaSerie(idRegistro, divTabla, tituloejeX, tituloejeY, tipoTabla, classTable)
	{
		datosTabla = $('.datosWidget'+idRegistro+' .datosTabla').text();
		if(datosTabla.length > 0)
		{
			// guardamos los datos de la tabla
			datosTabla = JSON.parse(datosTabla);
			
			// creamos una rray para guardar los titulos y recorrerlos mas facil
			titulos = new Array();
			titulos[0] = 'codigo';
			titulos[1] = tituloejeX;
			titulos[2] = tituloejeY;
			if(tipoTabla == 2)
			{
				titulos[3] = '%';
			}
			
			var tbl=document.createElement('table');
			tbl.style.width='100%';
			tbl.setAttribute('border','1');
			tbl.className = "table table-bordered table-striped table-condensed "+classTable;
			
			// ponemos los titulos
			var thead=document.createElement('thead');
			var tr=document.createElement('tr');
			$.each( titulos, function( row, titulo )
			{
		        tr.appendChild(agregarTd(titulo.toUpperCase(), 'th', false));
			});
	        thead.appendChild(tr);
	        tbl.appendChild(thead);
	        
	        // sacamos el valor total desde antes para poder sacar el valor de participación
	        var total = 0;
	        var numDecim = false;
	        for(var i=0; i < datosTabla.length; i++)
			{
				total += parseFloat(datosTabla[i]["valor"]);
				if(!numDecim)
				{
					numDecim = enteroODecimal(datosTabla[i]["valor"]);
				}
			}
	        
	        // creamos el cuerpo
			var tbdy=document.createElement('tbody');
			for(var i=0; i < datosTabla.length; i++)
			{
				var tr=document.createElement('tr');
				tr.appendChild(agregarTd(datosTabla[i]["codcategoria"], 'td', false));
				tr.appendChild(agregarTd(datosTabla[i]["nomcategoria"], 'td', false));
				tr.appendChild(agregarTd(numberFormat(datosTabla[i]["valor"], numDecim), 'td', true));
				//total += parseFloat(datosTabla[i]["valor"]);
				if(tipoTabla == 2)
				{
					tr.appendChild(agregarTd(numberFormat((parseFloat(datosTabla[i]["valor"])/total)*100, true) + " %", 'td', true));
				}
				tbdy.appendChild(tr);
			}
			tbl.appendChild(tbdy);
			// agregamos el total
			var tfoot=document.createElement('tfoot');
			var tr=document.createElement('tr');
			tr.appendChild(agregarTd("", 'td', false, true));
			tr.appendChild(agregarTd("Total", 'td', false, true));
			tr.appendChild(agregarTd(numberFormat(total, numDecim), 'td', true, true));
			if(tipoTabla == 2)
			{
				tr.appendChild(agregarTd("100%", 'td', true, true));
			}
			tfoot.appendChild(tr);
			tbl.appendChild(tfoot);
			$(divTabla).html(tbl);
		}
	}
	
	function tablaVariacionesMultiSerie(idRegistro, divTabla, tituloejeX, tituloejeY, tipoTabla, classTable)
	{
		datosTabla = $('.datosWidget'+idRegistro+' .datosTabla').text();
		if(datosTabla.length > 0)
		{
			datosSerie = new Array();
			valoresSerie = new Array();
			maxSerie = 0;
			// guardamos los datos de la tabla
			datosTabla = JSON.parse(datosTabla);
			
			datosJson = $('.datosWidget'+idRegistro+' .datosGrafico').text();
			datosJson = JSON.parse(datosJson);
			
			// creamos el array de titulo
			datosSerie.push('codigo');
			datosSerie.push(tituloejeX);
			
			$.each( datosJson, function(nombre, datos )
			{
				if(nombre != 'datosArray')
				{
					$.each( datos, function(ordenSerie, datos2 )
					{
						datosSerie.push(datos2);
						valoresSerie[ordenSerie] = 0;
						maxSerie = Math.max(maxSerie, ordenSerie);
					});
				}
			});
			
			//console.log(datosTabla);
			
			var tbl=document.createElement('table');
			tbl.style.width='100%';
			tbl.setAttribute('border','1');
			tbl.className = "table table-bordered table-striped table-condensed "+classTable;
			
			// ponemos los titulos
			var thead=document.createElement('thead');
			var tr=document.createElement('tr');
			i = 1;
			$.each( datosSerie, function( row, titulo )
			{
		        tr.appendChild(agregarTd(titulo.toUpperCase(), 'th', false));
		        // colocamos los titulos de variación y %, que empiezan en la columna 4 de la tabla
		        if(i >= 4)
		        {
		        	tr.appendChild(agregarTd("VARIACIÓN", 'th', false));
		        	tr.appendChild(agregarTd("%", 'th', false));
		        }
		        i++;
			});
	        thead.appendChild(tr);
	        tbl.appendChild(thead);
	        
	        // validamos si ponemos decimales
	        var numDecim = false;
	        $.each( datosTabla, function( row, datos )
			{
				$.each( datos['valores'], function( ordenSerie, datos2 )
				{
					numDecim = enteroODecimal(datos2['valor']);
				});
				if(numDecim)
				{
					return false;
				}
			});
	        
	        // creamos el cuerpo
			var tbdy=document.createElement('tbody');
			
			$.each( datosTabla, function( row, datos )
			{
				//console.log(row);
				//console.log(datos);
		        var tr=document.createElement('tr');
				tr.appendChild(agregarTd(row, 'td', false));
				tr.appendChild(agregarTd(datos["nomcategoria"], 'td', false));
				
				// llenamos los datos de la serie
				var valorAnt = 0;
				i = 1;
				$.each( datos['valores'], function( ordenSerie, datos2 )
				{
					tr.appendChild(agregarTd(numberFormat(datos2['valor'], numDecim), 'td', true));
					valoresSerie[ordenSerie] += parseFloat(datos2['valor']);
					if(i >= 2)
					{
						tr.appendChild(agregarTd(numberFormat(parseFloat(datos2['valor'])-valorAnt, numDecim), 'td', true));
						tr.appendChild(agregarTd((valorAnt > 0 ? numberFormat(((parseFloat(datos2['valor'])/valorAnt)-1)*100) : 0) + " %", 'td', true, true));
					}
					valorAnt = parseFloat(datos2['valor']);
					i++;
				});
				tbdy.appendChild(tr);
			});
			
			tbl.appendChild(tbdy);
			// agregamos el total
			var tfoot=document.createElement('tfoot');
			var tr=document.createElement('tr');
			tr.appendChild(agregarTd("", 'td', false, true));
			tr.appendChild(agregarTd("Total", 'td', false, true));
			
			i = 1;
			valorAnt = 0;
			$.each( valoresSerie, function( ordenSerie, datos2 )
			{
				if(datos2 != undefined)
				{
					tr.appendChild(agregarTd(numberFormat(datos2, numDecim), 'td', true, true));
					if(i >= 2)
					{
						tr.appendChild(agregarTd(numberFormat(parseFloat(datos2)-valorAnt, numDecim), 'td', true, true));
						tr.appendChild(agregarTd((valorAnt > 0 ? numberFormat(((parseFloat(datos2)/valorAnt)-1)*100, 2) : true) + " %", 'td', true, true));
					}
					valorAnt = parseFloat(datos2);
					i++;
				}
			});
			tfoot.appendChild(tr);
			tbl.appendChild(tfoot);
			$(divTabla).html(tbl);
		}
	}
	
	function tablaCumplimientoMultiSerie(idRegistro, divTabla, tituloejeX, tituloejeY, tipoTabla, classTable)
	{
		datosTabla = $('.datosWidget'+idRegistro+' .datosTabla').text();
		if(datosTabla.length > 0)
		{
			datosSerie = new Array();
			valoresSerie = new Array();
			maxSerie = 0;
			// guardamos los datos de la tabla
			datosTabla = JSON.parse(datosTabla);
			
			datosJson = $('.datosWidget'+idRegistro+' .datosGrafico').text();
			datosJson = JSON.parse(datosJson);
			
			// creamos el array de titulo
			datosSerie.push('codigo');
			datosSerie.push(tituloejeX);
			
			$.each( datosJson, function(nombre, datos )
			{
				if(nombre != 'datosArray')
				{
					$.each( datos, function(ordenSerie, datos2 )
					{
						datosSerie.push(datos2);
						valoresSerie[ordenSerie] = 0;
						maxSerie = Math.max(maxSerie, ordenSerie);
					});
				}
			});
			datosSerie.push('Cumplimiento');
			
			//console.log(datosTabla);
			
			var tbl=document.createElement('table');
			tbl.style.width='100%';
			tbl.setAttribute('border','1');
			tbl.className = "table table-bordered table-striped table-condensed "+classTable;
			
			// ponemos los titulos
			var thead=document.createElement('thead');
			var tr=document.createElement('tr');
			$.each( datosSerie, function( row, titulo )
			{
		        tr.appendChild(agregarTd(titulo.toUpperCase(), 'th', false));
			});
	        thead.appendChild(tr);
	        tbl.appendChild(thead);
	        
	        // validamos si ponemos decimales
	        var numDecim = false;
	        $.each( datosTabla, function( row, datos )
			{
				$.each( datos['valores'], function( ordenSerie, datos2 )
				{
					numDecim = enteroODecimal(datos2['valor']);
				});
				if(numDecim)
				{
					return false;
				}
			});
	        
	        // creamos el cuerpo
			var tbdy=document.createElement('tbody');
			var total = 0;
			
			$.each( datosTabla, function( row, datos )
			{
				//console.log(row);
				//console.log(datos);
		        var tr=document.createElement('tr');
				tr.appendChild(agregarTd(row, 'td', false));
				tr.appendChild(agregarTd(datos["nomcategoria"], 'td', false));
				
				// llenamos los datos de la serie
				var real = 0;
				var presupuesto = 0;
				var primerPaso = true;
				$.each( datos['valores'], function( ordenSerie, datos2 )
				{
					tr.appendChild(agregarTd(numberFormat(datos2['valor'], numDecim), 'td', true));
					if(primerPaso)
					{
						presupuesto = round(datos2['valor'], 2);
						primerPaso = false;
					}
					else
					{
						real = round(datos2['valor'], 2);
					}
					valoresSerie[ordenSerie] += parseFloat(datos2['valor']);
				});
				// colocamos la barra de porcentaje
				td = agregarTdBarra((presupuesto > 0 ? (real/presupuesto)*100: "0,0"), 'td');
		        tr.appendChild(td);
		        
				tbdy.appendChild(tr);
			});
			tbl.appendChild(tbdy);
			// agregamos el total
			var tfoot=document.createElement('tfoot');
			var tr=document.createElement('tr');
			tr.appendChild(agregarTd("", 'td', false, true));
			tr.appendChild(agregarTd("Total", 'td', false, true));
			
			real = 0;
			presupuesto = 0;
			primerPaso = true;
			$.each( valoresSerie, function( ordenSerie, datos2 )
			{
				if(datos2 != undefined)
				{
					tr.appendChild(agregarTd(numberFormat(datos2, numDecim), 'td', true, true));
					if(primerPaso)
					{
						presupuesto = round(datos2, 2);
						primerPaso = false;
					}
					else
					{
						real = round(datos2, 2);
					}
				}
			});
			td = agregarTdBarra((presupuesto > 0 ? (real/presupuesto)*100: "0,0"), 'td');
	        tr.appendChild(td);
			tfoot.appendChild(tr);
			tbl.appendChild(tfoot);
			$(divTabla).html(tbl);
		}
	}
	
	function agregarTd(valor, tipo, classNum, bold)
	{
		var td=document.createElement(tipo);
        td.appendChild(document.createTextNode('\u0020'));
        td.textContent = valor;
        if(classNum)
        {
        	td.className = "css-colnum";
        }
        if(bold)
        {
        	td.style.fontWeight = "bold";
        }
        return td;
	}
	
	function agregarTdBarra(valor, tipo)
	{
		if(valor == "0,0")
		{
			valor = 0;
		}
		var td = document.createElement(tipo);
		var span = document.createElement('span');
        span.appendChild(document.createTextNode('\u0020'));
        span.textContent = numberFormat(valor, true) + " %";
        span.className = "css-porcProgress";
        td.appendChild(span);
		var div=document.createElement('div');
		switch(true)
		{
			case (valor<=50):
				div.className = "progress progress-danger";
				break;
			case (valor>50 && valor<=75):
				div.className = "progress progress-warning";
				break;
			case (valor>75 && valor<=100):
				div.className = "progress progress-success";
				break;
			case (valor>100):
				div.className = "progress progress-info";
				break;
		}
		var div2= document.createElement('div');
		div2.className = "bar";
		div2.style.width= (valor > 100 ? 100 : valor) + '%';
		div.appendChild(div2);
		td.appendChild(div);
        return td;
	}
	
	// redondea igual que php
	function round(value, precision, mode) 
	{
	  	// http://kevin.vanzonneveld.net
	  	// +   original by: Philip Peterson
	  	// +    revised by: Onno Marsman
	  	// +      input by: Greenseed
	  	// +    revised by: T.Wild
	  	// +      input by: meo
	  	// +      input by: William
	  	// +   bugfixed by: Brett Zamir (http://brett-zamir.me)
	  	// +      input by: Josep Sanz (http://www.ws3.es/)
	  	// +    revised by: Rafał Kukawski (http://blog.kukawski.pl/)
	  	// %        note 1: Great work. Ideas for improvement:
	  	// %        note 1:  - code more compliant with developer guidelines
	  	// %        note 1:  - for implementing PHP constant arguments look at
	  	// %        note 1:  the pathinfo() function, it offers the greatest
	  	// %        note 1:  flexibility & compatibility possible
	  	// *     example 1: round(1241757, -3);
	  	// *     returns 1: 1242000
	  	// *     example 2: round(3.6);
	  	// *     returns 2: 4
	  	// *     example 3: round(2.835, 2);
	  	// *     returns 3: 2.84
	  	// *     example 4: round(1.1749999999999, 2);
	  	// *     returns 4: 1.17
	  	// *     example 5: round(58551.799999999996, 2);
	  	// *     returns 5: 58551.8
	  	if(typeof(value) === 'string' || value instanceof String)
		{
	    	value = parseFloat(value);
	    }
	  	var m, f, isHalf, sgn; // helper variables
	  	precision |= 0; // making sure precision is integer
	  	m = Math.pow(10, precision);
	  	value *= m;
	  	sgn = (value > 0) | -(value < 0); // sign of the number
	  	isHalf = value % 1 === 0.5 * sgn;
	  	f = Math.floor(value);
		
	  	if (isHalf)
	  	{
	    	switch (mode) 
	    	{
		    	case 'PHP_ROUND_HALF_DOWN':
		      		value = f + (sgn < 0); // rounds .5 toward zero
		      		break;
		    	case 'PHP_ROUND_HALF_EVEN':
		      		value = f + (f % 2 * sgn); // rouds .5 towards the next even integer
		      		break;
		    	case 'PHP_ROUND_HALF_ODD':
		      		value = f + !(f % 2); // rounds .5 towards the next odd integer
		      		break;
		    	default:
		      		value = f + (sgn > 0); // rounds .5 away from zero
	    	}
	  	}
		
	  	return (isHalf ? value : Math.round(value)) / m;
	}
	
	function enteroODecimal(numero)
	{
		if(typeof(numero) === 'string' || numero instanceof String)
		{
	    	numero = parseFloat( numero );
	    }
		redondeado = round(numero, 0);
		
		if(redondeado != numero)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function numberFormat(numero, numDecim)
	{
		numero = round(numero, 1);
		numero = numero.toString();
		numero = numero.replace(".",",");
        // Variable que contendra el resultado final
        var resultado = "";
        
        // Si el numero empieza por el valor "-" (numero negativo)
        if(numero[0]=="-")
        {
            // Cogemos el numero eliminando los posibles puntos que tenga, y sin
            // el signo negativo
            nuevoNumero=numero.replace(/\./g,'').substring(1);
        }
        else
        {
            // Cogemos el numero eliminando los posibles puntos que tenga
            nuevoNumero=numero.replace(/\./g,'');
        }
        
        // Si tiene decimales, se los quitamos al numero
        if(numero.indexOf(",")>=0)
            nuevoNumero=nuevoNumero.substring(0,nuevoNumero.indexOf(","));

        // Ponemos un punto cada 3 caracteres
        for (var j, i = nuevoNumero.length - 1, j = 0; i >= 0; i--, j++) 
            resultado = nuevoNumero.charAt(i) + ((j > 0) && (j % 3 == 0)? ".": "") + resultado; 
        
        // Si tiene decimales, se lo añadimos al numero una vez forateado con 
        // los separadores de miles
        if(numDecim)
        {
	        if(numero.indexOf(",")>=0)
	            resultado+=numero.substring(numero.indexOf(","));
	        else
	            resultado+=",0";    
		}
        if(numero[0]=="-")
        {
            // Devolvemos el valor añadiendo al inicio el signo negativo
            return "-"+resultado;
        }
        else
        {
            return resultado;
        }
    }
    
    // sortable
    	// validamos que cuando en el sortable se pase el mouse, le ponga el la funcion de jquery - ui sortable
   	//$(document).on("mousemove", "#css-grupoWidgets", function(event) // evento mouseover o mousemove
    //{
	   	$(".sortableWidgets").sortable
	   	({
			distance: 12,
	        forcePlaceholderSize: true,
	        items: '.css-widget',
	        placeholder: 'card-sortable-placeholder css-widget',
	        tolerance: 'pointer',
	        handle: ".css-header-widget",
	        
	        create: function( event, ui )
	        {
	        	// guardamos la posición de los widgets en una variable global
	        	widgetsArray = $(this).sortable("toArray");
	        },
	        
	        start:  function(event, ui)
	        {         
	        	ui.item.addClass('dragging').removeClass('css-widget');
	            if ( ui.item.hasClass('bigun') ) 
	            {
	                 ui.placeholder.addClass('bigun');
	            }
             	//setTimeout(function(){ $("#css-grupoWidgets").masonry() }, 400);
               //$("#css-grupoWidgets").masonry('reload');
	        },
	        stop:   function(event, ui)
	        { 
               	ui.item.removeClass('dragging').addClass('css-widget');
               	//setTimeout(function(){ $("#css-grupoWidgets").masonry() }, 400);
               	//$("#css-grupoWidgets").masonry('reload');
               	widgetsArrayCambio = $(this).sortable("toArray");
               	//console.log(widgetsArray);
	            //console.log(widgetsArrayCambio);
	            
	            // si la posición de los widgets cambio, guardamos el cambio en la BD
	            if(widgetsArray.compare(widgetsArrayCambio) === false)
	            {
	            	Sortable($(this), widgetsArray, widgetsArrayCambio);
	            	//widgetsArray = widgetsArrayCambio;
	            }
	        },
	   	});
   	//});
   
   	function Sortable(element, widgetsArrayViejo, widgetsArrayNuevo)
   	{
		$.ajax
		({
			type: "POST",
			dataType: "json",
			url: base_url+'admin/reports/sortableWidget',
			data: ({widgetsArrayViejo: widgetsArrayViejo, widgetsArrayNuevo: widgetsArrayNuevo}),
			beforeSend: function () 
			{
			    //$("#carga").html('<img src="'+base_url+'uploads/default/spinner.gif" width="28" height="28"/>');
			},
			success: function(data, status)
			{
				$("#carga").empty();
				if(data.status == 'error')
		        {
		           	bootbox.alert(data.msg, function()
					{
						location.reload();
					});
		        }
		        else
		        {
		        	widgetsArray = widgetsArrayNuevo;
		        }
			},
			error: function(err)
	        {
	        	simple_notice(titulo="Error",mensaje="Ocurrió un error. Por favor inténtelo de nuevo.",tiempo=2000,estilo="error",clasepersonal="",icono="icon-warning-sign");
	        	$("#carga").empty();
	        }
		});
   	}
   	// -----------------------------------------------------------------------------------
   	// comparar dos array para saber si son iguales
   	Array.prototype.compare = function (array)
   	{
	    // if the other array is a falsy value, return
	    if (!array)
	        return false;
	
	    // compare lengths - can save a lot of time
	    if (this.length != array.length)
	        return false;
	
	    for (var i = 0; i < this.length; i++) {
	        // Check if we have nested arrays
	        if (this[i] instanceof Array && array[i] instanceof Array) {
	            // recurse into the nested arrays
	            if (!this[i].compare(array[i]))
	                return false;
	        }
	        else if (this[i] != array[i]) {
	            // Warning - two different object instances will never be equal: {x:20} != {x:20}
	            return false;
	        }
	    }
	    return true;
	}
	
	function isEmpty( el )
	{
      	return !$.trim(el.html());
  	}
  
  	function timerRefrescoWidget(callback, delay)
  	{
    	var timerId, start, remaining = delay;
		
    	this.pause = function()
    	{
        	window.clearTimeout(timerId);
        	remaining -= new Date() - start;
    	};

    	this.resume = function()
    	{
        	start = new Date();
        	timerId = window.setTimeout(callback, remaining);
    	};
		
    	this.resume();
	}
	
	function simple_notice()
	{
		$.pnotify({
			title: titulo,
			text: mensaje,
			type: estilo,
			delay:tiempo,
			addclass:clasepersonal,
			icon:icono,
			history:false, // quita la opción de histótico de avisos
			mouse_reset:false, // evita que el mensaje no se oculte cuando el mouse está encima
			sticker: false // desaparece el botón play
		});
	}
	
	function replaceAll( text, busca, reemplaza ){
	  while (text.toString().indexOf(busca) != -1)
	      text = text.toString().replace(busca,reemplaza);
	  return text;
	}
});
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
*
* @author 	    Luis Fernando Salazar
* @package 	PyroCMS
* @subpackage 	google_maps
* @category 	Modulos
*/

class Google_Maps extends Public_Controller {
	
	var $moduleName = 'google_maps';
	
    public function __construct() {
        parent::__construct();
        $models = array(
            "google_map_model",
            "google_map_category_model",
            "google_map_image_model",
            "google_map_intro_model"
            );
        $this->load->model($models);
    }

// -----------------------------------------------------------------

    public function index($selCategory = null)
    {
        $category = null;
		$search = '';
        if($selCategory){
            $category = $this->google_map_category_model->get_many_by("slug",$selCategory);
            if (count($category) > 0) {
            $category = $category[0]; // Categoria Seleccionada

            $return = $this->db->where('category_id',$category->id)->get('google_maps_categories')->result();
            $selected_category = array();
            $google_maps= array();
            $selected_google_maps = array();

            foreach ($return as $item) {
                $selected_google_maps[] = $item->google_map_id;
            }

            if(count($selected_google_maps) > 0){
                for ($i=0;$i<count($selected_google_maps);$i++) {
                // Se consulta google_maps a google_maps de la categoria
                    $google_map = $this->google_map_model->get_many_by("id",$selected_google_maps[$i]);
                    $google_map = $google_map[0];
                // Llenamos el nuevo objeto con todos los Mapas de google
                    $google_mapsObj = new stdClass();
                    $google_mapsObj->id = $google_map->id;
                    $google_mapsObj->name = substr($google_map->name, 0, 20);
                    $google_mapsObj->image = val_image($google_map->image);
                    $google_mapsObj->description = substr($google_map->description, 0, 100);
                    $google_mapsObj->slug = $google_map->slug;
                    $google_mapsObj->url = site_url('google_maps/detail/'.$google_map->slug);
                $google_maps[] = $google_mapsObj; // Array de objectos
            }

                // Se ordena el Array por id de forma DESC
            @usort($google_maps, function($a, $b)
            {
                return strcmp($b->id, $a->id);
            });
        }

    }else{
                redirect("google_maps"); // si la categoria no existe me redirecciona a todos
            }
        }else{
		
		if(isset($_POST['shearch']))
		{
			// Se consultan los Mapas de google
            $google_maps = $this->google_map_model
            ->like('name', $_POST['shearch'])
            ->order_by('id', 'DESC')
            ->get_all();
			$search = $_POST['shearch'];
		}
		else
		{
			// Se consultan los Mapas de google
            $google_maps = $this->google_map_model
            ->order_by('id', 'DESC')
            ->get_all();
		}
        
            foreach($google_maps AS $item)
            {
                $item->name = substr($item->name, 0, 20);
                $item->image = val_image($item->image);
                $item->description = substr($item->description, 0, 100);
                $item->url = site_url('google_maps/detail/'.$item->slug);
            }
        }

    // Consultamos las categorias
        $categories = $this->google_map_category_model
        //->order_by('title', 'ASC')
        ->order_by('position', 'ASC')
        ->get_all();

    // Intro
        $in = $this->google_map_intro_model->get_all();
        $intro = array();
        if (count($in) > 0) {
            $intro = $in[0];
        }

    // Devuelve arbol en HTML, el segundo parametro es el nombre del modulo
        $menu = treemenu($categories,'google_maps');

        $this->template
        ->set('google_maps', $google_maps)
        ->set('category', ($category) ? "/ ".$category->title : null)
        ->set('categories', $categories)
		->set('current', ($category) ? $category->title : null)
        //->set('menu', $menu)
        ->set('intro', $intro)
		->set('search', $search)
		->set('selCategory', $selCategory)
        ->build('index');
    }


// ----------------------------------------------------------------------

    public function detail($slug)
    {
		
        $return = $this->google_map_model->get_many_by('slug', $slug);
        $return = $return[0];

        if(!$return)
            redirect('google_maps');

        // Se convierten algunas variables necesarias para usar como slugs
        $setter = array(
            'image' => val_image($return->image),
            );

        $google_map = array_merge((array)$return,$setter);

        $relation = $this->db->where('google_map_id',$google_map['id'])->get('google_maps_categories')->result();
        $categories = array();
        foreach ($relation as $item) {
            $category = $this->google_map_category_model->get_many_by('id', $item->category_id);
            $category = $category[0];
            $categories[] = array(
                    "title" => $category->title,
                    "slug" => $category->slug
                );
        }

        // imagenes para slider
        $images = $this->google_map_image_model->get_many_by('google_map_id', $google_map['id'] );
		
		// datos para el mapa de google
		
		// mandamos los puntos
		$json_markers = json_encode(array( array($google_map['adress'].', Prueba-Cali', (double)$google_map['coordinate1'], (double)$google_map['coordinate2'] )));
		
		// sacamos solo el nombre de la imagen
		$imageTogoogle = str_replace(site_url().'uploads/default/'.$this->moduleName.'/', "", $google_map['image']);
		
		// mandamos los datos para el modal dentro del mapa
		$json_info_content = json_encode(array( array('image' => $imageTogoogle, 'adress' => $google_map['adress'], 'title' => $google_map['name'], 'moduleName' => $this->moduleName, 'schedule' => $google_map['schedule'], 'description' => $google_map['description'] ) ));
		
        $this->template
    		->append_js('module::google_maps.js')
			->set('json_info_content', $json_info_content)
			->set('json_markers', $json_markers)
            ->set('google_map', (object) $google_map)
            ->set('categories', $categories)
            ->set('images', $images)
            ->build('detail');

    }

	public function multiple_poins()
	{
		$json_markers = array();
		$json_info_content = array();
		$statusJson = 'error';
		$msgJson = 'Ocurrio un error al cargar los datos de google, porfavor intente más tarde';
		
		if(isset($_POST['idcategory']))
		{
			$idCategory = $_POST['idcategory'];
			
			$google_map = $this->db
					->select('*')
					->from('google_maps AS gm')
					->join('google_maps_categories AS gc', 'gc.google_map_id = gm.id', 'left')
					->where('gc.category_id', $idCategory)
					//->limit($pagination['limit'], $pagination['offset'])
					->get()->result();
			
			if(!empty($google_map))
			{
				foreach($google_map AS $item)
				{
					$item->image = val_image($item->image);
					array_push($json_markers, array($item->adress.', '.$item->name, (double)$item->coordinate1, (double)$item->coordinate2) );
					
					$imageTogoogle = str_replace(site_url().'uploads/default/'.$this->moduleName.'/', "", $item->image);
					
					array_push($json_info_content, array('image' => $imageTogoogle, 'adress' => $item->adress, 'title' => $item->name, 'moduleName' => $this->moduleName, 'schedule' => $item->schedule, 'description' => $item->description ) );
				}
			}
			$statusJson = '';
			$msgJson = 'datos de google cargados correctamente';
		}
		// mandamos los puntos
		$json_markers = json_encode($json_markers);
		
		// mandamos los datos para el modal dentro del mapa
		$json_info_content = json_encode($json_info_content);
		
		echo json_encode(array('status' => $statusJson, 'msg' => $msgJson, 'json_markers' => $json_markers, 'json_info_content' => $json_info_content));
	}
}
<div>
{{ sel }}
</div>

  <section class="cont_home">   
      <!--Titulo-->
      <div class="tittles marco">
        <h1>{{ category.title }}</h1>
        <span>{{ if pagination.shown > 0 }} {{ pagination.shown }}  Items {{ else }} 0 Items  {{ endif }}</span>
      </div>
      <!--Fin Titulo-->

      <div class="clear"></div>      

       <!--Contenidos Sitio-->
      <section class="cont_produt marco">
        <!--Filtrador de busqueda-->
        <section class="row buscador_cat">
          <article class="large-3 medium-3 small-12 columns"><strong>Búsqueda por Categoría</strong></article>
          <article class="large-9 medium-9 small-12 columns filtros_produc">
             <div class="controls elem_blancos">		
				
                <div class="large-9 medium-9 small-12 columns filter_for_tipo">
                   <label>Filtrar por:</label>
                    <form class="category-filter">
                      <select class="filtro_orden">
                          {{ ordering }}
                            <option value="{{ firesale:url route="category-custom" }}order/{{ key }}"><a href="{{ firesale:url route="category-custom" }}order/{{ key }}">{{ title }}</a></option>
                          {{ /ordering }}
                        </select>
                    </form>

                <div class="large-3 medium-3 small-12 columns filter_for_orden">
                  <!--<label>Ordenar por:</label>
                    <form class="category-filter">
                      <select class="filtro_orden">
                          {{ ordering }}
                            <option value="{{ firesale:url route="category-custom" }}order/{{ key }}"><a href="{{ firesale:url route="category-custom" }}order/{{ key }}">{{ title }}</a></option>
                          {{ /ordering }}
                        </select>
                    </form>
                  -->
                </div>

                <div class="large-3 medium-3 small-12 columns filter_responsive">
                    <select class="filtro_select item1">
                      <option value="0">Categoria </option>   
                      <option value=".category-3">Categoria</option>
                      <option value=".category-1">Categoria</option>
                      <option value=".category-3">Categoria</option>
                      <option value=".category-2">Categoria</option>
                      <option value=".category-1">Categoria</option>
                      <option value=".category-3">Categoria</option>
                    </select>

                    <select class="filtro_select item2">

                      <option value="0">Subcategoria </option>                  

                      <option value=".category-3">Subcategoria</option>

                      <option value=".category-1">Subcategoria</option>

                      <option value=".category-3">Subcategoria</option>

                      <option value=".category-2">Subcategoria</option>

                      <option value=".category-1">Subcategoria</option>

                      <option value=".category-3">Subcategoria</option>

                    </select>

                </div>



              </div>

          </article>

        </section>

        <!--Fin Filtrador de busqueda-->





        <!--TODOS LOS PRODUCTOS-->

        <section class="all_product_categ">

          <!--Categorias-->

          <article class="large-3 medium-3 small-12 columns relative list_categ_lis">


            <ul class="list_cat">
                {{ firesale:categories order="ordering_count asc" parent="0" }}
               {{ if children > 0 }}
                <li>
                    <a href="javascript:void(0)" {{ if <?php echo $categoryId; ?> == id }} class="active_acord" {{ endif }}>{{ title }}</a>
                      <div class="conten_subCagtegori {{ if <?php echo $categoryId; ?> == id }} conten_subCagtegori_open {{ endif }}">
                    {{ firesale:sub_categories order="ordering_count asc" parent=id }}
                    {{ if children > 0 }}
                     <a href="{{ firesale:url route='category' id=id }}" title="{{ title }}<">{{ title }}</a>
                        <ul id="cat_ul_{{id}}">
                          {{ firesale:sub_sub_categories order="ordering_count asc" parent=id }}
                           <li id="cat_{{id}}"><a href="{{ firesale:url route='category' id=id }}"><span>{{ title }}</span></a></li>
                          {{ /firesale:sub_sub_categories }}
                        </ul>
                     
                    {{ else }}
                      <a href="{{ firesale:url route='category' id=id }}" title="{{ title }}<">{{ title }}</a>
                    {{ endif }}
                    {{ /firesale:sub_categories }}
                    </div>
                </li>
                {{ else }}
                  <li id="cat_{{id}}">
                    <a href="{{ firesale:url route='category' id=id }}">{{ title }}</a>
                  </li>
                {{ endif }}
                {{ /firesale:categories }}
          </ul>



            <div class="newsll elem_blancos">

              <h3>Suscríbete a nuestro</h3>

              <h1>{{ text_newsletter:title }}</h1>

              <p>{{ text_newsletter:text }}</p>

              <form method="get">
                  <input type="text" name="email" id="email" placeholder="Correo Electrónico">  
                <div class="confirm checkbox">
                  <input type="checkbox" id="termCond" name="termCond"/>

                  <label for="termCond">Acepto <a href="{{ url:base }}{{ text_newsletter:terms_cond }}" target="_blank">Términos y Condiciones.</a>
                  <!--<label for="termCond">Acepto <a href="#modal_terminos" data-effect="mfp-move-horizontal">Términos y Condiciones.</a>
                  <p>Acepto <a href="{{ url:base }}{{ text_newsletter:terms_cond }}" target="_blank">Términos y Condiciones</a>.</p> -->

                  </label>
                </div>               
                
                <a href="#modal_nwslletter" data-effect="mfp-move-horizontal" class="btn_gris block btn_suscrip">SUSCRIBIRME</a> 

              </form>

            </div>

          </article>

          <!--Fin Categorias-->



          <!--Productos-->

          <article class="large-9 medium-9 small-12 columns relative lista_products">             

              <div class="destacados container">
                {{ if products }}
                {{ products }}
                  <li class="large-4 medium-6 small-12 columns mix category-1" >
                      <div class="marco_prod">
                        <div class="img_prod">
                          <div class="action_share modal_all">
                              <?php if (module_enabled('firesale_wishlist')): ?> 
                                  {{ if user:logged_in }} 
                                  <a href="#wishlistModal" data-item-id="{{ id }}" data-toggle="modal" data-target="#wishlistModal" data-placement="top" data-original-title="Agregar a mi lista" data-effect="mfp-move-horizontal" class="wish_modal"><i class="fa fa-heart"></i></a> 
                                  {{ endif }}
                              <?php endif; ?>
                              <a class="fb-ico facebook_popup link" data-placement="top" data-original-title="Compartir en Facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer.php?s=100&p[title]={{ title }}&p[url]={{ firesale:url route='product' id=id }}{{ if images.1.id }}&p[images][0]={{ url:site uri='files/large' }}/{{ images.1.id }}{{ endif }}">
                                <i class="fa fa-facebook"></i>
                              </a>
                              <a class="tw-ico twitter_popup link" data-placement="top" data-original-title="Compartir en Twitter" href="http://twitter.com/share?url={{ firesale:url route='product' id=id }}&text={{ title }}%20is%20awesome!{{ if {theme:options:twitter_account} }}&via={{ theme:options:twitter_account }}{{ endif }}">
                                <i class="fa fa-twitter"></i>
                              </a>
                          </div>
                          <div class="mask modal_all">
                             <a href="#modal_preview" data-href="{{url:base}}product/{{slug}}" id="quick_view" data-effect="mfp-3d-unfold">
                             <div class="btn_blanco">Vista rápida</div>
                             </a>
                          </div>
                          {{ if images }}
                            {{ if images.0.id }}
                            <img  src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }} alternate" />
                            {{ else }}
                            <img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                            {{ endif }}
                            {{ else }}
                            {{ theme:image file="no-image.png" alt=title }}
                          {{ endif }}
                            {{ if new_item AND descuento > '0' }}
                                <span class="etiq_new">NUEVO</span>
                                <span class="etiq_desc desc_bot">{{ descuento }}%<i>dscto</i></span>
                            {{ elseif descuento > '0' }}
                               <span class="etiq_desc">{{ descuento }}%<i>dscto</i></span>
                            {{ elseif new_item }}
                                <span class="etiq_new">NUEVO</span>
                            {{ endif }}
                        </div>
                        <div class="cont_head_cat color">
                          <!--<div class="right">
                              {{ if stock > "0" }}
                                <b>En stock</b>
                              {{ else }}
                                <b>Sin stock</b>
                              {{ endif }}
                          </div>-->
                            <div class="clear"></div>
                        </div>
                        <p>{{ title }}</p>
                        {{ if descuento > '0' }}
                            <div class="precio precio_ant">${{ price_ant }}</div>
                            <div class="precio">{{ price_formatted }}</div>
                        {{ else }}
                                <div class="precio">{{ price_formatted }}</div>
                        {{ endif }}
                         <div class="clear"></div>
                        <div class="block"> 
                        {{ if stock > 0 }}
                          <a href="{{ firesale:url route='product' id=id }}" title="">
                            <div class="btn_border">
                              Detalle
                            </div>  
                          </a>
                          <!-- {{ if modifiers }}
                              {{ if modifiers.1.variations.1.product.id }}
                                <a href="{{ firesale:url route='cart' after='/insert/' }}{{ modifiers.1.variations.1.product.id }}">
                                  <div class="btn_border p_cart">
                                  Agregar al Carrito
                                  </div>
                                </a>
                              {{ endif }}
                            {{ else }}
                            <a href="{{ firesale:url route='cart' after='/insert/' }}{{ id }}">
                              <div class="btn_border p_cart">
                              Agregar al Carrito
                              </div>       
                            </a>
                          {{ endif }} -->                      
                              
                        {{ else }}
                          <a href="{{ firesale:url route='product' id=id }}" title="">
                            <div class="btn_border">
                              Detalle
                            </div>  
                          </a>
                        {{ endif }}
                        </div>
                      </div>
                  </li>
                {{ /products }}
                {{ else }}
                <div class="aler_modal_p animated fadeInUp m_t_40"><p ><?php echo lang('firesale:prod_none'); ?></p><i class="fa fa-warning"></i></div>
                {{ endif }}
                <div class="clear"></div>
                <div class="large-12">
                    <!-- Pagination -->
                    <div class="paging pagination-container">
                        {{ pagination.links }}
                    </div>
                </div>  
              </div>

          </article>

          <!-- Fin Productos-->



        </section>  

        <!--FIN TODOS LOS PRODUCTOS-->          



      </section>

        <!--Fin Contenidos Sitio-->



  </section>

  <!--Fin Contenidos Sitio-->

<div class="search">
    <div class="container">
		
        
        <div class="quarter">

            <!-- Sidebar items -->

            <div class="sidebar-items bkg-beta">
				<div class="inner-box">
                    {{ firesale:products p.featured="1" limit="4" order="title desc" }}
                    {{ if entries }}
                    <strong class="main-font box-title">Productos Destacados</strong>
                    {{ entries }}
                    <div class="sidebar-item item">
    
                        <div class="sidebar-image">
                            <a href="{{ firesale:url route='product' id=id }}">
                                {{ if image }}
                                <img src="{{ url:site uri='files/thumb' }}/{{ image }}/200/200/fit" alt="{{ title }}" />
                                {{ else }}
                                {{ theme:image file="no-image.png" alt=title }}
                                {{ endif }}
                            </a>
                        </div>
    
                        <div class="sidebar-text">
                            <a class="item-title pull-right" href="{{ firesale:url route='product' id=id }}">{{ title }}</a>
                            <p class="price main-font">{{ price_formatted }}</p>
                            <a class="btn btn-alpha" href="{{ firesale:url route='product' id=id }}"><span>VER</span></a>
                            {{ if modifiers }}
                            <!--<a href="{{ firesale:url route='cart' after='/insert/' }}{{ modifiers.1.variations.1.product.id }}">Add to Cart</a>-->
                            {{ else }}
                            <!--<a href="{{ firesale:url route='cart' after='/insert/' }}{{ id }}">Add to Cart</a>-->
                            {{ endif }}
                        </div>
    
                        <div class="clearfix"></div>
    
                    </div>
                    {{ /entries }}
                    {{ endif }}
                    {{ /firesale:products }}
				</div>
            </div>
        </div>
        
        <div class="three-quarter">
			
            <div class="bkg-delta products-top">
            	<div class="inner-box">
                    <strong class="main-font box-title">{{ template.title }}</strong>
				</div>
            </div>
<!--                {{ if products }}
            <div id="listing-sort">
                <select class="sortby" name="order">
                    {{ ordering }}
                    <option value="{{ key }}">{{ title }}</option>
                    {{ /ordering }}
                </select> 
            </div>
            <div class="sort-paging">
                <div class="paging pagination-container">
                    {{ pagination.links }}
                </div>
            </div>
            {{ endif }}-->

            <div class="clearfix"></div>

            <div class="listings">
                {{ if products }}
                {{ products }}
            
            <div class="third prod-item clearfix">
                <div class="prod-box">
                    <div class="prod-actions">
                        <div class="actions-btn">
                            
                             <?php if (module_enabled('firesale_wishlist')): ?>
                                {{ if user:logged_in }} 
                                    <a class="add_to_wishlist whish-ico link wish_modal" data-item-id="{{ id }}" data-toggle="modal" data-target="#wishlistModal" data-placement="top" data-original-title="Agregar a la whishlist"></a>
                                {{ endif }}
                            <?php endif; ?>
                           
                            <a class="fb-ico facebook_popup link" data-placement="top" data-original-title="Share this on Facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer.php?s=100&p[title]={{ title }}&p[url]={{ firesale:url route='product' id=id }}{{ if images.1.id }}&p[images][0]={{ url:site uri='files/large' }}/{{ images.0.id }}{{ endif }}"></a>
                            <a class="tw-ico twitter_popup link" data-placement="top" data-original-title="Share this on Twitter" href="http://twitter.com/share?url={{ firesale:url route='product' id=id }}&text={{ title }}%20is%20awesome!{{ if {theme:options:twitter_account} }}&via={{ theme:options:twitter_account }}{{ endif }}"></a>
                        </div>
                    </div>	
                    <a class="item-image" href="{{ firesale:url route='product' id=id }}">
                        {{ if images }}
                        {{ if images.1.id }}
                        <img class="first" src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                        <img class="rollover" src="{{ url:site uri='files/thumb' }}/{{ images.1.id }}/360" alt="{{ title }} alternate" />
                        {{ else }}
                        <img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                        {{ endif }}
                        {{ else }}
                        {{ theme:image file="no-image.png" alt=title }}
                        {{ endif }}
                        
                    </a>
                   
                    <div class="inner-box">
                        <a href="{{ firesale:url route='product' id=id }}">
                            <span class="product-title">{{ title }}</span>
                        </a>
                        <div class="price main-font">{{ price_formatted }}</div>
                        <a class="btn btn-alpha" href="{{ firesale:url route='product' id=id }}"><span>VER</span></a>
                        {{ if modifiers }}
                        <!--<a class="btn btn-alpha" href="{{ firesale:url route='cart' after='/insert/' }}{{ modifiers.1.variations.1.product.id }}"><span>AGREGAR AL CARRITO</span></a>-->
                        {{ else }}
                        <!--<a class="btn btn-alpha" href="{{ firesale:url route='cart' after='/insert/' }}{{ id }}"><span>AGREGAR AL CARRITO</span></a>-->
                        {{ endif }}  
                    </div>
    
                </div>
            </div>
            {{ /products }}
                {{ else }}
                <div class="span9"><p class="center-text large-text"><?php echo lang('firesale:prod_none'); ?></p><p class="center-text">(Don't give up - check the spelling, or try less specific search terms)</p></div>
                {{ endif }}
                <div class="span9">
                    <!-- Pagination -->
                    <div class="paging pagination-container">
                        {{ pagination.links }}
                    </div>
                </div>           

            </div>


        </div>                                                                    

        


    </div>
</div>

<div class="container">
    {{ theme:partial name="recent_items" }}
</div>  
<?php defined('BASEPATH') or exit('No direct script access allowed')

// CH: Recursive rmdir
function rmdirr($directory)
{
	// CH: I can't remember if this works in PHP 5.2 or not... I guess we'll find out.
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($directory, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::CHILD_FIRST
    );

    foreach ($files as $fileinfo) {
        $func = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
        $func($fileinfo->getRealPath());
    }

    rmdir($directory);
}
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_ratings extends Admin_Controller
{
    public $section = "reviews";

    public function __construct()
    {

        parent::__construct();

        // Add data
        $this->data = new stdClass();

        // Load libraries
        $this->load->driver('Streams');
        $this->lang->load('firesale_reviews/firesale');
        $this->load->model('reviews_m');

        // Get the stream
        $this->stream = $this->streams->streams->get_stream('firesale_reviews', 'firesale_reviews');

    }

    public function index()
    {

        // Get fields
        $this->data->fields = $this->reviews_m->get_fields();

        // Build page
        $this->template->append_css('module::reviews.css')
                       ->append_js('module::reviews.js')
                       ->title(lang('firesale:title') . ' ' . lang('firesale:reviews:ratings'))
                       ->build('admin/ratings/index', $this->data);
    }

    public function create()
    {

        // Posted
        if( $this->input->post('btnAction') == 'save' ) {

            // Add validation
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('maximum', 'Maximum Rating', 'required|numeric');
            $this->form_validation->set_rules('default', 'Default Rating', 'required|numeric');

            // Run validation
            if( $this->form_validation->run() ) {

                // Build data to create field
                $data = $this->reviews_m->build_field_data($this->input->post());

                // Add to the stream
                if( $this->streams->fields->add_field($data) ) {
                    $this->session->set_flashdata('success', 'Success!');
                    redirect('admin/firesale_reviews/ratings');
                }

            }

            // Something went wrong
            $this->session->set_flashdata('error', 'Error!');
            $this->data->input = $this->input->post();

        }

        // Assign data
        $this->data->type = 'create';

        // Build page
        $this->template->append_css('module::reviews.css')
                       ->append_js('module::reviews.js')
                       ->title(lang('firesale:title') . ' ' . lang('firesale:reviews:ratings:create'))
                       ->build('admin/ratings/create', $this->data);
    }

    public function edit($id)
    {

        // Variables
        $field = $this->reviews_m->get_fields($id);

        // Input data
        $this->data->input = array(
                                'title' 	  => $field->field_name,
                                'is_required' => $field->is_required,
                                'maximum'	  => $field->field_max,
                                'default'	  => $field->field_data['default_value']
                             );

        // Posted
        if( $this->input->post('btnAction') == 'save' ) {

            // Add validation
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('maximum', 'Maximum Rating', 'required|numeric');
            $this->form_validation->set_rules('default', 'Default Rating', 'required|numeric');

            // Run validation
            if( $this->form_validation->run() ) {

                // Load required items
                $this->load->model('streams_core/fields_m');

                // Build data to create field
                $data = $this->reviews_m->build_field_data($this->input->post());

                // Add to the stream
                if( $this->fields_m->update_field($field, $data) ) {
                    $this->session->set_flashdata('success', 'Success!');
                    redirect('admin/firesale_reviews/ratings');
                }

            }

            // Something went wrong
            $this->session->set_flashdata('error', 'Error!');
            $this->data->input = $this->input->post();

        }

        // Assign data
        $this->data->id   = $id;
        $this->data->type = 'edit';

        // Build page
        $this->template->append_css('module::reviews.css')
                       ->append_js('module::reviews.js')
                       ->title(lang('firesale:title') . ' ' . lang('firesale:reviews:ratings:edit'))
                       ->build('admin/ratings/create', $this->data);
    }

    public function delete($id)
    {

        // Variables
        $current = $this->reviews_m->get_fields($id);
        $all     = $this->reviews_m->get_fields();

    }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wishlists_m extends MY_Model
{

    public function __construct()
    {
        parent::__construct();

        // Add initial items
        $this->data   = new stdClass();
        $this->stream = $this->streams->streams->get_stream('firesale_wishlists', 'firesale_wishlists');
    }

    public function create($name, $desc, $user_id)
    {
    	// Variables
    	$data = array(
			'created'        => date(),
			'created_by'     => $user_id,
			'ordering_count' => '0',
			'title'          => $name,
			'status'         => '1',
			'privacy'        => '0',
			'description'    => $desc
    	);

    	// Ensure this isn't a duplicate
    	$query = $this->db->where('created_by', $user_id)->where('title', $name)->get('firesale_wishlists');
    	if ( $query->num_rows() ) {
    		$result = $query->row();
    		return $result->id;
    	}

    	// Add it
    	$status = $this->db->insert('firesale_wishlists', $data);

    	// Send back ID
    	return $this->db->insert_id();
    }

	public function owner($list, $user_id)
	{
		// Get list owner
		$query = $this->db->select('created_by')->where('id', $list)->get('firesale_wishlists');

		// Check for results
		if ( $query->num_rows() ) {

			// Get owner and check with user
			$owner = $query->row()->created_by;
			return ( $owner == $user_id );
		}

		// Not found
		return false;
	}

	public function get_list($list_id)
	{
        // Load required items
        $this->load->model('firesale/currency_m');

		// Variables
		$currency = $this->pyrocache->model('currency_m', 'get', array(), $this->firesale->cache_time);
        $params   = array(
            'stream'    => $this->stream->stream_slug,
            'namespace' => $this->stream->stream_namespace,
            'limit'     => 100,
            'offset'    => 0,
            'order_by'  => 'ordering_count',
            'sort'      => 'asc',
            'where'     => SITE_REF."_firesale_wishlists.id = '{$list_id}'"
        );

        // Get the list
        $list = $this->streams->entries->get_entries($params);
		
        // Check we have a list
        if ( $list['total'] == 1 ) {

        	// Reassign
        	$list = $list['entries'][0];

        	// Get list items
        	$list['items'] = $this->list_items($list['id']);
        	$list['total'] = 0;
        	$list['price'] = 0;

        	// Check items
        	if ( ! $list['items'] ) {
        		return $list;
        	}

        	// Loop items
        	foreach ( $list['items'] as &$item ) {
        		// Update price and total items
        		$list['total'] += $item['quantity'];
        		$list['price'] += ( (isset($item['price_rounded']) ? $item['price_rounded'] : 1) * $item['quantity'] );
        	}

        	// Format the price
        	$list['price'] = $this->pyrocache->model('currency_m', 'format_string', array($list['price'], $currency), $this->firesale->cache_time);

        	return $list;
        }

        return false;
	}

	public function user_lists($user_id, $privacy = null, $public = null)
	{
        // Load required items
        $this->load->model('firesale/currency_m');

		// Variables
		$currency = $this->pyrocache->model('currency_m', 'get', array(), $this->firesale->cache_time);
        $params   = array(
            'stream'    => $this->stream->stream_slug,
            'namespace' => $this->stream->stream_namespace,
            'limit'     => 100,
            'offset'    => 0,
            'order_by'  => 'ordering_count',
            'sort'      => 'asc',
            'where'     => ($public !== null ? SITE_REF."_firesale_wishlists.created_by != '{$user_id}'" : SITE_REF."_firesale_wishlists.created_by = '{$user_id}'")
        );

        // Add privacy
        if ( $privacy !== null ) {
        	$params['where'] .= ' AND '.SITE_REF."_firesale_wishlists.privacy = '{$privacy}'";
        }
		
		// Add public
        if ( $public !== null ) {
        	$params['where'] .= ' AND '.SITE_REF."_firesale_wishlists.privacy = '{$public}'";
        }
		
        // Get the list
        $lists = $this->streams->entries->get_entries($params);

        // Check we have a list
        if ( $lists['total'] > 0 ) {

        	// Loop and format entries
        	foreach ( $lists['entries'] as &$list ) {

        		// Get list items
        		$list['items'] = $this->list_items($list['id']);
        		$list['total'] = 0;
        		$list['price'] = 0;

	        	// Check items
	        	if ( ! $list['items'] ) {
	        		continue;
	        	}

        		// Loop items 
        		foreach ( $list['items'] as &$item ) {
        			// Update price and total items
        			$list['total'] += $item['quantity'];
        			$list['price'] += ( (isset($item['price_rounded']) ? $item['price_rounded'] : 1) * $item['quantity'] );
        		}

        		// Format the price
        		$list['price'] = $this->pyrocache->model('currency_m', 'format_string', array($list['price'], $currency), $this->firesale->cache_time);
        	}

        	return $lists;
        }

        return false;
	}

	public function list_delete($id)
	{
		// Delete list
		if ( $this->db->where('id', $id)->delete('firesale_wishlists') ) {
			// Delete products
			return $this->db->where('row_id', $id)->delete('firesale_wishlists_firesale_products');
		}

		// Something went wrong
		return false;
	}

	public function list_item($list, $item)
	{
		// Load required items
		$this->load->model('firesale/products_m');

		// Build initial query
		$query = $this->db->select('firesale_products_id AS id, firesale_products_qty AS quantity')
						  ->where('row_id', $list)
						  ->where('firesale_wishlists_id', $this->stream->id)
						  ->where('firesale_products_id', $item)
						  ->get('firesale_wishlists_firesale_products');

		// Check for result
		if ( $query->num_rows() ) {
			
			// Get result and product
			$result  = $query->row();
			$product = $this->pyrocache->model('products_m', 'get_product', array($result->id), $this->firesale->cache_time);
			$product['quantity'] = $result->quantity;

			return $product;
		}

		// Nothing found
		return false;
	}

	public function list_items($list)
	{
		// Variables
		$currency = $this->pyrocache->model('currency_m', 'get', array(), $this->firesale->cache_time);
		$items    = array();

		// Load required items
		$this->load->model('firesale/products_m');

		// Build initial query
		$query = $this->db->select('firesale_products_id AS id, firesale_products_qty AS quantity')
						  ->where('row_id', $list)
						  ->where('firesale_wishlists_id', $this->stream->id)
						  ->get('firesale_wishlists_firesale_products');

		// Check for result
		if ( $query->num_rows() ) {

			// Loop and get products
			foreach ( $query->result_array() as $key => $result ) {
				
				// Get product
				$product = $this->pyrocache->model('products_m', 'get_product', array($result['id']), $this->firesale->cache_time);
				if(!empty($product))
				{
					$items[$key] = $product;
					// Format data
					$items[$key]['quantity']    = $result['quantity'];
					$items[$key]['parent']      = $list;
					$items[$key]['plus']        = ( $result['quantity'] + 1 );
					$items[$key]['minus']       = ( $result['quantity'] > 1 ? ( $result['quantity'] - 1 ) : 0 );
					$items[$key]['price_total'] = ( (isset($items[$key]['price_rounded']) ? $items[$key]['price_rounded'] : 1) * $result['quantity'] );
					$items[$key]['price_total'] = $this->pyrocache->model('currency_m', 'format_string', array($items[$key]['price_total'], $currency), $this->firesale->cache_time);
				}
			}

			return $items;
		}

		// Nothing found
		return false;
	}

	public function list_item_add($list, $item, $quantity)
	{
		// Variables
		$list_item = $this->list_item($list, $item);

		// Update quantity
		if ( $list_item ) {
			return $this->db->where('row_id', $list)
							->where('firesale_products_id', $item)
							->update('firesale_wishlists_firesale_products', array('firesale_products_qty' => $quantity));
		}

		// Add to list
		return $this->db->insert('firesale_wishlists_firesale_products', array(
			'row_id'                => $list,
			'firesale_wishlists_id' => $this->stream->id,
			'firesale_products_id'  => $item,
			'firesale_products_qty' => $quantity
		));
	}

	public function list_item_remove($list, $item)
	{
		return $this->db->where('row_id', $list)
						->where('firesale_wishlists_id', $this->stream->id)
						->where('firesale_products_id', $item)
						->delete('firesale_wishlists_firesale_products');
	}

	public function in_list($list, $item)
	{
		$query = $this->db->where('row_id', $list)
						  ->where('firesale_wishlists_id', $this->stream->id)
						  ->where('firesale_products_id', $item)
						  ->get('firesale_wishlists_firesale_products');

		return ( $query->num_rows() ? true : false );
	}

}

<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Firesale_discount_codes extends Module {
	
	public $version = '1.1.2';
	public $language_file = 'firesale_discount_codes/firesale';
	
	public function __construct()
	{
		parent::__construct();
		
		// Load in the FireSale library
		$this->load->library('firesale/firesale');
        $this->lang->load($this->language_file);
	}

	public function info()
	{

		$info = array(
			'name' => array(
				'en' => 'Discount Codes',
				'it' => 'Codici Sconto',
                'fr' => 'Codes de réduction'
			),
			'description' => array(
				'en' => 'Apply discounts to the shopping cart',
				'it' => 'Applica codici sconto per gli acquisti',
                'fr' => 'Appliquez des codes de réduction sur le panier'
			),
			'frontend'		=> TRUE,
			'backend'		=> TRUE,
			'firesale_core'	=> FALSE,
			'author'   => 'Chris Harvey',
			'sections' => array(
				'discounts' => array(
					'name'	=> 'fs_discount_codes:sections:discounts',
					'uri'	=> 'admin/firesale_discount_codes',
                    'shortcuts' => array(
                        array(
                            'name'  => 'fs_discount_codes:shortcuts:add',
                            'uri'   => 'admin/firesale_discount_codes/create',
                            'class' => 'add'
                        )
                    )
				)
			)
		);

		return $info;
	}

    public function admin_menu(&$menu)
    {
        $menu['lang:firesale:title']['lang:fs_discount_codes:sections:discounts'] = 'admin/firesale_discount_codes';
    }
	
	public function install()
	{
		$this->load->driver('Streams');
		
        $this->streams->streams->add_stream('Discount Codes', 'firesale_discount_codes', 'firesale_discount_codes');

        $discount_codes = $this->streams->streams->get_stream('firesale_discount_codes', 'firesale_discount_codes');
        $products = $this->streams->streams->get_stream('firesale_products', 'firesale_products');
        $categories = $this->streams->streams->get_stream('firesale_categories', 'firesale_categories');

        // Add some fields
        $fields = array(
            array(
                'name' => 'lang:fs_discount_codes:code',
                'slug' => 'code',
                'namespace' => 'firesale_discount_codes',
                'type' => 'text',
                'extra' => array('max_length' => 50),
                'assign' => 'firesale_discount_codes',
                'title_column' => TRUE,
                'required' => TRUE,
                'unique' => TRUE
            ),
            array(
                'name' => 'lang:fs_discount_codes:type',
                'slug' => 'type',
                'namespace' => 'firesale_discount_codes',
                'type' => 'choice',
                'extra' => array(
                	'choice_data' =>
                		'fixed : Fixed
                		percentage : Percentage',
                    'choice_type'   => 'dropdown',
                    'default_value' => 'percentage'
                ),
                'assign' => 'firesale_discount_codes',
                'required' => TRUE
            ),
            array(
                'name' => 'lang:fs_discount_codes:value',
                'slug' => 'value',
                'namespace' => 'firesale_discount_codes',
                'type' => 'text',
                'extra' => array(
                	'max_length' => 10,
                	'pattern' => '^\d+(?:,\d{3})*\.\d{2}$'
                ),
                'instructions' => 'lang:fs_discount_codes:value_info',
                'assign' => 'firesale_discount_codes',
                'required' => TRUE,
            ),
            array(
                'name' => 'lang:fs_discount_codes:desc',
                'slug' => 'desc',
                'namespace' => 'firesale_discount_codes',
                'type' => 'textarea',
                'assign' => 'firesale_discount_codes',
                'required' => TRUE
            ),
            array(
                'name' => 'lang:fs_discount_codes:applies_to',
                'slug' => 'applies_to',
                'namespace' => 'firesale_discount_codes',
                'type' => 'choice',
                'extra' => array(
                	'choice_data' =>
                		'0 : lang:fs_discount_codes:everything
                		1 : lang:fs_discount_codes:selected_prods
                		2 : lang:fs_discount_codes:selected_cats',
                    'choice_type'   => 'dropdown',
                    'default_value' => '0'
                ),
                'assign' => 'firesale_discount_codes',
                'required' => TRUE
            ),
            array(
                'name' => 'lang:fs_discount_codes:products',
                'slug' => 'products',
                'namespace' => 'firesale_discount_codes',
                'type' => 'multiple',
                'extra' => array(
                	'choose_stream' => $products->id
                ),
                'assign' => 'firesale_discount_codes',
                'required' => FALSE
            ),
            array(
                'name' => 'lang:fs_discount_codes:categories',
                'slug' => 'categories',
                'namespace' => 'firesale_discount_codes',
                'type' => 'multiple',
                'extra' => array(
                	'choose_stream' => $categories->id
                ),
                'assign' => 'firesale_discount_codes',
                'required' => FALSE
            ),
            array(
                'name' => 'lang:fs_discount_codes:usage',
                'slug' => 'usage',
                'namespace' => 'firesale_discount_codes',
                'type' => 'choice',
                'extra' => array(
                	'choice_data' =>
                		'once : lang:fs_discount_codes:once
                        once_user : lang:fs_discount_codes:once_user
                		multiple : lang:fs_discount_codes:multiple',
                    'choice_type'   => 'dropdown',
                    'default_value' => 'once'
                ),
                'assign' => 'firesale_discount_codes',
                'required' => TRUE
            ),
            array(
                'name' => 'lang:fs_discount_codes:start_date',
                'slug' => 'start',
                'namespace' => 'firesale_discount_codes',
                'type' => 'datetime',
                'extra' => array(
                	'use_time'   => FALSE,
                	'storage'    => 'unix',
                	'input_type' => 'datepicker'
                ),
                'assign' => 'firesale_discount_codes',
                'required' => TRUE
            ),
            array(
                'name' => 'lang:fs_discount_codes:end_date',
                'slug' => 'end',
                'namespace' => 'firesale_discount_codes',
                'type' => 'datetime',
                'extra' => array(
                	'use_time'   => FALSE,
                	'storage'    => 'unix',
                	'input_type' => 'datepicker'
                ),
                'assign' => 'firesale_discount_codes',
                'required' => TRUE
            ),
            array(
                'name' => 'lang:fs_discount_codes:used',
                'slug' => 'used',
                'namespace' => 'firesale_discount_codes',
                'type' => 'choice',
                'extra' => array(
                    'choice_data' =>
                        'y : lang:fs_discount_codes:yes
                        n : lang:fs_discount_codes:no',
                    'choice_type'   => 'radio',
                    'default_value' => 'n'
                ),
                'assign' => 'firesale_discount_codes',
                'required' => FALSE
            )
        );

        $this->streams->fields->add_fields($fields);

        $code_used = array(
            'name'          => 'lang:fs_discount_codes:code_used',
            'slug'          => 'code_used',
            'namespace'     => 'firesale_orders',
            'type'          => 'relationship',
            'extra'         => array('choose_stream' => $discount_codes->id),
            'assign'        => 'firesale_orders',
            'required'      => FALSE
        );

        $this->streams->fields->add_field($code_used);
		
		return TRUE;
	}

	public function uninstall()
	{
        // Drop stream
		$this->streams->utilities->remove_namespace('firesale_discount_codes');

        // Drop multiple-created tables
        $this->dbforge->drop_table('firesale_discount_codes_firesale_categories');
        $this->dbforge->drop_table('firesale_discount_codes_firesale_products');

        // Remove the field from orders
        $this->streams->fields->delete_field('code_used', 'firesale_orders');
        
		return TRUE;
	}

	public function upgrade($old_version)
	{
        switch ($old_version)
        {
            case '0.9.8':
                $this->streams->fields->delete_field('code_used', 'firesale_orders');

                $discount_codes = $this->streams->streams->get_stream('firesale_discount_codes', 'firesale_discount_codes');

                $code_used = array(
                    'name'          => 'lang:fs_discount_codes:code_used',
                    'slug'          => 'code_used',
                    'namespace'     => 'firesale_orders',
                    'type'          => 'relationship',
                    'extra'         => array('choose_stream' => $discount_codes->id),
                    'assign'        => 'firesale_orders',
                    'required'      => FALSE
                );

                $this->streams->fields->add_field($code_used);
            break;
        }
		return TRUE;
	}

	public function help()
	{

		return "Some Help Stuff";
	}

}

$(document).ready(function()
{
	// activamos los multiselects para los años
	$(".multiSelect").multiselect({ 
	   header: "Máximo 3 años",
	   selectedList:3,
	   click: function(e)
	   {
	   		chekeado = $(this).multiselect("widget").find("input:checked").length;
	       if( chekeado > 3 || chekeado < 2)
	       {
	           return false;
	       }
	   }
	});
	
	// activamos los multiselects para los vendedores
	$(".multiSelectNoData").multiselect({
	   header: "Máx. 3 Vendedores",
	   click: function(e)
	   {
	   		chekeado = $(this).multiselect("widget").find("input:checked").length;
	       if( chekeado > 3 || chekeado < 1)
	       {
	           return false;
	       }
	   }
	});
	
});
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><!--<i class="icon-heart color"></i>--> Vista Rapida</h3>
</div>

<div class="modal-body">
    <div class="product-main bkg-delta clearfix">

        <div class="half prod-slide" id="prod-slide">
            <!-- Image -->
            <div class='swipe-wrap'>
                {{ if images }}
                {{ images }}
                <li id="image-{{ id }}" class="slider-item" style="display: block;">
                    <img src="{{ url:site }}files/thumb/{{ id }}/464/464/fit" alt="{{ product.title }} {{ key }}"/>
                </li>
                {{ /images }}
                {{ else }}
                <li class="slider-item" style="display: block;">{{ theme:image file="no-image.png" alt=title }}</li>
                {{ endif }}     
            </div>

            <div class="slide-btn">
                <a onclick='mySwipeProd.prev()' class="btn-prev"></a> 
                <a onclick='mySwipeProd.next()' class="btn-next"></a>
            </div>

        </div>
        <div class="half">
			<div class="inner-box">
            	<strong class="main-font product-name">{{ product.title }}</strong>

                <h5>Nuestro Precio : <p class="price main-font">{{ product.price_formatted }}</p></h5>
                <p>RRP : <span class="rrp">{{ product.rrp_formatted }}</span></p>             
                {{ if product.code }}
                <p class="prodid"><?php echo lang('firesale:product:label_product_code'); ?> : <span>{{ product.code }}</span></p>
                {{ endif }}
                {{ if product.brand.title }}
                <p>Marca : <a href="{{ firesale:url route='brand' id=product.brand.id }}">{{ product.brand.title }}</a></p>
                {{ endif }}
                {{ if product.stock_status.value }}
                <p class="availability"><?php echo lang('firesale:product:label_availability'); ?> : <span>{{ product.stock_status.value }} ({{ product.stock }})</span></p>
                {{ endif }}


                {{ firesale:modifier_form type="select" product=product.id }}         

                {{ if theme:options:add_this_pubid }}
                {{ if theme:options:add_this_html }}

                {{ theme:options:add_this_html }}

                {{ if theme:options:add_this_track == "yes "}}
                <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                {{ endif }}

                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid={{ theme:options:add_this_pubid }}"></script>

                {{ endif }}
                {{ endif }}
			</div>

        </div>

    </div>
</div>

<div class="modal-footer"></div>

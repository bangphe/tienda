<?php if( !$reviewed ): ?>
<?php echo form_open('/reviews/create/' . $product['id'], 'class="crud"'); ?>

    <ul class="fields">
        <li><h3 class="form"><?php echo sprintf(lang('firesale:reviews:form_title'), $product['title']); ?></h3></li>
<?php foreach( $fields AS $field ): ?>
        <li class="<?php echo $field['odd_even']; ?>">
            <label for="name"><?php echo sprintf(lang($field['input_title']), $field['value']); ?>:</label>
            <?php echo $field['input']; ?>
        </li>
<?php endforeach; ?>
        <li class="<?php ( $field['odd_even'] == 'even' ? 'odd' : 'even' ); ?>">
            <label>&nbsp;</label>
            <button type="submit" name="btnAction" value="create" class="btn"><span><?php echo lang('firesale:reviews:form_submit'); ?></span></button>
        </li>
    </ul>
    <br class="clear" />

<?php echo form_close(); ?>
<?php endif;

<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6 no-js" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7 no-js" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="en"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<!-- html5.js for IE less than 9 -->
<!--[if lt IE 9]>  <script src="assets/js/lib/html5.js"></script>  <![endif]-->
<html>
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta http-equiv="content-language" content="es" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title>Imaginamos.com / e-commerce</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="copyright" content="imaginamos.com" />
  <meta name="date" content="2014" />
  <meta name="author" content="diseño web: imaginamos.com" />
  <meta name="robots" content="All" />
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <link rel="author" type="text/plain" href="humans.txt" />
  <!--Style's-->
  <link href="assets/css/tienda.css" rel="stylesheet" type="text/css" />
  <!--Librerias-->
  <script src="assets/js/lib/modernizr.min.js"></script>
</head>

<body>

<!-- <section id="preload">
    <div id="preload-container">
      <div id="dot"></div>
        <div class="step" id="s1"></div>
        <div class="step" id="s2"></div>
        <div class="step" id="s3"></div>
    </div>
</section> -->

<?php include("nav_responsive.php"); ?>  

<section class="content_all">

  <?php include("header.php"); ?> 

  <!--Contenidos Sitio-->
  <section class="barra_int"></section>
  <section class="cont_home">   

      <!--Miga de pan-->
      <div class="bg_miga">
        <ul class="marco miga_pan">
          <a href=""><i class="fa fa-angle-double-right"></i>Inicio</a>
          <a href=""><i class="fa fa-angle-double-right"></i>Hombres</a>
          <a href=""><i class="fa fa-angle-double-right"></i>Chaquetas</a>
          <a href=""><i class="fa fa-angle-double-right"></i>Chaquetas de cuero</a>
          <a href=""><i class="fa fa-angle-double-right"></i>Chaquetas doble fast</a>
        </ul>
          <div class="clear"></div>
      </div>    
      <!--Fin Miga de pan-->

      <div class="clear"></div>

      <!--Controles Productos-->
      <div class="controls_top marco">        
        <a href="" class="atras"><i class="fa fa-angle-left"></i> Producto anterior</a>
          <h2>Chaqueta Doble Faz</h2>
        <a href="" class="siguiente">Producto siguiente <i class="fa fa-angle-right"></i></a>
      </div>
      <!--Fin Controles Productos-->

      <div class="clear"></div>

      <!--Detalle Producto-->
      <div class="bg_detalle_product">
        <div class="marco elem_negros bounceInUp animated">
           <!--Detalle-->
           <div class="large-6 medium-6 small-12 columns datos_detalle">
               <h3>Ref. 3309/137</h3>
               <h1>CHAQUETA DOBLE FAZ</h1>
               <h1>$1’050.000</h1>                        
               <div class="row bg_tallas">
                  <h2>TALLA</h2>
                  <ul class="tallas">
                    <li>
                      <a href="javascript:void(0)" class="activo_talla">XS</a>  
                    </li>
                    <li>
                      <a href="javascript:void(0)">S</a>  
                    </li>
                    <li>
                      <a href="javascript:void(0)">M</a>  
                    </li>
                    <li>
                      <a href="javascript:void(0)">L</a>  
                    </li>
                    <li>
                      <a href="javascript:void(0)">XL</a>  
                    </li>
                    <!-- <div class="text-ayuda ">
                       Por favor, selecciona tu talla
                     </div> -->
                      <div class="clear"></div>
                  </ul>
               </div>
               <div class="row bg_colors">             
                  <div class="large-10 medium-10 small-10 columns">
                    <h2>COLOR</h2>
                    <ul class="colores">                
                        <a href="javascript:void(0)" class="cafe"></a>   
                        <a href="javascript:void(0)" class="negro"></a>  
                        <a href="javascript:void(0)" class="azul"></a>                  
                        <a href="javascript:void(0)" class="gris"></a>                  
                        <div class="clear"></div>
                        <!--Algunos los colores:
                          Cafe, Negro, Azul, Gris, Rojo, Verde, Amarillo, Naranja, ect
                        -->
                    </ul>
                  </div>
                  <div class="large-2 medium-2 small-2 columns item2">
                    <h2>TALLA</h2>
                    <select name="talla">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>
               </div>

               <!--Tab Comentario -->
               <section class="tab_detalle">
                 <a href="#descripcion_pro" class="active_detalle">DESCRIPCIÓN</a>
                 <a href="#comment_pro">COMENTARIOS</a>
                 <div class="btn_blanco comentar"><i class="fa fa-pencil"></i> COMENTAR</div>
                   <div class="clear"></div>
               </section>   
               <section class="descrip cont_tab_det" id="descripcion_pro">
                 <p><span>El modelo mide 6.20 ft / 189 cm y lleva la talla L.</span></p>
                 <p>Nec elit eleifend augue posuere sollicitudin vitae vel varius blandit cras porta porta felis, non elementum nunc fringilla consectetur vitae praesent ut odio sodales, fermentum massa sed lacinia arcu.</p>
               </section>
               <section class="coment cont_tab_det" id="comment_pro">
                  <ul class="comnet_user">
                     <li class="item">
                        <h3>Maecenas convallis fringilla libero sed dapibus cras at tristique</h3>
                        <p><strong>Por: Fabián Blanco</strong> <span>05/092014, 14:30 PM</span></p>
                        <div class="ranting">
                           <div class="estrellas-rating">
                             <input class="star required" type="radio" name="ranting_user1" value="1"/>
                             <input class="star" type="radio" name="ranting_user1" value="2"/>
                             <input class="star" type="radio" name="ranting_user1" value="3" checked="checked"/>
                             <input class="star" type="radio" name="ranting_user1" value="4"/>
                             <input class="star" type="radio" name="ranting_user1" value="5"/>
                          </div>
                        </div>
                        <p>Quisque accumsan risus et leo efficitur, quis finibus nulla commodo. Aenean lobortis eget leo mattis pharetra. Suspendisse potenti. Aliquam augue velit, blandit ut metus et, posuere malesuada nulla. Phasellus mollis eros purus. Nam convallis volutpat suscipit. Praesent </p>
                     </li>
                     <li class="item">
                        <h3>Maecenas convallis fringilla libero sed dapibus cras at tristique</h3>
                        <p><strong>Por: Fabián Blanco</strong> <span>05/092014, 14:30 PM</span></p>
                        <div class="ranting">
                           <div class="estrellas-rating">
                             <input class="star required" type="radio" name="ranting_user2" value="1"/>
                             <input class="star" type="radio" name="ranting_user2" value="2"/>
                             <input class="star" type="radio" name="ranting_user2" value="3" checked="checked"/>
                             <input class="star" type="radio" name="ranting_user2" value="4"/>
                             <input class="star" type="radio" name="ranting_user2" value="5"/>
                          </div>
                        </div>
                        <p>Quisque accumsan risus et leo efficitur, quis finibus nulla commodo. Aenean lobortis eget leo mattis pharetra. Suspendisse potenti. Aliquam augue velit, blandit ut metus et, posuere malesuada nulla. Phasellus mollis eros purus. Nam convallis volutpat suscipit. Praesent </p>
                     </li>
                     <li class="item">
                        <h3>Maecenas convallis fringilla libero sed dapibus cras at tristique</h3>
                        <p><strong>Por: Fabián Blanco</strong> <span>05/092014, 14:30 PM</span></p>
                        <div class="ranting">
                           <div class="estrellas-rating">
                             <input class="star required" type="radio" name="ranting_user3" value="1"/>
                             <input class="star" type="radio" name="ranting_user3" value="2"/>
                             <input class="star" type="radio" name="ranting_user3" value="3" checked="checked"/>
                             <input class="star" type="radio" name="ranting_user3" value="4"/>
                             <input class="star" type="radio" name="ranting_user3" value="5"/>
                          </div>
                        </div>
                        <p>Quisque accumsan risus et leo efficitur, quis finibus nulla commodo. Aenean lobortis eget leo mattis pharetra. Suspendisse potenti. Aliquam augue velit, blandit ut metus et, posuere malesuada nulla. Phasellus mollis eros purus. Nam convallis volutpat suscipit. Praesent </p>
                     </li>
                  </ul>
               </section>
               <section class="form_cometar elem_negros">
                  <p></p>
                  <form action="" method="get">
                      <fieldset class="fila large-9 medium-9 columns item1">
                         <input type="text" name="mail" placeholder="Titulo">
                      </fieldset>
                      <fieldset class="fila large-3 medium-3 columns item2">
                         <select class="calific_rant"  placeholder="Rating">
                            <option value="0">Rating</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                          </select>
                      </fieldset>
                      <fieldset class="fila large-12 medium-12 columns">
                         <textarea id="ccomment" name="comment" placeholder="Comentario" cols="22"></textarea>            
                      </fieldset>
                        <div class="clear"></div>

                      <buttom type="sumbit" class="btn_blanco">SUSCRIBIRME</buttom>  
                  </form>
                   <div class="clear"></div>
               </section>
               <!-- Fin Tab Comentario -->

               <div class="row btns_red_agreg">
                  <div class="large-4  medium-4 small-12 columns">
                      <a href="" class="btn_border_blan btn_agregar">AGREGAR AL CARRITO</a>
                  </div>    
                  <div class="large-8 medium-8 small-12 columns">          
                    <div class="redes">
                      <a class="face" href="" target="_blank"><i class="fa fa-facebook"></i></a>            
                      <a class="twitter" href="" target="_blank"><i class="fa fa-twitter"></i></a>
                      <a class="google" href="" target="_blank"><i class="fa fa-google-plus"></i></a>
                      <a class="linkedin" href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                      <a class="intagram" href="" target="_blank"><i class="fa fa-instagram"></i></a>
                    </div>  
                  </div>           
               </div>
                 <div class="clear"></div>
           </div>
           <!--Fin Detalle-->

           <!--Fin Detalle Galeria-->            
           <div class="large-6 medium-6 small-12 columns galerias galeria_detalle item2">
              <!--Galeria-->
              <div class="galeria_detalle_produc columns">
                  <div class="imagen_grande_produc zoom">
                      <img src="assets/images/galeria/img1.jpg" alt="">
                  </div>
                  <div class="imagenes_peq_produc gal-prod clearfix">
                   <ul class="gal-prod">
                    <li>
                      <img src="assets/images/galeria/img1.jpg" alt="">
                    </li>
                    <li>
                      <img src="assets/images/galeria/img2.png" alt="">
                    </li>
                    <li>
                      <img src="assets/images/galeria/img1.png" alt="">
                    </li>
                    <li>  
                      <img src="assets/images/galeria/img4.png" alt="">
                    </li>
                    <li>  
                      <img src="assets/images/galeria/img5.png" alt="">
                    </li>  
                   </ul>  
                  </div>
              </div>    
              <!--Fin Galeria-->
           </div>
           <!--Fin Detalle Galeria-->
        </div>

        <!--Next y Pev Proyect-->
        <a href="" class="next_proyectW">
          <div class="img_pyW"><img src="assets/images/preview_product1.png" alt="">
            <span>siguiente PRODUCTO</span>
          </div>
        </a>
        <a href="" class="prev_proyectW">
          <div class="img_pyW"><img src="assets/images/preview_product1.png" alt="">
            <span>ANTERIOR PRODUCTO</span>
          </div>
        </a>
        <!--Fin Next y Pev Proyect-->

      </div>
      <!--Fin Detalle Producto-->

        <div class="clear"></div>

      <!--Productos destacados-->
      <section class="bg_destacados">
        <div class="marco">
          <div class="subtittle">
              <h1>PRODUCTOS DESTACADOS</h1>
          </div>          
          <ul class="destacados"> 
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.5s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <img src="assets/images/productos/5.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a>
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.6s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <img src="assets/images/productos/6.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a> 
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.7s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <img src="assets/images/productos/7.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a> 
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.8s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <img src="assets/images/productos/8.jpg" alt="">
                    <span class="etiq_desc">50%<i>dscto</i></span>
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio precio_ant">$120.000</div>
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a> 
            </li>
          </ul> 
        </div>  
      </section>   
      <!--Fim Productos destacados-->  

  </section>
  <!--Fin Contenidos Sitio-->

    <div class="clear"></div>

  <?php include("footer.php"); ?> 

</section><!--Content_all-->

</body>
</html>

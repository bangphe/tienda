<script>
var currency = "<?php echo $this->fs_cart->currency()->symbol; ?>";
</script>

      <div class="tittles marco">
        <h1>CAJA </h1>
      </div>
      <!--Fin Titulo-->

      <div class="clear"></div>

        {{ if ! user:logged_in }}
        <!--<section class="bg_pasos_comp marco">
            <h3><span class="login_h"><i class="fa fa-thumbs-o-up"></i> Haz <a href="javascript:void(0)">clic aquí!</a> para ingresar a tu cuenta.</span></h3>
        </section>-->


        <section class="cont_home">
            <section class="marco">
                <div style="margin: 0 -15px;">
                    <div class="large-6 medium-4 small-12 columns" style="padding:0 15px">
                    
                         <!--Titulo-->
                        <div class="subtittle titulo_int animated fadeInDown">
                            <h1>Accede a tu Cuenta</h1>
                        </div>
                          <div class="clear"></div>
                        <section class="cont_nosotros">
                                
                            <div class="bg_log_v elem_blancos animated fadeInUp">
                             <?php if (validation_errors()): ?>
                                <div class="alert-error">
                                    <i class="fa fa-warning animated zoomIn"></i>
                                  <?php echo validation_errors();?>
                                </div>
                                <?php endif ?>

                               <?php echo form_open('users/login', array('class'=>'user_form form-horizontal'), array('redirect_to' => 'cart/checkout')) ?>

                                  <fieldset>
                                    <label for="email" class="col-sm-2 control-label"><?php echo lang('global:email') ?></label>                   
                                      <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Email"> -->
                                      <?php echo form_input('email', $this->input->post('email') ? $this->input->post('email') : '', 'class="form-control" placeholder="Correo electrónico"')?>
                                  </fieldset>
                                  <fieldset>
                                    <label for="password" class="col-sm-2 control-label"><?php echo lang('global:password') ?></label>                 
                                    <input type="password" class="form-control" id="password" placeholder="Contraseña" name="password" maxlength="20">
                                  </fieldset>
                                  <fieldset>
                                  <div class="confirm checkbox">
                                    <?php echo form_checkbox('remember', '1', false, 'id="remember"') ?>
                                    <label for="remember">
                                       Recordar contraseña.
                                    </label>
                                  </div>
                                  </fieldset>
                                  <fieldset>
                                    <div class="text-center">
                                      <button type="submit" class="btn_gris" value="Iniciar Sesión" name="btnLogin">Iniciar Sesión</button>
                                    </div>
                                    <div class="reset_pass bg_pasos_comp">
                                        <h3 style="margin-top: 5px;"><span><i class="fa fa-thumbs-o-up"></i>
                                        <?php echo anchor('users/reset_pass', lang('user:reset_password_link'));?>
                                     </div>
                                  </fieldset>
                                  
                               <?php echo form_close() ?>    
                            </div>
                        </section>
                    </div>
                
                    <div class="large-6 medium-4 small-12 columns" style="padding: 0 15px;">
                        <!--Titulo-->
                        <div class="subtittle titulo_int animated fadeInDown">
                            <h1>Registrar nueva cuenta</h1>
                        </div>
                          <div class="clear"></div>
                        <section class="cont_nosotros">
                                
                            <div class="bg_log_v elem_blancos animated fadeInUp">
                              
                              <?php if ( ! empty($error_string)):?>
                                <div class="alert-error">
                                  <?php echo $error_string;?>
                                </div>
                                <?php endif;?>

                                <?php echo form_open('register', array('class'=>'user_form form-horizontal')) ?>       
                                   <?php if ( ! Settings::get('auto_username')): ?>
                                  <fieldset>
                                    <label class="col-sm-3 control-label" for="username"><?php echo lang('user:username', 'placeholder="btn btn-primary"') ?></label>                    
                                      <input type="text" name="username" maxlength="100" value="<?php echo $_user->username ?>" placeholder="<?php echo $_user->username ?>"  class="form-control" />                    
                                  </fieldset>
                                  <?php endif ?>
                                  <fieldset>
                                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo lang('global:email') ?></label>                    
                                      <input type="text" name="email" maxlength="100" value="<?php echo $_user->email ?>" placeholder="Correo electrónico" class="form-control"/>                    
                                    <?php echo form_input('d0ntf1llth1s1n', ' ', 'class="default-form" style="display:none"') ?>
                                  </fieldset>
                                  <fieldset>
                                    <label for="password" class="col-sm-3 control-label"><?php echo lang('global:password') ?></label>                    
                                      <input type="password" class="form-control" id="password" placeholder="Contraseña" name="password" maxlength="100">                    
                                  </fieldset>
                                  
                                  <?php foreach($profile_fields as $field) { if($field['required'] and $field['field_slug'] != 'display_name' ) { ?>
                                  <fieldset>
                                    <label class="col-sm-3 control-label" for="<?php echo $field['field_slug'] ?>" ><?php echo (lang($field['field_name'])) ? lang($field['field_name']) : $field['field_name'];  ?></label>
                                    <div class="col-sm-9" ><?php echo $field['input']?></div>
                                  </fieldset>
                                  <?php } } ?>
                                  <div class="confirm checkbox">
                                    <input type="checkbox" id="termCondnewsletter" name="termCondnewsletter"/>
                                    <label for="termCondnewsletter">Acepto <a href="{{ url:site }}{{terms_cond}}" data-effect="mfp-move-horizontal" target="_blank">Términos y Condiciones.</a>
                                    </label>
                                  </div> 

                                  <fieldset>
                                    <div class="text-center">
                                      <button type="submit" class="btn_gris" value="<?php echo lang('user:register_btn'); ?>"  name="btnSubmit">Regístrate</button>
                                    </div>
                                  </fieldset>
                                <?php echo form_close() ?> 
                                <br>

                            </div>

                        </section>
                    </div>
                     
                </div>
            </section><!--Marco-->  
        </section>
        {{ else }}
        <section class="bg_pasos_comp marco">
            <div class="elem_blancos">
        <?php echo form_open('', 'class="checkout" id="luchito"'); ?>

        <!-- Billing (and shipping) Address -->
        <article class="col1_p large-6 medium-6 small-12 columns relative item1">
        <div class="half bkg-delta">
            <div class="form inner-box billing_address">
                <h3 ><i class="icon-list"></i> {{ helper:count identifier="checkout_steps" }}. Detalles de facturación</h2>
                <fieldset class="checkout_addresses">

                    <!-- Saved Billing Addresses -->
                    <?php if (isset($addresses) && !empty($addresses)): ?>
                    <div class="control-group form-group">
                        <div class="row">
                            <div class="controls col-sm-12 input_b">

                                <select name="bill_to">
                                    <?php foreach ($addresses AS $key => $address): ?>
                                    <option value="<?php echo $address['id']; ?>" <?php echo ( ( isset($_POST['bill_to']) && $_POST['bill_to'] == $address['id'] ) || $key == 0 ? ' selected="selected"' : '' ); ?>><?php echo $address['title']; ?></option>
                                <?php endforeach; ?>
                                <option value="new" <?php echo ( isset($_POST['bill_to']) && $_POST['bill_to'] == 'new' ? ' selected="selected"' : '' ); ?>>Nueva dirección</option>
                            </select>

                            <div class="saved_bill_addresses">
                                <?php foreach ($addresses AS $key => $address): ?>
                                <div class="saved_address address_<?php echo $address['id']; ?>">

                                    <?php echo ( isset($address['firstname']) ? $address['firstname'] . ' ' . $address['lastname'] . '</br>' : '' ); ?>
                                    <?php echo ( isset($address['cedule']) ? $address['cedule'] . '</br>' : '' ); ?>
                                    <?php echo ( isset($address['address1']) ? $address['address1'] . '</br>' : '' ); ?>
                                    <?php echo ( isset($address['email']) ? $address['email'] . '</br>' : '' ); ?>
                                    <?php echo ( isset($address['phone']) ? $address['phone'] . '</br>' : '' ); ?>
                                    <?php echo ( isset($address['id_deparment']['name']) ? $address['id_deparment']['name'] . '</br>' : '' ); ?>
                                    <?php echo ( isset($address['id_city']['name']) ? $address['id_city']['name'] . '</br>' : '' ); ?>
									<?php echo ( isset($address['observation']) ? $address['observation'] . '</br>' : '' ); ?>
                                </div>
                            <?php endforeach; ?>
                        </div>

                    </div>
                </div>
            </div>
        <?php elseif ($ship_req): ?>
        <div class="no_select"></div>
    <?php endif; ?>

    <!-- new billing address -->
    <?php if (!$this->current_user->id or ( $this->current_user->id and (!isset($addresses) or empty($addresses) ) )): ?>
    <div class="new_billing_address">
        <input name="bill_to" type="hidden" value="new" />
    <?php else: ?>
    <div class="new_billing_address">
    <?php endif; ?> 

    <p>Los campos marcados con <span class="asterix">*</span> son requeridos.</p>

    <?php 
                                /*$bill_fields['firesale:label_postcode'][0]['required'] = '';
                                $bill_fields['firesale:label_phone'][0]['required'] = '<span>*</span>';
                                 // quitamos los que no se van a usar
                                unset($bill_fields['firesale:label_address2']);
                                unset($bill_fields['firesale:label_postcode']);
                                unset($bill_fields['firesale:label_country']);*/
                                /* quitamos los que no se van a usar*/
                                unset($bill_fields['firesale:label_company']);
                                unset($bill_fields['firesale:label_address2']);
                                unset($bill_fields['firesale:label_postcode']);
                                unset($bill_fields['firesale:label_country']);
                                foreach ($bill_fields AS $subtitle => $section): ?>
                                <?php foreach ($section AS $field): ?>
                                <div class="control-group form-group <?php echo ( $field['input_slug'] == "title" && !$this->current_user->id ? ' hide' : '' ); ?>">
                                    <div class="controls col-sm-10 input_b">
                                        <label for="bill_<?php echo $field['input_slug']; ?>"><?php echo lang(substr($field['input_title'], 5)); ?> <?php echo $field['required']; ?></label>
                                        <?php if($field['input_slug'] == 'id_city')
                                        {
                                            echo '<div id="select_city_ajax">';
                                            echo $field['input'];
                                            echo '</div>';
                                        }
                                        else
                                        {
                                            echo $field['input'];
                                        }
                                        ?>
                                        <?php echo ( form_error($field['input_slug']) ? '<div class="clearfix"></div><div class="alert-error">' . form_error($field['input_slug']) . '</div>' : '' ); ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>

                </fieldset>
            </div>
        </div>
    </article>
    <article class="col1_p large-6 medium-6 small-12 columns relative item1">
        <?php if ($ship_req): ?>
        <div class="half bkg-delta">
            <!-- Shipping Address -->
            <div class="form inner-box shipping_address">
                <h3 ><i class="icon-home"></i> {{ helper:count identifier="checkout_steps" }}. Detalles de Envío</h3>

                <fieldset class="checkout_addresses">
                    <!-- Saved Shipping Addresses -->
                    <div class="control-group form-group">
                        <div class="row">
                            <div class="controls col-sm-12 input_b">
                                <select name="ship_to">
                                    <option value="same_as_billing" <?php echo ( isset($_POST['ship_to']) && $_POST['ship_to'] == 'same_as_billing' ? ' selected="selected"' : '' ); ?>>Igual que la dirección de facturación</option>
                                    <option value="new" <?php echo ( isset($_POST['ship_to']) && $_POST['ship_to'] == 'new' ? ' selected="selected"' : '' ); ?>>Nueva dirección</option>
                                    <?php if (isset($addresses) && !empty($addresses)): ?>
                                    <?php foreach ($addresses AS $key => $address): ?>
                                    <?php if (isset($_POST['ship_to']) && $_POST['ship_to'] == $address['id']): ?>
                                    <option value="<?php echo $address['id']; ?>" selected="selected"><?php echo $address['title']; ?></option>    
                                <?php elseif ($key == 0 && $_POST['ship_to'] != 'same_as_billing' && $_POST['ship_to'] != 'new'): ?>
                                <option value="<?php echo $address['id']; ?>" selected="selected"><?php echo $address['title']; ?></option>        
                            <?php else: ?>
                            <option value="<?php echo $address['id']; ?>"><?php echo $address['title']; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
        </div>
    </div>
</div>



<?php if (isset($addresses) && !empty($addresses)): ?>
    <div class="saved_ship_addresses">
        <?php foreach ($addresses AS $key => $address): ?>
        <div class="saved_address address_<?php echo $address['id']; ?>">
			<?php echo ( isset($address['firstname']) ? $address['firstname'] . ' ' . $address['lastname'] . '</br>' : '' ); ?>
            <?php echo ( isset($address['cedule']) ? $address['cedule'] . '</br>' : '' ); ?>
            <?php echo ( isset($address['address1']) ? $address['address1'] . '</br>' : '' ); ?>
            <?php echo ( isset($address['email']) ? $address['email'] . '</br>' : '' ); ?>
            <?php echo ( isset($address['phone']) ? $address['phone'] . '</br>' : '' ); ?>
            <?php echo ( isset($address['id_deparment']['name']) ? $address['id_deparment']['name'] . '</br>' : '' ); ?>
            <?php echo ( isset($address['id_city']['name']) ? $address['id_city']['name'] . '</br>' : '' ); ?>
			<?php echo ( isset($address['observation']) ? $address['observation'] . '</br>' : '' ); ?>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?> 

<!-- new shipping address -->
<?php if ($this->current_user->id && (!isset($addresses) || empty($addresses))): ?>
    <div class="new_shipping_address">
    <?php elseif ($this->current_user->id): ?>
    <div class="new_shipping_address">
    <?php else: ?>
    <div class="new_shipping_address">
    <?php endif; ?> 

    <p>Los campos marcados con <span class="asterix">*</span> son requeridos.</p>

    <?php 
    /* quitamos los que no se van a usar*/
    unset($ship_fields['firesale:label_company']);
    unset($ship_fields['firesale:label_address2']);
    unset($ship_fields['firesale:label_postcode']);
    unset($ship_fields['firesale:label_country']);
    foreach ($ship_fields AS $subtitle => $section): ?>
    <?php foreach ($section AS $field): ?>
    <?php if ($field['input_slug'] == "title" && !$this->current_user->id): ?>
    <div class="control-group">
    <?php else: ?>
    <div class="control-group form-group">
    <?php endif; ?>
    <div class="controls col-sm-10 input_b">
        <label for="ship_<?php echo $field['input_slug']; ?>"><?php echo lang(substr($field['input_title'], 5)); ?> <?php echo $field['required']; ?></label>
        <?php if($field['input_slug'] == 'id_city')
        {
            echo '<div id="select_city_ajax_ship">';
            echo $field['input'];
            echo '</div>';
        }
        else
        {
            echo $field['input'];
        }?>
        <?php echo ( form_error($field['input_slug']) ? '<div class="clearfix"></div><div class="alert-error">' . form_error($field['input_slug']) . '</div>' : '' ); ?>
    </div>
</div>
<?php endforeach; ?>

<?php endforeach; ?>
</div>

</fieldset>

</div>
</div>
<?php endif; ?>
</article>
<article class="col1_p large-6 medium-6 small-12 columns relative item1">
<!-- Shipping Methods -->
<div class="half bkg-delta">

    <div class="form inner-box<?php echo (!$valid_shipping ? ' error' : '' ); ?>">
        <h3><i class="icon-truck"></i> {{ helper:count identifier="checkout_steps" }}. <?php echo lang('firesale:checkout:title:ship_method'); ?></h3>

        <?php if ($ship_req && isset($shipping) && is_array($shipping)): ?>

        <fieldset>

            <?php if (count($shipping) > 1): ?>
            <p><?php echo lang('firesale:checkout:select_shipping_method'); ?></p>
        <?php endif; ?>

        <ul class="shipping-methods">
            <?php foreach ($shipping AS $key => $option): ?>
            <div class="radiobtn">
            <li data-cost="<?php echo $option['price']; ?>">
                <label for="shipping_<?php echo $key; ?>"><div class="option_title"><strong><?php echo $option['title']; ?></strong> - {{ settings:currency }}<?php echo number_format($option['price']); ?></div><p><?php echo $option['description']; ?></p></label>
                <?php if ($_POST['shipping'] == $option['id']): ?>
                <input type="radio" name="shipping" id="shipping_<?php echo $key; ?>" value="<?php echo $option['id']; ?>" checked="checked" />
            <?php elseif ($key == 0 && (!isset($_POST['shipping']) OR empty($_POST['shipping']) )): ?>
            <input type="radio" name="shipping" id="shipping_<?php echo $key; ?>" value="<?php echo $option['id']; ?>" checked="checked" />                        
        <?php else: ?>
        <input type="radio" name="shipping" id="shipping_<?php echo $key; ?>" value="<?php echo $option['id']; ?>" />
    <?php endif; ?>
</li>
</div>
<?php endforeach; ?>
</ul>

</fieldset>

<?php elseif ($ship_req && isset($shipping) && (0 + $shipping ) > 0): ?>
    <input type="hidden" name="shipping" value="<?php echo $shipping; ?>" />
<?php endif; ?>
</div>

</div>
</article>
<!-- Payment Gateways -->
<?php if (count($this->gateways->get_enabled()) > 1): ?>
    <div class="half bkg-delta">
    <?php else: ?>
    <div class="span6">
    <?php endif; ?>
    <div class="form inner-box<?php echo (!$valid_gateway ? ' error' : '' ); ?>">
        <h3><i class="icon-credit-card"></i> {{ helper:count identifier="checkout_steps" }}. <?php echo lang('firesale:checkout:title:payment_method'); ?></h3>
        <fieldset>
            <p><?php echo lang('firesale:checkout:select_payment_method'); ?></p>

            <ul class="payment-gateways">
                <?php $gateway_count = 0; ?>
                <?php foreach ($this->gateways->get_enabled() as $gateway_id => $gateway): ?>
                <div class="radiobtn">
                <li>
                    <label for="gateway_<?php echo $gateway_id; ?>">
                        <strong><?php echo $gateway['name']; ?></strong>
                        <span class="gateway_<?php echo $gateway['slug']; ?>"></span>
                    </label>
                    <?php if ($_POST['gateway'] == $gateway_id): ?>
                    <input type="radio" name="gateway" id="gateway_<?php echo $gateway_id; ?>" value="<?php echo $gateway_id; ?>" checked="checked" />
                <?php elseif ($gateway_count == 0 && (!isset($_POST['gateway']) OR empty($_POST['gateway']) )): ?>
                <input type="radio" name="gateway" id="gateway_<?php echo $gateway_id; ?>" value="<?php echo $gateway_id; ?>" checked="checked" />
            <?php else: ?>
            <input type="radio" name="gateway" id="gateway_<?php echo $gateway_id; ?>" value="<?php echo $gateway_id; ?>" <?php echo set_radio('gateway', $gateway_id); ?> />
        <?php endif; ?>
    </li>
</div>  
    <?php $gateway_count++; ?>
<?php endforeach; ?>
</ul>
</fieldset>
</div>

</div>

<div class="clear"></div>
<!-- Cart -->

<div class="clear"></div>
    </div>
 </section> 


<section class="bg_carrito">

          <div class="tittles marco">

            <h1>REVISIÓN DEL CARRITO ({{ firesale:cart }}{{ count }}{{ /firesale:cart }})</h1>

          </div>

          <!--Contenidos Sitio-->

          <section class="cont_produt marco">

            <!--Lista Carrito-->

            <article class="large-8 medium-8 small-12 columns ">

              <div class="caja_borde">

                 <ul class="list_carrito">
                  {{ firesale:cart }}
                     {{ products }}  
                     <li class="product" data-cost="{{ price }}" data-queantity="{{ quantity }}">
                       <a href="{{ firesale:url route='cart' }}/remove/{{ rowid }}" class="delete_prd" type="">x</a>
                       <div class="large-2 medium-2 small-4 columns">
                         {{ if image }}
                          <a href="{{ if parent }}{{ firesale:url route='product' id=parent }}{{ else }}{{ firesale:url route='product' id=id }}{{ endif }}" title="View {{ name }}"><img src="{{ url:site }}files/large/{{ image }}" alt=" {{ name }}"/></a>
                          {{ else }}
                          <div class="cart-left">{{ theme:image file="no-image.png" alt=title }}</div>
                          {{ endif }}
                       </div>
                       <div class="detalle_prod_carrito large-10 medium-10 small-8 columns">
                           <div class="nom_prod large-6 medium-6 small-12 columns">
                              <h2>{{ name }}</h2>
                              <h4>{{ code }}</h4>
                              <h4> Cantidad: {{ quantity }}</h4>
                           </div>
                           <div class="nom_prod2 large-6 medium-6 small-12 columns">
                              {{ if orig_price_formatted }}
                                <span>Precio Original:</span> <s><h1>{{ orig_price_formatted }}</h1></s>
                                <span>Precio de descuento:</span> <h1>{{ price_format }}</h1>
                              {{ else }}
                                <h1>{{ price_format }}</h1>
                              {{ endif }}
                              <h3>Subtotal {{ subtotal }}</h3>
                              {{ if options }}
                              <div class="options">
                                  <span class="title">Opciones</span><br>
                                  {{ options }}
                                    <span>{{ title }} : {{ value }}</span><br/>
                                  {{ /options }}
                              </div>
                              {{ endif }}
                           </div>
                       </div>
						
                       

                        <div class="clear"></div>

                    </li>

                     
                     {{ /products }}  
                   {{ /firesale:cart }}

                 </ul>

                   <div class="clear"></div>

              </div>

            </article>

            <!--Fin Lista Carrito-->



            <!--Lista de pago-->

            <article class="large-4 medium-4 small-12 columns padding_l">

              <div class="caja_borde">

                 <ul class="tabla_pagos">

                   <li>

                     <div class="large-8 medium-8 small-12 columns">

                          Cantidad de Artículos

                     </div>

                     <div class="large-4 medium-4 small-12 columns valor">

                          {{ firesale:cart }}
                            {{ count }}
                          {{ /firesale:cart }}

                     </div>

                   </li>
				   <li>
					  <div class="large-8 medium-8 small-12 columns">
							Iva
					  </div>
					  <div class="large-4 medium-4 small-12 columns valor">
						  {{ firesale:cart }} {{ tax }}{{ /firesale:cart }}<span>*</span>
					  </div>
					</li>
                   <li>
                     <div class="large-8 medium-8 small-12 columns">
                          Envío estimado
                     </div>
                     <div class="large-4 medium-4 small-12 columns valor price-ship">
                          {{ price_ship }}<span>*</span>
                     </div>
                   </li>
                   <div class="valor_comp">
                      <div class="large-6 medium-6 small-12 columns">TOTAL</div>
                      <div class="large-6 medium-6 small-12 columns valor_t price-total2 price-total"> {{ firesale:cart }} {{ total }} {{ /firesale:cart }}</div> 
                   </div>
                     <div class="clear"></div>
                  <!-- <a href="carrito_paso2.php" class="btn_gris">PAGAR AHORA</a>  -->



                   <!--<div class="term_compra">

                     <p>*Envío calculado para <span>Colombia</span>, <span>Bogotá D.C.</span>, <span>Despacho Estándar</span> <a href="" title="">Cambiar destino</a></p>                  

                   </div>       
                    -->
                 </ul>

              </div>

            </article>

            <!--Fin Lista de pago--> 



             <div class="clear"></div>    



            <!--Codigos Descuentos-->

            <article class="large-12 medium-12 small-12 columns">

              <div class="codigo_dest">

                  <div class="large-2 medium-2 small-12 columns right">
                      <button type="submit" name="btnAction" value="pay" class="btn_blanco" >CONTINUAR</button>
                  </div>

              </div>

            </article>

            <!--Fin Codigos Descuentos-->  
               <div class="clear"></div>

          </section>

          <!--Fin Contenidos Sitio-->      

      </section>
<?php echo form_close(); ?>


</div>
</div>
{{ endif }}

                <!-- Cart ends -->
<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['firesale:field:digital'] = 'Digitale?';
$lang['firesale:field:download'] = 'Download';
$lang['firesale:field:downloads'] = 'Permetti Download';
$lang['firesale:field:expires_after'] = 'Scade dopo';

$lang['firesale:instructions:digital'] = 'Ordini che contengono solo oggetti digitali saranno segnati automaticamente come spediti.';
$lang['firesale:instructions:download'] = 'File ai quali l\'utente ha accesso.';
$lang['firesale:instructions:downloads'] = 'Numero di volte che l\'utente può scaricare questo file. Se lasciato vuoto sarà illimitato.';
$lang['firesale:instructions:expires_after'] = 'Per quanto tempo l\'utente sarà in grado di scaricare il file dopo l\'acquisto.';

$lang['firesale:interval:hours'] = 'Ore';
$lang['firesale:interval:days'] = 'Giorni';
$lang['firesale:interval:weeks'] = 'Settimane';
$lang['firesale:interval:months'] = 'Mesi';
$lang['firesale:interval:years'] = 'Anni';

// Tabs
$lang['firesale:tabs:digital'] = 'Digitale';
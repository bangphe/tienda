<style type="text/css">
.chzn-container-single{
 width: 50px !important;
}

</style>

<section class="title">
    <h4><?php echo lang('firesale:prod_title'); ?></h4>
    <a class="btn" href="<?php echo site_url('admin/firesale/products/change_prices'); ?>">Cambiar Productos</a>
    <a class="btn" href="<?php echo site_url('admin/firesale/products/import_products'); ?>">Importar Productos</a>
    <a class="show-filter toggle" original-title="<?php echo lang('firesale:label_showfilter'); ?>"></a>
</section>

<?php echo form_open($this->uri->uri_string(), 'class="crud" id="products"'); ?>

<section class="item">
    <div class="content">
        <?php if ($count == 0): ?>
            <div class="no_data"><?php echo lang('firesale:prod_none'); ?></div>
        </div>
    </section>
<?php else: ?>

    <fieldset id="filters" class="four" style="display: none">
        <legend><?php echo lang('global:filters'); ?></legend>
        <ul>
            <li>
            <center><?php echo form_dropdown('category', $categories, ( isset($category) ? $category : 0)); ?></center>
            </li>
            <li>
            <center><?php echo $status; ?></center>
            </li>
            <li>
            <center><?php echo $stock_status; ?></center>
            </li>
            <li>
            <center><input type="text" name="search" placeholder="Palabra Clave..." /></center>
            </li>
            <li class="wide">
            <center>
                <div class="ui-slider-cont">
                    <label class="left"><?php echo $this->settings->get('currency'); ?><span><?php echo $min_max['min']; ?></span></label>
                    <label class="right"><?php echo $this->settings->get('currency'); ?><span><?php echo $min_max['max']; ?></span></label>
                    <div id="price-slider"></div>
                </div>
                <input type="hidden" name="price" value="<?php echo $min_max['min']; ?>-<?php echo $min_max['max']; ?>" />
            </center>
            </li>
        </ul>
    </fieldset>
	<div class="scroll-table">
        <table id="product_table">
            <thead>
                <tr>
                    <th data-sorter="false" style="width: 15px"><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
                    <th style="width: 110px"><?php echo lang('firesale:label_id'); ?></th>
                    <th data-sorter="false" style="width: 40px"><?php echo lang('firesale:label_image'); ?></th>
                    <th style="width: 340px"><?php echo lang('firesale:label_title'); ?></th>
                    <th style="width: 160px"><?php echo lang('firesale:label_parent'); ?></th>
                    <th style="width: 160px">Orden</th>
                    <th style="width: 80px"><?php echo lang('firesale:label_stock_short'); ?></th>
                    <th style="width: 90px"><?php echo lang('firesale:label_price'); ?></th>
                    <th data-sorter="false"></th>
                </tr>
            </thead>
    
            <tfoot>
                <tr>
                    <td colspan="9"><div style="float:right;"><?php echo $pagination['links']; ?></div></td>
                </tr>
            </tfoot>
    
            <tbody>
                <?php //echo '<pre>'; print_r($products); exit(); ?>
                <?php foreach ($products as $product): ?>
                    <tr data-id="<?php echo $product['id']; ?>">
                        <td><input type="checkbox" name="action_to[]" value="<?php echo $product['id']; ?>"  /></td>
                        <td class="item-id"><?php echo $product['code']; ?></td>
                        <td class="item-img"><center><img src="<?php echo ($product['imagen']['image'] != '')? $product['imagen']['image']  :  ( $product['image'] != FALSE ? site_url('files/thumb/' . $product['image'] . '/32/32') : '' ); ?>" alt="Product Image" /></center></td>
            <td class="item-title"><a href="<?php echo $this->pyrocache->model('routes_m', 'build_url', array('product', $product['id']), $this->firesale->cache_time); ?>" target="_blank"><?php echo $product['title']; ?></a></td>
            
            <td class="item-category">
            <table>
					<?php $string = '';
                    $cont = 1;
                    foreach ($product['category'] AS $cat) {
            	echo '<tr>';
                	echo '<td style="text-align:right; height:30px; vertical-align:middle;">';
                        
                        $string = '<span data-id="' . $cat['id'] . '"><b>' . $cat['title'] . '</b></span><br>';
                        echo ' '.$string;
                        $cont++;
                    echo '</td>';
                echo '</tr>';
                        
                     
                    }  ?>
            </table>
            </td> 
            
            <td class="item-orden">
            <table>
                <?php $string = '';
                $cont = 1;
                foreach ($product['category'] AS $cat) {
            	echo '<tr>';
                	echo '<td style="height:30px; vertical-align:middle; text-align:left;">';
                    $cant = cache('categories_m/total_products', $cat['id']);
                    $orden = $this->categories_m->get_orden($cat['id'], $product['id']);
                    //echo 'orden: '.$orden.'<br>';
                   // echo '<br>';
                    //echo 'cat: '.$cat['id'].' ---- prod: '.$product['id'].'  orden: '.$orden;
                    //echo '<br>';
                    //$string = $cont.'<span data-id="' . $cat['id'] . '"><b>' . $cat['title'] . '</b></span><br>';
                    //echo '<span>'.$cont.'</span>';
                    $cont++;
                    ?>

                    <select data-prod="<?php echo $product['id']; ?>" data-orden="<?php echo $orden; ?>" data-cat="<?php echo $cat['id'] ?>" name="orden_<?php echo $product['id']; ?>_<?php echo $cat['id'] ?>" class="orden"   > 
                     <option value="-1">--</option>
                     <?php  for($x=1; $x<=$cant; $x++){ ?>
                        <option value="<?= $x ?>" <?php echo ($x == $orden)? 'selected' : '' ?> ><?= $x ?></option>
                     <?php } ?>
                    </select><br>
                     
                    <?php 
                    echo '</td>';
                echo '</tr>';
                    
                }  ?>
            </table>
            </td>
            
            <td class="item-stock"><?php echo ( $product['stock_status']['key'] == 6 ? lang('firesale:label_stock_unlimited') . ' (&infin;)' : $product['stock'] ); ?></td>
            <td class="item-price"><?php echo $product['price_formatted']; ?></td>
            <td class="actions">
                <a href="<?php echo site_url('admin/firesale/products/edit/' . $product['id']); ?>" class="btn blue small"><?php echo lang('global:edit'); ?></a>
                <a href="<?php echo site_url('admin/firesale/products/edit/' . $product['id']).'#images'; ?>" class="btn blue orange small">Imagenes</a>
                <a href="<?php echo site_url('admin/firesale/products/delete/' . $product['id']); ?>" class="btn red small confirm"><?php echo lang('global:delete'); ?></a>
            </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        </table>
    </div>

    <br />
    <div class="table_action_buttons">
        <?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
        <button class="btn green" name="btnAction" value="duplicate"><?php echo lang('firesale:label_duplicate'); ?></button>
        <button class="btn blue quickedit" name="btnAction" value="quickedit"><?php echo lang('firesale:prod_button_quick_edit'); ?></button>
    <?php echo $buttons; ?>
    </div>

    </div>
    </section>
<?php endif; ?>

<?php echo form_close(); ?>


   <script type="text/javascript">
        $(function(){
            //$(".orden").change(function(cat,prod){
            $(document).on("change", ".orden", function(event){
                var product = $(this).attr('data-prod');
                var category = $(this).attr('data-cat');
                var orden = $(this).attr('data-orden');
                
                if(this.value != '-1'){
                    var url = "<?php echo base_url().'admin/firesale/products/set_orden' ?>";
                     $.ajax({
                           type: "POST",
                           url: url,
                           data: { category: category, product:product, orden:this.value, actual:orden }, // Adjuntar los campos del formulario enviado.
                           success: function(data)
                           {
                                //alert(data);
                                //response = jQuery.parseJSON(data);                         
                                //$("#msg").html(response.msg);
                                //setTimeout(function(){ location.reload();}, 1000); 
                                location.reload();
                          }
                    });
                }

                return false; // Evitar ejecutar el submit del formulario.
             });
        });

    </script>
<section class="cont_home">
    <section class="marco">
        <!--Titulo-->
        <div class="subtittle titulo_int animated fadeInDown">
            <h1>Activación de cuenta</h1>
        </div>
          <div class="clear"></div>
        <section class="cont_nosotros">
                
            <div class="bg_log_v elem_blancos animated fadeInUp">
               
               <div class="alert-error">
					Un código de activación ha sido enviado a tu dirección de correo.
				</div>
				<?php if (validation_errors()): ?>
				<div class="alert-error">
				  <?php echo validation_errors();?>
				</div>
				<?php endif ?>
				<?php echo form_open('users/activate', array('class'=>'user_form form-horizontal')) ?>
	    		  	<fieldset>
						<label class="control-label" for="email"><?php echo lang('global:email') ?></label>					
						<?php echo form_input('email', isset($_user['email']) ? $_user['email'] : '', 'maxlength="40"');?>						
					</fieldset>
	    		  	<fieldset>				
						<label class="control-label" for="activation_code"><?php echo lang('user:activation_code') ?></label>					
						<?php echo form_input('activation_code', '', 'maxlength="40"');?>						
					</fieldset>

					<div class="form_buttons center drop-buttons">
						<input class="btn_gris" type="submit" value="Activar" name="btnSubmit">
					</div>

				<?php echo form_close() ?>
                   
            </div>

        </section>

    </section><!--Marco-->  
</section>

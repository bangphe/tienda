# FireSale Discount Codes

* Website: http://www.getfiresale.org
* Documentation: http://docs.getfiresale.org
* License: Included with release
* Version: 1.1.2

Easily add discount codes to selected products, categories or everything. You can apply a fixed amount or a percentage discount.

### Installation

1. Create a new folder in your chosen addons directory named "firesale\_discount\_codes"
2. Drop in the files included with this release
3. Navigate to your control panel and goto add-ons
4. Install the FireSale Discount Codes module
5. Add an extra form to the cart page of your theme

### Form Example

	{{ if {firesale_discount_codes:code_applied} }}
        You currently have the "{{ firesale_discount_codes:code_applied }}" code applied to your basket. <a href="{{ url:site uri="cart/remove_discount" }}">Remove it?</a>
    {{ else }}
        <form method="post" action="{{ url:site uri="cart/apply_discount" }}">
          <input name="code" id="code" type="text" />
          <button type="submit" value="submit">Apply Discount</button>
        </form>
    {{ endif }}

You can also show the old and new totals for your shopping cart using the following code in your view.

	{{ if {firesale_discount_codes:code_applied} }}
          <li>Total Before Discount: {{ settings:currency }}{{ orig_total }}</li>
          <li>New Total: {{ settings:currency }}{{ total }}</li>
	{{ else }}
		<li>Total: {{ settings:currency }}{{ total }}</li>
	{{ endif }}

### Feedback and Issues

If you have any issues please email [support@getfiresale.org ](mailto:support@getfiresale.org)and either report your issue or request access to the GitHub repo and we will consider the issue/request as soon as we can.

-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-04-2015 a las 15:02:13
-- Versión del servidor: 5.1.73-cll
-- Versión de PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `proye127_database`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `core_domains`
--

CREATE TABLE IF NOT EXISTS `core_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `site_id` int(11) NOT NULL,
  `type` enum('park','redirect') NOT NULL DEFAULT 'park',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`domain`),
  KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `core_settings`
--

CREATE TABLE IF NOT EXISTS `core_settings` (
  `slug` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `default` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`slug`),
  UNIQUE KEY `unique - slug` (`slug`),
  KEY `index - slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Stores settings for the multi-site interface';

--
-- Volcado de datos para la tabla `core_settings`
--

INSERT INTO `core_settings` (`slug`, `default`, `value`) VALUES
('date_format', 'g:ia -- m/d/y', 'g:ia -- m/d/y'),
('lang_direction', 'ltr', 'ltr'),
('status_message', 'This site has been disabled by a super-administrator.', 'This site has been disabled by a super-administrator.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `core_sites`
--

CREATE TABLE IF NOT EXISTS `core_sites` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ref` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` int(11) NOT NULL DEFAULT '0',
  `updated_on` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique ref` (`ref`),
  UNIQUE KEY `Unique domain` (`domain`),
  KEY `ref` (`ref`),
  KEY `domain` (`domain`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `core_sites`
--

INSERT INTO `core_sites` (`id`, `name`, `ref`, `domain`, `active`, `created_on`, `updated_on`) VALUES
(1, 'Default Site', 'default', 'localhost', 1, 1391099018, 0),
(3, 'Mauxi', 'mauxi', 'mauxi.local', 1, 1391105865, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `core_users`
--

CREATE TABLE IF NOT EXISTS `core_users` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salt` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Super User Information' AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `core_users`
--

INSERT INTO `core_users` (`id`, `email`, `password`, `salt`, `group_id`, `ip_address`, `active`, `activation_code`, `created_on`, `last_login`, `username`, `forgotten_password_code`, `remember_code`) VALUES
(1, 'brayan.acebo@imaginamos.co', '359c900346bb6071caae59f016baa85c63378e5b', '9c6dd', 1, '', 1, '', 1391099015, 1391098125, 'brayanacebo', NULL, 'dd707f7728e5239e532958e0ee0bc6fc89add79a');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_about_us`
--

CREATE TABLE IF NOT EXISTS `default_about_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` text COLLATE utf8_unicode_ci,
  `text` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_about_us`
--

INSERT INTO `default_about_us` (`id`, `title`, `image`, `video`, `text`) VALUES
(1, 'lorm ipsum1', 'uploads/default/about_us/34684fdbe9cad55cf3c7f81fd685bbd5.png', '&lt;iframe width="478" height="315" src="//www.youtube.com/embed/iQmRIw5SkDY" frameborder="0" allowfullscreen&gt;&lt;/iframe>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In dignissim justo et est vehicula laoreet. Nunc ut urna enim. Integer scelerisque, velit in vulputate egestas, magna justo fringilla erat, nec hendrerit quam justo quis mi. In tincidunt fermentum mauris, sed bibendum lacus sodales quis. Sed sit amet vestibulum risus, ac vehicula enim. Integer posuere felis eu faucibus auctor. Vestibulum vitae ante et ante tempor sagittis a facilisis orci. Maecenas et nisl pretium odio fringilla elementum. In eleifend ac ante at aliquet. Duis eget erat accumsan, dignissim ipsum id, iaculis mi. Proin finibus vitae leo in ultrices. Aliquam et nisl nunc. Quisque pulvinar lorem tortor, sed vulputate ante convallis nec. Fusce viverra mauris placerat nulla egestas hendrerit. Nunc maximus erat auctor, lobortis massa vitae, malesuada nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus.<br />\r\n<br />\r\n​Suspendisse eget nisi in sem lobortis scelerisque. Pellentesque arcu elit, lobortis a lacus in, sagittis posuere ipsum. Praesent tristique pharetra magna sit amet commodo. Proin vulputate ligula eget iaculis fringilla. Aliquam justo nisl, venenatis a ullamcorper et, eleifend ac eros. Duis id tempor dolor, nec posuere libero. Cras vel vestibulum odio. Donec scelerisque sapien ligula, varius sollicitudin nunc finibus non. Phasellus vulputate nunc vitae nisl condimentum tincidunt. In ac ullamcorper leo, quis maximus risus. Vestibulum feugiat est in posuere interdum. Sed quis imperdiet ipsum. Sed id felis commodo, lacinia orci eu, tristique lorem. Suspendisse porta, felis id pharetra condimentum, libero est tincidunt neque, id porta diam risus vulputate enim.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_about_us_multiples`
--

CREATE TABLE IF NOT EXISTS `default_about_us_multiples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci,
  `slug` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `introduction` text COLLATE utf8_unicode_ci,
  `date` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `outstanding` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `default_about_us_multiples`
--

INSERT INTO `default_about_us_multiples` (`id`, `title`, `slug`, `image`, `content`, `introduction`, `date`, `position`, `outstanding`) VALUES
(1, 'Sobre Nosotros', 'sobre-nosotros', 'uploads/default/about_us_multiples/4da643c980a41a394075157726b3672d.png', 'Morbi quis tincidunt nulla, ac molestie libero. Integer nec pretium nisl. Morbi blandit eu massa sit amet aliquet. Mauris rutrum sodales odio, vel molestie risus pharetra in. Etiam blandit nulla ut blandit ultrices. Aliquam sit amet nisi sed nisl accumsan vestibulum. Curabitur eu lobortis ex. Mauris non lectus vehicula, posuere tortor a, bibendum tellus. Curabitur condimentum tellus sit amet est laoreet, sed lacinia mi accumsan. Praesent id odio eleifend, tempus nulla nec, tempus lectus. Mauris bibendum ac turpis eu mollis. Nullam tincidunt fermentum elementum. Aenean faucibus eros a bibendum lacinia. Maecenas lectus ipsum, volutpat at elit id, egestas imperdiet massa. Ut finibus massa vel eros rhoncus, nec placerat tellus sollicitudin. Vivamus felis augue, aliquet id imperdiet a, commodo ut mauris.', '', '2014-10-02 21:00:24', 1, NULL),
(2, 'Misión', 'mision', 'uploads/default/about_us_multiples/0c79a482d5c7420d3186414aa6c0d443.jpg', '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse cursus libero leo, quis dapibus ante sollicitudin at. In nec eros et augue tempus mollis in a mauris. Sed odio ex, mollis id urna cursus, mattis accumsan libero. Nam quis dui at velit pharetra placerat. Donec fringilla ultrices placerat. Morbi sed posuere eros. Morbi tincidunt velit in turpis fringilla consectetur. Nunc tempor porttitor efficitur. Cras rutrum mi eros, eget pulvinar nisl aliquet id. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus rutrum nibh quam. Fusce vitae vestibulum augue. Fusce pretium magna eu felis volutpat, sed accumsan eros viverra. Aliquam ullamcorper finibus eros, ut vulputate nisl condimentum ut.</span>', '', '2014-10-02 21:00:40', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_advance_news`
--

CREATE TABLE IF NOT EXISTS `default_advance_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `introduction` varchar(600) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `outstanding` int(1) DEFAULT NULL,
  `title_said` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autor_said` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_audio` text COLLATE utf8_unicode_ci,
  `type` int(1) DEFAULT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'es',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `default_advance_news`
--

INSERT INTO `default_advance_news` (`id`, `title`, `slug`, `autor`, `image`, `content`, `introduction`, `date`, `position`, `outstanding`, `title_said`, `autor_said`, `video_audio`, `type`, `lang`) VALUES
(1, 'Imagen', 'imagen', 'luis', 'uploads/default/advance_news/c57e3d028239bd5130d087399b393d0f.png', '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.</span>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.', '2014-11-04 14:58:05', 1, NULL, '', '', '', 1, 'es'),
(2, 'Texto', 'texto', 'Luis', NULL, '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.</span>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.', '2014-11-04 14:58:27', 2, NULL, 'lorem impsu', 'Lorem', '', 2, 'es'),
(3, 'Slider Imagenes', 'slider-imagenes', 'Luis', NULL, '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.</span>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.', '2014-11-04 14:58:39', 3, NULL, '', '', '', 3, 'es'),
(4, 'Audio', 'audio', 'Luis', NULL, '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.</span>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.', '2014-11-04 14:59:18', 4, NULL, '', '', '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/173752179&color=ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false"></iframe>', 4, 'es'),
(5, 'Video', 'video', 'Luis', NULL, '<span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.</span>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus, augue eu fringilla volutpat, ligula ex rutrum leo, sit amet ornare sem eros pharetra diam. Maecenas euismod viverra enim, a tempus libero malesuada sit amet. Aliquam metus augue, lacinia eget ullamcorper sagittis, ultricies eget turpis. Donec id dolor ipsum. Quisque convallis condimentum vulputate. Nam sollicitudin vehicula eros, quis facilisis enim scelerisque eu. Pellentesque velit erat, maximus eget sagittis et, dictum nec lectus. Curabitur efficitur leo non iaculis consectetur.', '2014-11-04 14:59:34', 5, NULL, '', '', '<iframe src="//player.vimeo.com/video/108888841?title=0&byline=0&portrait=0&color=ff0179" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/108888841">Earthworm Heart</a> from <a href="http://vimeo.com/user603401">Trunk Animation</a> on <a href="https://vimeo.com">Vimeo</a>.</p>', 5, 'es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_advance_news_comments`
--

CREATE TABLE IF NOT EXISTS `default_advance_news_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_advance_new` int(11) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_advance_news_images`
--

CREATE TABLE IF NOT EXISTS `default_advance_news_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `advance_new_id` int(11) NOT NULL,
  `path` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `default_advance_news_images`
--

INSERT INTO `default_advance_news_images` (`id`, `advance_new_id`, `path`, `video`, `created_at`) VALUES
(1, 3, 'uploads/default/advance_news/8f02e2d5a3600a3e40894dc187e6b442.png', NULL, '2014-11-04 13:59:43'),
(2, 3, 'uploads/default/advance_news/92ec9ce5ab40fd30e9510818cea429df.jpg', NULL, '2014-11-04 13:59:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_blog`
--

CREATE TABLE IF NOT EXISTS `default_blog` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `intro` longtext COLLATE utf8_unicode_ci,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `parsed` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `author_id` int(11) NOT NULL DEFAULT '0',
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL DEFAULT '0',
  `comments_enabled` enum('no','1 day','1 week','2 weeks','1 month','3 months','always') COLLATE utf8_unicode_ci NOT NULL DEFAULT '3 months',
  `status` enum('draft','live') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `type` set('html','markdown','wysiwyg-advanced','wysiwyg-simple') COLLATE utf8_unicode_ci NOT NULL,
  `preview_hash` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_blog`
--

INSERT INTO `default_blog` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `intro`, `title`, `slug`, `category_id`, `body`, `parsed`, `keywords`, `author_id`, `created_on`, `updated_on`, `comments_enabled`, `status`, `type`, `preview_hash`, `image`) VALUES
(1, '2014-02-28 15:42:00', '2014-02-28 15:42:00', 1, 1, 'adsf', 'asdf', 'asdfadsf', 0, 'asdfasdf', '', '', 1, 1393598520, 1393598520, '3 months', 'live', 'wysiwyg-advanced', '', '35bc6037a154502');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_blog_categories`
--

CREATE TABLE IF NOT EXISTS `default_blog_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_slug` (`slug`),
  UNIQUE KEY `unique_title` (`title`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_chat`
--

CREATE TABLE IF NOT EXISTS `default_chat` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `day` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_hour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_hour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_chat`
--

INSERT INTO `default_chat` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `day`, `start_hour`, `end_hour`) VALUES
(1, '2014-10-29 15:41:46', NULL, 10, 1, 'Wed', '00:00', '23:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_chat_assign`
--

CREATE TABLE IF NOT EXISTS `default_chat_assign` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `assign_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assign_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assign_user_id` int(11) DEFAULT NULL,
  `assign_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assign_start_on` datetime DEFAULT NULL,
  `assign_finished_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_chat_assign`
--

INSERT INTO `default_chat_assign` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `assign_name`, `assign_email`, `assign_user_id`, `assign_status`, `assign_start_on`, `assign_finished_on`) VALUES
(1, '2014-10-29 15:43:06', '2014-10-29 15:43:13', 10, 1, 'luis', 'luis.salazar@imagina.co', 10, 'active', '2014-10-29 15:43:13', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_chat_message`
--

CREATE TABLE IF NOT EXISTS `default_chat_message` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `message_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message_assign_id` int(11) DEFAULT NULL,
  `message_sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message_username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `default_chat_message`
--

INSERT INTO `default_chat_message` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `message_text`, `message_assign_id`, `message_sender`, `message_username`) VALUES
(1, '2014-10-29 15:43:06', NULL, 10, 1, 'prueba', 1, 'client', 'luis'),
(2, '2014-10-29 15:43:20', NULL, NULL, NULL, 'prueba', 1, 'admin', 'Asesor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_ci_sessions`
--

CREATE TABLE IF NOT EXISTS `default_ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `default_ci_sessions`
--

INSERT INTO `default_ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('7d5ea93032dce7afe929d14cb0ac5b3f', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429695779, ''),
('610bbdc9448ecc91ee044760f8e0a16d', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429679927, ''),
('be720183cc38de96a72bb028f75f4612', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429679926, ''),
('a3930596e881abf89626d59b95639b05', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429679926, ''),
('53e304cb1973bade03c1b1f1954a9ed7', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429679925, ''),
('363bc7777466859fb3524f916ac9136f', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429680832, 'a:1:{s:8:"category";s:1:"3";}'),
('51831b0ee1f35a4c51cb177c3cea7f13', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429680838, 'a:1:{s:8:"category";s:1:"3";}'),
('d0f2a0e2103680942651c8141396ba86', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429680841, 'a:1:{s:8:"category";s:1:"3";}'),
('aa0e801604d2c996f93fc73ca17ee90d', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429680843, 'a:1:{s:8:"category";s:1:"3";}'),
('1d5d5c3256ebd9108499be7c40e05d27', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429680846, ''),
('6c5805ed5a94ac16305dc56d54cdb491', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429681758, 'a:1:{s:8:"category";s:1:"1";}'),
('62d97251516ea15b9b008ea8f5c7d764', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429681765, 'a:1:{s:8:"category";s:1:"1";}'),
('6e04241ee5ef8df15dc4919fd2eff850', '66.249.83.138', 'Mozilla/5.0 (Windows NT 6.1; rv:6.0) Gecko/20110814 Firefox/6.0 Google favicon', 1429683865, ''),
('f49a91970e4fd23515fbd04a57a69da7', '66.249.65.149', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e', 1429683154, ''),
('7236d050f379857c34ea6269525fea42', '201.216.0.173', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko', 1429734047, ''),
('0f703a4d185bdc6faba6e8ec0eccaa14', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429738809, ''),
('b35108daf240332080f3fb64fdcea5f5', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429534754, ''),
('55761ed4a4cc4e73c6df167f0e4d5aab', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429410679, ''),
('3aaf6ff6427dc3e6e41fc29c749d0ece', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429416451, ''),
('dafeb58b053b4b72b61ca6d07efbaadf', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429419111, ''),
('23a2c1f023fc42166100bbab98abe8be', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429426859, ''),
('69922ca75ba187831f21b5aab1642e5d', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429429061, ''),
('03b81ff65aa8aaec622ebec43afcd6b2', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429448008, ''),
('e44d1cc600c2c371a04931f5c2d8d83e', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429467999, ''),
('9f6da1c0c51cb4fe4a95e4a575e29f6f', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429441536, ''),
('2da8b4c64ce7a3379494f0fada504e6e', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429449464, ''),
('1dd303b41d4caac48e2ed0949f12c90e', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429605704, ''),
('5043b2ccee1432b2d22185b7aad4e2fe', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429488854, ''),
('3a906691c2cbdd429c62f9d779215b56', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429440916, ''),
('1eaf0d4baf6fc5959b9c71f06f86d16e', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429489079, ''),
('6ded7fe33a77468b2f6d89b1826d8d27', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429512401, ''),
('5eadcf8f53b957f2b03180986cb2ea49', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429525300, ''),
('ce58cae161c30a548f00d339ccfc4795', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429586814, ''),
('1924241b88eb0363aa66f3ec1094a593', '186.28.141.44', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429596868, 'a:1:{s:17:"flash:old:success";s:24:"Has salido de tu cuenta.";}'),
('45a82db437129c76270d5481a9e8c681', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429260473, ''),
('fe3e33c9fc74dff6b3fbff2f6c1057f8', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429261421, ''),
('250bc265e50d4069521e113e1f00845c', '66.249.64.39', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429234018, 'a:1:{s:9:"lang_code";s:2:"es";}'),
('fbed36031d795227b7a90a097a8e2867', '190.26.241.210', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 1429574620, ''),
('bdb61bdc7693dc3042686fab2d48b00d', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429565479, ''),
('ad27eafac0f18f5a451855cd6ddee61a', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429260422, ''),
('d98870935d253472e977a0230373b2ac', '190.27.219.164', 'Mozilla/5.0 (BB10; Touch) AppleWebKit/537.35+ (KHTML, like Gecko) Version/10.3.1.2576 Mobile Safari/537.35+', 1429260223, ''),
('9b0f90472552d37d5ae22c3cbe9fd917', '66.249.64.43', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429281890, ''),
('39676d8c680add2884aef6c1c0b68d86', '66.249.64.43', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e', 1429270047, ''),
('507f8422a7d99c0ddaf7dd3f7085902b', '66.249.64.43', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429281891, ''),
('935d68ec53254cd22881871cec2280fd', '66.249.64.39', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429286787, 'a:1:{s:9:"lang_code";s:2:"es";}'),
('0cbad4fdd50fa9451a7668f67839539d', '66.249.64.39', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429286788, 'a:1:{s:9:"lang_code";s:2:"es";}'),
('c44c3ade2f576468c364ca38d2c07e59', '190.85.21.242', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429302276, 'a:1:{s:8:"category";s:0:"";}'),
('5fb2de7be68f3a58a00042abe9c4d68a', '107.167.107.157', 'Mozilla/4.0 (Windows 98; US) Opera 10.00 [en]', 1429328029, ''),
('437ac0db803f176adad24165480f7e62', '66.249.64.41', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429330584, ''),
('7cbfbfce508475445740990535b92620', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429410675, ''),
('41495387eb4456af5f10942d98227465', '66.249.64.41', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429330587, ''),
('2dc4449d670e72a0f830b8e208ec67f6', '66.249.64.39', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429333847, ''),
('03fdde15073302357909c8feea4ff94d', '186.28.183.72', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429338513, 'a:1:{s:17:"flash:old:success";s:24:"Has salido de tu cuenta.";}'),
('7027733d331c2ab232f0908a4b5ddd0d', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429347913, ''),
('94e671703096507f95bb852c6b6ecdf4', '66.249.64.43', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429348444, ''),
('91ba9a257b231f6f608c51ffdca3fb15', '66.249.64.41', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429363784, ''),
('c259d1286440709de3d9cc7b688cd247', '66.249.64.41', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429369247, ''),
('b2781496ff3aaf8e680833f1e4763165', '199.21.99.201', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429376580, ''),
('a0d984a4d7b4dc4f848269316229ae5f', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429386845, ''),
('1aae7f3d6f81704cbd39efcf81ebb00a', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429350451, ''),
('58e054c068fd6ea0abab5510e7bff2eb', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429357378, ''),
('d378d9689acdd780b6735c7a5b1ebe8c', '181.143.140.59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429727569, 'a:7:{s:5:"email";s:23:"luis.salazar@imagina.co";s:2:"id";s:2:"10";s:7:"user_id";s:2:"10";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";s:13:"cart_contents";a:3:{s:10:"cart_total";d:309800;s:11:"total_items";d:2;s:32:"9d73f3d09bf359aa7581c160c3e422e7";a:14:{s:2:"id";s:3:"608";s:4:"code";s:8:"AB0379-M";s:3:"qty";d:2;s:5:"price";d:154900;s:8:"tax_band";s:1:"1";s:4:"name";s:38:"CAMISETA OFICIAL 2015 MANGA LARGA  - M";s:4:"slug";s:35:"camiseta-oficial-2015-manga-larga-m";s:4:"ship";s:1:"1";s:6:"weight";s:5:"0,250";s:5:"image";s:15:"94f5df765f3a33f";s:7:"options";a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"222";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"M";s:5:"price";s:1:"0";}}s:6:"parent";s:3:"545";s:5:"rowid";s:32:"9d73f3d09bf359aa7581c160c3e422e7";s:8:"subtotal";d:309800;}}s:8:"order_id";i:335;}'),
('58ecc08ff5060501894114e43c2789e6', '181.143.140.59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2377.0 Safari/537.36', 1429725270, ''),
('2d42cc19c4597ba76338b70059739a9c', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429718956, ''),
('ef4dd820865a1bfc23172ab5cea5cef8', '66.249.65.145', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e', 1429723219, ''),
('0d036c7cbeb52a4a9ea7f545ba977430', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429695781, ''),
('330beb671e5945614b00dad41e1f6f19', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429701764, ''),
('459500caefeb6c17096e28bfd8c068e1', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429582827, ''),
('7a576d3643a29d7d941c6954e2ecebcd', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429579131, ''),
('fdb494f6990aab41719b1bae476e25b5', '190.27.77.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429420974, 'a:1:{s:17:"flash:new:success";s:24:"Has salido de tu cuenta.";}'),
('09926310f9798218d64e3afc78332ad5', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429424792, ''),
('efff76a011058e90666e62c8d9d08f10', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429535137, ''),
('4da21d0d4c4ba98048456400e32dcd17', '186.28.141.44', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429595147, 'a:7:{s:8:"username";s:4:"demo";s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";s:8:"category";s:1:"4";}'),
('85d5463fd0d5554b97f2d0aadfb724ff', '181.60.20.6', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429727439, 'a:8:{s:5:"email";s:23:"luis.salazar@imagina.co";s:2:"id";s:2:"10";s:7:"user_id";s:2:"10";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";s:8:"category";s:1:"3";s:13:"cart_contents";a:3:{s:32:"0537fb40a68c18da59a35c2bfe1ca554";a:14:{s:2:"id";s:3:"744";s:4:"code";s:6:"AB0399";s:3:"qty";d:1;s:5:"price";d:1160000;s:8:"tax_band";s:1:"1";s:4:"name";s:42:"RELOJ EDICION LIMITADA MILLONARIOS-BOMBERG";s:4:"slug";s:41:"reloj-edicion-limitada-millonariosbomberg";s:4:"ship";s:1:"1";s:6:"weight";s:0:"";s:5:"image";s:15:"e0786047d574f11";s:7:"options";N;s:6:"parent";N;s:5:"rowid";s:32:"0537fb40a68c18da59a35c2bfe1ca554";s:8:"subtotal";d:1160000;}s:10:"cart_total";d:1160000;s:11:"total_items";d:1;}s:8:"order_id";i:333;}'),
('c7dfca30b577d44212bd92679df261d0', '107.167.107.102', 'Mozilla/4.0 (Windows 98; US) Opera 10.00 [en]', 1429661834, ''),
('ce6721afee84a82763a0a85e6dfef852', '181.143.140.59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2376.0 Safari/537.36', 1429663054, ''),
('89d7294009fe4f80d8a4f4c41532c7df', '190.85.21.242', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429671498, 'a:1:{s:8:"category";s:1:"3";}'),
('6039043acab166d5414e3558f05a4814', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429672305, ''),
('39d75db55d415adabea0f3c912388ad0', '199.21.99.201', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429672310, ''),
('774e0ff6c7a9208ba2274aea3737886c', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429672905, ''),
('a3d378891c505d25e4b942b4e01be0bf', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429652576, ''),
('06190775394834f1deadb32d9accbd25', '190.144.28.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429658960, ''),
('fadb1dc83fc627709a28b2fa073d07cd', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429605707, ''),
('814e4326341c407fe77a5f7bc67bf51c', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429629259, ''),
('49ce77b2cbcc3c46b0b111036ffa4796', '181.143.140.59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2376.0 Safari/537.36', 1429662772, 'a:4:{s:13:"cart_contents";a:3:{s:10:"cart_total";d:54000;s:11:"total_items";d:1;s:32:"37f0e884fbad9667e38940169d0a3c95";a:18:{s:2:"id";s:3:"529";s:4:"code";s:6:"AB0269";s:3:"qty";d:1;s:5:"price";d:54000;s:8:"tax_band";s:1:"1";s:4:"name";s:10:"AUDIFONOS ";s:4:"slug";s:9:"audifonos";s:4:"ship";s:1:"1";s:6:"weight";s:5:"0,340";s:5:"image";s:15:"07c5252226202fa";s:7:"options";N;s:6:"parent";N;s:5:"rowid";s:32:"37f0e884fbad9667e38940169d0a3c95";s:10:"orig_price";d:60000;s:13:"discount_code";s:6:"123456";s:8:"modified";b:1;s:20:"orig_price_formatted";s:7:"$60.000";s:8:"subtotal";d:54000;}}s:13:"discount_code";s:6:"123456";s:8:"category";s:0:"";s:4:"term";s:11:"AURICULARES";}'),
('a7777cc8a844b3e73fb8afc60ea24f8b', '181.143.140.59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429659841, 'a:8:{s:4:"term";s:8:"chaqueta";s:8:"username";s:4:"luis";s:5:"email";s:23:"luis.salazar@imagina.co";s:2:"id";s:2:"10";s:7:"user_id";s:2:"10";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";s:8:"category";s:0:"";}'),
('1003bd078296ce9e616ca1c7ab891769', '199.21.99.201', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429744387, ''),
('6b985f4e02afb25fae005dbe6f7690ef', '66.249.64.39', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e', 1429135767, 'a:1:{s:8:"category";s:2:"10";}'),
('0d4e703984835ffed52a09467d6abc47', '190.147.0.131', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429746920, 'a:1:{s:17:"flash:old:success";s:24:"Has salido de tu cuenta.";}'),
('ad513e33fb0e47a97d652ef15dd3e7ad', '181.60.20.6', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429658977, 'a:5:{s:5:"email";s:23:"luis.salazar@imagina.co";s:2:"id";s:2:"10";s:7:"user_id";s:2:"10";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";}'),
('eafce8290910dd24b57ca8070bbc22f6', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429652577, ''),
('e4ddd93995e3dbbb0fa86d9b0cdc2db4', '190.144.28.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429658910, 'a:1:{s:17:"flash:old:success";s:24:"Has salido de tu cuenta.";}'),
('6e1b4b63add3d504780fbb45c5979844', '190.159.146.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429679925, 'a:1:{s:8:"category";s:1:"1";}'),
('601df93bc36ff9a96b3a6c6978c019d9', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429146033, 'a:5:{s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";}'),
('6569f637a1894a304bfe55a1a183c641', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429146033, 'a:5:{s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";}'),
('27ac17c8e605ad011cbd6798d9a4e218', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429146033, 'a:5:{s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";}'),
('54a2c3ae61f2c9e1ddb4879c9c84193f', '37.228.105.25', 'Mozilla/4.0 (Windows 98; US) Opera 10.00 [en]', 1429143758, ''),
('22279d4da8d949060e78f0246ea74c33', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429145728, 'a:1:{s:14:"admin_redirect";s:5:"admin";}'),
('e4ad487015fcc0b76f325bffad5f68f0', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429146032, 'a:7:{s:8:"category";s:2:"12";s:8:"username";s:4:"demo";s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";}'),
('f6449af498022e89efef8bb8ff6a40de', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429146032, 'a:5:{s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";}'),
('35e47918ecdf6cda5549a93d5d89a2a9', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429171487, ''),
('0cd0c00aa4ec0e58e5ea918975563855', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429175451, 'a:1:{s:15:"flash:new:error";s:39:"You must be logged in to view this page";}'),
('6b36464492c354a9e178d51dd9df6a0a', '66.249.88.186', 'Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)', 1429565637, ''),
('3872dcfc0c8cdb1bad5a680b358d977d', '66.249.67.13', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429143515, ''),
('a188e523216b569f07c0eefc4f2d86c0', '66.249.64.43', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429181472, ''),
('931d052fe7ac90b813a0b74390ec84aa', '100.43.85.12', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429175454, ''),
('06d97e75de2975cd12b733e1717b4c1f', '181.143.140.59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429727567, 'a:8:{s:5:"email";s:23:"luis.salazar@imagina.co";s:2:"id";s:2:"10";s:7:"user_id";s:2:"10";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";s:8:"category";s:1:"3";s:13:"cart_contents";a:3:{s:10:"cart_total";d:154900;s:11:"total_items";d:1;s:32:"39a3c93bd043c48765ca86f059b4ddac";a:14:{s:2:"id";s:3:"609";s:4:"code";s:8:"AB0379-L";s:3:"qty";d:1;s:5:"price";d:154900;s:8:"tax_band";s:1:"1";s:4:"name";s:38:"CAMISETA OFICIAL 2015 MANGA LARGA  - L";s:4:"slug";s:35:"camiseta-oficial-2015-manga-larga-l";s:4:"ship";s:1:"1";s:6:"weight";s:5:"0,250";s:5:"image";s:15:"94f5df765f3a33f";s:7:"options";a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"223";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"L";s:5:"price";s:1:"0";}}s:6:"parent";s:3:"545";s:5:"rowid";s:32:"39a3c93bd043c48765ca86f059b4ddac";s:8:"subtotal";d:154900;}}s:8:"order_id";i:330;}'),
('56406cb773447b0a53345eb8c457bae7', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429148550, 'a:1:{s:17:"flash:old:success";s:24:"Has salido de tu cuenta.";}'),
('c6425acf2f892cc2901d7f24f4cf4b1d', '66.249.64.77', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429152146, 'a:1:{s:9:"lang_code";s:2:"es";}'),
('35353f53e95d11c81c7c8b37a679fac9', '66.249.64.77', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429147623, ''),
('99cd624572b00b481bd2154473f548f5', '66.249.88.189', 'Mozilla/5.0 (Windows NT 6.1; rv:6.0) Gecko/20110814 Firefox/6.0 Google favicon', 1429196354, ''),
('ff6f7e135a5955e9ce3b6ee6005ac07d', '66.249.64.41', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429181473, ''),
('8d5635bec402c9e33c646251291cdf1e', '66.249.67.5', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429143513, ''),
('b929202e4971d51507893a371a5ba59d', '66.249.64.41', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429267546, ''),
('9d6fd1b6605a6b66af0f4de957f21b09', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429262198, ''),
('931d32d2cd5a082d1104d1dc8b1366b9', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429261426, ''),
('5f76f5cf574d42359738c00960cfa577', '66.249.64.43', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429218073, ''),
('8c7b560f8681ce1ef96bf34fcdf8b75c', '66.249.64.43', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429234018, 'a:1:{s:9:"lang_code";s:2:"es";}'),
('cb0ed1ef1fd90608c45e7e6929d6fb49', '199.21.99.201', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429240183, ''),
('1f7ffb43eded862157d8f7167a97694f', '186.155.47.245', 'Mozilla/5.0 (Linux; U; Android 4.2.2; es-mx; ALCATEL ONE TOUCH 7042A Build/JDQ39) AppleWebKit/534.30 (KHTML, like Gecko)', 1429242464, ''),
('1f7dc7efea83e92a5018c39aa2b9a70f', '66.249.64.39', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429243004, 'a:1:{s:9:"lang_code";s:2:"es";}'),
('0aa8774827e36d13d0d57bd368dc4faf', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429248568, ''),
('c13acaee432478dfec17b9c98a6b98d3', '186.28.58.85', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429251551, 'a:6:{s:8:"username";s:4:"demo";s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";}'),
('03cad97f0f1ed652917aebf387a5bd53', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429252387, 'a:1:{s:8:"category";s:2:"13";}'),
('4cb324872765f7d01ba02ff8cf2871f6', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429259710, ''),
('b6bfd45d8ad67436d77b87045b8f24f4', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429260172, ''),
('c4cb182e91ca8d62dcecea024193ad83', '66.249.64.41', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429234016, ''),
('553fef1cc97d27b639e57dce43773a9b', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429233051, 'a:1:{s:8:"category";s:1:"6";}'),
('a432fb49b185cbb764e9c6004f1b3d80', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429231734, ''),
('e96c7f3ea5d1198aa218252e99c9d0d9', '66.249.64.41', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429221695, ''),
('b3b6c39299977762b0473b29bc38da30', '66.249.64.43', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429218525, 'a:1:{s:9:"lang_code";s:2:"es";}'),
('13e00db27e70b85e03e6420863c6847b', '181.143.140.59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2369.0 Safari/537.36', 1429147310, 'a:8:{s:8:"username";s:13:"fabian.andres";s:5:"email";s:25:"fabian.riascos@imagina.co";s:2:"id";s:2:"21";s:7:"user_id";s:2:"21";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";s:8:"category";s:1:"3";s:17:"flash:old:message";s:13:"Carrito vacio";}'),
('146ccb7aee74321e2581f95261817921', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429134537, 'a:1:{s:17:"flash:old:success";s:24:"Has salido de tu cuenta.";}'),
('564f902776cc8d06fe147d9270d05fd6', '66.249.64.39', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e', 1429134982, ''),
('ab3d8843cd08f9a3ee759da739fafcba', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429512407, ''),
('cf93d0b000532e6c74d1871a8b680b31', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429524431, ''),
('c359c4502789a867ec54555cb946a657', '181.60.20.6', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429658678, 'a:1:{s:14:"admin_redirect";s:5:"admin";}'),
('042fafab97bf21f85420dec0de4fff92', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429560333, ''),
('8faa7d564f5fdae2ca5fa777dbf1d1f1', '66.249.65.141', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e', 1429572468, ''),
('9076f7a1d7dfcf524ac469f4d61a0f1f', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429560340, ''),
('788975131fb7fc67c5ed57e9e2de9472', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429561069, ''),
('72f9d6ced16c1e3a9077ca16920b8b57', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429563713, 'a:1:{s:17:"flash:old:success";s:24:"Has salido de tu cuenta.";}'),
('a7b3d8d973637b52fce4db13fa041016', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429576525, ''),
('ef8aec565f854e1badca03e8822df37d', '191.64.122.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 1429576695, ''),
('799fff11c0699fb4810ae9f478c70924', '191.64.122.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 1429576438, 'a:5:{s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";}'),
('ac9776a38a333263ef4ae57f167e8e8e', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429524717, ''),
('ef140ebaadf33c9405fdfc178721f79c', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429525006, ''),
('7d1b1be02789c5e0352bb5710dab023e', '190.85.21.242', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429580603, 'a:1:{s:8:"category";s:1:"4";}'),
('ace911e886b4929effbf2ca9e0ea5339', '66.249.65.145', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429674640, ''),
('9d076299ada927a1f23a2386f4a443dd', '190.25.117.247', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429683788, 'a:7:{s:8:"username";s:4:"demo";s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";s:8:"category";s:1:"6";}'),
('772d1bc85e3bc1cc1025417848ea0437', '191.64.122.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 1429575267, 'a:6:{s:8:"username";s:4:"demo";s:5:"email";s:18:"demo@imaginamos.co";s:2:"id";s:2:"13";s:7:"user_id";s:2:"13";s:8:"group_id";s:1:"3";s:5:"group";s:7:"pruebas";}'),
('7afb00e0dce5e4a069bc080191ed7c22', '181.136.114.142', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.3', 1429566632, 'a:9:{s:8:"category";s:1:"1";s:13:"cart_contents";a:3:{s:32:"504a9bb60959a7037fae1afa36bdf1f7";a:14:{s:2:"id";s:3:"695";s:4:"code";s:8:"AB0352-S";s:3:"qty";d:1;s:5:"price";d:69900;s:8:"tax_band";s:1:"1";s:4:"name";s:28:"SUETER CAPOTA STYLE GRIS - S";s:4:"slug";s:26:"sueter-capota-style-gris-s";s:4:"ship";s:1:"1";s:6:"weight";s:3:"400";s:5:"image";s:15:"498e1e5ca0dd2de";s:7:"options";a:1:{i:88;a:6:{s:6:"mod_id";s:2:"88";s:6:"var_id";s:3:"309";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}s:6:"parent";s:3:"570";s:5:"rowid";s:32:"504a9bb60959a7037fae1afa36bdf1f7";s:8:"subtotal";d:69900;}s:10:"cart_total";d:69900;s:11:"total_items";d:1;}s:8:"username";s:4:"luis";s:5:"email";s:23:"luis.salazar@imagina.co";s:2:"id";s:2:"10";s:7:"user_id";s:2:"10";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";s:17:"flash:old:success";s:29:"The gateway has been disabled";}'),
('938d0e255457f38b7ac7b98dd4b662d9', '66.249.64.43', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429198128, ''),
('be933c009be7f604692a2a92b76d9c1c', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429528756, ''),
('5a787b313f0865beefae47a98ab7a5d9', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429468002, ''),
('fc37846d72f84f72440f31ae1e5d5b17', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429483063, ''),
('89c8d8e7b9ccd51fd1a2aac5f476b959', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429486702, ''),
('a574d0022b61c9860681a72073500929', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429487416, ''),
('e9edfc1a7256759ef2cc60a889dd9421', '100.43.81.143', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1429488127, ''),
('cb0891c4f0d65b7e2d5b767b9bacb726', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1429535936, ''),
('c25e6b5e5e7729c746afdcc17ae8d06f', '190.145.2.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 1429567182, 'a:1:{s:17:"flash:old:success";s:24:"Has salido de tu cuenta.";}'),
('06a9f8a3c26412315338bf3478daffa6', '181.60.20.6', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429658674, ''),
('1424a699c01c28bcecbade88f5c0c636', '181.60.20.6', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429658978, 'a:5:{s:5:"email";s:23:"luis.salazar@imagina.co";s:2:"id";s:2:"10";s:7:"user_id";s:2:"10";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";}'),
('5adfb39b2c2521af42744f3f1f61d5fb', '181.143.140.59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2376.0 Safari/537.36', 1429638107, ''),
('4cdb8b2fb140d69c58565ccbf25def22', '181.60.20.6', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429658977, 'a:6:{s:8:"username";s:4:"luis";s:5:"email";s:23:"luis.salazar@imagina.co";s:2:"id";s:2:"10";s:7:"user_id";s:2:"10";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";}'),
('c3406f654eb0f96630734c59e29fb0f5', '190.26.83.240', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1429140111, 'a:1:{s:8:"category";s:0:"";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_comments`
--

CREATE TABLE IF NOT EXISTS `default_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `parsed` text COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `entry_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `entry_title` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entry_plural` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_comment_blacklists`
--

CREATE TABLE IF NOT EXISTS `default_comment_blacklists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_contact_log`
--

CREATE TABLE IF NOT EXISTS `default_contact_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `sender_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sender_ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sender_os` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sent_at` int(11) NOT NULL DEFAULT '0',
  `attachments` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_contact_us`
--

CREATE TABLE IF NOT EXISTS `default_contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adress` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map` text COLLATE utf8_unicode_ci,
  `schedule` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_contact_us`
--

INSERT INTO `default_contact_us` (`id`, `facebook`, `twitter`, `linkedin`, `youtube`, `adress`, `phone`, `email`, `map`, `schedule`) VALUES
(1, 'https://www.facebook.com/', 'https://twitter.com/', 'http://instagram.com', '', '', '', '', '<iframe frameborder="0" height="300" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3982.5973531308123!2d-76.53902!3d3.4476588!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e30a67c8d40055f:0x958e006cad1053b2!2sCarrera+6+#+2-4!5e0!3m2!1ses!2ses!4v1401567476879" style="border:0" width="600"></iframe>', 'Tiempo Completo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_contact_us_emails`
--

CREATE TABLE IF NOT EXISTS `default_contact_us_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `default_contact_us_emails`
--

INSERT INTO `default_contact_us_emails` (`id`, `name`, `email`, `phone`, `cell`, `company`, `city`, `message`) VALUES
(2, 'Prueba', 'correo@prueba.com', '1234567', '', '', NULL, 'Prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_data_fields`
--

CREATE TABLE IF NOT EXISTS `default_data_fields` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `field_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `field_slug` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `field_namespace` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `field_data` blob,
  `view_options` blob,
  `is_locked` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=188 ;

--
-- Volcado de datos para la tabla `default_data_fields`
--

INSERT INTO `default_data_fields` (`id`, `field_name`, `field_slug`, `field_namespace`, `field_type`, `field_data`, `view_options`, `is_locked`) VALUES
(1, 'lang:blog:intro_label', 'intro', 'blogs', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a363a2273696d706c65223b733a31303a22616c6c6f775f74616773223b733a313a2279223b7d, NULL, 'no'),
(2, 'lang:firesale:label_parent', 'parent', 'firesale_categories', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2232223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(3, 'lang:firesale:label_status', 'status', 'firesale_categories', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a35383a2230203a206c616e673a6669726573616c653a6c6162656c5f64726166740a31203a206c616e673a6669726573616c653a6c6162656c5f6c697665223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a303b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(4, 'lang:firesale:label_title', 'title', 'firesale_categories', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(5, 'lang:firesale:label_slug', 'slug', 'firesale_categories', 'slug', 0x613a323a7b733a31303a2273706163655f74797065223b733a313a222d223b733a31303a22736c75675f6669656c64223b733a353a227469746c65223b7d, NULL, 'no'),
(6, 'lang:firesale:label_description', 'description', 'firesale_categories', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a363a2273696d706c65223b733a31303a22616c6c6f775f74616773223b4e3b7d, NULL, 'no'),
(7, 'lang:firesale:label_id', 'code', 'firesale_products', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a36343b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(8, 'lang:firesale:label_title', 'title', 'firesale_products', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(9, 'lang:firesale:label_slug', 'slug', 'firesale_products', 'slug', 0x613a323a7b733a31303a2273706163655f74797065223b733a313a222d223b733a31303a22736c75675f6669656c64223b733a353a227469746c65223b7d, NULL, 'no'),
(10, 'lang:firesale:label_category', 'category', 'firesale_products', 'multiple', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2232223b733a393a2263686f6f73655f7569223b4e3b7d, NULL, 'no'),
(13, 'lang:firesale:label_price', 'price', 'firesale_products', 'decimal', 0x613a343a7b733a31343a22646563696d616c5f706c61636573223b733a313a2230223b733a31333a2264656661756c745f76616c7565223b4e3b733a393a226d696e5f76616c7565223b733a313a2230223b733a393a226d61785f76616c7565223b733a303a22223b7d, NULL, 'no'),
(14, 'lang:firesale:label_price_tax', 'price_tax', 'firesale_products', 'decimal', 0x613a343a7b733a31343a22646563696d616c5f706c61636573223b733a313a2230223b733a31333a2264656661756c745f76616c7565223b4e3b733a393a226d696e5f76616c7565223b733a313a2230223b733a393a226d61785f76616c7565223b733a303a22223b7d, NULL, 'no'),
(15, 'lang:firesale:label_status', 'status', 'firesale_products', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a35383a2230203a206c616e673a6669726573616c653a6c6162656c5f64726166740a31203a206c616e673a6669726573616c653a6c6162656c5f6c697665223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a313b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(16, 'lang:firesale:label_stock', 'stock', 'firesale_products', 'integer', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(17, 'lang:firesale:label_stock_status', 'stock_status', 'firesale_products', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a3231333a2231203a206c616e673a6669726573616c653a6c6162656c5f73746f636b5f696e0a32203a206c616e673a6669726573616c653a6c6162656c5f73746f636b5f6c6f770a33203a206c616e673a6669726573616c653a6c6162656c5f73746f636b5f6f75740a34203a206c616e673a6669726573616c653a6c6162656c5f73746f636b5f6f726465720a35203a206c616e673a6669726573616c653a6c6162656c5f73746f636b5f656e6465640a36203a206c616e673a6669726573616c653a6c6162656c5f73746f636b5f756e6c696d697465640a223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a313b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(18, 'lang:firesale:label_ship_req', 'ship_req', 'firesale_products', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33383a2230203a206c616e673a676c6f62616c3a6e6f0a31203a206c616e673a676c6f62616c3a796573223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a313b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(19, 'lang:firesale:label_description', 'description', 'firesale_products', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a383a22616476616e636564223b733a31303a22616c6c6f775f74616773223b733a313a226e223b7d, NULL, 'no'),
(20, 'lang:firesale:label_type', 'type', 'firesale_product_modifiers', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a3130343a2231203a206c616e673a6669726573616c653a6c6162656c5f6d6f645f76617269616e740a32203a206c616e673a6669726573616c653a6c6162656c5f6d6f645f696e7075740a33203a206c616e673a6669726573616c653a6c6162656c5f6d6f645f73696e676c65223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a313b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(21, 'lang:firesale:label_title', 'title', 'firesale_product_modifiers', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(22, 'lang:firesale:label_inst', 'instructions', 'firesale_product_modifiers', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a363a2273696d706c65223b733a31303a22616c6c6f775f74616773223b4e3b7d, NULL, 'no'),
(23, 'lang:firesale:label_parent', 'parent', 'firesale_product_modifiers', 'integer', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a363b733a31333a2264656661756c745f76616c7565223b693a303b7d, NULL, 'no'),
(24, 'lang:firesale:label_title', 'title', 'firesale_product_variations', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(25, 'lang:firesale:label_mod_price', 'price', 'firesale_product_variations', 'decimal', 0x613a343a7b733a31343a22646563696d616c5f706c61636573223b733a313a2232223b733a31333a2264656661756c745f76616c7565223b4e3b733a393a226d696e5f76616c7565223b4e3b733a393a226d61785f76616c7565223b4e3b7d, NULL, 'no'),
(26, 'lang:firesale:label_parent', 'parent', 'firesale_product_variations', 'integer', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a363b733a31333a2264656661756c745f76616c7565223b693a303b7d, NULL, 'no'),
(27, 'lang:firesale:label_product', 'product', 'firesale_product_variations', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2233223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(28, 'lang:firesale:label_title', 'title', 'firesale_taxes', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3230303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(29, 'lang:firesale:label_description', 'description', 'firesale_taxes', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b4e3b733a31303a22616c6c6f775f74616773223b4e3b7d, NULL, 'no'),
(30, 'lang:firesale:label_tax_band', 'tax_band', 'firesale_products', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2236223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(31, 'lang:firesale:label_title', 'name', 'firesale_gateways', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(32, 'lang:firesale:label_slug', 'slug', 'firesale_gateways', 'slug', 0x613a323a7b733a31303a2273706163655f74797065223b733a313a222d223b733a31303a22736c75675f6669656c64223b733a343a226e616d65223b7d, NULL, 'no'),
(33, 'lang:firesale:label_description', 'desc', 'firesale_gateways', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a383a22616476616e636564223b733a31303a22616c6c6f775f74616773223b4e3b7d, NULL, 'no'),
(34, 'lang:firesale:label_status', 'enabled', 'firesale_gateways', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a35383a2230203a206c616e673a6669726573616c653a6c6162656c5f64726166740a31203a206c616e673a6669726573616c653a6c6162656c5f6c697665223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a303b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(35, 'lang:firesale:label_address_title', 'title', 'firesale_addresses', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(36, 'lang:firesale:label_company', 'company', 'firesale_addresses', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(37, 'lang:firesale:label_firstname', 'firstname', 'firesale_addresses', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3130303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(38, 'lang:firesale:label_lastname', 'lastname', 'firesale_addresses', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3130303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(39, 'lang:firesale:label_email', 'email', 'firesale_addresses', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(40, 'lang:firesale:label_phone', 'phone', 'firesale_addresses', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(41, 'lang:firesale:label_address1', 'address1', 'firesale_addresses', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(42, 'lang:firesale:label_address2', 'address2', 'firesale_addresses', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(46, 'lang:firesale:label_country', 'country', 'firesale_addresses', 'country', 0x613a313a7b733a31353a2264656661756c745f636f756e747279223b733a323a22434f223b7d, NULL, 'no'),
(47, 'lang:firesale:label_cur_code', 'cur_code', 'firesale_currency', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a333b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(48, 'lang:firesale:label_title', 'title', 'firesale_currency', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(49, 'lang:firesale:label_slug', 'slug', 'firesale_currency', 'slug', 0x613a323a7b733a31303a2273706163655f74797065223b733a313a222d223b733a31303a22736c75675f6669656c64223b733a353a227469746c65223b7d, NULL, 'no'),
(50, 'lang:firesale:label_enabled', 'enabled', 'firesale_currency', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a36343a2230203a206c616e673a6669726573616c653a6c6162656c5f64697361626c65640a31203a206c616e673a6669726573616c653a6c6162656c5f656e61626c6564223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a313b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(51, 'lang:firesale:label_cur_tax', 'cur_tax', 'firesale_currency', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(52, 'lang:firesale:label_cur_format', 'cur_format', 'firesale_currency', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a36343b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(53, 'lang:firesale:label_cur_format_dec', 'cur_format_dec', 'firesale_currency', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31323b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(54, 'lang:firesale:label_cur_format_sep', 'cur_format_sep', 'firesale_currency', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31323b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(55, 'lang:firesale:label_cur_format_num', 'cur_format_num', 'firesale_currency', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a3135303a2230203a206c616e673a6669726573616c653a63757272656e63793a666f726d61745f6e6f6e650a31203a206c616e673a6669726573616c653a63757272656e63793a666f726d61745f30300a32203a206c616e673a6669726573616c653a63757272656e63793a666f726d61745f35300a33203a206c616e673a6669726573616c653a63757272656e63793a666f726d61745f39390a223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b733a313a2231223b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(56, 'lang:firesale:label_cur_mod', 'cur_mod', 'firesale_currency', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(57, 'lang:firesale:label_cur_flag', 'image', 'firesale_currency', 'image', 0x613a353a7b733a363a22666f6c646572223b693a313b733a31323a22726573697a655f7769647468223b4e3b733a31333a22726573697a655f686569676874223b4e3b733a31303a226b6565705f726174696f223b4e3b733a31333a22616c6c6f7765645f7479706573223b4e3b7d, NULL, 'no'),
(58, 'lang:firesale:label_exch_rate', 'exch_rate', 'firesale_currency', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(59, 'lang:firesale:label_ip', 'ip', 'firesale_orders', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a33323b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(60, 'lang:firesale:label_gateway', 'gateway', 'firesale_orders', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2237223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(61, 'lang:firesale:label_status', 'order_status', 'firesale_orders', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a3430383a2231203a206c616e673a6669726573616c653a6f72646572733a7374617475735f756e706169640a32203a206c616e673a6669726573616c653a6f72646572733a7374617475735f706169640a33203a206c616e673a6669726573616c653a6f72646572733a7374617475735f646973706174636865640a34203a206c616e673a6669726573616c653a6f72646572733a7374617475735f70726f63657373696e670a35203a206c616e673a6669726573616c653a6f72646572733a7374617475735f726566756e6465640a36203a206c616e673a6669726573616c653a6f72646572733a7374617475735f63616e63656c6c65640a37203a206c616e673a6669726573616c653a6f72646572733a7374617475735f6661696c65640a38203a206c616e673a6669726573616c653a6f72646572733a7374617475735f6465636c696e65640a39203a206c616e673a6669726573616c653a6f72646572733a7374617475735f6d69736d617463680a3130203a206c616e673a6669726573616c653a6f72646572733a7374617475735f70726566756e646564223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b733a313a2231223b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(62, 'lang:firesale:label_price_sub', 'price_sub', 'firesale_orders', 'decimal', 0x613a343a7b733a31343a22646563696d616c5f706c61636573223b733a313a2230223b733a31333a2264656661756c745f76616c7565223b4e3b733a393a226d696e5f76616c7565223b733a313a2230223b733a393a226d61785f76616c7565223b733a303a22223b7d, NULL, 'no'),
(63, 'lang:firesale:label_price_ship', 'price_ship', 'firesale_orders', 'decimal', 0x613a343a7b733a31343a22646563696d616c5f706c61636573223b733a313a2230223b733a31333a2264656661756c745f76616c7565223b4e3b733a393a226d696e5f76616c7565223b733a313a2230223b733a393a226d61785f76616c7565223b733a303a22223b7d, NULL, 'no'),
(64, 'lang:firesale:label_price_total', 'price_total', 'firesale_orders', 'decimal', 0x613a343a7b733a31343a22646563696d616c5f706c61636573223b733a313a2230223b733a31333a2264656661756c745f76616c7565223b4e3b733a393a226d696e5f76616c7565223b733a313a2230223b733a393a226d61785f76616c7565223b733a303a22223b7d, NULL, 'no'),
(65, 'lang:firesale:sections:currency', 'currency', 'firesale_orders', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2239223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(66, 'lang:firesale:label_exch_rate', 'exchange_rate', 'firesale_orders', 'decimal', 0x613a343a7b733a31343a22646563696d616c5f706c61636573223b733a313a2232223b733a31333a2264656661756c745f76616c7565223b4e3b733a393a226d696e5f76616c7565223b733a343a22302e3030223b733a393a226d61785f76616c7565223b4e3b7d, NULL, 'no'),
(67, 'lang:firesale:label_ship_to', 'ship_to', 'firesale_orders', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2238223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(68, 'lang:firesale:label_bill_to', 'bill_to', 'firesale_orders', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2238223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(70, 'lang:firesale:label_order', 'order_id', 'firesale_orders_items', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a323a223130223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(71, 'lang:firesale:label_product', 'product_id', 'firesale_orders_items', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2233223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(72, 'lang:firesale:label_id', 'code', 'firesale_orders_items', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a36343b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(73, 'lang:firesale:label_title', 'name', 'firesale_orders_items', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(74, 'lang:firesale:label_price', 'price', 'firesale_orders_items', 'decimal', 0x613a343a7b733a31343a22646563696d616c5f706c61636573223b733a313a2232223b733a31333a2264656661756c745f76616c7565223b4e3b733a393a226d696e5f76616c7565223b4e3b733a393a226d61785f76616c7565223b4e3b7d, NULL, 'no'),
(75, 'lang:firesale:label_quantity', 'qty', 'firesale_orders_items', 'integer', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(76, 'lang:firesale:label_tax_band', 'tax_band', 'firesale_orders_items', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2236223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(77, 'lang:firesale:label_title', 'title', 'firesale_routes', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(78, 'lang:firesale:label_slug', 'slug', 'firesale_routes', 'slug', 0x613a323a7b733a31303a2273706163655f74797065223b733a313a222d223b733a31303a22736c75675f6669656c64223b733a353a227469746c65223b7d, NULL, 'no'),
(79, 'lang:firesale:label_table', 'table', 'firesale_routes', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(80, 'lang:firesale:label_map', 'map', 'firesale_routes', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(81, 'lang:firesale:label_route', 'route', 'firesale_routes', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(82, 'lang:firesale:label_translation', 'translation', 'firesale_routes', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3132383b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(83, 'lang:firesale:label_use_https', 'https', 'firesale_routes', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33383a2230203a206c616e673a676c6f62616c3a6e6f0a31203a206c616e673a676c6f62616c3a796573223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a303b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(84, 'lang:firesale:label_title', 'title', 'firesale_brands', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(85, 'lang:firesale:label_slug', 'slug', 'firesale_brands', 'slug', 0x613a323a7b733a31303a2273706163655f74797065223b733a313a222d223b733a31303a22736c75675f6669656c64223b733a353a227469746c65223b7d, NULL, 'no'),
(86, 'lang:firesale:label_status', 'status', 'firesale_brands', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a35383a2230203a206c616e673a6669726573616c653a6c6162656c5f64726166740a31203a206c616e673a6669726573616c653a6c6162656c5f6c697665223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a313b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(87, 'lang:firesale:label_description', 'description', 'firesale_brands', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a363a2273696d706c65223b733a31303a22616c6c6f775f74616773223b4e3b7d, NULL, 'no'),
(88, 'lang:firesale:label_brand', 'brand', 'firesale_products', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a323a223133223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(89, 'lang:firesale:design:label_element', 'element', 'firesale_design', 'integer', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(90, 'lang:firesale:design:label_type', 'type', 'firesale_design', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(91, 'lang:firesale:design:enable', 'enabled', 'firesale_design', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33383a2230203a206c616e673a676c6f62616c3a6e6f0a31203a206c616e673a676c6f62616c3a796573223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a313b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(92, 'lang:firesale:design:label_layout', 'layout', 'firesale_design', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(93, 'lang:firesale:design:label_view', 'view', 'firesale_design', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(94, 'lang:firesale:field:digital', 'digital', 'firesale_products', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a32353a2279203a205965730a09090909090909090909096e203a204e6f223b733a31313a2263686f6963655f74797065223b733a353a22726164696f223b733a31333a2264656661756c745f76616c7565223b733a313a226e223b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(95, 'lang:firesale:field:download', 'download', 'firesale_products', 'firesale_digital', 0x4e3b, NULL, 'no'),
(96, 'lang:firesale:field:downloads', 'downloads', 'firesale_products', 'integer', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31313b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(97, 'lang:firesale:field:expires_after', 'expires_after', 'firesale_products', 'integer', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31313b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(98, 'expires_interval', 'expires_interval', 'firesale_products', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a3233323a22686f757273203a206c616e673a6669726573616c653a696e74657276616c3a686f7572730a09090909090909090909202064617973203a206c616e673a6669726573616c653a696e74657276616c3a646179730a0909090909090909090920207765656b73203a206c616e673a6669726573616c653a696e74657276616c3a7765656b730a0909090909090909090920206d6f6e746873203a206c616e673a6669726573616c653a696e74657276616c3a6d6f6e7468730a0909090909090909090920207965617273203a206c616e673a6669726573616c653a696e74657276616c3a7965617273223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b4e3b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(99, 'lang:fs_discount_codes:code', 'code', 'firesale_discount_codes', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a35303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(100, 'lang:fs_discount_codes:type', 'type', 'firesale_discount_codes', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a35353a226669786564203a2046697865640a20202020202020202020202020202020090970657263656e74616765203a2050657263656e74616765223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b733a31303a2270657263656e74616765223b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(101, 'lang:fs_discount_codes:value', 'value', 'firesale_discount_codes', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(102, 'lang:fs_discount_codes:desc', 'desc', 'firesale_discount_codes', 'textarea', 0x613a333a7b733a31323a2264656661756c745f74657874223b4e3b733a31303a22616c6c6f775f74616773223b4e3b733a31323a22636f6e74656e745f74797065223b4e3b7d, NULL, 'no'),
(103, 'lang:fs_discount_codes:applies_to', 'applies_to', 'firesale_discount_codes', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a3135363a2230203a206c616e673a66735f646973636f756e745f636f6465733a65766572797468696e670a20202020202020202020202020202020090931203a206c616e673a66735f646973636f756e745f636f6465733a73656c65637465645f70726f64730a20202020202020202020202020202020090932203a206c616e673a66735f646973636f756e745f636f6465733a73656c65637465645f63617473223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b733a313a2230223b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(104, 'lang:fs_discount_codes:products', 'products', 'firesale_discount_codes', 'multiple', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2233223b733a393a2263686f6f73655f7569223b4e3b7d, NULL, 'no'),
(105, 'lang:fs_discount_codes:categories', 'categories', 'firesale_discount_codes', 'multiple', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2232223b733a393a2263686f6f73655f7569223b4e3b7d, NULL, 'no'),
(106, 'lang:fs_discount_codes:usage', 'usage', 'firesale_discount_codes', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a3136343a226f6e6365203a206c616e673a66735f646973636f756e745f636f6465733a6f6e63650a2020202020202020202020202020202020202020202020206f6e63655f75736572203a206c616e673a66735f646973636f756e745f636f6465733a6f6e63655f757365720a2020202020202020202020202020202009096d756c7469706c65203a206c616e673a66735f646973636f756e745f636f6465733a6d756c7469706c65223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b733a343a226f6e6365223b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(107, 'lang:fs_discount_codes:start_date', 'start', 'firesale_discount_codes', 'datetime', 0x613a353a7b733a383a227573655f74696d65223b623a303b733a31303a2273746172745f64617465223b4e3b733a383a22656e645f64617465223b4e3b733a373a2273746f72616765223b733a343a22756e6978223b733a31303a22696e7075745f74797065223b733a31303a22646174657069636b6572223b7d, NULL, 'no'),
(108, 'lang:fs_discount_codes:end_date', 'end', 'firesale_discount_codes', 'datetime', 0x613a353a7b733a383a227573655f74696d65223b623a303b733a31303a2273746172745f64617465223b4e3b733a383a22656e645f64617465223b4e3b733a373a2273746f72616765223b733a343a22756e6978223b733a31303a22696e7075745f74797065223b733a31303a22646174657069636b6572223b7d, NULL, 'no'),
(109, 'lang:fs_discount_codes:used', 'used', 'firesale_discount_codes', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a38343a2279203a206c616e673a66735f646973636f756e745f636f6465733a7965730a2020202020202020202020202020202020202020202020206e203a206c616e673a66735f646973636f756e745f636f6465733a6e6f223b733a31313a2263686f6963655f74797065223b733a353a22726164696f223b733a31333a2264656661756c745f76616c7565223b733a313a226e223b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(110, 'lang:fs_discount_codes:code_used', 'code_used', 'firesale_orders', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a323a223135223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(111, 'Product', 'product', 'firesale_reviews', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a313a2233223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(112, 'Status', 'status', 'firesale_reviews', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a35383a2230203a206c616e673a6669726573616c653a6c6162656c5f64726166740a31203a206c616e673a6669726573616c653a6c6162656c5f6c697665223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a303b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(113, 'Review Title', 'title', 'firesale_reviews', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3136303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(114, 'Review', 'review', 'firesale_reviews', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a363a2273696d706c65223b733a31303a22616c6c6f775f74616773223b733a313a226e223b7d, NULL, 'no'),
(115, 'Rating', 'rating_1', 'firesale_reviews', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a3130393a22302e30203a20302e300a302e35203a20302e350a312e30203a20312e300a312e35203a20312e350a322e30203a20322e300a322e35203a20322e350a332e30203a20332e300a332e35203a20332e350a342e30203a20342e300a342e35203a20342e350a352e30203a20352e30223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b643a322e353b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(116, 'lang:firesale:seo:label_title', 'meta_title', 'firesale_products', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(117, 'lang:firesale:seo:label_desc', 'meta_description', 'firesale_products', 'textarea', 0x613a333a7b733a31323a2264656661756c745f74657874223b4e3b733a31303a22616c6c6f775f74616773223b4e3b733a31323a22636f6e74656e745f74797065223b4e3b7d, NULL, 'no'),
(118, 'lang:firesale:seo:label_keywords', 'meta_keywords', 'firesale_products', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(119, 'lang:firesale:seo:label_title', 'meta_title', 'firesale_categories', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(120, 'lang:firesale:seo:label_desc', 'meta_description', 'firesale_categories', 'textarea', 0x613a333a7b733a31323a2264656661756c745f74657874223b4e3b733a31303a22616c6c6f775f74616773223b4e3b733a31323a22636f6e74656e745f74797065223b4e3b7d, NULL, 'no'),
(121, 'lang:firesale:seo:label_keywords', 'meta_keywords', 'firesale_categories', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(122, 'lang:firesale:label_weight_kg', 'shipping_weight', 'firesale_products', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(123, 'lang:firesale:label_height_cm', 'shipping_height', 'firesale_products', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(124, 'lang:firesale:label_width_cm', 'shipping_width', 'firesale_products', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(125, 'lang:firesale:label_depth_cm', 'shipping_depth', 'firesale_products', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(126, 'lang:firesale:label_title', 'title', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(127, 'lang:firesale:label_slug', 'slug', 'firesale_shipping', 'slug', 0x613a323a7b733a31303a2273706163655f74797065223b733a313a222d223b733a31303a22736c75675f6669656c64223b733a353a227469746c65223b7d, NULL, 'no'),
(128, 'lang:firesale:label_courier', 'company', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(129, 'lang:firesale:label_status', 'status', 'firesale_shipping', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a35383a2230203a206c616e673a6669726573616c653a6c6162656c5f64726166740a31203a206c616e673a6669726573616c653a6c6162656c5f6c697665223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a303b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(130, 'lang:firesale:label_price', 'price', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(131, 'lang:firesale:label_price_min', 'price_min', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(132, 'lang:firesale:label_price_max', 'price_max', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(133, 'lang:firesale:label_weight_min', 'weight_min', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(134, 'lang:firesale:label_weight_max', 'weight_max', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a31303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(135, 'lang:firesale:label_description', 'description', 'firesale_shipping', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a363a2273696d706c65223b733a31303a22616c6c6f775f74616773223b4e3b7d, NULL, 'no'),
(136, 'lang:firesale:label_shipping', 'shipping', 'firesale_orders', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a323a223137223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(137, 'lang:firesale:wish:label:title', 'title', 'firesale_wishlists', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3235353b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(138, 'lang:firesale:wish:label:status', 'status', 'firesale_wishlists', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a35383a2230203a206c616e673a6669726573616c653a6c6162656c5f64726166740a31203a206c616e673a6669726573616c653a6c6162656c5f6c697665223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a303b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(139, 'lang:firesale:wish:label:privacy', 'privacy', 'firesale_wishlists', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33363a2231203a206c616e673a6669726573616c653a776973683a6c6162656c3a70726976617465223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b4e3b733a31313a226d696e5f63686f69636573223b733a303a22223b733a31313a226d61785f63686f69636573223b733a303a22223b7d, NULL, 'no'),
(140, 'lang:firesale:wish:label:desc', 'description', 'firesale_wishlists', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a363a2273696d706c65223b733a31303a22616c6c6f775f74616773223b4e3b7d, NULL, 'no'),
(141, 'lang:pages:body_label', 'body', 'pages', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a383a22616476616e636564223b733a31303a22616c6c6f775f74616773223b733a313a2279223b7d, NULL, 'no'),
(142, 'lang:user:first_name_label', 'first_name', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a35303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(143, 'lang:user:last_name_label', 'last_name', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a35303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(146, 'lang:user:lang', 'lang', 'users', 'pyro_lang', 0x613a313a7b733a31323a2266696c7465725f7468656d65223b733a333a22796573223b7d, NULL, 'no'),
(147, 'lang:profile_dob', 'dob', 'users', 'datetime', 0x613a353a7b733a383a227573655f74696d65223b733a323a226e6f223b733a31303a2273746172745f64617465223b733a353a222d31303059223b733a383a22656e645f64617465223b4e3b733a373a2273746f72616765223b733a343a22756e6978223b733a31303a22696e7075745f74797065223b733a383a2264726f70646f776e223b7d, NULL, 'no'),
(148, 'lang:profile_gender', 'gender', 'users', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33343a22203a204e6f742054656c6c696e670a6d203a204d616c650a66203a2046656d616c65223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b4e3b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(149, 'lang:profile_phone', 'phone', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a32303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(155, 'lang:profile_website', 'website', 'users', 'url', NULL, NULL, 'no'),
(156, 'lang:firesale:featured', 'featured', 'firesale_products', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33383a2230203a206c616e673a676c6f62616c3a6e6f0a31203a206c616e673a676c6f62616c3a796573223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a303b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(157, 'lang:firesale:featured', 'featured', 'firesale_categories', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33383a2230203a206c616e673a676c6f62616c3a6e6f0a31203a206c616e673a676c6f62616c3a796573223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a303b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(158, 'lang:firesale:featured', 'featured', 'firesale_brands', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33383a2230203a206c616e673a676c6f62616c3a6e6f0a31203a206c616e673a676c6f62616c3a796573223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b693a303b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(160, 'Descuento', 'descuento', 'firesale_products', 'integer', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(161, 'Imagen', 'image', 'blogs', 'image', 0x613a353a7b733a363a22666f6c646572223b733a323a223636223b733a31323a22726573697a655f7769647468223b733a333a22353030223b733a31333a22726573697a655f686569676874223b733a333a22353030223b733a31303a226b6565705f726174696f223b733a333a22796573223b733a31333a22616c6c6f7765645f7479706573223b733a31323a226a70677c6a7065677c706e67223b7d, NULL, 'no'),
(162, 'lang:chat:day', 'day', 'chat', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a3234343a220d0a20202020202020202020202020202020202020204d6f6e203a204c756e65730d0a2020202020202020202020202020202020202020547565203a204d61727465730d0a2020202020202020202020202020202020202020576564203a204d69c3a972636f6c65730d0a2020202020202020202020202020202020202020546875203a204a75657665730d0a2020202020202020202020202020202020202020467269203a20566965726e65730d0a2020202020202020202020202020202020202020536174203a2053c3a16261646f0d0a202020202020202020202020202020202020202053756e203a20446f6d696e676f223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b4e3b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(163, 'lang:chat:start_hour', 'start_hour', 'chat', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(164, 'lang:chat:end_hour', 'end_hour', 'chat', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(165, 'lang:name_label', 'assign_name', 'chat', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(166, 'lang:global:email', 'assign_email', 'chat', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(167, 'lang:chat:user', 'assign_user_id', 'chat', 'user', 0x613a313a7b733a31343a2272657374726963745f67726f7570223b4e3b7d, NULL, 'no'),
(168, 'lang:chat:status', 'assign_status', 'chat', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33323a2230203a20300d0a202020202020202020202020202020202020202031203a2031223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b4e3b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(169, 'lang:chat:start_date', 'assign_start_on', 'chat', 'datetime', 0x613a353a7b733a383a227573655f74696d65223b4e3b733a31303a2273746172745f64617465223b4e3b733a383a22656e645f64617465223b4e3b733a373a2273746f72616765223b4e3b733a31303a22696e7075745f74797065223b4e3b7d, NULL, 'no'),
(170, 'lang:chat:end_date', 'assign_finished_on', 'chat', 'datetime', 0x613a353a7b733a383a227573655f74696d65223b4e3b733a31303a2273746172745f64617465223b4e3b733a383a22656e645f64617465223b4e3b733a373a2273746f72616765223b4e3b733a31303a22696e7075745f74797065223b4e3b7d, NULL, 'no'),
(171, 'lang:chat:message', 'message_text', 'chat', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(172, 'lang:chat:assing', 'message_assign_id', 'chat', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b623a303b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(173, 'lang:chat:sender', 'message_sender', 'chat', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a35303a22636c69656e74203a20636c69656e740d0a202020202020202020202020202020202020202061646d696e203a2061646d696e223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b4e3b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(174, 'lang:chat:username', 'message_username', 'chat', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(175, 'lang:firesale:label_main_image', 'main_image', 'firesale_products', 'image', 0x613a353a7b733a363a22666f6c646572223b733a333a22313038223b733a31323a22726573697a655f7769647468223b733a303a22223b733a31333a22726573697a655f686569676874223b733a333a22333030223b733a31303a226b6565705f726174696f223b733a333a22796573223b733a31333a22616c6c6f7765645f7479706573223b733a31323a226a70677c6a7065677c706e67223b7d, NULL, 'no'),
(176, 'code', 'code', 'firesale_department', 'textarea', 0x613a333a7b733a31323a2264656661756c745f74657874223b4e3b733a31303a22616c6c6f775f74616773223b4e3b733a31323a22636f6e74656e745f74797065223b4e3b7d, NULL, 'no'),
(177, 'name', 'name', 'firesale_department', 'textarea', 0x613a333a7b733a31323a2264656661756c745f74657874223b4e3b733a31303a22616c6c6f775f74616773223b4e3b733a31323a22636f6e74656e745f74797065223b4e3b7d, NULL, 'no'),
(178, 'code', 'code', 'firesale_city', 'textarea', 0x613a333a7b733a31323a2264656661756c745f74657874223b4e3b733a31303a22616c6c6f775f74616773223b4e3b733a31323a22636f6e74656e745f74797065223b4e3b7d, NULL, 'no'),
(179, 'name', 'name', 'firesale_city', 'textarea', 0x613a333a7b733a31323a2264656661756c745f74657874223b4e3b733a31303a22616c6c6f775f74616773223b4e3b733a31323a22636f6e74656e745f74797065223b4e3b7d, NULL, 'no'),
(180, 'deparment', 'deparment', 'firesale_city', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a323a223236223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(181, 'lang:firesale:label_deparment', 'id_deparment', 'firesale_addresses', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a323a223236223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(182, 'lang:firesale:label_city', 'id_city', 'firesale_addresses', 'relationship', 0x613a323a7b733a31333a2263686f6f73655f73747265616d223b733a323a223237223b733a383a226c696e6b5f757269223b4e3b7d, NULL, 'no'),
(183, 'lang:firesale:label_cc_address', 'cedule', 'firesale_addresses', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(184, 'lang:firesale:label_observation', 'observation', 'firesale_addresses', 'textarea_limited', 0x613a343a7b733a31323a2264656661756c745f74657874223b733a303a22223b733a31303a22616c6c6f775f74616773223b733a313a226e223b733a31323a22636f6e74656e745f74797065223b733a343a2274657874223b733a31353a226368617261637465725f6c696d6974223b733a333a22353030223b7d, NULL, 'no'),
(185, 'lang:firesale:label_price_ue', 'price_ue', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(186, 'lang:firesale:label_tax_percentage', 'tax_percentage', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(187, 'lang:firesale:type_packaging', 'type_packaging', 'firesale_shipping', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_data_field_assignments`
--

CREATE TABLE IF NOT EXISTS `default_data_field_assignments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `is_required` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `is_unique` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `instructions` text COLLATE utf8_unicode_ci,
  `field_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=188 ;

--
-- Volcado de datos para la tabla `default_data_field_assignments`
--

INSERT INTO `default_data_field_assignments` (`id`, `sort_order`, `stream_id`, `field_id`, `is_required`, `is_unique`, `instructions`, `field_name`) VALUES
(1, 1, 1, 1, 'yes', 'no', '', NULL),
(2, 1, 2, 2, 'no', 'no', NULL, NULL),
(3, 2, 2, 3, 'yes', 'no', NULL, NULL),
(4, 3, 2, 4, 'yes', 'no', '', NULL),
(5, 3, 2, 5, 'yes', 'no', '', NULL),
(6, 3, 2, 6, 'no', 'no', NULL, NULL),
(7, 1, 3, 7, 'yes', 'yes', NULL, NULL),
(8, 2, 3, 8, 'yes', 'yes', NULL, NULL),
(9, 3, 3, 9, 'yes', 'yes', NULL, NULL),
(10, 3, 3, 10, 'yes', 'no', NULL, NULL),
(13, 6, 3, 13, 'yes', 'no', 'lang:firesale:inst_price', NULL),
(14, 7, 3, 14, 'yes', 'no', '', NULL),
(15, 7, 3, 15, 'yes', 'no', NULL, NULL),
(16, 7, 3, 16, 'yes', 'no', NULL, NULL),
(17, 7, 3, 17, 'yes', 'no', NULL, NULL),
(18, 7, 3, 18, 'yes', 'no', NULL, NULL),
(19, 8, 3, 19, 'no', 'no', '', NULL),
(20, 1, 4, 20, 'yes', 'no', NULL, NULL),
(21, 2, 4, 21, 'yes', 'no', NULL, NULL),
(22, 3, 4, 22, 'no', 'no', NULL, NULL),
(23, 3, 4, 23, 'yes', 'no', NULL, NULL),
(24, 1, 5, 24, 'yes', 'no', NULL, NULL),
(25, 2, 5, 25, 'yes', 'no', 'lang:firesale:label_mod_price_inst', NULL),
(26, 3, 5, 26, 'yes', 'no', NULL, NULL),
(27, 3, 5, 27, 'no', 'no', NULL, NULL),
(28, 1, 6, 28, 'yes', 'no', NULL, NULL),
(29, 2, 6, 29, 'yes', 'no', NULL, NULL),
(30, 9, 3, 30, 'yes', 'no', NULL, NULL),
(31, 1, 7, 31, 'yes', 'yes', NULL, NULL),
(32, 2, 7, 32, 'yes', 'no', NULL, NULL),
(33, 3, 7, 33, 'yes', 'no', NULL, NULL),
(34, 3, 7, 34, 'yes', 'no', NULL, NULL),
(35, 1, 8, 35, 'no', 'no', NULL, NULL),
(36, 2, 8, 36, 'no', 'no', NULL, NULL),
(37, 3, 8, 37, 'yes', 'no', NULL, NULL),
(38, 3, 8, 38, 'yes', 'no', NULL, NULL),
(39, 4, 8, 39, 'yes', 'no', NULL, NULL),
(40, 5, 8, 40, 'no', 'no', NULL, NULL),
(41, 6, 8, 41, 'yes', 'no', NULL, NULL),
(42, 7, 8, 42, 'no', 'no', NULL, NULL),
(46, 7, 8, 46, 'no', 'no', '', NULL),
(47, 1, 9, 47, 'yes', 'no', 'lang:firesale:label_cur_code_inst', NULL),
(48, 2, 9, 48, 'yes', 'yes', NULL, NULL),
(49, 3, 9, 49, 'yes', 'no', NULL, NULL),
(50, 3, 9, 50, 'yes', 'no', NULL, NULL),
(51, 3, 9, 51, 'yes', 'no', NULL, NULL),
(52, 4, 9, 52, 'yes', 'no', 'lang:firesale:label_cur_format_inst', NULL),
(53, 5, 9, 53, 'no', 'no', NULL, NULL),
(54, 6, 9, 54, 'yes', 'no', NULL, NULL),
(55, 7, 9, 55, 'yes', 'no', NULL, NULL),
(56, 7, 9, 56, 'yes', 'no', 'lang:firesale:label_cur_mod_inst', NULL),
(57, 7, 9, 57, 'no', 'no', NULL, NULL),
(58, 7, 9, 58, 'no', 'no', 'lang:firesale:label_exch_rate_inst', NULL),
(59, 1, 10, 59, 'no', 'no', NULL, NULL),
(60, 2, 10, 60, 'no', 'no', NULL, NULL),
(61, 3, 10, 61, 'yes', 'no', NULL, NULL),
(62, 3, 10, 62, 'no', 'no', '', NULL),
(63, 3, 10, 63, 'no', 'no', '', NULL),
(64, 4, 10, 64, 'no', 'no', '', NULL),
(65, 5, 10, 65, 'yes', 'no', NULL, NULL),
(66, 6, 10, 66, 'yes', 'no', NULL, NULL),
(67, 7, 10, 67, 'no', 'no', NULL, NULL),
(68, 7, 10, 68, 'no', 'no', NULL, NULL),
(69, 7, 10, 69, 'no', 'no', NULL, NULL),
(70, 1, 11, 70, 'yes', 'no', NULL, NULL),
(71, 2, 11, 71, 'yes', 'no', NULL, NULL),
(72, 3, 11, 72, 'yes', 'yes', NULL, NULL),
(73, 3, 11, 73, 'yes', 'yes', NULL, NULL),
(74, 3, 11, 74, 'yes', 'no', NULL, NULL),
(75, 4, 11, 75, 'no', 'no', NULL, NULL),
(76, 5, 11, 76, 'yes', 'no', NULL, NULL),
(77, 1, 12, 77, 'yes', 'yes', NULL, NULL),
(78, 2, 12, 78, 'yes', 'yes', NULL, NULL),
(79, 3, 12, 79, 'no', 'no', NULL, NULL),
(80, 3, 12, 80, 'yes', 'yes', NULL, NULL),
(81, 3, 12, 81, 'yes', 'yes', NULL, NULL),
(82, 4, 12, 82, 'yes', 'yes', NULL, NULL),
(83, 5, 12, 83, 'yes', 'no', NULL, NULL),
(84, 1, 13, 84, 'yes', 'yes', NULL, NULL),
(85, 2, 13, 85, 'yes', 'no', NULL, NULL),
(86, 3, 13, 86, 'yes', 'no', NULL, NULL),
(87, 3, 13, 87, 'no', 'no', NULL, NULL),
(88, 10, 3, 88, 'no', 'no', NULL, NULL),
(89, 1, 14, 89, 'yes', 'no', NULL, NULL),
(90, 2, 14, 90, 'yes', 'no', NULL, NULL),
(91, 3, 14, 91, 'yes', 'no', NULL, NULL),
(92, 3, 14, 92, 'yes', 'no', NULL, NULL),
(93, 3, 14, 93, 'yes', 'no', NULL, NULL),
(94, 11, 3, 94, 'yes', 'no', 'lang:firesale:instructions:digital', NULL),
(95, 12, 3, 95, 'no', 'no', 'lang:firesale:instructions:download', NULL),
(96, 13, 3, 96, 'no', 'no', 'lang:firesale:instructions:downloads', NULL),
(97, 14, 3, 97, 'no', 'no', 'lang:firesale:instructions:expires_after', NULL),
(98, 15, 3, 98, 'no', 'no', NULL, NULL),
(99, 1, 15, 99, 'yes', 'yes', NULL, NULL),
(100, 2, 15, 100, 'yes', 'no', NULL, NULL),
(101, 3, 15, 101, 'yes', 'no', 'lang:fs_discount_codes:value_info', NULL),
(102, 3, 15, 102, 'yes', 'no', NULL, NULL),
(103, 3, 15, 103, 'yes', 'no', NULL, NULL),
(104, 4, 15, 104, 'no', 'no', NULL, NULL),
(105, 5, 15, 105, 'no', 'no', NULL, NULL),
(106, 6, 15, 106, 'yes', 'no', NULL, NULL),
(107, 7, 15, 107, 'yes', 'no', NULL, NULL),
(108, 7, 15, 108, 'yes', 'no', NULL, NULL),
(109, 7, 15, 109, 'no', 'no', NULL, NULL),
(110, 7, 10, 110, 'no', 'no', NULL, NULL),
(111, 1, 16, 111, 'no', 'no', NULL, NULL),
(112, 2, 16, 112, 'no', 'no', NULL, NULL),
(113, 3, 16, 113, 'no', 'no', NULL, NULL),
(114, 3, 16, 114, 'no', 'no', '', NULL),
(115, 3, 16, 115, 'yes', 'no', NULL, NULL),
(116, 16, 3, 116, 'no', 'yes', NULL, NULL),
(117, 17, 3, 117, 'no', 'no', NULL, NULL),
(118, 18, 3, 118, 'no', 'no', NULL, NULL),
(119, 4, 2, 119, 'no', 'yes', NULL, NULL),
(120, 5, 2, 120, 'no', 'no', NULL, NULL),
(121, 6, 2, 121, 'no', 'no', NULL, NULL),
(122, 19, 3, 122, 'no', 'no', NULL, NULL),
(123, 20, 3, 123, 'no', 'no', NULL, NULL),
(124, 21, 3, 124, 'no', 'no', NULL, NULL),
(125, 22, 3, 125, 'no', 'no', NULL, NULL),
(126, 1, 17, 126, 'yes', 'yes', NULL, NULL),
(127, 2, 17, 127, 'yes', 'yes', NULL, NULL),
(128, 3, 17, 128, 'yes', 'no', NULL, NULL),
(129, 3, 17, 129, 'yes', 'no', NULL, NULL),
(130, 3, 17, 130, 'yes', 'no', NULL, NULL),
(131, 5, 17, 131, 'no', 'no', NULL, NULL),
(132, 6, 17, 132, 'no', 'no', NULL, NULL),
(133, 7, 17, 133, 'no', 'no', NULL, NULL),
(134, 7, 17, 134, 'no', 'no', NULL, NULL),
(135, 7, 17, 135, 'yes', 'no', NULL, NULL),
(136, 7, 10, 136, 'no', 'no', NULL, NULL),
(137, 1, 18, 137, 'no', 'yes', NULL, NULL),
(138, 2, 18, 138, 'no', 'no', NULL, NULL),
(139, 3, 18, 139, 'no', 'no', '', NULL),
(140, 3, 18, 140, 'no', 'no', NULL, NULL),
(141, 1, 19, 141, 'no', 'no', NULL, NULL),
(142, 1, 20, 142, 'yes', 'no', NULL, NULL),
(143, 2, 20, 143, 'yes', 'no', NULL, NULL),
(146, 3, 20, 146, 'no', 'no', NULL, NULL),
(147, 4, 20, 147, 'no', 'no', NULL, NULL),
(148, 5, 20, 148, 'no', 'no', NULL, NULL),
(149, 6, 20, 149, 'no', 'no', NULL, NULL),
(155, 7, 20, 155, 'no', 'no', NULL, NULL),
(156, 23, 3, 156, 'no', 'no', NULL, NULL),
(157, 7, 2, 157, 'no', 'no', NULL, NULL),
(158, 3, 13, 158, 'no', 'no', NULL, NULL),
(160, 7, 3, 160, 'no', 'no', 'Indique el % de descuento', NULL),
(161, 2, 1, 161, 'no', 'no', '', NULL),
(162, 1, 21, 162, 'yes', 'no', NULL, NULL),
(163, 2, 21, 163, 'yes', 'no', NULL, NULL),
(164, 3, 21, 164, 'yes', 'no', NULL, NULL),
(165, 1, 22, 165, 'yes', 'no', NULL, NULL),
(166, 2, 22, 166, 'yes', 'no', NULL, NULL),
(167, 3, 22, 167, 'yes', 'no', NULL, NULL),
(168, 3, 22, 168, 'yes', 'no', NULL, NULL),
(169, 3, 22, 169, 'yes', 'no', NULL, NULL),
(170, 4, 22, 170, 'yes', 'no', NULL, NULL),
(171, 1, 23, 171, 'yes', 'no', NULL, NULL),
(172, 2, 23, 172, 'yes', 'no', NULL, NULL),
(173, 3, 23, 173, 'yes', 'no', NULL, NULL),
(174, 3, 23, 174, 'yes', 'no', NULL, NULL),
(175, 24, 3, 175, 'no', 'no', 'Imagen principal del producto', NULL),
(176, 1, 26, 176, 'no', 'no', '', NULL),
(177, 2, 26, 177, 'no', 'no', '', NULL),
(178, 1, 27, 178, 'no', 'no', '', NULL),
(179, 2, 27, 179, 'no', 'no', '', NULL),
(180, 3, 27, 180, 'no', 'no', '', NULL),
(181, 7, 8, 181, 'yes', 'no', '', NULL),
(182, 7, 8, 182, 'yes', 'no', '', NULL),
(183, 3, 8, 183, 'no', 'no', '', NULL),
(184, 7, 8, 184, 'no', 'no', '', NULL),
(185, 4, 17, 185, 'yes', 'no', '', NULL),
(186, 7, 17, 186, 'yes', 'no', '', NULL),
(187, 7, 17, 187, 'no', 'no', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_data_streams`
--

CREATE TABLE IF NOT EXISTS `default_data_streams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `stream_slug` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `stream_namespace` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stream_prefix` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_options` blob NOT NULL,
  `title_column` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sorting` enum('title','custom') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'title',
  `permissions` text COLLATE utf8_unicode_ci,
  `is_hidden` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `menu_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Volcado de datos para la tabla `default_data_streams`
--

INSERT INTO `default_data_streams` (`id`, `stream_name`, `stream_slug`, `stream_namespace`, `stream_prefix`, `about`, `view_options`, `title_column`, `sorting`, `permissions`, `is_hidden`, `menu_path`) VALUES
(1, 'lang:blog:blog_title', 'blog', 'blogs', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, NULL, 'title', NULL, 'no', NULL),
(2, 'Categories', 'firesale_categories', 'firesale_categories', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'slug', 'title', NULL, 'no', NULL),
(3, 'Products', 'firesale_products', 'firesale_products', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(4, 'Modifiers', 'firesale_product_modifiers', 'firesale_product_modifiers', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(5, 'Variations', 'firesale_product_variations', 'firesale_product_variations', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(6, 'Taxes', 'firesale_taxes', 'firesale_taxes', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(7, 'Gateways', 'firesale_gateways', 'firesale_gateways', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'name', 'title', NULL, 'no', NULL),
(8, 'Addresses', 'firesale_addresses', 'firesale_addresses', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(9, 'Currency', 'firesale_currency', 'firesale_currency', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(10, 'Orders', 'firesale_orders', 'firesale_orders', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, NULL, 'title', NULL, 'no', NULL),
(11, 'Order Items', 'firesale_orders_items', 'firesale_orders_items', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'name', 'title', NULL, 'no', NULL),
(12, 'Routes', 'firesale_routes', 'firesale_routes', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(13, 'Brands', 'firesale_brands', 'firesale_brands', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(14, 'FireSale Design', 'firesale_design', 'firesale_design', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, NULL, 'title', NULL, 'no', NULL),
(15, 'Discount Codes', 'firesale_discount_codes', 'firesale_discount_codes', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'code', 'title', NULL, 'no', NULL),
(16, 'Product Reviews', 'firesale_reviews', 'firesale_reviews', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(17, 'Shipping', 'firesale_shipping', 'firesale_shipping', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'title', 'title', NULL, 'no', NULL),
(18, 'Wishlists', 'firesale_wishlists', 'firesale_wishlists', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, NULL, 'title', NULL, 'no', NULL),
(19, 'Default', 'def_page_fields', 'pages', NULL, 'A simple page type with a WYSIWYG editor that will get you started adding content.', 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, NULL, 'title', NULL, 'no', NULL),
(20, 'lang:user_profile_fields_label', 'profiles', 'users', NULL, 'Profiles for users module', 0x613a313a7b693a303b733a31323a22646973706c61795f6e616d65223b7d, 'display_name', 'title', NULL, 'no', NULL),
(21, 'lang:chat:title', 'chat', 'chat', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'day', 'title', NULL, 'no', NULL),
(22, 'lang:chat:title', 'assign', 'chat', 'chat_', NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'assign_email', 'title', NULL, 'no', NULL),
(23, 'lang:chat:title', 'message', 'chat', 'chat_', NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, NULL, 'title', NULL, 'no', NULL),
(26, 'department', 'firesale_department', 'firesale_department', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'name', 'title', NULL, 'no', NULL),
(27, 'city', 'firesale_city', 'firesale_city', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, 'name', 'title', NULL, 'no', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_data_stream_searches`
--

CREATE TABLE IF NOT EXISTS `default_data_stream_searches` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stream_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stream_namespace` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `search_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `search_term` text COLLATE utf8_unicode_ci,
  `ip_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_results` int(11) NOT NULL,
  `query_string` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_def_page_fields`
--

CREATE TABLE IF NOT EXISTS `default_def_page_fields` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `default_def_page_fields`
--

INSERT INTO `default_def_page_fields` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `body`) VALUES
(5, '2013-07-16 02:02:57', '2014-02-07 17:20:07', 1, NULL, '<p><span style="font-size:18px;">No podemos encontrar la p&aacute;gina que est&aacute; buscando, por favor haga clic <a href="{{ pages:url id=''1'' }}">aqu&iacute;</a> para ir a la p&aacute;gina de inicio.</span></p>\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_email_templates`
--

CREATE TABLE IF NOT EXISTS `default_email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` int(1) NOT NULL DEFAULT '0',
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_lang` (`slug`,`lang`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `default_email_templates`
--

INSERT INTO `default_email_templates` (`id`, `slug`, `name`, `description`, `subject`, `body`, `lang`, `is_default`, `module`) VALUES
(1, 'comments', 'Notificación de comentario', 'Correo electrónico que se envía al administrador cuando alguien crea un comentario', 'Usted acaba de recibir un comentario de {{ name }}', '<h3>Ha recibido un comentario de {{ name }}</h3>\r\n\r\n<p><strong>Direcci&oacute;n IP: {{ sender_ip }}</strong><br />\r\n<strong>Sistema Operativo: {{ sender_os }}<br />\r\n<strong>Agente: {{ sender_agent }}</strong> </strong></p>\r\n\r\n<p><strong>{{ comment }}</strong></p>\r\n\r\n<p><strong>Ver Comentario: {{ redirect_url }}</strong></p>', 'es', 1, 'comments'),
(2, 'contact', 'Notificación de contacto', 'Plantilla para el formulario de contacto', '{{ settings:site_name }} :: {{ subject }}', '<h3>Hola se ha mandado un mensaje a {{ settings:site_name }}</h3>\r\n\r\n<p style="font-size: 13px; line-height: 20.7999992370605px;">Hay alguien que desea ponerse en contacto, a continuaci&oacute;n sus datos.</p>\r\n<strong style="font-size: 13px; line-height: 20.7999992370605px;">Nombre y Apellido:&nbsp;</strong><label style="font-size: 13px; line-height: 20.7999992370605px;">{{name}}</label><br style="font-size: 13px; line-height: 20.7999992370605px;" />\r\n<strong style="font-size: 13px; line-height: 20.7999992370605px;">E-mail:&nbsp;</strong><label style="font-size: 13px; line-height: 20.7999992370605px;">{{ email }}</label><br style="font-size: 13px; line-height: 20.7999992370605px;" />\r\n<strong style="font-size: 13px; line-height: 20.7999992370605px;">Tel&eacute;fono:&nbsp;</strong><label style="font-size: 13px; line-height: 20.7999992370605px;">{{ phone }}</label><br style="font-size: 13px; line-height: 20.7999992370605px;" />\r\n<strong style="font-size: 13px; line-height: 20.7999992370605px;">Empresa:&nbsp;</strong><label style="font-size: 13px; line-height: 20.7999992370605px;">{{ company }}</label><br style="font-size: 13px; line-height: 20.7999992370605px;" />\r\n<strong style="font-size: 13px; line-height: 20.7999992370605px;">Mensaje:</strong>\r\n\r\n<div style="font-size: 13px; line-height: 20.7999992370605px; width: 700px;"><label>{{ message }}</label></div>\r\n<br style="font-size: 13px; line-height: 20.7999992370605px;" />\r\n<br style="font-size: 13px; line-height: 20.7999992370605px;" />\r\n<img src="http://www.blissfulproductivity.com/wp-content/uploads/2013/02/linea.jpg" style="font-size: 13px; line-height: 20.7999992370605px;" />\r\n<div style="font-size: 13px; line-height: 20.7999992370605px; text-align: center;">\r\n<p style="color: gray; font-size: 10px;">La informaci&oacute;n contenida en este mensaje es confidencial y s&oacute;lo puede ser utilizada por el individuo o la compa&ntilde;&iacute;a a la cual est&aacute; dirigido. Si no es el receptor autorizado, cualquier retenci&oacute;n, reproducci&oacute;n, difusi&oacute;n, distribuci&oacute;n o copia de este correo electr&oacute;nico es prohibida y ser&aacute; sancionada por la ley. Excepto en casos de culpa grave o dolo, no aceptamos ninguna responsabilidad por p&eacute;rdidas o da&ntilde;o causados por virus inform&aacute;ticos en correos electr&oacute;nicos o en programas de computador (software). El que il&iacute;citamente sustraiga, oculte, extrav&iacute;e, destruya, intercepte, controle o impida esta comunicaci&oacute;n, antes de que llegue a su destinatario, estar&aacute; sujeto a las sanciones penales correspondientes. Igualmente, incurrir&aacute; en sanciones penales el que, en provecho propio o ajeno o con perjuicio de otro, divulgue o emplee la informaci&oacute;n contenida en esta comunicaci&oacute;n. En particular, los servidores p&uacute;blicos que reciban este mensaje est&aacute;n obligados a asegurar y mantener la confidencialidad de la informaci&oacute;n en &eacute;l contenida y, en general, a cumplir con los deberes de custodia, cuidado, manejo y dem&aacute;s previstos en el r&eacute;gimen disciplinario. Si por error recibe este mensaje, le solicitamos enviarlo de vuelta a Imaginamos a la direcci&oacute;n de correo electr&oacute;nico que se lo envi&oacute; y borrarlo de sus archivos electr&oacute;nicos o destruirlo.</p>\r\n\r\n<p style="color: green; font-size: 12px; text-align: justify;">Evite imprimir, piense en su compromiso con el Medio Ambiente.</p>\r\n</div>', 'es', 1, 'pages'),
(3, 'registered', 'Nuevo usuario registrado', 'Correo electrónico enviado a la dirección de correo electrónico de contacto del sitio cuando un nuevo usuario se registra', '{{ settings:site_name }} :: Usted acaba de recibir un registro de {{ name }}', '<h3>Usted ha recibido una solicitud de registro de {{ name }}</h3>\r\n\r\n<p><strong>Direcci&oacute;n IP: {{ sender_ip }}</strong><br />\r\n<strong>Sistema Operativo: {{ sender_os }}</strong><br />\r\n<strong>Agente: {{ sender_agent }}</strong></p>', 'es', 1, 'users'),
(4, 'activation', 'Correo de activación', 'The email which contains the activation code that is sent to a new user', '{{ settings:site_name }} - Activar Cuenta', '<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#E9E9EA" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" target="_blank"><img border="0" class="ecxlogo" data-pyroimage="true" height="36" src="{{ url:site }}{{ widgets:instance id=''1''}}" style="display:block;border:none;text-decoration:none;" /></a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td height="20" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Estimado Cliente</p>\n									</td>\n								</tr>\n								<tr>\n									<td height="8" width="100%">&nbsp;</td>\n								</tr>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Gracias por registrarte en&nbsp;{{ settings:site_name }}. Antes de activar tu cuenta, por favor completa el proceso de registro y preciona el siguiente link:<br />\n									<a href="{{ url:site }}users/activate/{{ user:id }}/{{ activation_code }}">{{ url:site }}users/activate/{{ user:id }}/{{ activation_code }}</a></p>\n									&nbsp;\n\n									<p>En caso de que tu programa de correo electr&oacute;nico no reconoca el enlace anterior, por favor ve al&nbsp;siguiente link e introduzce el c&oacute;digo de activaci&oacute;n:<br />\n									<a href="{{ url:site }}users/activate">{{ url:site }}users/activate</a></p>\n									&nbsp;\n\n									<p><b>C&oacute;digo de activaci&oacute;n:</b> {{ activation_code }}</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>&nbsp;</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n				</tbody>\n			</table>\n\n			<table align="center" bgcolor="#26468D" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" style="color: #fff;text-decoration: none;padding-left: 15px;font-family: Helvetica, arial, sans-serif;font-size: 14px;text-align: left;line-height: 20px;" target="_blank">&copy; {{ helper:date format=&quot;Y&quot; }} {{ settings:site_name }} - Todos los derechos reservados </a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>', 'es', 1, 'users'),
(5, 'forgotten_password', 'Olvido su contraseña', 'El correo electrónico que se envía con un código de restablecimiento de contraseña', '{{ settings:site_name }} - Olvido la contraseña', '<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#E9E9EA" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" target="_blank"><img border="0" class="ecxlogo" data-pyroimage="true" height="36" src="{{ url:site }}{{ widgets:instance id=''1''}}" style="display:block;border:none;text-decoration:none;" /></a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td height="20" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Estimado Cliente</p>\n									</td>\n								</tr>\n								<tr>\n									<td height="8" width="100%">&nbsp;</td>\n								</tr>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Parece que has solicitado un restablecimiento de contrase&ntilde;a, por favor, haga clic en este enlace para completar el restablecimiento:<br />\n									<a href="{{ url:site }}users/reset_pass/{{ user:forgotten_password_code }}">{{ url:site }}users/reset_pass/{{ user:forgotten_password_code }}</a></p>\n									&nbsp;\n\n									<p>Si no has solicitado un restablecimiento de contrase&ntilde;a, por favor ignora este mensaje.</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>&nbsp;</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n				</tbody>\n			</table>\n\n			<table align="center" bgcolor="#26468D" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" style="color: #fff;text-decoration: none;padding-left: 15px;font-family: Helvetica, arial, sans-serif;font-size: 14px;text-align: left;line-height: 20px;" target="_blank">&copy; {{ helper:date format=&quot;Y&quot; }} {{ settings:site_name }} - Todos los derechos reservados </a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>', 'es', 1, 'users'),
(6, 'new_password', 'Nueva contraseña', 'Después se restablece una contraseña se envía este correo electrónico que contiene la nueva contraseña', '{{ settings:site_name }} - Nueva Contraseña', '<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#E9E9EA" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" target="_blank"><img border="0" class="ecxlogo" data-pyroimage="true" height="36" src="{{ url:site }}{{ widgets:instance id=''1''}}" style="display:block;border:none;text-decoration:none;" /></a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td height="20" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Estimado Cliente</p>\n									</td>\n								</tr>\n								<tr>\n									<td height="8" width="100%">&nbsp;</td>\n								</tr>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Tu nueva contrase&ntilde;a es: {{ new_password }}</p>\n									&nbsp;\n\n									<p>Despu&eacute;s de iniciar sesi&oacute;n, puedes cambiar su contrase&ntilde;a al visitar <a href="{{ url:site }}edit-profile">{{ url:site }}edit-profile</a></p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>&nbsp;</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n				</tbody>\n			</table>\n\n			<table align="center" bgcolor="#26468D" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" style="color: #fff;text-decoration: none;padding-left: 15px;font-family: Helvetica, arial, sans-serif;font-size: 14px;text-align: left;line-height: 20px;" target="_blank">&copy; {{ helper:date format=&quot;Y&quot; }} {{ settings:site_name }} - Todos los derechos reservados </a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>', 'es', 1, 'users'),
(7, 'pedido-completo-admin', 'Pedido Completo (Admin)', 'Se envía al administrador del sitio una vez que un pedido ha sido completado', '{{ settings:site_name }} :: Una orden ha sido completada', '<style type="text/css">.ecximgpop{\n      position: relative;\n    }\n    .ecximgpop>div{\n      color: #686970;\n      font-family: Helvetica, arial, sans-serif;\n      font-size: 15px;\n      text-align: left;\n      line-height: 20px;\n      font-weight: bold;\n      position: absolute;\n      right: 0;\n      top: 8px;\n    }\n    .cart-left img{\n      width: 70px;\n    }\n</style>\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#E9E9EA" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" target="_blank"><img border="0" class="ecxlogo" data-pyroimage="true" height="36" src="{{ url:site }}{{ widgets:instance id=''1''}}" style="display:block;border:none;text-decoration:none;" /></a>\n						<div>Pedido: N&deg; {{id}}</div>\n						</div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td height="20" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Estimado Admin,</p>\n									</td>\n								</tr>\n								<tr>\n									<td height="8" width="100%">&nbsp;</td>\n								</tr>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Acabe de recibir una orden en {{ settings:site_name }}.</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p><strong>Informaci&oacute;n General:</strong></p>\n\n									<p>{{ if ship_to.firstname }}<b>Nombre: </b>{{ ship_to.firstname }}<br />\n									{{ endif }} {{ if ship_to.address1 }}<b>Direcci&oacute;n 1: </b>{{ ship_to.address1 }}<br />\n									{{ endif }} {{ if ship_to.address2 }}<b>Direcci&oacute;n 2: </b>{{ ship_to.address2 }}<br />\n									{{ endif }} {{ if ship_to.city }}<b>Ciudad: </b>{{ ship_to.city }}<br />\n									{{ endif }} {{ if ship_to.county }}<b>Departamento: </b>{{ ship_to.county }}<br />\n									{{ endif }} {{ if ship_to.postcode }}<b>Cod postal: </b>{{ ship_to.postcode }}<br />\n									{{ endif }} {{ if ship_to.country }}<b>Pa&iacute;s: </b>{{ ship_to.country.name }}{{ endif }}</p>\n									</td>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p><strong>Informaci&oacute;n de Facturaci&oacute;n:</strong></p>\n\n									<p>{{ if bill_to.firstname }}<b>Nombre: </b>{{ bill_to.firstname }}<br />\n									{{ endif }} {{ if bill_to.address1 }}<b>Direcci&oacute;n 1: </b>{{ bill_to.address1 }}<br />\n									{{ endif }} {{ if bill_to.address2 }}<b>Direcci&oacute;n 2: </b>{{ bill_to.address2 }}<br />\n									{{ endif }} {{ if bill_to.city }}<b>Ciudad: </b>{{ bill_to.city }}<br />\n									{{ endif }} {{ if bill_to.county }}<b>Departamento: </b>{{ bill_to.county }}<br />\n									{{ endif }} {{ if bill_to.postcode }}<b>Cod postal: </b>{{ bill_to.postcode }}<br />\n									{{ endif }} {{ if bill_to.country }}<b>Pa&iacute;s: </b>{{ bill_to.country.name }}{{ endif }}</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" style="" width="640">\n							<thead style="border: 1px solid #ddd;background: rgba(233, 233, 234, 1);">\n								<tr>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">Imagen</th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;width:190px;"><b>Producto</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>C&oacute;digo</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Precio U.</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Cantidad</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Total</b></th>\n								</tr>\n							</thead>\n							<tbody style="border: 1px solid #ddd;">\n								{{ items }} \n								<tr>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ if image }} <a href="{{ if parent }}{{ firesale:url route=''product'' id=parent }}{{ else }}{{ firesale:url route=''product'' id=id }}{{ endif }}" target="_blank" title="Ver {{ name }}"><img alt="{{ name }}" data-pyroimage="true" src="{{ url:site }}files/large/{{ image }}" width="70" /></a> {{ else }}\n									<div class="cart-left"><a href="{{ if parent }}{{ firesale:url route=''product'' id=parent }}{{ else }}{{ firesale:url route=''product'' id=id }}{{ endif }}" title="View {{ name }}">{{ theme:image file=&quot;no-image.png&quot; alt=title }}</a></div>\n									{{ endif }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><a href="{{ firesale:url route=''product'' id=id }}" id="id" style="color:#000;text-decoration: none;" target="_blank">{{ name }}</a></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">Item No: {{ code }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ price_formatted }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ qty }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ total }}</td>\n								</tr>\n								{{ /items }}\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Sub-total:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_sub }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Env&iacute;o:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_ship }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Iva:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_tax }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Total a consignar:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_total }}</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n				</tbody>\n			</table>\n\n			<table align="center" bgcolor="#26468D" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" style="color: #fff;text-decoration: none;padding-left: 15px;font-family: Helvetica, arial, sans-serif;font-size: 14px;text-align: left;line-height: 20px;" target="_blank">&copy; {{ helper:date format=&quot;Y&quot; }} {{ settings:site_name }} - Todos los derechos reservados </a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>', 'es', 0, ''),
(8, 'pedido-completo-user', 'Pedido Completo (User)', 'Se envía al usuario una vez que un pedido ha sido completado', '{{ settings:site_name }} :: La confirmación del pedido', '<style type="text/css">.ecximgpop{\n      position: relative;\n    }\n    .ecximgpop>div{\n      color: #686970;\n      font-family: Helvetica, arial, sans-serif;\n      font-size: 15px;\n      text-align: left;\n      line-height: 20px;\n      font-weight: bold;\n      position: absolute;\n      right: 0;\n      top: 8px;\n    }\n    .cart-left img{\n      width: 70px;\n    }\n</style>\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#E9E9EA" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" target="_blank"><img border="0" class="ecxlogo" data-pyroimage="true" height="36" src="{{ url:site }}{{ widgets:instance id=''1''}}" style="display:block;border:none;text-decoration:none;" /></a>\n						<div>Pedido: N&deg; {{ id }}</div>\n						</div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td height="20" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Estimado {{ bill_to.firstname }},</p>\n									</td>\n								</tr>\n								<tr>\n									<td height="8" width="100%">&nbsp;</td>\n								</tr>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Gracias por su reciente pedido en {{ settings:site_name }},&nbsp;a continuaci&oacute;n encontrar&aacute; los detalles de su pedido y estos se deben mantener para sus propios registros.</p>\n									&nbsp;\n\n									<p>Tenga en cuenta que esto es s&oacute;lo una confirmaci&oacute;n de su pedido, una vez que el pago haya sido procesado y sus art&iacute;culos han sido enviados nos pondremos en contacto con usted de nuevo para hacerle saber.</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p><strong>Informaci&oacute;n General:</strong></p>\n\n									<p>{{ if ship_to.firstname }}<b>Nombre: </b>{{ ship_to.firstname }}<br />\n									{{ endif }} {{ if ship_to.address1 }}<b>Direcci&oacute;n 1: </b>{{ ship_to.address1 }}<br />\n									{{ endif }} {{ if ship_to.address2 }}<b>Direcci&oacute;n 2: </b>{{ ship_to.address2 }}<br />\n									{{ endif }} {{ if ship_to.city }}<b>Ciudad: </b>{{ ship_to.city }}<br />\n									{{ endif }} {{ if ship_to.county }}<b>Departamento: </b>{{ ship_to.county }}<br />\n									{{ endif }} {{ if ship_to.postcode }}<b>Cod postal: </b>{{ ship_to.postcode }}<br />\n									{{ endif }} {{ if ship_to.country }}<b>Pa&iacute;s: </b>{{ ship_to.country.name }}{{ endif }}</p>\n									</td>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p><strong>Informaci&oacute;n de Facturaci&oacute;n:</strong></p>\n\n									<p>{{ if bill_to.firstname }}<b>Nombre: </b>{{ bill_to.firstname }}<br />\n									{{ endif }} {{ if bill_to.address1 }}<b>Direcci&oacute;n 1: </b>{{ bill_to.address1 }}<br />\n									{{ endif }} {{ if bill_to.address2 }}<b>Direcci&oacute;n 2: </b>{{ bill_to.address2 }}<br />\n									{{ endif }} {{ if bill_to.city }}<b>Ciudad: </b>{{ bill_to.city }}<br />\n									{{ endif }} {{ if bill_to.county }}<b>Departamento: </b>{{ bill_to.county }}<br />\n									{{ endif }} {{ if bill_to.postcode }}<b>Cod postal: </b>{{ bill_to.postcode }}<br />\n									{{ endif }} {{ if bill_to.country }}<b>Pa&iacute;s: </b>{{ bill_to.country.name }}{{ endif }}</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" style="" width="640">\n							<thead style="border: 1px solid #ddd;background: rgba(233, 233, 234, 1);">\n								<tr>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">Imagen</th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;width:190px;"><b>Producto</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>C&oacute;digo</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Precio U.</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Cantidad</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Total</b></th>\n								</tr>\n							</thead>\n							<tbody style="border: 1px solid #ddd;">\n								{{ items }} \n								<tr>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ if image }} <a href="{{ if parent }}{{ firesale:url route=''product'' id=parent }}{{ else }}{{ firesale:url route=''product'' id=id }}{{ endif }}" target="_blank" title="Ver {{ name }}"><img alt="{{ name }}" data-pyroimage="true" src="{{ url:site }}files/large/{{ image }}" width="70" /></a> {{ else }}\n									<div class="cart-left"><a href="{{ if parent }}{{ firesale:url route=''product'' id=parent }}{{ else }}{{ firesale:url route=''product'' id=id }}{{ endif }}" title="View {{ name }}">{{ theme:image file=&quot;no-image.png&quot; alt=title }}</a></div>\n									{{ endif }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><a href="{{ firesale:url route=''product'' id=id }}" id="id" style="color:#000;text-decoration: none;" target="_blank">{{ name }}</a></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">Item No: {{ code }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ price_formatted }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ qty }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ total }}</td>\n								</tr>\n								{{ /items }}\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Sub-total:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_sub }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Env&iacute;o:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_ship }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Iva:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_tax }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Total a consignar:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_total }}</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="left" border="0" cellpadding="0" cellspacing="0" class="ecxtablet-button" height="30" style="margin-left: 30px;">\n							<tbody>\n								<tr>\n									<td align="center" height="30" style="background-color:#042039;border-top-left-radius:4px;border-bottom-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;background-clip:padding-box;font-size:14px;font-family:Helvetica, arial, sans-serif;text-align:center;color:#ffffff;font-weight:300;padding-left:18px;padding-right:18px;" valign="middle" width="auto"><span style="color:#ffffff;"><a href="{{ url:site }}" style="color:#ffffff;text-align:center;text-decoration:none;" target="_blank">Ir a nuestra tienda</a> </span></td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n				</tbody>\n			</table>\n\n			<table align="center" bgcolor="#26468D" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" style="color: #fff;text-decoration: none;padding-left: 15px;font-family: Helvetica, arial, sans-serif;font-size: 14px;text-align: left;line-height: 20px;" target="_blank">&copy; {{ helper:date format=&quot;Y&quot; }} {{ settings:site_name }} - Todos los derechos reservados </a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>', 'es', 0, ''),
(9, 'pedido-enviado-user', 'Pedido Enviado (user)', 'Se envía al usuario cuando su pedido haya sido enviado', '{{ settings:site_name }} :: Su pedido ha sido enviado', '<style type="text/css">.ecximgpop{\n      position: relative;\n    }\n    .ecximgpop>div{\n      color: #686970;\n      font-family: Helvetica, arial, sans-serif;\n      font-size: 15px;\n      text-align: left;\n      line-height: 20px;\n      font-weight: bold;\n      position: absolute;\n      right: 0;\n      top: 8px;\n    }\n    .cart-left img{\n      width: 70px;\n    }\n</style>\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#E9E9EA" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" target="_blank"><img border="0" class="ecxlogo" data-pyroimage="true" height="36" src="{{ url:site }}{{ widgets:instance id=''1''}}" style="display:block;border:none;text-decoration:none;" /></a>\n						<div>Pedido: N&deg; {{ id }}</div>\n						</div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td height="20" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Estimado {{ bill_to.firstname }},</p>\n									</td>\n								</tr>\n								<tr>\n									<td height="8" width="100%">&nbsp;</td>\n								</tr>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Graciar por reciente pedido en&nbsp;{{ settings:site_name }},&nbsp;s&oacute;lo estamos dejando saber que su pedido a&nbsp;sido enviado, as&iacute; como confirmar los datos una vez m&aacute;s.</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p><strong>Informaci&oacute;n General:</strong></p>\n\n									<p>{{ if ship_to.firstname }}<b>Nombre: </b>{{ ship_to.firstname }}<br />\n									{{ endif }} {{ if ship_to.address1 }}<b>Direcci&oacute;n 1: </b>{{ ship_to.address1 }}<br />\n									{{ endif }} {{ if ship_to.address2 }}<b>Direcci&oacute;n 2: </b>{{ ship_to.address2 }}<br />\n									{{ endif }} {{ if ship_to.city }}<b>Ciudad: </b>{{ ship_to.city }}<br />\n									{{ endif }} {{ if ship_to.county }}<b>Departamento: </b>{{ ship_to.county }}<br />\n									{{ endif }} {{ if ship_to.postcode }}<b>Cod postal: </b>{{ ship_to.postcode }}<br />\n									{{ endif }} {{ if ship_to.country }}<b>Pa&iacute;s: </b>{{ ship_to.country.name }}{{ endif }}</p>\n									</td>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p><strong>Informaci&oacute;n de Facturaci&oacute;n:</strong></p>\n\n									<p>{{ if bill_to.firstname }}<b>Nombre: </b>{{ bill_to.firstname }}<br />\n									{{ endif }} {{ if bill_to.address1 }}<b>Direcci&oacute;n 1: </b>{{ bill_to.address1 }}<br />\n									{{ endif }} {{ if bill_to.address2 }}<b>Direcci&oacute;n 2: </b>{{ bill_to.address2 }}<br />\n									{{ endif }} {{ if bill_to.city }}<b>Ciudad: </b>{{ bill_to.city }}<br />\n									{{ endif }} {{ if bill_to.county }}<b>Departamento: </b>{{ bill_to.county }}<br />\n									{{ endif }} {{ if bill_to.postcode }}<b>Cod postal: </b>{{ bill_to.postcode }}<br />\n									{{ endif }} {{ if bill_to.country }}<b>Pa&iacute;s: </b>{{ bill_to.country.name }}{{ endif }}</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" style="" width="640">\n							<thead style="border: 1px solid #ddd;background: rgba(233, 233, 234, 1);">\n								<tr>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">Imagen</th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;width:190px;"><b>Producto</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>C&oacute;digo</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Precio U.</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Cantidad</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Total</b></th>\n								</tr>\n							</thead>\n							<tbody style="border: 1px solid #ddd;">\n							   {{ items }} \n								<tr>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ if image }} <a href="{{ if parent }}{{ firesale:url route=''product'' id=parent }}{{ else }}{{ firesale:url route=''product'' id=id }}{{ endif }}" target="_blank" title="Ver {{ name }}"><img alt="{{ name }}" data-pyroimage="true" src="{{ url:site }}files/large/{{ image }}" width="70" /></a> {{ else }}\n									<div class="cart-left"><a href="{{ if parent }}{{ firesale:url route=''product'' id=parent }}{{ else }}{{ firesale:url route=''product'' id=id }}{{ endif }}" title="View {{ name }}">{{ theme:image file=&quot;no-image.png&quot; alt=title }}</a></div>\n									{{ endif }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><a href="{{ firesale:url route=''product'' id=id }}" id="id" style="color:#000;text-decoration: none;" target="_blank">{{ name }}</a></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">Item No: {{ code }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ price_formatted }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ qty }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ total }}</td>\n								</tr>\n								{{ /items }}\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Sub-total:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_sub }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Env&iacute;o:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_ship }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Iva:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_tax }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Total a consignar:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_total }}</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="left" border="0" cellpadding="0" cellspacing="0" class="ecxtablet-button" height="30" style="margin-left: 30px;">\n							<tbody>\n								<tr>\n									<td align="center" height="30" style="background-color:#042039;border-top-left-radius:4px;border-bottom-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;background-clip:padding-box;font-size:14px;font-family:Helvetica, arial, sans-serif;text-align:center;color:#ffffff;font-weight:300;padding-left:18px;padding-right:18px;" valign="middle" width="auto"><span style="color:#ffffff;"><a href="{{ url:site }}" style="color:#ffffff;text-align:center;text-decoration:none;" target="_blank">Ir a nuestra tienda</a> </span></td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n				</tbody>\n			</table>\n\n			<table align="center" bgcolor="#26468D" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" style="color: #fff;text-decoration: none;padding-left: 15px;font-family: Helvetica, arial, sans-serif;font-size: 14px;text-align: left;line-height: 20px;" target="_blank">&copy; {{ helper:date format=&quot;Y&quot; }} {{ settings:site_name }} - Todos los derechos reservados </a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>', 'es', 0, '');
INSERT INTO `default_email_templates` (`id`, `slug`, `name`, `description`, `subject`, `body`, `lang`, `is_default`, `module`) VALUES
(10, 'cambiar-estado-order', 'Cambiar Estado Order', 'Cuando se cambia el estado de la orden se manda un correo a el usuario', 'Cambio de estado de la orden a {{ status_order }}', '<style type="text/css">.ecximgpop{\n      position: relative;\n    }\n    .ecximgpop>div{\n      color: #686970;\n      font-family: Helvetica, arial, sans-serif;\n      font-size: 15px;\n      text-align: left;\n      line-height: 20px;\n      font-weight: bold;\n      position: absolute;\n      right: 0;\n      top: 8px;\n    }\n    .cart-left img{\n      width: 70px;\n    }\n</style>\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#E9E9EA" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" target="_blank"><img border="0" class="ecxlogo" data-pyroimage="true" height="36" src="{{ url:site }}{{ widgets:instance id=''1''}}" style="display:block;border:none;text-decoration:none;" /></a>\n						<div>Pedido: N&deg; {{ id }}</div>\n						</div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n\n<div class="ecxblock">\n<table bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" id="ecxbackgroundTable" width="100%">\n	<tbody>\n		<tr>\n			<td>\n			<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td height="20" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Estimado {{ bill_to.firstname }},</p>\n									</td>\n								</tr>\n								<tr>\n									<td height="8" width="100%">&nbsp;</td>\n								</tr>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p>Pedido cambiado a&nbsp;{{ status_order }}<br />\n									Gracias por su reciente pedido &nbsp;{{ settings:site_name }},&nbsp;a continuaci&oacute;n encontrar&aacute; los detalles de su pedido y estos se deben mantener para sus propios registros.</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" width="640">\n							<tbody>\n								<tr>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p><strong>Informaci&oacute;n General:</strong></p>\n\n									<p>{{ if ship_to.firstname }}<b>Nombre: </b>{{ ship_to.firstname }}<br />\n									{{ endif }} {{ if ship_to.address1 }}<b>Direcci&oacute;n 1: </b>{{ ship_to.address1 }}<br />\n									{{ endif }} {{ if ship_to.address2 }}<b>Direcci&oacute;n 2: </b>{{ ship_to.address2 }}<br />\n									{{ endif }} {{ if ship_to.city }}<b>Ciudad: </b>{{ ship_to.city }}<br />\n									{{ endif }} {{ if ship_to.county }}<b>Departamento: </b>{{ ship_to.county }}<br />\n									{{ endif }} {{ if ship_to.postcode }}<b>Cod postal: </b>{{ ship_to.postcode }}<br />\n									{{ endif }} {{ if ship_to.country }}<b>Pa&iacute;s: </b>{{ ship_to.country.name }}{{ endif }}</p>\n									</td>\n									<td style="font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">\n									<p><strong>Informaci&oacute;n de Facturaci&oacute;n:</strong></p>\n\n									<p>{{ if bill_to.firstname }}<b>Nombre: </b>{{ bill_to.firstname }}<br />\n									{{ endif }} {{ if bill_to.address1 }}<b>Direcci&oacute;n 1: </b>{{ bill_to.address1 }}<br />\n									{{ endif }} {{ if bill_to.address2 }}<b>Direcci&oacute;n 2: </b>{{ bill_to.address2 }}<br />\n									{{ endif }} {{ if bill_to.city }}<b>Ciudad: </b>{{ bill_to.city }}<br />\n									{{ endif }} {{ if bill_to.county }}<b>Departamento: </b>{{ bill_to.county }}<br />\n									{{ endif }} {{ if bill_to.postcode }}<b>Cod postal: </b>{{ bill_to.postcode }}<br />\n									{{ endif }} {{ if bill_to.country }}<b>Pa&iacute;s: </b>{{ bill_to.country.name }}{{ endif }}</p>\n									</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="center" bgcolor="#f3f3f4" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidthinner" style="" width="640">\n							<thead style="border: 1px solid #ddd;background: rgba(233, 233, 234, 1);">\n								<tr>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">Imagen</th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;width:190px;"><b>Producto</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>C&oacute;digo</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Precio U.</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Cantidad</b></th>\n									<th style="border: 1px solid #ddd;padding:5px;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Total</b></th>\n								</tr>\n							</thead>\n							<tbody style="border: 1px solid #ddd;">\n								{{ items }} \n								<tr>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ if image }} <a href="{{ if parent }}{{ firesale:url route=''product'' id=parent }}{{ else }}{{ firesale:url route=''product'' id=id }}{{ endif }}" target="_blank" title="Ver {{ name }}"><img alt="{{ name }}" data-pyroimage="true" src="{{ url:site }}files/large/{{ image }}" width="70" /></a> {{ else }}\n									<div class="cart-left"><a href="{{ if parent }}{{ firesale:url route=''product'' id=parent }}{{ else }}{{ firesale:url route=''product'' id=id }}{{ endif }}" title="View {{ name }}">{{ theme:image file=&quot;no-image.png&quot; alt=title }}</a></div>\n									{{ endif }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><a href="{{ firesale:url route=''product'' id=id }}" id="id" style="color:#000;text-decoration: none;" target="_blank">{{ name }}</a></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">Item No: {{ code }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ price_formatted }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ qty }}</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ total }}</td>\n								</tr>\n								{{ /items }}\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Sub-total:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_sub }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Env&iacute;o:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_ship }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Iva:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_tax }}</td>\n								</tr>\n								<tr>\n									<td colspan="4">&nbsp;</td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;"><b>Total a consignar:</b></td>\n									<td style="padding:5px;border: 1px solid #ddd;font-family:Helvetica, arial, sans-serif;font-size:14px;color:#686970;text-align:left;line-height:20px;">{{ settings:currency }}{{ price_total }}</td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n					<tr>\n						<td>\n						<table align="left" border="0" cellpadding="0" cellspacing="0" class="ecxtablet-button" height="30" style="margin-left: 30px;">\n							<tbody>\n								<tr>\n									<td align="center" height="30" style="background-color:#042039;border-top-left-radius:4px;border-bottom-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;background-clip:padding-box;font-size:14px;font-family:Helvetica, arial, sans-serif;text-align:center;color:#ffffff;font-weight:300;padding-left:18px;padding-right:18px;" valign="middle" width="auto"><span style="color:#ffffff;"><a href="{{ url:site }}" style="color:#ffffff;text-align:center;text-decoration:none;" target="_blank">Ir a nuestra tienda</a> </span></td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n					<tr>\n						<td height="15" width="100%">&nbsp;</td>\n					</tr>\n				</tbody>\n			</table>\n\n			<table align="center" bgcolor="#26468D" border="0" cellpadding="0" cellspacing="0" class="ecxdevicewidth" width="700">\n				<tbody>\n					<tr>\n						<td class="ecxlogo" style="padding:10px 20px;">\n						<div class="ecximgpop"><a href="{{ url:site }}" style="color: #fff;text-decoration: none;padding-left: 15px;font-family: Helvetica, arial, sans-serif;font-size: 14px;text-align: left;line-height: 20px;" target="_blank">&copy; {{ helper:date format=&quot;Y&quot; }} {{ settings:site_name }} - Todos los derechos reservados </a></div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>', 'es', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_files`
--

CREATE TABLE IF NOT EXISTS `default_files` (
  `id` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '1',
  `type` enum('a','v','d','i','o') COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mimetype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `width` int(5) DEFAULT NULL,
  `height` int(5) DEFAULT NULL,
  `filesize` int(11) NOT NULL DEFAULT '0',
  `alt_attribute` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download_count` int(11) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `default_files`
--

INSERT INTO `default_files` (`id`, `folder_id`, `user_id`, `type`, `name`, `filename`, `path`, `description`, `extension`, `mimetype`, `keywords`, `width`, `height`, `filesize`, `alt_attribute`, `download_count`, `date_added`, `sort`) VALUES
('005a4fd7cfde6e2', 108, 10, 'i', 'mfc_1014.jpg', '280ec8be7c8db22d99cdc84a926acc4c.jpg', '{{ url:site }}files/large/280ec8be7c8db22d99cdc84a926acc4c.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 74, '', 0, 1428622494, 0),
('009adcfe658fa05', 108, 10, 'i', 'mfc_0302.jpg', '67d91dcd1bba2524ce81e9322adbe00d.jpg', '{{ url:site }}files/large/67d91dcd1bba2524ce81e9322adbe00d.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 80, '', 0, 1428622519, 0),
('02e4ff94242447c', 108, 10, 'i', 'mfc_1008.jpg', 'd0ea8c5c5d11b725646a9599fe8cc7e1.jpg', '{{ url:site }}files/large/d0ea8c5c5d11b725646a9599fe8cc7e1.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 209, '', 0, 1428622490, 0),
('03bc2d6ba444148', 30, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('04bfaa6bd132d14', 52, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('053009f610f1e99', 48, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('0692a1de446455f', 48, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('0768ba6390eb3e4', 112, 10, 'i', 'images-1.jpg', '1097f22a6ba2b31f7d5fb6a99b40642a.jpg', '{{ url:site }}files/large/1097f22a6ba2b31f7d5fb6a99b40642a.jpg', '', '.jpg', 'image/jpeg', '', 275, 275, 5, '', 0, 1421691080, 1),
('076d1ca52650c41', 25, 1, 'i', 'nude-shoes-2.jpg', 'e82fa0be0793f42eca14e8cf27f6f08c.jpg', '{{ url:site }}files/large/e82fa0be0793f42eca14e8cf27f6f08c.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 252, '', 0, 1374121715, 1),
('07c5252226202fa', 108, 10, 'i', 'mfc_0325.jpg', '993c0aade27b07138ad90030e308b7b9.jpg', '{{ url:site }}files/large/993c0aade27b07138ad90030e308b7b9.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 99, '', 0, 1428622521, 0),
('07cb5f08bd35465', 23, 1, 'i', 'nude-shoes-1.jpg', 'e957ccaa1303ba905bc13c600ac2970f.jpg', '{{ url:site }}files/large/e957ccaa1303ba905bc13c600ac2970f.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 207, '', 0, 1374121712, 0),
('0888e0f8342484b', 31, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('09203a0c52262b8', 49, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('093fea663707039', 42, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('09be4521a6245b9', 108, 10, 'i', 'mfc_0184.jpg', 'f0e261c51e1c193a0ac8642726f2b61a.jpg', '{{ url:site }}files/large/f0e261c51e1c193a0ac8642726f2b61a.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 69, '', 0, 1428622509, 0),
('0a129fe4c9e36ef', 108, 10, 'i', 'mfc_1002.jpg', '648b639d4f571190c56de27075d4b28c.jpg', '{{ url:site }}files/large/648b639d4f571190c56de27075d4b28c.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 133, '', 0, 1428622489, 0),
('0a24f3e06ea6703', 35, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('0a8475b5287df1e', 47, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('0a8ee58f1c58730', 108, 10, 'i', 'mfc_0262.jpg', 'a213a88af49487fabdb157a0e88911a6.jpg', '{{ url:site }}files/large/a213a88af49487fabdb157a0e88911a6.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 111, '', 0, 1428622516, 0),
('0ad7b0c45d6b551', 108, 10, 'i', 'mfc_0294.jpg', 'e12c3fadf6576b23a438ed66701f9095.jpg', '{{ url:site }}files/large/e12c3fadf6576b23a438ed66701f9095.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 84, '', 0, 1428622518, 0),
('0bf8f05b38677a0', 65, 10, 'i', 'descarga-2.jpg', '3bf7b514944b82bf44ab24949af39e78.jpg', '{{ url:site }}files/large/3bf7b514944b82bf44ab24949af39e78.jpg', '', '.jpg', 'image/jpeg', '', 340, 340, 12, '', 0, 1401884063, 2),
('0c5044cfeb53e92', 108, 10, 'i', 'mfc_1111.jpg', '5f395134cfbac0dbd29acf80d3acc4e3.jpg', '{{ url:site }}files/large/5f395134cfbac0dbd29acf80d3acc4e3.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 86, '', 0, 1428622496, 0),
('0c9e862b299ba4b', 36, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('0ddccd2631b2d58', 108, 10, 'i', 'mfc_0330.jpg', 'f4816f22a63cf333e3501b985fe38804.jpg', '{{ url:site }}files/large/f4816f22a63cf333e3501b985fe38804.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 83, '', 0, 1428622522, 0),
('0e787f934911c46', 108, 10, 'i', 'mfc_0304.jpg', '8746b390fba41b6828a00295bd4c1ebc.jpg', '{{ url:site }}files/large/8746b390fba41b6828a00295bd4c1ebc.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 96, '', 0, 1428622520, 0),
('0ede5c92df1ceae', 38, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('0f5e55d8e29a465', 108, 10, 'i', 'mfc_0361.jpg', '2133abcf5e9c83bd201f888c6f207418.jpg', '{{ url:site }}files/large/2133abcf5e9c83bd201f888c6f207418.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 142, '', 0, 1428622481, 0),
('10090f61ead37cb', 38, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('102b7d7123ede39', 108, 10, 'i', 'mfc_0313.jpg', 'c3fd44eb0887da93e2b4c5bbdc88b555.jpg', '{{ url:site }}files/large/c3fd44eb0887da93e2b4c5bbdc88b555.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 85, '', 0, 1428622521, 0),
('122aeda6201ec3b', 86, 10, 'i', '2.jpg', 'eae1d57d73fb617eb23f98ed6546e474.jpg', '{{ url:site }}files/large/eae1d57d73fb617eb23f98ed6546e474.jpg', '', '.jpg', 'image/jpeg', '', 270, 270, 23, '', 0, 1416588069, 0),
('12a4bccb147041d', 108, 10, 'i', 'mfc_0135.jpg', '55106ab0e193aec4060ba1cdb94ad08e.jpg', '{{ url:site }}files/large/55106ab0e193aec4060ba1cdb94ad08e.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 95, '', 0, 1428622506, 0),
('135ca4319e9475b', 108, 13, 'i', 'mfc-464.jpg', 'b0c368fad10d3044654c78951b98dc27.jpg', '{{ url:site }}files/large/b0c368fad10d3044654c78951b98dc27.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 109, '', 0, 1429050774, 0),
('13d0eadffb4fea5', 87, 10, 'i', '2.jpg', 'eae1d57d73fb617eb23f98ed6546e474.jpg', '{{ url:site }}files/large/eae1d57d73fb617eb23f98ed6546e474.jpg', '', '.jpg', 'image/jpeg', '', 270, 270, 23, '', 0, 1416588069, 0),
('14f988b187f0764', 64, 10, 'i', 'descarga-3.jpg', '4bf991c6b8e3109f0145b87cc4508367.jpg', '{{ url:site }}files/large/4bf991c6b8e3109f0145b87cc4508367.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 4, '', 0, 1401884064, 5),
('176f30d687fea8d', 108, 10, 'i', 'mfc_0287.jpg', '85b2f133ab421d53b193df80e0d20792.jpg', '{{ url:site }}files/large/85b2f133ab421d53b193df80e0d20792.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 85, '', 0, 1428622519, 0),
('17c4986daf9e37d', 108, 10, 'i', 'mfc_0002.jpg', 'fe3f382a25fa01f30dac2f8f4ff6ed4e.jpg', '{{ url:site }}files/large/fe3f382a25fa01f30dac2f8f4ff6ed4e.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 119, '', 0, 1428622503, 0),
('181e9d8a655fcab', 41, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('1a652f39f044ce4', 108, 10, 'i', 'mfc_0346.jpg', 'e7e3d56a90004d788e846a3eb8e81a38.jpg', '{{ url:site }}files/large/e7e3d56a90004d788e846a3eb8e81a38.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 119, '', 0, 1428622481, 0),
('1b0eef0a30164d6', 40, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('1bcfff4def049cc', 96, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('1c2c95e23d2b5a6', 108, 10, 'i', 'mfc_2015.jpg', '06645652c3434d5677b8741eb3d40f25.jpg', '{{ url:site }}files/large/06645652c3434d5677b8741eb3d40f25.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 118, '', 0, 1428622502, 0),
('1c9f4a2be32cdb7', 97, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('1e65c6a19608cb4', 26, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('1f1104debe8b928', 46, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('1fbabbe8a2b0701', 108, 10, 'i', 'mfc_0257g.jpg', 'b92b0c71c46b6b7bc298102b49bb85cc.jpg', '{{ url:site }}files/large/b92b0c71c46b6b7bc298102b49bb85cc.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 104, '', 0, 1428622515, 0),
('2088a99d24792e9', 75, 10, 'i', 'fit_green.jpg', 'aeaa9c002c1e26cd5e94c1fc2b1671a0.jpg', '{{ url:site }}files/large/aeaa9c002c1e26cd5e94c1fc2b1671a0.jpg', '', '.jpg', 'image/jpeg', '', 424, 424, 18, '', 0, 1413370771, 0),
('2135488f6808864', 47, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('25ebf6000d5c40e', 71, 10, 'i', 'nike-0620-71217-1-product.jpg', 'fcc679e48cbb64faa95bf3a2f34e9190.jpg', '{{ url:site }}files/large/fcc679e48cbb64faa95bf3a2f34e9190.jpg', '', '.jpg', 'image/jpeg', '', 620, 620, 58, '', 0, 1413310032, 0),
('296b4742e5db29b', 103, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('2a09731132f28e4', 108, 10, 'i', 'mfc_0301.jpg', '61d8a3275b67d2bef176d74cf3516cb6.jpg', '{{ url:site }}files/large/61d8a3275b67d2bef176d74cf3516cb6.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 84, '', 0, 1428622519, 0),
('2a2df3511ff35c1', 28, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('2a438603e9d0798', 40, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('2b176791fff8980', 108, 13, 'i', 'mfc_0423.jpg', '3de448a0a99d3ef0f4b00fd85390ca62.jpg', '{{ url:site }}files/large/3de448a0a99d3ef0f4b00fd85390ca62.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 84, '', 0, 1429049154, 0),
('2b66c7d63a50e39', 95, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('2bad09cfe808732', 26, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('2e8e5f92a125367', 63, 10, 'i', 'descarga.jpg', 'dc26cfd65206c2ca4702dd6ebb2dd9fd.jpg', '{{ url:site }}files/large/dc26cfd65206c2ca4702dd6ebb2dd9fd.jpg', '', '.jpg', 'image/jpeg', '', 144, 144, 2, '', 0, 1401882305, 0),
('2f3b4fd4d08b7ad', 108, 10, 'i', 'mfc_1022.jpg', '20cad0edd0d31bb937e9d5baba6afd42.jpg', '{{ url:site }}files/large/20cad0edd0d31bb937e9d5baba6afd42.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 152, '', 0, 1428622495, 0),
('2fc9a5e6c22ab24', 53, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('3001145f421fefc', 108, 10, 'i', 'mfc_ 1021.jpg', '1ba6db2840bcaac16992f1f3f33e2560.jpg', '{{ url:site }}files/large/1ba6db2840bcaac16992f1f3f33e2560.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 121, '', 0, 1428622501, 0),
('3007fee52df3771', 108, 10, 'i', 'mfc_1001.jpg', '3e8ea2602fd4f77f5206ee1528c71134.jpg', '{{ url:site }}files/large/3e8ea2602fd4f77f5206ee1528c71134.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 119, '', 0, 1428622488, 0),
('3296208ba00d8ae', 73, 10, 'i', 'nike-6087-73574-1-zoom.jpg', 'cf2d0d13d489585d26779e3ba4d1fe0b.jpg', '{{ url:site }}files/large/cf2d0d13d489585d26779e3ba4d1fe0b.jpg', '', '.jpg', 'image/jpeg', '', 1100, 1100, 257, '', 0, 1413310079, 0),
('35bc6037a154502', 66, 10, 'i', '0d19f312b94129a4b95fb86fdbe7a0b0.png', '505b3a0c4bf0a6198e9bc2b4bec05006.png', '{{ url:site }}files/large/505b3a0c4bf0a6198e9bc2b4bec05006.png', '', '.png', 'image/png', '', 500, 209, 330, '', 0, 1412261387, 0),
('35cbfe466604d04', 108, 10, 'i', 'mfc_0300.jpg', 'e0b1bcc3d9ea935c41497a87db66302f.jpg', '{{ url:site }}files/large/e0b1bcc3d9ea935c41497a87db66302f.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 84, '', 0, 1428622519, 0),
('35f0371274d2833', 108, 10, 'i', 'mfc_0210.jpg', '84fc36da011b250c71372dd46437a4ea.jpg', '{{ url:site }}files/large/84fc36da011b250c71372dd46437a4ea.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 160, '', 0, 1428622514, 0),
('3672f2708ac241e', 44, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('36ba44ed27821a1', 57, 1, 'i', 'lotions-and-potions.jpg', 'd7f5b0de62ce046eef9bf4b9198f25be.jpg', '{{ url:site }}files/large/d7f5b0de62ce046eef9bf4b9198f25be.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 28, '', 0, 1374121941, 0),
('36bacdabe89aa89', 53, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('373bc29a51ce9bb', 32, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('387acbee569f0b0', 108, 10, 'i', 'mfc_0051.jpg', '1bc2c99c2bb8f4fbe359c7e28fe11960.jpg', '{{ url:site }}files/large/1bc2c99c2bb8f4fbe359c7e28fe11960.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 136, '', 0, 1428622503, 0),
('38941643d5bf729', 48, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('3b924aa1fa3f73e', 112, 10, 'i', 'images.jpg', '99b27f6239c52f285bb37a5766021048.jpg', '{{ url:site }}files/large/99b27f6239c52f285bb37a5766021048.jpg', '', '.jpg', 'image/jpeg', '', 254, 254, 4, '', 0, 1421691069, 0),
('3b996db4ec73181', 108, 10, 'i', 'mfc_20152.jpg', '29381e95c4c5f2f2e25956a998f17fee.jpg', '{{ url:site }}files/large/29381e95c4c5f2f2e25956a998f17fee.jpg', '', '.jpg', 'image/jpeg', '', 2400, 2400, 1096, '', 0, 1428622501, 0),
('3c3c20350ed38e7', 31, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('3c6f0f9b827551c', 63, 10, 'i', 'descarga-2.jpg', '3bf7b514944b82bf44ab24949af39e78.jpg', '{{ url:site }}files/large/3bf7b514944b82bf44ab24949af39e78.jpg', '', '.jpg', 'image/jpeg', '', 340, 340, 12, '', 0, 1401884063, 2),
('3ee6770b707ecf0', 108, 10, 'i', 'mfc_0380.jpg', '4c367ab843d4656757e817a2cd484a55.jpg', '{{ url:site }}files/large/4c367ab843d4656757e817a2cd484a55.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 84, '', 0, 1428622483, 0),
('3fed7e54b23e980', 108, 10, 'i', 'mfc_0407.jpg', '19b3678febe3cf9ff018ce229b53c070.jpg', '{{ url:site }}files/large/19b3678febe3cf9ff018ce229b53c070.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 86, '', 0, 1428622486, 0),
('41afd6d5674313a', 108, 10, 'i', 'mfc_0279.jpg', '4e3f21d3223ce3d9ca5dfdc62834ea62.jpg', '{{ url:site }}files/large/4e3f21d3223ce3d9ca5dfdc62834ea62.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 133, '', 0, 1428622517, 0),
('41b834dbcd3e16e', 108, 10, 'i', 'mfc_ 1018.jpg', '9b1bd753427dbde8672d004e3d306c99.jpg', '{{ url:site }}files/large/9b1bd753427dbde8672d004e3d306c99.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 116, '', 0, 1428622502, 0),
('426167c21ab1de2', 114, 10, 'i', 'zara-coleccion-ninos.jpg', 'bba40d7d08d339f39b291b0a68c3b1a2.jpg', '{{ url:site }}files/large/bba40d7d08d339f39b291b0a68c3b1a2.jpg', '', '.jpg', 'image/jpeg', '', 505, 505, 50, '', 0, 1421691286, 1),
('4357d1da5378619', 30, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('44003a1cdca1f31', 94, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('44573213daf1775', 45, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('44c37274c29330c', 28, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('45fc534233f3694', 58, 1, 'i', 'lotions-and-potions.jpg', 'd7f5b0de62ce046eef9bf4b9198f25be.jpg', '{{ url:site }}files/large/d7f5b0de62ce046eef9bf4b9198f25be.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 28, '', 0, 1374121941, 0),
('4731f1b9878b6d2', 65, 10, 'i', 'descarga-6.jpg', '5981b46923dd99ae409d7a86d6bdb033.jpg', '{{ url:site }}files/large/5981b46923dd99ae409d7a86d6bdb033.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 6, '', 0, 1401884063, 3),
('477bddeedba95fb', 108, 10, 'i', 'mfc_0225.jpg', '64da0fb4185ad6377da5732735a796cc.jpg', '{{ url:site }}files/large/64da0fb4185ad6377da5732735a796cc.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 61, '', 0, 1428622512, 0),
('481a25b8559829f', 106, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('498e1e5ca0dd2de', 108, 10, 'i', 'mfc_0221.jpg', '696fbbcb7687ea169e5149c611028556.jpg', '{{ url:site }}files/large/696fbbcb7687ea169e5149c611028556.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 152, '', 0, 1428622514, 0),
('49a5d52eefeb29d', 108, 13, 'i', 'mfc_457.jpg', '2dfe94e2fc52014ff77c773a00328229.jpg', '{{ url:site }}files/large/2dfe94e2fc52014ff77c773a00328229.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 64, '', 0, 1429069676, 0),
('49b8d7e56175a39', 108, 10, 'i', 'mfc_1006.jpg', 'e711823581cfe9cc490f0b55f872a8c7.jpg', '{{ url:site }}files/large/e711823581cfe9cc490f0b55f872a8c7.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 131, '', 0, 1428622489, 0),
('4a015f26e9747b0', 108, 13, 'i', 'mfc_1017.jpg', '13fad10ef7a1a195414e87ff70133873.jpg', '{{ url:site }}files/large/13fad10ef7a1a195414e87ff70133873.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 149, '', 0, 1429068223, 0),
('4b704a19526f7f9', 24, 1, 'i', 'nude-shoes-2.jpg', 'e82fa0be0793f42eca14e8cf27f6f08c.jpg', '{{ url:site }}files/large/e82fa0be0793f42eca14e8cf27f6f08c.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 252, '', 0, 1374121715, 1),
('4c2e56edf1f3758', 52, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('4c85d93b10a0445', 109, 10, 'i', 'produc5-g.jpg', '269fc2e149482e88bf4853e00db3df53.jpg', '{{ url:site }}files/large/269fc2e149482e88bf4853e00db3df53.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 81, '', 0, 1421690041, 1),
('4d7ae514a70d0e8', 108, 10, 'i', 'mfc_1011.jpg', '91ee90bf26fbb5e86af7daa866244e78.jpg', '{{ url:site }}files/large/91ee90bf26fbb5e86af7daa866244e78.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 170, '', 0, 1428622491, 0),
('4e3e3ee159c6f36', 34, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('4e8c358c170a3c9', 40, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('4ec8ddf78a29de7', 115, 10, 'i', 'images-2.jpg', '92a54b7d4fdc396183a3d81bbca1c9d1.jpg', '{{ url:site }}files/large/92a54b7d4fdc396183a3d81bbca1c9d1.jpg', '', '.jpg', 'image/jpeg', '', 249, 249, 4, '', 0, 1421691923, 1),
('4f0aaec1628aaca', 108, 13, 'i', 'mfc_1010.jpg', 'f71b4da31ec2d63dac524e23576eea39.jpg', '{{ url:site }}files/large/f71b4da31ec2d63dac524e23576eea39.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 105, '', 0, 1429066726, 0),
('506393086b268ab', 76, 10, 'i', 'fit_mag.jpg', 'a08cf821ecd08953627a0a1cd8f34f72.jpg', '{{ url:site }}files/large/a08cf821ecd08953627a0a1cd8f34f72.jpg', '', '.jpg', 'image/jpeg', '', 424, 424, 20, '', 0, 1413370780, 0),
('51e2d059ec86ef7', 65, 10, 'i', 'descarga-3.jpg', '4bf991c6b8e3109f0145b87cc4508367.jpg', '{{ url:site }}files/large/4bf991c6b8e3109f0145b87cc4508367.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 4, '', 0, 1401884064, 5),
('52531f049b222cb', 41, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('529e2800521eef0', 31, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('531ee5d87333c3b', 34, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('536d4b67b896797', 69, 10, 'i', 'nike-0620-71217-1-product.jpg', 'e3bf895280a566c59bf09a8c887112b6.jpg', '{{ url:site }}files/large/e3bf895280a566c59bf09a8c887112b6.jpg', '', '.jpg', 'image/jpeg', '', 620, 620, 58, '', 0, 1413309866, 0),
('563aea34e0bdf89', 108, 13, 'i', 'img_1014.jpg', 'b6264f69cc26c4ac78182815ec2bdc17.jpg', '{{ url:site }}files/large/b6264f69cc26c4ac78182815ec2bdc17.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 1183, '', 0, 1429062777, 0),
('56575d134a8e671', 108, 10, 'i', 'mfc_0366.jpg', '7bf6fe41e786dda26014597130990943.jpg', '{{ url:site }}files/large/7bf6fe41e786dda26014597130990943.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 91, '', 0, 1428622482, 0),
('56783d0d9b42a18', 46, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('580c0467f406ec6', 43, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('5861b3f33f77ca6', 65, 10, 'i', 'descarga-1.jpg', '9e6251a4f50c8dde54a1b7a3754854e4.jpg', '{{ url:site }}files/large/9e6251a4f50c8dde54a1b7a3754854e4.jpg', '', '.jpg', 'image/jpeg', '', 323, 323, 12, '', 0, 1401884064, 6),
('5a5da497948336e', 70, 10, 'i', 'nike-0620-71217-1-product.jpg', 'e3bf895280a566c59bf09a8c887112b6.jpg', '{{ url:site }}files/large/e3bf895280a566c59bf09a8c887112b6.jpg', '', '.jpg', 'image/jpeg', '', 620, 620, 58, '', 0, 1413309866, 0),
('5d513f5e0f2df59', 108, 10, 'i', 'mfc_457.jpg', '653f8886b0593dcbec8cb1ca751291be.jpg', '{{ url:site }}files/large/653f8886b0593dcbec8cb1ca751291be.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 69, '', 0, 1428622487, 0),
('5da92cbcf9eb0c7', 108, 10, 'i', 'mfc_0205.jpg', 'e731db3a6a16927e3739e186ea97f66c.jpg', '{{ url:site }}files/large/e731db3a6a16927e3739e186ea97f66c.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 150, '', 0, 1428622511, 0),
('61d5bca7e8f6918', 114, 10, 'i', 'images-1.jpg', 'feb1afe3599a554496a6db1e01275975.jpg', '{{ url:site }}files/large/feb1afe3599a554496a6db1e01275975.jpg', '', '.jpg', 'image/jpeg', '', 287, 287, 5, '', 0, 1421691282, 0),
('62a37a086d864f7', 123, 10, 'i', 'des1.jpg', 'df39ddad11700f37eac63d2a06663d8c.jpg', '{{ url:site }}files/large/df39ddad11700f37eac63d2a06663d8c.jpg', '', '.jpg', 'image/jpeg', '', 370, 370, 105, '', 0, 1421767638, 0),
('63b615d04a688c3', 33, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('645bd509a2f10a0', 108, 13, 'i', 'mfc-464.jpg', '4ca12789ecbf2bdcb8fde04af36af3c9.jpg', '{{ url:site }}files/large/4ca12789ecbf2bdcb8fde04af36af3c9.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 113, '', 0, 1428651002, 0),
('64d9980419e5e31', 108, 10, 'i', 'mfc_0194.jpg', '8a2e60500186129a630a8ab8cfe8f8bf.jpg', '{{ url:site }}files/large/8a2e60500186129a630a8ab8cfe8f8bf.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 123, '', 0, 1428622510, 0),
('66207c29ab77946', 49, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('664f978655dbaf1', 108, 10, 'i', 'mfc_0413.jpg', '5124d82662287ccf3bd20581c056fea7.jpg', '{{ url:site }}files/large/5124d82662287ccf3bd20581c056fea7.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 100, '', 0, 1428622487, 0),
('677fec97e74f978', 108, 10, 'i', 'mfc_0254.jpg', '0b59b7ef83499332970db5df0a8cce80.jpg', '{{ url:site }}files/large/0b59b7ef83499332970db5df0a8cce80.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 94, '', 0, 1428622514, 0),
('6b792546f133ea6', 108, 10, 'i', 'mfc_1016.jpg', 'e67d730c464bdd335698b25c453958cd.jpg', '{{ url:site }}files/large/e67d730c464bdd335698b25c453958cd.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 122, '', 0, 1428622494, 0),
('6e5d358ac1ad84b', 109, 10, 'i', 'produc2-g.jpg', 'c67c7dec921b322bd483099f3cf8bfed.jpg', '{{ url:site }}files/large/c67c7dec921b322bd483099f3cf8bfed.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 76, '', 0, 1421690049, 2),
('6e9548cb2f4f309', 108, 10, 'i', 'mfc_0394.jpg', 'f4922f37d4e51ff923d88ee5be287a6b.jpg', '{{ url:site }}files/large/f4922f37d4e51ff923d88ee5be287a6b.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 148, '', 0, 1428622484, 0),
('6f34700e98312e8', 38, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('6f4dfb16e105a5a', 32, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('6f672618e9642bc', 33, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('6f764d6f9008d1a', 35, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('6ff83219de0fd8c', 78, 10, 'i', '0e900de2738f2cfe5cf95286a90981ac.png', '427f5a57c73796fc34ea556bac8193e6.png', '{{ url:site }}files/large/427f5a57c73796fc34ea556bac8193e6.png', '', '.png', 'image/png', '', 500, 500, 91, '', 0, 1413370804, 0),
('70f66b1584b3717', 108, 10, 'i', 'mfc_0067.jpg', '4b6d6bc8b83d5899893ebe7602c82d2c.jpg', '{{ url:site }}files/large/4b6d6bc8b83d5899893ebe7602c82d2c.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 98, '', 0, 1428622504, 0),
('71d178e9d50178e', 27, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('72d15b7e3bc1714', 108, 13, 'i', 'mfc_1013.jpg', '0625db89c12a8b093ba31badb5eb39b1.jpg', '{{ url:site }}files/large/0625db89c12a8b093ba31badb5eb39b1.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 135, '', 0, 1429068701, 0),
('735d0a45e59feaf', 108, 13, 'i', 'img-20150410-wa0006.jpg', '25141fe6b6447b765aac6c53429bbdd9.jpg', '{{ url:site }}files/large/25141fe6b6447b765aac6c53429bbdd9.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 371, '', 0, 1428975138, 0),
('742f92da9133b6b', 122, 10, 'i', 'prod.jpg', '86bb74e700900033a3498034c8922078.jpg', '{{ url:site }}files/large/86bb74e700900033a3498034c8922078.jpg', '', '.jpg', 'image/jpeg', '', 600, 600, 166, '', 0, 1421767231, 0),
('761219011e1447e', 65, 10, 'i', 'descarga.jpg', 'dc26cfd65206c2ca4702dd6ebb2dd9fd.jpg', '{{ url:site }}files/large/dc26cfd65206c2ca4702dd6ebb2dd9fd.jpg', '', '.jpg', 'image/jpeg', '', 144, 144, 2, '', 0, 1401882305, 0),
('768b908c4853f03', 53, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('76c222cca95bd58', 112, 10, 'i', 'descarga.jpg', 'bf86427ade415774370a0feac391688c.jpg', '{{ url:site }}files/large/bf86427ade415774370a0feac391688c.jpg', '', '.jpg', 'image/jpeg', '', 254, 254, 5, '', 0, 1421691088, 2),
('76d39bfac143ab2', 49, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('770e84e66fe01af', 45, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('774501bc0dad2a4', 108, 10, 'i', 'mfc_0423.jpg', '9aa1427bf3011fc98adfb651111c8e2d.jpg', '{{ url:site }}files/large/9aa1427bf3011fc98adfb651111c8e2d.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 84, '', 0, 1428622486, 0),
('794e0b751c875ea', 108, 10, 'i', 'mfc_0327.jpg', 'fb5476b0d8c3872679379306aaeaf599.jpg', '{{ url:site }}files/large/fb5476b0d8c3872679379306aaeaf599.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 67, '', 0, 1428622521, 0),
('7ab897e495ecb8e', 108, 10, 'i', 'mfc_0399.jpg', '85048ac2d2be08300bd7b4a112abd33e.jpg', '{{ url:site }}files/large/85048ac2d2be08300bd7b4a112abd33e.jpg', '', '.jpg', 'image/jpeg', '', 3000, 3000, 348, '', 0, 1428622486, 0),
('7b49b22d20d9502', 59, 1, 'i', 'lotions-and-potions.jpg', 'd7f5b0de62ce046eef9bf4b9198f25be.jpg', '{{ url:site }}files/large/d7f5b0de62ce046eef9bf4b9198f25be.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 28, '', 0, 1374121941, 0),
('7c1dec019a17c12', 102, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('7c43143b1a5c901', 41, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('7cdbf01e27760c6', 44, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('7ee1fe82072a852', 22, 1, 'i', 'nude-shoes-1.jpg', 'e957ccaa1303ba905bc13c600ac2970f.jpg', '{{ url:site }}files/large/e957ccaa1303ba905bc13c600ac2970f.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 207, '', 0, 1374121712, 0),
('80fc8a9a7ad034e', 108, 10, 'i', 'mfc_1017.jpg', '4c082ab8bcd8cdc7e38c88b7983df0ba.jpg', '{{ url:site }}files/large/4c082ab8bcd8cdc7e38c88b7983df0ba.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 156, '', 0, 1428622494, 0),
('8139654e936e9d7', 121, 10, 'i', 'prod.jpg', '86bb74e700900033a3498034c8922078.jpg', '{{ url:site }}files/large/86bb74e700900033a3498034c8922078.jpg', '', '.jpg', 'image/jpeg', '', 600, 600, 166, '', 0, 1421767231, 0),
('81b8c3cd874fb22', 68, 10, 'i', 'nike-0620-71217-1-product.jpg', 'e3bf895280a566c59bf09a8c887112b6.jpg', '{{ url:site }}files/large/e3bf895280a566c59bf09a8c887112b6.jpg', '', '.jpg', 'image/jpeg', '', 620, 620, 58, '', 0, 1413309866, 0),
('822e448006e57af', 74, 10, 'i', 'fit.jpg', '0830fc13452809c529728b1632d30d22.jpg', '{{ url:site }}files/large/0830fc13452809c529728b1632d30d22.jpg', '', '.jpg', 'image/jpeg', '', 424, 424, 20, '', 0, 1413370758, 0),
('83c403e631f43a7', 90, 10, 'i', '2.jpg', 'eae1d57d73fb617eb23f98ed6546e474.jpg', '{{ url:site }}files/large/eae1d57d73fb617eb23f98ed6546e474.jpg', '', '.jpg', 'image/jpeg', '', 270, 270, 23, '', 0, 1416588069, 0),
('840d9f1a06c2cce', 47, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('863d08ada8d45e5', 56, 1, 'i', 'lotions-and-potions.jpg', 'd7f5b0de62ce046eef9bf4b9198f25be.jpg', '{{ url:site }}files/large/d7f5b0de62ce046eef9bf4b9198f25be.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 28, '', 0, 1374121941, 0),
('866e74b7c6d9e84', 24, 1, 'i', 'nude-shoes-1.jpg', 'e957ccaa1303ba905bc13c600ac2970f.jpg', '{{ url:site }}files/large/e957ccaa1303ba905bc13c600ac2970f.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 207, '', 0, 1374121712, 0),
('86ec18a88d30fbd', 54, 1, 'i', 'lotions-and-potions.jpg', 'd7f5b0de62ce046eef9bf4b9198f25be.jpg', '{{ url:site }}files/large/d7f5b0de62ce046eef9bf4b9198f25be.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 28, '', 0, 1374121941, 0),
('87477593c48a96e', 37, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('8843cb39cf500b1', 63, 10, 'i', 'descarga-3.jpg', '4bf991c6b8e3109f0145b87cc4508367.jpg', '{{ url:site }}files/large/4bf991c6b8e3109f0145b87cc4508367.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 4, '', 0, 1401884064, 5),
('88721675c62aae4', 26, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('89cecb97c659340', 91, 10, 'i', '2.jpg', 'eae1d57d73fb617eb23f98ed6546e474.jpg', '{{ url:site }}files/large/eae1d57d73fb617eb23f98ed6546e474.jpg', '', '.jpg', 'image/jpeg', '', 270, 270, 23, '', 0, 1416588069, 0),
('8a06e3998496a19', 38, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('8a249ba0e0950fd', 23, 1, 'i', 'nude-shoes-2.jpg', 'e82fa0be0793f42eca14e8cf27f6f08c.jpg', '{{ url:site }}files/large/e82fa0be0793f42eca14e8cf27f6f08c.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 252, '', 0, 1374121715, 1),
('8b1701fc529d18c', 64, 10, 'i', 'descarga-1.jpg', '9e6251a4f50c8dde54a1b7a3754854e4.jpg', '{{ url:site }}files/large/9e6251a4f50c8dde54a1b7a3754854e4.jpg', '', '.jpg', 'image/jpeg', '', 323, 323, 12, '', 0, 1401884064, 6),
('8b3e335a67bb6f9', 39, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('8e8ec12922c453e', 42, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('8f00bdb5ff1bdc6', 107, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('8f108bed1d89e3b', 100, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('8f937c237daa043', 51, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('8ff6d38156c9328', 20, 1, 'i', 'nude-shoes-1.jpg', 'e957ccaa1303ba905bc13c600ac2970f.jpg', '{{ url:site }}files/large/e957ccaa1303ba905bc13c600ac2970f.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 207, '', 0, 1374121712, 0),
('902f9a35c6a7fab', 108, 13, 'i', 'mfc_-1019.jpg', 'af5a5f8a8c0e429fcc3a9151c03dc27b.jpg', '{{ url:site }}files/large/af5a5f8a8c0e429fcc3a9151c03dc27b.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 129, '', 0, 1428962798, 0),
('909c260d5d69dfe', 119, 10, 'i', 'prod.jpg', '86bb74e700900033a3498034c8922078.jpg', '{{ url:site }}files/large/86bb74e700900033a3498034c8922078.jpg', '', '.jpg', 'image/jpeg', '', 600, 600, 166, '', 0, 1421767231, 0),
('90d9781a4d705a2', 50, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('90ecd01e7ff990c', 64, 10, 'i', 'descarga-5.jpg', 'b6a0b4707cac78fc6963cf37900e56d8.jpg', '{{ url:site }}files/large/b6a0b4707cac78fc6963cf37900e56d8.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 8, '', 0, 1401884063, 1),
('91699d7ca87d5d6', 108, 13, 'i', 'bomberg_webmillos1000x1000_col_1002_relojmillonarios_tiendavirtual_1.jpg', 'b6cc3799b7bfbb9d81fc38bd95ec4d2d.jpg', '{{ url:site }}files/large/b6cc3799b7bfbb9d81fc38bd95ec4d2d.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 86, '', 0, 1429595748, 0),
('91a90d0625481cf', 39, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('92108c2cf484b2d', 44, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('93c707409db9977', 115, 10, 'i', '20111220135829778_6_6611.jpg', '48547a92fe9b612f9bc01ee745828ffb.jpg', '{{ url:site }}files/large/48547a92fe9b612f9bc01ee745828ffb.jpg', '', '.jpg', 'image/jpeg', '', 415, 415, 17, '', 0, 1421691943, 2),
('9467b6ab4cb8f69', 98, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('948cd29d1ed0552', 34, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('94f5df765f3a33f', 108, 13, 'i', 'mfc_1011.jpg', 'bc2d205e99b85756346749c6229c184d.jpg', '{{ url:site }}files/large/bc2d205e99b85756346749c6229c184d.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 167, '', 0, 1429048913, 0),
('9517fd0bf8faa65', 108, 10, 'i', 'mfc_0352.jpg', '5853e95860e865439c7fd6b18a2e625e.jpg', '{{ url:site }}files/large/5853e95860e865439c7fd6b18a2e625e.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 70, '', 0, 1428622480, 0),
('95f32b0fcffab43', 83, 10, 'i', '1.png', '9e2c2ccf6fcd440ced60d225f02ec3eb.png', '{{ url:site }}files/large/9e2c2ccf6fcd440ced60d225f02ec3eb.png', '', '.png', 'image/png', '', 133, 133, 56, '', 0, 1421357515, 0),
('9a4405d006c899c', 108, 13, 'i', 'img_1012.jpg', 'e236a6503c5751df7399895e5f4177eb.jpg', '{{ url:site }}files/large/e236a6503c5751df7399895e5f4177eb.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 1599, '', 0, 1428975548, 0),
('9b2c84f85da5eb2', 44, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('9cfd42ae1523070', 27, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('9dd4a89f2abf44e', 108, 10, 'i', 'mfc_0428.jpg', 'c01600045b5056fb42bfc037b5137af8.jpg', '{{ url:site }}files/large/c01600045b5056fb42bfc037b5137af8.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 100, '', 0, 1428622486, 0),
('9f3f7dfe61d6b2f', 28, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('a03159802dbbb95', 64, 10, 'i', 'descarga-6.jpg', '5981b46923dd99ae409d7a86d6bdb033.jpg', '{{ url:site }}files/large/5981b46923dd99ae409d7a86d6bdb033.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 6, '', 0, 1401884063, 3),
('a040537e366eb48', 108, 13, 'i', 'doble2013-1.jpg', 'b87a08ab8122928785d5930e3df15604.jpg', '{{ url:site }}files/large/b87a08ab8122928785d5930e3df15604.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 162, '', 0, 1429336969, 0),
('a1581d9311145df', 108, 10, 'i', 'mfc_0229.jpg', 'ee21d84a5f68e5b94ad0ea2881330073.jpg', '{{ url:site }}files/large/ee21d84a5f68e5b94ad0ea2881330073.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 105, '', 0, 1428622513, 0),
('a18d06f1b928fc5', 32, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('a2dcfed9fe8acba', 108, 13, 'i', 'mfc_1011.jpg', 'e7430b125fe77b44f61c408b2659cc3d.jpg', '{{ url:site }}files/large/e7430b125fe77b44f61c408b2659cc3d.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 170, '', 0, 1428646450, 0),
('a37396e68e736dd', 108, 13, 'i', 'mfc_0413.jpg', '8a5389ef8007895c2f9b68e27794e2c8.jpg', '{{ url:site }}files/large/8a5389ef8007895c2f9b68e27794e2c8.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 94, '', 0, 1429052055, 0),
('a5b4b71908442b4', 108, 13, 'i', 'mfc_0128.jpg', '44aa3d0f74cf81cf820bb25439597233.jpg', '{{ url:site }}files/large/44aa3d0f74cf81cf820bb25439597233.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 101, '', 0, 1429074817, 0),
('a622e1649d9fd4f', 65, 10, 'i', 'descarga-5.jpg', 'b6a0b4707cac78fc6963cf37900e56d8.jpg', '{{ url:site }}files/large/b6a0b4707cac78fc6963cf37900e56d8.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 8, '', 0, 1401884063, 1),
('a84f36107b640a7', 39, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('a8b4ad187110b8f', 108, 10, 'i', 'mfc_0249.jpg', '80f3ec6351e09cc87691b46d748c088f.jpg', '{{ url:site }}files/large/80f3ec6351e09cc87691b46d748c088f.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 63, '', 0, 1428622514, 0),
('aa2ded689c2b9e4', 108, 10, 'i', 'mfc_0227.jpg', '57231261be1126167b6af3d627990f3d.jpg', '{{ url:site }}files/large/57231261be1126167b6af3d627990f3d.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 94, '', 0, 1428622512, 0),
('aa72ed34af90b72', 30, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('aac79a44d0cd995', 64, 10, 'i', 'descarga.jpg', 'dc26cfd65206c2ca4702dd6ebb2dd9fd.jpg', '{{ url:site }}files/large/dc26cfd65206c2ca4702dd6ebb2dd9fd.jpg', '', '.jpg', 'image/jpeg', '', 144, 144, 2, '', 0, 1401882305, 0),
('abc0a4cf8e05cbf', 92, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('ac0a966f833c56c', 63, 10, 'i', 'descarga-4.jpg', '2bdf186511a289c14df0fd124669fbc0.jpg', '{{ url:site }}files/large/2bdf186511a289c14df0fd124669fbc0.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 5, '', 0, 1401884064, 4),
('ac5e8aa29f05a25', 47, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('adb606dd793134f', 30, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('ae5acd487b8bfff', 108, 13, 'i', 'mfc_-1020.jpg', 'a7331182b42d38a436cdd50e94bc3a69.jpg', '{{ url:site }}files/large/a7331182b42d38a436cdd50e94bc3a69.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 106, '', 0, 1428979154, 0),
('ae5eafb269b5902', 108, 10, 'i', 'mfc_0095.jpg', '5cdb002c22e0b2b6858c50c5e02f9d52.jpg', '{{ url:site }}files/large/5cdb002c22e0b2b6858c50c5e02f9d52.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 145, '', 0, 1428622507, 0),
('af12d5044ed34ab', 61, 10, 'i', '0e900de2738f2cfe5cf95286a90981ac.png', 'c8212dafee9ff50a5a963aed55c691ab.png', '{{ url:site }}files/large/c8212dafee9ff50a5a963aed55c691ab.png', '', '.png', 'image/png', '', 500, 500, 91, '', 0, 1400073702, 0),
('af4039a52281944', 43, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('af769070d922da4', 108, 13, 'i', 'mfc_0428.jpg', 'a9babaf1b6da82df49d7fd2011d863f0.jpg', '{{ url:site }}files/large/a9babaf1b6da82df49d7fd2011d863f0.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 94, '', 0, 1429052133, 0),
('b0651b33bec93b7', 108, 10, 'i', 'mfc_0312.jpg', 'a2b83673abc26ea8a905c8a475a2a31f.jpg', '{{ url:site }}files/large/a2b83673abc26ea8a905c8a475a2a31f.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 89, '', 0, 1428622520, 0),
('b090eb58b342eb9', 49, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('b13833139b21f03', 108, 10, 'i', 'mfc_0101.jpg', 'adafdf0ee1481af94ed0286b5bb805c9.jpg', '{{ url:site }}files/large/adafdf0ee1481af94ed0286b5bb805c9.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 116, '', 0, 1428622505, 0),
('b156ee9b322cdd6', 50, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('b255d5a303cbd6c', 108, 10, 'i', 'mfc_0435.jpg', '8a8e03021ba7092c269fb2529fba668a.jpg', '{{ url:site }}files/large/8a8e03021ba7092c269fb2529fba668a.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 96, '', 0, 1428622487, 0),
('b27a9097dbd4f9f', 36, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('b2f8767ac766481', 27, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('b361821675bf2ac', 108, 10, 'i', 'mfc_0016.jpg', 'b4ff4b339d9dce67b2f9fd842d5a1d3b.jpg', '{{ url:site }}files/large/b4ff4b339d9dce67b2f9fd842d5a1d3b.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 127, '', 0, 1428622503, 0),
('b438738211f78ed', 40, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('b48d5cca10758b7', 108, 13, 'i', 'mfc_0428.jpg', 'bd99b3fd3f4ed06f8b81ed45f4dd3b5e.jpg', '{{ url:site }}files/large/bd99b3fd3f4ed06f8b81ed45f4dd3b5e.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 94, '', 0, 1429052088, 0),
('b5202d5eef003f0', 45, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('b5bf0535a66e8c4', 42, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2);
INSERT INTO `default_files` (`id`, `folder_id`, `user_id`, `type`, `name`, `filename`, `path`, `description`, `extension`, `mimetype`, `keywords`, `width`, `height`, `filesize`, `alt_attribute`, `download_count`, `date_added`, `sort`) VALUES
('b6315e32b157e9c', 105, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('b6b554fa6f5698b', 108, 10, 'i', 'mfc_1010.jpg', '4c8eee6358edc053fb25d6a35cedcfd4.jpg', '{{ url:site }}files/large/4c8eee6358edc053fb25d6a35cedcfd4.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 107, '', 0, 1428622492, 0),
('b898944e87befcd', 108, 13, 'i', 'mfc_1014.jpg', '95029fc93a959905c664a0b5ee88c81f.jpg', '{{ url:site }}files/large/95029fc93a959905c664a0b5ee88c81f.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 74, '', 0, 1429052688, 0),
('b95706ba4374b30', 18, 2, 'i', 'strainer-web-size-blue.jpg', '7d8504043c9774d036504c14311ce096.jpg', '{{ url:site }}files/large/7d8504043c9774d036504c14311ce096.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 388, '', 0, 1374171938, 1),
('b985577f98130e8', 120, 10, 'i', 'prod.jpg', '86bb74e700900033a3498034c8922078.jpg', '{{ url:site }}files/large/86bb74e700900033a3498034c8922078.jpg', '', '.jpg', 'image/jpeg', '', 600, 600, 166, '', 0, 1421767231, 0),
('baeade8b3ab7f65', 108, 10, 'i', 'mfc_0083.jpg', '50f2c45712105fd2bd1f966372c8ad3a.jpg', '{{ url:site }}files/large/50f2c45712105fd2bd1f966372c8ad3a.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 146, '', 0, 1428622506, 0),
('bde47a2ee20ba06', 50, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('be17bb20dcee6b0', 108, 10, 'i', 'mfc_0146.jpg', '512149bb14956e7c0b5a7da12562d45d.jpg', '{{ url:site }}files/large/512149bb14956e7c0b5a7da12562d45d.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 89, '', 0, 1428622507, 0),
('be8491b318a7fe6', 33, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('beea4cc0fb2c6fc', 26, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('befcbd82989541f', 108, 10, 'i', 'mfc_0160.jpg', '7cc52fe29a63c8e4553bee1874f138c7.jpg', '{{ url:site }}files/large/7cc52fe29a63c8e4553bee1874f138c7.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 127, '', 0, 1428622508, 0),
('bf78227cb5e401c', 21, 1, 'i', 'nude-shoes-2.jpg', 'e82fa0be0793f42eca14e8cf27f6f08c.jpg', '{{ url:site }}files/large/e82fa0be0793f42eca14e8cf27f6f08c.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 252, '', 0, 1374121715, 1),
('bf9519d794689fe', 108, 10, 'i', 'mfc_1013.jpg', '2faef8cb17b2129758de5a3edf8d829d.jpg', '{{ url:site }}files/large/2faef8cb17b2129758de5a3edf8d829d.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 140, '', 0, 1428622494, 0),
('c06f687a7d8db39', 104, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('c07711e13adebc4', 64, 10, 'i', 'descarga-4.jpg', '2bdf186511a289c14df0fd124669fbc0.jpg', '{{ url:site }}files/large/2bdf186511a289c14df0fd124669fbc0.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 5, '', 0, 1401884064, 4),
('c07c8258fd645a7', 33, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('c0c50db017d6ff3', 20, 1, 'i', 'nude-shoes-2.jpg', 'e82fa0be0793f42eca14e8cf27f6f08c.jpg', '{{ url:site }}files/large/e82fa0be0793f42eca14e8cf27f6f08c.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 252, '', 0, 1374121715, 1),
('c0f4f04bb2a5ec7', 108, 10, 'i', 'mfc_0058.jpg', '78614cc2f47cac30ddecdec8fba1930e.jpg', '{{ url:site }}files/large/78614cc2f47cac30ddecdec8fba1930e.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 132, '', 0, 1428622504, 0),
('c0f857ea44ea79d', 108, 10, 'i', 'mfc_0177.jpg', '2bbfb715bb2886ea758704ab6672a577.jpg', '{{ url:site }}files/large/2bbfb715bb2886ea758704ab6672a577.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 125, '', 0, 1428622509, 0),
('c1497ebd8aa25ea', 46, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('c2792347a689aa3', 36, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('c354225b0ba659c', 48, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('c4a21fc4ad11561', 108, 10, 'i', 'mfc_0151.jpg', 'd1b161825916f8d11ffbb2e4cab34925.jpg', '{{ url:site }}files/large/d1b161825916f8d11ffbb2e4cab34925.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 84, '', 0, 1428622508, 0),
('c5b2c920b9256e3', 108, 10, 'i', 'mfc_0257.jpg', 'c9e9caed37ac03e3b0c159643d043d9b.jpg', '{{ url:site }}files/large/c9e9caed37ac03e3b0c159643d043d9b.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 110, '', 0, 1428622516, 0),
('c62fcfcd75d9a27', 27, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('c69846791a78cab', 108, 10, 'i', 'mfc_0357.jpg', '8f48d93894557943ca25a1836c6d756e.jpg', '{{ url:site }}files/large/8f48d93894557943ca25a1836c6d756e.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 128, '', 0, 1428622481, 0),
('c7aec9a02f589ae', 108, 10, 'i', 'mfc_1012.jpg', 'aeb005fa4197593d4248637340cf5a54.jpg', '{{ url:site }}files/large/aeb005fa4197593d4248637340cf5a54.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 151, '', 0, 1428622493, 0),
('c7cf2fa67fa18fc', 99, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('c84327e5197b44f', 63, 10, 'i', 'descarga-1.jpg', '9e6251a4f50c8dde54a1b7a3754854e4.jpg', '{{ url:site }}files/large/9e6251a4f50c8dde54a1b7a3754854e4.jpg', '', '.jpg', 'image/jpeg', '', 323, 323, 12, '', 0, 1401884064, 6),
('c991ea77ddeb618', 108, 10, 'i', 'mfc_0189.jpg', '7274aa5729679e5b4f3740a1ebe52984.jpg', '{{ url:site }}files/large/7274aa5729679e5b4f3740a1ebe52984.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 121, '', 0, 1428622510, 0),
('c9b6e2df7f827dc', 108, 13, 'i', 'img_1015.jpg', 'aa4f2f501ee0b2f7a6912e50089150da.jpg', '{{ url:site }}files/large/aa4f2f501ee0b2f7a6912e50089150da.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 212, '', 0, 1429062592, 0),
('ca63242c08dcf2f', 21, 1, 'i', 'nude-shoes-1.jpg', 'e957ccaa1303ba905bc13c600ac2970f.jpg', '{{ url:site }}files/large/e957ccaa1303ba905bc13c600ac2970f.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 207, '', 0, 1374121712, 0),
('cab06c413b053ac', 108, 13, 'i', 'mfc_1022.jpg', '8a01ce5991dd1725a8fdb2aadc181c46.jpg', '{{ url:site }}files/large/8a01ce5991dd1725a8fdb2aadc181c46.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 147, '', 0, 1429068161, 0),
('cac1b67a0d8cc12', 52, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('caf18e306b240f8', 63, 10, 'i', 'descarga-5.jpg', 'b6a0b4707cac78fc6963cf37900e56d8.jpg', '{{ url:site }}files/large/b6a0b4707cac78fc6963cf37900e56d8.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 8, '', 0, 1401884063, 1),
('cb3fabe0ece1406', 37, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('cc1e8a7465c9408', 32, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('cd3eef1dc45c0ef', 114, 10, 'i', 'images.jpg', 'dc5f6ba4053d693d6a7416cf576f629b.jpg', '{{ url:site }}files/large/dc5f6ba4053d693d6a7416cf576f629b.jpg', '', '.jpg', 'image/jpeg', '', 274, 274, 5, '', 0, 1421691293, 2),
('cdf7778826ff934', 39, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('ce674f94b796067', 45, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('cee660a1b4ff0dd', 35, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('cf1f746461ee2d4', 64, 10, 'i', 'descarga-2.jpg', '3bf7b514944b82bf44ab24949af39e78.jpg', '{{ url:site }}files/large/3bf7b514944b82bf44ab24949af39e78.jpg', '', '.jpg', 'image/jpeg', '', 340, 340, 12, '', 0, 1401884063, 2),
('cfa36cb40a8f1e1', 34, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('d1ee21d7c98a6e5', 108, 13, 'i', 'mfc_0399.jpg', 'bf39371bca596f07d2e7cbbc3cbfc33a.jpg', '{{ url:site }}files/large/bf39371bca596f07d2e7cbbc3cbfc33a.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 336, '', 0, 1429050319, 0),
('d293bf0b1b28c2d', 108, 10, 'i', 'mfc_0128.jpg', '88a71280046b24835cc0048cf2b3f6fc.jpg', '{{ url:site }}files/large/88a71280046b24835cc0048cf2b3f6fc.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 100, '', 0, 1428622505, 0),
('d32380cf780b41b', 25, 1, 'i', 'nude-shoes-1.jpg', 'e957ccaa1303ba905bc13c600ac2970f.jpg', '{{ url:site }}files/large/e957ccaa1303ba905bc13c600ac2970f.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 207, '', 0, 1374121712, 0),
('d36ed748f5c5b5e', 108, 10, 'i', 'mfc_0355.jpg', 'bd53aac7804970dd8b9ee0a64ef1acc2.jpg', '{{ url:site }}files/large/bd53aac7804970dd8b9ee0a64ef1acc2.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 72, '', 0, 1428622480, 0),
('d458c4e6d574ef6', 108, 10, 'i', 'mfc_0270.jpg', '2942b4f42ac8fc03e15f1689c86404f5.jpg', '{{ url:site }}files/large/2942b4f42ac8fc03e15f1689c86404f5.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 140, '', 0, 1428622517, 0),
('d53be276d0940a7', 53, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('d567b5dd3db1987', 51, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('d5db761f2a5d241', 29, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('d6e27adefab9987', 52, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('d92d2cb229406e3', 109, 10, 'i', 'produc6-g.jpg', '10f5c7a0d3f52610884d401a0f085bc6.jpg', '{{ url:site }}files/large/10f5c7a0d3f52610884d401a0f085bc6.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 81, '', 0, 1421690033, 0),
('d9927f318e13e44', 28, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('d99a32a77bfc70d', 22, 1, 'i', 'nude-shoes-2.jpg', 'e82fa0be0793f42eca14e8cf27f6f08c.jpg', '{{ url:site }}files/large/e82fa0be0793f42eca14e8cf27f6f08c.jpg', '', '.jpg', 'image/jpeg', '', 700, 688, 252, '', 0, 1374121715, 1),
('db6a2cdad0a4d20', 61, 10, 'i', '37cb4ae91d868097d2e7e556ce17cdc6-1.jpg', '16f406189c27b2c0071d9c0f31de9f7a.jpg', '{{ url:site }}files/large/16f406189c27b2c0071d9c0f31de9f7a.jpg', '', '.jpg', 'image/jpeg', '', 490, 490, 130, '', 0, 1400075053, 1),
('dc0825472fbfa9c', 29, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('dccb83a94082468', 108, 10, 'i', 'mfc_1009.jpg', '7f7b991d9ac0da6d6003a6ed7389437d.jpg', '{{ url:site }}files/large/7f7b991d9ac0da6d6003a6ed7389437d.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 100, '', 0, 1428622490, 0),
('dd747fa492d5426', 108, 10, 'i', 'mfc_1015.jpg', '60abe81bf0b563f6043d4f2a451d4ff4.jpg', '{{ url:site }}files/large/60abe81bf0b563f6043d4f2a451d4ff4.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 133, '', 0, 1428622494, 0),
('de67363bb311149', 18, 2, 'i', 'strainer-web-size-red.jpg', 'e0f19e37fdd13eaa2d64ea9682315a6b.jpg', '{{ url:site }}files/large/e0f19e37fdd13eaa2d64ea9682315a6b.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 473, '', 0, 1374171922, 0),
('de7575213e87c9d', 51, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('dec32d829a4cb34', 108, 10, 'i', 'mfc_0377.jpg', '7841f3c4a36b17f3a0bbf9ba687af395.jpg', '{{ url:site }}files/large/7841f3c4a36b17f3a0bbf9ba687af395.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 82, '', 0, 1428622482, 0),
('e0786047d574f11', 108, 13, 'i', 'bomberg_webmillos1000x1000_col_1002_relojmillonarios_tiendavirtual_1.jpg', 'e21038e25a7418a869183775b6f85fe6.jpg', '{{ url:site }}files/large/e21038e25a7418a869183775b6f85fe6.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 86, '', 0, 1429595750, 0),
('e1e96b9afe627a2', 63, 10, 'i', 'descarga-6.jpg', '5981b46923dd99ae409d7a86d6bdb033.jpg', '{{ url:site }}files/large/5981b46923dd99ae409d7a86d6bdb033.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 6, '', 0, 1401884063, 3),
('e23d87c693a81c2', 65, 10, 'i', 'descarga-4.jpg', '2bdf186511a289c14df0fd124669fbc0.jpg', '{{ url:site }}files/large/2bdf186511a289c14df0fd124669fbc0.jpg', '', '.jpg', 'image/jpeg', '', 204, 204, 5, '', 0, 1401884064, 4),
('e31c1a630e0f7fd', 93, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('e45429f6c8dfa15', 42, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('e4d19fc58e1b16f', 43, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('e4d5e19c1b70d21', 51, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('e56e271df9c9c83', 29, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('e5d68c4c9133250', 43, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('e679e24078c983e', 108, 13, 'i', 'mfc_0241-1.jpg', 'e024d1e812778e6799f7c679be82d976.jpg', '{{ url:site }}files/large/e024d1e812778e6799f7c679be82d976.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 356, '', 0, 1429594838, 0),
('e71dc5d84a53399', 108, 10, 'i', 'mfc_0187.jpg', 'a063594064b6ed64b9503aab988648ab.jpg', '{{ url:site }}files/large/a063594064b6ed64b9503aab988648ab.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 88, '', 0, 1428622511, 0),
('e93bff7b43de7c5', 37, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('ea3d71e4f7a804d', 108, 13, 'i', 'mfc_0262.jpg', '301009fa26b85e25c19d11bee82b4f7b.jpg', '{{ url:site }}files/large/301009fa26b85e25c19d11bee82b4f7b.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 111, '', 0, 1429062182, 0),
('eaaf42cabb33d30', 36, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('eda1f3f1ccec9f5', 108, 10, 'i', 'mfc_0336.jpg', '991bd73820cd9b1686241a250ab7a18d.jpg', '{{ url:site }}files/large/991bd73820cd9b1686241a250ab7a18d.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 97, '', 0, 1428622480, 0),
('edd2f9ac11655cf', 88, 10, 'i', '2.jpg', 'eae1d57d73fb617eb23f98ed6546e474.jpg', '{{ url:site }}files/large/eae1d57d73fb617eb23f98ed6546e474.jpg', '', '.jpg', 'image/jpeg', '', 270, 270, 23, '', 0, 1416588069, 0),
('ee0e7b52bda6656', 108, 10, 'i', 'mfc_0285.jpg', 'd9a60c14c73876ef60979fabdae61d28.jpg', '{{ url:site }}files/large/d9a60c14c73876ef60979fabdae61d28.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 121, '', 0, 1428622518, 0),
('eedeb010651f9df', 72, 10, 'i', 'nike-7860-61217-1-product.jpg', 'f1efbe00dc94b986dd24acc4e2785b81.jpg', '{{ url:site }}files/large/f1efbe00dc94b986dd24acc4e2785b81.jpg', '', '.jpg', 'image/jpeg', '', 620, 620, 61, '', 0, 1413310065, 0),
('ef5fe01026e300a', 108, 10, 'i', 'mfc_0480.jpg', 'f23dddd090c1b6d66f52bba649252bdb.jpg', '{{ url:site }}files/large/f23dddd090c1b6d66f52bba649252bdb.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 112, '', 0, 1428622488, 0),
('ef7943b0525ed97', 41, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('efa7f567e7dbe25', 108, 10, 'i', 'mfc_0164.jpg', 'ec52115db36699f04eed885816e15ba1.jpg', '{{ url:site }}files/large/ec52115db36699f04eed885816e15ba1.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 85, '', 0, 1428622509, 0),
('efc20ab45570cda', 37, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3),
('f0291643d939cc8', 19, 2, 'i', 'strainer-web-size-blue.jpg', '7d8504043c9774d036504c14311ce096.jpg', '{{ url:site }}files/large/7d8504043c9774d036504c14311ce096.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 388, '', 0, 1374171938, 1),
('f23fd84cbb16448', 108, 10, 'i', 'mfc_0388.jpg', 'dc29d09d1bcd83d5bce5bb5793e78ea0.jpg', '{{ url:site }}files/large/dc29d09d1bcd83d5bce5bb5793e78ea0.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 116, '', 0, 1428622483, 0),
('f25c98d847029d3', 108, 13, 'i', 'mfc_0160.jpg', '21b477ed82b5c2ebca52e318711fb3d4.jpg', '{{ url:site }}files/large/21b477ed82b5c2ebca52e318711fb3d4.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 127, '', 0, 1429118225, 0),
('f33c0afea427877', 108, 10, 'i', 'mfc_0372.jpg', '056dcdad70e02428d7b584ddb15b6bc5.jpg', '{{ url:site }}files/large/056dcdad70e02428d7b584ddb15b6bc5.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 92, '', 0, 1428622482, 0),
('f465eb8bcf1137e', 89, 10, 'i', '2.jpg', 'eae1d57d73fb617eb23f98ed6546e474.jpg', '{{ url:site }}files/large/eae1d57d73fb617eb23f98ed6546e474.jpg', '', '.jpg', 'image/jpeg', '', 270, 270, 23, '', 0, 1416588069, 0),
('f4790701d982132', 46, 2, 'i', 'magenta-shoes-2.jpg', 'e40174becccd1f24e26951828a07c29a.jpg', '{{ url:site }}files/large/e40174becccd1f24e26951828a07c29a.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 159, '', 0, 1374172527, 1),
('f6e4d7414bb9dc3', 108, 10, 'i', 'MFC 464.jpg', 'f4e7260971f26c8c55e570c3082f6f32.jpg', '{{ url:site }}files/large/f4e7260971f26c8c55e570c3082f6f32.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 109, '', 0, 1428622496, 0),
('f718ead44661529', 5, 1, 'i', 'firesale.png', '4a6a2246edcbaf223de95c1a5c8b43f5.png', '{{ url:site }}files/large/4a6a2246edcbaf223de95c1a5c8b43f5.png', '', '.png', 'image/png', '', 587, 330, 110, '', 0, 1374121053, 0),
('f80ca912c787222', 108, 13, 'i', 'mfc_0480.jpg', '260d6dba10b80217c8e352f7e2d39769.jpg', '{{ url:site }}files/large/260d6dba10b80217c8e352f7e2d39769.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 107, '', 0, 1429069604, 0),
('f8235f02b2936ae', 115, 10, 'i', 'descarga.jpg', '692065c24c2fa510fe11485544cad571.jpg', '{{ url:site }}files/large/692065c24c2fa510fe11485544cad571.jpg', '', '.jpg', 'image/jpeg', '', 225, 225, 3, '', 0, 1421691601, 0),
('f86db43c0f6c2e0', 50, 2, 'i', 'red-shoes-2.jpg', 'c809343671e7e1d91ef471487d90d100.jpg', '{{ url:site }}files/large/c809343671e7e1d91ef471487d90d100.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 165, '', 0, 1374172527, 2),
('f8758ea9b15e7ff', 55, 1, 'i', 'lotions-and-potions.jpg', 'd7f5b0de62ce046eef9bf4b9198f25be.jpg', '{{ url:site }}files/large/d7f5b0de62ce046eef9bf4b9198f25be.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 28, '', 0, 1374121941, 0),
('f8ebe784ff5a37f', 31, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('fbddf084fc01c73', 77, 10, 'i', 'fit_purple.jpg', '377dddf66c06e03bbb08241019406e18.jpg', '{{ url:site }}files/large/377dddf66c06e03bbb08241019406e18.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 45, '', 0, 1413370789, 0),
('fc92cd7120de8a8', 108, 13, 'i', 'mfc_1006.jpg', 'b61a6f4f2e39d8b3b92ad4b09b956570.jpg', '{{ url:site }}files/large/b61a6f4f2e39d8b3b92ad4b09b956570.jpg', '', '.jpg', 'image/jpeg', '', 300, 300, 131, '', 0, 1429072752, 0),
('fd4de096e3b12e3', 108, 10, 'i', 'mfc_0155.jpg', '2ddd509f4faea2f9df7f45c36e0255eb.jpg', '{{ url:site }}files/large/2ddd509f4faea2f9df7f45c36e0255eb.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 91, '', 0, 1428622507, 0),
('fe0e111355ee23f', 101, 10, 'i', 'banner-marranito.png', 'c569a126c3c98dfcdd1edfa5b41e7795.png', '{{ url:site }}files/large/c569a126c3c98dfcdd1edfa5b41e7795.png', '', '.png', 'image/png', '', 264, 264, 49, '', 0, 1395276512, 0),
('fe22afaca55d7fd', 108, 10, 'i', 'mfc_0241.jpg', '90117ea38794723b20fc084c888ebc19.jpg', '{{ url:site }}files/large/90117ea38794723b20fc084c888ebc19.jpg', '', '.jpg', 'image/jpeg', '', 1000, 1000, 112, '', 0, 1428622514, 0),
('ff3980ad6a3ce43', 19, 2, 'i', 'strainer-web-size-red.jpg', 'e0f19e37fdd13eaa2d64ea9682315a6b.jpg', '{{ url:site }}files/large/e0f19e37fdd13eaa2d64ea9682315a6b.jpg', '', '.jpg', 'image/jpeg', '', 800, 800, 473, '', 0, 1374171922, 0),
('ff7a9ee0e1a0b95', 35, 2, 'i', 'red-shoes.jpg', 'd5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '{{ url:site }}files/large/d5e94f32a3d3e7c484c04f9c351f6fc4.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 156, '', 0, 1374172513, 0),
('ffd930890326b68', 29, 2, 'i', 'green-shoes-2.jpg', 'f15244066e9a18ea49aa436989728268.jpg', '{{ url:site }}files/large/f15244066e9a18ea49aa436989728268.jpg', '', '.jpg', 'image/jpeg', '', 700, 700, 137, '', 0, 1374172527, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_file_folders`
--

CREATE TABLE IF NOT EXISTS `default_file_folders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'local',
  `remote_container` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date_added` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=126 ;

--
-- Volcado de datos para la tabla `default_file_folders`
--

INSERT INTO `default_file_folders` (`id`, `parent_id`, `slug`, `name`, `location`, `remote_container`, `date_added`, `sort`, `hidden`) VALUES
(1, 0, 'currency-images', 'Currency Images', 'local', '', 1373940105, 1373940105, 0),
(2, 0, 'category-images', 'Category Images', 'local', '', 1373940138, 1373940138, 0),
(3, 0, 'brand-images', 'Brand Images', 'local', '', 1373940138, 1373940138, 0),
(4, 0, 'product-images', 'Product Images', 'local', '', 1373940138, 1373940138, 0),
(5, 3, 'firesale-example', 'FireSale Example', 'local', '', 1374120843, 1374120843, 0),
(6, 4, 'holiday-shower-gel', 'Holiday Shower Gel', 'local', '', 1374121572, 1374121572, 0),
(7, 4, 'nude-court-shoe', 'Nude Court Shoe', 'local', '', 1374121711, 1374121711, 0),
(10, 4, 'colander', 'Colander', 'local', '', 1374171922, 1374171922, 0),
(16, 4, 'ballet-pumps', 'Ballet Pumps', 'local', '', 1374172513, 1374172513, 0),
(18, 10, 'colander-2', 'Colander (2)', 'local', '', 1374188575, 1374188575, 0),
(19, 10, 'colander-3', 'Colander (3)', 'local', '', 1374188583, 1374188583, 0),
(20, 7, 'nude-court-shoe-2', 'Nude Court Shoe (2)', 'local', '', 1374188630, 1374188630, 0),
(21, 7, 'nude-court-shoe-3', 'Nude Court Shoe (3)', 'local', '', 1374188636, 1374188636, 0),
(22, 7, 'nude-court-shoe-4', 'Nude Court Shoe (4)', 'local', '', 1374188642, 1374188642, 0),
(23, 7, 'nude-court-shoe-5', 'Nude Court Shoe (5)', 'local', '', 1374188655, 1374188655, 0),
(24, 7, 'nude-court-shoe-6', 'Nude Court Shoe (6)', 'local', '', 1374188662, 1374188662, 0),
(25, 7, 'nude-court-shoe-7', 'Nude Court Shoe (7)', 'local', '', 1374188675, 1374188675, 0),
(26, 16, 'ballet-pumps-2', 'Ballet Pumps (2)', 'local', '', 1374189336, 1374189336, 0),
(27, 16, 'ballet-pumps-3', 'Ballet Pumps (3)', 'local', '', 1374189347, 1374189347, 0),
(28, 16, 'ballet-pumps-4', 'Ballet Pumps (4)', 'local', '', 1374189369, 1374189369, 0),
(29, 16, 'ballet-pumps-5', 'Ballet Pumps (5)', 'local', '', 1374189383, 1374189383, 0),
(30, 16, 'ballet-pumps-6', 'Ballet Pumps (6)', 'local', '', 1374189397, 1374189397, 0),
(31, 16, 'ballet-pumps-7', 'Ballet Pumps (7)', 'local', '', 1374189398, 1374189398, 0),
(32, 16, 'ballet-pumps-8', 'Ballet Pumps (8)', 'local', '', 1374189398, 1374189398, 0),
(33, 16, 'ballet-pumps-9', 'Ballet Pumps (9)', 'local', '', 1374189399, 1374189399, 0),
(34, 16, 'ballet-pumps-10', 'Ballet Pumps (10)', 'local', '', 1374189406, 1374189406, 0),
(35, 16, 'ballet-pumps-11', 'Ballet Pumps (11)', 'local', '', 1374189407, 1374189407, 0),
(36, 16, 'ballet-pumps-12', 'Ballet Pumps (12)', 'local', '', 1374189407, 1374189407, 0),
(37, 16, 'ballet-pumps-13', 'Ballet Pumps (13)', 'local', '', 1374189408, 1374189408, 0),
(38, 16, 'ballet-pumps-14', 'Ballet Pumps (14)', 'local', '', 1374189416, 1374189416, 0),
(39, 16, 'ballet-pumps-15', 'Ballet Pumps (15)', 'local', '', 1374189416, 1374189416, 0),
(40, 16, 'ballet-pumps-16', 'Ballet Pumps (16)', 'local', '', 1374189417, 1374189417, 0),
(41, 16, 'ballet-pumps-17', 'Ballet Pumps (17)', 'local', '', 1374189418, 1374189418, 0),
(42, 16, 'ballet-pumps-18', 'Ballet Pumps (18)', 'local', '', 1374189425, 1374189425, 0),
(43, 16, 'ballet-pumps-19', 'Ballet Pumps (19)', 'local', '', 1374189426, 1374189426, 0),
(44, 16, 'ballet-pumps-20', 'Ballet Pumps (20)', 'local', '', 1374189427, 1374189427, 0),
(45, 16, 'ballet-pumps-21', 'Ballet Pumps (21)', 'local', '', 1374189427, 1374189427, 0),
(46, 16, 'ballet-pumps-22', 'Ballet Pumps (22)', 'local', '', 1374189437, 1374189437, 0),
(47, 16, 'ballet-pumps-23', 'Ballet Pumps (23)', 'local', '', 1374189437, 1374189437, 0),
(48, 16, 'ballet-pumps-24', 'Ballet Pumps (24)', 'local', '', 1374189438, 1374189438, 0),
(49, 16, 'ballet-pumps-25', 'Ballet Pumps (25)', 'local', '', 1374189438, 1374189438, 0),
(50, 16, 'ballet-pumps-26', 'Ballet Pumps (26)', 'local', '', 1374189448, 1374189448, 0),
(51, 16, 'ballet-pumps-27', 'Ballet Pumps (27)', 'local', '', 1374189448, 1374189448, 0),
(52, 16, 'ballet-pumps-28', 'Ballet Pumps (28)', 'local', '', 1374189449, 1374189449, 0),
(53, 16, 'ballet-pumps-29', 'Ballet Pumps (29)', 'local', '', 1374189450, 1374189450, 0),
(54, 6, 'holiday-shower-gel-2', 'Holiday Shower Gel (2)', 'local', '', 1391464786, 1391464786, 0),
(55, 6, 'holiday-shower-gel-3', 'Holiday Shower Gel (3)', 'local', '', 1392059316, 1392059316, 0),
(56, 6, 'holiday-shower-gel-4', 'Holiday Shower Gel (4)', 'local', '', 1392069908, 1392069908, 0),
(57, 6, 'holiday-shower-gel-5', 'Holiday Shower Gel (5)', 'local', '', 1392069929, 1392069929, 0),
(58, 6, 'holiday-shower-gel-6', 'Holiday Shower Gel (6)', 'local', '', 1392069931, 1392069931, 0),
(59, 6, 'holiday-shower-gel-3', 'Holiday Shower Gel (3)', 'local', '', 1392335401, 1392335401, 0),
(61, 2, 'cosmeticos', 'Cosmeticos', 'local', '', 1400073683, 1400073683, 0),
(62, 4, 'alcancia-imaginamos', 'Alcancia Imaginamos', 'local', '', 1401882305, 1401882305, 0),
(63, 62, 'alcancia-imaginamos-2', 'Alcancia Imaginamos (2)', 'local', '', 1401885369, 1401885369, 0),
(64, 62, 'alcancia-imaginamos-2', 'Alcancia Imaginamos (2)', 'local', '', 1403718831, 1403718831, 0),
(65, 62, 'alcancia-imaginamos-3', 'Alcancia Imaginamos (3)', 'local', '', 1403718831, 1403718831, 0),
(66, 0, 'blog-images', 'blog_images', 'local', '', 1412261055, 1412261055, 0),
(67, 4, 'running-nike-dart-10-msl-negrorojo', 'Running Nike Dart 10 msl Negro-Rojo', 'local', '', 1413309866, 1413309866, 0),
(68, 67, 'running-nike-dart-10-msl-negrorojo-2', 'Running Nike Dart 10 msl Negro-Rojo (2)', 'local', '', 1413309977, 1413309977, 0),
(69, 67, 'running-nike-dart-10-msl-negrorojo-3', 'Running Nike Dart 10 msl Negro-Rojo (3)', 'local', '', 1413309988, 1413309988, 0),
(70, 67, 'running-nike-dart-10-msl-negrorojo-4', 'Running Nike Dart 10 msl Negro-Rojo (4)', 'local', '', 1413309994, 1413309994, 0),
(71, 4, 'running-nike-dart-10-msl-negrorojo-rojo', 'Running Nike Dart 10 msl Negro-Rojo - Rojo', 'local', '', 1413310011, 1413310011, 0),
(72, 4, 'running-nike-dart-10-msl-negrorojo-amarillo', 'Running Nike Dart 10 msl Negro-Rojo - Amarillo', 'local', '', 1413310065, 1413310065, 0),
(73, 4, 'running-nike-dart-10-msl-negrorojo-verde', 'Running Nike Dart 10 msl Negro-Rojo - Verde', 'local', '', 1413310079, 1413310079, 0),
(74, 4, 'ballet-pumps-4-red', 'Ballet Pumps - 4 Red', 'local', '', 1413370758, 1413370758, 0),
(75, 4, 'ballet-pumps-4-green', 'Ballet Pumps - 4 Green', 'local', '', 1413370771, 1413370771, 0),
(76, 4, 'ballet-pumps-4-magenta', 'Ballet Pumps - 4 Magenta', 'local', '', 1413370780, 1413370780, 0),
(77, 4, 'ballet-pumps-4-purple', 'Ballet Pumps - 4 Purple', 'local', '', 1413370789, 1413370789, 0),
(78, 4, 'ballet-pumps-5-red', 'Ballet Pumps - 5 Red', 'local', '', 1413370804, 1413370804, 0),
(83, 3, 'dolce-and-gabbana', 'Dolce & Gabbana', 'local', '', 1415802893, 1415802893, 0),
(84, 4, 'chaqueta-doble-faz', 'CHAQUETA DOBLE FAZ', 'local', '', 1416586297, 1416586297, 0),
(85, 4, 'maletin-coco', 'Maletin Coco', 'local', '', 1416588069, 1416588069, 0),
(86, 85, 'maletin-coco-2', 'Maletin Coco (2)', 'local', '', 1416589178, 1416589178, 0),
(87, 85, 'maletin-coco-3', 'Maletin Coco (3)', 'local', '', 1416589301, 1416589301, 0),
(88, 85, 'maletin-coco-4', 'Maletin Coco (4)', 'local', '', 1416665466, 1416665466, 0),
(89, 85, 'maletin-coco-5', 'Maletin Coco (5)', 'local', '', 1416665466, 1416665466, 0),
(90, 85, 'maletin-coco-6', 'Maletin Coco (6)', 'local', '', 1416665479, 1416665479, 0),
(91, 85, 'maletin-coco-7', 'Maletin Coco (7)', 'local', '', 1416665479, 1416665479, 0),
(92, 6, 'holiday-shower-gel-4', 'Holiday Shower Gel (4)', 'local', '', 1416775709, 1416775709, 0),
(93, 6, 'holiday-shower-gel-5', 'Holiday Shower Gel (5)', 'local', '', 1416775713, 1416775713, 0),
(94, 6, 'holiday-shower-gel-6', 'Holiday Shower Gel (6)', 'local', '', 1416775718, 1416775718, 0),
(95, 6, 'holiday-shower-gel-7', 'Holiday Shower Gel (7)', 'local', '', 1416775723, 1416775723, 0),
(96, 6, 'holiday-shower-gel-8', 'Holiday Shower Gel (8)', 'local', '', 1416775730, 1416775730, 0),
(97, 6, 'holiday-shower-gel-9', 'Holiday Shower Gel (9)', 'local', '', 1416775730, 1416775730, 0),
(98, 6, 'holiday-shower-gel-10', 'Holiday Shower Gel (10)', 'local', '', 1416775730, 1416775730, 0),
(99, 6, 'holiday-shower-gel-11', 'Holiday Shower Gel (11)', 'local', '', 1416775730, 1416775730, 0),
(100, 6, 'holiday-shower-gel-12', 'Holiday Shower Gel (12)', 'local', '', 1416775731, 1416775731, 0),
(101, 6, 'holiday-shower-gel-13', 'Holiday Shower Gel (13)', 'local', '', 1416775732, 1416775732, 0),
(102, 6, 'holiday-shower-gel-14', 'Holiday Shower Gel (14)', 'local', '', 1416775732, 1416775732, 0),
(103, 6, 'holiday-shower-gel-15', 'Holiday Shower Gel (15)', 'local', '', 1416775732, 1416775732, 0),
(104, 6, 'holiday-shower-gel-16', 'Holiday Shower Gel (16)', 'local', '', 1416775745, 1416775745, 0),
(105, 6, 'holiday-shower-gel-17', 'Holiday Shower Gel (17)', 'local', '', 1416775745, 1416775745, 0),
(106, 6, 'holiday-shower-gel-18', 'Holiday Shower Gel (18)', 'local', '', 1416775746, 1416775746, 0),
(107, 6, 'holiday-shower-gel-19', 'Holiday Shower Gel (19)', 'local', '', 1416775746, 1416775746, 0),
(108, 0, 'products-main-images', 'products_main_images', 'local', '', 1417010466, 1417010466, 0),
(109, 2, 'mujeres', 'Mujeres', 'local', '', 1417809742, 1417809742, 0),
(112, 2, 'hombres', 'Hombres', 'local', '', 1421333505, 1421333505, 0),
(114, 2, 'ninos', 'Niños', 'local', '', 1421691282, 1421691282, 0),
(115, 2, 'accesorios', 'Accesorios', 'local', '', 1421691601, 1421691601, 0),
(119, 84, 'chaqueta-doble-faz-2', 'CHAQUETA DOBLE FAZ (2)', 'local', '', 1421767303, 1421767303, 0),
(120, 84, 'chaqueta-doble-faz-3', 'CHAQUETA DOBLE FAZ (3)', 'local', '', 1421767306, 1421767306, 0),
(121, 84, 'chaqueta-doble-faz-4', 'CHAQUETA DOBLE FAZ (4)', 'local', '', 1421767309, 1421767309, 0),
(122, 84, 'chaqueta-doble-faz-5', 'CHAQUETA DOBLE FAZ (5)', 'local', '', 1421767312, 1421767312, 0),
(123, 4, 'chaqueta-doble-faz-rojo', 'CHAQUETA DOBLE FAZ - Rojo', 'local', '', 1421767638, 1421767638, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_addresses`
--

CREATE TABLE IF NOT EXISTS `default_firesale_addresses` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_deparment` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `cedule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observation` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=79 ;

--
-- Volcado de datos para la tabla `default_firesale_addresses`
--

INSERT INTO `default_firesale_addresses` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `title`, `company`, `firstname`, `lastname`, `email`, `phone`, `address1`, `address2`, `country`, `id_deparment`, `id_city`, `cedule`, `observation`) VALUES
(43, '2015-03-18 19:10:26', NULL, 10, 0, '', NULL, 'Luis', 'Salazar', 'luis.salazar@imagina.co', '77777', 'Calle', NULL, NULL, 6, 186, '123456789', 'klaslkas'),
(44, '2015-03-18 19:10:26', NULL, 10, 0, 'Casa', '', 'Luis', 'Salazar', 'luis.salazar@imagina.co', '77777', 'Calle', '', 'CO', 6, 186, '123456789', 'klaslkas'),
(45, '2015-03-19 18:40:22', NULL, 10, 1, NULL, NULL, 'Luis', 'Salazar', 'luis.salazar@imagina.co', '77777', 'Calle', NULL, NULL, NULL, 3, NULL, NULL),
(46, '2015-03-19 18:40:22', NULL, 10, 2, NULL, NULL, 'Luis', 'Salazar', 'luis.salazar@imagina.co', '77777', 'Calle', NULL, NULL, NULL, 3, NULL, NULL),
(47, '2015-03-23 16:31:48', NULL, 13, 0, '', NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', '', 'carrera 99 # 14 a 61', NULL, NULL, 2, 11, 'ddsd', ''),
(48, '2015-03-23 16:31:48', NULL, 13, 0, 'ffgg', NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', '', 'carrera 99 # 14 a 61', NULL, NULL, 2, 11, 'ddsd', ''),
(49, '2015-04-04 20:18:52', NULL, 13, 3, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(50, '2015-04-04 20:18:52', NULL, 13, 4, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(51, '2015-04-04 20:19:10', NULL, 13, 5, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(52, '2015-04-04 20:19:10', NULL, 13, 6, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(53, '2015-04-04 20:19:26', NULL, 13, 7, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(54, '2015-04-04 20:19:26', NULL, 13, 8, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(55, '2015-04-04 20:20:21', NULL, 13, 9, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(56, '2015-04-04 20:20:21', NULL, 13, 10, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(57, '2015-04-04 20:21:40', NULL, 13, 11, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(58, '2015-04-04 20:21:40', NULL, 13, 12, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(59, '2015-04-09 22:37:30', NULL, 18, 0, '', NULL, 'LILIANA', 'ARTUNDUAGA', 'lilianaartun@gmail.com', '3155800759', 'CLLE 90 # 19 -41 OFICINA 803', NULL, NULL, 15, 510, '26572327', ''),
(60, '2015-04-09 22:37:30', NULL, 18, 0, 'BOGOTA', NULL, 'LILIANA', 'ARTUNDUAGA', 'lilianaartun@gmail.com', '3155800759', 'CLLE 90 # 19 -41 OFICINA 803', NULL, NULL, 15, 510, '26572327', ''),
(61, '2015-04-10 19:54:50', NULL, 13, 13, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(62, '2015-04-10 19:54:50', NULL, 13, 14, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(63, '2015-04-10 19:55:05', NULL, 13, 15, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(64, '2015-04-10 19:55:05', NULL, 13, 16, NULL, NULL, 'demo', 'imaginamos', 'demo@imaginamos.co', NULL, 'carrera 99 # 14 a 61', NULL, NULL, NULL, 3, NULL, NULL),
(65, '2015-04-13 11:03:06', NULL, 10, 0, 'Dirección 1', NULL, 'Luis', 'Salazar', 'luis.salazar@imagina.co', '77777', 'Calle', NULL, NULL, 26, 876, '4242424242', ''),
(66, '2015-04-13 11:08:10', NULL, 10, 0, '', NULL, 'Luis', 'Salazar', 'luis.salazar@imagina.co', '77777', 'Calle', NULL, NULL, 26, 876, '4242424242', ''),
(67, '2015-04-13 11:48:31', NULL, 10, 17, NULL, NULL, 'Luis', 'Salazar', 'luis.salazar@imagina.co', '77777', 'Calle', NULL, NULL, NULL, 3, NULL, NULL),
(68, '2015-04-13 11:48:31', NULL, 10, 18, NULL, NULL, 'Luis', 'Salazar', 'luis.salazar@imagina.co', '77777', 'Calle', NULL, NULL, NULL, 3, NULL, NULL),
(69, '2015-04-14 12:38:14', NULL, 20, 0, '', NULL, 'Jorge', 'Clavijo', 'jorge.clavijo@imaginamos.com', '', 'Calle 93a # 19-39', NULL, NULL, 1, 3, '1130606389', 'Empaque azul'),
(70, '2015-04-14 12:38:14', NULL, 20, 0, 'Casa', NULL, 'Jorge', 'Clavijo', 'jorge.clavijo@imaginamos.com', '', 'Calle 93a # 19-39', NULL, NULL, 1, 3, '1130606389', 'Empaque azul'),
(71, '2015-04-14 12:45:21', NULL, 20, 19, NULL, NULL, 'Jorge', 'Clavijo', 'jorge.clavijo@imaginamos.com', NULL, 'Calle 93a # 19-39', NULL, NULL, NULL, 3, NULL, NULL),
(72, '2015-04-14 12:45:21', NULL, 20, 20, NULL, NULL, 'Jorge', 'Clavijo', 'jorge.clavijo@imaginamos.com', NULL, 'Calle 93a # 19-39', NULL, NULL, NULL, 3, NULL, NULL),
(73, '2015-04-14 12:45:58', NULL, 20, 21, NULL, NULL, 'Jorge', 'Clavijo', 'jorge.clavijo@imaginamos.com', NULL, 'Calle 93a # 19-39', NULL, NULL, NULL, 3, NULL, NULL),
(74, '2015-04-14 12:45:58', NULL, 20, 22, NULL, NULL, 'Jorge', 'Clavijo', 'jorge.clavijo@imaginamos.com', NULL, 'Calle 93a # 19-39', NULL, NULL, NULL, 3, NULL, NULL),
(75, '2015-04-15 10:45:32', NULL, 21, 0, '', NULL, 'Fabian', 'Andres', 'mikaetnil009@gmail.com', '', 'Calle 2', NULL, NULL, 15, 510, '', ''),
(76, '2015-04-15 11:05:04', NULL, 21, 0, '', NULL, 'Fabian', 'Andres', 'fabian.riascos@imagina.co', '', 'Calle 2', NULL, NULL, 15, 510, '', ''),
(77, '2015-04-20 13:33:40', NULL, 25, 0, '', '', 'leyner', 'venté', 'm91levesi@hotmail.com', '4211080', 'CRR 1 A # 48J-77 SUR', '', 'CO', 15, 510, '1064489788', ''),
(78, '2015-04-20 13:52:23', NULL, 25, 0, 'cerros de oriente', '', 'leyner', 'venté', 'm91levesi@hotmail.com', '4211080', 'CRR 1 A # 48J-77 SUR', '', 'CO', 15, 510, '1064489788', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_attributes`
--

CREATE TABLE IF NOT EXISTS `default_firesale_attributes` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `default_firesale_attributes`
--

INSERT INTO `default_firesale_attributes` (`id`, `title`) VALUES
(1, 'Color'),
(2, 'Size'),
(3, 'Scents'),
(4, 'Heel Height'),
(5, 'Upper Material'),
(6, 'valor 1'),
(7, 'talla_prueba'),
(8, 'tamano'),
(9, 'alcancia pequeña'),
(10, 'Estanpado'),
(11, 'Talla'),
(12, 'heel2'),
(13, 'heel'),
(14, 'alcancia2'),
(15, 'ver'),
(16, 'Colores'),
(17, 'prueba'),
(18, 'Tallas:'),
(19, 'Talla:'),
(20, 'Prueba1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_attributes_assignments`
--

CREATE TABLE IF NOT EXISTS `default_firesale_attributes_assignments` (
  `ordering_count` int(3) NOT NULL DEFAULT '0',
  `stream_id` int(6) NOT NULL,
  `row_id` int(6) NOT NULL,
  `attribute_id` int(6) NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  KEY `stream_id` (`stream_id`,`row_id`,`attribute_id`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `default_firesale_attributes_assignments`
--

INSERT INTO `default_firesale_attributes_assignments` (`ordering_count`, `stream_id`, `row_id`, `attribute_id`, `value`) VALUES
(0, 3, 13, 1, 'Red'),
(0, 3, 14, 1, 'Blue'),
(0, 3, 15, 2, '4'),
(0, 3, 16, 2, '5'),
(0, 3, 17, 2, '6'),
(0, 3, 18, 2, '7'),
(0, 3, 19, 2, '8'),
(0, 3, 20, 2, '9'),
(0, 3, 21, 1, 'Red'),
(0, 3, 22, 1, 'Green'),
(0, 3, 23, 1, 'Magenta'),
(0, 3, 24, 1, 'Purple'),
(0, 3, 25, 2, '4'),
(0, 3, 25, 1, 'Red'),
(0, 3, 26, 2, '4'),
(0, 3, 26, 1, 'Green'),
(0, 3, 27, 2, '4'),
(0, 3, 27, 1, 'Magenta'),
(0, 3, 28, 2, '4'),
(0, 3, 28, 1, 'Purple'),
(0, 3, 29, 2, '5'),
(0, 3, 29, 1, 'Red'),
(0, 3, 30, 2, '5'),
(0, 3, 30, 1, 'Green'),
(0, 3, 31, 2, '5'),
(0, 3, 31, 1, 'Magenta'),
(0, 3, 32, 2, '5'),
(0, 3, 32, 1, 'Purple'),
(0, 3, 33, 2, '6'),
(0, 3, 33, 1, 'Red'),
(0, 3, 34, 2, '6'),
(0, 3, 34, 1, 'Green'),
(0, 3, 35, 2, '6'),
(0, 3, 35, 1, 'Magenta'),
(0, 3, 36, 2, '6'),
(0, 3, 36, 1, 'Purple'),
(0, 3, 37, 2, '7'),
(0, 3, 37, 1, 'Red'),
(0, 3, 38, 2, '7'),
(0, 3, 38, 1, 'Green'),
(0, 3, 39, 2, '7'),
(0, 3, 39, 1, 'Magenta'),
(0, 3, 40, 2, '7'),
(0, 3, 40, 1, 'Purple'),
(0, 3, 41, 2, '8'),
(0, 3, 41, 1, 'Red'),
(0, 3, 42, 2, '8'),
(0, 3, 42, 1, 'Green'),
(0, 3, 43, 2, '8'),
(0, 3, 43, 1, 'Magenta'),
(0, 3, 44, 2, '8'),
(0, 3, 44, 1, 'Purple'),
(0, 3, 45, 2, '9'),
(0, 3, 45, 1, 'Red'),
(0, 3, 46, 2, '9'),
(0, 3, 46, 1, 'Green'),
(0, 3, 47, 2, '9'),
(0, 3, 47, 1, 'Magenta'),
(0, 3, 48, 2, '9'),
(0, 3, 48, 1, 'Purple'),
(0, 3, 1, 1, 'rojo'),
(0, 3, 2, 4, 'prueba'),
(0, 3, 2, 5, 'Patent Leathef'),
(0, 3, 49, 6, 'talla 38'),
(0, 3, 50, 6, 'talla 37'),
(0, 3, 51, 6, 'de 0  a 10'),
(0, 3, 52, 6, 'de 11 a 20'),
(0, 3, 53, 6, 'de 11 a 20'),
(0, 3, 54, 6, 'rojo'),
(0, 3, 87, 9, 'Alcancia pequeña'),
(0, 3, 88, 9, 'alcancia pque'),
(0, 3, 90, 8, 'S'),
(0, 3, 91, 8, 'S'),
(0, 3, 91, 10, 'Esqueleto'),
(0, 3, 92, 8, 'S'),
(0, 3, 92, 10, 'Esqueleto'),
(0, 3, 92, 1, 'Rojo'),
(0, 3, 94, 1, 'Rojo'),
(0, 3, 95, 1, 'Amarillo'),
(0, 3, 96, 1, 'Verde'),
(0, 3, 99, 1, 'Rojo'),
(0, 3, 100, 1, 'azul'),
(0, 3, 101, 1, 'Rojo'),
(0, 3, 101, 11, 'XS'),
(0, 3, 102, 1, 'azul'),
(0, 3, 102, 11, 'XS'),
(0, 3, 103, 1, 'Rojo'),
(0, 3, 103, 11, 'S'),
(0, 3, 104, 1, 'azul'),
(0, 3, 104, 11, 'S'),
(0, 3, 105, 11, 'xs'),
(0, 3, 106, 11, 's'),
(0, 3, 107, 11, 'm'),
(0, 3, 108, 11, 'l'),
(0, 3, 109, 11, 'xs'),
(0, 3, 109, 1, 'Rojo'),
(0, 3, 110, 11, 's'),
(0, 3, 110, 1, 'Rojo'),
(0, 3, 111, 11, 'm'),
(0, 3, 111, 1, 'Rojo'),
(0, 3, 112, 11, 'l'),
(0, 3, 112, 1, 'Rojo'),
(0, 3, 113, 11, 'xs'),
(0, 3, 113, 1, 'Rojo'),
(0, 3, 114, 11, 's'),
(0, 3, 114, 1, 'Rojo'),
(0, 3, 115, 11, 'm'),
(0, 3, 115, 1, 'Rojo'),
(0, 3, 116, 11, 'l'),
(0, 3, 116, 1, 'Rojo'),
(0, 3, 117, 11, 'xs'),
(0, 3, 117, 1, 'Verde'),
(0, 3, 118, 11, 's'),
(0, 3, 118, 1, 'Verde'),
(0, 3, 119, 11, 'm'),
(0, 3, 119, 1, 'Verde'),
(0, 3, 120, 11, 'l'),
(0, 3, 120, 1, 'Verde'),
(0, 3, 137, 16, 'Rojo'),
(0, 3, 138, 16, 'Rojo'),
(0, 3, 139, 16, 'Rojo'),
(0, 3, 140, 16, 'Rojo'),
(0, 3, 362, 18, 'S'),
(0, 3, 363, 18, 'M'),
(0, 3, 364, 18, 'L'),
(0, 3, 365, 18, 'XL'),
(0, 3, 366, 19, 'S'),
(0, 3, 367, 19, 'M'),
(0, 3, 368, 19, 'L'),
(0, 3, 369, 19, 'XL'),
(0, 3, 370, 19, 'S'),
(0, 3, 371, 19, 'S'),
(0, 3, 372, 19, 'S'),
(0, 3, 373, 19, 'm'),
(0, 3, 374, 19, 'M'),
(0, 3, 375, 19, 'L'),
(0, 3, 376, 19, 'M'),
(0, 3, 377, 19, 'L'),
(0, 3, 378, 19, 'XL'),
(0, 3, 379, 19, 'S'),
(0, 3, 380, 19, 'M'),
(0, 3, 381, 19, 'L'),
(0, 3, 382, 19, 'XL'),
(0, 3, 383, 19, 'S'),
(0, 3, 384, 19, 'M'),
(0, 3, 385, 19, 'L'),
(0, 3, 386, 19, 'XL'),
(0, 3, 387, 19, 'S'),
(0, 3, 388, 19, 'S'),
(0, 3, 389, 19, 'M'),
(0, 3, 390, 19, 'L'),
(0, 3, 391, 19, 'XL'),
(0, 3, 392, 19, 'L'),
(0, 3, 393, 19, 'XL'),
(0, 3, 394, 19, 'S'),
(0, 3, 395, 19, 'S'),
(0, 3, 396, 19, 'M'),
(0, 3, 397, 19, 'L'),
(0, 3, 398, 19, 'S'),
(0, 3, 399, 19, 'M'),
(0, 3, 400, 19, 'L'),
(0, 3, 401, 11, 'S'),
(0, 3, 402, 11, 'M'),
(0, 3, 403, 11, 'L'),
(0, 3, 404, 11, 'S'),
(0, 3, 405, 11, 'S'),
(0, 3, 406, 11, 'M'),
(0, 3, 407, 11, 'L'),
(0, 3, 408, 11, 'XL'),
(0, 3, 409, 19, 'S'),
(0, 3, 410, 19, 'M'),
(0, 3, 411, 19, 'M'),
(0, 3, 412, 19, 'L'),
(0, 3, 413, 19, 'XL'),
(0, 3, 414, 19, 'S'),
(0, 3, 415, 19, 'M'),
(0, 3, 416, 19, 'L'),
(0, 3, 417, 19, 'XL'),
(0, 3, 418, 19, 'S'),
(0, 3, 419, 19, 'M'),
(0, 3, 420, 19, 'L'),
(0, 3, 421, 19, 'XL'),
(0, 3, 422, 11, 'S'),
(0, 3, 423, 11, 'M'),
(0, 3, 424, 11, 'L'),
(0, 3, 425, 11, 'XL'),
(0, 3, 426, 19, 'S'),
(0, 3, 427, 19, 'M'),
(0, 3, 428, 19, 'L'),
(0, 3, 429, 19, 'XL'),
(0, 3, 430, 19, 'XS'),
(0, 3, 431, 19, 'XS'),
(0, 3, 432, 19, 'S'),
(0, 3, 433, 19, 'M'),
(0, 3, 434, 19, 'L'),
(0, 3, 435, 19, 'XL'),
(0, 3, 436, 19, '2T'),
(0, 3, 437, 19, '2XS'),
(0, 3, 438, 19, '3T'),
(0, 3, 439, 19, '4T'),
(0, 3, 440, 19, '5T'),
(0, 3, 441, 19, 'S'),
(0, 3, 442, 19, 'M'),
(0, 3, 443, 19, 'L'),
(0, 3, 444, 19, 'XL'),
(0, 3, 445, 19, 'S'),
(0, 3, 446, 19, 'M'),
(0, 3, 447, 19, 'L'),
(0, 3, 448, 19, 'XL'),
(0, 3, 449, 19, 'S'),
(0, 3, 450, 19, 'M'),
(0, 3, 451, 19, 'L'),
(0, 3, 452, 19, 'XL'),
(0, 3, 453, 18, 'S'),
(0, 3, 454, 18, 'M'),
(0, 3, 455, 18, 'L'),
(0, 3, 456, 18, 'XL'),
(0, 3, 457, 19, 'S'),
(0, 3, 458, 19, 'M'),
(0, 3, 459, 19, 'L'),
(0, 3, 460, 19, 'L'),
(0, 3, 461, 19, 'S'),
(0, 3, 462, 19, 'M'),
(0, 3, 463, 19, 'S'),
(0, 3, 464, 19, 'L'),
(0, 3, 465, 19, 'XL'),
(0, 3, 466, 19, 'S'),
(0, 3, 467, 19, 'M'),
(0, 3, 468, 19, 'L'),
(0, 3, 469, 19, 'XL'),
(0, 3, 470, 19, 'S'),
(0, 3, 471, 19, 'M'),
(0, 3, 472, 19, 'L'),
(0, 3, 473, 19, 'XL'),
(0, 3, 474, 19, 'S'),
(0, 3, 475, 19, 'M'),
(0, 3, 476, 19, 'L'),
(0, 3, 477, 19, 'XL'),
(0, 3, 478, 19, 'S'),
(0, 3, 479, 19, 'M'),
(0, 3, 480, 19, 'L'),
(0, 3, 481, 19, 'XL'),
(0, 3, 482, 19, 'S'),
(0, 3, 483, 19, 'M'),
(0, 3, 484, 19, 'L'),
(0, 3, 485, 19, 'XL'),
(0, 3, 486, 19, 'S'),
(0, 3, 487, 19, 'M'),
(0, 3, 488, 19, 'L'),
(0, 3, 489, 19, 'XL'),
(0, 3, 490, 19, 'S'),
(0, 3, 491, 19, 'M'),
(0, 3, 492, 19, 'L'),
(0, 3, 493, 19, 'XL'),
(0, 3, 494, 19, 'S'),
(0, 3, 495, 19, 'M'),
(0, 3, 496, 19, 'L'),
(0, 3, 497, 19, 'XL'),
(0, 3, 498, 19, 'XS'),
(0, 3, 499, 19, 'S'),
(0, 3, 500, 19, 'M'),
(0, 3, 501, 19, 'L'),
(0, 3, 502, 19, 'XL'),
(0, 3, 503, 19, 'XS'),
(0, 3, 504, 19, 'XS'),
(0, 3, 505, 19, 'S'),
(0, 3, 506, 19, 'M'),
(0, 3, 507, 19, 'L'),
(0, 3, 508, 19, 'XL'),
(0, 3, 509, 19, 'S'),
(0, 3, 510, 19, 'M'),
(0, 3, 511, 19, 'L'),
(0, 3, 512, 19, 'XL'),
(0, 3, 298, 20, 'Prueba'),
(0, 3, 513, 11, 'S'),
(0, 3, 514, 11, 'S'),
(0, 3, 515, 11, 'M'),
(0, 3, 516, 11, 'L'),
(0, 3, 583, 11, 'XS'),
(0, 3, 584, 11, 'S'),
(0, 3, 585, 11, 'M'),
(0, 3, 586, 11, 'L'),
(0, 3, 587, 11, 'XS'),
(0, 3, 588, 11, 'S'),
(0, 3, 589, 11, 'M'),
(0, 3, 590, 11, 'L'),
(0, 3, 591, 11, 'S'),
(0, 3, 592, 11, 'M'),
(0, 3, 593, 11, 'L'),
(0, 3, 594, 11, 'XL'),
(0, 3, 595, 11, 'S'),
(0, 3, 596, 11, 'M'),
(0, 3, 597, 11, 'L'),
(0, 3, 598, 11, 'XL'),
(0, 3, 599, 11, 'S'),
(0, 3, 600, 11, 'M'),
(0, 3, 601, 11, 'L'),
(0, 3, 602, 11, 'XL'),
(0, 3, 603, 11, 'XS'),
(0, 3, 604, 11, 'S'),
(0, 3, 605, 11, 'M'),
(0, 3, 606, 11, 'L'),
(0, 3, 607, 11, 'S'),
(0, 3, 608, 11, 'M'),
(0, 3, 609, 11, 'L'),
(0, 3, 610, 11, 'XL'),
(0, 3, 611, 11, 'S'),
(0, 3, 612, 11, 'M'),
(0, 3, 613, 11, 'L'),
(0, 3, 614, 11, 'XL'),
(0, 3, 615, 11, 'S'),
(0, 3, 616, 11, 'M'),
(0, 3, 617, 11, 'L'),
(0, 3, 618, 11, 'XL'),
(0, 3, 619, 11, 'XS'),
(0, 3, 620, 11, 'S'),
(0, 3, 621, 11, 'M'),
(0, 3, 622, 11, 'L'),
(0, 3, 623, 11, 'S'),
(0, 3, 624, 11, 'M'),
(0, 3, 625, 11, 'XS'),
(0, 3, 626, 11, 'L'),
(0, 3, 627, 11, 'XS'),
(0, 3, 628, 11, 'S'),
(0, 3, 629, 11, 'M'),
(0, 3, 630, 11, 'L'),
(0, 3, 631, 11, 'S'),
(0, 3, 632, 11, 'M'),
(0, 3, 633, 11, 'L'),
(0, 3, 634, 11, 'XL'),
(0, 3, 635, 11, 'S'),
(0, 3, 636, 11, 'M'),
(0, 3, 637, 11, 'L'),
(0, 3, 638, 11, 'XL'),
(0, 3, 639, 11, 'S'),
(0, 3, 640, 11, 'M'),
(0, 3, 641, 11, 'L'),
(0, 3, 642, 11, 'XL'),
(0, 3, 643, 11, 'S'),
(0, 3, 644, 11, 'M'),
(0, 3, 645, 11, 'L'),
(0, 3, 646, 11, 'XL'),
(0, 3, 647, 11, 'S'),
(0, 3, 648, 11, 'M'),
(0, 3, 649, 11, 'L'),
(0, 3, 650, 11, 'XL'),
(0, 3, 651, 11, 'S'),
(0, 3, 652, 11, 'M'),
(0, 3, 653, 11, 'L'),
(0, 3, 654, 11, 'XL'),
(0, 3, 655, 11, 'S'),
(0, 3, 656, 11, 'M'),
(0, 3, 657, 11, 'S'),
(0, 3, 658, 11, 'S'),
(0, 3, 659, 11, 'M'),
(0, 3, 660, 11, 'L'),
(0, 3, 661, 11, 'XL'),
(0, 3, 662, 11, 'S'),
(0, 3, 663, 11, 'M'),
(0, 3, 664, 11, 'L'),
(0, 3, 665, 11, 'XL'),
(0, 3, 666, 11, 'S'),
(0, 3, 667, 11, 'M'),
(0, 3, 668, 11, 'L'),
(0, 3, 669, 11, 'XL'),
(0, 3, 670, 11, 'S'),
(0, 3, 671, 11, 'M'),
(0, 3, 672, 11, 'L'),
(0, 3, 673, 11, 'XL'),
(0, 3, 674, 11, '18/24'),
(0, 3, 675, 11, '2/3'),
(0, 3, 676, 11, '3/4'),
(0, 3, 677, 11, '4/5'),
(0, 3, 678, 11, '5/6'),
(0, 3, 679, 11, 'XS'),
(0, 3, 680, 11, 'S'),
(0, 3, 681, 11, 'M'),
(0, 3, 682, 11, 'L'),
(0, 3, 683, 11, 'S'),
(0, 3, 684, 11, 'M'),
(0, 3, 685, 11, 'L'),
(0, 3, 686, 11, 'S'),
(0, 3, 687, 11, 'M'),
(0, 3, 688, 11, 'L'),
(0, 3, 689, 11, 'XL'),
(0, 3, 690, 11, 'S'),
(0, 3, 691, 11, 'M'),
(0, 3, 692, 11, 'L'),
(0, 3, 693, 11, 'XL'),
(0, 3, 694, 11, 'XS'),
(0, 3, 695, 11, 'S'),
(0, 3, 696, 11, 'M'),
(0, 3, 697, 11, 'L'),
(0, 3, 698, 11, 'S'),
(0, 3, 699, 11, 'M'),
(0, 3, 700, 11, 'L'),
(0, 3, 701, 11, 'XL'),
(0, 3, 702, 11, 'S'),
(0, 3, 703, 11, 'M'),
(0, 3, 704, 11, 'L'),
(0, 3, 705, 11, 'XL'),
(0, 3, 706, 11, 'M'),
(0, 3, 707, 11, 'L'),
(0, 3, 708, 11, 'XL'),
(0, 3, 709, 11, 'S'),
(0, 3, 710, 11, 'L'),
(0, 3, 711, 11, 'XL'),
(0, 3, 712, 11, 'M'),
(0, 3, 713, 11, 'L'),
(0, 3, 714, 11, 'XL'),
(0, 3, 718, 11, 'XS'),
(0, 3, 719, 11, 'S'),
(0, 3, 720, 11, 'M'),
(0, 3, 721, 11, 'L'),
(0, 3, 722, 11, 'XL'),
(0, 3, 723, 11, 'S'),
(0, 3, 724, 11, 'M'),
(0, 3, 725, 11, 'L'),
(0, 3, 726, 11, 'XL'),
(0, 3, 727, 11, 'TU'),
(0, 3, 728, 11, 'TU'),
(0, 3, 729, 11, 'TU'),
(0, 3, 730, 11, 'S'),
(0, 3, 731, 11, 'M'),
(0, 3, 732, 11, 'L'),
(0, 3, 733, 11, 'XL'),
(0, 3, 735, 11, '2/4'),
(0, 3, 736, 11, '6/8'),
(0, 3, 737, 11, '10/12'),
(0, 3, 738, 11, '10/16'),
(0, 3, 739, 11, 'S'),
(0, 3, 740, 11, 'M'),
(0, 3, 741, 11, 'L'),
(0, 3, 742, 11, 'XL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_brands`
--

CREATE TABLE IF NOT EXISTS `default_firesale_brands` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `description` longtext COLLATE utf8_unicode_ci,
  `featured` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `default_firesale_brands`
--

INSERT INTO `default_firesale_brands` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `title`, `slug`, `status`, `description`, `featured`) VALUES
(5, '2014-11-12 16:34:46', '2015-01-21 14:57:29', 10, 5, 'Adidas', 'adidas', '1', NULL, '1'),
(6, '2015-01-21 14:57:13', NULL, 10, 6, 'MFC', 'mfc', '1', NULL, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_categories`
--

CREATE TABLE IF NOT EXISTS `default_firesale_categories` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8_unicode_ci,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `default_firesale_categories`
--

INSERT INTO `default_firesale_categories` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `parent`, `status`, `title`, `slug`, `description`, `meta_title`, `meta_description`, `meta_keywords`, `featured`) VALUES
(1, '2013-07-16 02:01:10', '2015-01-27 10:44:34', NULL, 0, 0, '1', 'Mujer', 'mujer', NULL, NULL, '', NULL, '1'),
(2, '2013-07-18 04:19:49', '2015-04-13 17:08:26', 1, 3, 0, '0', 'Bebes', 'bebes', NULL, NULL, '', NULL, '1'),
(3, '2013-07-18 04:20:29', '2015-04-14 17:37:04', 1, 1, 0, '1', 'Hombre', 'hombre', NULL, NULL, '', NULL, '1'),
(4, '2013-07-18 18:36:33', '2015-01-19 20:15:00', 2, 2, 0, '1', 'Niños', 'ninos', 'Our handpicked cosmetics just for you!', 'modelos armados bogota', 'cosmeticos', NULL, '1'),
(5, '2014-01-30 22:23:56', '2015-04-13 17:07:02', 1, 0, 2, '0', 'Adidas', 'bebes/adidas.bebes', NULL, NULL, '', NULL, '1'),
(6, '2014-11-19 18:48:26', '2015-01-27 10:46:00', 10, 4, 1, '1', 'Adidas', 'mujer/adidas', NULL, NULL, '', NULL, NULL),
(7, '2014-11-19 18:48:52', '2015-01-27 10:46:13', 10, 5, 1, '1', 'MFC', 'mujer/mfc', NULL, NULL, '', NULL, NULL),
(10, '2014-11-23 16:06:26', '2015-01-27 10:57:00', 10, 7, 3, '1', 'Adidas', 'hombre/adidas-hombres', NULL, NULL, '', NULL, NULL),
(11, '2014-11-23 16:07:20', '2015-01-27 10:58:57', 10, 8, 4, '1', 'Adidas', 'ninos/adidas-ninos', NULL, NULL, '', NULL, NULL),
(12, '2015-01-27 10:45:33', NULL, 10, 9, 0, '1', 'Hogar', 'hogar', NULL, NULL, NULL, NULL, NULL),
(13, '2015-01-27 10:58:40', '2015-01-27 16:06:45', 10, 10, 3, '1', 'MFC', 'hombre/mfc-hombre', NULL, NULL, '', NULL, NULL),
(14, '2015-01-27 10:59:11', NULL, 10, 11, 4, '1', 'MFC', 'ninos/mfc-ninos', NULL, NULL, NULL, NULL, NULL),
(16, '2015-01-27 11:00:05', NULL, 10, 13, 12, '1', 'MFC', 'hogar/mfc-hogar', NULL, NULL, NULL, NULL, NULL),
(17, '2015-04-13 17:05:42', NULL, 13, 14, 2, '0', 'bebes', 'bebes/bebes', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_city`
--

CREATE TABLE IF NOT EXISTS `default_firesale_city` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `code` longtext COLLATE utf8_unicode_ci,
  `name` longtext COLLATE utf8_unicode_ci,
  `deparment` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1112 ;

--
-- Volcado de datos para la tabla `default_firesale_city`
--

INSERT INTO `default_firesale_city` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `code`, `name`, `deparment`) VALUES
(3, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LETICIA', 1),
(4, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'APARTADO', 2),
(5, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '11SIA', 2),
(6, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MEDELLIN', 2),
(7, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIONEGRO (ANT)', 2),
(8, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ITAGUI', 2),
(9, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TURBO', 2),
(10, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ENVIGADO', 2),
(11, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELLO', 2),
(12, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO BERRIO', 2),
(13, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIGORODO', 2),
(14, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CALDAS', 2),
(15, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAREPA', 2),
(16, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CARMEN DE VIBORAL', 2),
(17, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COPACABANA', 2),
(18, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GIRARDOTA', 2),
(19, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA CEJA', 2),
(20, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA UNION', 2),
(21, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MARINILLA', 2),
(22, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL SANTUARIO', 2),
(23, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ROSA DE OSOS', 2),
(24, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YARUMAL', 2),
(25, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ZARAGOZA', 2),
(26, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NECHI', 2),
(27, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO TRIUNFO', 2),
(28, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AMAGA', 2),
(29, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CIUDAD 6', 2),
(30, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TARAZA', 2),
(31, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HELICONIA', 2),
(32, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DORADAL', 2),
(33, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN LUIS', 2),
(34, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUARNE', 2),
(35, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ABEJORRAL', 2),
(36, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO NARE', 2),
(37, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ENTRERRIOS', 2),
(38, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'URRAO', 2),
(39, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA ESTRELLA', 2),
(40, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CISNEROS', 2),
(41, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EBEJICO', 2),
(42, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SABANETA', 2),
(43, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JUAN DE URABA', 2),
(44, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALEJANDRIA', 2),
(45, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANDES', 2),
(46, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARBOLETES', 2),
(47, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARBOSA', 2),
(48, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TAMESIS', 2),
(49, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BETULIA (ANT)', 2),
(50, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAROLINA', 2),
(51, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CONCEPCION', 2),
(52, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DON MATIAS', 2),
(53, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUATAPE', 2),
(54, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'JARDIN', 2),
(55, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'JERICO', 2),
(56, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MACEO', 2),
(57, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'REMEDIOS', 2),
(58, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SALGAR', 2),
(59, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN CARLOS', 2),
(60, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JERONIMO', 2),
(61, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN PEDRO DE URABA', 2),
(62, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN VICENTE', 2),
(63, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SONSON', 2),
(64, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VALPARAISO', 2),
(65, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANORI', 2),
(66, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AMALFI', 2),
(67, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VENECIA', 2),
(68, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PEÑOL', 2),
(69, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAICEDO', 2),
(70, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAMPAMENTO', 2),
(71, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FREDONIA', 2),
(72, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA BARBARA', 2),
(73, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RETIRO', 2),
(74, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '22', 2),
(75, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NECOCLI', 2),
(76, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN RAFAEL', 2),
(77, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DABEIBA', 2),
(78, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA SIERRA', 2),
(79, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MONTEBELLO', 2),
(80, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COCORNA', 2),
(81, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SEGOVIA', 2),
(82, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'URAMITA', 2),
(83, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YOLOMBO', 2),
(84, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FRONTINO', 2),
(85, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MUTATA', 2),
(86, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CURRULAO', 2),
(87, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTAFE DE 2', 2),
(88, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANTONIO DE PRADO', 2),
(89, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANGOSTURA', 2),
(90, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YALI', 2),
(91, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VEGACHI', 2),
(92, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GOMEZ PLATA', 2),
(93, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELMIRA', 2),
(94, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VALDIVIA', 2),
(95, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CACERES', 2),
(96, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAÑASGORDAS', 2),
(97, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TITIRIBI', 2),
(98, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HISPANIA', 2),
(99, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PINTADA', 2),
(100, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CARAMANTA', 2),
(101, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TARSO', 2),
(102, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUEBLORRICO', 2),
(103, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CONCORDIA', 2),
(104, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GRANADA', 2),
(105, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARGELIA (A)', 2),
(106, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALERMO', 2),
(107, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ABRIAQUI', 2),
(108, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANGELOPOLIS', 2),
(109, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANZA', 2),
(110, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARMENIA', 2),
(111, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BETANIA', 2),
(112, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BURITICA', 2),
(113, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CARACOLI', 2),
(114, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CASABE - YONDO', 2),
(115, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL BAGRE', 2),
(116, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL 30', 2),
(117, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GIRALDO', 2),
(118, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUADALUPE', 2),
(119, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ITUANGO', 2),
(120, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LIBORINA', 2),
(121, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MURINDO', 2),
(122, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OLAYA', 2),
(123, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PEQUE', 2),
(124, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO CLAVER', 2),
(125, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO PERALES NUEVO', 2),
(126, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SABANALARGA', 2),
(127, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANDRES DE CUERQUIA', 2),
(128, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN CRISTOBAL', 2),
(129, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN FRANCISCO', 2),
(130, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JOSE DE LA MONTAÑA', 2),
(131, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN PEDRO DE LOS MILAGROS', 2),
(132, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ROQUE', 2),
(133, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTO DOMINGO', 2),
(134, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOPETRAN', 2),
(135, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOLEDO', 2),
(136, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VIGIA DEL FUERTE', 2),
(137, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '3', 3),
(138, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SARAVENA', 3),
(139, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TAME', 3),
(140, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARAUQUITA', 3),
(141, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CRAVO NORTE', 3),
(142, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FORTUL', 3),
(143, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO RONDON', 3),
(144, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANDRES', 4),
(145, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PROVIDENCIA', 4),
(146, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARRANQUILLA', 5),
(147, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LURUACO', 5),
(148, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SABANALARGA', 5),
(149, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOLEDAD', 5),
(150, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARANOA', 5),
(151, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GALAPA', 5),
(152, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SABANAGRANDE', 5),
(153, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTO TOMAS', 5),
(154, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MALAMBO', 5),
(155, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO COLOMBIA', 5),
(156, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAMPO DE LA CRUZ', 5),
(157, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAMPECHE', 5),
(158, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUAN', 5),
(159, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MANATI', 5),
(160, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'JUAN DE ACOSTA', 5),
(161, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'USIACURI', 5),
(162, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TUBARA', 5),
(163, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'JUAN MINA', 5),
(164, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'POLONUEVO', 5),
(165, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALMAR DE VARELA', 5),
(166, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CANDELARIA', 5),
(167, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PONEDERA', 5),
(168, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO GIRALDO', 5),
(169, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'REPELON', 5),
(170, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CARTAGENA', 6),
(171, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MOMPOS', 6),
(172, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MAGANGUE', 6),
(173, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CARMEN DE 6', 6),
(174, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JACINTO', 6),
(175, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN PABLO', 6),
(176, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CALAMAR', 6),
(177, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ANA (B)', 6),
(178, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JUAN NEPOMUCENO', 6),
(179, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YATI', 6),
(180, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ACHI', 6),
(181, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ESTANISLAO', 6),
(182, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALTOS DEL ROSARIO', 6),
(183, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARENAL', 6),
(184, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CICUCO', 6),
(185, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PINILLOS', 6),
(186, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SIMITI', 6),
(187, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TURBANA', 6),
(188, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MORALES (B)', 6),
(189, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BAYUNCA', 6),
(190, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARJONA B', 6),
(191, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TURBACO B', 6),
(192, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ZAMBRANO', 6),
(193, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN MARTIN DE LOBA', 6),
(194, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOPLAVIENTO B', 6),
(195, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA CATALINA', 6),
(196, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CLEMENCIA B', 6),
(197, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MARIA LA BAJA B', 6),
(198, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MAHATES', 6),
(199, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SINCERIN', 6),
(200, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GAMBOTE', 6),
(201, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MALAGANA', 6),
(202, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ROSA DEL SUR', 6),
(203, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARRANCO DE LOBA', 6),
(204, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CANTAGALLO', 6),
(205, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CASCAJAL', 6),
(206, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL GUAMO (B)', 6),
(207, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL PEÑON (BOL)', 6),
(208, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HATILLO DE LOBA', 6),
(209, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MARGARITA', 6),
(210, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PASACABALLOS', 6),
(211, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUNTA DE CARTAGENA', 6),
(212, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIO VIEJO', 6),
(213, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN FERNANDO', 6),
(214, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JACINTO DEL 11', 6),
(215, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN MARTIN', 6),
(216, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ROSA DE LIMA', 6),
(217, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TALAIGUA NUEVO', 6),
(218, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIQUISIO NUEVO', 6),
(219, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLANUEVA', 6),
(220, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIQUINQUIRA', 7),
(221, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DUITAMA', 7),
(222, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOGAMOSO', 7),
(223, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAIPA', 7),
(224, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TUNJA', 7),
(225, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO 7', 7),
(226, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ROSA DE VITERBO', 7),
(227, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA VICTORIA (B)', 7),
(228, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MONIQUIRA', 7),
(229, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RAMIRIQUI', 7),
(230, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIBASOSA', 7),
(231, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTANA', 7),
(232, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAJARITO', 7),
(233, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'JENESANO', 7),
(234, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AQUITANIA', 7),
(235, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARCABUCO', 7),
(236, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELENCITO', 7),
(237, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CERINZA', 7),
(238, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CORRALES', 7),
(239, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHISCAS', 7),
(240, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL ESPINO', 7),
(241, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FLORESTA', 7),
(242, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GAMEZA', 7),
(243, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GARAGOA', 7),
(244, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAYATA', 7),
(245, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUICAN', 7),
(246, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MACANAL', 7),
(247, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MIRAFLORES', 7),
(248, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MONGUA', 7),
(249, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NUEVO COLON', 7),
(250, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAZ DE RIO', 7),
(251, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RAQUIRA', 7),
(252, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SABOYA', 7),
(253, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAMACA', 7),
(254, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN LUIS DE GACENO', 7),
(255, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN MATEO', 7),
(256, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA MARIA', 7),
(257, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA SOFIA', 7),
(258, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SATIVANORTE', 7),
(259, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOCOTA', 7),
(260, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOCHA', 7),
(261, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOTAQUIRA', 7),
(262, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUTAMARCHAN', 7),
(263, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TASCO', 7),
(264, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TENZA', 7),
(265, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIBANA', 7),
(266, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TURMEQUE', 7),
(267, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TUTA', 7),
(268, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VENTAQUEMADA', 7),
(269, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLA DE LEYVA', 7),
(270, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELEN', 7),
(271, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'IZA', 7),
(272, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MONGUI', 7),
(273, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOPAGA', 7),
(274, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL COCUY', 7),
(275, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOCA', 7),
(276, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NOBSA', 7),
(277, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUATEQUE', 7),
(278, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA CAPILLA', 7),
(279, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PACHAVITA', 7),
(280, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUTATENZA', 7),
(281, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHINAVITA', 7),
(282, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOATA', 7),
(283, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MUZO', 7),
(284, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OTANCHE', 7),
(285, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAEZ', 7),
(286, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SACHICA', 7),
(287, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JOSE DE PARE', 7),
(288, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '7', 7),
(289, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PESCA', 7),
(290, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BOAVITA', 7),
(291, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALMEIDA', 7),
(292, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BERBEO', 7),
(293, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BETEITIVA', 7),
(294, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BRICEÑO (B)', 7),
(295, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUENAVISTA', 7),
(296, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUSBANZA', 7),
(297, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '8', 7),
(298, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAMPOHERMOSO', 7),
(299, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIQUIZA', 7),
(300, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHITA', 7),
(301, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHITARAQUE', 7),
(302, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIVATA', 7),
(303, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIVOR', 7),
(304, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CIENEGA', 7),
(305, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COMBITA', 7),
(306, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COPER', 7),
(307, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COVARACHIA', 7),
(308, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUBARA', 7),
(309, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUCAITA', 7),
(310, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUITIVA', 7),
(311, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FIRAVITOBA', 7),
(312, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GACHANTIVA', 7),
(313, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUACAMAYAS', 7),
(314, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA UVITA', 7),
(315, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LABRANZAGRANDE', 7),
(316, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MARIPI', 7),
(317, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MOTAVITA', 7),
(318, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OICATA', 7),
(319, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PANQUEBA', 7),
(320, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAUNA', 7),
(321, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'QUIPAMA', 7),
(322, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RONDON', 7),
(323, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN EDUARDO', 7),
(324, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN MIGUEL DE SEMA', 7),
(325, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN PABLO DE BORBUR', 7),
(326, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SATIVASUR', 7),
(327, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SIACHOQUE', 7),
(328, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOMONDOCO', 7),
(329, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SORA', 7),
(330, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SORACA', 7),
(331, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUSACON', 7),
(332, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TINJACA', 7),
(333, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIPACOQUE', 7),
(334, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOGÜI', 7),
(335, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOTA', 7),
(336, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TRINIDAD', 7),
(337, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TUTAZA', 7),
(338, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'UMBITA', 7),
(339, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VIRACACHA', 7),
(340, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ZETAQUIRA', 7),
(341, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA DORADA', 8),
(342, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHINCHINA', 8),
(343, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MANIZALES', 8),
(344, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANSERMA', 8),
(345, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VICTORIA', 8),
(346, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MANZANARES', 8),
(347, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PACORA', 8),
(348, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VITERBO', 8),
(349, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUARINOCITO', 8),
(350, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AGUADAS', 8),
(351, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '26', 8),
(352, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLAMARIA', 8),
(353, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SALAMINA', 8),
(354, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NEIRA', 8),
(355, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FILADELFIA', 8),
(356, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA MERCED', 8),
(357, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MARMATO', 8),
(358, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MARQUETALIA', 8),
(359, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALESTINA', 8),
(360, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIOSUCIO', 8),
(361, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUPIA', 8),
(362, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NORCASIA', 8),
(363, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELALCAZAR (CL)', 8),
(364, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PENSILVANIA', 8),
(365, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARANZAZU', 8),
(366, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '3', 8),
(367, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BOLIVIA', 8),
(368, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAMANA', 8),
(369, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAMARIA', 8),
(370, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JOSE (C)', 8),
(371, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FLORENCIA', 9),
(372, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL DONCELLO', 9),
(373, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN VICENTE DEL CAGUAN', 9),
(374, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL PAUJIL', 9),
(375, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CARTAGENA DEL CHAIRA', 9),
(376, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CURILLO', 9),
(377, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO RICO', 9),
(378, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JOSE DEL FRAGUA', 9),
(379, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALBANIA', 9),
(380, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELEN DE LOS ANDAQUIES', 9),
(381, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAMPO HERMOSO', 9),
(382, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA MONTAÑITA', 9),
(383, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MILAN', 9),
(384, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOLANO', 9),
(385, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOLITA', 9),
(386, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VALPARAISO (CAQ)', 9),
(387, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YOPAL', 10),
(388, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TAURAMENA', 10),
(389, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AGUAZUL', 10),
(390, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLANUEVA (C)', 10),
(391, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MANI', 10),
(392, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PORE', 10),
(393, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TRINIDAD', 10),
(394, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MONTERREY', 10),
(395, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN LUIS DE PALENQUE', 10),
(396, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OROCUE', 10),
(397, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAZ DE ARIPORO', 10),
(398, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHAMEZA', 10),
(399, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HATO COROZAL', 10),
(400, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NUNCHIA', 10),
(401, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SABANALARGA', 10),
(402, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SACAMA', 10),
(403, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TAMARA', 10),
(404, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'POPAYAN', 11),
(405, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '27 DE QUILICHAO', 11),
(406, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PIENDAMO', 11),
(407, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO TEJADA', 11),
(408, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL BORDO', 11),
(409, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CORINTO', 11),
(410, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIMBIO', 11),
(411, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SILVIA', 11),
(412, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MIRANDA', 11),
(413, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CALOTO', 11),
(414, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL TAMBO (CA)', 11),
(415, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLA RICA', 11),
(416, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BALBOA (C)', 11),
(417, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAPI', 11),
(418, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAJIBIO', 11),
(419, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MERCADERES', 11),
(420, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ORTIGAL', 11),
(421, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUAREZ', 11),
(422, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARGELIA (C)', 11),
(423, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL ESTRECHO (C)', 11),
(424, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL PLATEADO', 11),
(425, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUACHENE', 11),
(426, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MICAY', 11),
(427, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MORALES (C)', 11),
(428, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PADILLA', 11),
(429, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '28', 11),
(430, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIMBIQUI', 11),
(431, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TUNIA', 11),
(432, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AGUACHICA', 12),
(433, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BOSCONIA', 12),
(434, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CODAZZI', 12),
(435, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ALBERTO', 12),
(436, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAILITAS', 12),
(437, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CURUMANI', 12),
(438, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '30DUPAR', 12),
(439, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIO DE ORO', 12),
(440, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA GLORIA', 12),
(441, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN MARTIN (C)', 12),
(442, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIRIGUANA', 12),
(443, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ASTREA', 12),
(444, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GAMARRA', 12),
(445, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL PASO', 12),
(446, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PAZ', 12),
(447, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIMICHAGUA', 12),
(448, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN DIEGO', 12),
(449, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BECERRIL', 12),
(450, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CASACAZA', 12),
(451, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL COPEY', 12),
(452, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA JAGUA DE IBIRICO', 12),
(453, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PELAYA', 12),
(454, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MANAURE (C)', 12),
(455, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL JUNCAL', 12),
(456, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA LOMA', 12),
(457, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA MATA', 12),
(458, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA SIERRA', 12),
(459, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LAS VEGAS', 12),
(460, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUEBLO BELLO', 12),
(461, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ROQUE', 12),
(462, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TAMALAMEQUE', 12),
(463, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MINA DRUMOND PRIBBENOW', 12),
(464, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CONDOTO', 13),
(465, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'QUIBDO', 13),
(466, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TADO', 13),
(467, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ISTMINA', 13),
(468, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'UNGUIA', 13),
(469, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ACANDI', 13),
(470, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELEN DE BAJIRA', 13),
(471, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CARMEN DE ATRATO', 13),
(472, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BAHIA SOLANO MUTIS', 13),
(473, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PIZARRO', 13),
(474, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIOSUCIO', 13),
(475, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CERETE', 14),
(476, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MONTERIA', 14),
(477, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LORICA', 14),
(478, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PLANETA RICA', 14),
(479, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAHAGUN', 14),
(480, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MONTELIBANO', 14),
(481, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AYAPEL', 14),
(482, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHINU', 14),
(483, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PURISIMA', 14),
(484, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN BERNARDO DEL VIENTO', 14),
(485, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CIENAGA DE ORO', 14),
(486, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANDRES DE SOTAVENTO', 14),
(487, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANTERO', 14),
(488, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN PELAYO', 14),
(489, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIERRALTA', 14),
(490, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VALENCIA', 14),
(491, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CANALETE', 14),
(492, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA APARTADA Y LA FRONTERA', 14),
(493, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MOÑITOS', 14),
(494, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN CARLOS', 14),
(495, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TUCHIN', 14),
(496, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CERROMATOSO', 14),
(497, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MOMIL', 14),
(498, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUEBLO NUEVO', 14),
(499, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUENAVISTA (COR)', 14),
(500, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CARRILLO', 14),
(501, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIMA (C)', 14),
(502, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COTORRA', 14),
(503, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COTORRA (I)', 14),
(504, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL PORVENIR (C)', 14),
(505, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LOS 14S', 14),
(506, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LOS GARZONES', 14),
(507, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO ESCONDIDO', 14),
(508, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO LIBERTADOR', 14),
(509, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN CARLOS', 14),
(510, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BOGOTA', 15),
(511, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GIRARDOT', 15),
(512, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ZIPAQUIRA', 15),
(513, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FUSAGASUGA', 15),
(514, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FACATATIVA', 15),
(515, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLETA', 15),
(516, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'UBATE', 15),
(517, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUADUAS', 15),
(518, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAQUEZA', 15),
(519, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOCAIMA', 15),
(520, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAJICA', 15),
(521, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANAPOIMA', 15),
(522, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'APULO', 15),
(523, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FUNZA', 15),
(524, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TABIO', 15),
(525, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO SALGAR', 15),
(526, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MADRID', 15),
(527, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MOSQUERA', 15),
(528, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUATAVITA', 15),
(529, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SILVANIA', 15),
(530, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOACHA', 15),
(531, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANOLAIMA', 15),
(532, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TENJO', 15),
(533, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CACHIPAY', 15),
(534, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AGUA DE DIOS', 15),
(535, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'UBAQUE', 15),
(536, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PACHO', 15),
(537, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COTA', 15),
(538, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ZIPACON', 15),
(539, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GACHETA', 15),
(540, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN BERNARDO', 15),
(541, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOPO', 15),
(542, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALBAN', 15),
(543, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CABRERA', 15),
(544, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COGUA', 15),
(545, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIPAQUE', 15),
(546, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GACHANCIPA', 15),
(547, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUACHETA', 15),
(548, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA MESA', 15),
(549, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MANTA', 15),
(550, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NEMOCON', 15),
(551, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SESQUILE', 15),
(552, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SIBATE', 15),
(553, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUPATA', 15),
(554, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOCANCIPA', 15),
(555, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'UBALA', 15),
(556, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VERGARA', 15),
(557, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VIANI', 15),
(558, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL COLEGIO (MESITAS)', 15),
(559, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '13NTA', 15),
(560, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA CALERA', 15),
(561, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUASCA', 15),
(562, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARBELAEZ', 15),
(563, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LENGUAZAQUE', 15),
(564, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PARATEBUENO', 15),
(565, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BOJACA', 15),
(566, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHOACHI', 15),
(567, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GRANADA', 15),
(568, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANTONIO DEL TEQUENDAMA', 15),
(569, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN FRANCISCO', 15),
(570, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '27CITO', 15),
(571, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHINAUTA', 15),
(572, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FOMEQUE', 15),
(573, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUESCA', 15),
(574, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MEDINA', 15),
(575, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA FLORIDA', 15),
(576, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA VEGA', 15),
(577, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUBACHOQUE', 15),
(578, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUTATAUSA', 15),
(579, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'QUEBRADANEGRA', 15),
(580, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUSA', 15),
(581, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SASAIMA', 15),
(582, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIBITO', 15),
(583, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BRICEÑO', 15),
(584, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL ROSAL', 15),
(585, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIA', 15),
(586, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NOCAIMA', 15),
(587, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SIMIJACA', 15),
(588, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VIOTA', 15),
(589, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLAPINZON', 15),
(590, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RICAURTE', 15),
(591, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO BOGOTA', 15),
(592, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAPARRAPI', 15),
(593, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUCUNUBA', 15),
(594, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAYABETAL', 15),
(595, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FOSCA', 15),
(596, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAYABAL DE SIQUIMA', 15),
(597, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PALMA', 15),
(598, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MACHETA', 15),
(599, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PANDI', 15),
(600, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'QUETAME', 15),
(601, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JUAN DE RIO SECO', 15),
(602, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOPAIPI', 15),
(603, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'UNE', 15),
(604, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'UTICA', 15),
(605, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YACOPI', 15),
(606, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA GRAN VÍA', 15),
(607, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELTRAN', 15),
(608, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BITUIMA', 15),
(609, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CARMEN DE CARUPA', 15),
(610, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHAGUANI', 15),
(611, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL PEÑON', 15),
(612, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GACHALA', 15),
(613, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GAMA', 15),
(614, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUATAQUI', 15),
(615, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUTIERREZ', 15),
(616, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'JERUSALEN', 15),
(617, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'JUNIN', 15),
(618, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PEÑA', 15),
(619, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA VICTORIA', 15),
(620, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LIMONCITOS', 15),
(621, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '22', 15),
(622, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NILO', 15),
(623, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NIMAIMA', 15),
(624, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAIME', 15),
(625, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PASCA', 15),
(626, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUENTE DE PIEDRA', 15),
(627, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUENTE QUETAME', 15),
(628, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PULI', 15),
(629, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'QUIPILE', 15),
(630, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JOAQUIN (C)', 15),
(631, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUBIA', 15),
(632, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TENA', 15),
(633, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIBACUY', 15),
(634, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VENECIA (C)', 15),
(635, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLAGOMEZ', 15),
(636, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO INIRIDA', 16),
(637, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JOSE DEL 17', 17),
(638, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GARZON', 18),
(639, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAMPOALEGRE', 18),
(640, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NEIVA', 18),
(641, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PITALITO', 18),
(642, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PLATA', 18),
(643, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GIGANTE', 18),
(644, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALGECIRAS', 18),
(645, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AIPE', 18),
(646, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ACEVEDO', 18),
(647, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA ARGENTINA', 18),
(648, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARAYA', 18),
(649, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AGRADO', 18),
(650, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUADALUPE', 18),
(651, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALERMO', 18),
(652, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUAZA', 18),
(653, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TARQUI', 18),
(654, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YAGUARA', 18),
(655, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN AGUSTIN', 18),
(656, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIMANA', 18),
(657, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PACARNI', 18),
(658, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HOBO', 18),
(659, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TELLO', 18),
(660, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TERUEL', 18),
(661, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TESALIA', 18),
(662, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIVERA', 18),
(663, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AIPECITO', 18),
(664, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALTAMIRA', 18),
(665, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELEN (H)', 18),
(666, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BRUSELAS', 18),
(667, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHAPINERO', 18),
(668, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COLOMBIA', 18),
(669, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAGUAN', 18),
(670, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ELIAS', 18),
(671, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUACIRCO', 18),
(672, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAYABAL (HUL)', 18),
(673, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'IQUIRA', 18),
(674, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NATAGA', 18),
(675, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OPORAPA', 18),
(676, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAICOL', 18),
(677, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALESTINA', 18),
(678, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PITAL', 18),
(679, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SALADOBLANCO', 18),
(680, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JOSE DE ISNOS', 18),
(681, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN LUIS', 18),
(682, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA MARIA', 18),
(683, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VEGALARGA', 18),
(684, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLAVIEJA', 18),
(685, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MAICAO', 19),
(686, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIOHACHA', 19),
(687, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JUAN DEL 12', 19),
(688, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARRANCAS', 19),
(689, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FONSECA', 19),
(690, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLANUEVA', 19),
(691, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA MINA', 19),
(692, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DISTRACCION', 19),
(693, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL MOLINO', 19),
(694, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HATONUEVO', 19),
(695, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MANAURE', 19),
(696, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'URUMITA', 19),
(697, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALBANIA', 19),
(698, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'URIBIA', 19),
(699, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MINGUEO', 19),
(700, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DIBULLA', 19),
(701, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PAZ', 19),
(702, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALOMINO', 19),
(703, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PARAGUACHON', 19),
(704, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CIENAGA', 20),
(705, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FUNDACION', 20),
(706, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA MARTA', 20),
(707, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL BANCO', 20),
(708, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PLATO', 20),
(709, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARACATACA', 20),
(710, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL DIFICIL', 20),
(711, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN SEBASTIAN DE BUENAVISTA', 20),
(712, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PIVIJAY', 20),
(713, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAMAL (MAG)', 20),
(714, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SALAMINA', 20),
(715, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TAGANGA', 20),
(716, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GAIRA', 20),
(717, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BURITACA', 20),
(718, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CERRO SAN ANTONIO', 20),
(719, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL RETEN', 20),
(720, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NUEVA GRANADA (M)', 20),
(721, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUACHACA', 20),
(722, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA GRAN VIA', 20),
(723, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALERMO', 20),
(724, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PEDRAZA', 20),
(725, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PIJIÑO', 20),
(726, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SABANAS', 20),
(727, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANGEL', 20),
(728, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ANA (M)', 20),
(729, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA BARBARA DE PINTO', 20),
(730, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TENERIFE', 20),
(731, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GRANADA (M)', 21),
(732, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN MARTIN', 21);
INSERT INTO `default_firesale_city` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `code`, `name`, `deparment`) VALUES
(733, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLAVICENCIO', 21),
(734, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ACACIAS', 21),
(735, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAMAL', 21),
(736, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RESTREPO', 21),
(737, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VISTAHERMOSA', 21),
(738, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MESETAS', 21),
(739, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUMARAL', 21),
(740, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO LOPEZ', 21),
(741, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'APIAY', 21),
(742, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUBARRAL', 21),
(743, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FUENTE DE ORO', 21),
(744, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CASTILLA LA NUEVA', 21),
(745, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO GAITAN', 21),
(746, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO CONCORDIA', 21),
(747, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CABUYARO', 21),
(748, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CASTILLO', 21),
(749, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO RICO', 21),
(750, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALTO POMPEYA', 21),
(751, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARRANCA DE UPIA', 21),
(752, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL DORADO', 21),
(753, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA MACARENA', 21),
(754, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PALMERA', 21),
(755, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LEJANIAS', 21),
(756, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MAPIRIPAN', 21),
(757, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PACHAQUIARO', 21),
(758, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO LLERAS', 21),
(759, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RUBIALES', 21),
(760, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN CARLOS DE GUAROA', 21),
(761, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JUAN DE ARAMA', 21),
(762, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JUAN DEL LOSADA', 21),
(763, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'URIBE', 21),
(764, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'IPIALES', 22),
(765, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PASTO', 22),
(766, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TUMACO', 22),
(767, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TUQUERRES', 22),
(768, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANCUYA', 22),
(769, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARBACOAS', 22),
(770, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUESACO', 22),
(771, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUMBAL', 22),
(772, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA CRUZ', 22),
(773, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA UNION', 22),
(774, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAMANIEGO', 22),
(775, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANDONA', 22),
(776, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TANGUA', 22),
(777, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUPIALES', 22),
(778, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALDANA', 22),
(779, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CATAMBUCO', 22),
(780, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHACHAGÜI', 22),
(781, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CONSACA', 22),
(782, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '14 N', 22),
(783, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUALMATÁN', 22),
(784, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA FLORIDA (N)', 22),
(785, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA LLANADA', 22),
(786, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERRES', 22),
(787, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN PABLO', 22),
(788, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TAMINANGO', 22),
(789, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '20 DE JULIO', 22),
(790, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELEN', 22),
(791, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BOCAS DE SATINGA', 22),
(792, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CONTADERO', 22),
(793, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CHARCO', 22),
(794, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL ENCANO', 22),
(795, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL TAMBO (NA)', 22),
(796, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GENOVA', 22),
(797, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUACHUCAL', 22),
(798, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAITARILLA', 22),
(799, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ISCUANDE', 22),
(800, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LLORENTE', 22),
(801, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '22', 22),
(802, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OLAYA HERRERA', 22),
(803, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RICAURTE (N)', 22),
(804, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOTOMAYOR', 22),
(805, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YACUANQUER', 22),
(806, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CONVENCION', 23),
(807, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUCUTA', 23),
(808, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAMPLONA', 23),
(809, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OCAÑA', 23),
(810, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ABREGO', 23),
(811, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHINACOTA', 23),
(812, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLA DEL ROSARIO', 23),
(813, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MUTISCUA', 23),
(814, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIBU', 23),
(815, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOLEDO', 23),
(816, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARBOLEDAS', 23),
(817, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BOCHALEMA', 23),
(818, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CACHIRA', 23),
(819, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUCUTILLA', 23),
(820, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL ZULIA', 23),
(821, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HERRAN', 23),
(822, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LABATECA', 23),
(823, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SARDINATA', 23),
(824, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO 27', 23),
(825, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RAGONVALIA', 23),
(826, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SALAZAR', 23),
(827, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTIAGO', 23),
(828, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DURANIA', 23),
(829, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LOS PATIOS', 23),
(830, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA ESPERANZA', 23),
(831, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AGUAS CLARAS', 23),
(832, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CACOTA', 23),
(833, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CARCASI', 23),
(834, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHITAGA', 23),
(835, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CARMEN', 23),
(836, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL TARRA', 23),
(837, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HACARI', 23),
(838, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA DONJUANA', 23),
(839, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA FLORESTA', 23),
(840, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA JARRA', 23),
(841, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PLAYA', 23),
(842, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA VEGA', 23),
(843, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LOURDES', 23),
(844, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAMPLONITA', 23),
(845, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN BERNARDO DE BATA', 23),
(846, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN CALIXTO', 23),
(847, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN CAYETANO', 23),
(848, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SILOS', 23),
(849, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TEORAMA', 23),
(850, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLA CARO', 23),
(851, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO ASIS', 24),
(852, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MOCOA', 24),
(853, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LEGUIZAMO', 24),
(854, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ORITO', 24),
(855, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA HORMIGA', 24),
(856, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SIBUNDOY', 24),
(857, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLAGARZON', 24),
(858, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA DORADA (P)', 24),
(859, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO CAICEDO', 24),
(860, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO GUZMAN', 24),
(861, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARMENIA (Q)', 25),
(862, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'QUIMBAYA', 25),
(863, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CALARCA', 25),
(864, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA TEBAIDA', 25),
(865, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MONTENEGRO', 25),
(866, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARCELONA', 25),
(867, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '14', 25),
(868, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PIJAO', 25),
(869, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FILANDIA', 25),
(870, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GENOVA', 25),
(871, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SALENTO', 25),
(872, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CIRCASIA', 25),
(873, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUENAVISTA', 25),
(874, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CAIMO', 25),
(875, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUEBLO TAPADO', 25),
(876, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DOSQUEBRADAS', 26),
(877, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PEREIRA', 26),
(878, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ROSA DE CABAL', 26),
(879, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA VIRGINIA', 26),
(880, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MARSELLA', 26),
(881, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BELEN DE UMBRIA', 26),
(882, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUATICA', 26),
(883, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'APIA', 26),
(884, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MISTRATO', 26),
(885, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUEBLO RICO', 26),
(886, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'QUINCHIA', 26),
(887, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTUARIO', 26),
(888, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BALBOA', 26),
(889, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA CELIA', 26),
(890, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CRUCERO DE COMBIA', 26),
(891, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'IRRA', 26),
(892, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARRANCABERMEJA', 27),
(893, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHARALA', 27),
(894, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARBOSA', 27),
(895, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUCARAMANGA', 27),
(896, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOCORRO', 27),
(897, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN GIL', 27),
(898, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MALAGA', 27),
(899, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO WILCHES', 27),
(900, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ZAPATOCA', 27),
(901, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN VICENTE DE CHUCURI', 27),
(902, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUENTE NACIONAL', 27),
(903, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VELEZ', 27),
(904, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PIEDECUESTA', 27),
(905, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PINCHOTE', 27),
(906, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OIBA', 27),
(907, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BARICHARA', 27),
(908, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAPITANEJO', 27),
(909, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CONCEPCION', 27),
(910, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CURITI', 27),
(911, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARATOCA', 27),
(912, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COROMORO', 27),
(913, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FLORIDABLANCA', 27),
(914, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAVATA', 27),
(915, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUEPSA', 27),
(916, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'JESUS MARIA', 27),
(917, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ONZAGA', 27),
(918, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JOSE DE MIRANDA', 27),
(919, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TONA', 27),
(920, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VADO REAL', 27),
(921, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUADALUPE', 27),
(922, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MOGOTES', 27),
(923, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OCAMONTE', 27),
(924, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO ARAUJO', 27),
(925, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUAITA', 27),
(926, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CONTRATACION', 27),
(927, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LEBRIJA', 27),
(928, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLANUEVA', 27),
(929, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GIRON', 27),
(930, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CENTRO', 27),
(931, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL PLAYON', 27),
(932, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIONEGRO', 27),
(933, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CIMITARRA', 27),
(934, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SABANA DE TORRES', 27),
(935, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ACAPULCO', 27),
(936, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALBANIA (S)', 27),
(937, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BETULIA', 27),
(938, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '6', 27),
(939, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CALIFORNIA (S)', 27),
(940, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CERRITO', 27),
(941, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHIMA (S)', 27),
(942, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CITE', 27),
(943, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CONFINES', 27),
(944, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CARMEN DE CHUCURI', 27),
(945, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL LLANITO', 27),
(946, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ENCINO', 27),
(947, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ENCISO', 27),
(948, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GALAN (S)', 27),
(949, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GAMBITA', 27),
(950, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUACA', 27),
(951, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAPOTA', 27),
(952, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HATO (S)', 27),
(953, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA BELLEZA', 27),
(954, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LANDAZURI', 27),
(955, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LOS LAURELES', 27),
(956, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LOS SANTOS', 27),
(957, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MATANZA', 27),
(958, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MOLAGAVITA', 27),
(959, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OLIVAL', 27),
(960, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALMAR (S)', 27),
(961, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALO GORDO', 27),
(962, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PARAMO', 27),
(963, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUENTE SOGAMOSO', 27),
(964, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO PARRA', 27),
(965, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANDRES', 27),
(966, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN MIGUEL', 27),
(967, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN RAFAEL', 27),
(968, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SIMACOTA', 27),
(969, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SOGAMOSO', 27),
(970, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SURATA', 27),
(971, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TIENDA NUEVA', 27),
(972, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '30 DE SAN JOSE', 27),
(973, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VETAS', 27),
(974, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SINCELEJO', 28),
(975, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COROZAL', 28),
(976, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN PEDRO', 28),
(977, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOLU', 28),
(978, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN MARCOS', 28),
(979, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '28 (S)', 28),
(980, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUARANDA', 28),
(981, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COVEÑAS', 28),
(982, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MAJAGUAL', 28),
(983, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SINCE', 28),
(984, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALMITO', 28),
(985, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAMPUES', 28),
(986, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ONOFRE', 28),
(987, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOLU VIEJO', 28),
(988, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GALERAS', 28),
(989, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MORROA', 28),
(990, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN JUAN DE BETULIA', 28),
(991, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUENAVISTA', 28),
(992, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHALAN', 28),
(993, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAYABAL', 28),
(994, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA UNION (S)', 28),
(995, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LOS PALMITOS', 28),
(996, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OVEJAS', 28),
(997, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN BENITO ABAD', 28),
(998, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'IBAGUE', 29),
(999, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ESPINAL', 29),
(1000, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HONDA', 29),
(1001, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MELGAR', 29),
(1002, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MARIQUITA', 29),
(1003, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHICORAL', 29),
(1004, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LIBANO', 29),
(1005, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NATAGAIMA', 29),
(1006, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOLEMAIDA-MELGAR', 29),
(1007, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANZOATEGUI', 29),
(1008, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DOLORES', 29),
(1009, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALPUJARRA', 29),
(1010, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARMERO', 29),
(1011, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAJAMARCA', 29),
(1012, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CARMEN DE APICALA', 29),
(1013, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHAPARRAL', 29),
(1014, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FALAN', 29),
(1015, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FRESNO', 29),
(1016, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAMO', 29),
(1017, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ICONONZO', 29),
(1018, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LERIDA', 29),
(1019, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ORTEGA', 29),
(1020, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PURIFICACION', 29),
(1021, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ROVIRA', 29),
(1022, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SALDAÑA', 29),
(1023, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VENADILLO', 29),
(1024, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HERRERA', 29),
(1025, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PLANADAS', 29),
(1026, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALVARADO', 29),
(1027, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FLANDES', 29),
(1028, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PRADO', 29),
(1029, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUALANDAY', 29),
(1030, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUENOS AIRES', 29),
(1031, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ISABEL', 29),
(1032, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AMBALEMA', 29),
(1033, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ATACO', 29),
(1034, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'COYAIMA', 29),
(1035, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUNDAY', 29),
(1036, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GAITANIA', 29),
(1037, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HERVEO', 29),
(1038, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MURILLO', 29),
(1039, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PAJONALES', 29),
(1040, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALOCABILDO', 29),
(1041, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PIEDRAS', 29),
(1042, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIOBLANCO', 29),
(1043, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RONCES30S', 29),
(1044, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANTONIO', 29),
(1045, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN LUIS', 29),
(1046, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTIAGO PEREZ', 29),
(1047, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUAREZ', 29),
(1048, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '30 DE SAN JUAN', 29),
(1049, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLA HERMOSA', 29),
(1050, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLARRICA (T)', 29),
(1051, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUENAVENTURA', 30),
(1052, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUGA', 30),
(1053, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CARTAGO', 30),
(1054, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CALI', 30),
(1055, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PALMIRA', 30),
(1056, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TULUA', 30),
(1057, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA UNION (V)', 30),
(1058, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ROLDANILLO', 30),
(1059, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SEVILLA', 30),
(1060, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AMAIME', 30),
(1061, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANDALUCIA', 30),
(1062, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YUMBO', 30),
(1063, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PRADERA', 30),
(1064, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BUGALAGRANDE', 30),
(1065, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAICEDONIA', 30),
(1066, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'JAMUNDI', 30),
(1067, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CERRITO', 30),
(1068, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'FLORIDA', 30),
(1069, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GINEBRA', 30),
(1070, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUACARI', 30),
(1071, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ZARAGOZA (V)', 30),
(1072, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ZARZAL', 30),
(1073, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL QUEREMAL', 30),
(1074, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ULLOA', 30),
(1075, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VIJES', 30),
(1076, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'YOTOCO', 30),
(1077, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL DOVIO', 30),
(1078, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'OBANDO', 30),
(1079, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DAGUA', 30),
(1080, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'DARIEN (CALIMA)', 30),
(1081, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RESTREPO', 30),
(1082, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RIOFRIO', 30),
(1083, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TRUJILLO', 30),
(1084, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CANDELARIA', 30),
(1085, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA VICTORIA (V)', 30),
(1086, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TORO', 30),
(1087, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '6 (V)', 30),
(1088, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ALCALA', 30),
(1089, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PAILA', 30),
(1090, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VERSALLES', 30),
(1091, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANSERMA NUEVO', 30),
(1092, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA CUMBRE', 30),
(1093, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ROZO', 30),
(1094, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARGELIA (V)', 30),
(1095, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BORRERO AYERBE', 30),
(1096, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL AGUILA', 30),
(1097, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CABUYAL (V)', 30),
(1098, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CAIRO', 30),
(1099, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL CARMELO', 30),
(1100, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'EL PLACER (V)', 30),
(1101, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUABITAS', 30),
(1102, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN ANTONIO DE LOS CABALLEROS', 30),
(1103, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SAN PEDRO(V)', 30),
(1104, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ELENA', 30),
(1105, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VILLA GORGONA', 30),
(1106, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BAHÍA MÁLAGA', 30),
(1107, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MITU', 31),
(1108, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUERTO CARREÑO', 32),
(1109, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUMARIBO', 32),
(1110, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA PRIMAVERA', 32),
(1111, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTA ROSALIA', 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_currency`
--

CREATE TABLE IF NOT EXISTS `default_firesale_currency` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `cur_code` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `cur_tax` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cur_format` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cur_format_dec` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cur_format_sep` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cur_format_num` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `cur_mod` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exch_rate` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `default_firesale_currency`
--

INSERT INTO `default_firesale_currency` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `cur_code`, `title`, `slug`, `enabled`, `cur_tax`, `cur_format`, `cur_format_dec`, `cur_format_sep`, `cur_format_num`, `cur_mod`, `image`, `exch_rate`) VALUES
(1, '2014-02-10 00:00:00', '2015-04-21 15:38:43', 1, 1, 'COP', 'Peso Colombiano', 'peso-colombiano', '1', '16', '${{ price }}', ',', '.', '1', '+|1', 'dummy', '1'),
(2, '2014-01-30 17:24:06', '2014-02-10 20:51:51', NULL, 0, 'GBP', 'Default', 'default', '0', '20', '${{ price }}', '.', ',', '0', '+|0', 'dummy', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_department`
--

CREATE TABLE IF NOT EXISTS `default_firesale_department` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `code` longtext COLLATE utf8_unicode_ci,
  `name` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Volcado de datos para la tabla `default_firesale_department`
--

INSERT INTO `default_firesale_department` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `code`, `name`) VALUES
(1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'AMAZONAS'),
(2, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ANTIOQUIA'),
(3, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARAUCA'),
(4, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ARCHIPIELAGO DE SAN ANDRES'),
(5, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'ATLANTICO'),
(6, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BOLIVAR'),
(7, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'BOYACA'),
(8, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CALDAS'),
(9, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAQUETA'),
(10, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CASANARE'),
(11, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CAUCA'),
(12, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CESAR'),
(13, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CHOCO'),
(14, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CORDOBA'),
(15, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'CUNDINAMARCA'),
(16, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAINIA'),
(17, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'GUAVIARE'),
(18, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'HUILA'),
(19, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'LA GUAJIRA'),
(20, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'MAGDALENA'),
(21, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'META'),
(22, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NARIÑO'),
(23, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'NORTE DE SANTANDER'),
(24, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'PUTUMAYO'),
(25, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'QUINDIO'),
(26, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'RISARALDA'),
(27, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SANTANDER'),
(28, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'SUCRE'),
(29, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'TOLIMA'),
(30, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VALLE'),
(31, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VAUPES'),
(32, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'VICHADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_design`
--

CREATE TABLE IF NOT EXISTS `default_firesale_design` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `element` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `layout` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_digital_downloads`
--

CREATE TABLE IF NOT EXISTS `default_firesale_digital_downloads` (
  `order_item_id` int(11) NOT NULL,
  `downloads` int(11) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_discount_codes`
--

CREATE TABLE IF NOT EXISTS `default_firesale_discount_codes` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'percentage',
  `value` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` longtext COLLATE utf8_unicode_ci,
  `applies_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `usage` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'once',
  `start` int(11) DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  `used` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `default_firesale_discount_codes`
--

INSERT INTO `default_firesale_discount_codes` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `code`, `type`, `value`, `desc`, `applies_to`, `usage`, `start`, `end`, `used`) VALUES
(4, '2015-01-21 14:58:19', '2015-04-14 12:34:57', 10, 1, 'Codigo prueba imaginamos', 'fixed', '10000', 'Codigo de prueba', '0', 'once', 1421730000, 1422421200, 'n'),
(5, '2015-01-21 15:00:09', '2015-04-14 12:42:06', 10, 2, 'Codigo de prueba', 'percentage', '10', 'Prueba', '1', 'once_user', 1421730000, 1430366400, 'n'),
(6, '2015-01-21 15:00:55', '2015-04-14 12:33:48', 10, 3, '123456', 'percentage', '10', 'Prueba', '0', 'multiple', 1421730000, 1430366400, 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_discount_codes_firesale_categories`
--

CREATE TABLE IF NOT EXISTS `default_firesale_discount_codes_firesale_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `firesale_discount_codes_id` int(11) NOT NULL,
  `firesale_categories_id` int(11) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Volcado de datos para la tabla `default_firesale_discount_codes_firesale_categories`
--

INSERT INTO `default_firesale_discount_codes_firesale_categories` (`id`, `row_id`, `firesale_discount_codes_id`, `firesale_categories_id`, `orden`) VALUES
(1, 6, 15, 6, NULL),
(2, 6, 15, 6, NULL),
(3, 6, 15, 6, NULL),
(4, 6, 15, 6, NULL),
(5, 6, 15, 6, NULL),
(6, 6, 15, 6, NULL),
(7, 6, 15, 6, NULL),
(8, 6, 15, 6, NULL),
(9, 6, 15, 6, NULL),
(10, 6, 15, 6, NULL),
(11, 6, 15, 6, NULL),
(12, 6, 15, 6, NULL),
(13, 6, 15, 6, NULL),
(14, 6, 15, 6, NULL),
(15, 6, 15, 6, NULL),
(16, 6, 15, 6, NULL),
(17, 6, 15, 6, NULL),
(18, 6, 15, 6, NULL),
(19, 6, 15, 6, NULL),
(20, 6, 15, 6, NULL),
(21, 6, 15, 6, NULL),
(22, 6, 15, 6, NULL),
(23, 6, 15, 6, NULL),
(24, 6, 15, 6, NULL),
(25, 6, 15, 6, NULL),
(26, 6, 15, 6, NULL),
(27, 6, 15, 6, NULL),
(28, 6, 15, 6, NULL),
(29, 6, 15, 6, NULL),
(30, 6, 15, 6, NULL),
(31, 6, 15, 6, NULL),
(32, 6, 15, 6, NULL),
(33, 6, 15, 6, NULL),
(34, 6, 15, 6, NULL),
(35, 6, 15, 6, NULL),
(36, 6, 15, 6, NULL),
(37, 6, 15, 6, NULL),
(38, 6, 15, 6, NULL),
(39, 6, 15, 6, NULL),
(40, 6, 15, 6, NULL),
(41, 6, 15, 6, NULL),
(42, 6, 15, 6, NULL),
(43, 6, 15, 6, NULL),
(44, 6, 15, 6, NULL),
(45, 6, 15, 6, NULL),
(46, 6, 15, 6, NULL),
(47, 6, 15, 6, NULL),
(48, 6, 15, 6, NULL),
(49, 6, 15, 6, NULL),
(50, 6, 15, 6, NULL),
(51, 6, 15, 6, NULL),
(52, 6, 15, 6, NULL),
(53, 6, 15, 6, NULL),
(54, 6, 15, 6, NULL),
(55, 6, 15, 6, NULL),
(56, 6, 15, 6, NULL),
(57, 6, 15, 6, NULL),
(58, 6, 15, 6, NULL),
(59, 6, 15, 6, NULL),
(60, 6, 15, 6, NULL),
(61, 6, 15, 6, NULL),
(62, 6, 15, 6, NULL),
(63, 6, 15, 6, NULL),
(64, 6, 15, 6, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_discount_codes_firesale_products`
--

CREATE TABLE IF NOT EXISTS `default_firesale_discount_codes_firesale_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `firesale_discount_codes_id` int(11) NOT NULL,
  `firesale_products_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `default_firesale_discount_codes_firesale_products`
--

INSERT INTO `default_firesale_discount_codes_firesale_products` (`id`, `row_id`, `firesale_discount_codes_id`, `firesale_products_id`) VALUES
(1, 5, 15, 138),
(2, 5, 15, 138),
(3, 5, 15, 552);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_gateways`
--

CREATE TABLE IF NOT EXISTS `default_firesale_gateways` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` longtext COLLATE utf8_unicode_ci,
  `enabled` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `default_firesale_gateways`
--

INSERT INTO `default_firesale_gateways` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `name`, `slug`, `desc`, `enabled`) VALUES
(1, '2014-01-30 17:23:59', NULL, NULL, 0, 'Únicamente Solicitud de Pedido', 'dummy', 'Se enviaría correos de notificación y el administrador únicamente recibirá la solicitud y posteriormente se pondrá en contacto con usted.', '0'),
(2, '2014-02-10 22:12:09', NULL, NULL, 0, 'Payu', 'payu', 'El mismo sistema y seguridad de la plataforma de Pagosonline que conocías, lo encontrarás \nen PayU Latam, con aún más servicios, productos y herramientas.', '1'),
(3, '2014-01-30 17:23:59', NULL, NULL, 0, '2Checkout', '2checkout', '2Checkout.com is an online payment processing service that helps you accept all major credit cards, debit cards, PayPal and more.', '0'),
(4, '2014-01-30 17:23:59', NULL, NULL, 0, 'CardSave', 'cardsave', 'CardSave makes card processing easy with a wide range of cost effective solutions.', '0'),
(5, '2014-01-30 17:23:59', NULL, NULL, 0, 'DPS (PX Pay)', 'dps_pxpay', 'Integration for DPS PX Pay Gateway for Australia and New Zealand.', '0'),
(6, '2014-01-30 17:23:59', NULL, NULL, 0, 'DPS (PX Post)', 'dps_pxpost', 'PX POST is designed to handle transactions using a HTTPS Post Request.', '0'),
(7, '2014-01-30 17:23:59', NULL, NULL, 0, 'Authorize.Net SIM', 'authorize_net_sim', 'Server Integration Method (SIM) API uses a transaction payment fingerprint via a hosted payment form and 128-bit SSL encryption.', '0'),
(8, '2014-01-30 17:23:59', NULL, NULL, 0, 'eWAY', 'eway', 'Accept credit cards on your website with the leading UK payment gateway to connect your website directly to your bank to process your customers'' online credit card payments.', '0'),
(9, '2014-01-30 17:23:59', NULL, NULL, 0, 'eWAY Shared Payments', 'eway_shared', 'Shared Payments allows your customers to be redirected via HTTP FORM POST to a payment page which is hosted and secured by eWAY.', '0'),
(10, '2014-01-30 17:23:59', NULL, NULL, 0, 'GoCardless', 'gocardless', 'GoCardless makes it simple and cheap to take payments online. No merchant account. No credit card fees. No hassle.', '0'),
(11, '2014-01-30 17:23:59', NULL, NULL, 0, 'Netaxept', 'netaxept', 'Norwegian payment gateway.', '0'),
(12, '2014-01-30 17:23:59', NULL, NULL, 0, 'PayFlow', 'payflow_pro', 'A payment gateway links your website to your processing network and merchant account. Like most gateways, Payflow payment gateway handles all major credit cards.', '0'),
(13, '2014-01-30 17:23:59', NULL, NULL, 0, 'PayPal Express Checkout', 'paypal_express', 'Quick. Customers don’t need to enter their postage and billing information when making a purchase - PayPal already has the information stored.', '0'),
(14, '2014-01-30 17:23:59', NULL, NULL, 0, 'PayPal Payments Pro', 'paypal_pro', 'PayPal Payments Pro is an affordable website payment processing solution for businesses with 100+ orders/month.', '0'),
(15, '2014-01-30 17:23:59', NULL, NULL, 0, 'Rabo OmniKassa', 'rabo_omnikassa', 'Duch payment gateway.', '0'),
(16, '2014-01-30 17:23:59', NULL, NULL, 0, 'Sage Pay Direct', 'sagepay_direct', 'Sage Pay Go with Direct integration gives you complete control over the look and feel of your payment pages, and helps you manage the entire payment.', '0'),
(17, '2014-01-30 17:23:59', NULL, NULL, 0, 'Sage Pay Server', 'sagepay_server', 'Sage Pay Go with Server integration is a winning combination of flexibility and ease of integration. You get the security of outsourcing your payments with the ability to manage transactions and reporting on your own servers.', '0'),
(18, '2014-01-30 17:23:59', NULL, NULL, 0, 'Stripe', 'stripe', 'Stripe makes it easy for developers to accept credit cards on the web.', '0'),
(19, '2014-01-30 17:23:59', NULL, NULL, 0, 'WorldPay', 'worldpay', 'Online payment gateways, online merchant accounts and risk management products to grow your business online.', '0'),
(20, '2014-01-30 17:23:59', NULL, NULL, 0, 'Authorize.Net', 'authorize_net', 'Payment gateway enables internet merchants to accept online payments via credit card and e-check.', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_gateway_settings`
--

CREATE TABLE IF NOT EXISTS `default_firesale_gateway_settings` (
  `id` int(11) NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `default_firesale_gateway_settings`
--

INSERT INTO `default_firesale_gateway_settings` (`id`, `key`, `value`) VALUES
(2, 'merchantId', '523996'),
(2, 'ApiKey', '362i5dg8674dl46hgrmqdk3bqd'),
(2, 'test', '0'),
(2, 'currency', 'COP'),
(2, 'email', 'brayan.acebo@imaginamos.co'),
(2, 'accountId', '525672'),
(2, 'description_extra', 'Este texto saldra en Payu cuando se este procesando el pago en la plataforma');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_orders`
--

CREATE TABLE IF NOT EXISTS `default_firesale_orders` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gateway` int(11) DEFAULT NULL,
  `order_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `price_sub` float DEFAULT NULL,
  `price_ship` float DEFAULT NULL,
  `price_total` float DEFAULT NULL,
  `currency` int(11) DEFAULT NULL,
  `exchange_rate` float DEFAULT NULL,
  `ship_to` int(11) DEFAULT NULL,
  `bill_to` int(11) DEFAULT NULL,
  `shipping` int(11) NOT NULL,
  `code_used` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=336 ;

--
-- Volcado de datos para la tabla `default_firesale_orders`
--

INSERT INTO `default_firesale_orders` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `ip`, `gateway`, `order_status`, `price_sub`, `price_ship`, `price_total`, `currency`, `exchange_rate`, `ship_to`, `bill_to`, `shipping`, `code_used`) VALUES
(176, '2015-02-23 15:05:18', NULL, NULL, 0, '190.9.193.195', 0, '1', 38706.9, 0, 44900, 1, 1, NULL, NULL, 0, NULL),
(177, '2015-02-25 09:11:49', NULL, NULL, 0, '190.9.193.195', 0, '1', 51724.1, 0, 60000, 1, 1, NULL, NULL, 0, NULL),
(178, '2015-02-25 13:53:31', NULL, 10, 0, '181.58.168.110', 0, '1', 129864, 0, 154600, 1, 1, NULL, NULL, 0, NULL),
(179, '2015-02-25 13:56:49', NULL, 10, 0, '181.58.168.110', 0, '1', 133276, 0, 154600, 1, 1, NULL, NULL, 0, NULL),
(181, '2015-02-28 18:19:04', NULL, 10, 0, '181.58.168.110', 0, '1', 17155.2, 0, 19900, 1, 1, NULL, NULL, 0, NULL),
(182, '2015-03-03 11:37:02', NULL, 10, 0, '181.58.168.110', 0, '1', 17155.2, 0, 19900, 1, 1, NULL, NULL, 0, NULL),
(186, '2015-03-04 14:28:26', NULL, 10, 0, '181.143.140.59', 0, '1', 51465.5, 0, 59700, 1, 1, NULL, NULL, 0, NULL),
(187, '2015-03-10 13:24:21', NULL, 10, 0, '181.143.140.59', 0, '1', 206810, 0, 239900, 1, 1, NULL, NULL, 0, NULL),
(189, '2015-03-10 14:19:55', NULL, 13, 0, '181.143.140.59', 0, '1', 151049, 0, 179820, 1, 1, NULL, NULL, 0, '6'),
(190, '2015-03-10 14:24:24', NULL, 13, 0, '181.143.140.59', 0, '1', 155017, 0, 179820, 1, 1, NULL, NULL, 0, '6'),
(191, '2015-03-10 18:15:25', NULL, 10, 0, '181.143.140.59', 0, '1', 77508.6, 0, 89910, 1, 1, NULL, NULL, 0, '6'),
(192, '2015-03-10 18:16:21', NULL, 10, 0, '181.143.140.59', 0, '1', 17155.2, 0, 19900, 1, 1, NULL, NULL, 0, NULL),
(193, '2015-03-12 09:37:54', NULL, 10, 0, '181.58.168.110', 0, '1', 17155.2, 0, 19900, 1, 1, NULL, NULL, 0, NULL),
(195, '2015-03-12 09:52:50', NULL, 10, 0, '181.58.168.110', 0, '1', 335664, 0, 399600, 1, 1, NULL, NULL, 0, NULL),
(196, '2015-03-12 09:57:31', NULL, 13, 0, '181.143.140.59', 0, '1', 38706.9, 0, 44900, 1, 1, NULL, NULL, 0, NULL),
(197, '2015-03-12 10:55:52', NULL, 10, 0, '181.58.168.110', 0, '1', 469728, 0, 559200, 1, 1, NULL, NULL, 0, NULL),
(199, '2015-03-12 12:29:05', NULL, 10, 0, '181.58.168.110', 0, '1', 482069, 0, 559200, 1, 1, NULL, NULL, 0, NULL),
(201, '2015-03-12 12:34:31', NULL, 10, 0, '190.85.21.242', 0, '1', 17155.2, 0, 19900, 1, 1, NULL, NULL, 0, NULL),
(248, '2015-03-19 17:40:07', NULL, 10, 0, '190.85.21.242', 0, '1', 129224, 0, 149900, 1, 1, NULL, NULL, 0, NULL),
(250, '2015-03-19 18:00:06', NULL, 10, 0, '181.143.140.59', 0, '1', 129224, 0, 149900, 1, 1, NULL, NULL, 0, NULL),
(252, '2015-03-19 18:19:27', NULL, 10, 0, '190.85.21.242', 0, '1', 125916, 0, 149900, 1, 1, NULL, NULL, 0, NULL),
(253, '2015-03-19 18:20:39', NULL, 10, 0, '190.85.21.242', 0, '1', 129224, 0, 149900, 1, 1, NULL, NULL, 0, NULL),
(255, '2015-03-19 18:40:08', NULL, 10, 0, '190.85.21.242', 0, '1', 134316, 0, 159900, 1, 1, NULL, NULL, 0, NULL),
(256, '2015-03-19 18:42:55', NULL, 10, 0, '190.85.21.242', 0, '1', 537264, 0, 639600, 1, 1, NULL, NULL, 0, NULL),
(257, '2015-03-19 18:43:24', NULL, 10, 0, '190.85.21.242', 0, '1', 551379, 0, 639600, 1, 1, NULL, NULL, 0, NULL),
(259, '2015-03-20 10:24:07', NULL, 10, 0, '181.58.168.110', 0, '1', 86120.7, 0, 99900, 1, 1, NULL, NULL, 0, NULL),
(260, '2015-03-23 16:30:19', NULL, 13, 0, '186.28.184.241', 0, '1', 81810.3, 0, 94900, 1, 1, NULL, NULL, 0, NULL),
(263, '2015-03-30 17:01:10', NULL, 10, 0, '181.143.140.59', 0, '1', 172241, 0, 199800, 1, 1, NULL, NULL, 0, NULL),
(266, '2015-04-03 22:11:01', NULL, 13, 0, '190.27.189.67', 0, '1', 51637.9, 0, 59900, 1, 1, NULL, NULL, 0, NULL),
(268, '2015-04-03 22:15:14', NULL, 13, 0, '190.27.189.67', 0, '1', 129224, 0, 149900, 1, 1, NULL, NULL, 0, NULL),
(271, '2015-04-04 20:16:22', NULL, 13, 0, '186.28.173.114', 0, '1', 116121, 0, 134700, 1, 1, NULL, NULL, 0, NULL),
(273, '2015-04-04 20:18:42', NULL, 13, 0, '186.28.173.114', 0, '1', 85428, 0, 101700, 1, 1, NULL, NULL, 0, NULL),
(274, '2015-04-04 20:21:21', NULL, 13, 0, '186.28.173.114', 0, '1', 96348, 0, 114700, 1, 1, NULL, NULL, 0, NULL),
(275, '2015-04-04 20:30:07', NULL, 13, 0, '186.28.173.114', 0, '1', 98879.3, 0, 114700, 1, 1, NULL, NULL, 0, NULL),
(277, '2015-04-05 19:08:26', NULL, 13, 0, '186.28.59.32', 0, '1', 120603, 0, 139899, 1, 1, NULL, NULL, 0, NULL),
(279, '2015-04-07 11:58:51', NULL, 13, 0, '186.28.147.159', 0, '1', 38706.9, 0, 44900, 1, 1, NULL, NULL, 0, NULL),
(280, '2015-04-08 15:53:40', NULL, 10, 0, '181.143.140.59', 0, '1', 129224, 0, 149900, 1, 1, NULL, NULL, 0, NULL),
(284, '2015-04-08 16:15:15', NULL, 10, 0, '181.143.140.59', 0, '1', 129224, 0, 149900, 1, 1, NULL, NULL, 0, NULL),
(286, '2015-04-09 09:44:09', NULL, 13, 0, '186.28.17.90', 0, '1', 94741.4, 0, 109900, 1, 1, NULL, NULL, 0, NULL),
(288, '2015-04-09 09:48:25', NULL, 13, 0, '186.28.17.90', 0, '1', 223966, 0, 259800, 1, 1, NULL, NULL, 0, NULL),
(290, '2015-04-09 22:30:32', NULL, 13, 0, '186.29.164.186', 0, '1', 38706.9, 0, 44900, 1, 1, NULL, NULL, 0, NULL),
(291, '2015-04-09 22:30:41', NULL, 13, 0, '186.29.164.186', 2, '1', 38706.9, 10649, 55549, 1, 1, 48, 48, 2, NULL),
(292, '2015-04-09 22:34:18', NULL, 18, 0, '190.159.146.87', 0, '1', 28362.1, 0, 32900, 1, 1, NULL, NULL, 0, NULL),
(293, '2015-04-09 22:37:30', NULL, 18, 0, '190.159.146.87', 2, '2', 28362.1, 10529, 43429, 1, 1, 59, 60, 2, NULL),
(294, '2015-04-10 19:54:31', NULL, 13, 0, '186.31.112.142', 0, '1', 76551.7, 0, 88800, 1, 1, NULL, NULL, 0, NULL),
(295, '2015-04-10 19:58:08', NULL, 18, 0, '186.31.112.142', 0, '1', 64482.8, 0, 74800, 1, 1, NULL, NULL, 0, NULL),
(296, '2015-04-10 19:58:26', NULL, 18, 0, '186.31.112.142', 2, '1', 64482.8, 10948, 85748, 1, 1, 60, 60, 2, NULL),
(297, '2015-04-13 10:52:03', NULL, 10, 0, '181.58.168.110', 0, '1', 103362, 0, 119900, 1, 1, NULL, NULL, 0, NULL),
(298, '2015-04-13 11:03:06', NULL, 10, 0, '181.58.168.110', 2, '1', 103362, 11399, 131299, 1, 1, 44, 65, 2, NULL),
(299, '2015-04-13 11:07:42', NULL, 10, 0, '181.58.168.110', 2, '1', 103362, 11399, 131299, 1, 1, 44, 65, 2, NULL),
(300, '2015-04-13 11:08:10', NULL, 10, 0, '181.58.168.110', 2, '1', 103362, 11399, 131299, 1, 1, 66, 65, 2, NULL),
(301, '2015-04-13 11:48:06', NULL, 10, 0, '181.58.168.110', 0, '1', 33516, 0, 39900, 1, 1, NULL, NULL, 0, NULL),
(302, '2015-04-13 12:12:12', NULL, 10, 0, '181.58.168.110', 0, '1', 50316, 0, 59900, 1, 1, NULL, NULL, 0, NULL),
(303, '2015-04-13 12:13:31', NULL, 10, 0, '181.58.168.110', 0, '1', 16716, 0, 19900, 1, 1, NULL, NULL, 0, NULL),
(304, '2015-04-13 12:14:16', NULL, 10, 0, '181.58.168.110', 0, '1', 17155.2, 0, 19900, 1, 1, NULL, NULL, 0, NULL),
(305, '2015-04-14 12:35:20', NULL, 20, 0, '181.135.200.248', 0, '1', 93025.9, 0, 107910, 1, 1, NULL, NULL, 0, '6'),
(306, '2015-04-14 12:38:14', NULL, 20, 0, '181.135.200.248', 2, '1', 359276, 11279.1, 427710, 1, 1, 69, 70, 2, 'Codigo de prueba'),
(307, '2015-04-14 12:42:51', NULL, 20, 0, '181.135.200.248', 0, '1', 359276, 0, 427710, 1, 1, NULL, NULL, 0, '5'),
(308, '2015-04-14 12:44:59', NULL, 20, 0, '181.135.200.248', 0, '1', 368716, 0, 427710, 1, 1, NULL, NULL, 0, '5'),
(309, '2015-04-14 12:50:55', NULL, 20, 0, '181.135.200.248', 0, '1', 516897, 0, 599600, 1, 1, NULL, NULL, 0, NULL),
(310, '2015-04-14 12:51:11', NULL, 20, 0, '181.135.200.248', 2, '1', 516897, 17996, 617596, 1, 1, 70, 70, 1, NULL),
(311, '2015-04-14 12:52:31', NULL, 20, 0, '181.135.200.248', 1, '11', 516897, 17996, 617596, 1, 1, 70, 70, 1, NULL),
(312, '2015-04-14 13:03:32', NULL, 20, 0, '181.135.200.248', 0, '1', 133534, 0, 154900, 1, 1, NULL, NULL, 0, NULL),
(313, '2015-04-14 13:03:42', NULL, 20, 0, '181.135.200.248', 1, '11', 133534, 11749, 166649, 1, 1, 70, 70, 2, NULL),
(314, '2015-04-14 14:44:07', NULL, 20, 0, '181.135.200.248', 0, '1', 206724, 0, 239800, 1, 1, NULL, NULL, 0, NULL),
(315, '2015-04-14 14:45:32', NULL, 20, 0, '181.135.200.248', 1, '1', 201432, 12598, 239800, 1, 1, 70, 70, 2, NULL),
(316, '2015-04-14 17:54:31', NULL, 20, 0, '181.135.200.248', 0, '1', 206724, 0, 239800, 1, 1, NULL, NULL, 0, NULL),
(317, '2015-04-14 17:56:36', NULL, 10, 0, '181.58.168.110', 0, '1', 309655, 0, 359200, 1, 1, NULL, NULL, 0, NULL),
(318, '2015-04-14 17:58:39', NULL, 10, 0, '181.58.168.110', 1, '11', 309655, 13792, 372992, 1, 1, 65, 65, 2, NULL),
(319, '2015-04-15 10:45:32', NULL, 21, 0, '181.143.140.59', 1, '11', 133534, 11749, 166648, 1, 1, 75, 75, 2, NULL),
(320, '2015-04-15 11:03:59', NULL, 21, 0, '181.143.140.59', 0, '1', 86120.7, 0, 99900, 1, 1, NULL, NULL, 0, NULL),
(321, '2015-04-15 11:05:04', NULL, 21, 0, '181.143.140.59', 1, '11', 86120.7, 11199, 111099, 1, 1, 76, 76, 2, NULL),
(322, '2015-04-15 11:07:21', NULL, 21, 0, '181.143.140.59', 0, '1', 38706.9, 0, 44900, 1, 1, NULL, NULL, 0, NULL),
(323, '2015-04-15 11:07:47', NULL, 21, 0, '181.143.140.59', 1, '11', 38706.9, 10649, 55549, 1, 1, 76, 76, 2, NULL),
(324, '2015-04-20 13:33:40', NULL, 25, 0, '190.145.2.43', 1, '11', 28362.1, 10529, 43429, 1, 1, 77, 77, 2, NULL),
(325, '2015-04-20 13:51:06', NULL, 25, 0, '190.145.2.43', 0, '1', 31293.1, 0, 36300, 1, 1, NULL, NULL, 0, NULL),
(326, '2015-04-20 13:52:23', '2015-04-20 15:59:57', 25, 0, '190.145.2.43', 2, '2', 31293, 10563, 46863, 1, 1, 77, 78, 2, NULL),
(327, '2015-04-22 09:38:16', NULL, 10, 0, '181.143.140.59', 0, '1', 254224, 0, 294900, 1, 1, NULL, NULL, 0, NULL),
(328, '2015-04-22 09:38:36', NULL, 10, 0, '181.143.140.59', 2, '1', 254224, 13149, 308049, 1, 1, 65, 65, 2, NULL),
(329, '2015-04-22 09:40:42', NULL, 10, 0, '181.143.140.59', 2, '1', 254224, 13149, 308049, 1, 1, 65, 65, 2, NULL),
(330, '2015-04-22 09:44:49', NULL, 10, 0, '181.143.140.59', 2, '1', 254224, 13149, 308049, 1, 1, 65, 65, 2, NULL),
(331, '2015-04-22 10:15:38', NULL, 10, 0, '181.60.20.6', 0, '1', 1e+06, 0, 1.16e+06, 1, 1, NULL, NULL, 0, NULL),
(332, '2015-04-22 10:15:45', NULL, 10, 0, '181.60.20.6', 2, '1', 1e+06, 21800, 1.1818e+06, 1, 1, 65, 65, 2, NULL),
(333, '2015-04-22 10:21:59', NULL, 10, 0, '181.60.20.6', 2, '1', 1e+06, 21800, 1.1818e+06, 1, 1, 65, 65, 2, NULL),
(334, '2015-04-22 10:34:07', NULL, 10, 0, '181.143.140.59', 0, '1', 267069, 0, 309800, 1, 1, NULL, NULL, 0, NULL),
(335, '2015-04-22 10:34:21', NULL, 10, 0, '181.143.140.59', 2, '1', 267069, 13298, 323098, 1, 1, 65, 65, 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_orders_items`
--

CREATE TABLE IF NOT EXISTS `default_firesale_orders_items` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `code` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` float DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `tax_band` int(11) DEFAULT NULL,
  `options` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=456 ;

--
-- Volcado de datos para la tabla `default_firesale_orders_items`
--

INSERT INTO `default_firesale_orders_items` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `order_id`, `product_id`, `code`, `name`, `price`, `qty`, `tax_band`, `options`) VALUES
(245, '2015-02-23 15:05:18', NULL, NULL, 0, 176, 176, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 1, 1, ''),
(246, '2015-02-25 09:11:49', NULL, NULL, 0, 177, 200, 'AB0269', 'AUDIFONOS ', 60000, 1, 1, ''),
(247, '2015-02-25 13:53:31', NULL, 10, 0, 178, 176, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 3, 1, ''),
(248, '2015-02-25 13:53:31', NULL, 10, 0, 178, 178, 'AB0036', 'CAMISETA OFICIAL 2013', 19900, 1, 1, ''),
(249, '2015-02-25 13:56:49', NULL, 10, 0, 179, 176, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 3, 1, ''),
(250, '2015-02-25 13:56:49', NULL, 10, 0, 179, 178, 'AB0036', 'CAMISETA OFICIAL 2013', 19900, 1, 1, ''),
(253, '2015-02-28 18:19:04', NULL, 10, 0, 181, 178, 'AB0036', 'CAMISETA OFICIAL 2013', 19900, 1, 1, ''),
(254, '2015-03-03 11:37:02', NULL, 10, 0, 182, 178, 'AB0036', 'CAMISETA OFICIAL 2013', 19900, 1, 1, ''),
(258, '2015-03-04 14:28:26', NULL, 10, 0, 186, 177, 'AB0031', 'CAMISETA OFICIAL 2013', 19900, 3, 1, ''),
(259, '2015-03-10 13:24:21', NULL, 10, 0, 187, 177, 'AB0031', 'CAMISETA OFICIAL 2013', 19900, 1, 1, ''),
(260, '2015-03-10 13:24:21', NULL, 10, 0, 187, 179, 'AB0095', 'SUDADERA DE PRESENTACION 2014', 110000, 2, 1, ''),
(263, '2015-03-10 14:19:55', NULL, 13, 0, 189, 185, 'AB0344', 'BUSO SHOP ', 89910, 2, 1, ''),
(265, '2015-03-10 18:15:25', NULL, 10, 0, 191, 185, 'AB0344', 'BUSO SHOP ', 89910, 1, 1, ''),
(266, '2015-03-10 18:16:21', NULL, 10, 0, 192, 178, 'AB0036', 'CAMISETA OFICIAL 2013', 19900, 1, 1, ''),
(267, '2015-03-12 09:37:54', NULL, 10, 0, 193, 178, 'AB0036', 'CAMISETA OFICIAL 2013', 19900, 1, 1, ''),
(269, '2015-03-12 09:52:50', NULL, 10, 0, 195, 185, 'AB0344', 'BUSO SHOP ', 99900, 3, 1, ''),
(270, '2015-03-12 09:57:31', NULL, 13, 0, 196, 176, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 1, 1, ''),
(271, '2015-03-12 10:55:52', NULL, 10, 0, 197, 185, 'AB0344', 'BUSO SHOP ', 99900, 4, 1, ''),
(273, '2015-03-12 12:29:05', NULL, 10, 0, 199, 185, 'AB0344', 'BUSO SHOP ', 99900, 5, 1, ''),
(274, '2015-03-12 12:29:05', NULL, 10, 0, 199, 178, 'AB0036', 'CAMISETA OFICIAL 2013', 19900, 3, 1, ''),
(277, '2015-03-12 12:34:31', NULL, 10, 0, 201, 178, 'AB0036', 'CAMISETA OFICIAL 2013', 19900, 1, 1, ''),
(348, '2015-03-19 17:40:07', NULL, 10, 0, 248, 406, 'AB0378-M', 'CAMISETA OFICIAL 2015 MANGA CORTA  - M', 149900, 1, 1, 'a:1:{i:28;a:6:{s:6:"mod_id";s:2:"28";s:6:"var_id";s:2:"86";s:4:"type";s:1:"1";s:5:"title";s:5:"Talla";s:5:"value";s:1:"M";s:5:"price";s:1:"0";}}'),
(350, '2015-03-19 18:00:06', NULL, 10, 0, 250, 383, 'AB0302-S', 'CAMISETA VISITANTE 2015 MANGA CORTA  - S', 149900, 1, 1, 'a:1:{i:23;a:6:{s:6:"mod_id";s:2:"23";s:6:"var_id";s:2:"63";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(353, '2015-03-19 18:20:39', NULL, 10, 0, 253, 383, 'AB0302-S', 'CAMISETA VISITANTE 2015 MANGA CORTA  - S', 149900, 1, 1, 'a:1:{i:23;a:6:{s:6:"mod_id";s:2:"23";s:6:"var_id";s:2:"63";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(357, '2015-03-19 18:43:24', NULL, 10, 0, 257, 306, 'AB0243', 'CUBRELECHO DOBLE ', 159900, 4, 1, ''),
(359, '2015-03-20 10:24:07', NULL, 10, 0, 259, 394, 'AB0344-S', 'BUSO SHOP  - S', 99900, 1, 1, 'a:1:{i:25;a:6:{s:6:"mod_id";s:2:"25";s:6:"var_id";s:2:"74";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(360, '2015-03-23 16:30:19', NULL, 13, 0, 260, 418, 'AB0385-S', 'CAMISETA ENTRENAMIENTO 2015 AZUL  - S', 94900, 1, 1, 'a:1:{i:31;a:6:{s:6:"mod_id";s:2:"31";s:6:"var_id";s:2:"98";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(363, '2015-03-30 17:01:10', NULL, 10, 0, 263, 394, 'AB0344-S', 'BUSO SHOP  - S', 99900, 2, 1, 'a:1:{i:25;a:6:{s:6:"mod_id";s:2:"25";s:6:"var_id";s:2:"74";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(366, '2015-04-03 22:11:01', NULL, 13, 0, 266, 323, 'AB0355', 'OSO MFC ', 59900, 1, 1, ''),
(368, '2015-04-03 22:15:14', NULL, 13, 0, 268, 383, 'AB0302-S', 'CAMISETA VISITANTE 2015 MANGA CORTA  - S', 149900, 1, 1, 'a:1:{i:23;a:6:{s:6:"mod_id";s:2:"23";s:6:"var_id";s:2:"63";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(371, '2015-04-04 20:16:22', NULL, 13, 0, 271, 495, 'AB0341-M', 'ESQUELETO ESTRELLA BLANCO - M', 44900, 3, 1, 'a:1:{i:50;a:6:{s:6:"mod_id";s:2:"50";s:6:"var_id";s:3:"175";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"M";s:5:"price";s:1:"0";}}'),
(373, '2015-04-04 20:18:42', NULL, 13, 0, 273, 309, 'AB0266', 'COJIN TV', 46900, 1, 1, ''),
(374, '2015-04-04 20:21:10', NULL, 13, 0, 273, 357, '', 'COJIN 45 x 45  TAPETE 60 x 40', 54800, 1, 1, ''),
(376, '2015-04-04 20:21:21', NULL, 13, 0, 274, 357, '', 'COJIN 45 x 45  TAPETE 60 x 40', 54800, 1, 1, ''),
(377, '2015-04-04 20:30:07', NULL, 13, 0, 275, 357, '', 'COJIN 45 x 45  TAPETE 60 x 40', 54800, 1, 1, ''),
(378, '2015-04-04 20:30:07', NULL, 13, 0, 275, 323, 'AB0355', 'OSO MFC ', 59900, 1, 1, ''),
(381, '2015-04-05 19:08:26', NULL, 13, 0, 277, 436, 'AB0376-2T', 'MINIKIT 2015 - 2T', 139899, 1, 1, 'a:1:{i:35;a:6:{s:6:"mod_id";s:2:"35";s:6:"var_id";s:3:"116";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:2:"2T";s:5:"price";s:1:"0";}}'),
(383, '2015-04-07 11:58:51', NULL, 13, 0, 279, 362, 'AB0027-S', 'CAMISETA VISITANTE  2013  - S', 44900, 1, 1, 'a:1:{i:17;a:6:{s:6:"mod_id";s:2:"17";s:6:"var_id";s:2:"42";s:4:"type";s:1:"1";s:5:"title";s:7:"Tallas:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(384, '2015-04-08 15:53:40', NULL, 10, 0, 280, 384, 'AB0303-M', 'CAMISETA VISITANTE 2015 MANGA CORTA  - M', 149900, 1, 1, 'a:1:{i:23;a:6:{s:6:"mod_id";s:2:"23";s:6:"var_id";s:2:"64";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"M";s:5:"price";s:1:"0";}}'),
(388, '2015-04-08 16:15:15', NULL, 10, 0, 284, 384, 'AB0303-M', 'CAMISETA VISITANTE 2015 MANGA CORTA  - M', 149900, 1, 1, 'a:1:{i:23;a:6:{s:6:"mod_id";s:2:"23";s:6:"var_id";s:2:"64";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"M";s:5:"price";s:1:"0";}}'),
(390, '2015-04-09 09:44:09', NULL, 13, 0, 286, 470, 'AB0380-S', 'CAMISETA OFICIAL NIOS 2015 - S', 109900, 1, 1, 'a:1:{i:44;a:6:{s:6:"mod_id";s:2:"44";s:6:"var_id";s:3:"150";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(392, '2015-04-09 09:48:25', NULL, 13, 0, 288, 470, 'AB0380-S', 'CAMISETA OFICIAL NIOS 2015 - S', 109900, 1, 1, 'a:1:{i:44;a:6:{s:6:"mod_id";s:2:"44";s:6:"var_id";s:3:"150";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(393, '2015-04-09 09:48:25', NULL, 13, 0, 288, 383, 'AB0302-S', 'CAMISETA VISITANTE 2015 MANGA CORTA  - S', 149900, 1, 1, 'a:1:{i:23;a:6:{s:6:"mod_id";s:2:"23";s:6:"var_id";s:2:"63";s:4:"type";s:1:"1";s:5:"title";s:6:"Talla:";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(396, '2015-04-09 22:30:32', NULL, 13, 0, 290, 517, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 1, 1, ''),
(397, '2015-04-09 22:30:41', NULL, 13, 0, 291, 517, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 1, 1, ''),
(398, '2015-04-09 22:34:18', NULL, 18, 0, 292, 531, 'AB0273', 'COBIJA FLECEE', 32900, 1, 1, ''),
(399, '2015-04-09 22:37:30', NULL, 18, 0, 293, 531, 'AB0273', 'COBIJA FLECEE', 32900, 1, 1, ''),
(400, '2015-04-10 19:54:31', NULL, 13, 0, 294, 540, 'AB0346', 'T SHIRT OVER CON ESTAMPADO CAMPIN', 48900, 1, 1, ''),
(401, '2015-04-10 19:54:31', NULL, 13, 0, 294, 572, 'AB0349', 'CAMISETA ESTAMPADA MC AZUL REY', 39900, 1, 1, ''),
(402, '2015-04-10 19:58:08', NULL, 18, 0, 295, 578, '', 'PROTECTOR PARA IPHONE 5s  AURICULARES MFC', 44900, 1, 1, ''),
(403, '2015-04-10 19:58:08', NULL, 18, 0, 295, 522, 'AB0175', 'COJIN 45X45', 29900, 1, 1, ''),
(404, '2015-04-10 19:58:26', NULL, 18, 0, 296, 578, '', 'PROTECTOR PARA IPHONE 5s  AURICULARES MFC', 44900, 1, 1, ''),
(405, '2015-04-10 19:58:26', NULL, 18, 0, 296, 522, 'AB0175', 'COJIN 45X45', 29900, 1, 1, ''),
(406, '2015-04-13 10:52:03', NULL, 10, 0, 297, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 1, 1, ''),
(407, '2015-04-13 11:03:06', NULL, 10, 0, 298, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 1, 1, ''),
(408, '2015-04-13 11:07:42', NULL, 10, 0, 299, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 1, 1, ''),
(409, '2015-04-13 11:08:10', NULL, 10, 0, 300, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 1, 1, ''),
(413, '2015-04-13 12:14:16', NULL, 10, 0, 304, 519, 'AB0036', 'CAMISETA OFICIAL 2013', 19900, 1, 1, ''),
(414, '2015-04-14 12:35:20', NULL, 20, 0, 305, 552, 'AB0377', 'OFICIAL DAMA 2015', 107910, 1, 1, ''),
(415, '2015-04-14 12:38:14', NULL, 20, 0, 306, 552, 'AB0377', 'OFICIAL DAMA 2015', 107910, 1, 1, ''),
(416, '2015-04-14 12:42:51', NULL, 20, 0, 307, 552, 'AB0377', 'OFICIAL DAMA 2015', 107910, 1, 1, ''),
(417, '2015-04-14 12:42:51', NULL, 20, 0, 307, 525, 'AB0243', 'CUBRELECHO DOBLE ', 159900, 2, 1, ''),
(418, '2015-04-14 12:44:59', NULL, 20, 0, 308, 552, 'AB0377', 'OFICIAL DAMA 2015', 107910, 1, 1, ''),
(420, '2015-04-14 12:50:55', NULL, 20, 0, 309, 525, 'AB0243', 'CUBRELECHO DOBLE ', 159900, 3, 1, ''),
(421, '2015-04-14 12:50:55', NULL, 20, 0, 309, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 1, 1, ''),
(422, '2015-04-14 12:51:11', NULL, 20, 0, 310, 525, 'AB0243', 'CUBRELECHO DOBLE ', 159900, 3, 1, ''),
(423, '2015-04-14 12:51:11', NULL, 20, 0, 310, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 1, 1, ''),
(424, '2015-04-14 12:52:31', NULL, 20, 0, 311, 525, 'AB0243', 'CUBRELECHO DOBLE ', 159900, 3, 1, ''),
(425, '2015-04-14 12:52:31', NULL, 20, 0, 311, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 1, 1, ''),
(426, '2015-04-14 13:03:32', NULL, 20, 0, 312, 545, 'AB0379', 'CAMISETA OFICIAL 2015 MANGA LARGA ', 154900, 1, 1, ''),
(427, '2015-04-14 13:03:42', NULL, 20, 0, 313, 545, 'AB0379', 'CAMISETA OFICIAL 2015 MANGA LARGA ', 154900, 1, 1, ''),
(428, '2015-04-14 14:44:07', NULL, 20, 0, 314, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 2, 1, ''),
(429, '2015-04-14 14:45:32', NULL, 20, 0, 315, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 2, 1, ''),
(430, '2015-04-14 17:54:31', NULL, 20, 0, 316, 552, 'AB0377', 'OFICIAL DAMA 2015', 119900, 2, 1, ''),
(431, '2015-04-14 17:56:36', NULL, 10, 0, 317, 517, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 8, 1, ''),
(432, '2015-04-14 17:58:39', NULL, 10, 0, 318, 517, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 8, 1, ''),
(433, '2015-04-15 10:45:33', NULL, 21, 0, 319, 607, 'AB0379-S', 'CAMISETA OFICIAL 2015 MANGA LARGA  - S', 154899, 1, 1, 'a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"221";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"S";s:5:"price";s:1:"0";}}'),
(434, '2015-04-15 11:03:59', NULL, 21, 0, 320, 603, 'AB0344-XS', 'BUSO SHOP  - XS', 99900, 1, 1, 'a:1:{i:61;a:6:{s:6:"mod_id";s:2:"61";s:6:"var_id";s:3:"217";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:2:"XS";s:5:"price";s:1:"0";}}'),
(435, '2015-04-15 11:05:04', NULL, 21, 0, 321, 603, 'AB0344-XS', 'BUSO SHOP  - XS', 99900, 1, 1, 'a:1:{i:61;a:6:{s:6:"mod_id";s:2:"61";s:6:"var_id";s:3:"217";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:2:"XS";s:5:"price";s:1:"0";}}'),
(436, '2015-04-15 11:07:21', NULL, 21, 0, 322, 517, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 1, 1, ''),
(437, '2015-04-15 11:07:47', NULL, 21, 0, 323, 517, 'AB0027', 'CAMISETA VISITANTE  2013 ', 44900, 1, 1, ''),
(438, '2015-04-20 13:33:40', NULL, 25, 0, 324, 524, 'AB0240', 'TOALLA ', 32900, 1, 1, ''),
(439, '2015-04-20 13:51:06', NULL, 25, 0, 325, 575, '', 'VINILO 30 x 50  HOLA DE STICKERS', 36300, 1, 1, ''),
(440, '2015-04-20 13:52:23', NULL, 25, 0, 326, 575, '', 'VINILO 30 x 50  HOLA DE STICKERS', 36300, 1, 1, ''),
(441, '2015-04-22 09:38:16', NULL, 10, 0, 327, 676, 'AB0376-3/', 'MINIKIT 2015 - 3/4', 140000, 1, 1, 'a:1:{i:83;a:6:{s:6:"mod_id";s:2:"83";s:6:"var_id";s:3:"290";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:3:"3/4";s:5:"price";s:1:"0";}}'),
(442, '2015-04-22 09:38:16', NULL, 10, 0, 327, 609, 'AB0379-L', 'CAMISETA OFICIAL 2015 MANGA LARGA  - L', 154900, 1, 1, 'a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"223";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"L";s:5:"price";s:1:"0";}}'),
(443, '2015-04-22 09:38:36', NULL, 10, 0, 328, 676, 'AB0376-3/', 'MINIKIT 2015 - 3/4', 140000, 1, 1, 'a:1:{i:83;a:6:{s:6:"mod_id";s:2:"83";s:6:"var_id";s:3:"290";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:3:"3/4";s:5:"price";s:1:"0";}}'),
(444, '2015-04-22 09:38:36', NULL, 10, 0, 328, 609, 'AB0379-L', 'CAMISETA OFICIAL 2015 MANGA LARGA  - L', 154900, 1, 1, 'a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"223";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"L";s:5:"price";s:1:"0";}}'),
(445, '2015-04-22 09:40:42', NULL, 10, 0, 329, 676, 'AB0376-3/', 'MINIKIT 2015 - 3/4', 140000, 1, 1, 'a:1:{i:83;a:6:{s:6:"mod_id";s:2:"83";s:6:"var_id";s:3:"290";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:3:"3/4";s:5:"price";s:1:"0";}}'),
(446, '2015-04-22 09:40:42', NULL, 10, 0, 329, 609, 'AB0379-L', 'CAMISETA OFICIAL 2015 MANGA LARGA  - L', 154900, 1, 1, 'a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"223";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"L";s:5:"price";s:1:"0";}}'),
(448, '2015-04-22 09:44:49', NULL, 10, 0, 330, 609, 'AB0379-L', 'CAMISETA OFICIAL 2015 MANGA LARGA  - L', 154900, 1, 1, 'a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"223";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"L";s:5:"price";s:1:"0";}}'),
(449, '2015-04-22 10:15:38', NULL, 10, 0, 331, 744, 'AB0399', 'RELOJ EDICION LIMITADA MILLONARIOS-BOMBERG', 1.16e+06, 1, 1, ''),
(450, '2015-04-22 10:15:45', NULL, 10, 0, 332, 744, 'AB0399', 'RELOJ EDICION LIMITADA MILLONARIOS-BOMBERG', 1.16e+06, 1, 1, ''),
(451, '2015-04-22 10:22:00', NULL, 10, 0, 333, 744, 'AB0399', 'RELOJ EDICION LIMITADA MILLONARIOS-BOMBERG', 1.16e+06, 1, 1, ''),
(452, '2015-04-22 10:34:07', NULL, 10, 0, 334, 610, 'AB0379-XL', 'CAMISETA OFICIAL 2015 MANGA LARGA  - XL', 154900, 1, 1, 'a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"224";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:2:"XL";s:5:"price";s:1:"0";}}'),
(453, '2015-04-22 10:34:07', NULL, 10, 0, 334, 608, 'AB0379-M', 'CAMISETA OFICIAL 2015 MANGA LARGA  - M', 154900, 1, 1, 'a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"222";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"M";s:5:"price";s:1:"0";}}'),
(455, '2015-04-22 10:34:21', NULL, 10, 0, 335, 608, 'AB0379-M', 'CAMISETA OFICIAL 2015 MANGA LARGA  - M', 154900, 1, 1, 'a:1:{i:62;a:6:{s:6:"mod_id";s:2:"62";s:6:"var_id";s:3:"222";s:4:"type";s:1:"1";s:5:"title";s:5:"TALLA";s:5:"value";s:1:"M";s:5:"price";s:1:"0";}}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_products`
--

CREATE TABLE IF NOT EXISTS `default_firesale_products` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `code` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(14,4) DEFAULT NULL,
  `price_tax` decimal(14,4) DEFAULT NULL,
  `rrp` decimal(14,4) DEFAULT '0.0000',
  `rrp_tax` decimal(14,4) DEFAULT '0.0000',
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `stock` int(11) DEFAULT NULL,
  `stock_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `ship_req` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `description` longtext COLLATE utf8_unicode_ci,
  `is_variation` tinyint(1) NOT NULL DEFAULT '0',
  `tax_band` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `digital` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'n',
  `download` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `downloads` int(11) DEFAULT NULL,
  `expires_after` int(11) DEFAULT NULL,
  `expires_interval` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8_unicode_ci,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_weight` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_height` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_width` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_depth` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descuento` int(11) DEFAULT NULL,
  `main_image` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title` (`title`,`description`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=745 ;

--
-- Volcado de datos para la tabla `default_firesale_products`
--

INSERT INTO `default_firesale_products` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `code`, `title`, `slug`, `price`, `price_tax`, `rrp`, `rrp_tax`, `status`, `stock`, `stock_status`, `ship_req`, `description`, `is_variation`, `tax_band`, `brand`, `digital`, `download`, `downloads`, `expires_after`, `expires_interval`, `meta_title`, `meta_description`, `meta_keywords`, `shipping_weight`, `shipping_height`, `shipping_width`, `shipping_depth`, `featured`, `descuento`, `main_image`) VALUES
(528, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0266', 'COJIN TV', 'cojin-tv', '46900.0000', '40431.0345', '0.0000', '0.0000', '1', 10, '1', '1', 'Cojin en fleece ', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1280', '70', '25', '80', '0', 0, 'dccb83a94082468'),
(537, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0335', 'AURICULARES ', 'auriculares', '24900.0000', '21465.5172', '0.0000', '0.0000', '1', 10, '1', '1', 'Auriculares Mfc', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,013', '1', '6', '13', '0', 0, '794e0b751c875ea'),
(538, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0337', 'JARRO DON CERVECERO ', 'jarro-don-cervecero', '49900.0000', '43017.2414', '0.0000', '0.0000', '1', 20, '1', '1', 'Jarro Cervezero en cristal italiano ', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1305', '20', '16', '11', '0', 0, '0ad7b0c45d6b551'),
(584, '2015-04-09 15:40:41', '2015-04-13 18:39:14', 10, NULL, 'AB0341-S', 'ESQUELETO ESTRELLA BLANCO - S', 'esqueleto-estrella-blanco-s', '44900.0000', '38706.9000', '0.0000', '0.0000', '1', 16, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0.5', '25', '36', '0', 0, 'ae5acd487b8bfff'),
(583, '2015-04-09 15:40:41', '2015-04-13 18:39:14', 10, NULL, 'AB0341-XS', 'ESQUELETO ESTRELLA BLANCO - XS', 'esqueleto-estrella-blanco-xs', '44900.0000', '38706.9000', '0.0000', '0.0000', '1', 16, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0.5', '25', '36', '0', 0, 'ae5acd487b8bfff'),
(534, '2015-04-09 15:40:41', '2015-04-14 20:17:45', 10, NULL, 'AB0306', 'CAMISETA VISITANTE 2015 MANGA LARGA 2015 ', 'camiseta-visitante-2015-manga-larga-2015', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta Visitante 2015 Manga Larga Talla S', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '1', 0, 'b13833139b21f03'),
(532, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0274', 'COOLER ', 'cooler', '33900.0000', '29224.1379', '0.0000', '0.0000', '1', 10, '1', '1', 'Cooler termico ', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,220', '2', '17', '40', '0', 0, 'f33c0afea427877'),
(533, '2015-04-09 15:40:41', '2015-04-14 19:06:12', 10, NULL, 'AB0302', 'CAMISETA VISITANTE 2015 MANGA CORTA ', 'camiseta-visitante-2015-manga-corta', '144900.0000', '124914.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Camiseta Visitante 2015 manga Corta Talla s', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '70f66b1584b3717'),
(531, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0273', 'COBIJA FLECEE', 'cobija-flecee', '32900.0000', '28362.0690', '0.0000', '0.0000', '1', 10, '1', '1', 'Cobija en fleece', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,540', '5', '20', '35', '0', 0, '02e4ff94242447c'),
(529, '2015-04-09 15:40:41', '2015-04-09 23:34:45', 10, NULL, 'AB0269', 'AUDIFONOS ', 'audifonos', '60000.0000', '51724.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'Audifonos con microfono y switch para contestar y colgar llamadas', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,340', '22', '10', '17', '1', 0, '07c5252226202fa'),
(524, '2015-04-09 15:40:40', NULL, 10, NULL, 'AB0240', 'TOALLA ', 'toalla', '32900.0000', '28362.0690', '0.0000', '0.0000', '1', 9, '2', '1', 'Toalla de 70 x 130', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,440', '4', '25', '25', '0', 0, '3007fee52df3771'),
(525, '2015-04-09 15:40:40', '2015-04-14 14:51:05', 10, NULL, 'AB0243', 'CUBRELECHO DOBLE ', 'cubrelecho-doble', '159900.0000', '137845.0000', '0.0000', '0.0000', '1', 7, '2', '1', 'COMFORT DOBLE CON DOS FUNDAS', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '2.2', '24', '45', '50', '0', 0, 'f23fd84cbb16448'),
(526, '2015-04-09 15:40:40', NULL, 10, NULL, 'AB0246', 'JUEGO DE SABANAS DOBLE ', 'juego-de-sabanas-doble', '92900.0000', '80086.2069', '0.0000', '0.0000', '1', 10, '1', '1', 'Juego de sabanas para cama doble', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1600', '7', '22', '30', '0', 0, '6e9548cb2f4f309'),
(523, '2015-04-09 15:40:40', '2015-04-15 17:16:58', 10, NULL, 'AB0221', 'BUSO TEJIDO MFC ', 'buso-shetland', '99400.0000', '85690.0000', '0.0000', '0.0000', '1', 20, '1', '1', 'Buso Tejido Cuello V Talla M', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '3', '31', '29', '0', 0, '12a4bccb147041d'),
(522, '2015-04-09 15:40:40', '2015-04-15 17:16:24', 10, NULL, 'AB0175', 'COJIN 45X45', 'cojin-45x45', '29900.0000', '25776.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'Cojin en fleece', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,420', '45', '15', '45', '0', 0, '49b8d7e56175a39'),
(521, '2015-04-09 15:40:40', '2015-04-14 19:09:31', 10, NULL, 'AB0096', 'SUDADERA DE ENTRENAMIENTO 2014', 'sudadera-de-entrenamiento-2014', '99000.0000', '85345.0000', '0.0000', '0.0000', '1', 32, '1', '1', 'Sudadera De entrenamiento Talla S', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'dec32d829a4cb34'),
(520, '2015-04-09 15:40:40', '2015-04-14 19:10:13', 10, NULL, 'AB0095', 'SUDADERA DE PRESENTACION 2014', 'sudadera-de-presentacion-2014', '99000.0000', '85345.0000', '0.0000', '0.0000', '1', 38, '1', '1', 'Sudadera Impermeable Talla S', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3ee6770b707ecf0'),
(518, '2015-04-09 15:40:40', '2015-04-18 21:20:38', 10, NULL, 'AB0031', 'CAMISETAS OFICIALES 2013', 'camiseta-oficial-2013', '39800.0000', '34310.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Corta Talla S', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, 'a040537e366eb48'),
(517, '2015-04-09 15:40:40', '2015-04-21 15:26:06', 10, NULL, 'AB0027', 'CAMISETA VISITANTE  2013 ', 'camiseta-visitante-2013', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 6, '2', '1', 'Camiseta visitante 2013 Manga Corta Talla S', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, '17c4986daf9e37d'),
(362, '2015-03-13 15:59:55', NULL, 10, NULL, 'AB0027-S', 'CAMISETA VISITANTE  2013  - S', 'camiseta-visitante-2013-s', '44900.0000', '38706.9000', '0.0000', '0.0000', '1', 14, '1', '1', 'Camiseta visitante 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,290', '1', '31', '29', '0', 0, '43ff56a27c001e0'),
(363, '2015-03-13 15:59:55', '2015-03-18 18:07:44', 10, NULL, 'AB0028', 'CAMISETA VISITANTE  2013  - M', 'camiseta-visitante-2013-m', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta visitante 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0.290', '1', '31', '29', '0', 0, '43ff56a27c001e0'),
(364, '2015-03-13 15:59:55', '2015-03-18 18:07:48', 10, NULL, 'AB0029', 'CAMISETA VISITANTE  2013  - L', 'camiseta-visitante-2013-l', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta visitante 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0.290', '1', '31', '29', '0', 0, '43ff56a27c001e0'),
(366, '2015-03-13 15:59:55', NULL, 10, NULL, 'AB0031-S', 'CAMISETA OFICIAL 2013 - S', 'camiseta-oficial-2013-s', '19900.0000', '17155.1700', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,290', '1', '31', '29', '0', 0, '37c4a006522c7f0'),
(367, '2015-03-13 15:59:55', '2015-03-13 16:19:07', 10, NULL, 'AB0032', 'CAMISETA OFICIAL 2013 - M', 'camiseta-oficial-2013-m', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, '37c4a006522c7f0'),
(368, '2015-03-13 15:59:55', '2015-03-13 16:19:13', 10, NULL, 'AB0033', 'CAMISETA OFICIAL 2013 - L', 'camiseta-oficial-2013-l', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, '37c4a006522c7f0'),
(369, '2015-03-13 15:59:55', '2015-03-13 16:19:19', 10, NULL, 'AB0034', 'CAMISETA OFICIAL 2013 - XL', 'camiseta-oficial-2013-xl', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, '37c4a006522c7f0'),
(38, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-7GR', 'Ballet Pumps - 7 Green', 'ballet-pumps-7-green', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(39, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-7MA', 'Ballet Pumps - 7 Magenta', 'ballet-pumps-7-magenta', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(40, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-7PU', 'Ballet Pumps - 7 Purple', 'ballet-pumps-7-purple', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(41, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-8RE', 'Ballet Pumps - 8 Red', 'ballet-pumps-8-red', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(42, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-8GR', 'Ballet Pumps - 8 Green', 'ballet-pumps-8-green', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(43, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-8MA', 'Ballet Pumps - 8 Magenta', 'ballet-pumps-8-magenta', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(44, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-8PU', 'Ballet Pumps - 8 Purple', 'ballet-pumps-8-purple', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(45, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-9RE', 'Ballet Pumps - 9 Red', 'ballet-pumps-9-red', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(46, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-9GR', 'Ballet Pumps - 9 Green', 'ballet-pumps-9-green', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(47, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-9MA', 'Ballet Pumps - 9 Magenta', 'ballet-pumps-9-magenta', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(48, '2013-07-18 18:35:03', '2013-07-18 18:35:29', 2, 11, 'PRD_AP0002-9PU', 'Ballet Pumps - 9 Purple', 'ballet-pumps-9-purple', '25.0000', '21.0000', '0.0000', '0.0000', '1', 120, '1', '1', 'ballet pump desc', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(49, '2013-07-18 04:25:43', '2013-07-19 01:15:26', 1, 1, 'PRD_C0002-TA', 'Holiday Shower Gel - talla 38', 'holiday-shower-gel-talla-38', '34810.0000', '30009.0000', '0.0000', '0.0000', '1', 250, '1', '1', 'Perfect size for travel, these fruit scented shower gels will keep you feeling fresh throughout summer.', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(54, '2013-07-18 04:25:43', '2014-02-11 23:27:35', 1, 1, 'PRD_C0002-RO', 'Holiday Shower Gel - rojo', 'holiday-shower-gel-rojo', '35800.0000', '30862.0000', '0.0000', '0.0000', '1', 250, '1', '1', '<div style="text-align: center;"><em><strong>hola</strong></em><br /><br /> </div><div style="text-align: justify;"><table border="1" cellpadding="1" cellspacing="1" style="width: 500px;"> <tbody>  <tr>   <td>asfdasdf</td>   <td>dfgsdfgsd</td>  </tr>  <tr>   <td>sdfgsdfg</td>   <td>sdfgdsfg</td>  </tr>  <tr>   <td>sdfgdsfg</td>   <td>fdghfdgh</td>  </tr> </tbody></table></div>', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(90, '2014-10-10 15:27:23', NULL, 10, 15, 'prueba-S', 'prueba - S', 'prueba-s', '11440.0000', '9862.0700', '0.0000', '0.0000', '1', 100, '1', '1', 'prueba', 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(87, '2014-06-04 15:44:17', '2014-06-25 21:52:08', 10, 14, '1234587-AL', 'Alcancia Imaginamos - Alcancia pequeña', 'alcancia-imaginamos-alcancia-pequena', '20.4000', '17.5862', '0.0000', '0.0000', '1', 0, '3', '1', 'Alcancia de Imaginamos S.A', 1, 1, NULL, 'n', NULL, NULL, NULL, '', 'Imaginamos', 'Alcancia Imaginamos', 'alcancia,imaginamos', NULL, NULL, NULL, NULL, '1', NULL, NULL),
(91, '2014-10-10 15:27:23', NULL, 10, 15, 'prueba-SES', 'prueba - S Esqueleto', 'prueba-s-esqueleto', '13440.0000', '11586.2100', '0.0000', '0.0000', '1', 100, '1', '1', 'prueba', 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(92, '2014-10-10 15:27:23', NULL, 10, 15, 'prueba-SESRO', 'prueba - S Esqueleto Rojo', 'prueba-s-esqueleto-rojo', '14440.0000', '12448.2800', '0.0000', '0.0000', '1', 100, '1', '1', 'prueba', 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(539, '2015-04-09 15:40:41', '2015-04-14 15:55:58', 10, NULL, 'AB0344', 'BUSO SHOP ', 'buso-shop', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'Sweter Estampado Con Mangas en Pu Talla S', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,600', '2', '25', '36', '1', 0, '35f0371274d2833'),
(94, '2014-10-14 21:59:53', '2014-10-15 00:20:10', 10, 16, '71217-RO', 'Running Nike Dart 10 msl Negro-Rojo - Rojo', 'running-nike-dart-10-msl-negrorojo-rojo', '232.0040', '200.0040', '0.0000', '0.0000', '1', 100, '1', '1', '<div><em>Running </em>Nike <em>Dart 10 msl </em>color negro/rojo,  Elaborado en textil. Su diseño cuenta con aplique contramarcado de alto contraste en los laterales y refuerzo en material sintético para mejor ajuste al pie. Posee capellada tipo malla para mayor frescura. Tiene punta redonda, empeine acordonado, collarín moldeado, forro en textil,  suela<em>reslon </em>en Eva y caucho para una excelente amortiguación.<br />\r\n<br />\r\n </div>\r\n\r\n<div>INFORMACIÓN\r\n<table style="width:478px;">\r\n <tbody>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Sexo</td>\r\n   <td style="line-height:22px;">Masculino</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Material exterior</td>\r\n   <td style="line-height:22px;">Sintético</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Material interior</td>\r\n   <td style="line-height:22px;">POLYESTER</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Color</td>\r\n   <td style="line-height:22px;">negro/rojo</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Descripción corta</td>\r\n   <td style="line-height:22px;"><em>Running </em>Nike <em>Dart 10 msl </em>color negro/rojo  Sólo en Dafiti, tu tienda de moda online.</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Actividad</td>\r\n   <td style="line-height:22px;">training</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Código Artículo</td>\r\n   <td style="line-height:22px;">NI010SH82BJDCO</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 20, NULL),
(95, '2014-10-14 21:59:53', NULL, 10, 16, '71217-AM', 'Running Nike Dart 10 msl Negro-Rojo - Amarillo', 'running-nike-dart-10-msl-negrorojo-amarillo', '232.0040', '200.0040', '0.0000', '0.0000', '1', 98, '1', '1', '<div><em>Running </em>Nike <em>Dart 10 msl </em>color negro/rojo,  Elaborado en textil. Su diseño cuenta con aplique contramarcado de alto contraste en los laterales y refuerzo en material sintético para mejor ajuste al pie. Posee capellada tipo malla para mayor frescura. Tiene punta redonda, empeine acordonado, collarín moldeado, forro en textil,  suela<em>reslon </em>en Eva y caucho para una excelente amortiguación.<br />\r\n<br />\r\n </div>\r\n\r\n<div>INFORMACIÓN\r\n<table style="width:478px;">\r\n <tbody>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Sexo</td>\r\n   <td style="line-height:22px;">Masculino</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Material exterior</td>\r\n   <td style="line-height:22px;">Sintético</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Material interior</td>\r\n   <td style="line-height:22px;">POLYESTER</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Color</td>\r\n   <td style="line-height:22px;">negro/rojo</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Descripción corta</td>\r\n   <td style="line-height:22px;"><em>Running </em>Nike <em>Dart 10 msl </em>color negro/rojo  Sólo en Dafiti, tu tienda de moda online.</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Actividad</td>\r\n   <td style="line-height:22px;">training</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Código Artículo</td>\r\n   <td style="line-height:22px;">NI010SH82BJDCO</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>', 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 20, NULL),
(96, '2014-10-14 21:59:53', NULL, 10, 16, '71217-VE', 'Running Nike Dart 10 msl Negro-Rojo - Verde', 'running-nike-dart-10-msl-negrorojo-verde', '232.0040', '200.0040', '0.0000', '0.0000', '1', 100, '1', '1', '<div><em>Running </em>Nike <em>Dart 10 msl </em>color negro/rojo,  Elaborado en textil. Su diseño cuenta con aplique contramarcado de alto contraste en los laterales y refuerzo en material sintético para mejor ajuste al pie. Posee capellada tipo malla para mayor frescura. Tiene punta redonda, empeine acordonado, collarín moldeado, forro en textil,  suela<em>reslon </em>en Eva y caucho para una excelente amortiguación.<br />\r\n<br />\r\n </div>\r\n\r\n<div>INFORMACIÓN\r\n<table style="width:478px;">\r\n <tbody>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Sexo</td>\r\n   <td style="line-height:22px;">Masculino</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Material exterior</td>\r\n   <td style="line-height:22px;">Sintético</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Material interior</td>\r\n   <td style="line-height:22px;">POLYESTER</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Color</td>\r\n   <td style="line-height:22px;">negro/rojo</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Descripción corta</td>\r\n   <td style="line-height:22px;"><em>Running </em>Nike <em>Dart 10 msl </em>color negro/rojo  Sólo en Dafiti, tu tienda de moda online.</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Actividad</td>\r\n   <td style="line-height:22px;">training</td>\r\n  </tr>\r\n  <tr>\r\n   <td style="line-height:22px;width:1px;">Código Artículo</td>\r\n   <td style="line-height:22px;">NI010SH82BJDCO</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>', 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 20, NULL),
(99, '2014-11-21 16:40:14', NULL, 10, 18, 'MAL124-RO', 'Maletin Coco - Rojo', 'maletin-coco-rojo', '650000.0000', '560344.8300', '0.0000', '0.0000', '1', 5, '1', '1', NULL, 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(100, '2014-11-21 16:40:14', NULL, 10, 18, 'MAL124-AZ', 'Maletin Coco - azul', 'maletin-coco-azul', '639000.0000', '550862.0700', '0.0000', '0.0000', '1', 5, '1', '1', NULL, 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(101, '2014-11-21 16:40:14', NULL, 10, 18, 'MAL124-ROXS', 'Maletin Coco - Rojo XS', 'maletin-coco-rojo-xs', '650000.0000', '560344.8300', '0.0000', '0.0000', '1', 5, '1', '1', NULL, 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(102, '2014-11-21 16:40:14', NULL, 10, 18, 'MAL124-AZXS', 'Maletin Coco - azul XS', 'maletin-coco-azul-xs', '639000.0000', '550862.0700', '0.0000', '0.0000', '1', 5, '1', '1', NULL, 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(103, '2014-11-21 16:40:14', NULL, 10, 18, 'MAL124-ROS', 'Maletin Coco - Rojo S', 'maletin-coco-rojo-s', '650000.0000', '560344.8300', '0.0000', '0.0000', '1', 5, '1', '1', NULL, 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(104, '2014-11-21 16:40:14', NULL, 10, 18, 'MAL124-AZS', 'Maletin Coco - azul S', 'maletin-coco-azul-s', '639000.0000', '550862.0700', '0.0000', '0.0000', '1', 5, '1', '1', NULL, 1, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL),
(105, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-XS', 'Holiday Shower Gel - xs', 'holiday-shower-gel-xs', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(106, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-S', 'Holiday Shower Gel - s', 'holiday-shower-gel-s', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(107, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-M', 'Holiday Shower Gel - m', 'holiday-shower-gel-m', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(108, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-L', 'Holiday Shower Gel - l', 'holiday-shower-gel-l', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(109, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-XSRO', 'Holiday Shower Gel - xs Rojo', 'holiday-shower-gel-xs-rojo', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(110, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-SRO', 'Holiday Shower Gel - s Rojo', 'holiday-shower-gel-s-rojo', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(111, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-MRO', 'Holiday Shower Gel - m Rojo', 'holiday-shower-gel-m-rojo', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(112, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-LRO', 'Holiday Shower Gel - l Rojo', 'holiday-shower-gel-l-rojo', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(113, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-XSAZ', 'Holiday Shower Gel - xs Azul', 'holiday-shower-gel-xs-rojo', '25369.2000', '21870.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(114, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-SAZ', 'Holiday Shower Gel - s Azul', 'holiday-shower-gel-s-rojo', '25369.2000', '21870.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(115, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-MAZ', 'Holiday Shower Gel - m Azul', 'holiday-shower-gel-m-rojo', '25369.2000', '21870.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(116, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-LAZ', 'Holiday Shower Gel - l Azul', 'holiday-shower-gel-l-rojo', '25369.2000', '21870.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(117, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-XSVE', 'Holiday Shower Gel - xs Verde', 'holiday-shower-gel-xs-verde', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(118, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-SVE', 'Holiday Shower Gel - s Verde', 'holiday-shower-gel-s-verde', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(119, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-MVE', 'Holiday Shower Gel - m Verde', 'holiday-shower-gel-m-verde', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(120, '2013-07-18 04:25:43', '2014-09-30 15:27:49', 1, 1, 'PRD_C0002-LVE', 'Holiday Shower Gel - l Verde', 'holiday-shower-gel-l-verde', '31320.0000', '27000.0000', '0.0000', '0.0000', '1', 3999, '1', '0', '<div style="text-align: center;"><em><strong>hola</strong></em><br />\r\n<br />\r\n </div>\r\n\r\n<div style="text-align: justify;">\r\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n <tbody>\r\n  <tr>\r\n   <td>asfdasdf</td>\r\n   <td>dfgsdfgsd</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgsdfg</td>\r\n   <td>sdfgdsfg</td>\r\n  </tr>\r\n  <tr>\r\n   <td>sdfgdsfg</td>\r\n   <td>fdghfdgh</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', 10, NULL),
(137, '2014-11-21 16:10:52', '2015-01-20 17:19:19', 10, 17, 'CH123-RO', 'CHAQUETA DOBLE FAZ - Rojo', 'chaqueta-doble-faz-rojo', '385700.0000', '332500.0000', '0.0000', '0.0000', '1', 74, '1', '1', 'Nec elit eleifend augue posuere sollicitudin vitae vel varius blandit cras porta porta felis, non elementum nunc fringilla consectetur vitae praesent ut odio sodales, fermentum massa sed lacinia arcu.', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', 5, '96a4ba2973ade2c'),
(138, '2014-11-21 16:10:52', '2015-01-20 17:19:19', 10, 17, 'CH123-VE', 'CHAQUETA DOBLE FAZ - Verde', 'chaqueta-doble-faz-rojo', '366415.0000', '315875.0000', '0.0000', '0.0000', '1', 75, '1', '1', 'Nec elit eleifend augue posuere sollicitudin vitae vel varius blandit cras porta porta felis, non elementum nunc fringilla consectetur vitae praesent ut odio sodales, fermentum massa sed lacinia arcu.', 1, 1, NULL, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', 5, '96a4ba2973ade2c'),
(744, '2015-04-20 21:55:00', '2015-04-21 20:31:20', 13, 26, 'AB0399', 'RELOJ EDICION LIMITADA MILLONARIOS-BOMBERG', 'reloj-edicion-limitada-millonariosbomberg', '1160000.0000', '1000000.0000', '0.0000', '0.0000', '1', 5, '1', '1', '', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '1', NULL, 'e0786047d574f11'),
(743, '2015-04-20 21:34:55', '2015-04-20 21:40:38', 13, 25, 'AB0374', 'KIT CUADERNO OFICIO Y CARTUCHERA MFC', 'kit-cuaderno-oficio-y-cartuchera-mfc', '32800.0000', '28276.0000', '0.0000', '0.0000', '1', 10, '1', '1', '', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, 'e679e24078c983e'),
(541, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0354', 'OSO MFC ', 'oso-mfc', '49900.0000', '43017.2414', '0.0000', '0.0000', '1', 5, '1', '1', 'Oso Pequeo ', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,350', '16', '30', '32', '0', 0, 'c5b2c920b9256e3'),
(542, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0355', 'OSO MFC ', 'oso-mfc', '59900.0000', '51637.9310', '0.0000', '0.0000', '1', 5, '1', '1', 'Oso Grande ', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,450', '20', '38', '40', '0', 0, '1fbabbe8a2b0701'),
(543, '2015-04-09 15:40:41', '2015-04-14 16:03:00', 10, NULL, 'AB0360', 'CAMISETA PASION DE MILLONES ', 'camiseta-pasion-de-millones', '38500.0000', '33190.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Pasion De Millones Talla M', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '1', '31', '29', '0', 0, 'c4a21fc4ad11561'),
(544, '2015-04-09 15:40:41', '2015-04-14 16:37:44', 10, NULL, 'AB0378', 'CAMISETA OFICIAL 2015 MANGA CORTA ', 'camiseta-oficial-2015-manga-corta', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 14, '1', '1', 'Camiseta oficial 2015 manga corta Talla s', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '1c2c95e23d2b5a6'),
(545, '2015-04-09 15:40:41', '2015-04-22 09:45:39', 10, NULL, 'AB0379', 'CAMISETA OFICIAL 2015 MANGA LARGA ', 'camiseta-oficial-2015-manga-larga', '154900.0000', '133534.0000', '0.0000', '0.0000', '1', 13, '1', '1', 'Camiseta Oficial 2015 manga larga talla s', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '1', '31', '29', '1', 0, '94f5df765f3a33f'),
(546, '2015-04-09 15:40:41', '2015-04-21 20:23:14', 10, NULL, 'AB0384', 'CAMISETA ENTRENAMIENTO 2015 BLANCA ', 'camiseta-entrenamiento-2015-blanca', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 16, '1', '1', 'Camiseta de entrenamiento AdiZero', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '1', 0, '2b176791fff8980'),
(547, '2015-04-09 15:40:41', '2015-04-14 16:22:49', 10, NULL, 'AB0385', 'CAMISETA ENTRENAMIENTO 2015 AZUL ', 'camiseta-entrenamiento-2015-azul', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'Camiseta de entrenamiento AdiZero', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, 'b255d5a303cbd6c'),
(549, '2015-04-09 15:40:41', '2015-04-14 17:18:03', 10, NULL, 'AB0392', 'CHAQUETA TIPO FILLAT 2015 ', 'chaqueta-tipo-fillat-2015', '259900.0000', '224052.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Chaqueta de tipo gaban', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '1000', '18', '38', '38', '0', 0, 'a37396e68e736dd'),
(550, '2015-04-09 15:40:41', NULL, 10, NULL, NULL, 'Oficial dama 2015', 'oficial-dama-2015', NULL, NULL, '0.0000', '0.0000', '1', NULL, '1', '1', '', 0, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL),
(551, '2015-04-09 15:40:41', NULL, 10, NULL, NULL, 'Minikit 2015', 'minikit-2015', NULL, NULL, '0.0000', '0.0000', '1', NULL, '1', '1', '', 0, 1, NULL, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL),
(552, '2015-04-09 15:40:41', '2015-04-14 18:57:01', 10, NULL, 'AB0377', 'OFICIAL DAMA 2015', 'oficial-dama-2015', '119900.0000', '103362.0000', '0.0000', '0.0000', '1', 4, '2', '1', 'Oficial dama 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '1', 0, '41b834dbcd3e16e'),
(553, '2015-04-09 15:40:41', '2015-04-21 15:45:10', 10, NULL, 'AB0376', 'MINIKIT 2015', 'minikit-2015', '140000.0000', '120690.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Minikit 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '1', 0, 'dd747fa492d5426'),
(554, '2015-04-09 15:40:41', '2015-04-14 19:23:43', 10, NULL, 'AB0381', 'PANTALONETA OFICIAL 2015', 'pantaloneta-oficial-2015', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 16, '1', '1', 'Pantaloneta oficial 2105', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '4a015f26e9747b0'),
(555, '2015-04-09 15:40:41', '2015-04-14 19:31:41', 10, NULL, 'AB0382', 'PANTALONETA VISITANTE 2015', 'pantaloneta-visitante-2015', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'Pantaloneta visitante 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '72d15b7e3bc1714'),
(556, '2015-04-09 15:40:41', '2015-04-14 19:47:56', 10, NULL, 'AB0386', 'POLO BLANCA 2015', 'polo-blanca-2015', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Polo blanca 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '0', 0, '49a5d52eefeb29d'),
(557, '2015-04-09 15:40:41', '2015-04-14 17:03:42', 10, NULL, 'AB0387', 'POLO AZUL REY 2015', 'polo-azul-rey-2015', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'Polo azul rey 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '1', 0, '135ca4319e9475b'),
(558, '2015-04-09 15:40:41', '2015-04-14 19:22:41', 10, NULL, 'AB0388', 'PANTALONETA ENTRENO 2015', 'pantaloneta-entreno-2015', '64900.0000', '55948.0000', '0.0000', '0.0000', '1', 12, '1', '1', 'Pantaloneta entreno 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, 'cab06c413b053ac'),
(559, '2015-04-09 15:40:41', '2015-04-15 09:53:26', 10, NULL, 'AB0390', 'ROMPEVIENTOS 2015', 'rompevientos-2015', '159900.0000', '137845.0000', '0.0000', '0.0000', '1', 0, '1', '1', 'Rompevientos 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,520', '4', '40', '29', '0', 0, 'f80ca912c787222'),
(560, '2015-04-09 15:40:41', '2015-04-14 16:18:53', 10, NULL, 'AB0393', 'SUDADERA ENTRENO 2015', 'sudadera-entreno-2015', '174900.0000', '150776.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera entreno 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3fed7e54b23e980'),
(561, '2015-04-09 15:40:41', '2015-04-15 09:24:26', 10, NULL, 'AB0394', 'SUDADERA PRESENTACION 2015', 'sudadera-presentacion-2015', '199900.0000', '172328.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera presentacion 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'd1ee21d7c98a6e5'),
(562, '2015-04-09 15:40:41', '2015-04-21 15:42:54', 10, NULL, 'AB0396', 'CHAQUETA PRESENTACION 2015', 'chaqueta-presentacion-2015', '169900.0000', '146466.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Chaqueta presentacion 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '2', '38', '29', '0', 0, '3001145f421fefc'),
(563, '2015-04-09 15:40:41', '2015-04-14 16:45:56', 10, NULL, 'AB0380', 'CAMISETA OFICIAL NIOS 2015', 'camiseta-oficial-ni-os-2015', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'camiseta oficial nios 2015', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '30', '28', '0', 0, '1c2c95e23d2b5a6'),
(564, '2015-04-09 15:40:41', '2015-04-14 18:58:46', 10, NULL, 'AB0339', 'PANTALON TIPO YOGA GRIS JASPE', 'pantalon-tipo-yoga-gris-jaspe', '59900.0000', '51638.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'PANTALON TIPO YOGA GRIS JASPE', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '480', '1', '25', '36', '0', 0, '4f0aaec1628aaca'),
(565, '2015-04-09 15:40:41', '2015-04-09 22:25:53', 10, NULL, 'AB0345', 'T SHIRT OVER CON ESTAMPADO GRIS', 't-shirt-over-con-estampado-gris', '48900.0000', '42155.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'T SHIRT OVER CON ESTAMPADO GRIS', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '370', '0,5', '25', '36', '0', 0, '5da92cbcf9eb0c7'),
(566, '2015-04-09 15:40:41', '2015-04-14 16:52:55', 10, NULL, 'AB0347', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 'leggins-estanpado-foil-bicolor-azul', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '477bddeedba95fb'),
(567, '2015-04-09 15:40:41', '2015-04-14 16:54:47', 10, NULL, 'AB0348', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 'leggins-estanpado-foil-bicolor-negro', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 11, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'a8b4ad187110b8f'),
(568, '2015-04-09 15:40:41', '2015-04-14 16:52:03', 10, NULL, 'AB0340', 'ESQUELETO ESTRELLA GRIS', 'esqueleto-estrella-gris', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 21, '1', '1', 'ESQUELETO ESTRELLA', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0,5', '25', '36', '0', 0, '902f9a35c6a7fab'),
(569, '2015-04-09 15:40:41', '2015-04-14 16:51:16', 10, NULL, 'AB0341', 'ESQUELETO ESTRELLA BLANCO', 'esqueleto-estrella-blanco', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 16, '1', '1', 'ESQUELETO ESTRELLA', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0.5', '25', '36', '0', 0, 'ae5acd487b8bfff'),
(570, '2015-04-09 15:40:41', '2015-04-09 22:26:34', 10, NULL, 'AB0352', 'SUETER CAPOTA STYLE GRIS', 'sueter-capota-style-gris', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'SUETER CAPOTA STYLE GRIS', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '400', '1', '25', '36', '0', 0, '498e1e5ca0dd2de'),
(571, '2015-04-09 15:40:41', '2015-04-21 21:39:01', 10, NULL, 'AB0350', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 'camiseta-estampada-m-c-bicolor-blanco', '40000.0000', '34483.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'efa7f567e7dbe25'),
(572, '2015-04-09 15:40:41', '2015-04-21 21:49:30', 10, NULL, 'AB0349', 'CAMISETA ESTAMPADA MC AZUL REY', 'camiseta-estampada-m-c-azul-rey', '40000.0000', '34483.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'CAMISETA ESTAMPADA MC AZUL REY', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'c0f857ea44ea79d'),
(573, '2015-04-09 15:40:41', '2015-04-21 15:49:31', 10, NULL, 'AB0223', 'SWEATER CUELLO EN V MUJER MFC', 'sweater-cuello-en-v-mujer-mfc', '83000.0000', '71552.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'SWEATER CUELLO EN V MUJER MFC', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '200', '2', '25', '36', '0', 0, 'be17bb20dcee6b0'),
(574, '2015-04-09 15:40:41', NULL, 10, NULL, NULL, 'VINILO FRASES   HOJA DE STICKERS ', 'vinilo-frases-hoja-de-stickers', '36300.0000', '31293.1034', '0.0000', '0.0000', '1', 5, '1', '1', 'Vinilo decorativo frase  De Millos nada mas   hoja carta de stickers ', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.23', '30', '6.5', '6.5', '0', 0, 'c69846791a78cab'),
(575, '2015-04-09 15:40:41', NULL, 10, NULL, NULL, 'VINILO 30 x 50  HOLA DE STICKERS', 'vinilo-30-x-50-hola-de-stickers', '36300.0000', '31293.1034', '0.0000', '0.0000', '1', 5, '1', '1', 'Vinilo decorativo escudos  hoja de stickers ', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.23', '30', '6.5', '6.5', '0', 0, '0f5e55d8e29a465'),
(576, '2015-04-09 15:40:41', NULL, 10, NULL, NULL, 'COJIN 45 x 45  TAPETE 60 x 40', 'cojin-45-x-45-tapete-60-x-40', '54800.0000', '47241.3793', '0.0000', '0.0000', '1', 5, '1', '1', 'Cojin de 45 x 45 cm  Tapete azul rey', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.45', '45', '45', '30', '0', 0, '0c5044cfeb53e92'),
(578, '2015-04-09 15:40:41', NULL, 10, NULL, NULL, 'PROTECTOR PARA IPHONE 5s  AURICULARES MFC', 'protector-para-iphone-5s-auriculares-mfc', '44900.0000', '38706.8966', '0.0000', '0.0000', '1', 5, '1', '1', 'Protector para Iphone 5s  auriculares Mfc ', 0, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.05', '13', '6', '5', '0', 0, '005a4fd7cfde6e2'),
(579, '2015-04-09 15:40:41', '2015-04-14 15:04:48', 10, NULL, 'protector', 'PROTECTOR PARA SAMSUNG S4  AURICULARES MFC', 'protector-para-samsung-s4-auriculares-mfc', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Protector para Samsung S4 auriculares Mfc', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0.05', '13', '6', '5', '0', 0, 'b898944e87befcd'),
(370, '2015-03-13 15:59:55', NULL, 10, NULL, 'AB0036-M', 'CAMISETA OFICIAL 2013 - M', 'camiseta-oficial-2013-s', '19900.0000', '17155.1700', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Larga Talla M', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,290', '1', '31', '29', '0', 0, '6f8aab08b71f704'),
(372, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0095-S', 'SUDADERA DE PRESENTACION 2014 - S', 'sudadera-de-presentacion-2014-s', '110000.0000', '94827.5900', '0.0000', '0.0000', '1', 10, '1', '1', 'Sudadera Impermeable Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,980', '4', '40', '29', '0', 0, 'eccd93250ff0f9a'),
(375, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0095-L', 'SUDADERA DE PRESENTACION 2014 - L', 'sudadera-de-presentacion-2014-l', '110000.0000', '94827.5900', '0.0000', '0.0000', '1', 10, '1', '1', 'Sudadera Impermeable Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,980', '4', '40', '29', '0', 0, 'eccd93250ff0f9a'),
(374, '2015-03-13 15:59:56', '2015-03-13 16:30:40', 10, NULL, 'AB0095-M', 'SUDADERA DE PRESENTACION 2014 - M', 'sudadera-de-presentacion-2014-m', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'Sudadera Impermeable Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'eccd93250ff0f9a');
INSERT INTO `default_firesale_products` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `code`, `title`, `slug`, `price`, `price_tax`, `rrp`, `rrp_tax`, `status`, `stock`, `stock_status`, `ship_req`, `description`, `is_variation`, `tax_band`, `brand`, `digital`, `download`, `downloads`, `expires_after`, `expires_interval`, `meta_title`, `meta_description`, `meta_keywords`, `shipping_weight`, `shipping_height`, `shipping_width`, `shipping_depth`, `featured`, `descuento`, `main_image`) VALUES
(376, '2015-03-13 15:59:56', '2015-03-13 16:32:36', 10, NULL, 'AB0096-M', 'SUDADERA DE ENTRENAMIENTO 2014 - M', 'sudadera-de-entrenamiento-2014-m', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Sudadera De entrenamiento Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '6268c6dcbbc2edc'),
(377, '2015-03-13 15:59:56', '2015-03-13 16:35:04', 10, NULL, 'AB0096-L', 'SUDADERA DE ENTRENAMIENTO 2014 - L', 'sudadera-de-entrenamiento-2014-l', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 16, '1', '1', 'Sudadera De entrenamiento Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '6268c6dcbbc2edc'),
(378, '2015-03-13 15:59:56', '2015-03-13 16:35:45', 10, NULL, 'AB0096-XL', 'SUDADERA DE ENTRENAMIENTO 2014 - XL', 'sudadera-de-entrenamiento-2014-xl', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Sudadera De entrenamiento Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '6268c6dcbbc2edc'),
(379, '2015-03-13 15:59:56', '2015-03-13 16:39:41', 10, NULL, 'AB0221-S', 'BUSO SHETLAND  - S', 'buso-shetland-s', '99400.0000', '85690.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'Buso Tejido Cuello V Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '3', '31', '29', '0', 0, '266065ce0665d12'),
(380, '2015-03-13 15:59:56', '2015-03-13 16:42:53', 10, NULL, 'AB0221-M', 'BUSO SHETLAND  - M', 'buso-shetland-m', '99400.0000', '85690.0000', '0.0000', '0.0000', '1', 9, '1', '1', 'Buso Tejido Cuello V Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '3', '31', '29', '0', 0, '266065ce0665d12'),
(381, '2015-03-13 15:59:56', '2015-03-13 16:43:34', 10, NULL, 'AB0221-L', 'BUSO SHETLAND  - L', 'buso-shetland-l', '99400.0000', '85690.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'Buso Tejido Cuello V Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '3', '31', '29', '0', 0, '266065ce0665d12'),
(382, '2015-03-13 15:59:56', '2015-03-13 16:42:08', 10, NULL, 'AB0221-XL', 'BUSO SHETLAND  - XL', 'buso-shetland-xl', '99400.0000', '85690.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Buso Tejido Cuello V Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '3', '31', '29', '0', 0, '266065ce0665d12'),
(383, '2015-03-13 15:59:56', '2015-04-04 19:10:14', 10, NULL, 'AB0302-S', 'CAMISETA VISITANTE 2015 MANGA CORTA  - S', 'camiseta-visitante-2015-manga-corta-s', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 7, '1', '1', 'Camiseta Visitante 2015 manga Corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '0d951e415f65632'),
(384, '2015-03-13 15:59:56', '2015-04-04 19:10:34', 10, NULL, 'AB0303-M', 'CAMISETA VISITANTE 2015 MANGA CORTA  - M', 'camiseta-visitante-2015-manga-corta-m', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 24, '1', '1', 'Camiseta Visitante 2015 manga Corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '0d951e415f65632'),
(392, '2015-03-13 15:59:56', '2015-04-04 19:10:49', 10, NULL, 'AB0304-L', 'CAMISETA VISITANTE 2015 MANGA CORTA  - L', 'camiseta-visitante-2015-manga-corta-l', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'Camiseta Visitante 2015 manga Corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '0d951e415f65632'),
(393, '2015-03-13 15:59:56', '2015-04-04 19:11:06', 10, NULL, 'AB0305-XL', 'CAMISETA VISITANTE 2015 MANGA CORTA  - XL', 'camiseta-visitante-2015-manga-corta-xl', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'Camiseta Visitante 2015 manga Corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '0d951e415f65632'),
(387, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0306-S', 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - S', 'camiseta-visitante-2015-manga-larga-2015-s', '154900.0000', '133534.4800', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta Visitante 2015  Manga Larga Talla S ', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,200', '1', '31', '29', '0', 0, '9338db0732c2e7e'),
(394, '2015-03-13 15:59:56', '2015-03-13 17:12:30', 10, NULL, 'AB0344-S', 'BUSO SHOP  - S', 'buso-shop-s', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 8, '1', '1', 'Sweter Estampado Con Mangas en Pu Talla S', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,600', '2', '25', '36', '0', 0, '7bb1a0c9b9c273a'),
(389, '2015-03-13 15:59:56', '2015-03-13 17:02:00', 10, NULL, 'AB0307-M', 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - M', 'camiseta-visitante-2015-manga-larga-2015-m', '154899.0000', '133534.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta Visitante 2015 Manga Larga Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '9338db0732c2e7e'),
(390, '2015-03-13 15:59:56', '2015-03-13 16:58:55', 10, NULL, 'AB0308-L', 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - L', 'camiseta-visitante-2015-manga-larga-2015-l', '154899.0000', '133534.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Camiseta Visitante 2015 Manga Larga Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '9338db0732c2e7e'),
(391, '2015-03-13 15:59:56', '2015-03-13 16:59:56', 10, NULL, 'AB0309-XL', 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - XL', 'camiseta-visitante-2015-manga-larga-2015-xl', '154899.0000', '133534.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Camiseta Visitante 2015 Manga Larga Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '9338db0732c2e7e'),
(396, '2015-03-13 15:59:56', '2015-03-19 15:55:24', 10, NULL, 'AB0344-M', 'BUSO SHOP  - M', 'buso-shop-m', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'Sweter Estampado Con Mangas en Pu Talla S', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,600', '2', '25', '36', '0', 0, '7bb1a0c9b9c273a'),
(397, '2015-03-13 15:59:56', '2015-03-13 17:13:54', 10, NULL, 'AB0344-L', 'BUSO SHOP  - L', 'buso-shop-l', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Sweter Estampado Con Mangas en Pu Talla S', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,600', '2', '25', '36', '0', 0, '7bb1a0c9b9c273a'),
(398, '2015-03-13 15:59:56', '2015-04-04 19:08:17', 10, NULL, 'AB0346-S', 'T SHIRT OVER CON ESTAMPADO CAMPIN - S', 't-shirt-over-con-estampado-campin-s', '48900.0000', '42155.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'Camiseta Manga Larga Gris Talla 4', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,370', '1', '25', '36', '0', 0, '555a3444c261974'),
(399, '2015-03-13 15:59:56', '2015-04-04 19:08:32', 10, NULL, 'AB0346-M', 'T SHIRT OVER CON ESTAMPADO CAMPIN - M', 't-shirt-over-con-estampado-campin-m', '48900.0000', '42155.0000', '0.0000', '0.0000', '1', 12, '1', '1', 'Camiseta Manga Larga Gris Talla 4', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,370', '1', '25', '36', '0', 0, '555a3444c261974'),
(400, '2015-03-13 15:59:56', '2015-04-04 19:08:45', 10, NULL, 'AB0346-L', 'T SHIRT OVER CON ESTAMPADO CAMPIN - L', 't-shirt-over-con-estampado-campin-l', '48900.0000', '42155.0000', '0.0000', '0.0000', '1', 12, '1', '1', 'Camiseta Manga Larga Gris Talla 4', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,370', '1', '25', '36', '0', 0, '555a3444c261974'),
(401, '2015-03-13 15:59:56', '2015-03-13 17:19:13', 10, NULL, 'AB0360-S', 'CAMISETA PASION DE MILLONES  - S', 'camiseta-pasion-de-millones-s', '38500.0000', '33190.0000', '0.0000', '0.0000', '1', 20, '1', '1', 'Camiseta Pasion De Millones Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '1', '31', '29', '0', 0, '9517fd0bf8faa65'),
(402, '2015-03-13 15:59:56', '2015-03-13 17:19:56', 10, NULL, 'AB0360-M', 'CAMISETA PASION DE MILLONES  - M', 'camiseta-pasion-de-millones-m', '38500.0000', '33190.0000', '0.0000', '0.0000', '1', 46, '1', '1', 'Camiseta Pasion De Millones Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '1', '31', '29', '0', 0, '9517fd0bf8faa65'),
(403, '2015-03-13 15:59:56', '2015-03-13 17:20:10', 10, NULL, 'AB0360-L', 'CAMISETA PASION DE MILLONES  - L', 'camiseta-pasion-de-millones-l', '38500.0000', '33190.0000', '0.0000', '0.0000', '1', 30, '1', '1', 'Camiseta Pasion De Millones Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '1', '31', '29', '0', 0, '9517fd0bf8faa65'),
(404, '2015-03-13 15:59:56', '2015-03-13 17:23:44', 10, NULL, 'AB0378-S', 'CAMISETA OFICIAL 2015 MANGA CORTA  - S', 'camiseta-oficial-2015-manga-corta-s', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 20, '1', '1', 'Camiseta oficial 2015 manga corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '9df10eca9ffafc2'),
(406, '2015-03-13 15:59:56', '2015-03-13 17:24:03', 10, NULL, 'AB0378-M', 'CAMISETA OFICIAL 2015 MANGA CORTA  - M', 'camiseta-oficial-2015-manga-corta-m', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 34, '1', '1', 'Camiseta oficial 2015 manga corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '9df10eca9ffafc2'),
(407, '2015-03-13 15:59:56', '2015-03-13 17:24:28', 10, NULL, 'AB0378-L', 'CAMISETA OFICIAL 2015 MANGA CORTA  - L', 'camiseta-oficial-2015-manga-corta-l', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 34, '1', '1', 'Camiseta oficial 2015 manga corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '9df10eca9ffafc2'),
(408, '2015-03-13 15:59:56', '2015-03-13 17:24:41', 10, NULL, 'AB0378-XL', 'CAMISETA OFICIAL 2015 MANGA CORTA  - XL', 'camiseta-oficial-2015-manga-corta-xl', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 17, '1', '1', 'Camiseta oficial 2015 manga corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '9df10eca9ffafc2'),
(409, '2015-03-13 15:59:56', '2015-03-13 17:28:30', 10, NULL, 'AB0379-S', 'CAMISETA OFICIAL 2015 MANGA LARGA  - S', 'camiseta-oficial-2015-manga-larga-s', '154899.0000', '133534.0000', '0.0000', '0.0000', '1', 28, '1', '1', 'Camiseta Oficial 2015 manga larga talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '1', '31', '29', '0', 0, '0e6c038c7547501'),
(410, '2015-03-13 15:59:56', '2015-03-13 17:28:42', 10, NULL, 'AB0379-M', 'CAMISETA OFICIAL 2015 MANGA LARGA  - M', 'camiseta-oficial-2015-manga-larga-m', '154899.0000', '133534.0000', '0.0000', '0.0000', '1', 36, '1', '1', 'Camiseta Oficial 2015 manga larga talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '1', '31', '29', '0', 0, '0e6c038c7547501'),
(412, '2015-03-13 15:59:56', '2015-03-13 17:28:54', 10, NULL, 'AB0379-L', 'CAMISETA OFICIAL 2015 MANGA LARGA  - L', 'camiseta-oficial-2015-manga-larga-l', '154899.0000', '133534.0000', '0.0000', '0.0000', '1', 41, '1', '1', 'Camiseta Oficial 2015 manga larga talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '1', '31', '29', '0', 0, '0e6c038c7547501'),
(413, '2015-03-13 15:59:56', '2015-03-13 17:29:58', 10, NULL, 'AB0379-XL', 'CAMISETA OFICIAL 2015 MANGA LARGA  - XL', 'camiseta-oficial-2015-manga-larga-xl', '154899.0000', '133534.0000', '0.0000', '0.0000', '1', 17, '1', '1', 'Camiseta Oficial 2015 manga larga talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '1', '31', '29', '0', 0, '0e6c038c7547501'),
(414, '2015-03-13 15:59:56', '2015-04-04 19:25:48', 10, NULL, 'AB0384-S', 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - S', 'camiseta-entrenamiento-2015-blanca-s', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, '2a4159ec80fa911'),
(415, '2015-03-13 15:59:56', '2015-04-04 19:26:10', 10, NULL, 'AB0384-M', 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - M', 'camiseta-entrenamiento-2015-blanca-m', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, '2a4159ec80fa911'),
(416, '2015-03-13 15:59:56', '2015-04-04 19:26:25', 10, NULL, 'AB0384-L', 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - L', 'camiseta-entrenamiento-2015-blanca-l', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, '2a4159ec80fa911'),
(417, '2015-03-13 15:59:56', '2015-04-04 19:26:54', 10, NULL, 'AB0384-XL', 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - XL', 'camiseta-entrenamiento-2015-blanca-xl', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, '2a4159ec80fa911'),
(418, '2015-03-13 15:59:56', '2015-03-13 17:36:24', 10, NULL, 'AB0385-S', 'CAMISETA ENTRENAMIENTO 2015 AZUL  - S', 'camiseta-entrenamiento-2015-azul-s', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, 'ebd0a29a1304373'),
(419, '2015-03-13 15:59:56', '2015-03-13 17:36:37', 10, NULL, 'AB0385-M', 'CAMISETA ENTRENAMIENTO 2015 AZUL  - M', 'camiseta-entrenamiento-2015-azul-m', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, 'ebd0a29a1304373'),
(420, '2015-03-13 15:59:56', '2015-03-13 17:36:49', 10, NULL, 'AB0385-L', 'CAMISETA ENTRENAMIENTO 2015 AZUL  - L', 'camiseta-entrenamiento-2015-azul-l', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, 'ebd0a29a1304373'),
(421, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0385-XL', 'CAMISETA ENTRENAMIENTO 2015 AZUL  - XL', 'camiseta-entrenamiento-2015-azul-xl', '94900.0000', '81810.3400', '0.0000', '0.0000', '1', 2, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,090', '1', '31', '29', '0', 0, 'ebd0a29a1304373'),
(422, '2015-03-13 15:59:56', '2015-04-04 19:14:56', 10, NULL, 'AB0391-S', 'BUSO TIPO FLEECE - S', 'buso-tipo-fleece-s', '124900.0000', '107672.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Buso en fleece', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '4', '40', '29', '0', 0, '0b39cb746679789'),
(423, '2015-03-13 15:59:56', '2015-04-04 19:15:15', 10, NULL, 'AB0391-M', 'BUSO TIPO FLEECE - M', 'buso-tipo-fleece-m', '124900.0000', '107672.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Buso en fleece', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '4', '40', '29', '0', 0, '0b39cb746679789'),
(424, '2015-03-13 15:59:56', '2015-04-04 19:15:30', 10, NULL, 'AB0391-L', 'BUSO TIPO FLEECE - L', 'buso-tipo-fleece-l', '124900.0000', '107672.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Buso en fleece', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '4', '40', '29', '0', 0, '0b39cb746679789'),
(425, '2015-03-13 15:59:56', '2015-04-04 19:15:48', 10, NULL, 'AB0391-XL', 'BUSO TIPO FLEECE - XL', 'buso-tipo-fleece-xl', '124900.0000', '107672.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Buso en fleece', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '4', '40', '29', '0', 0, '0b39cb746679789'),
(426, '2015-03-13 15:59:56', '2015-04-04 19:32:41', 10, NULL, 'AB0392-S', 'CHAQUETA TIPO FILLAT 2015  - S', 'chaqueta-tipo-fillat-2015-s', '239900.0000', '206810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Chaqueta de tipo gaban', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '1000', '18', '38', '38', '0', 0, '0220cd5103f855a'),
(427, '2015-03-13 15:59:56', '2015-04-04 19:32:56', 10, NULL, 'AB0392-M', 'CHAQUETA TIPO FILLAT 2015  - M', 'chaqueta-tipo-fillat-2015-m', '239900.0000', '206810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Chaqueta de tipo gaban', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '1000', '18', '38', '38', '0', 0, '0220cd5103f855a'),
(428, '2015-03-13 15:59:56', '2015-04-04 19:33:11', 10, NULL, 'AB0392-L', 'CHAQUETA TIPO FILLAT 2015  - L', 'chaqueta-tipo-fillat-2015-l', '239900.0000', '206810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Chaqueta de tipo gaban', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '1000', '18', '38', '38', '0', 0, '0220cd5103f855a'),
(429, '2015-03-13 15:59:56', '2015-04-04 19:33:25', 10, NULL, 'AB0392-XL', 'CHAQUETA TIPO FILLAT 2015  - XL', 'chaqueta-tipo-fillat-2015-xl', '239900.0000', '206810.0000', '0.0000', '0.0000', '1', 4, '3', '1', 'Chaqueta de tipo gaban', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '1000', '18', '38', '38', '0', 0, '0220cd5103f855a'),
(430, '2015-03-13 15:59:56', '2015-03-13 17:45:53', 10, NULL, 'AB0377-XS', 'OFICIAL DAMA 2015 - XS', 'oficial-dama-2015-xs', '119900.0000', '103362.0000', '0.0000', '0.0000', '1', 14, '1', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '0', 0, 'e0b006df8c56742'),
(433, '2015-03-13 15:59:56', '2015-03-13 17:46:19', 10, NULL, 'AB0377-M', 'OFICIAL DAMA 2015 - M', 'oficial-dama-2015-m', '119900.0000', '103362.0000', '0.0000', '0.0000', '1', 13, '1', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '0', 0, 'e0b006df8c56742'),
(432, '2015-03-13 15:59:56', '2015-03-13 17:46:06', 10, NULL, 'AB0377-S', 'OFICIAL DAMA 2015 - S', 'oficial-dama-2015-s', '119900.0000', '103362.0000', '0.0000', '0.0000', '1', 12, '1', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '0', 0, 'e0b006df8c56742'),
(434, '2015-03-13 15:59:56', '2015-03-13 17:46:35', 10, NULL, 'AB0377-L', 'OFICIAL DAMA 2015 - L', 'oficial-dama-2015-l', '119900.0000', '103362.0000', '0.0000', '0.0000', '1', 12, '1', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '0', 0, 'e0b006df8c56742'),
(435, '2015-03-13 15:59:56', '2015-03-13 17:47:24', 10, NULL, 'AB0377-XL', 'OFICIAL DAMA 2015 - XL', 'oficial-dama-2015-xl', '119900.0000', '103362.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '0', 0, 'e0b006df8c56742'),
(436, '2015-03-13 15:59:56', '2015-03-13 17:50:02', 10, NULL, 'AB0376-2T', 'MINIKIT 2015 - 2T', 'minikit-2015-2t', '139899.0000', '120603.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '0', 0, 'e2ce0f076f8f15a'),
(437, '2015-03-13 15:59:56', '2015-03-13 17:50:14', 10, NULL, 'AB0376-2X', 'MINIKIT 2015 - 2XS', 'minikit-2015-2xs', '139899.0000', '120603.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '0', 0, 'e2ce0f076f8f15a'),
(438, '2015-03-13 15:59:56', '2015-03-13 17:50:28', 10, NULL, 'AB0376-3T', 'MINIKIT 2015 - 3T', 'minikit-2015-3t', '139899.0000', '120603.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '0', 0, 'e2ce0f076f8f15a'),
(439, '2015-03-13 15:59:56', '2015-03-13 17:50:42', 10, NULL, 'AB0376-4T', 'MINIKIT 2015 - 4T', 'minikit-2015-4t', '139899.0000', '120603.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '0', 0, 'e2ce0f076f8f15a'),
(440, '2015-03-13 15:59:56', '2015-03-13 17:50:54', 10, NULL, 'AB0376-5T', 'MINIKIT 2015 - 5T', 'minikit-2015-5t', '139899.0000', '120603.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '0', 0, 'e2ce0f076f8f15a'),
(441, '2015-03-13 15:59:56', '2015-04-04 19:39:44', 10, NULL, 'AB0381-S', 'PANTALONETA OFICIAL 2015 - S', 'pantaloneta-oficial-2015-s', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta oficial 2105', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, 'a5639f2e7e0850c'),
(442, '2015-03-13 15:59:56', '2015-04-04 19:40:04', 10, NULL, 'AB0381-M', 'PANTALONETA OFICIAL 2015 - M', 'pantaloneta-oficial-2015-m', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta oficial 2105', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, 'a5639f2e7e0850c'),
(443, '2015-03-13 15:59:56', '2015-04-04 19:40:19', 10, NULL, 'AB0381-L', 'PANTALONETA OFICIAL 2015 - L', 'pantaloneta-oficial-2015-l', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta oficial 2105', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, 'a5639f2e7e0850c'),
(444, '2015-03-13 15:59:56', '2015-04-04 19:40:33', 10, NULL, 'AB0381-XL', 'PANTALONETA OFICIAL 2015 - XL', 'pantaloneta-oficial-2015-xl', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta oficial 2105', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, 'a5639f2e7e0850c'),
(445, '2015-03-13 15:59:56', '2015-04-04 19:56:31', 10, NULL, 'AB0382-S', 'PANTALONETA VISITANTE 2015 - S', 'pantaloneta-visitante-2015-s', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta visitante 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '3c20fac57a59be0'),
(446, '2015-03-13 15:59:56', '2015-04-04 19:56:46', 10, NULL, 'AB0382-M', 'PANTALONETA VISITANTE 2015 - M', 'pantaloneta-visitante-2015-m', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta visitante 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '3c20fac57a59be0'),
(447, '2015-03-13 15:59:56', '2015-04-04 19:57:12', 10, NULL, 'AB0382-L', 'PANTALONETA VISITANTE 2015 - L', 'pantaloneta-visitante-2015-l', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta visitante 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '3c20fac57a59be0'),
(448, '2015-03-13 15:59:56', '2015-04-04 19:57:27', 10, NULL, 'AB0382-XL', 'PANTALONETA VISITANTE 2015 - XL', 'pantaloneta-visitante-2015-xl', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta visitante 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '3c20fac57a59be0'),
(449, '2015-03-13 15:59:56', '2015-03-13 17:58:25', 10, NULL, 'AB0386-S', 'POLO BLANCA 2015 - S', 'polo-blanca-2015-s', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Polo blanca 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '0', 0, '5519d75e9dde8ce'),
(450, '2015-03-13 15:59:56', '2015-03-13 17:58:33', 10, NULL, 'AB0386-M', 'POLO BLANCA 2015 - M', 'polo-blanca-2015-m', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Polo blanca 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '0', 0, '5519d75e9dde8ce'),
(451, '2015-03-13 15:59:56', '2015-03-13 17:58:41', 10, NULL, 'AB0386-L', 'POLO BLANCA 2015 - L', 'polo-blanca-2015-l', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Polo blanca 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '0', 0, '5519d75e9dde8ce'),
(452, '2015-03-13 15:59:56', '2015-03-13 17:58:48', 10, NULL, 'AB0386-XL', 'POLO BLANCA 2015 - XL', 'polo-blanca-2015-xl', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Polo blanca 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '0', 0, '5519d75e9dde8ce'),
(548, '2015-04-09 15:40:41', '2015-04-14 16:21:32', 10, NULL, 'AB0391', 'BUSO TIPO FLEECE', 'buso-tipo-fleece', '124900.0000', '107672.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Buso en fleece', 0, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '4', '40', '29', '0', 0, 'af769070d922da4'),
(453, '2015-03-13 15:59:56', '2015-03-15 13:33:10', 10, NULL, 'AB0390-S', 'ROMPEVIENTOS 2015 - S', 'rompevientos-2015-s', '159900.0000', '137845.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Rompevientos 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,520', '4', '40', '29', '0', 0, 'c199a0854d623d3'),
(454, '2015-03-13 15:59:56', '2015-03-15 13:33:20', 10, NULL, 'AB0390-M', 'ROMPEVIENTOS 2015 - M', 'rompevientos-2015-m', '159900.0000', '137845.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'Rompevientos 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,520', '4', '40', '29', '0', 0, 'c199a0854d623d3'),
(455, '2015-03-13 15:59:56', '2015-03-15 13:33:31', 10, NULL, 'AB0390-L', 'ROMPEVIENTOS 2015 - L', 'rompevientos-2015-l', '159900.0000', '137845.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Rompevientos 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,520', '4', '40', '29', '0', 0, 'c199a0854d623d3'),
(456, '2015-03-13 15:59:56', '2015-03-15 13:33:39', 10, NULL, 'AB0390-XL', 'ROMPEVIENTOS 2015 - XL', 'rompevientos-2015-xl', '159900.0000', '137845.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Rompevientos 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,520', '4', '40', '29', '0', 0, 'c199a0854d623d3'),
(457, '2015-03-13 15:59:56', '2015-04-04 20:01:16', 10, NULL, 'AB0393-S', 'SUDADERA ENTRENO 2015 - S', 'sudadera-entreno-2015-s', '174900.0000', '150776.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Sudadera entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'a5c77354b3eefa7'),
(458, '2015-03-13 15:59:56', '2015-04-04 20:01:39', 10, NULL, 'AB0393-M', 'SUDADERA ENTRENO 2015 - M', 'sudadera-entreno-2015-m', '174900.0000', '150776.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'a5c77354b3eefa7'),
(459, '2015-03-13 15:59:56', '2015-04-04 20:02:08', 10, NULL, 'AB0393-L', 'SUDADERA ENTRENO 2015 - L', 'sudadera-entreno-2015-l', '174900.0000', '150776.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'a5c77354b3eefa7'),
(460, '2015-03-13 15:59:56', '2015-04-04 20:02:27', 10, NULL, 'AB0393-XL', 'SUDADERA ENTRENO 2015 - XL', 'sudadera-entreno-2015-l', '174900.0000', '150775.8600', '0.0000', '0.0000', '1', 4, '1', '1', 'Sudadera entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'a5c77354b3eefa7'),
(461, '2015-03-13 15:59:56', '2015-04-04 20:06:28', 10, NULL, 'AB0394-S', 'SUDADERA PRESENTACION 2015 - S', 'sudadera-presentacion-2015-s', '199900.0000', '172328.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'df07f6cd54dfadd'),
(462, '2015-03-13 15:59:56', '2015-04-04 20:06:41', 10, NULL, 'AB0394-M', 'SUDADERA PRESENTACION 2015 - M', 'sudadera-presentacion-2015-m', '199900.0000', '172328.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'df07f6cd54dfadd'),
(464, '2015-03-13 15:59:56', '2015-04-04 20:07:00', 10, NULL, 'AB0394-L', 'SUDADERA PRESENTACION 2015 - L', 'sudadera-presentacion-2015-l', '199900.0000', '172328.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'df07f6cd54dfadd'),
(465, '2015-03-13 15:59:56', '2015-04-04 20:07:17', 10, NULL, 'AB0394-XL', 'SUDADERA PRESENTACION 2015 - XL', 'sudadera-presentacion-2015-xl', '199900.0000', '172328.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'df07f6cd54dfadd'),
(466, '2015-03-13 15:59:56', '2015-03-15 19:31:18', 10, NULL, 'AB0396-S', 'CHAQUETA PRESENTACION 2015 - S', 'chaqueta-presentacion-2015-s', '169901.0000', '146466.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'Chaqueta presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '2', '38', '29', '0', 0, '4ff569371d64148'),
(467, '2015-03-13 15:59:56', '2015-04-04 19:24:13', 10, NULL, 'AB0396-M', 'CHAQUETA PRESENTACION 2015 - M', 'chaqueta-presentacion-2015-m', '169901.0000', '146466.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Chaqueta presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '2', '38', '29', '0', 0, '4ff569371d64148'),
(468, '2015-03-13 15:59:56', '2015-04-04 19:24:33', 10, NULL, 'AB0396-L', 'CHAQUETA PRESENTACION 2015 - L', 'chaqueta-presentacion-2015-l', '169901.0000', '146466.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Chaqueta presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '2', '38', '29', '0', 0, '4ff569371d64148'),
(469, '2015-03-13 15:59:56', '2015-03-15 19:31:47', 10, NULL, 'AB0396-XL', 'CHAQUETA PRESENTACION 2015 - XL', 'chaqueta-presentacion-2015-xl', '169901.0000', '146466.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Chaqueta presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '2', '38', '29', '0', 0, '4ff569371d64148'),
(470, '2015-03-13 15:59:56', '2015-03-15 19:36:57', 10, NULL, 'AB0380-S', 'CAMISETA OFICIAL NIOS 2015 - S', 'camiseta-oficial-ni-os-2015-s', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'camiseta oficial nios 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '30', '28', '0', 0, '9df10eca9ffafc2'),
(471, '2015-03-13 15:59:56', '2015-03-15 19:35:00', 10, NULL, 'AB0380-M', 'CAMISETA OFICIAL NIOS 2015 - M', 'camiseta-oficial-ni-os-2015-m', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 12, '1', '1', 'camiseta oficial nios 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '30', '28', '0', 0, '9df10eca9ffafc2'),
(472, '2015-03-13 15:59:56', '2015-03-15 19:35:11', 10, NULL, 'AB0380-L', 'CAMISETA OFICIAL NIOS 2015 - L', 'camiseta-oficial-ni-os-2015-l', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 11, '1', '1', 'camiseta oficial nios 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '30', '28', '0', 0, '9df10eca9ffafc2'),
(473, '2015-03-13 15:59:56', '2015-03-15 19:35:22', 10, NULL, 'AB0380-XL', 'CAMISETA OFICIAL NIOS 2015 - XL', 'camiseta-oficial-ni-os-2015-xl', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'camiseta oficial nios 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '30', '28', '0', 0, '9df10eca9ffafc2'),
(474, '2015-03-13 15:59:56', '2015-03-15 19:44:53', 10, NULL, 'AB0339-S', 'PANTALON TIPO YOGA GRIS JASPE - S', 'pantalon-tipo-yoga-gris-jaspe-s', '59900.0000', '51638.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'PANTALON TIPO YOGA GRIS JASPE', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '480', '1', '25', '36', '0', 0, '29f1e2789485b23'),
(475, '2015-03-13 15:59:56', '2015-03-15 19:45:04', 10, NULL, 'AB0339-M', 'PANTALON TIPO YOGA GRIS JASPE - M', 'pantalon-tipo-yoga-gris-jaspe-m', '59900.0000', '51638.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'PANTALON TIPO YOGA GRIS JASPE', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '480', '1', '25', '36', '0', 0, '29f1e2789485b23'),
(476, '2015-03-13 15:59:56', '2015-03-15 19:45:15', 10, NULL, 'AB0339-L', 'PANTALON TIPO YOGA GRIS JASPE - L', 'pantalon-tipo-yoga-gris-jaspe-l', '59900.0000', '51638.0000', '0.0000', '0.0000', '1', 8, '1', '1', 'PANTALON TIPO YOGA GRIS JASPE', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '480', '1', '25', '36', '0', 0, '29f1e2789485b23'),
(477, '2015-03-13 15:59:56', '2015-03-15 19:45:28', 10, NULL, 'AB0339-XL', 'PANTALON TIPO YOGA GRIS JASPE - XL', 'pantalon-tipo-yoga-gris-jaspe-xl', '59900.0000', '51638.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'PANTALON TIPO YOGA GRIS JASPE', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '480', '1', '25', '36', '0', 0, '29f1e2789485b23'),
(478, '2015-03-13 15:59:56', '2015-03-15 19:48:53', 10, NULL, 'AB0345-S', 'T SHIRT OVER CON ESTAMPADO GRIS - S', 't-shirt-over-con-estampado-gris-s', '48900.0000', '42155.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'T SHIRT OVER CON ESTAMPADO GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '370', '0,5', '25', '36', '0', 0, '555a3444c261974'),
(479, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0345-M', 'T SHIRT OVER CON ESTAMPADO GRIS - M', 't-shirt-over-con-estampado-gris-m', '48900.0000', '42155.1700', '0.0000', '0.0000', '1', NULL, '1', '1', 'T SHIRT OVER CON ESTAMPADO GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '370', '0,5', '25', '36', '0', 0, '555a3444c261974'),
(480, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0345-L', 'T SHIRT OVER CON ESTAMPADO GRIS - L', 't-shirt-over-con-estampado-gris-l', '48900.0000', '42155.1700', '0.0000', '0.0000', '1', NULL, '1', '1', 'T SHIRT OVER CON ESTAMPADO GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '370', '0,5', '25', '36', '0', 0, '555a3444c261974'),
(481, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0345-XL', 'T SHIRT OVER CON ESTAMPADO GRIS - XL', 't-shirt-over-con-estampado-gris-xl', '48900.0000', '42155.1700', '0.0000', '0.0000', '1', NULL, '1', '1', 'T SHIRT OVER CON ESTAMPADO GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '370', '0,5', '25', '36', '0', 0, '555a3444c261974'),
(482, '2015-03-13 15:59:56', '2015-03-15 19:51:49', 10, NULL, 'AB0347-S', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - S', 'leggins-estanpado-foil-bicolor-azul-s', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '0224238e940c899'),
(483, '2015-03-13 15:59:56', '2015-03-15 19:51:56', 10, NULL, 'AB0347-M', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - M', 'leggins-estanpado-foil-bicolor-azul-m', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '0224238e940c899'),
(484, '2015-03-13 15:59:56', '2015-03-15 19:52:06', 10, NULL, 'AB0347-L', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - L', 'leggins-estanpado-foil-bicolor-azul-l', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '0224238e940c899'),
(485, '2015-03-13 15:59:56', '2015-03-15 19:52:15', 10, NULL, 'AB0347-XL', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - XL', 'leggins-estanpado-foil-bicolor-azul-xl', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '0224238e940c899'),
(486, '2015-03-13 15:59:56', '2015-03-15 19:55:43', 10, NULL, 'AB0348-S', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - S', 'leggins-estanpado-foil-bicolor-negro-s', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '62602e45e053af2'),
(487, '2015-03-13 15:59:56', '2015-03-15 19:55:53', 10, NULL, 'AB0348-M', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - M', 'leggins-estanpado-foil-bicolor-negro-m', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '62602e45e053af2'),
(488, '2015-03-13 15:59:56', '2015-03-15 19:56:05', 10, NULL, 'AB0348-L', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - L', 'leggins-estanpado-foil-bicolor-negro-l', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '62602e45e053af2'),
(489, '2015-03-13 15:59:56', '2015-03-15 19:56:09', 10, NULL, 'AB0348-XL', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - XL', 'leggins-estanpado-foil-bicolor-negro-xl', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '62602e45e053af2'),
(490, '2015-03-13 15:59:56', '2015-03-17 17:50:22', 10, NULL, 'AB0340-S', 'ESQUELETO ESTRELLA GRIS - S', 'esqueleto-estrella-gris-s', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,350', '0,5', '25', '36', '0', 0, 'ccbfe439698891c'),
(491, '2015-03-13 15:59:56', '2015-03-18 10:06:44', 10, NULL, 'AB0340-M', 'ESQUELETO ESTRELLA GRIS - M', 'esqueleto-estrella-gris-m', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,350', '0,5', '25', '36', '0', 0, 'ccbfe439698891c'),
(492, '2015-03-13 15:59:56', '2015-03-18 10:06:48', 10, NULL, 'AB0340-L', 'ESQUELETO ESTRELLA GRIS - L', 'esqueleto-estrella-gris-l', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,350', '0,5', '25', '36', '0', 0, 'ccbfe439698891c'),
(493, '2015-03-13 15:59:56', '2015-03-18 10:06:51', 10, NULL, 'AB0340-XL', 'ESQUELETO ESTRELLA GRIS - XL', 'esqueleto-estrella-gris-xl', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,350', '0,5', '25', '36', '0', 0, 'ccbfe439698891c'),
(494, '2015-03-13 15:59:56', '2015-03-18 10:17:37', 10, NULL, 'AB0341-S', 'ESQUELETO ESTRELLA BLANCO - S', 'esqueleto-estrella-blanco-s', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0.350', '0.5', '25', '36', '0', 0, 'ebd73055eebf263'),
(495, '2015-03-13 15:59:56', '2015-03-18 10:15:06', 10, NULL, 'AB0341-M', 'ESQUELETO ESTRELLA BLANCO - M', 'esqueleto-estrella-blanco-m', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,350', '0.5', '25', '36', '0', 0, 'ebd73055eebf263'),
(496, '2015-03-13 15:59:56', '2015-03-18 10:15:10', 10, NULL, 'AB0341-L', 'ESQUELETO ESTRELLA BLANCO - L', 'esqueleto-estrella-blanco-l', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,350', '0.5', '25', '36', '0', 0, 'ebd73055eebf263'),
(497, '2015-03-13 15:59:56', '2015-03-15 21:51:46', 10, NULL, 'AB0341-XL', 'ESQUELETO ESTRELLA BLANCO - XL', 'esqueleto-estrella-blanco-xl', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0.5', '25', '36', '0', 0, 'ebd73055eebf263'),
(498, '2015-03-13 15:59:56', '2015-03-18 10:15:15', 10, NULL, 'AB0341-XS', 'ESQUELETO ESTRELLA BLANCO - XS', 'esqueleto-estrella-blanco-xs', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,350', '0.5', '25', '36', '0', 0, 'ebd73055eebf263'),
(499, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0352-S', 'SUETER CAPOTA STYLE GRIS - S', 'sueter-capota-style-gris-s', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', NULL, '1', '1', 'SUETER CAPOTA STYLE GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '400', '1', '25', '36', '0', 0, 'e38bd3c7eb7506e'),
(500, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0352-M', 'SUETER CAPOTA STYLE GRIS - M', 'sueter-capota-style-gris-m', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', NULL, '1', '1', 'SUETER CAPOTA STYLE GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '400', '1', '25', '36', '0', 0, 'e38bd3c7eb7506e'),
(501, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0352-L', 'SUETER CAPOTA STYLE GRIS - L', 'sueter-capota-style-gris-l', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', NULL, '1', '1', 'SUETER CAPOTA STYLE GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '400', '1', '25', '36', '0', 0, 'e38bd3c7eb7506e'),
(502, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0352-XL', 'SUETER CAPOTA STYLE GRIS - XL', 'sueter-capota-style-gris-xl', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', NULL, '1', '1', 'SUETER CAPOTA STYLE GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '400', '1', '25', '36', '0', 0, 'e38bd3c7eb7506e'),
(503, '2015-03-13 15:59:56', NULL, 10, NULL, 'AB0352-XS', 'SUETER CAPOTA STYLE GRIS - XS', 'sueter-capota-style-gris-xs', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', NULL, '1', '1', 'SUETER CAPOTA STYLE GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '400', '1', '25', '36', '0', 0, 'e38bd3c7eb7506e'),
(504, '2015-03-13 15:59:56', '2015-03-15 21:58:29', 10, NULL, 'AB0350-XS', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - XS', 'camiseta-estampada-m-c-bicolor-blanco-xs', '39901.0000', '34397.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'f33e2f50dc11a83'),
(505, '2015-03-13 15:59:56', '2015-03-15 21:59:55', 10, NULL, 'AB0350-S', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - S', 'camiseta-estampada-m-c-bicolor-blanco-s', '39901.0000', '34397.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'f33e2f50dc11a83'),
(506, '2015-03-13 15:59:56', '2015-03-15 22:00:04', 10, NULL, 'AB0350-M', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - M', 'camiseta-estampada-m-c-bicolor-blanco-m', '39901.0000', '34397.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'f33e2f50dc11a83'),
(507, '2015-03-13 15:59:56', '2015-03-15 22:00:13', 10, NULL, 'AB0350-L', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - L', 'camiseta-estampada-m-c-bicolor-blanco-l', '39901.0000', '34397.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'f33e2f50dc11a83'),
(508, '2015-03-13 15:59:56', '2015-03-15 22:00:22', 10, NULL, 'AB0350-XL', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - XL', 'camiseta-estampada-m-c-bicolor-blanco-xl', '39901.0000', '34397.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'f33e2f50dc11a83'),
(509, '2015-03-13 15:59:56', '2015-04-04 20:15:34', 10, NULL, 'AB0223-S', 'SWEATER CUELLO EN V MUJER MFC - S', 'sweater-cuello-en-v-mujer-mfc-s', '82901.0000', '71466.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'SWEATER CUELLO EN V MUJER MFC', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '200', '2', '25', '36', '0', 0, '5565dc440793e41'),
(510, '2015-03-13 15:59:56', '2015-03-15 22:02:42', 10, NULL, 'AB0223-M', 'SWEATER CUELLO EN V MUJER MFC - M', 'sweater-cuello-en-v-mujer-mfc-m', '82901.0000', '71466.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'SWEATER CUELLO EN V MUJER MFC', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '200', '2', '25', '36', '0', 0, '5565dc440793e41'),
(511, '2015-03-13 15:59:56', '2015-03-15 22:02:54', 10, NULL, 'AB0223-L', 'SWEATER CUELLO EN V MUJER MFC - L', 'sweater-cuello-en-v-mujer-mfc-l', '82901.0000', '71466.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'SWEATER CUELLO EN V MUJER MFC', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '200', '2', '25', '36', '0', 0, '5565dc440793e41'),
(512, '2015-03-13 15:59:56', '2015-03-15 22:03:02', 10, NULL, 'AB0223-XL', 'SWEATER CUELLO EN V MUJER MFC - XL', 'sweater-cuello-en-v-mujer-mfc-xl', '82901.0000', '71466.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'SWEATER CUELLO EN V MUJER MFC', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '200', '2', '25', '36', '0', 0, '5565dc440793e41'),
(513, '2015-03-13 15:59:56', '2015-04-04 19:30:22', 10, NULL, 'AB0388-S', 'PANTALONETA ENTRENO 2015 - S', 'pantaloneta-entreno-2015-s', '64900.0000', '55948.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'Pantaloneta entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, 'fe437be3901caef'),
(515, '2015-03-13 15:59:56', '2015-04-04 19:30:33', 10, NULL, 'AB0388-M', 'PANTALONETA ENTRENO 2015 - M', 'pantaloneta-entreno-2015-m', '64900.0000', '55948.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'Pantaloneta entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, 'fe437be3901caef'),
(516, '2015-03-13 15:59:56', '2015-04-04 19:30:46', 10, NULL, 'AB0388-L', 'PANTALONETA ENTRENO 2015 - L', 'pantaloneta-entreno-2015-l', '64900.0000', '55948.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'Pantaloneta entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, 'fe437be3901caef'),
(581, '2015-04-13 17:30:41', '2015-04-15 10:02:28', 13, 19, 'AB0375', 'MORRAL MILLONARIOS MFC', 'morral-millonarios-mfc', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 30, '1', '1', '', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0.300', '10', '17', '40', '0', NULL, '735d0a45e59feaf'),
(582, '2015-04-13 17:39:08', '2015-04-14 20:07:56', 13, 20, 'AB0383 AZUL', 'GORRA MFC 2015', 'gorra-mfc-2015', '32900.0000', '28362.0000', '0.0000', '0.0000', '1', 8, '1', '1', '', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, '9a4405d006c899c'),
(585, '2015-04-09 15:40:41', '2015-04-13 18:39:14', 10, NULL, 'AB0341-M', 'ESQUELETO ESTRELLA BLANCO - M', 'esqueleto-estrella-blanco-m', '44900.0000', '38706.9000', '0.0000', '0.0000', '1', 16, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0.5', '25', '36', '0', 0, 'ae5acd487b8bfff'),
(586, '2015-04-09 15:40:41', '2015-04-13 18:39:14', 10, NULL, 'AB0341-L', 'ESQUELETO ESTRELLA BLANCO - L', 'esqueleto-estrella-blanco-l', '44900.0000', '38706.9000', '0.0000', '0.0000', '1', 16, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0.5', '25', '36', '0', 0, 'ae5acd487b8bfff'),
(587, '2015-04-09 15:40:41', '2015-04-15 09:30:29', 10, NULL, 'AB0340-XS', 'ESQUELETO ESTRELLA GRIS - XS', 'esqueleto-estrella-gris-xs', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0,5', '25', '36', '0', 0, '902f9a35c6a7fab'),
(588, '2015-04-09 15:40:41', '2015-04-15 09:30:39', 10, NULL, 'AB0340-S', 'ESQUELETO ESTRELLA GRIS - S', 'esqueleto-estrella-gris-s', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0,5', '25', '36', '0', 0, '902f9a35c6a7fab'),
(589, '2015-04-09 15:40:41', '2015-04-15 09:30:47', 10, NULL, 'AB0340-M', 'ESQUELETO ESTRELLA GRIS - M', 'esqueleto-estrella-gris-m', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0,5', '25', '36', '0', 0, '902f9a35c6a7fab'),
(590, '2015-04-09 15:40:41', '2015-04-15 09:30:59', 10, NULL, 'AB0340-L', 'ESQUELETO ESTRELLA GRIS - L', 'esqueleto-estrella-gris-l', '44900.0000', '38707.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'ESQUELETO ESTRELLA', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '350', '0,5', '25', '36', '0', 0, '902f9a35c6a7fab'),
(591, '2015-04-09 15:40:41', '2015-04-15 09:41:40', 10, NULL, 'AB0347-S', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - S', 'leggins-estanpado-foil-bicolor-azul-s', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '477bddeedba95fb'),
(592, '2015-04-09 15:40:41', '2015-04-15 09:41:50', 10, NULL, 'AB0347-M', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - M', 'leggins-estanpado-foil-bicolor-azul-m', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '477bddeedba95fb'),
(593, '2015-04-09 15:40:41', '2015-04-15 09:41:59', 10, NULL, 'AB0347-L', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - L', 'leggins-estanpado-foil-bicolor-azul-l', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '477bddeedba95fb');
INSERT INTO `default_firesale_products` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `code`, `title`, `slug`, `price`, `price_tax`, `rrp`, `rrp_tax`, `status`, `stock`, `stock_status`, `ship_req`, `description`, `is_variation`, `tax_band`, `brand`, `digital`, `download`, `downloads`, `expires_after`, `expires_interval`, `meta_title`, `meta_description`, `meta_keywords`, `shipping_weight`, `shipping_height`, `shipping_width`, `shipping_depth`, `featured`, `descuento`, `main_image`) VALUES
(594, '2015-04-09 15:40:41', '2015-04-15 09:42:09', 10, NULL, 'AB0347-XL', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - XL', 'leggins-estanpado-foil-bicolor-azul-xl', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, '477bddeedba95fb'),
(595, '2015-04-09 15:40:40', '2015-04-15 09:01:00', 10, NULL, 'AB0221-S', 'BUSO SHETLAND  - S', 'buso-shetland-s', '99400.0000', '85690.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'Buso Tejido Cuello V Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '3', '31', '29', '0', 0, '12a4bccb147041d'),
(596, '2015-04-09 15:40:40', '2015-04-15 09:01:08', 10, NULL, 'AB0221-M', 'BUSO SHETLAND  - M', 'buso-shetland-m', '99400.0000', '85690.0000', '0.0000', '0.0000', '1', 9, '1', '1', 'Buso Tejido Cuello V Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '3', '31', '29', '0', 0, '12a4bccb147041d'),
(597, '2015-04-09 15:40:40', '2015-04-15 09:01:18', 10, NULL, 'AB0221-L', 'BUSO SHETLAND  - L', 'buso-shetland-l', '99400.0000', '85690.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'Buso Tejido Cuello V Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '3', '31', '29', '0', 0, '12a4bccb147041d'),
(598, '2015-04-09 15:40:40', '2015-04-15 09:01:27', 10, NULL, 'AB0221-XL', 'BUSO SHETLAND  - XL', 'buso-shetland-xl', '99400.0000', '85690.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Buso Tejido Cuello V Talla M', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,300', '3', '31', '29', '0', 0, '12a4bccb147041d'),
(599, '2015-04-09 15:40:40', '2015-04-15 08:59:19', 10, NULL, 'AB0096-S', 'SUDADERA DE ENTRENAMIENTO 2014 - S', 'sudadera-de-entrenamiento-2014-s', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'Sudadera De entrenamiento Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'dec32d829a4cb34'),
(600, '2015-04-09 15:40:40', '2015-04-15 08:59:11', 10, NULL, 'AB0096-M', 'SUDADERA DE ENTRENAMIENTO 2014 - M', 'sudadera-de-entrenamiento-2014-m', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Sudadera De entrenamiento Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'dec32d829a4cb34'),
(601, '2015-04-09 15:40:40', '2015-04-15 08:59:02', 10, NULL, 'AB0096-L', 'SUDADERA DE ENTRENAMIENTO 2014 - L', 'sudadera-de-entrenamiento-2014-l', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 16, '1', '1', 'Sudadera De entrenamiento Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'dec32d829a4cb34'),
(602, '2015-04-09 15:40:40', '2015-04-15 08:58:52', 10, NULL, 'AB0096-XL', 'SUDADERA DE ENTRENAMIENTO 2014 - XL', 'sudadera-de-entrenamiento-2014-xl', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Sudadera De entrenamiento Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'dec32d829a4cb34'),
(603, '2015-04-09 15:40:41', '2015-04-09 23:02:46', 10, NULL, 'AB0344-XS', 'BUSO SHOP  - XS', 'buso-shop-xs', '99900.0000', '86120.6900', '0.0000', '0.0000', '1', 5, '2', '1', 'Sweter Estampado Con Mangas en Pu Talla S', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,600', '2', '25', '36', '1', 0, '35f0371274d2833'),
(604, '2015-04-09 15:40:41', '2015-04-09 23:02:46', 10, NULL, 'AB0344-S', 'BUSO SHOP  - S', 'buso-shop-s', '99900.0000', '86120.6900', '0.0000', '0.0000', '1', 6, '1', '1', 'Sweter Estampado Con Mangas en Pu Talla S', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,600', '2', '25', '36', '1', 0, '35f0371274d2833'),
(605, '2015-04-09 15:40:41', '2015-04-09 23:02:46', 10, NULL, 'AB0344-M', 'BUSO SHOP  - M', 'buso-shop-m', '99900.0000', '86120.6900', '0.0000', '0.0000', '1', 6, '1', '1', 'Sweter Estampado Con Mangas en Pu Talla S', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,600', '2', '25', '36', '1', 0, '35f0371274d2833'),
(606, '2015-04-09 15:40:41', '2015-04-09 23:02:46', 10, NULL, 'AB0344-L', 'BUSO SHOP  - L', 'buso-shop-l', '99900.0000', '86120.6900', '0.0000', '0.0000', '1', 6, '1', '1', 'Sweter Estampado Con Mangas en Pu Talla S', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,600', '2', '25', '36', '1', 0, '35f0371274d2833'),
(607, '2015-04-09 15:40:41', '2015-04-14 14:01:53', 10, NULL, 'AB0379-S', 'CAMISETA OFICIAL 2015 MANGA LARGA  - S', 'camiseta-oficial-2015-manga-larga-s', '154900.0040', '133534.4870', '0.0040', '0.0040', '1', 12, '1', '1', 'Camiseta Oficial 2015 manga larga talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '1', '31', '29', '1', 0, '94f5df765f3a33f'),
(608, '2015-04-09 15:40:41', '2015-04-14 14:01:53', 10, NULL, 'AB0379-M', 'CAMISETA OFICIAL 2015 MANGA LARGA  - M', 'camiseta-oficial-2015-manga-larga-m', '154900.0040', '133534.4870', '0.0040', '0.0040', '1', 13, '1', '1', 'Camiseta Oficial 2015 manga larga talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '1', '31', '29', '1', 0, '94f5df765f3a33f'),
(609, '2015-04-09 15:40:41', '2015-04-14 14:01:53', 10, NULL, 'AB0379-L', 'CAMISETA OFICIAL 2015 MANGA LARGA  - L', 'camiseta-oficial-2015-manga-larga-l', '154900.0040', '133534.4870', '0.0040', '0.0040', '1', 13, '1', '1', 'Camiseta Oficial 2015 manga larga talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '1', '31', '29', '1', 0, '94f5df765f3a33f'),
(610, '2015-04-09 15:40:41', '2015-04-14 14:01:53', 10, NULL, 'AB0379-XL', 'CAMISETA OFICIAL 2015 MANGA LARGA  - XL', 'camiseta-oficial-2015-manga-larga-xl', '154900.0040', '133534.4870', '0.0040', '0.0040', '1', 13, '1', '1', 'Camiseta Oficial 2015 manga larga talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '1', '31', '29', '1', 0, '94f5df765f3a33f'),
(611, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0378-S', 'CAMISETA OFICIAL 2015 MANGA CORTA  - S', 'camiseta-oficial-2015-manga-corta-s', '149900.0000', '129224.1400', '0.0000', '0.0000', '1', 14, '1', '1', 'Camiseta oficial 2015 manga corta  Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,200', '1', '31', '29', '0', 0, '1c2c95e23d2b5a6'),
(612, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0378-M', 'CAMISETA OFICIAL 2015 MANGA CORTA  - M', 'camiseta-oficial-2015-manga-corta-m', '149900.0000', '129224.1400', '0.0000', '0.0000', '1', 14, '1', '1', 'Camiseta oficial 2015 manga corta  Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,200', '1', '31', '29', '0', 0, '1c2c95e23d2b5a6'),
(613, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0378-L', 'CAMISETA OFICIAL 2015 MANGA CORTA  - L', 'camiseta-oficial-2015-manga-corta-l', '149900.0000', '129224.1400', '0.0000', '0.0000', '1', 14, '1', '1', 'Camiseta oficial 2015 manga corta  Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,200', '1', '31', '29', '0', 0, '1c2c95e23d2b5a6'),
(614, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0378-XL', 'CAMISETA OFICIAL 2015 MANGA CORTA  - XL', 'camiseta-oficial-2015-manga-corta-xl', '149900.0000', '129224.1400', '0.0000', '0.0000', '1', 14, '1', '1', 'Camiseta oficial 2015 manga corta  Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,200', '1', '31', '29', '0', 0, '1c2c95e23d2b5a6'),
(615, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0360-S', 'CAMISETA PASION DE MILLONES  - S', 'camiseta-pasion-de-millones-s', '38500.0000', '33189.6600', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Pasion De Millones Talla M ', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,300', '1', '31', '29', '0', 0, 'c4a21fc4ad11561'),
(616, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0360-M', 'CAMISETA PASION DE MILLONES  - M', 'camiseta-pasion-de-millones-m', '38500.0000', '33189.6600', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Pasion De Millones Talla M ', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,300', '1', '31', '29', '0', 0, 'c4a21fc4ad11561'),
(617, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0360-L', 'CAMISETA PASION DE MILLONES  - L', 'camiseta-pasion-de-millones-l', '38500.0000', '33189.6600', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Pasion De Millones Talla M ', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,300', '1', '31', '29', '0', 0, 'c4a21fc4ad11561'),
(618, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0360-XL', 'CAMISETA PASION DE MILLONES  - XL', 'camiseta-pasion-de-millones-xl', '38500.0000', '33189.6600', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Pasion De Millones Talla M ', 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,300', '1', '31', '29', '0', 0, 'c4a21fc4ad11561'),
(619, '2015-04-09 15:40:41', '2015-04-13 10:47:54', 10, NULL, 'AB0346-XS', 'T SHIRT OVER CON ESTAMPADO CAMPIN - XS', 't-shirt-over-con-estampado-campin-xs', '48900.0000', '42155.1700', '0.0000', '0.0000', '1', 33, '1', '1', 'Camiseta Manga Larga Gris Talla 4', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,370', '1', '25', '36', '0', 0, '5da92cbcf9eb0c7'),
(620, '2015-04-09 15:40:41', '2015-04-13 10:47:54', 10, NULL, 'AB0346-S', 'T SHIRT OVER CON ESTAMPADO CAMPIN - S', 't-shirt-over-con-estampado-campin-s', '48900.0000', '42155.1700', '0.0000', '0.0000', '1', 33, '1', '1', 'Camiseta Manga Larga Gris Talla 4', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,370', '1', '25', '36', '0', 0, '5da92cbcf9eb0c7'),
(621, '2015-04-09 15:40:41', '2015-04-13 10:47:54', 10, NULL, 'AB0346-M', 'T SHIRT OVER CON ESTAMPADO CAMPIN - M', 't-shirt-over-con-estampado-campin-m', '48900.0000', '42155.1700', '0.0000', '0.0000', '1', 33, '1', '1', 'Camiseta Manga Larga Gris Talla 4', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,370', '1', '25', '36', '0', 0, '5da92cbcf9eb0c7'),
(622, '2015-04-09 15:40:41', '2015-04-13 10:47:54', 10, NULL, 'AB0346-L', 'T SHIRT OVER CON ESTAMPADO CAMPIN - L', 't-shirt-over-con-estampado-campin-l', '48900.0000', '42155.1700', '0.0000', '0.0000', '1', 33, '1', '1', 'Camiseta Manga Larga Gris Talla 4', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,370', '1', '25', '36', '0', 0, '5da92cbcf9eb0c7'),
(623, '2015-04-09 15:40:41', '2015-04-09 23:40:45', 10, NULL, 'AB0349-S', 'CAMISETA ESTAMPADA MC AZUL REY - S', 'camiseta-estampada-m-c-azul-rey-s', '40000.0040', '34482.7630', '0.0040', '0.0040', '1', 6, '1', '1', 'CAMISETA ESTAMPADA MC AZUL REY', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'c0f857ea44ea79d'),
(624, '2015-04-09 15:40:41', '2015-04-09 23:40:45', 10, NULL, 'AB0349-M', 'CAMISETA ESTAMPADA MC AZUL REY - M', 'camiseta-estampada-m-c-azul-rey-m', '40000.0040', '34482.7630', '0.0040', '0.0040', '1', 6, '1', '1', 'CAMISETA ESTAMPADA MC AZUL REY', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'c0f857ea44ea79d'),
(625, '2015-04-09 15:40:41', '2015-04-09 23:40:45', 10, NULL, 'AB0349-XS', 'CAMISETA ESTAMPADA MC AZUL REY - XS', 'camiseta-estampada-m-c-azul-rey-xs', '40000.0040', '34482.7630', '0.0040', '0.0040', '1', 6, '1', '1', 'CAMISETA ESTAMPADA MC AZUL REY', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'c0f857ea44ea79d'),
(626, '2015-04-09 15:40:41', '2015-04-09 23:40:45', 10, NULL, 'AB0349-L', 'CAMISETA ESTAMPADA MC AZUL REY - L', 'camiseta-estampada-m-c-azul-rey-l', '40000.0040', '34482.7630', '0.0040', '0.0040', '1', 6, '1', '1', 'CAMISETA ESTAMPADA MC AZUL REY', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'c0f857ea44ea79d'),
(627, '2015-04-09 15:40:41', '2015-04-15 09:13:40', 10, NULL, 'AB0350-XS', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - XS', 'camiseta-estampada-m-c-bicolor-blanco-xs', '40000.0040', '34482.7630', '0.0040', '0.0040', '1', 4, '1', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'efa7f567e7dbe25'),
(628, '2015-04-09 15:40:41', '2015-04-15 09:12:25', 10, NULL, 'AB0350-S', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - S', 'camiseta-estampada-m-c-bicolor-blanco-s', '40000.0040', '34482.7630', '0.0040', '0.0040', '1', 4, '1', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'efa7f567e7dbe25'),
(629, '2015-04-09 15:40:41', '2015-04-15 09:14:58', 10, NULL, 'AB0350-M', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - M', 'camiseta-estampada-m-c-bicolor-blanco-m', '40000.0040', '34482.7630', '0.0040', '0.0040', '1', 4, '1', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'efa7f567e7dbe25'),
(630, '2015-04-09 15:40:41', '2015-04-15 09:12:42', 10, NULL, 'AB0350-L', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - L', 'camiseta-estampada-m-c-bicolor-blanco-l', '40000.0040', '34482.7630', '0.0040', '0.0040', '1', 0, '3', '1', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'efa7f567e7dbe25'),
(631, '2015-04-09 15:40:41', '2015-04-15 09:47:49', 10, NULL, 'AB0387-S', 'POLO AZUL REY 2015 - S', 'polo-azul-rey-2015-s', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Polo azul rey 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '1', 0, '135ca4319e9475b'),
(632, '2015-04-09 15:40:41', '2015-04-15 09:47:58', 10, NULL, 'AB0387-M', 'POLO AZUL REY 2015 - M', 'polo-azul-rey-2015-m', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Polo azul rey 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '1', 0, '135ca4319e9475b'),
(633, '2015-04-09 15:40:41', '2015-04-15 09:48:07', 10, NULL, 'AB0387-L', 'POLO AZUL REY 2015 - L', 'polo-azul-rey-2015-l', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Polo azul rey 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '1', 0, '135ca4319e9475b'),
(634, '2015-04-09 15:40:41', '2015-04-15 09:48:16', 10, NULL, 'AB0387-XL', 'POLO AZUL REY 2015 - XL', 'polo-azul-rey-2015-xl', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Polo azul rey 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '1', 0, '135ca4319e9475b'),
(635, '2015-04-09 15:40:41', '2015-04-14 14:25:20', 10, NULL, 'AB0394-S', 'SUDADERA PRESENTACION 2015 - S', 'sudadera-presentacion-2015-s', '199900.0000', '172327.5900', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'd1ee21d7c98a6e5'),
(636, '2015-04-09 15:40:41', '2015-04-14 14:25:20', 10, NULL, 'AB0394-M', 'SUDADERA PRESENTACION 2015 - M', 'sudadera-presentacion-2015-m', '199900.0000', '172327.5900', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'd1ee21d7c98a6e5'),
(637, '2015-04-09 15:40:41', '2015-04-14 14:25:20', 10, NULL, 'AB0394-L', 'SUDADERA PRESENTACION 2015 - L', 'sudadera-presentacion-2015-l', '199900.0000', '172327.5900', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'd1ee21d7c98a6e5'),
(638, '2015-04-09 15:40:41', '2015-04-14 14:25:20', 10, NULL, 'AB0394-XL', 'SUDADERA PRESENTACION 2015 - XL', 'sudadera-presentacion-2015-xl', '199900.0000', '172327.5900', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, 'd1ee21d7c98a6e5'),
(639, '2015-04-09 15:40:41', '2015-04-14 14:20:48', 10, NULL, 'AB0393-S', 'SUDADERA ENTRENO 2015 - S', 'sudadera-entreno-2015-s', '174900.0000', '150775.8600', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3fed7e54b23e980'),
(640, '2015-04-09 15:40:41', '2015-04-14 14:20:48', 10, NULL, 'AB0393-M', 'SUDADERA ENTRENO 2015 - M', 'sudadera-entreno-2015-m', '174900.0000', '150775.8600', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3fed7e54b23e980'),
(641, '2015-04-09 15:40:41', '2015-04-14 14:20:48', 10, NULL, 'AB0393-L', 'SUDADERA ENTRENO 2015 - L', 'sudadera-entreno-2015-l', '174900.0000', '150775.8600', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3fed7e54b23e980'),
(642, '2015-04-09 15:40:41', '2015-04-14 14:20:48', 10, NULL, 'AB0393-XL', 'SUDADERA ENTRENO 2015 - XL', 'sudadera-entreno-2015-xl', '174900.0000', '150775.8600', '0.0000', '0.0000', '1', 5, '1', '1', 'Sudadera entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3fed7e54b23e980'),
(643, '2015-04-09 15:40:41', '2015-04-15 09:10:25', 10, NULL, 'AB0391-S', 'BUSO TIPO FLEECE - S', 'buso-tipo-fleece-s', '124900.0000', '107672.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Buso en fleece', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '4', '40', '29', '0', 0, 'af769070d922da4'),
(644, '2015-04-09 15:40:41', '2015-04-15 09:10:32', 10, NULL, 'AB0391-M', 'BUSO TIPO FLEECE - M', 'buso-tipo-fleece-m', '124900.0000', '107672.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Buso en fleece', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '4', '40', '29', '0', 0, 'af769070d922da4'),
(645, '2015-04-09 15:40:41', '2015-04-15 09:10:33', 10, NULL, 'AB0391-L', 'BUSO TIPO FLEECE - L', 'buso-tipo-fleece-l', '124900.0000', '107672.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Buso en fleece', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '4', '40', '29', '0', 0, 'af769070d922da4'),
(646, '2015-04-09 15:40:41', '2015-04-15 09:10:41', 10, NULL, 'AB0391-XL', 'BUSO TIPO FLEECE - XL', 'buso-tipo-fleece-xl', '124900.0000', '107672.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Buso en fleece', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '4', '40', '29', '0', 0, 'af769070d922da4'),
(647, '2015-04-09 15:40:41', '2015-04-15 09:09:23', 10, NULL, 'AB0385-S', 'CAMISETA ENTRENAMIENTO 2015 AZUL  - S', 'camiseta-entrenamiento-2015-azul-s', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, 'b255d5a303cbd6c'),
(648, '2015-04-09 15:40:41', '2015-04-15 09:09:31', 10, NULL, 'AB0385-M', 'CAMISETA ENTRENAMIENTO 2015 AZUL  - M', 'camiseta-entrenamiento-2015-azul-m', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, 'b255d5a303cbd6c'),
(649, '2015-04-09 15:40:41', '2015-04-15 09:09:39', 10, NULL, 'AB0385-L', 'CAMISETA ENTRENAMIENTO 2015 AZUL  - L', 'camiseta-entrenamiento-2015-azul-l', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, 'b255d5a303cbd6c'),
(650, '2015-04-09 15:40:41', '2015-04-15 09:09:47', 10, NULL, 'AB0385-XL', 'CAMISETA ENTRENAMIENTO 2015 AZUL  - XL', 'camiseta-entrenamiento-2015-azul-xl', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '0', 0, 'b255d5a303cbd6c'),
(651, '2015-04-09 15:40:41', '2015-04-15 09:08:18', 10, NULL, 'AB0384-S', 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - S', 'camiseta-entrenamiento-2015-blanca-s', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '1', 0, '2b176791fff8980'),
(652, '2015-04-09 15:40:41', '2015-04-15 09:08:26', 10, NULL, 'AB0384-M', 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - M', 'camiseta-entrenamiento-2015-blanca-m', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '1', 0, '2b176791fff8980'),
(653, '2015-04-09 15:40:41', '2015-04-15 09:08:34', 10, NULL, 'AB0384-L', 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - L', 'camiseta-entrenamiento-2015-blanca-l', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '1', 0, '2b176791fff8980'),
(654, '2015-04-09 15:40:41', '2015-04-15 09:08:36', 10, NULL, 'AB0384-XL', 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - XL', 'camiseta-entrenamiento-2015-blanca-xl', '94900.0000', '81810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta de entrenamiento AdiZero', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '31', '29', '1', 0, '2b176791fff8980'),
(655, '2015-04-09 15:40:40', NULL, 10, NULL, 'AB0031-S', 'CAMISETA OFICIAL 2013 - S', 'camiseta-oficial-2013-s', '19900.0000', '17155.1700', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,290', '1', '31', '29', '0', 0, 'b361821675bf2ac'),
(656, '2015-04-09 15:40:40', '2015-04-14 17:22:22', 10, NULL, 'AB0036-M', 'CAMISETA OFICIAL 2013 - M', 'camiseta-oficial-2013-m', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Larga Talla M', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, '387acbee569f0b0'),
(657, '2015-04-09 15:40:41', '2015-04-15 09:05:27', 10, NULL, 'AB0302-S', 'CAMISETA VISITANTE 2015 MANGA CORTA  - S', 'camiseta-visitante-2015-manga-corta-s', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 9, '1', '1', 'Camiseta Visitante 2015 manga Corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '70f66b1584b3717'),
(658, '2015-04-09 15:40:40', '2015-04-15 08:59:57', 10, NULL, 'AB0095-S', 'SUDADERA DE PRESENTACION 2014 - S', 'sudadera-de-presentacion-2014-s', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'Sudadera Impermeable Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3ee6770b707ecf0'),
(659, '2015-04-09 15:40:40', '2015-04-15 09:00:07', 10, NULL, 'AB0095-M', 'SUDADERA DE PRESENTACION 2014 - M', 'sudadera-de-presentacion-2014-m', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 17, '1', '1', 'Sudadera Impermeable Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3ee6770b707ecf0'),
(660, '2015-04-09 15:40:40', '2015-04-15 09:00:15', 10, NULL, 'AB0095-L', 'SUDADERA DE PRESENTACION 2014 - L', 'sudadera-de-presentacion-2014-l', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 11, '1', '1', 'Sudadera Impermeable Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3ee6770b707ecf0'),
(661, '2015-04-09 15:40:40', '2015-04-15 09:00:23', 10, NULL, 'AB0095-XL', 'SUDADERA DE PRESENTACION 2014 - XL', 'sudadera-de-presentacion-2014-xl', '110000.0000', '94828.0000', '0.0000', '0.0000', '1', 0, '3', '1', 'Sudadera Impermeable Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,980', '4', '40', '29', '0', 0, '3ee6770b707ecf0'),
(662, '2015-04-09 15:40:41', '2015-04-15 09:51:25', 10, NULL, 'AB0380-S', 'CAMISETA OFICIAL NIOS 2015 - S', 'camiseta-oficial-ni-os-2015-s', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 10, '1', '1', 'camiseta oficial nios 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '30', '28', '0', 0, '1c2c95e23d2b5a6'),
(663, '2015-04-09 15:40:41', '2015-04-15 09:51:33', 10, NULL, 'AB0380-M', 'CAMISETA OFICIAL NIOS 2015 - M', 'camiseta-oficial-ni-os-2015-m', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 12, '1', '1', 'camiseta oficial nios 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '30', '28', '0', 0, '1c2c95e23d2b5a6'),
(664, '2015-04-09 15:40:41', '2015-04-15 09:51:42', 10, NULL, 'AB0380-L', 'CAMISETA OFICIAL NIOS 2015 - L', 'camiseta-oficial-ni-os-2015-l', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 11, '1', '1', 'camiseta oficial nios 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '30', '28', '0', 0, '1c2c95e23d2b5a6'),
(665, '2015-04-09 15:40:41', '2015-04-15 09:51:50', 10, NULL, 'AB0380-XL', 'CAMISETA OFICIAL NIOS 2015 - XL', 'camiseta-oficial-ni-os-2015-xl', '109900.0000', '94741.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'camiseta oficial nios 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,090', '1', '30', '28', '0', 0, '1c2c95e23d2b5a6'),
(666, '2015-04-09 15:40:41', '2015-04-21 15:28:26', 10, NULL, 'AB0396-S', 'CHAQUETA PRESENTACION 2015 - S', 'chaqueta-presentacion-2015-s', '169900.0040', '146465.5210', '0.0040', '0.0040', '1', 2, '3', '1', 'Chaqueta presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '2', '38', '29', '0', 0, '3001145f421fefc'),
(667, '2015-04-09 15:40:41', '2015-04-21 15:22:35', 10, NULL, 'AB0396-M', 'CHAQUETA PRESENTACION 2015 - M', 'chaqueta-presentacion-2015-m', '169900.0040', '146465.5210', '0.0040', '0.0040', '1', 5, '1', '1', 'Chaqueta presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '2', '38', '29', '0', 0, '3001145f421fefc'),
(668, '2015-04-09 15:40:41', '2015-04-21 15:27:12', 10, NULL, 'AB0396-L', 'CHAQUETA PRESENTACION 2015 - L', 'chaqueta-presentacion-2015-l', '169900.0040', '146465.5210', '0.0040', '0.0040', '1', 5, '1', '1', 'Chaqueta presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '2', '38', '29', '0', 0, '3001145f421fefc'),
(669, '2015-04-09 15:40:41', '2015-04-21 15:27:50', 10, NULL, 'AB0396-XL', 'CHAQUETA PRESENTACION 2015 - XL', 'chaqueta-presentacion-2015-xl', '169900.0040', '146465.5210', '0.0040', '0.0040', '1', 1, '1', '1', 'Chaqueta presentacion 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,500', '2', '38', '29', '0', 0, '3001145f421fefc'),
(670, '2015-04-09 15:40:41', '2015-04-15 09:43:21', 10, NULL, 'AB0348-S', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - S', 'leggins-estanpado-foil-bicolor-negro-s', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'a8b4ad187110b8f'),
(671, '2015-04-09 15:40:41', '2015-04-15 09:43:34', 10, NULL, 'AB0348-M', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - M', 'leggins-estanpado-foil-bicolor-negro-m', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'a8b4ad187110b8f'),
(672, '2015-04-09 15:40:41', '2015-04-15 09:43:46', 10, NULL, 'AB0348-L', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - L', 'leggins-estanpado-foil-bicolor-negro-l', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'a8b4ad187110b8f'),
(673, '2015-04-09 15:40:41', '2015-04-15 09:43:55', 10, NULL, 'AB0348-XL', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - XL', 'leggins-estanpado-foil-bicolor-negro-xl', '54900.0000', '47328.0000', '0.0000', '0.0000', '1', 3, '1', '1', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '320', '0,5', '25', '36', '0', 0, 'a8b4ad187110b8f'),
(674, '2015-04-09 15:40:41', '2015-04-15 09:59:28', 10, NULL, 'AB0376-18', 'MINIKIT 2015 - 18/24', 'minikit-2015-1824', '140000.0040', '120689.6590', '0.0040', '0.0040', '1', 3, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '1', 0, 'dd747fa492d5426'),
(675, '2015-04-09 15:40:41', '2015-04-15 09:59:43', 10, NULL, 'AB0376-2/', 'MINIKIT 2015 - 2/3', 'minikit-2015-23', '140000.0040', '120689.6590', '0.0040', '0.0040', '1', 3, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '1', 0, 'dd747fa492d5426'),
(676, '2015-04-09 15:40:41', '2015-04-09 23:42:11', 10, NULL, 'AB0376-3/', 'MINIKIT 2015 - 3/4', 'minikit-2015-34', '140000.0040', '120689.6590', '0.0040', '0.0040', '1', 5, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '1', 0, 'dd747fa492d5426'),
(677, '2015-04-09 15:40:41', '2015-04-15 09:59:56', 10, NULL, 'AB0376-4/', 'MINIKIT 2015 - 4/5', 'minikit-2015-45', '140000.0040', '120689.6590', '0.0040', '0.0040', '1', 4, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '1', 0, 'dd747fa492d5426'),
(678, '2015-04-09 15:40:41', '2015-04-15 10:00:07', 10, NULL, 'AB0376-5/', 'MINIKIT 2015 - 5/6', 'minikit-2015-56', '140000.0040', '120689.6590', '0.0040', '0.0040', '1', 4, '1', '1', 'Minikit 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,220', '6', '14', '20', '1', 0, 'dd747fa492d5426'),
(679, '2015-04-09 15:40:41', '2015-04-15 09:34:43', 10, NULL, 'AB0339-XS', 'PANTALON TIPO YOGA GRIS JASPE - XS', 'pantalon-tipo-yoga-gris-jaspe-xs', '59900.0000', '51638.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'PANTALON TIPO YOGA GRIS JASPE', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '480', '1', '25', '36', '0', 0, 'b6b554fa6f5698b'),
(680, '2015-04-09 15:40:41', '2015-04-15 09:35:01', 10, NULL, 'AB0339-S', 'PANTALON TIPO YOGA GRIS JASPE - S', 'pantalon-tipo-yoga-gris-jaspe-s', '59900.0000', '51638.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'PANTALON TIPO YOGA GRIS JASPE', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '480', '1', '25', '36', '0', 0, 'b6b554fa6f5698b'),
(681, '2015-04-09 15:40:41', '2015-04-15 09:35:15', 10, NULL, 'AB0339-M', 'PANTALON TIPO YOGA GRIS JASPE - M', 'pantalon-tipo-yoga-gris-jaspe-m', '59900.0000', '51638.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'PANTALON TIPO YOGA GRIS JASPE', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '480', '1', '25', '36', '0', 0, 'b6b554fa6f5698b'),
(682, '2015-04-09 15:40:41', '2015-04-15 09:35:28', 10, NULL, 'AB0339-L', 'PANTALON TIPO YOGA GRIS JASPE - L', 'pantalon-tipo-yoga-gris-jaspe-l', '59900.0000', '51638.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'PANTALON TIPO YOGA GRIS JASPE', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '480', '1', '25', '36', '0', 0, 'b6b554fa6f5698b'),
(683, '2015-04-09 15:40:41', '2015-04-13 10:01:25', 10, NULL, 'AB0388-S', 'PANTALONETA ENTRENO 2015 - S', 'pantaloneta-entreno-2015-s', '64900.0000', '55948.2800', '0.0000', '0.0000', '1', 12, '1', '1', 'Pantaloneta entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '2f3b4fd4d08b7ad'),
(684, '2015-04-09 15:40:41', '2015-04-13 10:01:25', 10, NULL, 'AB0388-M', 'PANTALONETA ENTRENO 2015 - M', 'pantaloneta-entreno-2015-m', '64900.0000', '55948.2800', '0.0000', '0.0000', '1', 12, '1', '1', 'Pantaloneta entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '2f3b4fd4d08b7ad'),
(685, '2015-04-09 15:40:41', '2015-04-13 10:01:25', 10, NULL, 'AB0388-L', 'PANTALONETA ENTRENO 2015 - L', 'pantaloneta-entreno-2015-l', '64900.0000', '55948.2800', '0.0000', '0.0000', '1', 12, '1', '1', 'Pantaloneta entreno 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '2f3b4fd4d08b7ad'),
(686, '2015-04-09 15:40:41', '2015-04-13 10:02:11', 10, NULL, 'AB0381-S', 'PANTALONETA OFICIAL 2015 - S', 'pantaloneta-oficial-2015-s', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', 16, '1', '1', 'Pantaloneta oficial 2105', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '80fc8a9a7ad034e'),
(687, '2015-04-09 15:40:41', '2015-04-13 10:02:11', 10, NULL, 'AB0381-M', 'PANTALONETA OFICIAL 2015 - M', 'pantaloneta-oficial-2015-m', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', 16, '1', '1', 'Pantaloneta oficial 2105', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '80fc8a9a7ad034e'),
(688, '2015-04-09 15:40:41', '2015-04-13 10:02:11', 10, NULL, 'AB0381-L', 'PANTALONETA OFICIAL 2015 - L', 'pantaloneta-oficial-2015-l', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', 16, '1', '1', 'Pantaloneta oficial 2105', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '80fc8a9a7ad034e'),
(689, '2015-04-09 15:40:41', '2015-04-13 10:02:11', 10, NULL, 'AB0381-XL', 'PANTALONETA OFICIAL 2015 - XL', 'pantaloneta-oficial-2015-xl', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', 16, '1', '1', 'Pantaloneta oficial 2105', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '80fc8a9a7ad034e'),
(690, '2015-04-09 15:40:41', NULL, 10, NULL, 'AB0390-S', 'ROMPEVIENTOS 2015 - S', 'rompevientos-2015-s', '159900.0000', '137844.8300', '0.0000', '0.0000', '1', NULL, '1', '1', 'Rompevientos 2015', 1, 1, 5, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0,520', '4', '40', '29', '0', 0, 'ef5fe01026e300a'),
(691, '2015-04-09 15:40:41', '2015-04-15 09:27:55', 10, NULL, 'AB0390-M', 'ROMPEVIENTOS 2015 - M', 'rompevientos-2015-m', '159900.0000', '137845.0000', '0.0000', '0.0000', '1', 2, '1', '1', 'Rompevientos 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,520', '4', '40', '29', '0', 0, 'ef5fe01026e300a'),
(692, '2015-04-09 15:40:41', '2015-04-15 09:28:04', 10, NULL, 'AB0390-L', 'ROMPEVIENTOS 2015 - L', 'rompevientos-2015-l', '159900.0000', '137845.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Rompevientos 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,520', '4', '40', '29', '0', 0, 'ef5fe01026e300a'),
(693, '2015-04-09 15:40:41', '2015-04-15 09:28:13', 10, NULL, 'AB0390-XL', 'ROMPEVIENTOS 2015 - XL', 'rompevientos-2015-xl', '159900.0000', '137845.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Rompevientos 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,520', '4', '40', '29', '0', 0, 'ef5fe01026e300a'),
(694, '2015-04-09 15:40:41', '2015-04-09 22:26:34', 10, NULL, 'AB0352-XS', 'SUETER CAPOTA STYLE GRIS - XS', 'sueter-capota-style-gris-xs', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', 5, '1', '1', 'SUETER CAPOTA STYLE GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '400', '1', '25', '36', '0', 0, '498e1e5ca0dd2de'),
(695, '2015-04-09 15:40:41', '2015-04-09 22:26:34', 10, NULL, 'AB0352-S', 'SUETER CAPOTA STYLE GRIS - S', 'sueter-capota-style-gris-s', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', 5, '1', '1', 'SUETER CAPOTA STYLE GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '400', '1', '25', '36', '0', 0, '498e1e5ca0dd2de'),
(696, '2015-04-09 15:40:41', '2015-04-09 22:26:34', 10, NULL, 'AB0352-M', 'SUETER CAPOTA STYLE GRIS - M', 'sueter-capota-style-gris-m', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', 5, '1', '1', 'SUETER CAPOTA STYLE GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '400', '1', '25', '36', '0', 0, '498e1e5ca0dd2de'),
(697, '2015-04-09 15:40:41', '2015-04-09 22:26:34', 10, NULL, 'AB0352-L', 'SUETER CAPOTA STYLE GRIS - L', 'sueter-capota-style-gris-l', '69900.0000', '60258.6200', '0.0000', '0.0000', '1', 5, '1', '1', 'SUETER CAPOTA STYLE GRIS', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '400', '1', '25', '36', '0', 0, '498e1e5ca0dd2de'),
(698, '2015-04-09 15:40:41', '2015-04-15 09:03:42', 10, NULL, 'AB0223-S', 'SWEATER CUELLO EN V MUJER MFC - S', 'sweater-cuello-en-v-mujer-mfc-s', '83000.0040', '71551.7280', '0.0040', '0.0040', '1', 2, '1', '1', 'SWEATER CUELLO EN V MUJER MFC', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '200', '2', '25', '36', '0', 0, 'be17bb20dcee6b0'),
(699, '2015-04-09 15:40:41', '2015-04-15 09:03:50', 10, NULL, 'AB0223-M', 'SWEATER CUELLO EN V MUJER MFC - M', 'sweater-cuello-en-v-mujer-mfc-m', '83000.0040', '71551.7280', '0.0040', '0.0040', '1', 3, '1', '1', 'SWEATER CUELLO EN V MUJER MFC', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '200', '2', '25', '36', '0', 0, 'be17bb20dcee6b0'),
(700, '2015-04-09 15:40:41', '2015-04-15 09:03:59', 10, NULL, 'AB0223-L', 'SWEATER CUELLO EN V MUJER MFC - L', 'sweater-cuello-en-v-mujer-mfc-l', '83000.0040', '71551.7280', '0.0040', '0.0040', '1', 6, '1', '1', 'SWEATER CUELLO EN V MUJER MFC', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '200', '2', '25', '36', '0', 0, 'be17bb20dcee6b0'),
(701, '2015-04-09 15:40:41', '2015-04-15 09:04:06', 10, NULL, 'AB0223-XL', 'SWEATER CUELLO EN V MUJER MFC - XL', 'sweater-cuello-en-v-mujer-mfc-xl', '83000.0040', '71551.7280', '0.0040', '0.0040', '1', 4, '1', '1', 'SWEATER CUELLO EN V MUJER MFC', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '200', '2', '25', '36', '0', 0, 'be17bb20dcee6b0'),
(702, '2015-04-09 15:40:41', '2015-04-15 09:49:51', 10, NULL, 'AB0392-S', 'CHAQUETA TIPO FILLAT 2015  - S', 'chaqueta-tipo-fillat-2015-s', '239900.0000', '206810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Chaqueta de tipo gaban', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '1000', '18', '38', '38', '0', 0, 'a37396e68e736dd'),
(703, '2015-04-09 15:40:41', '2015-04-15 09:49:59', 10, NULL, 'AB0392-M', 'CHAQUETA TIPO FILLAT 2015  - M', 'chaqueta-tipo-fillat-2015-m', '239900.0000', '206810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Chaqueta de tipo gaban', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '1000', '18', '38', '38', '0', 0, 'a37396e68e736dd'),
(704, '2015-04-09 15:40:41', '2015-04-15 09:50:08', 10, NULL, 'AB0392-L', 'CHAQUETA TIPO FILLAT 2015  - L', 'chaqueta-tipo-fillat-2015-l', '239900.0000', '206810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Chaqueta de tipo gaban', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '1000', '18', '38', '38', '0', 0, 'a37396e68e736dd'),
(705, '2015-04-09 15:40:41', '2015-04-15 09:50:17', 10, NULL, 'AB0392-XL', 'CHAQUETA TIPO FILLAT 2015  - XL', 'chaqueta-tipo-fillat-2015-xl', '239900.0000', '206810.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Chaqueta de tipo gaban', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '1000', '18', '38', '38', '0', 0, 'a37396e68e736dd'),
(706, '2015-04-09 15:40:40', '2015-04-14 17:19:30', 10, NULL, 'AB0032-M', 'CAMISETA OFICIAL 2013 - M', 'camiseta-oficial-2013-m', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, 'b361821675bf2ac'),
(707, '2015-04-09 15:40:40', '2015-04-14 17:20:17', 10, NULL, 'AB0033-L', 'CAMISETA OFICIAL 2013 - L', 'camiseta-oficial-2013-l', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, 'b361821675bf2ac'),
(708, '2015-04-09 15:40:40', '2015-04-14 17:21:07', 10, NULL, 'AB0034-XL', 'CAMISETA OFICIAL 2013 - XL', 'camiseta-oficial-2013-xl', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Corta Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, 'b361821675bf2ac'),
(709, '2015-04-09 15:40:40', '2015-04-14 17:23:26', 10, NULL, 'AB0035-S', 'CAMISETA OFICIAL 2013 - S', 'camiseta-oficial-2013-s', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Larga Talla M', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, '387acbee569f0b0'),
(710, '2015-04-09 15:40:40', '2015-04-14 17:24:14', 10, NULL, 'AB0037-L', 'CAMISETA OFICIAL 2013 - L', 'camiseta-oficial-2013-l', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Larga Talla M', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, '387acbee569f0b0'),
(711, '2015-04-09 15:40:40', '2015-04-14 17:24:47', 10, NULL, 'AB0038-XL', 'CAMISETA OFICIAL 2013 - XL', 'camiseta-oficial-2013-xl', '19900.0000', '17155.0000', '0.0000', '0.0000', '1', 15, '1', '1', 'Camiseta Oficial 2013 Manga Larga Talla M', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,290', '1', '31', '29', '0', 0, '387acbee569f0b0'),
(712, '2015-04-09 15:40:41', '2015-04-14 16:39:40', 10, NULL, 'AB0302-M', 'CAMISETA VISITANTE 2015 MANGA CORTA  - M', 'camiseta-visitante-2015-manga-corta-m', '149900.0000', '129224.1400', '0.0000', '0.0000', '1', 5, '1', '1', 'Camiseta Visitante 2015 manga Corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '70f66b1584b3717'),
(713, '2015-04-09 15:40:41', '2015-04-14 16:39:40', 10, NULL, 'AB0302-L', 'CAMISETA VISITANTE 2015 MANGA CORTA  - L', 'camiseta-visitante-2015-manga-corta-l', '149900.0000', '129224.1400', '0.0000', '0.0000', '1', 5, '1', '1', 'Camiseta Visitante 2015 manga Corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '70f66b1584b3717'),
(714, '2015-04-09 15:40:41', '2015-04-14 16:39:40', 10, NULL, 'AB0302-XL', 'CAMISETA VISITANTE 2015 MANGA CORTA  - XL', 'camiseta-visitante-2015-manga-corta-xl', '149900.0000', '129224.1400', '0.0000', '0.0000', '1', 5, '1', '1', 'Camiseta Visitante 2015 manga Corta Talla s', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '0', 0, '70f66b1584b3717'),
(715, '2015-04-14 17:43:02', '2015-04-15 17:02:44', 13, 21, 'AB0154', 'LIBRO LA GRAN HISTORIA', 'libro-la-gran-historia', '150000.0000', '129310.0000', '0.0000', '0.0000', '1', 5, '1', '1', '', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, 'ea3d71e4f7a804d'),
(716, '2015-04-14 17:49:52', '2015-04-21 20:26:02', 13, 22, 'AB0383', 'GORRA MFC 2015 NEGRA', 'gorra-mfc-2015-negra', '32900.0000', '28362.0000', '0.0000', '0.0000', '1', 8, '1', '1', '', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, 'c9b6e2df7f827dc'),
(717, '2015-04-14 17:52:57', '2015-04-14 20:09:58', 13, 23, 'AB0383 BLANCA', 'GORRA MFC BLANCA', 'gorra-mfc-blanca', '32900.0000', '28362.0000', '0.0000', '0.0000', '1', 8, '1', '1', '', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, '563aea34e0bdf89'),
(718, '2015-04-09 15:40:41', '2015-04-09 23:02:17', 10, NULL, 'AB0377-XS', 'OFICIAL DAMA 2015 - XS', 'oficial-dama-2015-xs', '119900.0000', '103362.0700', '0.0000', '0.0000', '1', 4, '2', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '1', 0, '41b834dbcd3e16e'),
(719, '2015-04-09 15:40:41', '2015-04-09 23:02:17', 10, NULL, 'AB0377-S', 'OFICIAL DAMA 2015 - S', 'oficial-dama-2015-s', '119900.0000', '103362.0700', '0.0000', '0.0000', '1', 4, '2', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '1', 0, '41b834dbcd3e16e'),
(720, '2015-04-09 15:40:41', '2015-04-09 23:02:17', 10, NULL, 'AB0377-M', 'OFICIAL DAMA 2015 - M', 'oficial-dama-2015-m', '119900.0000', '103362.0700', '0.0000', '0.0000', '1', 4, '2', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '1', 0, '41b834dbcd3e16e'),
(721, '2015-04-09 15:40:41', '2015-04-09 23:02:17', 10, NULL, 'AB0377-L', 'OFICIAL DAMA 2015 - L', 'oficial-dama-2015-l', '119900.0000', '103362.0700', '0.0000', '0.0000', '1', 4, '2', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '1', 0, '41b834dbcd3e16e'),
(722, '2015-04-09 15:40:41', '2015-04-09 23:02:17', 10, NULL, 'AB0377-XL', 'OFICIAL DAMA 2015 - XL', 'oficial-dama-2015-xl', '119900.0000', '103362.0700', '0.0000', '0.0000', '1', 4, '2', '1', 'Oficial dama 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,180', '1', '31', '29', '1', 0, '41b834dbcd3e16e'),
(723, '2015-04-09 15:40:41', '2015-04-15 09:46:09', 10, NULL, 'AB0386-S', 'POLO BLANCA 2015 - S', 'polo-blanca-2015-s', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'Polo blanca 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '0', 0, '5d513f5e0f2df59'),
(724, '2015-04-09 15:40:41', '2015-04-15 09:46:17', 10, NULL, 'AB0386-M', 'POLO BLANCA 2015 - M', 'polo-blanca-2015-m', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 6, '1', '1', 'Polo blanca 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '0', 0, '5d513f5e0f2df59'),
(725, '2015-04-09 15:40:41', '2015-04-15 09:46:27', 10, NULL, 'AB0386-L', 'POLO BLANCA 2015 - L', 'polo-blanca-2015-l', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 5, '1', '1', 'Polo blanca 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '0', 0, '5d513f5e0f2df59'),
(726, '2015-04-09 15:40:41', '2015-04-15 09:46:35', 10, NULL, 'AB0386-XL', 'POLO BLANCA 2015 - XL', 'polo-blanca-2015-xl', '99900.0000', '86121.0000', '0.0000', '0.0000', '1', 1, '1', '1', 'Polo blanca 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,250', '2', '28', '29', '0', 0, '5d513f5e0f2df59'),
(727, '2015-04-13 17:39:08', NULL, 13, 20, 'AB0383 AZUL-TU', 'GORRA MFC 2015 - TU', 'gorra-mfc-2015-tu', '32900.0000', '28362.0700', '0.0000', '0.0000', '1', 8, '1', '1', NULL, 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, '9a4405d006c899c'),
(728, '2015-04-14 17:49:52', '2015-04-14 17:50:01', 13, 22, 'AB0383-TU', 'GORRA MFC 2015 NEGRA - TU', 'gorra-mfc-2015-negra-tu', '32900.0040', '28362.0730', '0.0040', '0.0040', '1', 8, '1', '1', '', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, 'c9b6e2df7f827dc'),
(729, '2015-04-14 17:52:57', NULL, 13, 23, 'AB0383 BLANCA-TU', 'GORRA MFC BLANCA - TU', 'gorra-mfc-blanca-tu', '32900.0000', '28362.0700', '0.0000', '0.0000', '1', 8, '1', '1', NULL, 1, 1, 6, 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, '563aea34e0bdf89'),
(730, '2015-04-09 15:40:41', '2015-04-14 19:07:32', 10, NULL, 'AB0306-S', 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - S', 'camiseta-visitante-2015-manga-larga-2015-s', '149900.0000', '129224.1400', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta Visitante 2015 Manga Larga Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '1', 0, 'b13833139b21f03'),
(731, '2015-04-09 15:40:41', '2015-04-14 19:07:32', 10, NULL, 'AB0306-M', 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - M', 'camiseta-visitante-2015-manga-larga-2015-m', '149900.0000', '129224.1400', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta Visitante 2015 Manga Larga Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '1', 0, 'b13833139b21f03'),
(732, '2015-04-09 15:40:41', '2015-04-14 20:16:40', 10, NULL, 'AB0308-L.', 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - L', 'camiseta-visitante-2015-manga-larga-2015-l', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta Visitante 2015 Manga Larga Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '1', 0, 'b13833139b21f03'),
(733, '2015-04-09 15:40:41', '2015-04-14 20:17:02', 10, NULL, 'AB0309-XL.', 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - XL', 'camiseta-visitante-2015-manga-larga-2015-xl', '149900.0000', '129224.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Camiseta Visitante 2015 Manga Larga Talla S', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,200', '1', '31', '29', '1', 0, 'b13833139b21f03'),
(734, '2015-04-15 09:17:06', '2015-04-15 09:21:58', 13, 24, 'AB0359', 'CAMISETA NIÑO LA PASION', 'camiseta-nino-la-pasion', '26700.0000', '23017.0000', '0.0000', '0.0000', '1', 10, '1', '1', '', 0, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, 'f25c98d847029d3'),
(735, '2015-04-15 09:17:06', '2015-04-15 09:19:40', 13, 24, 'AB0359-2/', 'CAMISETA NIÑO LA PASION - 2/4', 'camiseta-nino-la-pasion-24', '0.0000', '0.0000', '0.0000', '0.0000', '1', 20, '1', '1', '', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, 'f25c98d847029d3'),
(736, '2015-04-15 09:17:06', '2015-04-15 09:19:49', 13, 24, 'AB0359-6/', 'CAMISETA NIÑO LA PASION - 6/8', 'camiseta-nino-la-pasion-68', '0.0000', '0.0000', '0.0000', '0.0000', '1', 30, '1', '1', '', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, 'f25c98d847029d3'),
(737, '2015-04-15 09:17:06', '2015-04-15 09:19:59', 13, 24, 'AB0359-10', 'CAMISETA NIÑO LA PASION - 10/12', 'camiseta-nino-la-pasion-1012', '0.0000', '0.0000', '0.0000', '0.0000', '1', 14, '1', '1', '', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, 'f25c98d847029d3'),
(738, '2015-04-15 09:17:06', '2015-04-15 09:20:12', 13, 24, 'AB0359-10', 'CAMISETA NIÑO LA PASION - 10/16', 'camiseta-nino-la-pasion-1016', '0.0000', '0.0000', '0.0000', '0.0000', '1', 9, '1', '1', '', 1, 1, 6, 'n', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0', NULL, 'f25c98d847029d3');
INSERT INTO `default_firesale_products` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `code`, `title`, `slug`, `price`, `price_tax`, `rrp`, `rrp_tax`, `status`, `stock`, `stock_status`, `ship_req`, `description`, `is_variation`, `tax_band`, `brand`, `digital`, `download`, `downloads`, `expires_after`, `expires_interval`, `meta_title`, `meta_description`, `meta_keywords`, `shipping_weight`, `shipping_height`, `shipping_width`, `shipping_depth`, `featured`, `descuento`, `main_image`) VALUES
(739, '2015-04-09 15:40:41', '2015-04-15 09:57:33', 10, NULL, 'AB0382-S', 'PANTALONETA VISITANTE 2015 - S', 'pantaloneta-visitante-2015-s', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta visitante 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '72d15b7e3bc1714'),
(740, '2015-04-09 15:40:41', '2015-04-15 09:57:41', 10, NULL, 'AB0382-M', 'PANTALONETA VISITANTE 2015 - M', 'pantaloneta-visitante-2015-m', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta visitante 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '72d15b7e3bc1714'),
(741, '2015-04-09 15:40:41', '2015-04-15 09:57:50', 10, NULL, 'AB0382-L', 'PANTALONETA VISITANTE 2015 - L', 'pantaloneta-visitante-2015-l', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta visitante 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '72d15b7e3bc1714'),
(742, '2015-04-09 15:40:41', '2015-04-15 09:57:49', 10, NULL, 'AB0382-XL', 'PANTALONETA VISITANTE 2015 - XL', 'pantaloneta-visitante-2015-xl', '69900.0000', '60259.0000', '0.0000', '0.0000', '1', 4, '1', '1', 'Pantaloneta visitante 2015', 1, 1, 5, 'n', NULL, NULL, NULL, '', NULL, '', NULL, '0,190', '2', '28', '29', '0', 0, '72d15b7e3bc1714');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_products_firesale_categories`
--

CREATE TABLE IF NOT EXISTS `default_firesale_products_firesale_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `firesale_products_id` int(11) NOT NULL,
  `firesale_categories_id` int(11) NOT NULL,
  `orden` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2525 ;

--
-- Volcado de datos para la tabla `default_firesale_products_firesale_categories`
--

INSERT INTO `default_firesale_products_firesale_categories` (`id`, `row_id`, `firesale_products_id`, `firesale_categories_id`, `orden`) VALUES
(700, 366, 299, 3, NULL),
(701, 366, 299, 10, NULL),
(708, 367, 3, 3, 13),
(709, 367, 3, 10, NULL),
(710, 368, 3, 3, 14),
(711, 368, 3, 10, NULL),
(712, 369, 3, 3, 15),
(713, 369, 3, 10, NULL),
(716, 370, 300, 3, NULL),
(717, 370, 300, 10, NULL),
(722, 372, 301, 3, NULL),
(723, 372, 301, 10, NULL),
(728, 375, 301, 3, NULL),
(729, 375, 301, 10, NULL),
(730, 374, 3, 3, 18),
(731, 374, 3, 10, NULL),
(738, 376, 3, 3, 19),
(739, 376, 3, 10, NULL),
(742, 377, 3, 3, 21),
(743, 377, 3, 10, NULL),
(744, 378, 3, 3, 22),
(745, 378, 3, 10, NULL),
(755, 379, 3, 3, 23),
(756, 379, 3, 13, NULL),
(757, 382, 3, 3, 24),
(758, 382, 3, 13, NULL),
(759, 380, 3, 3, 25),
(760, 380, 3, 13, NULL),
(761, 381, 3, 3, 26),
(762, 381, 3, 13, NULL),
(777, 387, 315, 3, NULL),
(778, 387, 315, 10, NULL),
(787, 390, 3, 3, 30),
(788, 390, 3, 10, NULL),
(789, 391, 3, 3, 31),
(790, 391, 3, 10, NULL),
(791, 389, 3, 3, 32),
(792, 389, 3, 10, NULL),
(815, 394, 3, 1, 5),
(816, 394, 3, 6, NULL),
(819, 397, 3, 1, 8),
(820, 397, 3, 6, NULL),
(837, 401, 3, 3, 36),
(838, 401, 3, 13, NULL),
(839, 402, 3, 3, 37),
(840, 402, 3, 13, NULL),
(841, 403, 3, 3, 38),
(842, 403, 3, 13, NULL),
(855, 404, 3, 3, 40),
(856, 404, 3, 10, NULL),
(857, 406, 3, 3, 41),
(858, 406, 3, 10, NULL),
(859, 407, 3, 3, 42),
(860, 407, 3, 10, NULL),
(861, 408, 3, 3, 43),
(862, 408, 3, 10, NULL),
(875, 409, 3, 3, 45),
(876, 409, 3, 10, NULL),
(877, 410, 3, 3, 46),
(878, 410, 3, 10, NULL),
(879, 412, 3, 3, 47),
(880, 412, 3, 10, NULL),
(881, 413, 3, 3, 48),
(882, 413, 3, 10, NULL),
(905, 421, 328, 3, NULL),
(906, 421, 328, 10, NULL),
(907, 418, 3, 3, 53),
(908, 418, 3, 10, NULL),
(909, 419, 3, 3, 54),
(910, 419, 3, 10, NULL),
(911, 420, 3, 3, 55),
(912, 420, 3, 10, NULL),
(957, 430, 3, 1, 12),
(958, 430, 3, 6, NULL),
(959, 432, 3, 1, 13),
(960, 432, 3, 6, NULL),
(961, 433, 3, 1, 14),
(962, 433, 3, 6, NULL),
(963, 434, 3, 1, 15),
(964, 434, 3, 6, NULL),
(965, 435, 3, 1, 16),
(966, 435, 3, 6, NULL),
(977, 436, 3, 4, 1),
(978, 436, 3, 11, NULL),
(979, 437, 3, 4, 2),
(980, 437, 3, 11, NULL),
(981, 438, 3, 4, 3),
(982, 438, 3, 11, NULL),
(983, 439, 3, 4, 4),
(984, 439, 3, 11, NULL),
(985, 440, 3, 4, 5),
(986, 440, 3, 11, NULL),
(1027, 449, 3, 3, 72),
(1028, 449, 3, 10, NULL),
(1029, 450, 3, 3, 73),
(1030, 450, 3, 10, NULL),
(1031, 451, 3, 3, 74),
(1032, 451, 3, 10, NULL),
(1033, 452, 3, 3, 75),
(1034, 452, 3, 10, NULL),
(1045, 453, 3, 3, 77),
(1046, 453, 3, 10, NULL),
(1047, 454, 3, 3, 78),
(1048, 454, 3, 10, NULL),
(1049, 455, 3, 3, 79),
(1050, 455, 3, 10, NULL),
(1051, 456, 3, 3, 80),
(1052, 456, 3, 10, NULL),
(1095, 466, 3, 3, 89),
(1096, 466, 3, 10, NULL),
(1101, 469, 3, 3, 92),
(1102, 469, 3, 10, NULL),
(1113, 471, 3, 4, 7),
(1114, 471, 3, 11, NULL),
(1115, 472, 3, 4, 8),
(1116, 472, 3, 11, NULL),
(1117, 473, 3, 4, 9),
(1118, 473, 3, 11, NULL),
(1119, 470, 3, 4, 10),
(1120, 470, 3, 11, NULL),
(1129, 474, 3, 1, 17),
(1130, 474, 3, 7, NULL),
(1131, 475, 3, 1, 18),
(1132, 475, 3, 7, NULL),
(1133, 476, 3, 1, 19),
(1134, 476, 3, 7, NULL),
(1135, 477, 3, 1, 20),
(1136, 477, 3, 7, NULL),
(1139, 479, 346, 1, NULL),
(1140, 479, 346, 7, NULL),
(1141, 480, 346, 1, NULL),
(1142, 480, 346, 7, NULL),
(1143, 481, 346, 1, NULL),
(1144, 481, 346, 7, NULL),
(1145, 478, 3, 1, 21),
(1146, 478, 3, 7, NULL),
(1155, 482, 3, 1, 22),
(1156, 482, 3, 7, NULL),
(1157, 483, 3, 1, 23),
(1158, 483, 3, 7, NULL),
(1159, 484, 3, 1, 24),
(1160, 484, 3, 7, NULL),
(1161, 485, 3, 1, 25),
(1162, 485, 3, 7, NULL),
(1171, 486, 3, 1, 26),
(1172, 486, 3, 7, NULL),
(1173, 487, 3, 1, 27),
(1174, 487, 3, 7, NULL),
(1175, 488, 3, 1, 28),
(1176, 488, 3, 7, NULL),
(1177, 489, 3, 1, 29),
(1178, 489, 3, 7, NULL),
(1211, 497, 3, 1, 38),
(1212, 497, 3, 7, NULL),
(1217, 499, 351, 1, NULL),
(1218, 499, 351, 7, NULL),
(1219, 500, 351, 1, NULL),
(1220, 500, 351, 7, NULL),
(1221, 501, 351, 1, NULL),
(1222, 501, 351, 7, NULL),
(1223, 502, 351, 1, NULL),
(1224, 502, 351, 7, NULL),
(1225, 503, 351, 1, NULL),
(1226, 503, 351, 7, NULL),
(1237, 504, 3, 1, 40),
(1238, 504, 3, 7, NULL),
(1239, 505, 3, 1, 41),
(1240, 505, 3, 7, NULL),
(1241, 506, 3, 1, 42),
(1242, 506, 3, 7, NULL),
(1243, 507, 3, 1, 43),
(1244, 507, 3, 7, NULL),
(1245, 508, 3, 1, 44),
(1246, 508, 3, 7, NULL),
(1257, 510, 3, 1, 46),
(1258, 510, 3, 7, NULL),
(1259, 511, 3, 1, 47),
(1260, 511, 3, 7, NULL),
(1261, 512, 3, 1, 48),
(1262, 512, 3, 7, NULL),
(1269, 490, 3, 1, 49),
(1270, 490, 3, 7, NULL),
(1278, 491, 3, 1, 51),
(1279, 491, 3, 7, NULL),
(1280, 492, 3, 1, 52),
(1281, 492, 3, 7, NULL),
(1282, 493, 3, 1, 53),
(1283, 493, 3, 7, NULL),
(1288, 495, 3, 1, 56),
(1289, 495, 3, 7, NULL),
(1290, 496, 3, 1, 57),
(1291, 496, 3, 7, NULL),
(1292, 498, 3, 1, 58),
(1293, 498, 3, 7, NULL),
(1294, 494, 3, 1, 59),
(1295, 494, 3, 7, NULL),
(1296, 363, 3, 3, 95),
(1297, 363, 3, 10, NULL),
(1298, 364, 3, 3, 96),
(1299, 364, 3, 10, NULL),
(1315, 396, 3, 1, 60),
(1316, 396, 3, 6, NULL),
(1318, 398, 3, 1, 61),
(1319, 398, 3, 7, NULL),
(1320, 399, 3, 1, 62),
(1321, 399, 3, 7, NULL),
(1322, 400, 3, 1, 63),
(1323, 400, 3, 7, NULL),
(1324, 383, 3, 3, 102),
(1325, 383, 3, 10, NULL),
(1326, 384, 3, 3, 103),
(1327, 384, 3, 10, NULL),
(1328, 392, 3, 3, 104),
(1329, 392, 3, 10, NULL),
(1330, 393, 3, 3, 105),
(1331, 393, 3, 10, NULL),
(1332, 422, 3, 3, 106),
(1333, 422, 3, 10, NULL),
(1336, 423, 3, 3, 107),
(1337, 423, 3, 10, NULL),
(1338, 424, 3, 3, 108),
(1339, 424, 3, 10, NULL),
(1340, 425, 3, 3, 109),
(1341, 425, 3, 10, NULL),
(1342, 467, 3, 3, 110),
(1343, 467, 3, 10, NULL),
(1344, 468, 3, 3, 111),
(1345, 468, 3, 10, NULL),
(1346, 414, 3, 3, 112),
(1347, 414, 3, 10, NULL),
(1348, 415, 3, 3, 113),
(1349, 415, 3, 10, NULL),
(1350, 416, 3, 3, 114),
(1351, 416, 3, 10, NULL),
(1352, 417, 3, 3, 115),
(1353, 417, 3, 10, NULL),
(1362, 513, 3, 3, 116),
(1363, 513, 3, 10, NULL),
(1364, 515, 3, 3, 117),
(1365, 515, 3, 10, NULL),
(1366, 516, 3, 3, 118),
(1367, 516, 3, 10, NULL),
(1368, 426, 3, 3, 119),
(1369, 426, 3, 10, NULL),
(1370, 427, 3, 3, 120),
(1371, 427, 3, 10, NULL),
(1372, 428, 3, 3, 121),
(1373, 428, 3, 10, NULL),
(1374, 429, 3, 3, 122),
(1375, 429, 3, 10, NULL),
(1376, 441, 3, 3, 123),
(1377, 441, 3, 10, NULL),
(1378, 442, 3, 3, 124),
(1379, 442, 3, 10, NULL),
(1380, 443, 3, 3, 125),
(1381, 443, 3, 10, NULL),
(1382, 444, 3, 3, 126),
(1383, 444, 3, 10, NULL),
(1386, 445, 3, 3, 127),
(1387, 445, 3, 10, NULL),
(1390, 446, 3, 3, 128),
(1391, 446, 3, 10, NULL),
(1392, 447, 3, 3, 129),
(1393, 447, 3, 10, NULL),
(1394, 448, 3, 3, 130),
(1395, 448, 3, 10, NULL),
(1396, 457, 3, 3, 131),
(1397, 457, 3, 10, NULL),
(1400, 458, 3, 3, 132),
(1401, 458, 3, 10, NULL),
(1404, 459, 3, 3, 133),
(1405, 459, 3, 10, NULL),
(1406, 460, 3, 3, 134),
(1407, 460, 3, 10, NULL),
(1408, 461, 3, 3, 135),
(1409, 461, 3, 10, NULL),
(1410, 462, 3, 3, 136),
(1411, 462, 3, 10, NULL),
(1414, 464, 3, 3, 137),
(1415, 464, 3, 10, NULL),
(1416, 465, 3, 3, 138),
(1417, 465, 3, 10, NULL),
(1418, 509, 3, 1, 64),
(1419, 509, 3, 7, NULL),
(1477, 524, 524, 16, NULL),
(1479, 526, 526, 16, NULL),
(1481, 528, 528, 16, NULL),
(1484, 531, 531, 16, NULL),
(1485, 532, 532, 16, NULL),
(1492, 537, 537, 16, NULL),
(1493, 538, 538, 16, NULL),
(1498, 541, 541, 14, NULL),
(1499, 542, 542, 14, NULL),
(1558, 574, 574, 16, NULL),
(1559, 575, 575, 16, NULL),
(1560, 576, 576, 16, NULL),
(1562, 578, 578, 16, NULL),
(1581, 565, 3, 1, 68),
(1582, 565, 3, 7, NULL),
(1583, 570, 3, 1, 9),
(1584, 570, 3, 7, NULL),
(1609, 529, 3, 16, 3),
(1678, 525, 3, 16, 4),
(1686, 579, 3, 16, 6),
(1687, 583, 3, 1, 74),
(1688, 583, 3, 7, NULL),
(1689, 584, 3, 1, 74),
(1690, 584, 3, 7, NULL),
(1691, 585, 3, 1, 74),
(1692, 585, 3, 7, NULL),
(1693, 586, 3, 1, 74),
(1694, 586, 3, 7, NULL),
(1737, 603, 3, 1, 69),
(1738, 603, 3, 6, NULL),
(1739, 604, 3, 1, 69),
(1740, 604, 3, 6, NULL),
(1741, 605, 3, 1, 69),
(1742, 605, 3, 6, NULL),
(1743, 606, 3, 1, 69),
(1744, 606, 3, 6, NULL),
(1745, 539, 3, 1, 78),
(1746, 539, 3, 6, NULL),
(1747, 607, 3, 3, 155),
(1748, 607, 3, 10, NULL),
(1749, 608, 3, 3, 155),
(1750, 608, 3, 10, NULL),
(1751, 609, 3, 3, 155),
(1752, 609, 3, 10, NULL),
(1753, 610, 3, 3, 155),
(1754, 610, 3, 10, NULL),
(1759, 611, 544, 3, NULL),
(1760, 611, 544, 10, NULL),
(1761, 612, 544, 3, NULL),
(1762, 612, 544, 10, NULL),
(1763, 613, 544, 3, NULL),
(1764, 613, 544, 10, NULL),
(1765, 614, 544, 3, NULL),
(1766, 614, 544, 10, NULL),
(1769, 615, 543, 3, NULL),
(1770, 615, 543, 13, NULL),
(1771, 616, 543, 3, NULL),
(1772, 616, 543, 13, NULL),
(1773, 617, 543, 3, NULL),
(1774, 617, 543, 13, NULL),
(1775, 618, 543, 3, NULL),
(1776, 618, 543, 13, NULL),
(1777, 543, 3, 3, 169),
(1778, 543, 3, 13, NULL),
(1779, 619, 3, 1, 73),
(1780, 619, 3, 7, NULL),
(1781, 620, 3, 1, 73),
(1782, 620, 3, 7, NULL),
(1783, 621, 3, 1, 73),
(1784, 621, 3, 7, NULL),
(1785, 622, 3, 1, 73),
(1786, 622, 3, 7, NULL),
(1789, 623, 3, 1, 4),
(1790, 623, 3, 7, 1),
(1791, 624, 3, 1, 4),
(1792, 624, 3, 7, 1),
(1793, 625, 3, 1, 4),
(1794, 625, 3, 7, 1),
(1795, 626, 3, 1, 4),
(1796, 626, 3, 7, 1),
(1823, 635, 3, 3, 159),
(1824, 635, 3, 10, NULL),
(1825, 636, 3, 3, 159),
(1826, 636, 3, 10, NULL),
(1827, 637, 3, 3, 159),
(1828, 637, 3, 10, NULL),
(1829, 638, 3, 3, 159),
(1830, 638, 3, 10, NULL),
(1833, 639, 3, 3, 158),
(1834, 639, 3, 10, NULL),
(1835, 640, 3, 3, 158),
(1836, 640, 3, 10, NULL),
(1837, 641, 3, 3, 158),
(1838, 641, 3, 10, NULL),
(1839, 642, 3, 3, 158),
(1840, 642, 3, 10, NULL),
(1841, 560, 3, 3, 171),
(1842, 560, 3, 10, NULL),
(1853, 548, 3, 3, 4),
(1854, 548, 3, 10, NULL),
(1863, 547, 3, 3, 173),
(1864, 547, 3, 10, NULL),
(1875, 655, 518, 3, NULL),
(1876, 655, 518, 10, NULL),
(1883, 544, 3, 3, 177),
(1884, 544, 3, 10, NULL),
(1913, 563, 3, 4, 14),
(1914, 563, 3, 11, NULL),
(1925, 569, 3, 1, 83),
(1926, 569, 3, 7, NULL),
(1927, 568, 3, 1, 84),
(1928, 568, 3, 7, NULL),
(1929, 566, 3, 1, 85),
(1930, 566, 3, 7, NULL),
(1939, 567, 3, 1, 86),
(1940, 567, 3, 7, NULL),
(1945, 676, 3, 4, 12),
(1946, 676, 3, 11, NULL),
(1965, 683, 3, 3, 148),
(1966, 683, 3, 10, NULL),
(1967, 684, 3, 3, 148),
(1968, 684, 3, 10, NULL),
(1969, 685, 3, 3, 148),
(1970, 685, 3, 10, NULL),
(1973, 686, 3, 3, 149),
(1974, 686, 3, 10, NULL),
(1975, 687, 3, 3, 149),
(1976, 687, 3, 10, NULL),
(1977, 688, 3, 3, 149),
(1978, 688, 3, 10, NULL),
(1979, 689, 3, 3, 149),
(1980, 689, 3, 10, NULL),
(1983, 557, 3, 3, 3),
(1984, 557, 3, 10, NULL),
(1997, 694, 3, 1, 9),
(1998, 694, 3, 7, NULL),
(1999, 695, 3, 1, 9),
(2000, 695, 3, 7, NULL),
(2001, 696, 3, 1, 9),
(2002, 696, 3, 7, NULL),
(2003, 697, 3, 1, 9),
(2004, 697, 3, 7, NULL),
(2039, 549, 3, 3, 185),
(2040, 549, 3, 10, NULL),
(2043, 706, 3, 3, 186),
(2044, 706, 3, 10, NULL),
(2047, 707, 3, 3, 187),
(2048, 707, 3, 10, NULL),
(2051, 708, 3, 3, 188),
(2052, 708, 3, 10, NULL),
(2059, 656, 3, 3, 189),
(2060, 656, 3, 10, NULL),
(2063, 709, 3, 3, 191),
(2064, 709, 3, 10, NULL),
(2065, 710, 3, 3, 192),
(2066, 710, 3, 10, NULL),
(2067, 711, 3, 3, 193),
(2068, 711, 3, 10, NULL),
(2083, 718, 3, 1, 1),
(2084, 718, 3, 6, 1),
(2085, 719, 3, 1, 1),
(2086, 719, 3, 6, 1),
(2087, 720, 3, 1, 1),
(2088, 720, 3, 6, 1),
(2089, 721, 3, 1, 1),
(2090, 721, 3, 6, 1),
(2091, 722, 3, 1, 1),
(2092, 722, 3, 6, 1),
(2093, 552, 3, 1, 89),
(2094, 552, 3, 6, NULL),
(2095, 564, 3, 1, 90),
(2096, 564, 3, 7, NULL),
(2103, 533, 3, 3, 195),
(2104, 533, 3, 10, NULL),
(2113, 521, 3, 3, 197),
(2114, 521, 3, 10, NULL),
(2117, 520, 3, 3, 198),
(2118, 520, 3, 10, NULL),
(2129, 558, 3, 3, 200),
(2130, 558, 3, 10, NULL),
(2131, 554, 3, 3, 201),
(2132, 554, 3, 10, NULL),
(2135, 555, 3, 3, 202),
(2136, 555, 3, 10, NULL),
(2139, 556, 3, 3, 204),
(2140, 556, 3, 10, NULL),
(2141, 727, 3, 3, 154),
(2142, 582, 3, 3, 205),
(2143, 728, 3, 3, 194),
(2144, 728, 3, 13, NULL),
(2147, 729, 3, 13, 2),
(2148, 729, 3, 3, NULL),
(2149, 717, 3, 13, 3),
(2150, 717, 3, 3, NULL),
(2151, 730, 3, 3, 196),
(2152, 730, 3, 10, NULL),
(2159, 732, 3, 3, 207),
(2160, 732, 3, 10, NULL),
(2161, 733, 3, 3, 208),
(2162, 733, 3, 10, NULL),
(2163, 534, 3, 3, 209),
(2164, 534, 3, 10, NULL),
(2167, 602, 3, 3, 210),
(2168, 602, 3, 10, NULL),
(2169, 601, 3, 3, 211),
(2170, 601, 3, 10, NULL),
(2171, 600, 3, 3, 212),
(2172, 600, 3, 10, NULL),
(2173, 599, 3, 3, 213),
(2174, 599, 3, 10, NULL),
(2175, 658, 3, 3, 214),
(2176, 658, 3, 10, NULL),
(2177, 659, 3, 3, 215),
(2178, 659, 3, 10, NULL),
(2179, 660, 3, 3, 216),
(2180, 660, 3, 10, NULL),
(2181, 661, 3, 3, 217),
(2182, 661, 3, 10, NULL),
(2183, 595, 3, 3, 218),
(2184, 595, 3, 13, NULL),
(2185, 596, 3, 3, 219),
(2186, 596, 3, 13, NULL),
(2187, 597, 3, 3, 220),
(2188, 597, 3, 13, NULL),
(2189, 598, 3, 3, 221),
(2190, 598, 3, 13, NULL),
(2192, 698, 3, 1, 91),
(2193, 698, 3, 7, NULL),
(2194, 699, 3, 1, 92),
(2195, 699, 3, 7, NULL),
(2196, 700, 3, 1, 93),
(2197, 700, 3, 7, NULL),
(2198, 701, 3, 1, 94),
(2199, 701, 3, 7, NULL),
(2200, 657, 3, 3, 222),
(2201, 657, 3, 10, NULL),
(2202, 651, 3, 3, 223),
(2203, 651, 3, 10, NULL),
(2204, 652, 3, 3, 224),
(2205, 652, 3, 10, NULL),
(2206, 653, 3, 3, 225),
(2207, 653, 3, 10, NULL),
(2208, 654, 3, 3, 226),
(2209, 654, 3, 10, NULL),
(2210, 647, 3, 3, 227),
(2211, 647, 3, 10, NULL),
(2212, 648, 3, 3, 228),
(2213, 648, 3, 10, NULL),
(2214, 649, 3, 3, 229),
(2215, 649, 3, 10, NULL),
(2216, 650, 3, 3, 230),
(2217, 650, 3, 10, NULL),
(2218, 643, 3, 3, 231),
(2219, 643, 3, 10, NULL),
(2220, 644, 3, 3, 232),
(2221, 644, 3, 10, NULL),
(2222, 645, 3, 3, 233),
(2223, 645, 3, 10, NULL),
(2224, 646, 3, 3, 234),
(2225, 646, 3, 10, NULL),
(2228, 628, 3, 1, 96),
(2229, 628, 3, 7, NULL),
(2232, 630, 3, 1, 97),
(2233, 630, 3, 7, NULL),
(2234, 627, 3, 1, 98),
(2235, 627, 3, 7, NULL),
(2236, 629, 3, 1, 99),
(2237, 629, 3, 7, NULL),
(2248, 735, 3, 14, 2),
(2249, 735, 3, 4, NULL),
(2250, 736, 3, 14, 3),
(2251, 736, 3, 4, NULL),
(2252, 737, 3, 14, 4),
(2253, 737, 3, 4, NULL),
(2254, 738, 3, 14, 5),
(2255, 738, 3, 4, NULL),
(2258, 734, 3, 14, 6),
(2259, 734, 3, 4, NULL),
(2260, 561, 3, 3, 235),
(2261, 561, 3, 10, NULL),
(2266, 691, 3, 3, 238),
(2267, 691, 3, 10, NULL),
(2268, 692, 3, 3, 239),
(2269, 692, 3, 10, NULL),
(2270, 693, 3, 3, 240),
(2271, 693, 3, 10, NULL),
(2274, 587, 3, 1, 100),
(2275, 587, 3, 7, NULL),
(2276, 588, 3, 1, 101),
(2277, 588, 3, 7, NULL),
(2278, 589, 3, 1, 102),
(2279, 589, 3, 7, NULL),
(2280, 590, 3, 1, 103),
(2281, 590, 3, 7, NULL),
(2282, 679, 3, 1, 104),
(2283, 679, 3, 7, NULL),
(2284, 680, 3, 1, 105),
(2285, 680, 3, 7, NULL),
(2286, 681, 3, 1, 106),
(2287, 681, 3, 7, NULL),
(2288, 682, 3, 1, 107),
(2289, 682, 3, 7, NULL),
(2290, 591, 3, 1, 108),
(2291, 591, 3, 7, NULL),
(2292, 592, 3, 1, 109),
(2293, 592, 3, 7, NULL),
(2294, 593, 3, 1, 110),
(2295, 593, 3, 7, NULL),
(2296, 594, 3, 1, 111),
(2297, 594, 3, 7, NULL),
(2298, 670, 3, 1, 112),
(2299, 670, 3, 7, NULL),
(2300, 671, 3, 1, 113),
(2301, 671, 3, 7, NULL),
(2302, 672, 3, 1, 114),
(2303, 672, 3, 7, NULL),
(2304, 673, 3, 1, 115),
(2305, 673, 3, 7, NULL),
(2306, 723, 3, 3, 242),
(2307, 723, 3, 10, NULL),
(2308, 724, 3, 3, 243),
(2309, 724, 3, 10, NULL),
(2310, 725, 3, 3, 244),
(2311, 725, 3, 10, NULL),
(2312, 726, 3, 3, 245),
(2313, 726, 3, 10, NULL),
(2314, 631, 3, 3, 246),
(2315, 631, 3, 10, NULL),
(2316, 632, 3, 3, 247),
(2317, 632, 3, 10, NULL),
(2318, 633, 3, 3, 248),
(2319, 633, 3, 10, NULL),
(2320, 634, 3, 3, 249),
(2321, 634, 3, 10, NULL),
(2322, 702, 3, 3, 250),
(2323, 702, 3, 10, NULL),
(2324, 703, 3, 3, 251),
(2325, 703, 3, 10, NULL),
(2326, 704, 3, 3, 252),
(2327, 704, 3, 10, NULL),
(2328, 705, 3, 3, 253),
(2329, 705, 3, 10, NULL),
(2330, 662, 3, 4, 16),
(2331, 662, 3, 11, NULL),
(2332, 663, 3, 4, 17),
(2333, 663, 3, 11, NULL),
(2334, 664, 3, 4, 18),
(2335, 664, 3, 11, NULL),
(2336, 665, 3, 4, 19),
(2337, 665, 3, 11, NULL),
(2340, 559, 3, 3, 255),
(2341, 559, 3, 10, NULL),
(2350, 739, 3, 3, 256),
(2351, 739, 3, 10, NULL),
(2352, 740, 3, 3, 257),
(2353, 740, 3, 10, NULL),
(2354, 742, 3, 3, 258),
(2355, 742, 3, 10, NULL),
(2356, 741, 3, 3, 259),
(2357, 741, 3, 10, NULL),
(2358, 674, 3, 4, 20),
(2359, 674, 3, 11, NULL),
(2360, 675, 3, 4, 21),
(2361, 675, 3, 11, NULL),
(2362, 677, 3, 4, 22),
(2363, 677, 3, 11, NULL),
(2364, 678, 3, 4, 23),
(2365, 678, 3, 11, NULL),
(2366, 581, 3, 4, 24),
(2367, 581, 3, 3, NULL),
(2376, 715, 3, 13, 4),
(2377, 715, 3, 16, NULL),
(2378, 522, 3, 16, 8),
(2379, 523, 3, 3, 260),
(2380, 523, 3, 13, NULL),
(2389, 518, 3, 3, 261),
(2390, 518, 3, 10, NULL),
(2411, 743, 3, 4, 26),
(2412, 743, 3, 1, NULL),
(2423, 667, 3, 3, 264),
(2424, 667, 3, 10, NULL),
(2437, 517, 3, 3, 266),
(2438, 517, 3, 10, NULL),
(2441, 668, 3, 3, 268),
(2442, 668, 3, 10, NULL),
(2443, 669, 3, 3, 269),
(2444, 669, 3, 10, NULL),
(2451, 666, 3, 3, 271),
(2452, 666, 3, 10, NULL),
(2463, 562, 3, 3, 272),
(2464, 562, 3, 10, NULL),
(2465, 553, 3, 4, 27),
(2466, 553, 3, 11, NULL),
(2469, 573, 3, 1, 118),
(2470, 573, 3, 7, NULL),
(2475, 546, 3, 3, 273),
(2476, 546, 3, 10, NULL),
(2477, 716, 3, 3, 274),
(2478, 716, 3, 13, NULL),
(2485, 744, 3, 3, 275),
(2486, 744, 3, 13, NULL),
(2499, 571, 3, 1, 120),
(2500, 571, 3, 7, NULL),
(2511, 572, 3, 1, 121),
(2512, 572, 3, 7, NULL),
(2523, 545, 3, 3, 276),
(2524, 545, 3, 10, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_product_modifiers`
--

CREATE TABLE IF NOT EXISTS `default_firesale_product_modifiers` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructions` longtext COLLATE utf8_unicode_ci,
  `parent` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=99 ;

--
-- Volcado de datos para la tabla `default_firesale_product_modifiers`
--

INSERT INTO `default_firesale_product_modifiers` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `type`, `title`, `instructions`, `parent`) VALUES
(17, '2015-03-13 16:05:23', NULL, 13, 1, '1', 'Tallas:', NULL, 298),
(18, '2015-03-13 16:16:59', NULL, 13, 2, '1', 'Talla:', NULL, 299),
(19, '2015-03-13 16:20:16', NULL, 13, 3, '1', 'Talla:', NULL, 300),
(20, '2015-03-13 16:25:51', NULL, 13, 4, '1', 'Talla:', NULL, 301),
(21, '2015-03-13 16:31:16', NULL, 13, 5, '1', 'Talla:', NULL, 302),
(22, '2015-03-13 16:37:58', NULL, 13, 6, '1', 'Talla:', NULL, 304),
(23, '2015-03-13 16:46:05', NULL, 13, 7, '1', 'Talla:', NULL, 314),
(24, '2015-03-13 16:55:55', NULL, 13, 8, '1', 'Talla:', NULL, 315),
(25, '2015-03-13 17:11:14', NULL, 13, 9, '1', 'Talla:', NULL, 320),
(26, '2015-03-13 17:14:39', NULL, 13, 10, '1', 'Talla:', NULL, 321),
(27, '2015-03-13 17:17:58', NULL, 13, 11, '1', 'Talla', NULL, 324),
(28, '2015-03-13 17:22:11', NULL, 13, 12, '1', 'Talla', NULL, 325),
(29, '2015-03-13 17:26:52', NULL, 13, 13, '1', 'Talla:', NULL, 326),
(30, '2015-03-13 17:31:35', NULL, 13, 14, '1', 'Talla:', NULL, 327),
(31, '2015-03-13 17:35:03', NULL, 13, 15, '1', 'Talla:', NULL, 328),
(32, '2015-03-13 17:39:00', NULL, 13, 16, '1', 'Talla', NULL, 329),
(33, '2015-03-13 17:41:15', NULL, 13, 17, '1', 'Talla:', NULL, 330),
(34, '2015-03-13 17:44:26', NULL, 13, 18, '1', 'Talla:', NULL, 333),
(35, '2015-03-13 17:48:11', NULL, 13, 19, '1', 'Talla:', NULL, 334),
(36, '2015-03-13 17:52:01', NULL, 13, 20, '1', 'Talla:', NULL, 335),
(37, '2015-03-13 17:55:00', NULL, 13, 21, '1', 'Talla:', NULL, 336),
(38, '2015-03-13 17:57:24', NULL, 13, 22, '1', 'Talla:', NULL, 337),
(39, '2015-03-15 13:31:21', NULL, 13, 23, '1', 'Tallas:', NULL, 340),
(40, '2015-03-15 13:35:09', NULL, 13, 24, '1', 'Talla:', NULL, 341),
(41, '2015-03-15 19:21:25', NULL, 13, 25, '1', 'Talla:', NULL, 342),
(43, '2015-03-15 19:27:31', NULL, 13, 27, '1', 'Talla:', NULL, 343),
(44, '2015-03-15 19:33:14', NULL, 13, 28, '1', 'Talla:', NULL, 344),
(45, '2015-03-15 19:38:25', NULL, 13, 29, '1', 'Talla:', NULL, 345),
(46, '2015-03-15 19:47:18', NULL, 13, 30, '1', 'Talla:', NULL, 346),
(47, '2015-03-15 19:50:00', NULL, 13, 31, '1', 'Talla:', NULL, 347),
(48, '2015-03-15 19:54:14', NULL, 13, 32, '1', 'Talla:', NULL, 348),
(49, '2015-03-15 19:57:59', NULL, 13, 33, '1', 'Talla:', NULL, 349),
(50, '2015-03-15 20:04:38', NULL, 13, 34, '1', 'Talla:', NULL, 350),
(51, '2015-03-15 21:54:03', NULL, 13, 35, '1', 'Talla:', NULL, 351),
(52, '2015-03-15 21:57:03', NULL, 13, 36, '1', 'talla:', NULL, 352),
(53, '2015-03-15 22:01:17', NULL, 13, 37, '1', 'Talla:', NULL, 354),
(54, '2015-04-04 19:28:49', NULL, 13, 38, '1', 'TALLA', NULL, 339),
(55, '2015-04-14 15:07:44', NULL, 13, 39, '1', 'TALLA', NULL, 569),
(56, '2015-04-14 15:11:53', NULL, 13, 40, '1', 'TALLA', NULL, 568),
(57, '2015-04-14 15:29:22', NULL, 13, 41, '1', 'TALLA', NULL, 566),
(59, '2015-04-14 15:49:41', NULL, 13, 42, '1', 'TALLA', NULL, 523),
(60, '2015-04-14 15:51:18', NULL, 13, 43, '1', 'TALLA', NULL, 521),
(61, '2015-04-14 15:53:06', NULL, 13, 44, '1', 'TALLA', NULL, 539),
(62, '2015-04-14 15:56:53', NULL, 13, 45, '1', 'TALLA', NULL, 545),
(63, '2015-04-14 15:59:50', NULL, 13, 46, '1', 'TALLA', NULL, 544),
(64, '2015-04-14 16:01:27', NULL, 13, 47, '1', 'TALLA', NULL, 543),
(65, '2015-04-14 16:03:35', NULL, 13, 48, '1', 'TALLA', NULL, 540),
(66, '2015-04-14 16:06:43', NULL, 13, 49, '1', 'TALLA', NULL, 572),
(67, '2015-04-14 16:12:20', NULL, 13, 50, '1', 'TALLA', NULL, 571),
(68, '2015-04-14 16:14:56', NULL, 13, 51, '1', 'TALLA', NULL, 557),
(69, '2015-04-14 16:16:40', NULL, 13, 52, '1', 'TALLA', NULL, 561),
(70, '2015-04-14 16:18:12', NULL, 13, 53, '1', 'TALLA', NULL, 560),
(71, '2015-04-14 16:20:34', NULL, 13, 54, '1', 'TALLA', NULL, 548),
(73, '2015-04-14 16:22:07', NULL, 13, 55, '1', 'TALLA', NULL, 547),
(74, '2015-04-14 16:23:20', NULL, 13, 56, '1', 'TALLA', NULL, 546),
(75, '2015-04-14 16:34:12', NULL, 13, 57, '1', 'TALLA', NULL, 518),
(77, '2015-04-14 16:35:48', '2015-04-14 16:36:05', 13, 59, '1', 'TALLA', NULL, 519),
(78, '2015-04-14 16:39:09', NULL, 13, 60, '1', 'TALLA', NULL, 533),
(79, '2015-04-14 16:42:28', NULL, 13, 61, '1', 'TALLA', NULL, 520),
(80, '2015-04-14 16:44:36', NULL, 13, 62, '1', 'TALLA', NULL, 563),
(81, '2015-04-14 16:46:54', NULL, 13, 63, '1', 'TALLA', NULL, 562),
(82, '2015-04-14 16:53:45', NULL, 13, 64, '1', 'TALLA', NULL, 567),
(83, '2015-04-14 16:55:44', NULL, 13, 65, '1', 'TALLA', NULL, 553),
(84, '2015-04-14 16:58:19', NULL, 13, 66, '1', 'TALLA', NULL, 564),
(85, '2015-04-14 17:00:35', NULL, 13, 67, '1', 'TALLA', NULL, 558),
(86, '2015-04-14 17:02:15', NULL, 13, 68, '1', 'TALLA', NULL, 554),
(87, '2015-04-14 17:05:44', NULL, 13, 69, '1', 'TALLA', NULL, 559),
(88, '2015-04-14 17:08:20', NULL, 13, 70, '1', 'TALLA', NULL, 570),
(89, '2015-04-14 17:10:17', NULL, 13, 71, '1', 'TALLA', NULL, 573),
(90, '2015-04-14 17:14:35', NULL, 13, 72, '1', 'TALLA', NULL, 549),
(91, '2015-04-14 18:56:09', NULL, 13, 73, '1', 'TALLA', NULL, 552),
(92, '2015-04-14 19:18:15', NULL, 13, 74, '1', 'TALLA', NULL, 556),
(93, '2015-04-14 20:07:40', NULL, 13, 75, '1', 'TALLA', NULL, 582),
(94, '2015-04-14 20:08:50', NULL, 13, 76, '1', 'TALLA', NULL, 716),
(95, '2015-04-14 20:09:44', NULL, 13, 77, '1', 'TALLA', NULL, 717),
(96, '2015-04-14 20:13:17', NULL, 13, 78, '1', 'TALLA', NULL, 534),
(97, '2015-04-15 09:18:12', NULL, 13, 79, '1', 'TALLA', NULL, 734),
(98, '2015-04-15 09:56:33', NULL, 13, 80, '1', 'TALLA', NULL, 555);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_product_variations`
--

CREATE TABLE IF NOT EXISTS `default_firesale_product_variations` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` float DEFAULT NULL,
  `parent` int(6) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=353 ;

--
-- Volcado de datos para la tabla `default_firesale_product_variations`
--

INSERT INTO `default_firesale_product_variations` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `title`, `price`, `parent`, `product`) VALUES
(42, '2015-03-13 16:05:39', NULL, 13, 1, 'S', 0, 17, NULL),
(43, '2015-03-13 16:05:48', NULL, 13, 2, 'M', 0, 17, NULL),
(44, '2015-03-13 16:05:58', NULL, 13, 3, 'L', 0, 17, NULL),
(46, '2015-03-13 16:17:13', NULL, 13, 4, 'S', 0, 18, NULL),
(47, '2015-03-13 16:17:47', NULL, 13, 5, 'M', 0, 18, NULL),
(48, '2015-03-13 16:17:58', NULL, 13, 6, 'L', 0, 18, NULL),
(49, '2015-03-13 16:18:09', NULL, 13, 7, 'XL', 0, 18, NULL),
(50, '2015-03-13 16:20:30', '2015-03-13 16:24:35', 13, 8, 'M', 0, 19, NULL),
(52, '2015-03-13 16:26:21', NULL, 13, 9, 'S', 0, 20, NULL),
(54, '2015-03-13 16:26:41', NULL, 13, 11, 'M', 0, 20, NULL),
(55, '2015-03-13 16:29:14', NULL, 13, 12, 'L', 0, 20, NULL),
(56, '2015-03-13 16:31:38', NULL, 13, 13, 'M', 0, 21, NULL),
(57, '2015-03-13 16:31:47', NULL, 13, 14, 'L', 0, 21, NULL),
(58, '2015-03-13 16:32:05', NULL, 13, 15, 'XL', 0, 21, NULL),
(59, '2015-03-13 16:38:47', NULL, 13, 16, 'S', 0, 22, NULL),
(60, '2015-03-13 16:38:58', NULL, 13, 17, 'M', 0, 22, NULL),
(61, '2015-03-13 16:39:08', NULL, 13, 18, 'L', 0, 22, NULL),
(62, '2015-03-13 16:39:21', NULL, 13, 19, 'XL', 0, 22, NULL),
(63, '2015-03-13 16:46:22', NULL, 13, 20, 'S', 0, 23, NULL),
(64, '2015-03-13 16:49:24', NULL, 13, 21, 'M', 0, 23, NULL),
(67, '2015-03-13 16:56:03', NULL, 13, 24, 'S', 0, 24, NULL),
(69, '2015-03-13 16:56:16', NULL, 13, 26, 'M', 0, 24, NULL),
(70, '2015-03-13 16:57:16', NULL, 13, 27, 'L', 0, 24, NULL),
(71, '2015-03-13 16:57:41', NULL, 13, 28, 'XL', 0, 24, NULL),
(72, '2015-03-13 17:05:25', NULL, 13, 29, 'L', 0, 23, NULL),
(73, '2015-03-13 17:06:44', NULL, 13, 30, 'XL', 0, 23, NULL),
(74, '2015-03-13 17:11:24', NULL, 13, 31, 'S', 0, 25, NULL),
(76, '2015-03-13 17:11:52', NULL, 13, 32, 'M', 0, 25, NULL),
(77, '2015-03-13 17:12:02', NULL, 13, 33, 'L', 0, 25, NULL),
(78, '2015-03-13 17:14:58', NULL, 13, 34, 'S', 0, 26, NULL),
(79, '2015-03-13 17:15:10', NULL, 13, 35, 'M', 0, 26, NULL),
(80, '2015-03-13 17:15:19', NULL, 13, 36, 'L', 0, 26, NULL),
(81, '2015-03-13 17:18:11', NULL, 13, 37, 'S', 0, 27, NULL),
(82, '2015-03-13 17:18:36', NULL, 13, 38, 'M', 0, 27, NULL),
(83, '2015-03-13 17:18:51', NULL, 13, 39, 'L', 0, 27, NULL),
(84, '2015-03-13 17:22:29', NULL, 13, 40, 'S', 0, 28, NULL),
(86, '2015-03-13 17:22:49', NULL, 13, 41, 'M', 0, 28, NULL),
(87, '2015-03-13 17:22:59', NULL, 13, 42, 'L', 0, 28, NULL),
(88, '2015-03-13 17:23:12', NULL, 13, 43, 'XL', 0, 28, NULL),
(89, '2015-03-13 17:27:14', NULL, 13, 44, 'S', 0, 29, NULL),
(90, '2015-03-13 17:27:23', NULL, 13, 45, 'M', 0, 29, NULL),
(92, '2015-03-13 17:27:47', NULL, 13, 46, 'L', 0, 29, NULL),
(93, '2015-03-13 17:28:00', NULL, 13, 47, 'XL', 0, 29, NULL),
(94, '2015-03-13 17:31:53', NULL, 13, 48, 'S', 0, 30, NULL),
(95, '2015-03-13 17:32:04', NULL, 13, 49, 'M', 0, 30, NULL),
(96, '2015-03-13 17:32:15', NULL, 13, 50, 'L', 0, 30, NULL),
(97, '2015-03-13 17:32:28', NULL, 13, 51, 'XL', 0, 30, NULL),
(98, '2015-03-13 17:35:14', NULL, 13, 52, 'S', 0, 31, NULL),
(99, '2015-03-13 17:35:24', NULL, 13, 53, 'M', 0, 31, NULL),
(100, '2015-03-13 17:35:32', NULL, 13, 54, 'L', 0, 31, NULL),
(101, '2015-03-13 17:35:41', NULL, 13, 55, 'XL', 0, 31, NULL),
(102, '2015-03-13 17:39:09', NULL, 13, 56, 'S', 0, 32, NULL),
(103, '2015-03-13 17:39:17', NULL, 13, 57, 'M', 0, 32, NULL),
(104, '2015-03-13 17:39:26', NULL, 13, 58, 'L', 0, 32, NULL),
(105, '2015-03-13 17:39:37', NULL, 13, 59, 'XL', 0, 32, NULL),
(106, '2015-03-13 17:41:43', NULL, 13, 60, 'S', 0, 33, NULL),
(107, '2015-03-13 17:41:54', NULL, 13, 61, 'M', 0, 33, NULL),
(108, '2015-03-13 17:42:03', NULL, 13, 62, 'L', 0, 33, NULL),
(109, '2015-03-13 17:42:15', NULL, 13, 63, 'XL', 0, 33, NULL),
(110, '2015-03-13 17:44:37', NULL, 13, 64, 'XS', 0, 34, NULL),
(112, '2015-03-13 17:44:55', NULL, 13, 65, 'S', 0, 34, NULL),
(113, '2015-03-13 17:45:03', NULL, 13, 66, 'M', 0, 34, NULL),
(114, '2015-03-13 17:45:12', NULL, 13, 67, 'L', 0, 34, NULL),
(115, '2015-03-13 17:45:24', NULL, 13, 68, 'XL', 0, 34, NULL),
(116, '2015-03-13 17:48:27', NULL, 13, 69, '2T', 0, 35, NULL),
(117, '2015-03-13 17:48:42', NULL, 13, 70, '2XS', 0, 35, NULL),
(118, '2015-03-13 17:48:57', NULL, 13, 71, '3T', 0, 35, NULL),
(119, '2015-03-13 17:49:07', NULL, 13, 72, '4T', 0, 35, NULL),
(120, '2015-03-13 17:49:27', NULL, 13, 73, '5T', 0, 35, NULL),
(121, '2015-03-13 17:52:18', NULL, 13, 74, 'S', 0, 36, NULL),
(122, '2015-03-13 17:52:31', NULL, 13, 75, 'M', 0, 36, NULL),
(123, '2015-03-13 17:52:44', NULL, 13, 76, 'L', 0, 36, NULL),
(124, '2015-03-13 17:52:59', NULL, 13, 77, 'XL', 0, 36, NULL),
(125, '2015-03-13 17:55:10', NULL, 13, 78, 'S', 0, 37, NULL),
(126, '2015-03-13 17:55:19', NULL, 13, 79, 'M', 0, 37, NULL),
(127, '2015-03-13 17:55:29', NULL, 13, 80, 'L', 0, 37, NULL),
(128, '2015-03-13 17:55:39', NULL, 13, 81, 'XL', 0, 37, NULL),
(129, '2015-03-13 17:57:32', NULL, 13, 82, 'S', 0, 38, NULL),
(130, '2015-03-13 17:57:40', NULL, 13, 83, 'M', 0, 38, NULL),
(131, '2015-03-13 17:57:47', NULL, 13, 84, 'L', 0, 38, NULL),
(132, '2015-03-13 17:57:57', NULL, 13, 85, 'XL', 0, 38, NULL),
(133, '2015-03-15 13:31:36', NULL, 13, 86, 'S', 0, 39, NULL),
(134, '2015-03-15 13:31:49', NULL, 13, 87, 'M', 0, 39, NULL),
(135, '2015-03-15 13:32:00', NULL, 13, 88, 'L', 0, 39, NULL),
(136, '2015-03-15 13:32:16', NULL, 13, 89, 'XL', 0, 39, NULL),
(137, '2015-03-15 13:35:31', NULL, 13, 90, 'S', 0, 40, NULL),
(138, '2015-03-15 13:42:08', NULL, 13, 91, 'M', 0, 40, NULL),
(139, '2015-03-15 13:42:20', NULL, 13, 92, 'L', 0, 40, NULL),
(140, '2015-03-15 13:42:41', '2015-04-04 20:03:32', 13, 93, 'XL', 0, 40, NULL),
(141, '2015-03-15 19:21:43', NULL, 13, 94, 'S', 0, 41, NULL),
(142, '2015-03-15 19:21:55', NULL, 13, 95, 'M', 0, 41, NULL),
(144, '2015-03-15 19:22:27', NULL, 13, 96, 'L', 0, 41, NULL),
(145, '2015-03-15 19:22:41', NULL, 13, 97, 'XL', 0, 41, NULL),
(146, '2015-03-15 19:28:27', NULL, 13, 98, 'S', 0, 43, NULL),
(147, '2015-03-15 19:28:53', NULL, 13, 99, 'M', 0, 43, NULL),
(148, '2015-03-15 19:29:11', NULL, 13, 100, 'L', 0, 43, NULL),
(149, '2015-03-15 19:29:35', NULL, 13, 101, 'XL', 0, 43, NULL),
(150, '2015-03-15 19:33:31', NULL, 13, 102, 'S', 0, 44, NULL),
(151, '2015-03-15 19:33:45', NULL, 13, 103, 'M', 0, 44, NULL),
(152, '2015-03-15 19:33:57', NULL, 13, 104, 'L', 0, 44, NULL),
(153, '2015-03-15 19:34:17', NULL, 13, 105, 'XL', 0, 44, NULL),
(154, '2015-03-15 19:38:46', NULL, 13, 106, 'S', 0, 45, NULL),
(155, '2015-03-15 19:38:58', NULL, 13, 107, 'M', 0, 45, NULL),
(156, '2015-03-15 19:39:09', NULL, 13, 108, 'L', 0, 45, NULL),
(157, '2015-03-15 19:39:22', NULL, 13, 109, 'XL', 0, 45, NULL),
(158, '2015-03-15 19:47:31', NULL, 13, 110, 'S', 0, 46, NULL),
(159, '2015-03-15 19:47:45', NULL, 13, 111, 'M', 0, 46, NULL),
(160, '2015-03-15 19:47:59', NULL, 13, 112, 'L', 0, 46, NULL),
(161, '2015-03-15 19:48:13', NULL, 13, 113, 'XL', 0, 46, NULL),
(162, '2015-03-15 19:50:17', NULL, 13, 114, 'S', 0, 47, NULL),
(163, '2015-03-15 19:50:32', NULL, 13, 115, 'M', 0, 47, NULL),
(164, '2015-03-15 19:50:57', NULL, 13, 116, 'L', 0, 47, NULL),
(165, '2015-03-15 19:51:18', NULL, 13, 117, 'XL', 0, 47, NULL),
(166, '2015-03-15 19:54:27', NULL, 13, 118, 'S', 0, 48, NULL),
(167, '2015-03-15 19:54:39', NULL, 13, 119, 'M', 0, 48, NULL),
(168, '2015-03-15 19:54:49', NULL, 13, 120, 'L', 0, 48, NULL),
(169, '2015-03-15 19:55:02', NULL, 13, 121, 'XL', 0, 48, NULL),
(170, '2015-03-15 19:58:50', NULL, 13, 122, 'S', 0, 49, NULL),
(171, '2015-03-15 19:59:31', NULL, 13, 123, 'M', 0, 49, NULL),
(172, '2015-03-15 19:59:48', NULL, 13, 124, 'L', 0, 49, NULL),
(173, '2015-03-15 20:00:16', NULL, 13, 125, 'XL', 0, 49, NULL),
(174, '2015-03-15 21:48:03', NULL, 13, 126, 'S', 0, 50, NULL),
(175, '2015-03-15 21:50:03', NULL, 13, 127, 'M', 0, 50, NULL),
(176, '2015-03-15 21:50:15', NULL, 13, 128, 'L', 0, 50, NULL),
(177, '2015-03-15 21:50:27', NULL, 13, 129, 'XL', 0, 50, NULL),
(178, '2015-03-15 21:52:36', NULL, 13, 130, 'XS', 0, 50, NULL),
(179, '2015-03-15 21:54:19', NULL, 13, 131, 'S', 0, 51, NULL),
(180, '2015-03-15 21:54:28', NULL, 13, 132, 'M', 0, 51, NULL),
(181, '2015-03-15 21:54:46', NULL, 13, 133, 'L', 0, 51, NULL),
(182, '2015-03-15 21:55:00', NULL, 13, 134, 'XL', 0, 51, NULL),
(183, '2015-03-15 21:55:37', NULL, 13, 135, 'XS', 0, 51, NULL),
(184, '2015-03-15 21:57:19', NULL, 13, 136, 'XS', 0, 52, NULL),
(185, '2015-03-15 21:57:28', NULL, 13, 137, 'S', 0, 52, NULL),
(186, '2015-03-15 21:57:36', NULL, 13, 138, 'M', 0, 52, NULL),
(187, '2015-03-15 21:57:45', NULL, 13, 139, 'L', 0, 52, NULL),
(188, '2015-03-15 21:57:56', NULL, 13, 140, 'XL', 0, 52, NULL),
(189, '2015-03-15 22:01:24', NULL, 13, 141, 'S', 0, 53, NULL),
(190, '2015-03-15 22:01:34', NULL, 13, 142, 'M', 0, 53, NULL),
(191, '2015-03-15 22:01:43', NULL, 13, 143, 'L', 0, 53, NULL),
(192, '2015-03-15 22:01:55', NULL, 13, 144, 'XL', 0, 53, NULL),
(193, '2015-04-04 19:29:05', NULL, 13, 145, 'S', 0, 54, NULL),
(195, '2015-04-04 19:29:33', NULL, 13, 146, 'M', 0, 54, NULL),
(196, '2015-04-04 19:29:44', NULL, 13, 147, 'L', 0, 54, NULL),
(197, '2015-04-14 15:07:56', NULL, 13, 148, 'XS', 0, 55, NULL),
(198, '2015-04-14 15:08:15', NULL, 13, 149, 'S', 0, 55, NULL),
(199, '2015-04-14 15:08:26', NULL, 13, 150, 'M', 0, 55, NULL),
(200, '2015-04-14 15:08:35', NULL, 13, 151, 'L', 0, 55, NULL),
(201, '2015-04-14 15:12:09', NULL, 13, 152, 'XS', 0, 56, NULL),
(202, '2015-04-14 15:12:30', NULL, 13, 153, 'S', 0, 56, NULL),
(203, '2015-04-14 15:12:49', NULL, 13, 154, 'M', 0, 56, NULL),
(204, '2015-04-14 15:13:13', NULL, 13, 155, 'L', 0, 56, NULL),
(205, '2015-04-14 15:31:15', NULL, 13, 156, 'S', 0, 57, NULL),
(206, '2015-04-14 15:31:33', NULL, 13, 157, 'M', 0, 57, NULL),
(207, '2015-04-14 15:31:46', NULL, 13, 158, 'L', 0, 57, NULL),
(208, '2015-04-14 15:32:04', NULL, 13, 159, 'XL', 0, 57, NULL),
(209, '2015-04-14 15:50:07', NULL, 13, 160, 'S', 0, 59, NULL),
(210, '2015-04-14 15:50:16', NULL, 13, 161, 'M', 0, 59, NULL),
(211, '2015-04-14 15:50:27', NULL, 13, 162, 'L', 0, 59, NULL),
(212, '2015-04-14 15:50:41', NULL, 13, 163, 'XL', 0, 59, NULL),
(213, '2015-04-14 15:51:24', NULL, 13, 164, 'S', 0, 60, NULL),
(214, '2015-04-14 15:51:32', NULL, 13, 165, 'M', 0, 60, NULL),
(215, '2015-04-14 15:51:40', NULL, 13, 166, 'L', 0, 60, NULL),
(216, '2015-04-14 15:51:50', NULL, 13, 167, 'XL', 0, 60, NULL),
(217, '2015-04-14 15:53:14', NULL, 13, 168, 'XS', 0, 61, NULL),
(218, '2015-04-14 15:53:26', NULL, 13, 169, 'S', 0, 61, NULL),
(219, '2015-04-14 15:53:35', NULL, 13, 170, 'M', 0, 61, NULL),
(220, '2015-04-14 15:54:00', NULL, 13, 171, 'L', 0, 61, NULL),
(221, '2015-04-14 15:57:04', '2015-04-21 22:01:29', 13, 172, 'S', 0, 62, NULL),
(222, '2015-04-14 15:57:14', NULL, 13, 173, 'M', 0, 62, NULL),
(223, '2015-04-14 15:57:24', NULL, 13, 174, 'L', 0, 62, NULL),
(224, '2015-04-14 15:57:33', NULL, 13, 175, 'XL', 0, 62, NULL),
(225, '2015-04-14 16:00:00', NULL, 13, 176, 'S', 0, 63, NULL),
(226, '2015-04-14 16:00:18', NULL, 13, 177, 'M', 0, 63, NULL),
(227, '2015-04-14 16:00:28', NULL, 13, 178, 'L', 0, 63, NULL),
(228, '2015-04-14 16:00:41', NULL, 13, 179, 'XL', 0, 63, NULL),
(229, '2015-04-14 16:01:47', NULL, 13, 180, 'S', 0, 64, NULL),
(230, '2015-04-14 16:01:57', NULL, 13, 181, 'M', 0, 64, NULL),
(231, '2015-04-14 16:02:06', NULL, 13, 182, 'L', 0, 64, NULL),
(232, '2015-04-14 16:02:43', NULL, 13, 183, 'XL', 0, 64, NULL),
(233, '2015-04-14 16:03:44', NULL, 13, 184, 'XS', 0, 65, NULL),
(234, '2015-04-14 16:03:53', NULL, 13, 185, 'S', 0, 65, NULL),
(235, '2015-04-14 16:04:11', NULL, 13, 186, 'M', 0, 65, NULL),
(236, '2015-04-14 16:04:21', NULL, 13, 187, 'L', 0, 65, NULL),
(237, '2015-04-14 16:07:32', NULL, 13, 188, 'S', 0, 66, NULL),
(238, '2015-04-14 16:07:41', NULL, 13, 189, 'M', 0, 66, NULL),
(239, '2015-04-14 16:09:01', NULL, 13, 190, 'XS', 0, 66, NULL),
(240, '2015-04-14 16:09:28', NULL, 13, 191, 'L', 0, 66, NULL),
(241, '2015-04-14 16:12:30', NULL, 13, 192, 'XS', 0, 67, NULL),
(242, '2015-04-14 16:13:21', NULL, 13, 193, 'S', 0, 67, NULL),
(243, '2015-04-14 16:13:29', NULL, 13, 194, 'M', 0, 67, NULL),
(244, '2015-04-14 16:13:38', NULL, 13, 195, 'L', 0, 67, NULL),
(245, '2015-04-14 16:15:04', NULL, 13, 196, 'S', 0, 68, NULL),
(246, '2015-04-14 16:15:29', NULL, 13, 197, 'M', 0, 68, NULL),
(247, '2015-04-14 16:15:37', NULL, 13, 198, 'L', 0, 68, NULL),
(248, '2015-04-14 16:15:47', NULL, 13, 199, 'XL', 0, 68, NULL),
(249, '2015-04-14 16:16:47', NULL, 13, 200, 'S', 0, 69, NULL),
(250, '2015-04-14 16:16:57', NULL, 13, 201, 'M', 0, 69, NULL),
(251, '2015-04-14 16:17:05', NULL, 13, 202, 'L', 0, 69, NULL),
(252, '2015-04-14 16:17:15', NULL, 13, 203, 'XL', 0, 69, NULL),
(253, '2015-04-14 16:18:19', NULL, 13, 204, 'S', 0, 70, NULL),
(254, '2015-04-14 16:18:27', NULL, 13, 205, 'M', 0, 70, NULL),
(255, '2015-04-14 16:18:36', NULL, 13, 206, 'L', 0, 70, NULL),
(256, '2015-04-14 16:18:46', NULL, 13, 207, 'XL', 0, 70, NULL),
(257, '2015-04-14 16:20:52', NULL, 13, 208, 'S', 0, 71, NULL),
(258, '2015-04-14 16:21:01', NULL, 13, 209, 'M', 0, 71, NULL),
(259, '2015-04-14 16:21:11', NULL, 13, 210, 'L', 0, 71, NULL),
(260, '2015-04-14 16:21:21', NULL, 13, 211, 'XL', 0, 71, NULL),
(261, '2015-04-14 16:22:17', NULL, 13, 212, 'S', 0, 73, NULL),
(262, '2015-04-14 16:22:25', NULL, 13, 213, 'M', 0, 73, NULL),
(263, '2015-04-14 16:22:34', NULL, 13, 214, 'L', 0, 73, NULL),
(264, '2015-04-14 16:22:43', NULL, 13, 215, 'XL', 0, 73, NULL),
(265, '2015-04-14 16:23:34', NULL, 13, 216, 'S', 0, 74, NULL),
(266, '2015-04-14 16:23:43', NULL, 13, 217, 'M', 0, 74, NULL),
(267, '2015-04-14 16:23:51', NULL, 13, 218, 'L', 0, 74, NULL),
(268, '2015-04-14 16:24:20', NULL, 13, 219, 'XL', 0, 74, NULL),
(269, '2015-04-14 16:34:44', NULL, 13, 220, 'S', 0, 75, NULL),
(270, '2015-04-14 16:36:17', NULL, 13, 221, 'M', 0, 77, NULL),
(271, '2015-04-14 16:39:18', NULL, 13, 222, 'S', 0, 78, NULL),
(272, '2015-04-14 16:42:39', NULL, 13, 223, 'S', 0, 79, NULL),
(273, '2015-04-14 16:42:46', NULL, 13, 224, 'M', 0, 79, NULL),
(274, '2015-04-14 16:42:54', NULL, 13, 225, 'L', 0, 79, NULL),
(275, '2015-04-14 16:43:04', NULL, 13, 226, 'XL', 0, 79, NULL),
(276, '2015-04-14 16:44:43', NULL, 13, 227, 'S', 0, 80, NULL),
(277, '2015-04-14 16:44:52', NULL, 13, 228, 'M', 0, 80, NULL),
(278, '2015-04-14 16:45:39', NULL, 13, 229, 'L', 0, 80, NULL),
(279, '2015-04-14 16:45:48', NULL, 13, 230, 'XL', 0, 80, NULL),
(280, '2015-04-14 16:47:01', NULL, 13, 231, 'S', 0, 81, NULL),
(281, '2015-04-14 16:47:09', NULL, 13, 232, 'M', 0, 81, NULL),
(282, '2015-04-14 16:47:22', NULL, 13, 233, 'L', 0, 81, NULL),
(283, '2015-04-14 16:47:32', NULL, 13, 234, 'XL', 0, 81, NULL),
(284, '2015-04-14 16:53:57', NULL, 13, 235, 'S', 0, 82, NULL),
(285, '2015-04-14 16:54:05', NULL, 13, 236, 'M', 0, 82, NULL),
(286, '2015-04-14 16:54:28', NULL, 13, 237, 'L', 0, 82, NULL),
(287, '2015-04-14 16:54:37', NULL, 13, 238, 'XL', 0, 82, NULL),
(288, '2015-04-14 16:56:01', NULL, 13, 239, '18/24', 0, 83, NULL),
(289, '2015-04-14 16:56:12', NULL, 13, 240, '2/3', 0, 83, NULL),
(290, '2015-04-14 16:56:26', NULL, 13, 241, '3/4', 0, 83, NULL),
(291, '2015-04-14 16:56:42', NULL, 13, 242, '4/5', 0, 83, NULL),
(292, '2015-04-14 16:57:00', NULL, 13, 243, '5/6', 0, 83, NULL),
(293, '2015-04-14 16:58:28', NULL, 13, 244, 'XS', 0, 84, NULL),
(294, '2015-04-14 16:58:51', NULL, 13, 245, 'S', 0, 84, NULL),
(295, '2015-04-14 16:59:01', NULL, 13, 246, 'M', 0, 84, NULL),
(296, '2015-04-14 16:59:09', NULL, 13, 247, 'L', 0, 84, NULL),
(297, '2015-04-14 17:00:44', NULL, 13, 248, 'S', 0, 85, NULL),
(298, '2015-04-14 17:00:57', NULL, 13, 249, 'M', 0, 85, NULL),
(299, '2015-04-14 17:01:09', NULL, 13, 250, 'L', 0, 85, NULL),
(300, '2015-04-14 17:02:22', NULL, 13, 251, 'S', 0, 86, NULL),
(301, '2015-04-14 17:02:30', NULL, 13, 252, 'M', 0, 86, NULL),
(302, '2015-04-14 17:02:38', NULL, 13, 253, 'L', 0, 86, NULL),
(303, '2015-04-14 17:02:47', NULL, 13, 254, 'XL', 0, 86, NULL),
(304, '2015-04-14 17:05:51', NULL, 13, 255, 'S', 0, 87, NULL),
(305, '2015-04-14 17:05:59', NULL, 13, 256, 'M', 0, 87, NULL),
(306, '2015-04-14 17:06:07', NULL, 13, 257, 'L', 0, 87, NULL),
(307, '2015-04-14 17:06:33', NULL, 13, 258, 'XL', 0, 87, NULL),
(308, '2015-04-14 17:08:41', NULL, 13, 259, 'XS', 0, 88, NULL),
(309, '2015-04-14 17:08:49', NULL, 13, 260, 'S', 0, 88, NULL),
(310, '2015-04-14 17:08:58', NULL, 13, 261, 'M', 0, 88, NULL),
(311, '2015-04-14 17:09:09', NULL, 13, 262, 'L', 0, 88, NULL),
(312, '2015-04-14 17:10:25', NULL, 13, 263, 'S', 0, 89, NULL),
(313, '2015-04-14 17:10:38', NULL, 13, 264, 'M', 0, 89, NULL),
(314, '2015-04-14 17:10:48', NULL, 13, 265, 'L', 0, 89, NULL),
(315, '2015-04-14 17:10:58', NULL, 13, 266, 'XL', 0, 89, NULL),
(316, '2015-04-14 17:14:42', NULL, 13, 267, 'S', 0, 90, NULL),
(317, '2015-04-14 17:14:51', NULL, 13, 268, 'M', 0, 90, NULL),
(318, '2015-04-14 17:15:01', NULL, 13, 269, 'L', 0, 90, NULL),
(319, '2015-04-14 17:15:12', NULL, 13, 270, 'XL', 0, 90, NULL),
(320, '2015-04-14 17:19:14', NULL, 13, 271, 'M', 0, 75, NULL),
(321, '2015-04-14 17:19:58', NULL, 13, 272, 'L', 0, 75, NULL),
(322, '2015-04-14 17:20:47', NULL, 13, 273, 'XL', 0, 75, NULL),
(323, '2015-04-14 17:21:45', NULL, 13, 274, 'S', 0, 77, NULL),
(324, '2015-04-14 17:21:55', NULL, 13, 275, 'L', 0, 77, NULL),
(325, '2015-04-14 17:22:05', NULL, 13, 276, 'XL', 0, 77, NULL),
(326, '2015-04-14 17:25:24', NULL, 13, 277, 'M', 0, 78, NULL),
(327, '2015-04-14 17:25:33', NULL, 13, 278, 'L', 0, 78, NULL),
(328, '2015-04-14 17:25:42', NULL, 13, 279, 'XL', 0, 78, NULL),
(329, '2015-04-14 18:56:17', NULL, 13, 280, 'XS', 0, 91, NULL),
(330, '2015-04-14 18:56:24', NULL, 13, 281, 'S', 0, 91, NULL),
(331, '2015-04-14 18:56:33', NULL, 13, 282, 'M', 0, 91, NULL),
(332, '2015-04-14 18:56:41', NULL, 13, 283, 'L', 0, 91, NULL),
(333, '2015-04-14 18:56:51', NULL, 13, 284, 'XL', 0, 91, NULL),
(334, '2015-04-14 19:18:24', NULL, 13, 285, 'S', 0, 92, NULL),
(335, '2015-04-14 19:19:13', NULL, 13, 286, 'M', 0, 92, NULL),
(336, '2015-04-14 19:19:22', NULL, 13, 287, 'L', 0, 92, NULL),
(337, '2015-04-14 19:19:33', NULL, 13, 288, 'XL', 0, 92, NULL),
(338, '2015-04-14 20:07:50', NULL, 13, 289, 'TU', 0, 93, NULL),
(339, '2015-04-14 20:08:58', NULL, 13, 290, 'TU', 0, 94, NULL),
(340, '2015-04-14 20:09:52', NULL, 13, 291, 'TU', 0, 95, NULL),
(341, '2015-04-14 20:13:29', NULL, 13, 292, 'S', 0, 96, NULL),
(342, '2015-04-14 20:13:54', NULL, 13, 293, 'M', 0, 96, NULL),
(343, '2015-04-14 20:14:02', NULL, 13, 294, 'L', 0, 96, NULL),
(344, '2015-04-14 20:14:11', NULL, 13, 295, 'XL', 0, 96, NULL),
(345, '2015-04-15 09:18:40', NULL, 13, 296, '2/4', 0, 97, NULL),
(346, '2015-04-15 09:18:52', NULL, 13, 297, '6/8', 0, 97, NULL),
(347, '2015-04-15 09:19:01', NULL, 13, 298, '10/12', 0, 97, NULL),
(348, '2015-04-15 09:19:11', NULL, 13, 299, '10/16', 0, 97, NULL),
(349, '2015-04-15 09:56:46', NULL, 13, 300, 'S', 0, 98, NULL),
(350, '2015-04-15 09:56:54', NULL, 13, 301, 'M', 0, 98, NULL),
(351, '2015-04-15 09:57:01', NULL, 13, 302, 'L', 0, 98, NULL),
(352, '2015-04-15 09:57:10', NULL, 13, 303, 'XL', 0, 98, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_product_variations_firesale_products`
--

CREATE TABLE IF NOT EXISTS `default_firesale_product_variations_firesale_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `firesale_product_variations_id` int(11) NOT NULL,
  `firesale_products_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=424 ;

--
-- Volcado de datos para la tabla `default_firesale_product_variations_firesale_products`
--

INSERT INTO `default_firesale_product_variations_firesale_products` (`id`, `row_id`, `firesale_product_variations_id`, `firesale_products_id`) VALUES
(113, 42, 5, 362),
(114, 43, 5, 363),
(115, 44, 5, 364),
(117, 46, 5, 366),
(118, 47, 5, 367),
(119, 48, 5, 368),
(120, 49, 5, 369),
(121, 50, 5, 370),
(123, 52, 5, 372),
(125, 54, 5, 374),
(126, 55, 5, 375),
(127, 56, 5, 376),
(128, 57, 5, 377),
(129, 58, 5, 378),
(130, 59, 5, 379),
(131, 60, 5, 380),
(132, 61, 5, 381),
(133, 62, 5, 382),
(134, 63, 5, 383),
(135, 64, 5, 384),
(138, 67, 5, 387),
(140, 69, 5, 389),
(141, 70, 5, 390),
(142, 71, 5, 391),
(143, 72, 5, 392),
(144, 73, 5, 393),
(145, 74, 5, 394),
(147, 76, 5, 396),
(148, 77, 5, 397),
(149, 78, 5, 398),
(150, 79, 5, 399),
(151, 80, 5, 400),
(152, 81, 5, 401),
(153, 82, 5, 402),
(154, 83, 5, 403),
(155, 84, 5, 404),
(157, 86, 5, 406),
(158, 87, 5, 407),
(159, 88, 5, 408),
(160, 89, 5, 409),
(161, 90, 5, 410),
(163, 92, 5, 412),
(164, 93, 5, 413),
(165, 94, 5, 414),
(166, 95, 5, 415),
(167, 96, 5, 416),
(168, 97, 5, 417),
(169, 98, 5, 418),
(170, 99, 5, 419),
(171, 100, 5, 420),
(172, 101, 5, 421),
(173, 102, 5, 422),
(174, 103, 5, 423),
(175, 104, 5, 424),
(176, 105, 5, 425),
(177, 106, 5, 426),
(178, 107, 5, 427),
(179, 108, 5, 428),
(180, 109, 5, 429),
(181, 110, 5, 430),
(183, 112, 5, 432),
(184, 113, 5, 433),
(185, 114, 5, 434),
(186, 115, 5, 435),
(187, 116, 5, 436),
(188, 117, 5, 437),
(189, 118, 5, 438),
(190, 119, 5, 439),
(191, 120, 5, 440),
(192, 121, 5, 441),
(193, 122, 5, 442),
(194, 123, 5, 443),
(195, 124, 5, 444),
(196, 125, 5, 445),
(197, 126, 5, 446),
(198, 127, 5, 447),
(199, 128, 5, 448),
(200, 129, 5, 449),
(201, 130, 5, 450),
(202, 131, 5, 451),
(203, 132, 5, 452),
(204, 133, 5, 453),
(205, 134, 5, 454),
(206, 135, 5, 455),
(207, 136, 5, 456),
(208, 137, 5, 457),
(209, 138, 5, 458),
(210, 139, 5, 459),
(211, 140, 5, 460),
(212, 141, 5, 461),
(213, 142, 5, 462),
(215, 144, 5, 464),
(216, 145, 5, 465),
(217, 146, 5, 466),
(218, 147, 5, 467),
(219, 148, 5, 468),
(220, 149, 5, 469),
(221, 150, 5, 470),
(222, 151, 5, 471),
(223, 152, 5, 472),
(224, 153, 5, 473),
(225, 154, 5, 474),
(226, 155, 5, 475),
(227, 156, 5, 476),
(228, 157, 5, 477),
(229, 158, 5, 478),
(230, 159, 5, 479),
(231, 160, 5, 480),
(232, 161, 5, 481),
(233, 162, 5, 482),
(234, 163, 5, 483),
(235, 164, 5, 484),
(236, 165, 5, 485),
(237, 166, 5, 486),
(238, 167, 5, 487),
(239, 168, 5, 488),
(240, 169, 5, 489),
(241, 170, 5, 490),
(242, 171, 5, 491),
(243, 172, 5, 492),
(244, 173, 5, 493),
(245, 174, 5, 494),
(246, 175, 5, 495),
(247, 176, 5, 496),
(248, 177, 5, 497),
(249, 178, 5, 498),
(250, 179, 5, 499),
(251, 180, 5, 500),
(252, 181, 5, 501),
(253, 182, 5, 502),
(254, 183, 5, 503),
(255, 184, 5, 504),
(256, 185, 5, 505),
(257, 186, 5, 506),
(258, 187, 5, 507),
(259, 188, 5, 508),
(260, 189, 5, 509),
(261, 190, 5, 510),
(262, 191, 5, 511),
(263, 192, 5, 512),
(264, 193, 5, 513),
(266, 195, 5, 515),
(267, 196, 5, 516),
(268, 197, 5, 583),
(269, 198, 5, 584),
(270, 199, 5, 585),
(271, 200, 5, 586),
(272, 201, 5, 587),
(273, 202, 5, 588),
(274, 203, 5, 589),
(275, 204, 5, 590),
(276, 205, 5, 591),
(277, 206, 5, 592),
(278, 207, 5, 593),
(279, 208, 5, 594),
(280, 209, 5, 595),
(281, 210, 5, 596),
(282, 211, 5, 597),
(283, 212, 5, 598),
(284, 213, 5, 599),
(285, 214, 5, 600),
(286, 215, 5, 601),
(287, 216, 5, 602),
(288, 217, 5, 603),
(289, 218, 5, 604),
(290, 219, 5, 605),
(291, 220, 5, 606),
(292, 221, 5, 607),
(293, 222, 5, 608),
(294, 223, 5, 609),
(295, 224, 5, 610),
(296, 225, 5, 611),
(297, 226, 5, 612),
(298, 227, 5, 613),
(299, 228, 5, 614),
(300, 229, 5, 615),
(301, 230, 5, 616),
(302, 231, 5, 617),
(303, 232, 5, 618),
(304, 233, 5, 619),
(305, 234, 5, 620),
(306, 235, 5, 621),
(307, 236, 5, 622),
(308, 237, 5, 623),
(309, 238, 5, 624),
(310, 239, 5, 625),
(311, 240, 5, 626),
(312, 241, 5, 627),
(313, 242, 5, 628),
(314, 243, 5, 629),
(315, 244, 5, 630),
(316, 245, 5, 631),
(317, 246, 5, 632),
(318, 247, 5, 633),
(319, 248, 5, 634),
(320, 249, 5, 635),
(321, 250, 5, 636),
(322, 251, 5, 637),
(323, 252, 5, 638),
(324, 253, 5, 639),
(325, 254, 5, 640),
(326, 255, 5, 641),
(327, 256, 5, 642),
(328, 257, 5, 643),
(329, 258, 5, 644),
(330, 259, 5, 645),
(331, 260, 5, 646),
(332, 261, 5, 647),
(333, 262, 5, 648),
(334, 263, 5, 649),
(335, 264, 5, 650),
(336, 265, 5, 651),
(337, 266, 5, 652),
(338, 267, 5, 653),
(339, 268, 5, 654),
(340, 269, 5, 655),
(341, 270, 5, 656),
(342, 271, 5, 657),
(343, 272, 5, 658),
(344, 273, 5, 659),
(345, 274, 5, 660),
(346, 275, 5, 661),
(347, 276, 5, 662),
(348, 277, 5, 663),
(349, 278, 5, 664),
(350, 279, 5, 665),
(351, 280, 5, 666),
(352, 281, 5, 667),
(353, 282, 5, 668),
(354, 283, 5, 669),
(355, 284, 5, 670),
(356, 285, 5, 671),
(357, 286, 5, 672),
(358, 287, 5, 673),
(359, 288, 5, 674),
(360, 289, 5, 675),
(361, 290, 5, 676),
(362, 291, 5, 677),
(363, 292, 5, 678),
(364, 293, 5, 679),
(365, 294, 5, 680),
(366, 295, 5, 681),
(367, 296, 5, 682),
(368, 297, 5, 683),
(369, 298, 5, 684),
(370, 299, 5, 685),
(371, 300, 5, 686),
(372, 301, 5, 687),
(373, 302, 5, 688),
(374, 303, 5, 689),
(375, 304, 5, 690),
(376, 305, 5, 691),
(377, 306, 5, 692),
(378, 307, 5, 693),
(379, 308, 5, 694),
(380, 309, 5, 695),
(381, 310, 5, 696),
(382, 311, 5, 697),
(383, 312, 5, 698),
(384, 313, 5, 699),
(385, 314, 5, 700),
(386, 315, 5, 701),
(387, 316, 5, 702),
(388, 317, 5, 703),
(389, 318, 5, 704),
(390, 319, 5, 705),
(391, 320, 5, 706),
(392, 321, 5, 707),
(393, 322, 5, 708),
(394, 323, 5, 709),
(395, 324, 5, 710),
(396, 325, 5, 711),
(397, 326, 5, 712),
(398, 327, 5, 713),
(399, 328, 5, 714),
(400, 329, 5, 718),
(401, 330, 5, 719),
(402, 331, 5, 720),
(403, 332, 5, 721),
(404, 333, 5, 722),
(405, 334, 5, 723),
(406, 335, 5, 724),
(407, 336, 5, 725),
(408, 337, 5, 726),
(409, 338, 5, 727),
(410, 339, 5, 728),
(411, 340, 5, 729),
(412, 341, 5, 730),
(413, 342, 5, 731),
(414, 343, 5, 732),
(415, 344, 5, 733),
(416, 345, 5, 735),
(417, 346, 5, 736),
(418, 347, 5, 737),
(419, 348, 5, 738),
(420, 349, 5, 739),
(421, 350, 5, 740),
(422, 351, 5, 741),
(423, 352, 5, 742);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_reviews`
--

CREATE TABLE IF NOT EXISTS `default_firesale_reviews` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(160) COLLATE utf8_unicode_ci DEFAULT NULL,
  `review` longtext COLLATE utf8_unicode_ci,
  `rating_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT '2.5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Volcado de datos para la tabla `default_firesale_reviews`
--

INSERT INTO `default_firesale_reviews` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `product`, `status`, `title`, `review`, `rating_1`) VALUES
(18, '2015-04-14 18:47:16', NULL, 10, 1, 556, NULL, NULL, NULL, '3.0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_routes`
--

CREATE TABLE IF NOT EXISTS `default_firesale_routes` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `translation` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `https` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_core` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `default_firesale_routes`
--

INSERT INTO `default_firesale_routes` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `title`, `slug`, `table`, `map`, `route`, `translation`, `https`, `is_core`) VALUES
(1, '2014-01-30 17:24:13', NULL, NULL, 0, 'lang:firesale:routes:category_custom', 'category-custom', '', 'store/{{ type }}/{{ slug }}', 'store/(order|style)/([a-z0-9]+)', 'firesale/front/category/$1/$2', '0', 1),
(2, '2014-01-30 17:24:13', NULL, NULL, 0, 'lang:firesale:routes:category', 'category', 'firesale_categories', 'store/{{ slug }}{{ any }}', 'store(/[a-z0-9-_/]+)?(/[0-9]+)?', 'firesale/front/category/index$1$2', '0', 1),
(3, '2014-01-30 17:24:13', NULL, NULL, 0, 'lang:firesale:routes:product', 'product', 'firesale_products', 'product/{{ slug }}', 'product/([a-z0-9-_]+)', 'firesale/front/product/index/$1', '0', 1),
(4, '2014-01-30 17:24:13', NULL, NULL, 0, 'lang:firesale:routes:cart', 'cart', '', 'cart{{ any }}', 'cart(/:any)?', 'firesale/front/cart$1', '0', 1),
(5, '2014-01-30 17:24:13', NULL, NULL, 0, 'lang:firesale:routes:order_single', 'orders-single', 'firesale_orders', 'users/orders/{{ id }}', 'users/orders/([0-9]+)', 'firesale/front/orders/view_order/$1', '0', 1),
(6, '2014-01-30 17:24:13', NULL, NULL, 0, 'lang:firesale:routes:orders', 'orders', '', 'users/orders', 'users/orders', 'firesale/front/orders/index', '0', 1),
(7, '2014-01-30 17:24:13', NULL, NULL, 0, 'lang:firesale:routes:addresses', 'addresses', 'firesale_addresses', 'users/addresses{{ any }}', 'users/addresses(/:any)?', 'firesale/front/address$1', '0', 1),
(8, '2014-01-30 17:24:13', NULL, NULL, 0, 'lang:firesale:routes:currency', 'currency', 'firesale_currency', 'currency/{{ id }}', 'currency/([0-9]+)?', 'firesale/front/currency/change/$1', '0', 1),
(9, '2014-01-30 17:24:13', NULL, NULL, 0, 'lang:firesale:routes:new_products', 'new', '', 'new{{ any }}/{{ pagination }}', 'new(/:any)?(/[0-9]+)?', 'firesale/front/latest/index$1$2', '0', 1),
(10, '2014-01-30 17:24:20', NULL, NULL, 0, 'Brand', 'brand', 'firesale_brands', 'brand/{{ slug }}/{{ any }}', 'brand/([a-z0-9-]+)?(/:any)?', 'firesale_brands/front/index/$1$2', NULL, 0),
(11, '2014-01-30 17:24:23', NULL, NULL, 0, 'Downloads', 'downloads', '', 'users/downloads', 'users/downloads', 'firesale_digital/front_digital/index', NULL, 0),
(12, '2014-01-30 17:24:23', NULL, NULL, 0, 'Downloads (File)', 'downloads-file', '', 'users/downloads/file/{{ any }}', 'users/downloads/file/([0-9]+)', 'firesale_digital/front_digital/download/$1', NULL, 0),
(13, '2014-01-30 17:24:28', NULL, NULL, 0, 'Reviews', 'reviews', '', 'reviews/create/{{ id }}', 'reviews/create/([0-9]+)', 'firesale_reviews/front/create/$1', NULL, 0),
(14, '2014-01-30 17:24:29', NULL, NULL, 0, 'lang:firesale:routes:search', 'search', '', 'search/{{ any }}', 'search(/:any)?', 'firesale_search/front/index$1', NULL, 1),
(15, '2014-01-30 17:24:37', NULL, NULL, 0, 'Wishlists', 'wishlist', '', 'users/wishlist{{ any }}', 'users/wishlist(:any)?', 'firesale_wishlist/front$1', '0', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_search`
--

CREATE TABLE IF NOT EXISTS `default_firesale_search` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `term` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(6) NOT NULL,
  `sales` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

--
-- Volcado de datos para la tabla `default_firesale_search`
--

INSERT INTO `default_firesale_search` (`id`, `term`, `count`, `sales`) VALUES
(15, 'ballet', 1, 0),
(16, 'c', 1, 0),
(17, 'prueba_db', 1, 0),
(18, 'lg', 3, 0),
(19, 'a', 3, 0),
(20, 'undefinedchat', 81, 0),
(21, 'troya', 1, 0),
(22, 'image', 1, 0),
(23, 'lips', 1, 0),
(24, 'chaqueta', 6, 0),
(25, 'jjj', 1, 0),
(26, 'asdga', 3, 0),
(27, 'camisa', 11, 0),
(28, 'camiseta', 6, 0),
(29, 'camis', 6, 0),
(30, 'esqueleto estrella gris', 1, 0),
(31, 'skype', 1, 0),
(32, 'buso', 1, 0),
(33, 'camiseta visitante 2013', 20, 0),
(34, 'camiseta visitante', 1, 0),
(35, 'sasdasd', 1, 0),
(36, 'cubrelecho', 1, 0),
(37, 'cubre', 1, 0),
(38, 'caqueta', 1, 0),
(39, 'cha', 6, 0),
(40, 'asd', 2, 0),
(41, 'buscar producto...', 4, 0),
(42, 'adasdasd', 1, 0),
(43, 'samsung', 1, 0),
(44, 'auriculares', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_shipping`
--

CREATE TABLE IF NOT EXISTS `default_firesale_shipping` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_min` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_max` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight_min` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight_max` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `price_ue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_percentage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_packaging` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `default_firesale_shipping`
--

INSERT INTO `default_firesale_shipping` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `title`, `slug`, `company`, `status`, `price`, `price_min`, `price_max`, `weight_min`, `weight_max`, `description`, `price_ue`, `tax_percentage`, `type_packaging`) VALUES
(1, '2013-07-26 11:13:25', '2015-03-19 17:41:48', 1, 1, 'Nacional 4-20kg', 'nacional-420kg', 'Servientrega', '1', '10000', NULL, NULL, '3.01', '20.00', 'Nacional', '2000', '1', '6'),
(2, '2014-04-22 18:06:13', '2015-04-13 11:49:16', 10, 2, 'Nacional 0kg - 3kg', 'nacional-0kg-3kg', 'Servientrega Bogotá', '1', '8200', '0', NULL, '0', '3.00', 'Envio Nacional', '2000', '1', '2'),
(3, '2015-03-13 18:27:04', '2015-03-19 17:42:14', 13, 3, '21-40kg', '2140kg', 'Servientrega', '1', '17000', NULL, NULL, '20.01', '40.00', 'Nacional', '2000', '1', '6'),
(4, '2015-03-13 18:28:07', '2015-03-19 17:42:31', 13, 4, '41-70kg', '4170kg', 'Servientrega', '1', '34000', NULL, NULL, '40.01', '70', 'Nacional', '2000', '1', '6');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_taxes`
--

CREATE TABLE IF NOT EXISTS `default_firesale_taxes` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_firesale_taxes`
--

INSERT INTO `default_firesale_taxes` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `title`, `description`) VALUES
(1, '2014-01-30 17:23:58', NULL, NULL, 0, 'Default', 'Default');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_taxes_assignments`
--

CREATE TABLE IF NOT EXISTS `default_firesale_taxes_assignments` (
  `tax_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `value` decimal(5,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_transactions`
--

CREATE TABLE IF NOT EXISTS `default_firesale_transactions` (
  `reference` longtext,
  `order_id` int(11) DEFAULT NULL,
  `gateway` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `data` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `default_firesale_transactions`
--

INSERT INTO `default_firesale_transactions` (`reference`, `order_id`, `gateway`, `amount`, `currency`, `status`, `data`) VALUES
(NULL, 1, 'dummy', '28.94', 'GBP', 'complete', 'N;'),
(NULL, 2, 'dummy', '13.98', 'GBP', 'complete', 'N;'),
(NULL, 4, 'paypal_express', '21.25', 'COP', 'failed', 'N;'),
(NULL, 4, 'paypal_express', '21.25', 'COP', 'failed', 'N;'),
(NULL, 16, 'dummy', '12.56', 'COP', 'failed', 'N;'),
(NULL, 16, 'dummy', '12.56', 'COP', 'failed', 'N;'),
(NULL, 17, 'dummy', '16.41', 'COP', 'complete', 'N;'),
(NULL, 18, 'dummy', '24.16', 'COP', 'complete', 'N;'),
(NULL, 19, 'dummy', '24.16', 'COP', 'complete', 'N;'),
(NULL, 19, 'dummy', '24.16', 'COP', 'complete', 'N;'),
(NULL, 20, 'dummy', '24.16', 'COP', 'complete', 'N;'),
(NULL, 21, 'dummy', '24.16', 'COP', 'complete', 'N;'),
(NULL, 22, 'dummy', '14.49', 'COP', 'complete', 'N;'),
(NULL, 23, 'dummy', '14.49', 'COP', 'complete', 'N;'),
(NULL, 23, 'dummy', '14.49', 'COP', 'complete', 'N;'),
(NULL, 23, 'dummy', '14.49', 'COP', 'complete', 'N;'),
(NULL, 24, 'dummy', '11.59', 'COP', 'complete', 'N;'),
(NULL, 25, 'dummy', '3.86', 'COP', 'complete', 'N;'),
(NULL, 25, 'dummy', '3.86', 'COP', 'complete', 'N;'),
(NULL, 25, 'dummy', '3.86', 'COP', 'complete', 'N;'),
(NULL, 26, 'dummy', '16.42', 'COP', 'complete', 'N;'),
(NULL, 27, 'dummy', '12.56', 'COP', 'complete', 'N;'),
(NULL, 27, 'dummy', '12.56', 'COP', 'complete', 'N;'),
(NULL, 28, 'dummy', '36.72', 'COP', 'complete', 'N;'),
(NULL, 28, 'dummy', '36.72', 'COP', 'complete', 'N;'),
(NULL, 29, 'dummy', '3.86', 'COP', 'complete', 'N;'),
(NULL, 29, 'dummy', '3.86', 'COP', 'complete', 'N;'),
(NULL, 30, 'dummy', '24.16', 'COP', 'complete', 'N;'),
(NULL, 31, 'dummy', '11.59', 'COP', 'complete', 'N;'),
(NULL, 32, 'dummy', '24.16', 'COP', 'complete', 'N;'),
(NULL, 33, 'dummy', '43.47', 'COP', 'complete', 'N;'),
('', 42, 'payu', NULL, NULL, 'failed', 'N;'),
(NULL, 46, 'dummy', '12.56', 'COP', 'complete', 'N;'),
(NULL, 67, 'dummy', '15.59', 'COP', 'complete', 'N;'),
(NULL, 68, 'dummy', '1160008.69', 'COP', 'complete', 'N;'),
(NULL, 73, 'dummy', '234.82', 'COP', 'complete', 'N;'),
(NULL, 74, 'dummy', '2800014.00', 'COP', 'complete', 'N;'),
(NULL, 78, 'dummy', '266.40', 'COP', 'complete', 'N;'),
(NULL, 80, 'dummy', '15.00', 'COP', 'complete', 'N;'),
(NULL, 82, 'dummy', '13.00', 'COP', 'complete', 'N;'),
(NULL, 84, 'dummy', '15.00', 'COP', 'complete', 'N;'),
(NULL, 89, 'dummy', '20.00', 'COP', 'complete', 'N;'),
(NULL, 91, 'dummy', '12.00', 'COP', 'complete', 'N;'),
(NULL, 91, 'dummy', '12.00', 'COP', 'complete', 'N;'),
(NULL, 91, 'dummy', '12.00', 'COP', 'complete', 'N;'),
(NULL, 93, 'dummy', '15.00', 'COP', 'complete', 'N;'),
(NULL, 93, 'dummy', '15.00', 'COP', 'complete', 'N;'),
(NULL, 93, 'dummy', '15.00', 'COP', 'complete', 'N;'),
(NULL, 93, 'dummy', '15.00', 'COP', 'complete', 'N;'),
(NULL, 93, 'dummy', '15.00', 'COP', 'complete', 'N;'),
(NULL, 93, 'dummy', '15.00', 'COP', 'complete', 'N;'),
(NULL, 93, 'dummy', '15.00', 'COP', 'complete', 'N;'),
(NULL, 98, 'dummy', '145.40', 'COP', 'complete', 'N;'),
(NULL, 100, 'dummy', '2800000.00', 'COP', 'complete', 'N;'),
(NULL, 102, 'dummy', '12.00', 'COP', 'complete', 'N;'),
(NULL, 104, 'dummy', '50.00', 'COP', 'complete', 'N;'),
(NULL, 109, 'dummy', '1191320.00', 'COP', 'complete', 'N;'),
(NULL, 111, 'dummy', '11.05', 'COP', 'complete', 'N;'),
(NULL, 113, 'dummy', '11.05', 'COP', 'complete', 'N;'),
(NULL, 139, 'dummy', '4058086.00', 'COP', 'complete', 'N;'),
(NULL, 144, 'dummy', '1542986.00', 'COP', 'complete', 'N;'),
(NULL, 148, 'dummy', '385700.00', 'COP', 'complete', 'N;'),
(NULL, 150, 'dummy', '2098520.00', 'COP', 'complete', 'N;'),
(NULL, 152, 'dummy', '366415.00', 'COP', 'complete', 'N;'),
(NULL, 161, 'dummy', '129920.00', 'COP', 'complete', 'N;'),
(NULL, 164, 'dummy', '32900.00', 'COP', 'complete', 'N;'),
(NULL, 168, 'dummy', '122030.00', 'COP', 'complete', 'N;'),
(NULL, 171, 'dummy', '44900.00', 'COP', 'complete', 'N;'),
(NULL, 173, 'dummy', '49800.00', 'COP', 'complete', 'N;'),
(NULL, 175, 'dummy', '239900.00', 'COP', 'complete', 'N;'),
(NULL, 185, 'dummy', '31500.00', 'COP', 'complete', 'N;'),
(NULL, 194, 'dummy', '19900.00', 'COP', 'complete', 'N;'),
(NULL, 200, 'dummy', '559200.00', 'COP', 'complete', 'N;'),
(NULL, 202, 'dummy', '31500.00', 'COP', 'complete', 'N;'),
(NULL, 207, 'dummy', '54412.00', 'COP', 'complete', 'N;'),
(NULL, 311, 'dummy', '611200.00', 'COP', 'complete', 'N;'),
(NULL, 313, 'dummy', '164412.00', 'COP', 'complete', 'N;'),
(NULL, 318, 'dummy', '368712.00', 'COP', 'complete', 'N;'),
(NULL, 319, 'dummy', '164411.00', 'COP', 'complete', 'N;'),
(NULL, 321, 'dummy', '109412.00', 'COP', 'complete', 'N;'),
(NULL, 323, 'dummy', '54412.00', 'COP', 'complete', 'N;'),
(NULL, 324, 'dummy', '42412.00', 'COP', 'complete', 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_wishlists`
--

CREATE TABLE IF NOT EXISTS `default_firesale_wishlists` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `privacy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `default_firesale_wishlists`
--

INSERT INTO `default_firesale_wishlists` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `title`, `status`, `privacy`, `description`) VALUES
(7, '2015-01-21 15:12:20', NULL, 13, 2, 'Prueba', '1', '1', 'Prueba nueva'),
(9, '2015-03-10 13:19:14', '2015-04-13 22:38:42', 10, 3, 'Prueba1', '1', '1', 'Lista de prueba'),
(11, '2015-04-13 22:38:07', NULL, 20, 5, 'Jorge Clavijo', '1', '1', 'Prueba'),
(12, '2015-04-13 22:39:09', NULL, 20, 6, 'Jorge C', '1', '1', 'Prueba'),
(13, '2015-04-13 22:39:34', NULL, 20, 7, 'Jorge', '1', '1', 'Prueba1'),
(14, '2015-04-13 22:39:53', NULL, 20, 8, 'Jorge A', '1', '1', 'Pruebaa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_firesale_wishlists_firesale_products`
--

CREATE TABLE IF NOT EXISTS `default_firesale_wishlists_firesale_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `firesale_wishlists_id` int(11) NOT NULL,
  `firesale_products_id` int(11) NOT NULL,
  `firesale_products_qty` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `default_firesale_wishlists_firesale_products`
--

INSERT INTO `default_firesale_wishlists_firesale_products` (`id`, `row_id`, `firesale_wishlists_id`, `firesale_products_id`, `firesale_products_qty`) VALUES
(3, 9, 18, 185, 1),
(6, 9, 18, 353, 1),
(7, 9, 18, 304, 1),
(8, 9, 18, 2, 1),
(9, 9, 18, 325, 1),
(10, 11, 18, 571, 2),
(11, 12, 18, 552, 1),
(12, 11, 18, 572, 1),
(13, 9, 18, 552, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_google_maps`
--

CREATE TABLE IF NOT EXISTS `default_google_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adress` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `schedule` text COLLATE utf8_unicode_ci,
  `coordinate1` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coordinate2` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `default_google_maps`
--

INSERT INTO `default_google_maps` (`id`, `adress`, `name`, `slug`, `description`, `schedule`, `coordinate1`, `coordinate2`, `image`, `position`, `created_at`, `updated_at`) VALUES
(2, 'Avenida 6A', 'Edificio Dinners', 'edificio-dinners', 'Cali, Valle Del Cauca, Colombia', 'Tiempo Completo', '3.465305', '-76.53117', 'uploads/default/google_maps/11f8894ccdb000eb3aa135891f1ef559.jpg', NULL, '2014-09-09 12:15:31', '2014-09-09 17:15:31'),
(3, 'Cali av 6', 'Super Inter', 'super-inter', 'Super inter av 6', 'Tiempo Completo', '3.465886', '-76.529828', 'uploads/default/google_maps/d909cfc05a990cfc4b56b378bf525d95.jpg', NULL, '2014-09-09 12:17:18', '2014-09-09 17:17:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_google_maps_categories`
--

CREATE TABLE IF NOT EXISTS `default_google_maps_categories` (
  `google_map_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `default_google_maps_categories`
--

INSERT INTO `default_google_maps_categories` (`google_map_id`, `category_id`) VALUES
(2, 2),
(3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_google_maps_intro`
--

CREATE TABLE IF NOT EXISTS `default_google_maps_intro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_google_maps_intro`
--

INSERT INTO `default_google_maps_intro` (`id`, `text`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_google_map_categories`
--

CREATE TABLE IF NOT EXISTS `default_google_map_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `parent` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `outstanding` int(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `default_google_map_categories`
--

INSERT INTO `default_google_map_categories` (`id`, `title`, `position`, `parent`, `slug`, `outstanding`, `created_at`, `updated_at`) VALUES
(1, 'Bogota', NULL, 0, 'bogota', 1, '2014-09-09 12:07:14', '2014-09-09 17:07:14'),
(2, 'Cali', NULL, 0, 'cali', 1, '2014-09-09 12:13:03', '2014-09-09 17:13:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_google_map_images`
--

CREATE TABLE IF NOT EXISTS `default_google_map_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `google_map_id` int(11) NOT NULL,
  `path` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_groups`
--

CREATE TABLE IF NOT EXISTS `default_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `default_groups`
--

INSERT INTO `default_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'User'),
(3, 'pruebas', 'Pruebas'),
(4, 'colaboradores', 'Colaboradores');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_home_banner`
--

CREATE TABLE IF NOT EXISTS `default_home_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `default_home_banner`
--

INSERT INTO `default_home_banner` (`id`, `image`, `title`, `text`, `link`) VALUES
(10, 'uploads/default/home_banner/5dac3a24a66fd471127d25fb1c42edda.jpg', '', '', ''),
(11, 'uploads/default/home_banner/db61068097c209c85e635e6821093701.jpg', '', '', ''),
(12, 'uploads/default/home_banner/2886fd3a2ce8fb2ce6bc4d93a24b158f.jpg', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_home_banner_two`
--

CREATE TABLE IF NOT EXISTS `default_home_banner_two` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `default_home_banner_two`
--

INSERT INTO `default_home_banner_two` (`id`, `image`, `title`, `text`, `link`) VALUES
(1, 'uploads/default/home_banner_two/5ddb7f993b6622e65cb7ad241ae0d7f0.jpg', 'COLECCIONES', 'PARA HOMBRES', 'http://imaginamos.com'),
(2, 'uploads/default/home_banner_two/1663ed56d9f267687499436dc1acc688.jpg', 'COLECCIONES', 'PARA MUJERES', 'http://imaginamos.com'),
(3, 'uploads/default/home_banner_two/79d0f43552399a044615ba8ae5b39bc4.jpg', 'COLECCIONES', 'PARA NIÑOS', 'http://imaginamos.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_home_others`
--

CREATE TABLE IF NOT EXISTS `default_home_others` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `default_home_others`
--

INSERT INTO `default_home_others` (`id`, `content`, `text`, `link`, `type`) VALUES
(4, 'uploads/default/home_others/021860eed458c1e19daa096977de3cea.png', 'hola', 'www.google.com', 1),
(8, 'http://www.youtube.com/watch?v=yT_WfATG5eg', 'Feria Latinoamericana del Entretenimiento', NULL, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_home_pautas`
--

CREATE TABLE IF NOT EXISTS `default_home_pautas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `default_home_pautas`
--

INSERT INTO `default_home_pautas` (`id`, `imagen`, `link`) VALUES
(4, 'uploads/default/home_pautas/8838261fcfb8968d50027ba5f5fd9f66.jpg', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_home_slider`
--

CREATE TABLE IF NOT EXISTS `default_home_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `default_home_slider`
--

INSERT INTO `default_home_slider` (`id`, `producto_id`, `orden`) VALUES
(10, 83, NULL),
(9, 97, NULL),
(12, 176, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_keywords`
--

CREATE TABLE IF NOT EXISTS `default_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_keywords_applied`
--

CREATE TABLE IF NOT EXISTS `default_keywords_applied` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `keyword_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_migrations`
--

CREATE TABLE IF NOT EXISTS `default_migrations` (
  `version` int(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `default_migrations`
--

INSERT INTO `default_migrations` (`version`) VALUES
(125);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_modules`
--

CREATE TABLE IF NOT EXISTS `default_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `skip_xss` tinyint(1) NOT NULL,
  `is_frontend` tinyint(1) NOT NULL,
  `is_backend` tinyint(1) NOT NULL,
  `menu` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `installed` tinyint(1) NOT NULL,
  `is_core` tinyint(1) NOT NULL,
  `updated_on` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

--
-- Volcado de datos para la tabla `default_modules`
--

INSERT INTO `default_modules` (`id`, `name`, `slug`, `version`, `type`, `description`, `skip_xss`, `is_frontend`, `is_backend`, `menu`, `enabled`, `installed`, `is_core`, `updated_on`) VALUES
(1, 'a:25:{s:2:"en";s:8:"Settings";s:2:"ar";s:18:"الإعدادات";s:2:"br";s:15:"Configurações";s:2:"pt";s:15:"Configurações";s:2:"cs";s:10:"Nastavení";s:2:"da";s:13:"Indstillinger";s:2:"de";s:13:"Einstellungen";s:2:"el";s:18:"Ρυθμίσεις";s:2:"es";s:15:"Configuraciones";s:2:"fa";s:14:"تنظیمات";s:2:"fi";s:9:"Asetukset";s:2:"fr";s:11:"Paramètres";s:2:"he";s:12:"הגדרות";s:2:"id";s:10:"Pengaturan";s:2:"it";s:12:"Impostazioni";s:2:"lt";s:10:"Nustatymai";s:2:"nl";s:12:"Instellingen";s:2:"pl";s:10:"Ustawienia";s:2:"ru";s:18:"Настройки";s:2:"sl";s:10:"Nastavitve";s:2:"tw";s:12:"網站設定";s:2:"cn";s:12:"网站设定";s:2:"hu";s:14:"Beállítások";s:2:"th";s:21:"ตั้งค่า";s:2:"se";s:14:"Inställningar";}', 'settings', '1.0.0', NULL, 'a:25:{s:2:"en";s:89:"Allows administrators to update settings like Site Name, messages and email address, etc.";s:2:"ar";s:161:"تمكن المدراء من تحديث الإعدادات كإسم الموقع، والرسائل وعناوين البريد الإلكتروني، .. إلخ.";s:2:"br";s:120:"Permite com que administradores e a equipe consigam trocar as configurações do website incluindo o nome e descrição.";s:2:"pt";s:113:"Permite com que os administradores consigam alterar as configurações do website incluindo o nome e descrição.";s:2:"cs";s:102:"Umožňuje administrátorům měnit nastavení webu jako jeho jméno, zprávy a emailovou adresu apod.";s:2:"da";s:90:"Lader administratorer opdatere indstillinger som sidenavn, beskeder og email adresse, etc.";s:2:"de";s:92:"Erlaubt es Administratoren die Einstellungen der Seite wie Name und Beschreibung zu ändern.";s:2:"el";s:230:"Επιτρέπει στους διαχειριστές να τροποποιήσουν ρυθμίσεις όπως το Όνομα του Ιστοτόπου, τα μηνύματα και τις διευθύνσεις email, κ.α.";s:2:"es";s:131:"Permite a los administradores y al personal configurar los detalles del sitio como el nombre del sitio y la descripción del mismo.";s:2:"fa";s:105:"تنظیمات سایت در این ماژول توسط ادمین هاس سایت انجام می شود";s:2:"fi";s:105:"Mahdollistaa sivuston asetusten muokkaamisen, kuten sivuston nimen, viestit ja sähköpostiosoitteet yms.";s:2:"fr";s:118:"Permet aux admistrateurs de modifier les paramètres du site : nom du site, description, messages, adresse email, etc.";s:2:"he";s:116:"ניהול הגדרות שונות של האתר כגון: שם האתר, הודעות, כתובות דואר וכו";s:2:"id";s:112:"Memungkinkan administrator untuk dapat memperbaharui pengaturan seperti nama situs, pesan dan alamat email, dsb.";s:2:"it";s:109:"Permette agli amministratori di aggiornare impostazioni quali Nome del Sito, messaggi e indirizzo email, etc.";s:2:"lt";s:104:"Leidžia administratoriams keisti puslapio vavadinimą, žinutes, administratoriaus el. pašta ir kitą.";s:2:"nl";s:114:"Maakt het administratoren en medewerkers mogelijk om websiteinstellingen zoals naam en beschrijving te veranderen.";s:2:"pl";s:103:"Umożliwia administratorom zmianę ustawień strony jak nazwa strony, opis, e-mail administratora, itd.";s:2:"ru";s:135:"Управление настройками сайта - Имя сайта, сообщения, почтовые адреса и т.п.";s:2:"sl";s:98:"Dovoljuje administratorjem posodobitev nastavitev kot je Ime strani, sporočil, email naslova itd.";s:2:"tw";s:99:"網站管理者可更新的重要網站設定。例如：網站名稱、訊息、電子郵件等。";s:2:"cn";s:99:"网站管理者可更新的重要网站设定。例如：网站名称、讯息、电子邮件等。";s:2:"hu";s:125:"Lehetővé teszi az adminok számára a beállítások frissítését, mint a weboldal neve, üzenetek, e-mail címek, stb...";s:2:"th";s:232:"ให้ผู้ดูแลระบบสามารถปรับปรุงการตั้งค่าเช่นชื่อเว็บไซต์ ข้อความและอีเมล์เป็นต้น";s:2:"se";s:84:"Administratören kan uppdatera webbplatsens titel, meddelanden och E-postadress etc.";}', 1, 0, 1, 'settings', 1, 1, 1, 1421698961),
(2, 'a:11:{s:2:"en";s:12:"Streams Core";s:2:"pt";s:14:"Núcleo Fluxos";s:2:"fr";s:10:"Noyau Flux";s:2:"el";s:23:"Πυρήνας Ροών";s:2:"se";s:18:"Streams grundmodul";s:2:"tw";s:14:"Streams 核心";s:2:"cn";s:14:"Streams 核心";s:2:"ar";s:31:"الجداول الأساسية";s:2:"it";s:12:"Streams Core";s:2:"fa";s:26:"هسته استریم ها";s:2:"fi";s:13:"Striimit ydin";}', 'streams_core', '1.0.0', NULL, 'a:11:{s:2:"en";s:29:"Core data module for streams.";s:2:"pt";s:37:"Módulo central de dados para fluxos.";s:2:"fr";s:32:"Noyau de données pour les Flux.";s:2:"el";s:113:"Προγραμματιστικός πυρήνας για την λειτουργία ροών δεδομένων.";s:2:"se";s:50:"Streams grundmodul för enklare hantering av data.";s:2:"tw";s:29:"Streams 核心資料模組。";s:2:"cn";s:29:"Streams 核心资料模组。";s:2:"ar";s:57:"وحدة البيانات الأساسية للجداول";s:2:"it";s:17:"Core dello Stream";s:2:"fa";s:48:"ماژول مرکزی برای استریم ها";s:2:"fi";s:48:"Ydin datan hallinoiva moduuli striimejä varten.";}', 1, 0, 0, '0', 1, 1, 1, 1421698961),
(3, 'a:21:{s:2:"en";s:15:"Email Templates";s:2:"ar";s:48:"قوالب الرسائل الإلكترونية";s:2:"br";s:17:"Modelos de e-mail";s:2:"pt";s:17:"Modelos de e-mail";s:2:"da";s:16:"Email skabeloner";s:2:"el";s:22:"Δυναμικά email";s:2:"es";s:19:"Plantillas de email";s:2:"fa";s:26:"قالب های ایمیل";s:2:"fr";s:17:"Modèles d''emails";s:2:"he";s:12:"תבניות";s:2:"id";s:14:"Template Email";s:2:"lt";s:22:"El. laiškų šablonai";s:2:"nl";s:15:"Email sjablonen";s:2:"ru";s:25:"Шаблоны почты";s:2:"sl";s:14:"Email predloge";s:2:"tw";s:12:"郵件範本";s:2:"cn";s:12:"邮件范本";s:2:"hu";s:15:"E-mail sablonok";s:2:"fi";s:25:"Sähköposti viestipohjat";s:2:"th";s:33:"แม่แบบอีเมล";s:2:"se";s:12:"E-postmallar";}', 'templates', '1.1.0', NULL, 'a:21:{s:2:"en";s:46:"Create, edit, and save dynamic email templates";s:2:"ar";s:97:"أنشئ، عدّل واحفظ قوالب البريد الإلكترني الديناميكية.";s:2:"br";s:51:"Criar, editar e salvar modelos de e-mail dinâmicos";s:2:"pt";s:51:"Criar, editar e salvar modelos de e-mail dinâmicos";s:2:"da";s:49:"Opret, redigér og gem dynamiske emailskabeloner.";s:2:"el";s:108:"Δημιουργήστε, επεξεργαστείτε και αποθηκεύστε δυναμικά email.";s:2:"es";s:54:"Crear, editar y guardar plantillas de email dinámicas";s:2:"fa";s:92:"ایحاد، ویرایش و ذخیره ی قالب های ایمیل به صورت پویا";s:2:"fr";s:61:"Créer, éditer et sauver dynamiquement des modèles d''emails";s:2:"he";s:54:"ניהול של תבניות דואר אלקטרוני";s:2:"id";s:55:"Membuat, mengedit, dan menyimpan template email dinamis";s:2:"lt";s:58:"Kurk, tvarkyk ir saugok dinaminius el. laiškų šablonus.";s:2:"nl";s:49:"Maak, bewerk, en beheer dynamische emailsjablonen";s:2:"ru";s:127:"Создавайте, редактируйте и сохраняйте динамические почтовые шаблоны";s:2:"sl";s:52:"Ustvari, uredi in shrani spremenljive email predloge";s:2:"tw";s:61:"新增、編輯與儲存可顯示動態資料的 email 範本";s:2:"cn";s:61:"新增、编辑与储存可显示动态资料的 email 范本";s:2:"hu";s:63:"Csináld, szerkeszd és mentsd el a dinamikus e-mail sablonokat";s:2:"fi";s:66:"Lisää, muokkaa ja tallenna dynaamisia sähköposti viestipohjia.";s:2:"th";s:129:"การสร้างแก้ไขและบันทึกแม่แบบอีเมลแบบไดนามิก";s:2:"se";s:49:"Skapa, redigera och spara dynamiska E-postmallar.";}', 1, 0, 1, 'structure', 1, 1, 1, 1421698961),
(4, 'a:25:{s:2:"en";s:7:"Add-ons";s:2:"ar";s:16:"الإضافات";s:2:"br";s:12:"Complementos";s:2:"pt";s:12:"Complementos";s:2:"cs";s:8:"Doplňky";s:2:"da";s:7:"Add-ons";s:2:"de";s:13:"Erweiterungen";s:2:"el";s:16:"Πρόσθετα";s:2:"es";s:9:"Agregados";s:2:"fa";s:17:"افزونه ها";s:2:"fi";s:9:"Lisäosat";s:2:"fr";s:10:"Extensions";s:2:"he";s:12:"תוספות";s:2:"id";s:7:"Pengaya";s:2:"it";s:7:"Add-ons";s:2:"lt";s:7:"Priedai";s:2:"nl";s:7:"Add-ons";s:2:"pl";s:12:"Rozszerzenia";s:2:"ru";s:20:"Дополнения";s:2:"sl";s:11:"Razširitve";s:2:"tw";s:12:"附加模組";s:2:"cn";s:12:"附加模组";s:2:"hu";s:14:"Bővítmények";s:2:"th";s:27:"ส่วนเสริม";s:2:"se";s:8:"Tillägg";}', 'addons', '2.0.0', NULL, 'a:25:{s:2:"en";s:59:"Allows admins to see a list of currently installed modules.";s:2:"ar";s:91:"تُمكّن المُدراء من معاينة جميع الوحدات المُثبّتة.";s:2:"br";s:75:"Permite aos administradores ver a lista dos módulos instalados atualmente.";s:2:"pt";s:75:"Permite aos administradores ver a lista dos módulos instalados atualmente.";s:2:"cs";s:68:"Umožňuje administrátorům vidět seznam nainstalovaných modulů.";s:2:"da";s:63:"Lader administratorer se en liste over de installerede moduler.";s:2:"de";s:56:"Zeigt Administratoren alle aktuell installierten Module.";s:2:"el";s:152:"Επιτρέπει στους διαχειριστές να προβάλουν μια λίστα των εγκατεστημένων πρόσθετων.";s:2:"es";s:71:"Permite a los administradores ver una lista de los módulos instalados.";s:2:"fa";s:93:"مشاهده لیست افزونه ها و مدیریت آنها برای ادمین سایت";s:2:"fi";s:60:"Listaa järjestelmänvalvojalle käytössä olevat moduulit.";s:2:"fr";s:66:"Permet aux administrateurs de voir la liste des modules installés";s:2:"he";s:160:"נותן אופציה למנהל לראות רשימה של המודולים אשר מותקנים כעת באתר או להתקין מודולים נוספים";s:2:"id";s:57:"Memperlihatkan kepada admin daftar modul yang terinstall.";s:2:"it";s:83:"Permette agli amministratori di vedere una lista dei moduli attualmente installati.";s:2:"lt";s:75:"Vartotojai ir svečiai gali komentuoti jūsų naujienas, puslapius ar foto.";s:2:"nl";s:79:"Stelt admins in staat om een overzicht van geinstalleerde modules te genereren.";s:2:"pl";s:81:"Umożliwiają administratorowi wgląd do listy obecnie zainstalowanych modułów.";s:2:"ru";s:83:"Список модулей, которые установлены на сайте.";s:2:"sl";s:65:"Dovoljuje administratorjem pregled trenutno nameščenih modulov.";s:2:"tw";s:54:"管理員可以檢視目前已經安裝模組的列表";s:2:"cn";s:54:"管理员可以检视目前已经安装模组的列表";s:2:"hu";s:79:"Lehetővé teszi az adminoknak, hogy lássák a telepített modulok listáját.";s:2:"th";s:162:"ช่วยให้ผู้ดูแลระบบดูรายการของโมดูลที่ติดตั้งในปัจจุบัน";s:2:"se";s:67:"Gör det möjligt för administratören att se installerade mouler.";}', 0, 0, 1, '0', 1, 1, 1, 1421698961),
(5, 'a:17:{s:2:"en";s:4:"Blog";s:2:"ar";s:16:"المدوّنة";s:2:"br";s:4:"Blog";s:2:"pt";s:4:"Blog";s:2:"el";s:18:"Ιστολόγιο";s:2:"fa";s:8:"بلاگ";s:2:"he";s:8:"בלוג";s:2:"id";s:4:"Blog";s:2:"lt";s:6:"Blogas";s:2:"pl";s:4:"Blog";s:2:"ru";s:8:"Блог";s:2:"tw";s:6:"文章";s:2:"cn";s:6:"文章";s:2:"hu";s:4:"Blog";s:2:"fi";s:5:"Blogi";s:2:"th";s:15:"บล็อก";s:2:"se";s:5:"Blogg";}', 'blog', '2.0.0', NULL, 'a:25:{s:2:"en";s:18:"Post blog entries.";s:2:"ar";s:48:"أنشر المقالات على مدوّنتك.";s:2:"br";s:30:"Escrever publicações de blog";s:2:"pt";s:39:"Escrever e editar publicações no blog";s:2:"cs";s:49:"Publikujte nové články a příspěvky na blog.";s:2:"da";s:17:"Skriv blogindlæg";s:2:"de";s:47:"Veröffentliche neue Artikel und Blog-Einträge";s:2:"sl";s:23:"Objavite blog prispevke";s:2:"fi";s:28:"Kirjoita blogi artikkeleita.";s:2:"el";s:93:"Δημιουργήστε άρθρα και εγγραφές στο ιστολόγιο σας.";s:2:"es";s:54:"Escribe entradas para los artículos y blog (web log).";s:2:"fa";s:44:"مقالات منتشر شده در بلاگ";s:2:"fr";s:34:"Poster des articles d''actualités.";s:2:"he";s:19:"ניהול בלוג";s:2:"id";s:15:"Post entri blog";s:2:"it";s:36:"Pubblica notizie e post per il blog.";s:2:"lt";s:40:"Rašykite naujienas bei blog''o įrašus.";s:2:"nl";s:41:"Post nieuwsartikelen en blogs op uw site.";s:2:"pl";s:27:"Dodawaj nowe wpisy na blogu";s:2:"ru";s:49:"Управление записями блога.";s:2:"tw";s:42:"發表新聞訊息、部落格等文章。";s:2:"cn";s:42:"发表新闻讯息、部落格等文章。";s:2:"th";s:48:"โพสต์รายการบล็อก";s:2:"hu";s:32:"Blog bejegyzések létrehozása.";s:2:"se";s:18:"Inlägg i bloggen.";}', 1, 1, 1, 'content', 1, 1, 1, 1421698961),
(6, 'a:25:{s:2:"en";s:8:"Comments";s:2:"ar";s:18:"التعليقات";s:2:"br";s:12:"Comentários";s:2:"pt";s:12:"Comentários";s:2:"cs";s:11:"Komentáře";s:2:"da";s:11:"Kommentarer";s:2:"de";s:10:"Kommentare";s:2:"el";s:12:"Σχόλια";s:2:"es";s:11:"Comentarios";s:2:"fi";s:9:"Kommentit";s:2:"fr";s:12:"Commentaires";s:2:"fa";s:10:"نظرات";s:2:"he";s:12:"תגובות";s:2:"id";s:8:"Komentar";s:2:"it";s:8:"Commenti";s:2:"lt";s:10:"Komentarai";s:2:"nl";s:8:"Reacties";s:2:"pl";s:10:"Komentarze";s:2:"ru";s:22:"Комментарии";s:2:"sl";s:10:"Komentarji";s:2:"tw";s:6:"回應";s:2:"cn";s:6:"回应";s:2:"hu";s:16:"Hozzászólások";s:2:"th";s:33:"ความคิดเห็น";s:2:"se";s:11:"Kommentarer";}', 'comments', '1.1.0', NULL, 'a:25:{s:2:"en";s:76:"Users and guests can write comments for content like blog, pages and photos.";s:2:"ar";s:152:"يستطيع الأعضاء والزوّار كتابة التعليقات على المُحتوى كالأخبار، والصفحات والصّوَر.";s:2:"br";s:97:"Usuários e convidados podem escrever comentários para quase tudo com suporte nativo ao captcha.";s:2:"pt";s:100:"Utilizadores e convidados podem escrever comentários para quase tudo com suporte nativo ao captcha.";s:2:"cs";s:100:"Uživatelé a hosté mohou psát komentáře k obsahu, např. neovinkám, stránkám a fotografiím.";s:2:"da";s:83:"Brugere og besøgende kan skrive kommentarer til indhold som blog, sider og fotoer.";s:2:"de";s:65:"Benutzer und Gäste können für fast alles Kommentare schreiben.";s:2:"el";s:224:"Οι χρήστες και οι επισκέπτες μπορούν να αφήνουν σχόλια για περιεχόμενο όπως το ιστολόγιο, τις σελίδες και τις φωτογραφίες.";s:2:"es";s:130:"Los usuarios y visitantes pueden escribir comentarios en casi todo el contenido con el soporte de un sistema de captcha incluído.";s:2:"fa";s:168:"کاربران و مهمان ها می توانند نظرات خود را بر روی محتوای سایت در بلاگ و دیگر قسمت ها ارائه دهند";s:2:"fi";s:107:"Käyttäjät ja vieraat voivat kirjoittaa kommentteja eri sisältöihin kuten uutisiin, sivuihin ja kuviin.";s:2:"fr";s:130:"Les utilisateurs et les invités peuvent écrire des commentaires pour quasiment tout grâce au générateur de captcha intégré.";s:2:"he";s:94:"משתמשי האתר יכולים לרשום תגובות למאמרים, תמונות וכו";s:2:"id";s:100:"Pengguna dan pengunjung dapat menuliskan komentaruntuk setiap konten seperti blog, halaman dan foto.";s:2:"it";s:85:"Utenti e visitatori possono scrivere commenti ai contenuti quali blog, pagine e foto.";s:2:"lt";s:75:"Vartotojai ir svečiai gali komentuoti jūsų naujienas, puslapius ar foto.";s:2:"nl";s:52:"Gebruikers en gasten kunnen reageren op bijna alles.";s:2:"pl";s:93:"Użytkownicy i goście mogą dodawać komentarze z wbudowanym systemem zabezpieczeń captcha.";s:2:"ru";s:187:"Пользователи и гости могут добавлять комментарии к новостям, информационным страницам и фотографиям.";s:2:"sl";s:89:"Uporabniki in obiskovalci lahko vnesejo komentarje na vsebino kot je blok, stra ali slike";s:2:"tw";s:75:"用戶和訪客可以針對新聞、頁面與照片等內容發表回應。";s:2:"cn";s:75:"用户和访客可以针对新闻、页面与照片等内容发表回应。";s:2:"hu";s:117:"A felhasználók és a vendégek hozzászólásokat írhatnak a tartalomhoz (bejegyzésekhez, oldalakhoz, fotókhoz).";s:2:"th";s:240:"ผู้ใช้งานและผู้เยี่ยมชมสามารถเขียนความคิดเห็นในเนื้อหาของหน้าเว็บบล็อกและภาพถ่าย";s:2:"se";s:98:"Användare och besökare kan skriva kommentarer till innehåll som blogginlägg, sidor och bilder.";}', 0, 0, 1, 'content', 1, 1, 1, 1421698961),
(7, 'a:25:{s:2:"en";s:7:"Contact";s:2:"ar";s:14:"الإتصال";s:2:"br";s:7:"Contato";s:2:"pt";s:8:"Contacto";s:2:"cs";s:7:"Kontakt";s:2:"da";s:7:"Kontakt";s:2:"de";s:7:"Kontakt";s:2:"el";s:22:"Επικοινωνία";s:2:"es";s:8:"Contacto";s:2:"fa";s:18:"تماس با ما";s:2:"fi";s:13:"Ota yhteyttä";s:2:"fr";s:7:"Contact";s:2:"he";s:17:"יצירת קשר";s:2:"id";s:6:"Kontak";s:2:"it";s:10:"Contattaci";s:2:"lt";s:18:"Kontaktinė formą";s:2:"nl";s:7:"Contact";s:2:"pl";s:7:"Kontakt";s:2:"ru";s:27:"Обратная связь";s:2:"sl";s:7:"Kontakt";s:2:"tw";s:12:"聯絡我們";s:2:"cn";s:12:"联络我们";s:2:"hu";s:9:"Kapcsolat";s:2:"th";s:18:"ติดต่อ";s:2:"se";s:7:"Kontakt";}', 'contact', '1.0.0', NULL, 'a:25:{s:2:"en";s:112:"Adds a form to your site that allows visitors to send emails to you without disclosing an email address to them.";s:2:"ar";s:157:"إضافة استمارة إلى موقعك تُمكّن الزوّار من مراسلتك دون علمهم بعنوان البريد الإلكتروني.";s:2:"br";s:139:"Adiciona um formulário para o seu site permitir aos visitantes que enviem e-mails para voce sem divulgar um endereço de e-mail para eles.";s:2:"pt";s:116:"Adiciona um formulário ao seu site que permite aos visitantes enviarem e-mails sem divulgar um endereço de e-mail.";s:2:"cs";s:149:"Přidá na web kontaktní formulář pro návštěvníky a uživatele, díky kterému vás mohou kontaktovat i bez znalosti vaší e-mailové adresy.";s:2:"da";s:123:"Tilføjer en formular på din side som tillader besøgende at sende mails til dig, uden at du skal opgive din email-adresse";s:2:"de";s:119:"Fügt ein Formular hinzu, welches Besuchern erlaubt Emails zu schreiben, ohne die Kontakt Email-Adresse offen zu legen.";s:2:"el";s:273:"Προσθέτει μια φόρμα στον ιστότοπό σας που επιτρέπει σε επισκέπτες να σας στέλνουν μηνύμα μέσω email χωρίς να τους αποκαλύπτεται η διεύθυνση του email σας.";s:2:"fa";s:239:"فرم تماس را به سایت اضافه می کند تا مراجعین بتوانند بدون اینکه ایمیل شما را بدانند برای شما پیغام هایی را از طریق ایمیل ارسال نمایند.";s:2:"es";s:156:"Añade un formulario a tu sitio que permitirá a los visitantes enviarte correos electrónicos a ti sin darles tu dirección de correo directamente a ellos.";s:2:"fi";s:128:"Luo lomakkeen sivustollesi, josta kävijät voivat lähettää sähköpostia tietämättä vastaanottajan sähköpostiosoitetta.";s:2:"fr";s:122:"Ajoute un formulaire à votre site qui permet aux visiteurs de vous envoyer un e-mail sans révéler votre adresse e-mail.";s:2:"he";s:155:"מוסיף תופס יצירת קשר לאתר על מנת לא לחסוף כתובת דואר האלקטרוני של האתר למנועי פרסומות";s:2:"id";s:149:"Menambahkan formulir ke dalam situs Anda yang memungkinkan pengunjung untuk mengirimkan email kepada Anda tanpa memberikan alamat email kepada mereka";s:2:"it";s:119:"Aggiunge un modulo al tuo sito che permette ai visitatori di inviarti email senza mostrare loro il tuo indirizzo email.";s:2:"lt";s:124:"Prideda jūsų puslapyje formą leidžianti lankytojams siūsti jums el. laiškus neatskleidžiant jūsų el. pašto adreso.";s:2:"nl";s:125:"Voegt een formulier aan de site toe waarmee bezoekers een email kunnen sturen, zonder dat u ze een emailadres hoeft te tonen.";s:2:"pl";s:126:"Dodaje formularz kontaktowy do Twojej strony, który pozwala użytkownikom wysłanie maila za pomocą formularza kontaktowego.";s:2:"ru";s:234:"Добавляет форму обратной связи на сайт, через которую посетители могут отправлять вам письма, при этом адрес Email остаётся скрыт.";s:2:"sl";s:113:"Dodaj obrazec za kontakt da vam lahko obiskovalci pošljejo sporočilo brez da bi jim razkrili vaš email naslov.";s:2:"tw";s:147:"為您的網站新增「聯絡我們」的功能，對訪客是較為清楚便捷的聯絡方式，也無須您將電子郵件公開在網站上。";s:2:"cn";s:147:"为您的网站新增“联络我们”的功能，对访客是较为清楚便捷的联络方式，也无须您将电子邮件公开在网站上。";s:2:"th";s:316:"เพิ่มแบบฟอร์มในเว็บไซต์ของคุณ ช่วยให้ผู้เยี่ยมชมสามารถส่งอีเมลถึงคุณโดยไม่ต้องเปิดเผยที่อยู่อีเมลของพวกเขา";s:2:"hu";s:156:"Létrehozható vele olyan űrlap, amely lehetővé teszi a látogatók számára, hogy e-mailt küldjenek neked úgy, hogy nem feded fel az e-mail címedet.";s:2:"se";s:53:"Lägger till ett kontaktformulär till din webbplats.";}', 0, 0, 0, '0', 1, 1, 1, 1421698961),
(8, 'a:8:{s:2:"en";s:7:"Domains";s:2:"el";s:7:"Domains";s:2:"fr";s:8:"Domaines";s:2:"se";s:8:"Domäner";s:2:"it";s:6:"Domini";s:2:"ar";s:27:"أسماء النطاقات";s:2:"tw";s:6:"網域";s:2:"cn";s:6:"网域";}', 'domains', '1.0.0', NULL, 'a:8:{s:2:"en";s:39:"Create domain aliases for your website.";s:2:"el";s:91:"Διαχειριστείτε συνώνυμα domain για τον ιστότοπό σας.";s:2:"fr";s:47:"Créer des alias de domaine pour votre site web";s:2:"se";s:36:"Skapa domänalias för din webbplats";s:2:"it";s:26:"Crea alias per il tuo sito";s:2:"ar";s:57:"أنشئ أسماء نطاقات بديلة لموقعك.";s:2:"tw";s:33:"為您的網站建立網域別名";s:2:"cn";s:33:"为您的网站建立网域别名";}', 0, 0, 1, 'structure', 1, 1, 1, 1421698961),
(9, 'a:24:{s:2:"en";s:5:"Files";s:2:"ar";s:16:"الملفّات";s:2:"br";s:8:"Arquivos";s:2:"pt";s:9:"Ficheiros";s:2:"cs";s:7:"Soubory";s:2:"da";s:5:"Filer";s:2:"de";s:7:"Dateien";s:2:"el";s:12:"Αρχεία";s:2:"es";s:8:"Archivos";s:2:"fa";s:13:"فایل ها";s:2:"fi";s:9:"Tiedostot";s:2:"fr";s:8:"Fichiers";s:2:"he";s:10:"קבצים";s:2:"id";s:4:"File";s:2:"it";s:4:"File";s:2:"lt";s:6:"Failai";s:2:"nl";s:9:"Bestanden";s:2:"ru";s:10:"Файлы";s:2:"sl";s:8:"Datoteke";s:2:"tw";s:6:"檔案";s:2:"cn";s:6:"档案";s:2:"hu";s:7:"Fájlok";s:2:"th";s:12:"ไฟล์";s:2:"se";s:5:"Filer";}', 'files', '2.0.0', NULL, 'a:24:{s:2:"en";s:40:"Manages files and folders for your site.";s:2:"ar";s:50:"إدارة ملفات ومجلّدات موقعك.";s:2:"br";s:53:"Permite gerenciar facilmente os arquivos de seu site.";s:2:"pt";s:59:"Permite gerir facilmente os ficheiros e pastas do seu site.";s:2:"cs";s:43:"Spravujte soubory a složky na vašem webu.";s:2:"da";s:41:"Administrer filer og mapper for dit site.";s:2:"de";s:35:"Verwalte Dateien und Verzeichnisse.";s:2:"el";s:100:"Διαχειρίζεται αρχεία και φακέλους για το ιστότοπό σας.";s:2:"es";s:43:"Administra archivos y carpetas en tu sitio.";s:2:"fa";s:79:"مدیریت فایل های چند رسانه ای و فولدر ها سایت";s:2:"fi";s:43:"Hallitse sivustosi tiedostoja ja kansioita.";s:2:"fr";s:46:"Gérer les fichiers et dossiers de votre site.";s:2:"he";s:47:"ניהול תיקיות וקבצים שבאתר";s:2:"id";s:42:"Mengatur file dan folder dalam situs Anda.";s:2:"it";s:38:"Gestisci file e cartelle del tuo sito.";s:2:"lt";s:28:"Katalogų ir bylų valdymas.";s:2:"nl";s:41:"Beheer bestanden en mappen op uw website.";s:2:"ru";s:78:"Управление файлами и папками вашего сайта.";s:2:"sl";s:38:"Uredi datoteke in mape na vaši strani";s:2:"tw";s:33:"管理網站中的檔案與目錄";s:2:"cn";s:33:"管理网站中的档案与目录";s:2:"hu";s:41:"Fájlok és mappák kezelése az oldalon.";s:2:"th";s:141:"บริหารจัดการไฟล์และโฟลเดอร์สำหรับเว็บไซต์ของคุณ";s:2:"se";s:45:"Hanterar filer och mappar för din webbplats.";}', 0, 0, 1, 'content', 1, 1, 1, 1421698961),
(10, 'a:8:{s:2:"en";s:5:"Store";s:2:"br";s:5:"Store";s:2:"fr";s:5:"Store";s:2:"id";s:5:"Store";s:2:"it";s:5:"Store";s:2:"lt";s:5:"Store";s:2:"pl";s:5:"Store";s:2:"es";s:5:"Store";}', 'firesale', '1.2.3-dev', NULL, 'a:8:{s:2:"en";s:47:"The lightweight & extensible eCommerce platform";s:2:"br";s:47:"The lightweight & extensible eCommerce platform";s:2:"fr";s:20:"Plateforme eCommerce";s:2:"id";s:47:"The lightweight & extensible eCommerce platform";s:2:"it";s:33:"Una leggera piattaforma eCommerce";s:2:"lt";s:47:"The lightweight & extensible eCommerce platform";s:2:"pl";s:38:"Lekka i elastyczna platforma eCommerce";s:2:"es";s:47:"The lightweight & extensible eCommerce platform";}', 0, 1, 1, '0', 1, 1, 1, 1421698961),
(11, 'a:2:{s:2:"en";s:10:"Attributes";s:2:"it";s:9:"Attributi";}', 'firesale_attributes', '1.0.2-dev', NULL, 'a:2:{s:2:"en";s:29:"Product attribute management.";s:2:"it";s:31:"Gestione attributi dei prodotti";}', 0, 0, 0, 'FireSale', 1, 1, 1, 1421698961),
(12, 'a:3:{s:2:"en";s:6:"Brands";s:2:"fr";s:7:"Marques";s:2:"it";s:6:"Marche";}', 'firesale_brands', '1.0.2-dev', NULL, 'a:3:{s:2:"en";s:16:"Brand Management";s:2:"fr";s:19:"Gestion des marques";s:2:"it";s:15:"Gestione Marche";}', 0, 0, 1, '0', 1, 1, 1, 1421698961),
(13, 'a:2:{s:2:"en";s:6:"Design";s:2:"it";s:6:"Design";}', 'firesale_design', '1.0.2-dev', NULL, 'a:2:{s:2:"en";s:61:"Complete control over your page layouts, style and javascript";s:2:"it";s:72:"Controllo completo sul layout della pagina, sugli stili e sul javascript";}', 0, 0, 0, 'FireSale', 1, 1, 1, 1421698961),
(14, 'a:2:{s:2:"en";s:7:"Digital";s:2:"it";s:17:"Prodotti Digitali";}', 'firesale_digital', '1.0.0', NULL, 'a:3:{s:2:"en";s:35:"Sell digital products and downloads";s:2:"fr";s:49:"Vente de produits numériques en téléchargement";s:2:"it";s:46:"Vendi prodotti digitali e permetti il download";}', 0, 1, 0, '0', 1, 1, 1, 1421698961),
(15, 'a:3:{s:2:"en";s:14:"Discount Codes";s:2:"it";s:13:"Codici Sconto";s:2:"fr";s:19:"Codes de réduction";}', 'firesale_discount_codes', '1.1.2', NULL, 'a:3:{s:2:"en";s:36:"Apply discounts to the shopping cart";s:2:"it";s:38:"Applica codici sconto per gli acquisti";s:2:"fr";s:47:"Appliquez des codes de réduction sur le panier";}', 0, 1, 1, '0', 1, 1, 1, 1421698961),
(16, 'a:2:{s:2:"en";s:23:"FireSale Dispatch Notes";s:2:"it";s:27:"FireSale note di spedizione";}', 'firesale_dispatch_notes', '1.0.2-dev', NULL, 'a:2:{s:2:"en";s:36:"Print dispatch notes for your orders";s:2:"it";s:43:"Stampa le note di spedizione per gli ordini";}', 0, 0, 0, 'FireSale', 1, 1, 1, 1421698961),
(17, 'a:2:{s:2:"en";s:13:"More Products";s:2:"it";s:25:"Prodotti visti di recente";}', 'firesale_more_products', '1.1.0', NULL, 'a:2:{s:2:"en";s:71:"Automatic tracking of recently viewed, also bought and related products";s:2:"it";s:84:"Tracciamento automatico dei prodotti visti di recente, prodotti comprati e correlati";}', 0, 0, 0, '0', 1, 1, 1, 1421698961),
(18, 'a:1:{s:2:"en";s:16:"FireSale Reviews";}', 'firesale_reviews', '1.1.0', NULL, 'a:1:{s:2:"en";s:30:"Product reviews and management";}', 0, 0, 1, '0', 1, 1, 1, 1421698961),
(19, 'a:3:{s:2:"en";s:15:"FireSale Search";s:2:"fr";s:18:"FireSale Recherche";s:2:"it";s:16:"FireSale Ricerca";}', 'firesale_search', '1.2.2-dev', NULL, 'a:3:{s:2:"en";s:50:"Product and category search with ajax capabilities";s:2:"fr";s:66:"Recherche dans les produits et la catégories, dynamisée par AJAX";s:2:"it";s:53:"Ricerca di prodotti e categorie, compatibile con AJAX";}', 0, 1, 0, '0', 1, 1, 1, 1421698961),
(20, 'a:3:{s:2:"en";s:3:"SEO";s:2:"fr";s:3:"SEO";s:2:"it";s:3:"SEO";}', 'firesale_seo', '1.2.0', NULL, 'a:3:{s:2:"en";s:57:"Search Engine Optimisation for products, categories, etc.";s:2:"fr";s:72:"Optimisation du référencement pour les produits, les catégories, etc.";s:2:"it";s:67:"Ottimizzazione per i motori di ricerca per prodotti, categorie etc.";}', 0, 0, 0, 'FireSale', 1, 1, 1, 1421698961),
(21, 'a:3:{s:2:"en";s:8:"Shipping";s:2:"fr";s:11:"Expédition";s:2:"it";s:10:"Spedizioni";}', 'firesale_shipping', '1.2.2-dev', NULL, 'a:3:{s:2:"en";s:33:"Basic band-based shipping options";s:2:"fr";s:35:"Gestionnaire de modes d''expédition";s:2:"it";s:30:"Gestione base delle spedizioni";}', 0, 0, 1, '0', 1, 1, 1, 1421698961),
(22, 'a:1:{s:2:"en";s:16:"FireSale Streams";}', 'firesale_streams', '1.0', NULL, 'a:1:{s:2:"en";s:51:"Easily add/edit/remove fields from FireSale streams";}', 0, 0, 1, '0', 1, 1, 1, 1421698961),
(23, 'a:1:{s:2:"en";s:18:"FireSale Wishlists";}', 'firesale_wishlist', '0.0.1', NULL, 'a:1:{s:2:"en";s:32:"Wishlist creation and management";}', 0, 0, 1, '0', 1, 1, 1, 1421698961),
(24, 'a:24:{s:2:"en";s:6:"Groups";s:2:"ar";s:18:"المجموعات";s:2:"br";s:6:"Grupos";s:2:"pt";s:6:"Grupos";s:2:"cs";s:7:"Skupiny";s:2:"da";s:7:"Grupper";s:2:"de";s:7:"Gruppen";s:2:"el";s:12:"Ομάδες";s:2:"es";s:6:"Grupos";s:2:"fa";s:13:"گروه ها";s:2:"fi";s:7:"Ryhmät";s:2:"fr";s:7:"Groupes";s:2:"he";s:12:"קבוצות";s:2:"id";s:4:"Grup";s:2:"it";s:6:"Gruppi";s:2:"lt";s:7:"Grupės";s:2:"nl";s:7:"Groepen";s:2:"ru";s:12:"Группы";s:2:"sl";s:7:"Skupine";s:2:"tw";s:6:"群組";s:2:"cn";s:6:"群组";s:2:"hu";s:9:"Csoportok";s:2:"th";s:15:"กลุ่ม";s:2:"se";s:7:"Grupper";}', 'groups', '1.0.0', NULL, 'a:24:{s:2:"en";s:54:"Users can be placed into groups to manage permissions.";s:2:"ar";s:100:"يمكن وضع المستخدمين في مجموعات لتسهيل إدارة صلاحياتهم.";s:2:"br";s:72:"Usuários podem ser inseridos em grupos para gerenciar suas permissões.";s:2:"pt";s:74:"Utilizadores podem ser inseridos em grupos para gerir as suas permissões.";s:2:"cs";s:77:"Uživatelé mohou být rozřazeni do skupin pro lepší správu oprávnění.";s:2:"da";s:49:"Brugere kan inddeles i grupper for adgangskontrol";s:2:"de";s:85:"Benutzer können zu Gruppen zusammengefasst werden um diesen Zugriffsrechte zu geben.";s:2:"el";s:168:"Οι χρήστες μπορούν να τοποθετηθούν σε ομάδες και έτσι να διαχειριστείτε τα δικαιώματά τους.";s:2:"es";s:75:"Los usuarios podrán ser colocados en grupos para administrar sus permisos.";s:2:"fa";s:149:"کاربرها می توانند در گروه های ساماندهی شوند تا بتوان اجازه های مختلفی را ایجاد کرد";s:2:"fi";s:84:"Käyttäjät voidaan liittää ryhmiin, jotta käyttöoikeuksia voidaan hallinnoida.";s:2:"fr";s:82:"Les utilisateurs peuvent appartenir à des groupes afin de gérer les permissions.";s:2:"he";s:62:"נותן אפשרות לאסוף משתמשים לקבוצות";s:2:"id";s:68:"Pengguna dapat dikelompokkan ke dalam grup untuk mengatur perizinan.";s:2:"it";s:69:"Gli utenti possono essere inseriti in gruppi per gestirne i permessi.";s:2:"lt";s:67:"Vartotojai gali būti priskirti grupei tam, kad valdyti jų teises.";s:2:"nl";s:73:"Gebruikers kunnen in groepen geplaatst worden om rechten te kunnen geven.";s:2:"ru";s:134:"Пользователей можно объединять в группы, для управления правами доступа.";s:2:"sl";s:64:"Uporabniki so lahko razvrščeni v skupine za urejanje dovoljenj";s:2:"tw";s:45:"用戶可以依群組分類並管理其權限";s:2:"cn";s:45:"用户可以依群组分类并管理其权限";s:2:"hu";s:73:"A felhasználók csoportokba rendezhetőek a jogosultságok kezelésére.";s:2:"th";s:84:"สามารถวางผู้ใช้ลงในกลุ่มเพื่";s:2:"se";s:76:"Användare kan delas in i grupper för att hantera roller och behörigheter.";}', 0, 0, 1, 'users', 1, 1, 1, 1421698961),
(25, 'a:17:{s:2:"en";s:8:"Keywords";s:2:"ar";s:21:"كلمات البحث";s:2:"br";s:14:"Palavras-chave";s:2:"pt";s:14:"Palavras-chave";s:2:"da";s:9:"Nøgleord";s:2:"el";s:27:"Λέξεις Κλειδιά";s:2:"fa";s:21:"کلمات کلیدی";s:2:"fr";s:10:"Mots-Clés";s:2:"id";s:10:"Kata Kunci";s:2:"nl";s:14:"Sleutelwoorden";s:2:"tw";s:6:"鍵詞";s:2:"cn";s:6:"键词";s:2:"hu";s:11:"Kulcsszavak";s:2:"fi";s:10:"Avainsanat";s:2:"sl";s:15:"Ključne besede";s:2:"th";s:15:"คำค้น";s:2:"se";s:9:"Nyckelord";}', 'keywords', '1.1.0', NULL, 'a:17:{s:2:"en";s:71:"Maintain a central list of keywords to label and organize your content.";s:2:"ar";s:124:"أنشئ مجموعة من كلمات البحث التي تستطيع من خلالها وسم وتنظيم المحتوى.";s:2:"br";s:85:"Mantém uma lista central de palavras-chave para rotular e organizar o seu conteúdo.";s:2:"pt";s:85:"Mantém uma lista central de palavras-chave para rotular e organizar o seu conteúdo.";s:2:"da";s:72:"Vedligehold en central liste af nøgleord for at organisere dit indhold.";s:2:"el";s:181:"Συντηρεί μια κεντρική λίστα από λέξεις κλειδιά για να οργανώνετε μέσω ετικετών το περιεχόμενό σας.";s:2:"fa";s:110:"حفظ و نگهداری لیست مرکزی از کلمات کلیدی برای سازماندهی محتوا";s:2:"fr";s:87:"Maintenir une liste centralisée de Mots-Clés pour libeller et organiser vos contenus.";s:2:"id";s:71:"Memantau daftar kata kunci untuk melabeli dan mengorganisasikan konten.";s:2:"nl";s:91:"Beheer een centrale lijst van sleutelwoorden om uw content te categoriseren en organiseren.";s:2:"tw";s:64:"集中管理可用於標題與內容的鍵詞(keywords)列表。";s:2:"cn";s:64:"集中管理可用于标题与内容的键词(keywords)列表。";s:2:"hu";s:65:"Ez egy központi kulcsszó lista a cimkékhez és a tartalmakhoz.";s:2:"fi";s:92:"Hallinnoi keskitettyä listaa avainsanoista merkitäksesi ja järjestelläksesi sisältöä.";s:2:"sl";s:82:"Vzdržuj centralni seznam ključnih besed za označevanje in ogranizacijo vsebine.";s:2:"th";s:189:"ศูนย์กลางการปรับปรุงคำค้นในการติดฉลากและจัดระเบียบเนื้อหาของคุณ";s:2:"se";s:61:"Hantera nyckelord för att organisera webbplatsens innehåll.";}', 0, 0, 1, 'data', 1, 1, 1, 1421698961),
(26, 'a:15:{s:2:"en";s:11:"Maintenance";s:2:"pt";s:12:"Manutenção";s:2:"ar";s:14:"الصيانة";s:2:"el";s:18:"Συντήρηση";s:2:"hu";s:13:"Karbantartás";s:2:"fa";s:15:"نگه داری";s:2:"fi";s:9:"Ylläpito";s:2:"fr";s:11:"Maintenance";s:2:"id";s:12:"Pemeliharaan";s:2:"it";s:12:"Manutenzione";s:2:"se";s:10:"Underhåll";s:2:"sl";s:12:"Vzdrževanje";s:2:"th";s:39:"การบำรุงรักษา";s:2:"tw";s:6:"維護";s:2:"cn";s:6:"维护";}', 'maintenance', '1.0.0', NULL, 'a:15:{s:2:"en";s:63:"Manage the site cache and export information from the database.";s:2:"pt";s:68:"Gerir o cache do seu site e exportar informações da base de dados.";s:2:"ar";s:81:"حذف عناصر الذاكرة المخبأة عبر واجهة الإدارة.";s:2:"el";s:142:"Διαγραφή αντικειμένων προσωρινής αποθήκευσης μέσω της περιοχής διαχείρισης.";s:2:"id";s:60:"Mengatur cache situs dan mengexport informasi dari database.";s:2:"it";s:65:"Gestisci la cache del sito e esporta le informazioni dal database";s:2:"fa";s:73:"مدیریت کش سایت و صدور اطلاعات از دیتابیس";s:2:"fr";s:71:"Gérer le cache du site et exporter les contenus de la base de données";s:2:"fi";s:59:"Hallinoi sivuston välimuistia ja vie tietoa tietokannasta.";s:2:"hu";s:66:"Az oldal gyorsítótár kezelése és az adatbázis exportálása.";s:2:"se";s:76:"Underhåll webbplatsens cache och exportera data från webbplatsens databas.";s:2:"sl";s:69:"Upravljaj s predpomnilnikom strani (cache) in izvozi podatke iz baze.";s:2:"th";s:150:"การจัดการแคชเว็บไซต์และข้อมูลการส่งออกจากฐานข้อมูล";s:2:"tw";s:45:"經由管理介面手動刪除暫存資料。";s:2:"cn";s:45:"经由管理介面手动删除暂存资料。";}', 0, 0, 1, 'data', 1, 1, 1, 1421698961),
(27, 'a:25:{s:2:"en";s:10:"Navigation";s:2:"ar";s:14:"الروابط";s:2:"br";s:11:"Navegação";s:2:"pt";s:11:"Navegação";s:2:"cs";s:8:"Navigace";s:2:"da";s:10:"Navigation";s:2:"de";s:10:"Navigation";s:2:"el";s:16:"Πλοήγηση";s:2:"es";s:11:"Navegación";s:2:"fa";s:11:"منو ها";s:2:"fi";s:10:"Navigointi";s:2:"fr";s:10:"Navigation";s:2:"he";s:10:"ניווט";s:2:"id";s:8:"Navigasi";s:2:"it";s:11:"Navigazione";s:2:"lt";s:10:"Navigacija";s:2:"nl";s:9:"Navigatie";s:2:"pl";s:9:"Nawigacja";s:2:"ru";s:18:"Навигация";s:2:"sl";s:10:"Navigacija";s:2:"tw";s:12:"導航選單";s:2:"cn";s:12:"导航选单";s:2:"th";s:36:"ตัวช่วยนำทาง";s:2:"hu";s:11:"Navigáció";s:2:"se";s:10:"Navigation";}', 'navigation', '1.1.0', NULL, 'a:25:{s:2:"en";s:78:"Manage links on navigation menus and all the navigation groups they belong to.";s:2:"ar";s:85:"إدارة روابط وقوائم ومجموعات الروابط في الموقع.";s:2:"br";s:91:"Gerenciar links do menu de navegação e todos os grupos de navegação pertencentes a ele.";s:2:"pt";s:93:"Gerir todos os grupos dos menus de navegação e os links de navegação pertencentes a eles.";s:2:"cs";s:73:"Správa odkazů v navigaci a všech souvisejících navigačních skupin.";s:2:"da";s:82:"Håndtér links på navigationsmenuerne og alle navigationsgrupperne de tilhører.";s:2:"de";s:76:"Verwalte Links in Navigationsmenüs und alle zugehörigen Navigationsgruppen";s:2:"el";s:207:"Διαχειριστείτε τους συνδέσμους στα μενού πλοήγησης και όλες τις ομάδες συνδέσμων πλοήγησης στις οποίες ανήκουν.";s:2:"es";s:102:"Administra links en los menús de navegación y en todos los grupos de navegación al cual pertenecen.";s:2:"fa";s:68:"مدیریت منو ها و گروه های مربوط به آنها";s:2:"fi";s:91:"Hallitse linkkejä navigointi valikoissa ja kaikkia navigointi ryhmiä, joihin ne kuuluvat.";s:2:"fr";s:97:"Gérer les liens du menu Navigation et tous les groupes de navigation auxquels ils appartiennent.";s:2:"he";s:73:"ניהול שלוחות תפריטי ניווט וקבוצות ניווט";s:2:"id";s:73:"Mengatur tautan pada menu navigasi dan semua pengelompokan grup navigasi.";s:2:"it";s:97:"Gestisci i collegamenti dei menu di navigazione e tutti i gruppi di navigazione da cui dipendono.";s:2:"lt";s:95:"Tvarkyk nuorodas navigacijų menių ir visas navigacijų grupes kurioms tos nuorodos priklauso.";s:2:"nl";s:92:"Beheer koppelingen op de navigatiemenu&apos;s en alle navigatiegroepen waar ze onder vallen.";s:2:"pl";s:95:"Zarządzaj linkami w menu nawigacji oraz wszystkimi grupami nawigacji do których one należą.";s:2:"ru";s:136:"Управление ссылками в меню навигации и группах, к которым они принадлежат.";s:2:"sl";s:64:"Uredi povezave v meniju in vse skupine povezav ki jim pripadajo.";s:2:"tw";s:72:"管理導航選單中的連結，以及它們所隸屬的導航群組。";s:2:"cn";s:72:"管理导航选单中的连结，以及它们所隶属的导航群组。";s:2:"th";s:108:"จัดการการเชื่อมโยงนำทางและกลุ่มนำทาง";s:2:"se";s:33:"Hantera länkar och länkgrupper.";s:2:"hu";s:100:"Linkek kezelése a navigációs menükben és a navigációs csoportok kezelése, amikhez tartoznak.";}', 0, 0, 1, 'structure', 1, 1, 1, 1421698961),
(28, 'a:25:{s:2:"en";s:5:"Pages";s:2:"ar";s:14:"الصفحات";s:2:"br";s:8:"Páginas";s:2:"pt";s:8:"Páginas";s:2:"cs";s:8:"Stránky";s:2:"da";s:5:"Sider";s:2:"de";s:6:"Seiten";s:2:"el";s:14:"Σελίδες";s:2:"es";s:8:"Páginas";s:2:"fa";s:14:"صفحه ها ";s:2:"fi";s:5:"Sivut";s:2:"fr";s:5:"Pages";s:2:"he";s:8:"דפים";s:2:"id";s:7:"Halaman";s:2:"it";s:6:"Pagine";s:2:"lt";s:9:"Puslapiai";s:2:"nl";s:13:"Pagina&apos;s";s:2:"pl";s:6:"Strony";s:2:"ru";s:16:"Страницы";s:2:"sl";s:6:"Strani";s:2:"tw";s:6:"頁面";s:2:"cn";s:6:"页面";s:2:"hu";s:7:"Oldalak";s:2:"th";s:21:"หน้าเพจ";s:2:"se";s:5:"Sidor";}', 'pages', '2.2.0', NULL, 'a:25:{s:2:"en";s:55:"Add custom pages to the site with any content you want.";s:2:"ar";s:99:"إضافة صفحات مُخصّصة إلى الموقع تحتوي أية مُحتوى تريده.";s:2:"br";s:82:"Adicionar páginas personalizadas ao site com qualquer conteúdo que você queira.";s:2:"pt";s:86:"Adicionar páginas personalizadas ao seu site com qualquer conteúdo que você queira.";s:2:"cs";s:74:"Přidávejte vlastní stránky na web s jakýmkoliv obsahem budete chtít.";s:2:"da";s:71:"Tilføj brugerdefinerede sider til dit site med det indhold du ønsker.";s:2:"de";s:49:"Füge eigene Seiten mit anpassbaren Inhalt hinzu.";s:2:"el";s:152:"Προσθέστε και επεξεργαστείτε σελίδες στον ιστότοπό σας με ό,τι περιεχόμενο θέλετε.";s:2:"es";s:77:"Agrega páginas customizadas al sitio con cualquier contenido que tu quieras.";s:2:"fa";s:96:"ایحاد صفحات جدید و دلخواه با هر محتوایی که دوست دارید";s:2:"fi";s:47:"Lisää mitä tahansa sisältöä sivustollesi.";s:2:"fr";s:89:"Permet d''ajouter sur le site des pages personalisées avec le contenu que vous souhaitez.";s:2:"he";s:35:"ניהול דפי תוכן האתר";s:2:"id";s:75:"Menambahkan halaman ke dalam situs dengan konten apapun yang Anda perlukan.";s:2:"it";s:73:"Aggiungi pagine personalizzate al sito con qualsiesi contenuto tu voglia.";s:2:"lt";s:46:"Pridėkite nuosavus puslapius betkokio turinio";s:2:"nl";s:70:"Voeg aangepaste pagina&apos;s met willekeurige inhoud aan de site toe.";s:2:"pl";s:53:"Dodaj własne strony z dowolną treścią do witryny.";s:2:"ru";s:134:"Управление информационными страницами сайта, с произвольным содержимым.";s:2:"sl";s:44:"Dodaj stran s kakršno koli vsebino želite.";s:2:"tw";s:39:"為您的網站新增自定的頁面。";s:2:"cn";s:39:"为您的网站新增自定的页面。";s:2:"th";s:168:"เพิ่มหน้าเว็บที่กำหนดเองไปยังเว็บไซต์ของคุณตามที่ต้องการ";s:2:"hu";s:67:"Saját oldalak hozzáadása a weboldalhoz, akármilyen tartalommal.";s:2:"se";s:39:"Lägg till egna sidor till webbplatsen.";}', 1, 1, 1, 'content', 1, 1, 1, 1421698961),
(29, 'a:25:{s:2:"en";s:11:"Permissions";s:2:"ar";s:18:"الصلاحيات";s:2:"br";s:11:"Permissões";s:2:"pt";s:11:"Permissões";s:2:"cs";s:12:"Oprávnění";s:2:"da";s:14:"Adgangskontrol";s:2:"de";s:14:"Zugriffsrechte";s:2:"el";s:20:"Δικαιώματα";s:2:"es";s:8:"Permisos";s:2:"fa";s:15:"اجازه ها";s:2:"fi";s:16:"Käyttöoikeudet";s:2:"fr";s:11:"Permissions";s:2:"he";s:12:"הרשאות";s:2:"id";s:9:"Perizinan";s:2:"it";s:8:"Permessi";s:2:"lt";s:7:"Teisės";s:2:"nl";s:15:"Toegangsrechten";s:2:"pl";s:11:"Uprawnienia";s:2:"ru";s:25:"Права доступа";s:2:"sl";s:10:"Dovoljenja";s:2:"tw";s:6:"權限";s:2:"cn";s:6:"权限";s:2:"hu";s:14:"Jogosultságok";s:2:"th";s:18:"สิทธิ์";s:2:"se";s:13:"Behörigheter";}', 'permissions', '1.0.0', NULL, 'a:25:{s:2:"en";s:68:"Control what type of users can see certain sections within the site.";s:2:"ar";s:127:"التحكم بإعطاء الصلاحيات للمستخدمين للوصول إلى أقسام الموقع المختلفة.";s:2:"br";s:68:"Controle quais tipos de usuários podem ver certas seções no site.";s:2:"pt";s:75:"Controle quais os tipos de utilizadores podem ver certas secções no site.";s:2:"cs";s:93:"Spravujte oprávnění pro jednotlivé typy uživatelů a ke kterým sekcím mají přístup.";s:2:"da";s:72:"Kontroller hvilken type brugere der kan se bestemte sektioner på sitet.";s:2:"de";s:70:"Regelt welche Art von Benutzer welche Sektion in der Seite sehen kann.";s:2:"el";s:180:"Ελέγξτε τα δικαιώματα χρηστών και ομάδων χρηστών όσο αφορά σε διάφορες λειτουργίες του ιστοτόπου.";s:2:"es";s:81:"Controla que tipo de usuarios pueden ver secciones específicas dentro del sitio.";s:2:"fa";s:59:"مدیریت اجازه های گروه های کاربری";s:2:"fi";s:72:"Hallitse minkä tyyppisiin osioihin käyttäjät pääsevät sivustolla.";s:2:"fr";s:104:"Permet de définir les autorisations des groupes d''utilisateurs pour afficher les différentes sections.";s:2:"he";s:75:"ניהול הרשאות כניסה לאיזורים מסוימים באתר";s:2:"id";s:76:"Mengontrol tipe pengguna mana yang dapat mengakses suatu bagian dalam situs.";s:2:"it";s:78:"Controlla che tipo di utenti posssono accedere a determinate sezioni del sito.";s:2:"lt";s:72:"Kontroliuokite kokio tipo varotojai kokią dalį puslapio gali pasiekti.";s:2:"nl";s:71:"Bepaal welke typen gebruikers toegang hebben tot gedeeltes van de site.";s:2:"pl";s:79:"Ustaw, którzy użytkownicy mogą mieć dostęp do odpowiednich sekcji witryny.";s:2:"ru";s:209:"Управление правами доступа, ограничение доступа определённых групп пользователей к произвольным разделам сайта.";s:2:"sl";s:85:"Uredite dovoljenja kateri tip uporabnika lahko vidi določena področja vaše strani.";s:2:"tw";s:81:"用來控制不同類別的用戶，設定其瀏覽特定網站內容的權限。";s:2:"cn";s:81:"用来控制不同类别的用户，设定其浏览特定网站内容的权限。";s:2:"hu";s:129:"A felhasználók felügyelet alatt tartására, hogy milyen típusú felhasználók, mit láthatnak, mely szakaszain az oldalnak.";s:2:"th";s:117:"ควบคุมว่าผู้ใช้งานจะเห็นหมวดหมู่ไหนบ้าง";s:2:"se";s:27:"Hantera gruppbehörigheter.";}', 0, 0, 1, 'users', 1, 1, 1, 1421698961);
INSERT INTO `default_modules` (`id`, `name`, `slug`, `version`, `type`, `description`, `skip_xss`, `is_frontend`, `is_backend`, `menu`, `enabled`, `installed`, `is_core`, `updated_on`) VALUES
(30, 'a:24:{s:2:"en";s:9:"Redirects";s:2:"ar";s:18:"التوجيهات";s:2:"br";s:17:"Redirecionamentos";s:2:"pt";s:17:"Redirecionamentos";s:2:"cs";s:16:"Přesměrování";s:2:"da";s:13:"Omadressering";s:2:"el";s:30:"Ανακατευθύνσεις";s:2:"es";s:13:"Redirecciones";s:2:"fa";s:17:"انتقال ها";s:2:"fi";s:18:"Uudelleenohjaukset";s:2:"fr";s:12:"Redirections";s:2:"he";s:12:"הפניות";s:2:"id";s:8:"Redirect";s:2:"it";s:11:"Reindirizzi";s:2:"lt";s:14:"Peradresavimai";s:2:"nl";s:12:"Verwijzingen";s:2:"ru";s:30:"Перенаправления";s:2:"sl";s:12:"Preusmeritve";s:2:"tw";s:6:"轉址";s:2:"cn";s:6:"转址";s:2:"hu";s:17:"Átirányítások";s:2:"pl";s:14:"Przekierowania";s:2:"th";s:42:"เปลี่ยนเส้นทาง";s:2:"se";s:14:"Omdirigeringar";}', 'redirects', '1.0.0', NULL, 'a:24:{s:2:"en";s:33:"Redirect from one URL to another.";s:2:"ar";s:47:"التوجيه من رابط URL إلى آخر.";s:2:"br";s:39:"Redirecionamento de uma URL para outra.";s:2:"pt";s:40:"Redirecionamentos de uma URL para outra.";s:2:"cs";s:43:"Přesměrujte z jedné adresy URL na jinou.";s:2:"da";s:35:"Omadresser fra en URL til en anden.";s:2:"el";s:81:"Ανακατευθείνετε μια διεύθυνση URL σε μια άλλη";s:2:"es";s:34:"Redireccionar desde una URL a otra";s:2:"fa";s:63:"انتقال دادن یک صفحه به یک آدرس دیگر";s:2:"fi";s:45:"Uudelleenohjaa käyttäjän paikasta toiseen.";s:2:"fr";s:34:"Redirection d''une URL à un autre.";s:2:"he";s:43:"הפניות מכתובת אחת לאחרת";s:2:"id";s:40:"Redirect dari satu URL ke URL yang lain.";s:2:"it";s:35:"Reindirizza da una URL ad un altra.";s:2:"lt";s:56:"Peradresuokite puslapį iš vieno adreso (URL) į kitą.";s:2:"nl";s:38:"Verwijs vanaf een URL naar een andere.";s:2:"ru";s:78:"Перенаправления с одного адреса на другой.";s:2:"sl";s:44:"Preusmeritev iz enega URL naslova na drugega";s:2:"tw";s:33:"將網址轉址、重新定向。";s:2:"cn";s:33:"将网址转址、重新定向。";s:2:"hu";s:38:"Egy URL átirányítása egy másikra.";s:2:"pl";s:44:"Przekierowanie z jednego adresu URL na inny.";s:2:"th";s:123:"เปลี่ยนเส้นทางจากที่หนึ่งไปยังอีกที่หนึ่ง";s:2:"se";s:38:"Omdirigera från en URL till en annan.";}', 0, 0, 1, 'structure', 1, 1, 1, 1421698961),
(31, 'a:9:{s:2:"en";s:6:"Search";s:2:"fr";s:9:"Recherche";s:2:"se";s:4:"Sök";s:2:"ar";s:10:"البحث";s:2:"tw";s:6:"搜尋";s:2:"cn";s:6:"搜寻";s:2:"it";s:7:"Ricerca";s:2:"fa";s:10:"جستجو";s:2:"fi";s:4:"Etsi";}', 'search', '1.0.0', NULL, 'a:9:{s:2:"en";s:72:"Search through various types of content with this modular search system.";s:2:"fr";s:84:"Rechercher parmi différents types de contenus avec système de recherche modulaire.";s:2:"se";s:36:"Sök igenom olika typer av innehåll";s:2:"ar";s:102:"ابحث في أنواع مختلفة من المحتوى باستخدام نظام البحث هذا.";s:2:"tw";s:63:"此模組可用以搜尋網站中不同類型的資料內容。";s:2:"cn";s:63:"此模组可用以搜寻网站中不同类型的资料内容。";s:2:"it";s:71:"Cerca tra diversi tipi di contenuti con il sistema di reicerca modulare";s:2:"fa";s:115:"توسط این ماژول می توانید در محتواهای مختلف وبسایت جستجو نمایید.";s:2:"fi";s:76:"Etsi eri tyypistä sisältöä tällä modulaarisella hakujärjestelmällä.";}', 0, 0, 0, '0', 1, 1, 1, 1421698961),
(32, 'a:20:{s:2:"en";s:7:"Sitemap";s:2:"ar";s:23:"خريطة الموقع";s:2:"br";s:12:"Mapa do Site";s:2:"pt";s:12:"Mapa do Site";s:2:"de";s:7:"Sitemap";s:2:"el";s:31:"Χάρτης Ιστότοπου";s:2:"es";s:14:"Mapa del Sitio";s:2:"fa";s:17:"نقشه سایت";s:2:"fi";s:10:"Sivukartta";s:2:"fr";s:12:"Plan du site";s:2:"id";s:10:"Peta Situs";s:2:"it";s:14:"Mappa del sito";s:2:"lt";s:16:"Svetainės medis";s:2:"nl";s:7:"Sitemap";s:2:"ru";s:21:"Карта сайта";s:2:"tw";s:12:"網站地圖";s:2:"cn";s:12:"网站地图";s:2:"th";s:21:"ไซต์แมพ";s:2:"hu";s:13:"Oldaltérkép";s:2:"se";s:9:"Sajtkarta";}', 'sitemap', '1.3.0', NULL, 'a:21:{s:2:"en";s:87:"The sitemap module creates an index of all pages and an XML sitemap for search engines.";s:2:"ar";s:120:"وحدة خريطة الموقع تنشئ فهرساً لجميع الصفحات وملف XML لمحركات البحث.";s:2:"br";s:102:"O módulo de mapa do site cria um índice de todas as páginas e um sitemap XML para motores de busca.";s:2:"pt";s:102:"O módulo do mapa do site cria um índice de todas as páginas e um sitemap XML para motores de busca.";s:2:"da";s:86:"Sitemapmodulet opretter et indeks over alle sider og et XML sitemap til søgemaskiner.";s:2:"de";s:92:"Die Sitemap Modul erstellt einen Index aller Seiten und eine XML-Sitemap für Suchmaschinen.";s:2:"el";s:190:"Δημιουργεί έναν κατάλογο όλων των σελίδων και έναν χάρτη σελίδων σε μορφή XML για τις μηχανές αναζήτησης.";s:2:"es";s:111:"El módulo de mapa crea un índice de todas las páginas y un mapa del sitio XML para los motores de búsqueda.";s:2:"fa";s:150:"ماژول نقشه سایت یک لیست از همه ی صفحه ها به فرمت فایل XML برای موتور های جستجو می سازد";s:2:"fi";s:82:"sivukartta moduuli luo hakemisto kaikista sivuista ja XML sivukartta hakukoneille.";s:2:"fr";s:106:"Le module sitemap crée un index de toutes les pages et un plan de site XML pour les moteurs de recherche.";s:2:"id";s:110:"Modul peta situs ini membuat indeks dari setiap halaman dan sebuah format XML untuk mempermudah mesin pencari.";s:2:"it";s:104:"Il modulo mappa del sito crea un indice di tutte le pagine e una sitemap in XML per i motori di ricerca.";s:2:"lt";s:86:"struktūra modulis sukuria visų puslapių ir XML Sitemap paieškos sistemų indeksas.";s:2:"nl";s:89:"De sitemap module maakt een index van alle pagina''s en een XML sitemap voor zoekmachines.";s:2:"ru";s:144:"Карта модуль создает индекс всех страниц и карта сайта XML для поисковых систем.";s:2:"tw";s:84:"站點地圖模塊創建一個索引的所有網頁和XML網站地圖搜索引擎。";s:2:"cn";s:84:"站点地图模块创建一个索引的所有网页和XML网站地图搜索引擎。";s:2:"th";s:202:"โมดูลไซต์แมพสามารถสร้างดัชนีของหน้าเว็บทั้งหมดสำหรับเครื่องมือค้นหา.";s:2:"hu";s:94:"Ez a modul indexeli az összes oldalt és egy XML oldaltéképet generál a keresőmotoroknak.";s:2:"se";s:86:"Sajtkarta, modulen skapar ett index av alla sidor och en XML-sitemap för sökmotorer.";}', 0, 1, 0, 'content', 1, 1, 1, 1421698961),
(33, 'a:6:{s:2:"en";s:7:"Streams";s:2:"es";s:7:"Streams";s:2:"de";s:7:"Streams";s:2:"el";s:8:"Ροές";s:2:"lt";s:7:"Srautai";s:2:"fr";s:7:"Streams";}', 'streams', '2.3.2', NULL, 'a:6:{s:2:"en";s:36:"Manage, structure, and display data.";s:2:"es";s:41:"Administra, estructura, y presenta datos.";s:2:"de";s:49:"Verwalte, strukturiere und veröffentliche Daten.";s:2:"el";s:106:"Διαχείριση, δόμηση και προβολή πληροφοριών και δεδομένων.";s:2:"lt";N;s:2:"fr";s:43:"Gérer, structurer et afficher des données";}', 0, 0, 1, 'content', 1, 1, 1, 1421698961),
(34, 'a:25:{s:2:"en";s:5:"Users";s:2:"ar";s:20:"المستخدمون";s:2:"br";s:9:"Usuários";s:2:"pt";s:12:"Utilizadores";s:2:"cs";s:11:"Uživatelé";s:2:"da";s:7:"Brugere";s:2:"de";s:8:"Benutzer";s:2:"el";s:14:"Χρήστες";s:2:"es";s:8:"Usuarios";s:2:"fa";s:14:"کاربران";s:2:"fi";s:12:"Käyttäjät";s:2:"fr";s:12:"Utilisateurs";s:2:"he";s:14:"משתמשים";s:2:"id";s:8:"Pengguna";s:2:"it";s:6:"Utenti";s:2:"lt";s:10:"Vartotojai";s:2:"nl";s:10:"Gebruikers";s:2:"pl";s:12:"Użytkownicy";s:2:"ru";s:24:"Пользователи";s:2:"sl";s:10:"Uporabniki";s:2:"tw";s:6:"用戶";s:2:"cn";s:6:"用户";s:2:"hu";s:14:"Felhasználók";s:2:"th";s:27:"ผู้ใช้งาน";s:2:"se";s:10:"Användare";}', 'users', '1.1.0', NULL, 'a:25:{s:2:"en";s:81:"Let users register and log in to the site, and manage them via the control panel.";s:2:"ar";s:133:"تمكين المستخدمين من التسجيل والدخول إلى الموقع، وإدارتهم من لوحة التحكم.";s:2:"br";s:125:"Permite com que usuários se registrem e entrem no site e também que eles sejam gerenciáveis apartir do painel de controle.";s:2:"pt";s:125:"Permite com que os utilizadores se registem e entrem no site e também que eles sejam geriveis apartir do painel de controlo.";s:2:"cs";s:103:"Umožňuje uživatelům se registrovat a přihlašovat a zároveň jejich správu v Kontrolním panelu.";s:2:"da";s:89:"Lader brugere registrere sig og logge ind på sitet, og håndtér dem via kontrolpanelet.";s:2:"de";s:108:"Erlaube Benutzern das Registrieren und Einloggen auf der Seite und verwalte sie über die Admin-Oberfläche.";s:2:"el";s:208:"Παρέχει λειτουργίες εγγραφής και σύνδεσης στους επισκέπτες. Επίσης από εδώ γίνεται η διαχείριση των λογαριασμών.";s:2:"es";s:138:"Permite el registro de nuevos usuarios quienes podrán loguearse en el sitio. Estos podrán controlarse desde el panel de administración.";s:2:"fa";s:151:"به کاربر ها امکان ثبت نام و لاگین در سایت را بدهید و آنها را در پنل مدیریت نظارت کنید";s:2:"fi";s:126:"Antaa käyttäjien rekisteröityä ja kirjautua sisään sivustolle sekä mahdollistaa niiden muokkaamisen hallintapaneelista.";s:2:"fr";s:112:"Permet aux utilisateurs de s''enregistrer et de se connecter au site et de les gérer via le panneau de contrôle";s:2:"he";s:62:"ניהול משתמשים: רישום, הפעלה ומחיקה";s:2:"id";s:102:"Memungkinkan pengguna untuk mendaftar dan masuk ke dalam situs, dan mengaturnya melalui control panel.";s:2:"it";s:95:"Fai iscrivere de entrare nel sito gli utenti, e gestiscili attraverso il pannello di controllo.";s:2:"lt";s:106:"Leidžia vartotojams registruotis ir prisijungti prie puslapio, ir valdyti juos per administravimo panele.";s:2:"nl";s:88:"Laat gebruikers registreren en inloggen op de site, en beheer ze via het controlepaneel.";s:2:"pl";s:87:"Pozwól użytkownikom na logowanie się na stronie i zarządzaj nimi za pomocą panelu.";s:2:"ru";s:155:"Управление зарегистрированными пользователями, активирование новых пользователей.";s:2:"sl";s:96:"Dovoli uporabnikom za registracijo in prijavo na strani, urejanje le teh preko nadzorne plošče";s:2:"tw";s:87:"讓用戶可以註冊並登入網站，並且管理者可在控制台內進行管理。";s:2:"cn";s:87:"让用户可以注册并登入网站，并且管理者可在控制台内进行管理。";s:2:"th";s:210:"ให้ผู้ใช้ลงทะเบียนและเข้าสู่เว็บไซต์และจัดการกับพวกเขาผ่านทางแผงควบคุม";s:2:"hu";s:120:"Hogy a felhasználók tudjanak az oldalra regisztrálni és belépni, valamint lehessen őket kezelni a vezérlőpulton.";s:2:"se";s:111:"Låt dina besökare registrera sig och logga in på webbplatsen. Hantera sedan användarna via kontrollpanelen.";}', 0, 0, 1, '0', 1, 1, 1, 1421698961),
(35, 'a:25:{s:2:"en";s:9:"Variables";s:2:"ar";s:20:"المتغيّرات";s:2:"br";s:10:"Variáveis";s:2:"pt";s:10:"Variáveis";s:2:"cs";s:10:"Proměnné";s:2:"da";s:8:"Variable";s:2:"de";s:9:"Variablen";s:2:"el";s:20:"Μεταβλητές";s:2:"es";s:9:"Variables";s:2:"fa";s:16:"متغییرها";s:2:"fi";s:9:"Muuttujat";s:2:"fr";s:9:"Variables";s:2:"he";s:12:"משתנים";s:2:"id";s:8:"Variabel";s:2:"it";s:9:"Variabili";s:2:"lt";s:10:"Kintamieji";s:2:"nl";s:10:"Variabelen";s:2:"pl";s:7:"Zmienne";s:2:"ru";s:20:"Переменные";s:2:"sl";s:13:"Spremenljivke";s:2:"tw";s:12:"系統變數";s:2:"cn";s:12:"系统变数";s:2:"th";s:18:"ตัวแปร";s:2:"se";s:9:"Variabler";s:2:"hu";s:10:"Változók";}', 'variables', '1.0.0', NULL, 'a:25:{s:2:"en";s:59:"Manage global variables that can be accessed from anywhere.";s:2:"ar";s:97:"إدارة المُتغيّرات العامة لاستخدامها في أرجاء الموقع.";s:2:"br";s:61:"Gerencia as variáveis globais acessíveis de qualquer lugar.";s:2:"pt";s:58:"Gerir as variáveis globais acessíveis de qualquer lugar.";s:2:"cs";s:56:"Spravujte globální proměnné přístupné odkudkoliv.";s:2:"da";s:51:"Håndtér globale variable som kan tilgås overalt.";s:2:"de";s:74:"Verwaltet globale Variablen, auf die von überall zugegriffen werden kann.";s:2:"el";s:129:"Διαχείριση μεταβλητών που είναι προσβάσιμες από παντού στον ιστότοπο.";s:2:"es";s:50:"Manage global variables to access from everywhere.";s:2:"fa";s:136:"مدیریت متغییر های جامع که می توانند در هر جای سایت مورد استفاده قرار بگیرند";s:2:"fi";s:66:"Hallitse globaali muuttujia, joihin pääsee käsiksi mistä vain.";s:2:"fr";s:92:"Gérer des variables globales pour pouvoir y accéder depuis n''importe quel endroit du site.";s:2:"he";s:96:"ניהול משתנים גלובליים אשר ניתנים להמרה בכל חלקי האתר";s:2:"id";s:59:"Mengatur variabel global yang dapat diakses dari mana saja.";s:2:"it";s:58:"Gestisci le variabili globali per accedervi da ogni parte.";s:2:"lt";s:64:"Globalių kintamujų tvarkymas kurie yra pasiekiami iš bet kur.";s:2:"nl";s:54:"Beheer globale variabelen die overal beschikbaar zijn.";s:2:"pl";s:86:"Zarządzaj globalnymi zmiennymi do których masz dostęp z każdego miejsca aplikacji.";s:2:"ru";s:136:"Управление глобальными переменными, которые доступны в любом месте сайта.";s:2:"sl";s:53:"Urejanje globalnih spremenljivk za dostop od kjerkoli";s:2:"th";s:148:"จัดการตัวแปรทั่วไปโดยที่สามารถเข้าถึงได้จากทุกที่.";s:2:"tw";s:45:"管理此網站內可存取的全局變數。";s:2:"cn";s:45:"管理此网站内可存取的全局变数。";s:2:"hu";s:62:"Globális változók kezelése a hozzáféréshez, bárhonnan.";s:2:"se";s:66:"Hantera globala variabler som kan avändas över hela webbplatsen.";}', 0, 0, 1, 'data', 1, 1, 1, 1421698961),
(36, 'a:23:{s:2:"en";s:7:"Widgets";s:2:"br";s:7:"Widgets";s:2:"pt";s:7:"Widgets";s:2:"cs";s:7:"Widgety";s:2:"da";s:7:"Widgets";s:2:"de";s:7:"Widgets";s:2:"el";s:7:"Widgets";s:2:"es";s:7:"Widgets";s:2:"fa";s:13:"ویجت ها";s:2:"fi";s:9:"Vimpaimet";s:2:"fr";s:7:"Widgets";s:2:"id";s:6:"Widget";s:2:"it";s:7:"Widgets";s:2:"lt";s:11:"Papildiniai";s:2:"nl";s:7:"Widgets";s:2:"ru";s:14:"Виджеты";s:2:"sl";s:9:"Vtičniki";s:2:"tw";s:9:"小組件";s:2:"cn";s:9:"小组件";s:2:"hu";s:9:"Widget-ek";s:2:"th";s:21:"วิดเจ็ต";s:2:"se";s:8:"Widgetar";s:2:"ar";s:14:"الودجتس";}', 'widgets', '1.2.0', NULL, 'a:23:{s:2:"en";s:69:"Manage small sections of self-contained logic in blocks or "Widgets".";s:2:"ar";s:132:"إدارة أقسام صغيرة من البرمجيات في مساحات الموقع أو ما يُسمّى بالـ"ودجتس".";s:2:"br";s:77:"Gerenciar pequenas seções de conteúdos em bloco conhecidos como "Widgets".";s:2:"pt";s:74:"Gerir pequenas secções de conteúdos em bloco conhecidos como "Widgets".";s:2:"cs";s:56:"Spravujte malé funkční části webu neboli "Widgety".";s:2:"da";s:74:"Håndter små sektioner af selv-opretholdt logik i blokke eller "Widgets".";s:2:"de";s:62:"Verwaltet kleine, eigentständige Bereiche, genannt "Widgets".";s:2:"el";s:149:"Διαχείριση μικρών τμημάτων αυτόνομης προγραμματιστικής λογικής σε πεδία ή "Widgets".";s:2:"es";s:75:"Manejar pequeñas secciones de lógica autocontenida en bloques o "Widgets"";s:2:"fa";s:76:"مدیریت قسمت های کوچکی از سایت به طور مستقل";s:2:"fi";s:81:"Hallitse pieniä osioita, jotka sisältävät erillisiä lohkoja tai "Vimpaimia".";s:2:"fr";s:41:"Gérer des mini application ou "Widgets".";s:2:"id";s:101:"Mengatur bagian-bagian kecil dari blok-blok yang memuat sesuatu atau dikenal dengan istilah "Widget".";s:2:"it";s:70:"Gestisci piccole sezioni di logica a se stante in blocchi o "Widgets".";s:2:"lt";s:43:"Nedidelių, savarankiškų blokų valdymas.";s:2:"nl";s:75:"Beheer kleine onderdelen die zelfstandige logica bevatten, ofwel "Widgets".";s:2:"ru";s:91:"Управление небольшими, самостоятельными блоками.";s:2:"sl";s:61:"Urejanje manjših delov blokov strani ti. Vtičniki (Widgets)";s:2:"tw";s:103:"可將小段的程式碼透過小組件來管理。即所謂 "Widgets"，或稱為小工具、部件。";s:2:"cn";s:103:"可将小段的程式码透过小组件来管理。即所谓 "Widgets"，或称为小工具、部件。";s:2:"hu";s:56:"Önálló kis logikai tömbök vagy widget-ek kezelése.";s:2:"th";s:152:"จัดการส่วนเล็ก ๆ ในรูปแบบของตัวเองในบล็อกหรือวิดเจ็ต";s:2:"se";s:83:"Hantera små sektioner med egen logik och innehåll på olika delar av webbplatsen.";}', 1, 0, 1, 'content', 1, 1, 1, 1421698961),
(37, 'a:9:{s:2:"en";s:7:"WYSIWYG";s:2:"fa";s:7:"WYSIWYG";s:2:"fr";s:7:"WYSIWYG";s:2:"pt";s:7:"WYSIWYG";s:2:"se";s:15:"HTML-redigerare";s:2:"tw";s:7:"WYSIWYG";s:2:"cn";s:7:"WYSIWYG";s:2:"ar";s:27:"المحرر الرسومي";s:2:"it";s:7:"WYSIWYG";}', 'wysiwyg', '1.0.0', NULL, 'a:10:{s:2:"en";s:60:"Provides the WYSIWYG editor for PyroCMS powered by CKEditor.";s:2:"fa";s:73:"ویرایشگر WYSIWYG که توسطCKEditor ارائه شده است. ";s:2:"fr";s:63:"Fournit un éditeur WYSIWYG pour PyroCMS propulsé par CKEditor";s:2:"pt";s:61:"Fornece o editor WYSIWYG para o PyroCMS, powered by CKEditor.";s:2:"el";s:113:"Παρέχει τον επεξεργαστή WYSIWYG για το PyroCMS, χρησιμοποιεί το CKEDitor.";s:2:"se";s:37:"Redigeringsmodul för HTML, CKEditor.";s:2:"tw";s:83:"提供 PyroCMS 所見即所得（WYSIWYG）編輯器，由 CKEditor 技術提供。";s:2:"cn";s:83:"提供 PyroCMS 所见即所得（WYSIWYG）编辑器，由 CKEditor 技术提供。";s:2:"ar";s:76:"توفر المُحرّر الرسومي لـPyroCMS من خلال CKEditor.";s:2:"it";s:57:"Fornisce l''editor WYSIWYG per PtroCMS creato con CKEditor";}', 0, 0, 0, '0', 1, 1, 1, 1421698961),
(38, 'a:1:{s:2:"en";s:14:"FireSale Theme";}', 'firesale_theme', '1.0.0', NULL, 'a:1:{s:2:"en";s:61:"Adds site colors from theme settings if present to your site.";}', 1, 0, 0, '0', 1, 1, 1, 1410475421),
(39, 'a:4:{s:2:"en";s:14:"API Management";s:2:"el";s:24:"Διαχείριση API";s:2:"fr";s:18:"Gestionnaire d''API";s:2:"hu";s:12:"API Kezelés";}', 'api', '1.0.0', NULL, 'a:4:{s:2:"en";s:66:"Set up a RESTful API with API Keys and out in JSON, XML, CSV, etc.";s:2:"el";s:129:"Ρυθμίσεις για ένα RESTful API με κλειδιά API και αποτελέσματα σε JSON, XML, CSV, κτλ.";s:2:"fr";s:79:"Paramétrage d''une API RESTgul avec clés API et export en JSON, XML, CSV, etc.";s:2:"hu";s:159:"Körültekintően állítsd be az API-t (alkalmazásprogramozási felület) az API Kulcsokkal együtt és küldd JSON-ba, XML-be, CSV-be, vagy bármi egyébbe.";}', 0, 1, 1, 'utilities', 0, 0, 0, 1421698961),
(52, 'a:2:{s:2:"es";s:4:"Home";s:2:"en";s:4:"Home";}', 'home', '1.0', NULL, 'a:2:{s:2:"es";s:31:"Home Basico de la Tienda Online";s:2:"en";s:10:"Basic Home";}', 0, 1, 1, 'content', 1, 1, 0, 1421698961),
(62, 'a:2:{s:2:"es";s:13:"Quienes Somos";s:2:"en";s:0:"";}', 'quienes_somos', '1.1', NULL, 'a:2:{s:2:"es";s:43:"Página de contenido; texto, imagen y video";s:2:"en";s:0:"";}', 0, 1, 1, 'content', 0, 0, 0, 1421698961),
(48, 'a:1:{s:2:"en";s:5:"Olark";}', 'olark', '1.1.0', NULL, 'a:1:{s:2:"en";s:123:"Olark Live Chat. Signup for a free account at <a target="_blank" href="https://www.olark.com/?r=4eqdhi13">www.olark.com</a>";}', 0, 0, 0, '0', 0, 0, 0, 1392068168),
(64, 'a:2:{s:2:"es";s:17:"Datos de Contacto";s:2:"en";s:13:"Contacts Data";}', 'contact_us', '1.0', NULL, 'a:2:{s:2:"es";s:89:"Modulo información de contacto © Brayan Acebo, Luis Fernando Salazar, Christian España";s:2:"en";s:75:"Manage Info Data © Christian España , Brayan Acebo, Luis fernando Salazar";}', 0, 1, 1, 'content', 1, 1, 0, 1421698961),
(55, 'a:2:{s:2:"es";s:21:"Otras configuraciones";s:2:"en";s:20:"Other configurations";}', 'others_conf', '1.2', NULL, 'a:2:{s:2:"es";s:58:"Otras configuraciones como el logo. @Luis Fernando Salazar";s:2:"en";s:52:"Other configuration like logo @Luis Fernando Salazar";}', 0, 1, 1, 'content', 1, 1, 0, 1421698961),
(59, 'a:2:{s:2:"es";s:14:"Sobre Nosotros";s:2:"en";s:8:"About Us";}', 'about_us', '1.3', NULL, 'a:2:{s:2:"es";s:30:"Sobre Nosotros © Brayan Acebo";s:2:"en";s:24:"About Us © Brayan Acebo";}', 0, 1, 1, 'content', 1, 1, 0, 1421698961),
(63, 'a:2:{s:2:"en";s:4:"Chat";s:2:"es";s:12:"Chat Interno";}', 'chat', '1.0', NULL, 'a:2:{s:2:"en";s:30:"This is a PyroCMS module chat.";s:2:"es";s:32:"Administrador de chat principal.";}', 0, 1, 1, 'content', 1, 1, 0, 1421698961),
(65, 'a:2:{s:2:"en";s:11:"Google Maps";s:2:"es";s:15:"Mapas de Google";}', 'google_maps', '1.2', NULL, 'a:1:{s:2:"en";s:45:"Mapas de Google © Luis Fernando Salazar 2014";}', 0, 0, 1, 'content', 1, 1, 0, 1421698961),
(66, 'a:2:{s:2:"en";s:18:"about us multiples";s:2:"es";s:23:"Sobre Nosotros Multiple";}', 'about_us_multiples', '1.2', NULL, 'a:2:{s:2:"en";s:58:"This is a module of About Us © Brayan Acebo, Luis Salazar";s:2:"es";s:54:"Modulo de Sobre Nosotros © Brayan Acebo, Luis Salazar";}', 0, 1, 1, 'content', 1, 1, 0, 1421698961),
(68, 'a:2:{s:2:"en";s:18:"News with comments";s:2:"es";s:24:"Noticias con comentarios";}', 'news', '1.2', NULL, 'a:2:{s:2:"en";s:54:"This is a module of news © Brayan Acebo, Luis Salazar";s:2:"es";s:48:"Modulo de Noticias © Brayan Acebo, Luis Salazar";}', 0, 1, 1, 'content', 1, 1, 0, 1421698961),
(69, 'a:2:{s:2:"en";s:12:"Advance News";s:2:"es";s:18:"Noticias Avanzadas";}', 'advance_news', '1.2', NULL, 'a:2:{s:2:"en";s:62:"This is a module of advance_news © Brayan Acebo, Luis Salazar";s:2:"es";s:48:"Modulo de Noticias © Brayan Acebo, Luis Salazar";}', 0, 1, 1, 'content', 1, 1, 0, 1421698961),
(72, 'a:2:{s:2:"es";s:10:"newsletter";s:2:"en";s:10:"newsletter";}', 'newsletter', '1.2', NULL, 'a:2:{s:2:"es";s:33:"newsletter @Luis Fernando Salazar";s:2:"en";s:33:"newsletter @Luis Fernando Salazar";}', 0, 1, 1, 'content', 1, 1, 0, 1421698961),
(73, 'a:2:{s:2:"es";s:8:"Reportes";s:2:"en";s:7:"Reports";}', 'reports', '1.2', NULL, 'a:2:{s:2:"es";s:42:"Modulo de reportes. @Luis Fernando Salazar";s:2:"en";s:37:"Reports module @Luis Fernando Salazar";}', 0, 0, 1, 'content', 1, 1, 0, 1421698992);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_navigation_groups`
--

CREATE TABLE IF NOT EXISTS `default_navigation_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `abbrev` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `abbrev` (`abbrev`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `default_navigation_groups`
--

INSERT INTO `default_navigation_groups` (`id`, `title`, `abbrev`) VALUES
(1, 'Header', 'header'),
(2, 'Sidebar', 'sidebar'),
(3, 'Footer', 'footer');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_navigation_links`
--

CREATE TABLE IF NOT EXISTS `default_navigation_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent` int(11) DEFAULT NULL,
  `link_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uri',
  `page_id` int(11) DEFAULT NULL,
  `module_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `navigation_group_id` int(5) NOT NULL DEFAULT '0',
  `position` int(5) NOT NULL DEFAULT '0',
  `target` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `restricted_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `navigation_group_id` (`navigation_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `default_navigation_links`
--

INSERT INTO `default_navigation_links` (`id`, `title`, `parent`, `link_type`, `page_id`, `module_name`, `url`, `uri`, `navigation_group_id`, `position`, `target`, `restricted_to`, `class`) VALUES
(3, 'Contacto', 0, 'module', 0, 'contact_us', '', '', 1, 5, '', '0', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_news`
--

CREATE TABLE IF NOT EXISTS `default_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci,
  `slug` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `introduction` text COLLATE utf8_unicode_ci,
  `date` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `outstanding` int(1) DEFAULT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'es',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `default_news`
--

INSERT INTO `default_news` (`id`, `title`, `slug`, `autor`, `image`, `content`, `introduction`, `date`, `position`, `outstanding`, `lang`) VALUES
(2, 'Noticias 1', 'noticias-1', 'Imaginamos', NULL, 'Morbi quis tincidunt nulla, ac molestie libero. Integer nec pretium nisl. Morbi blandit eu massa sit amet aliquet. Mauris rutrum sodales odio, vel molestie risus pharetra in. Etiam blandit nulla ut blandit ultrices. Aliquam sit amet nisi sed nisl accumsan vestibulum. Curabitur eu lobortis ex. Mauris non lectus vehicula, posuere tortor a, bibendum tellus. Curabitur condimentum tellus sit amet est laoreet, sed lacinia mi accumsan. Praesent id odio eleifend, tempus nulla nec, tempus lectus. Mauris bibendum ac turpis eu mollis. Nullam tincidunt fermentum elementum. Aenean faucibus eros a bibendum lacinia. Maecenas lectus ipsum, volutpat at elit id, egestas imperdiet massa. Ut finibus massa vel eros rhoncus, nec placerat tellus sollicitudin. Vivamus felis augue, aliquet id imperdiet a, commodo ut mauris.', 'Morbi quis tincidunt nulla, ac molestie libero. Integer nec pretium nisl. Morbi blandit eu massa sit amet aliquet.', '2015-01-18 23:56:27', 1, NULL, 'es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_newsletter`
--

CREATE TABLE IF NOT EXISTS `default_newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `default_newsletter`
--

INSERT INTO `default_newsletter` (`id`, `email`) VALUES
(1, 'weweqwewq'),
(2, 'jorge.clavijo@imaginamos.com'),
(3, 'jorge.clavijo@hotmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_newsletter_info`
--

CREATE TABLE IF NOT EXISTS `default_newsletter_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `terms_cond` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'es',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_newsletter_info`
--

INSERT INTO `default_newsletter_info` (`id`, `title`, `text`, `terms_cond`, `link`, `lang`) VALUES
(1, 'NEWSLETTER', 'Ingresa tu Correo Electrónico y Recibe descuentos, promociones exclusivas y se el primero en descubrir nuestras nuevas ofertas1', 'uploads/default/newsletter_info/490f71a874215f86bc773900a1c4fb16.pdf', '', 'es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_news_comments`
--

CREATE TABLE IF NOT EXISTS `default_news_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_new` int(11) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_others_conf`
--

CREATE TABLE IF NOT EXISTS `default_others_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_total` int(10) DEFAULT NULL,
  `terms_cond` varchar(455) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_others_conf`
--

INSERT INTO `default_others_conf` (`id`, `logo`, `min_total`, `terms_cond`) VALUES
(1, 'uploads/default/others_conf/219992631657925a68601e5472c8d289.png', 0, 'uploads/default/others_conf/5f0ed44ff59ad8baba3150d3b55d75f5.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_pages`
--

CREATE TABLE IF NOT EXISTS `default_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `uri` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `css` text COLLATE utf8_unicode_ci,
  `js` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_robots_no_index` tinyint(1) DEFAULT NULL,
  `meta_robots_no_follow` tinyint(1) DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `rss_enabled` int(1) NOT NULL DEFAULT '0',
  `comments_enabled` int(1) NOT NULL DEFAULT '0',
  `status` enum('draft','live') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `created_on` int(11) NOT NULL DEFAULT '0',
  `updated_on` int(11) NOT NULL DEFAULT '0',
  `restricted_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_home` int(1) NOT NULL DEFAULT '0',
  `strict_uri` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `default_pages`
--

INSERT INTO `default_pages` (`id`, `slug`, `class`, `title`, `uri`, `parent_id`, `type_id`, `entry_id`, `css`, `js`, `meta_title`, `meta_keywords`, `meta_robots_no_index`, `meta_robots_no_follow`, `meta_description`, `rss_enabled`, `comments_enabled`, `status`, `created_on`, `updated_on`, `restricted_to`, `is_home`, `strict_uri`, `order`) VALUES
(1, 'home', '', 'Home', 'home', 0, '1', '1', '', '', '', '', 0, 0, '', 0, 0, 'live', 1373940180, 1374190084, '0', 1, 1, 0),
(2, 'contacto', '', 'Contacto', 'contacto', 0, '1', '2', '', '', '', '', 0, 0, '', 0, 0, 'live', 1373940180, 1391114717, '0', 0, 1, 2),
(5, '404', '', 'Página Desaparecida', '404', 0, '1', '5', '', '', '', '', 0, 0, '', 0, 0, 'live', 1373940180, 1391786407, '0', 0, 1, 4),
(6, 'lo-mas-nuevo', '', 'Lo Más Nuevo', 'lo-mas-nuevo', 0, '1', '6', '', '', '', '', 0, 0, '', 0, 0, 'live', 1416771530, 1416772050, '0', 0, 1, 1416771530);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_page_types`
--

CREATE TABLE IF NOT EXISTS `default_page_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `stream_id` int(11) NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `css` text COLLATE utf8_unicode_ci,
  `js` text COLLATE utf8_unicode_ci,
  `theme_layout` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `updated_on` int(11) NOT NULL,
  `save_as_files` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `content_label` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_label` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_page_types`
--

INSERT INTO `default_page_types` (`id`, `slug`, `title`, `description`, `stream_id`, `meta_title`, `meta_keywords`, `meta_description`, `body`, `css`, `js`, `theme_layout`, `updated_on`, `save_as_files`, `content_label`, `title_label`) VALUES
(1, 'default', 'Default', 'A simple page type with a WYSIWYG editor that will get you started adding content.', 19, NULL, NULL, NULL, '<h2>{{ page:title }}</h2>\n\n{{ body }}', '', '', 'default', 1391099080, 'n', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_permissions`
--

CREATE TABLE IF NOT EXISTS `default_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roles` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=99 ;

--
-- Volcado de datos para la tabla `default_permissions`
--

INSERT INTO `default_permissions` (`id`, `group_id`, `module`, `roles`) VALUES
(41, 4, 'addons', NULL),
(42, 4, 'files', '{"wysiwyg":"1","upload":"1","download_file":"1","edit_file":"1","delete_file":"1","create_folder":"1","set_location":"1","synchronize":"1","edit_folder":"1","delete_folder":"1"}'),
(43, 4, 'blog', '{"put_live":"1","edit_live":"1","delete_live":"1"}'),
(44, 4, 'firesale_brands', NULL),
(45, 4, 'comments', NULL),
(46, 4, 'settings', NULL),
(47, 4, 'firesale_discount_codes', NULL),
(48, 4, 'domains', NULL),
(49, 4, 'firesale_reviews', NULL),
(50, 4, 'firesale_streams', NULL),
(51, 4, 'firesale_wishlist', NULL),
(52, 4, 'groups', NULL),
(53, 4, 'home', NULL),
(54, 4, 'keywords', NULL),
(55, 4, 'maintenance', NULL),
(56, 4, 'navigation', NULL),
(57, 4, 'templates', NULL),
(58, 4, 'pages', '{"put_live":"1","edit_live":"1","delete_live":"1","create_types":"1","edit_types":"1","delete_types":"1"}'),
(59, 4, 'quienes_somos', NULL),
(60, 4, 'redirects', NULL),
(61, 4, 'firesale_shipping', NULL),
(62, 4, 'firesale', '{"edit_orders":"1","access_routes":"1","create_edit_routes":"1","access_gateways":"1","install_uninstall_gateways":"1","enable_disable_gateways":"1","edit_gateways":"1","access_currency":"1","install_uninstall_currency":"1","access_taxes":"1","add_edit_taxes":"1"}'),
(63, 4, 'streams', '{"admin_streams":"1","admin_fields":"1"}'),
(64, 4, 'users', '{"admin_profile_fields":"1"}'),
(65, 4, 'variables', NULL),
(66, 4, 'widgets', NULL),
(84, 3, 'files', '{"wysiwyg":"1","upload":"1","download_file":"1","edit_file":"1","delete_file":"1","create_folder":"1","set_location":"1","synchronize":"1","edit_folder":"1","delete_folder":"1"}'),
(85, 3, 'blog', '{"put_live":"1","edit_live":"1","delete_live":"1"}'),
(86, 3, 'firesale_brands', NULL),
(87, 3, 'comments', NULL),
(88, 3, 'contact_us', NULL),
(89, 3, 'firesale_discount_codes', NULL),
(90, 3, 'firesale_reviews', NULL),
(91, 3, 'firesale_wishlist', NULL),
(92, 3, 'home', NULL),
(93, 3, 'others_conf', NULL),
(94, 3, 'templates', NULL),
(95, 3, 'firesale_shipping', NULL),
(96, 3, 'about_us', NULL),
(97, 3, 'firesale', '{"edit_orders":"1"}'),
(98, 3, 'users', '{"admin_profile_fields":"1"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_profiles`
--

CREATE TABLE IF NOT EXISTS `default_profiles` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `display_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `dob` int(11) DEFAULT NULL,
  `gender` set('m','f','') COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `default_profiles`
--

INSERT INTO `default_profiles` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `user_id`, `display_name`, `first_name`, `last_name`, `lang`, `dob`, `gender`, `phone`, `website`, `updated_on`) VALUES
(10, '2014-03-11 11:46:40', NULL, 1, 9, 10, 'Luis Salazar', 'Luis', 'Salazar', 'es', -90000, '', '77777', NULL, 1421346415),
(13, '2014-05-09 16:25:02', NULL, 10, 12, 13, 'demo', 'demo', 'imaginamos', 'es', -3600, '', NULL, NULL, 1401108537),
(20, '2015-04-13 22:34:41', NULL, NULL, 18, 20, 'jorge.clavijo', 'Jorge', 'Clavijo', 'en', NULL, NULL, NULL, NULL, NULL),
(17, '2015-04-08 20:14:20', NULL, NULL, 16, 17, 'maria.pedroza', 'Maria', 'Pedroza', 'en', NULL, NULL, NULL, NULL, NULL),
(18, '2015-04-09 22:29:34', NULL, NULL, 17, 18, 'liliana_.artunduaga', 'LILIANA', 'ARTUNDUAGA', 'en', NULL, NULL, NULL, NULL, NULL),
(21, '2015-04-15 00:20:53', NULL, NULL, 19, 21, 'fabian.andres', 'Fabian', 'Andres', 'es', -68400, '', NULL, NULL, 1429124563),
(23, '2015-04-15 10:56:25', NULL, NULL, 20, 23, 'luis.salazar', 'luis', 'salazar', 'en', NULL, NULL, NULL, NULL, NULL),
(24, '2015-04-20 13:14:21', NULL, NULL, 21, 24, 'maria_.pedroza', 'maria', 'pedroza', 'en', NULL, NULL, NULL, NULL, NULL),
(25, '2015-04-20 13:23:11', NULL, NULL, 22, 25, 'leyner.vente', 'leyner', 'venté', 'en', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_redirects`
--

CREATE TABLE IF NOT EXISTS `default_redirects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `to` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(3) NOT NULL DEFAULT '302',
  PRIMARY KEY (`id`),
  KEY `from` (`from`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_redirects`
--

INSERT INTO `default_redirects` (`id`, `from`, `to`, `type`) VALUES
(1, 'product/', 'product/modificadores', 301);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_rg_widgets`
--

CREATE TABLE IF NOT EXISTS `default_rg_widgets` (
  `idwidge` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `codwidge` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomwidge` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nummodulo` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `undsancho` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `tipografico` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `tipotabla` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `minrefresco` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `consultasql` text COLLATE utf8_unicode_ci,
  `tituloejex` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tituloejey` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`idwidge`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `default_rg_widgets`
--

INSERT INTO `default_rg_widgets` (`idwidge`, `codwidge`, `nomwidge`, `nummodulo`, `undsancho`, `tipografico`, `tipotabla`, `minrefresco`, `consultasql`, `tituloejex`, `tituloejey`, `descripcion`) VALUES
(1, '1', 'Ventas por mes', 1, 3, 1, 1, 3, 'SELECT 0 AS ordenserie, '''' AS nomserie, MONTH(created) AS codcategoria, MONTHNAME(created) AS nomcategoria, SUM(price_total) AS valor FROM default_firesale_orders GROUP BY codcategoria', 'Mes', 'Valor', 'Ventas mes a mes de la tienda'),
(2, '2', 'Ventas por año', 1, 3, 4, 2, 3, 'SELECT 1 AS ordenserie, YEAR(created) AS nomserie, MONTH(created) AS codcategoria, MONTHNAME(created) AS nomcategoria, SUM(price_total) AS valor FROM default_firesale_orders WHERE YEAR(created) = 2014 GROUP BY ordenserie, codcategoria\nUNION\nSELECT 2 AS ordenserie, YEAR(created) AS nomserie, MONTH(created) AS codcategoria, MONTHNAME(created) AS nomcategoria, SUM(price_total) AS valor FROM default_firesale_orders WHERE YEAR(created) = 2015 GROUP BY ordenserie, codcategoria', 'Meses', 'Ventas', 'Ventas por mes 2014 y 2015');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_rg_widgets_usuarios`
--

CREATE TABLE IF NOT EXISTS `default_rg_widgets_usuarios` (
  `idwiusu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idusuar` int(10) unsigned NOT NULL DEFAULT '0',
  `idwidge` smallint(4) unsigned NOT NULL DEFAULT '0',
  `posicion` smallint(4) unsigned NOT NULL DEFAULT '0',
  `undsancho` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `tipografico` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `var1` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `var2` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `divisor` int(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idwiusu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_search_index`
--

CREATE TABLE IF NOT EXISTS `default_search_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `keywords` text COLLATE utf8_unicode_ci,
  `keyword_hash` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_key` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_plural` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_edit_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_delete_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`module`,`entry_key`,`entry_id`(190)),
  FULLTEXT KEY `full search` (`title`,`description`,`keywords`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=811 ;

--
-- Volcado de datos para la tabla `default_search_index`
--

INSERT INTO `default_search_index` (`id`, `title`, `description`, `keywords`, `keyword_hash`, `module`, `entry_key`, `entry_plural`, `entry_id`, `uri`, `cp_edit_uri`, `cp_delete_uri`) VALUES
(3, 'Search', '', NULL, NULL, 'pages', 'pages:page', 'pages:pages', '3', 'search', 'admin/pages/edit/3', 'admin/pages/delete/3'),
(4, 'Results', '', NULL, NULL, 'pages', 'pages:page', 'pages:pages', '4', 'search/results', 'admin/pages/edit/4', 'admin/pages/delete/4'),
(552, 'Adidas', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '5', 'store/bebes/adidas.bebes', 'admin/firesale/categories#5', 'admin/firesale/categories/delete/5'),
(10, 'Página Desaparecida', '', NULL, NULL, 'pages', 'pages:page', 'pages:pages', '5', '404', 'admin/pages/edit/5', 'admin/pages/delete/5'),
(240, 'Mujer', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '1', 'store/mujer', 'admin/firesale/categories#1', 'admin/firesale/categories/delete/1'),
(628, 'Hombre', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '3', 'store/hombre', 'admin/firesale/categories#3', 'admin/firesale/categories/delete/3'),
(553, 'Bebes', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '2', 'store/bebes', 'admin/firesale/categories#2', 'admin/firesale/categories/delete/2'),
(243, 'Hogar', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '12', 'store/hogar', 'admin/firesale/categories#12', 'admin/firesale/categories/delete/12'),
(169, 'asdf', 'asdfasdf', NULL, NULL, 'blog', 'blog:post', 'blog:posts', '1', 'blog/2014/02/asdfadsf', 'admin/blog/edit/1', 'admin/blog/delete/1'),
(222, 'Niños', 'Our handpicked cosmetics just for you!', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '4', 'store/ninos', 'admin/firesale/categories#4', 'admin/firesale/categories/delete/4'),
(136, 'Nude Court Shoe - 9', 'A must have for any wardrobe, these nude courts will go with any outfit, night or day. For fairer skinned ladies the combination of a flesh tone and a higher heel will give the illusion of long pretty pins, whilst those with a deeper skin tone will enjoy an interesting, contrasting look!', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '20', 'product/nude-court-shoe', 'admin/firesale/products/edit/20', 'admin/firesale/products/delete/20'),
(426, 'CAMISETA VISITANTE  2013  - M', 'Camiseta visitante 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '363', 'product/', 'admin/firesale/products/edit/363', 'admin/firesale/products/delete/363'),
(176, 'Running Nike Dart 10 msl Negro-Rojo - Rojo', 'Running Nike Dart 10 msl color negro/rojo,  Elaborado en textil. Su diseño cuenta con aplique contramarcado de alto contraste en los laterales y refuerzo en material sintético para mejor ajuste al pie. Posee capellada tipo malla para mayor frescura. Tiene punta redonda, empeine acordonado, collarín moldeado, forro en textil,  suelareslon en Eva y caucho para una excelente amortiguación.\r\n\r\n \r\n\r\nINFORMACIÓN\r\n\r\n \r\n  \r\n   Sexo\r\n   Masculino\r\n  \r\n  \r\n   Material exterior\r\n   Sintético\r\n  \r\n  \r\n   Material interior\r\n   POLYESTER\r\n  \r\n  \r\n   Color\r\n   negro/rojo\r\n  \r\n  \r\n   Descripción corta\r\n   Running Nike Dart 10 msl color negro/rojo  Sólo en Dafiti, tu tienda de moda online.\r\n  \r\n  \r\n   Actividad\r\n   training\r\n  \r\n  \r\n   Código Artículo\r\n   NI010SH82BJDCO\r\n  \r\n \r\n\r\n\r\n', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '94', 'product/ballet-pumps', 'admin/firesale/products/edit/94', 'admin/firesale/products/delete/94'),
(244, 'Adidas', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '6', 'store/mujer/adidas', 'admin/firesale/categories#6', 'admin/firesale/categories/delete/6'),
(245, 'MFC', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '7', 'store/mujer/mfc', 'admin/firesale/categories#7', 'admin/firesale/categories/delete/7'),
(246, 'Adidas', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '10', 'store/hombre/adidas-hombres', 'admin/firesale/categories#10', 'admin/firesale/categories/delete/10'),
(248, 'Adidas', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '11', 'store/ninos/adidas-ninos', 'admin/firesale/categories#11', 'admin/firesale/categories/delete/11'),
(258, 'MFC', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '13', 'store/hombre/mfc-hombre', 'admin/firesale/categories#13', 'admin/firesale/categories/delete/13'),
(249, 'MFC', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '14', 'store/ninos/mfc-ninos', 'admin/firesale/categories#14', 'admin/firesale/categories/delete/14'),
(252, 'MFC', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '16', 'store/hogar/mfc-hogar', 'admin/firesale/categories#16', 'admin/firesale/categories/delete/16'),
(607, 'ESQUELETO ESTRELLA GRIS', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '568', 'product/esqueleto-estrella-gris', 'admin/firesale/products/edit/568', 'admin/firesale/products/delete/568'),
(427, 'CAMISETA VISITANTE  2013  - L', 'Camiseta visitante 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '364', 'product/', 'admin/firesale/products/edit/364', 'admin/firesale/products/delete/364'),
(272, 'CAMISETA OFICIAL 2013 - M', 'Camiseta Oficial 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '367', 'product/', 'admin/firesale/products/edit/367', 'admin/firesale/products/delete/367'),
(273, 'CAMISETA OFICIAL 2013 - L', 'Camiseta Oficial 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '368', 'product/', 'admin/firesale/products/edit/368', 'admin/firesale/products/delete/368'),
(274, 'CAMISETA OFICIAL 2013 - XL', 'Camiseta Oficial 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '369', 'product/', 'admin/firesale/products/edit/369', 'admin/firesale/products/delete/369'),
(760, 'KIT CUADERNO OFICIO Y CARTUCHERA MFC', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '743', 'product/kit-cuaderno-oficio-y-cartuchera-mfc', 'admin/firesale/products/edit/743', 'admin/firesale/products/delete/743'),
(551, 'bebes', '', NULL, NULL, 'firesale', 'firesale:category', 'firesale:categories', '17', 'store/bebes/bebes', 'admin/firesale/categories#17', 'admin/firesale/categories/delete/17'),
(791, 'RELOJ EDICION LIMITADA MILLONARIOS-BOMBERG', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '744', 'product/reloj-edicion-limitada-millonariosbomberg', 'admin/firesale/products/edit/744', 'admin/firesale/products/delete/744'),
(277, 'SUDADERA DE PRESENTACION 2014 - M', 'Sudadera Impermeable Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '374', 'product/', 'admin/firesale/products/edit/374', 'admin/firesale/products/delete/374'),
(278, 'SUDADERA DE ENTRENAMIENTO 2014 - M', 'Sudadera De entrenamiento Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '376', 'product/', 'admin/firesale/products/edit/376', 'admin/firesale/products/delete/376'),
(753, 'BUSO TEJIDO MFC', 'Buso Tejido Cuello V Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '523', 'product/buso-shetland', 'admin/firesale/products/edit/523', 'admin/firesale/products/delete/523'),
(280, 'SUDADERA DE ENTRENAMIENTO 2014 - L', 'Sudadera De entrenamiento Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '377', 'product/', 'admin/firesale/products/edit/377', 'admin/firesale/products/delete/377'),
(281, 'SUDADERA DE ENTRENAMIENTO 2014 - XL', 'Sudadera De entrenamiento Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '378', 'product/', 'admin/firesale/products/edit/378', 'admin/firesale/products/delete/378'),
(283, 'BUSO SHETLAND  - S', 'Buso Tejido Cuello V Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '379', 'product/', 'admin/firesale/products/edit/379', 'admin/firesale/products/delete/379'),
(284, 'BUSO SHETLAND  - XL', 'Buso Tejido Cuello V Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '382', 'product/', 'admin/firesale/products/edit/382', 'admin/firesale/products/delete/382'),
(285, 'BUSO SHETLAND  - M', 'Buso Tejido Cuello V Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '380', 'product/', 'admin/firesale/products/edit/380', 'admin/firesale/products/delete/380'),
(286, 'BUSO SHETLAND  - L', 'Buso Tejido Cuello V Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '381', 'product/', 'admin/firesale/products/edit/381', 'admin/firesale/products/delete/381'),
(442, 'CAMISETA VISITANTE 2015 MANGA CORTA  - S', 'Camiseta Visitante 2015 manga Corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '383', 'product/', 'admin/firesale/products/edit/383', 'admin/firesale/products/delete/383'),
(443, 'CAMISETA VISITANTE 2015 MANGA CORTA  - M', 'Camiseta Visitante 2015 manga Corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '384', 'product/', 'admin/firesale/products/edit/384', 'admin/firesale/products/delete/384'),
(289, 'CAMISETA VISITANTE 2015 MANGA CORTA  - XL', 'Camiseta Visitante 2015 manga Corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '386', 'product/', 'admin/firesale/products/edit/386', 'admin/firesale/products/delete/386'),
(290, 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - L', 'Camiseta Visitante 2015 Manga Larga Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '390', 'product/', 'admin/firesale/products/edit/390', 'admin/firesale/products/delete/390'),
(291, 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - XL', 'Camiseta Visitante 2015 Manga Larga Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '391', 'product/', 'admin/firesale/products/edit/391', 'admin/firesale/products/delete/391'),
(292, 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - M', 'Camiseta Visitante 2015 Manga Larga Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '389', 'product/', 'admin/firesale/products/edit/389', 'admin/firesale/products/delete/389'),
(638, 'SUDADERA DE ENTRENAMIENTO 2014', 'Sudadera De entrenamiento Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '521', 'product/sudadera-de-entrenamiento-2014', 'admin/firesale/products/edit/521', 'admin/firesale/products/delete/521'),
(444, 'CAMISETA VISITANTE 2015 MANGA CORTA  - L', 'Camiseta Visitante 2015 manga Corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '392', 'product/', 'admin/firesale/products/edit/392', 'admin/firesale/products/delete/392'),
(445, 'CAMISETA VISITANTE 2015 MANGA CORTA  - XL', 'Camiseta Visitante 2015 manga Corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '393', 'product/', 'admin/firesale/products/edit/393', 'admin/firesale/products/delete/393'),
(297, 'BUSO SHOP  - S', 'Sweter Estampado Con Mangas en Pu Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '394', 'product/', 'admin/firesale/products/edit/394', 'admin/firesale/products/delete/394'),
(437, 'BUSO SHOP  - M', 'Sweter Estampado Con Mangas en Pu Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '396', 'product/', 'admin/firesale/products/edit/396', 'admin/firesale/products/delete/396'),
(299, 'BUSO SHOP  - L', 'Sweter Estampado Con Mangas en Pu Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '397', 'product/', 'admin/firesale/products/edit/397', 'admin/firesale/products/delete/397'),
(440, 'T SHIRT OVER CON ESTAMPADO CAMPIN - M', 'Camiseta Manga Larga Gris Talla 4', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '399', 'product/', 'admin/firesale/products/edit/399', 'admin/firesale/products/delete/399'),
(441, 'T SHIRT OVER CON ESTAMPADO CAMPIN - L', 'Camiseta Manga Larga Gris Talla 4', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '400', 'product/', 'admin/firesale/products/edit/400', 'admin/firesale/products/delete/400'),
(302, 'CAMISETA PASION DE MILLONES  - S', 'Camiseta Pasion De Millones Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '401', 'product/', 'admin/firesale/products/edit/401', 'admin/firesale/products/delete/401'),
(303, 'CAMISETA PASION DE MILLONES  - M', 'Camiseta Pasion De Millones Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '402', 'product/', 'admin/firesale/products/edit/402', 'admin/firesale/products/delete/402'),
(304, 'CAMISETA PASION DE MILLONES  - L', 'Camiseta Pasion De Millones Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '403', 'product/', 'admin/firesale/products/edit/403', 'admin/firesale/products/delete/403'),
(642, 'PANTALONETA OFICIAL 2015', 'Pantaloneta oficial 2105', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '554', 'product/pantaloneta-oficial-2015', 'admin/firesale/products/edit/554', 'admin/firesale/products/delete/554'),
(639, 'SUDADERA DE PRESENTACION 2014', 'Sudadera Impermeable Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '520', 'product/sudadera-de-presentacion-2014', 'admin/firesale/products/edit/520', 'admin/firesale/products/delete/520'),
(306, 'CAMISETA OFICIAL 2015 MANGA CORTA  - S', 'Camiseta oficial 2015 manga corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '404', 'product/', 'admin/firesale/products/edit/404', 'admin/firesale/products/delete/404'),
(307, 'CAMISETA OFICIAL 2015 MANGA CORTA  - M', 'Camiseta oficial 2015 manga corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '406', 'product/', 'admin/firesale/products/edit/406', 'admin/firesale/products/delete/406'),
(308, 'CAMISETA OFICIAL 2015 MANGA CORTA  - L', 'Camiseta oficial 2015 manga corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '407', 'product/', 'admin/firesale/products/edit/407', 'admin/firesale/products/delete/407'),
(309, 'CAMISETA OFICIAL 2015 MANGA CORTA  - XL', 'Camiseta oficial 2015 manga corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '408', 'product/', 'admin/firesale/products/edit/408', 'admin/firesale/products/delete/408'),
(311, 'CAMISETA OFICIAL 2015 MANGA LARGA  - S', 'Camiseta Oficial 2015 manga larga talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '409', 'product/', 'admin/firesale/products/edit/409', 'admin/firesale/products/delete/409'),
(312, 'CAMISETA OFICIAL 2015 MANGA LARGA  - M', 'Camiseta Oficial 2015 manga larga talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '410', 'product/', 'admin/firesale/products/edit/410', 'admin/firesale/products/delete/410'),
(313, 'CAMISETA OFICIAL 2015 MANGA LARGA  - L', 'Camiseta Oficial 2015 manga larga talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '412', 'product/', 'admin/firesale/products/edit/412', 'admin/firesale/products/delete/412'),
(314, 'CAMISETA OFICIAL 2015 MANGA LARGA  - XL', 'Camiseta Oficial 2015 manga larga talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '413', 'product/', 'admin/firesale/products/edit/413', 'admin/firesale/products/delete/413'),
(453, 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - S', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '414', 'product/', 'admin/firesale/products/edit/414', 'admin/firesale/products/delete/414'),
(454, 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - M', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '415', 'product/', 'admin/firesale/products/edit/415', 'admin/firesale/products/delete/415'),
(455, 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - L', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '416', 'product/', 'admin/firesale/products/edit/416', 'admin/firesale/products/delete/416'),
(456, 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - XL', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '417', 'product/', 'admin/firesale/products/edit/417', 'admin/firesale/products/delete/417'),
(319, 'CAMISETA ENTRENAMIENTO 2015 AZUL  - S', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '418', 'product/', 'admin/firesale/products/edit/418', 'admin/firesale/products/delete/418'),
(320, 'CAMISETA ENTRENAMIENTO 2015 AZUL  - M', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '419', 'product/', 'admin/firesale/products/edit/419', 'admin/firesale/products/delete/419'),
(321, 'CAMISETA ENTRENAMIENTO 2015 AZUL  - L', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '420', 'product/', 'admin/firesale/products/edit/420', 'admin/firesale/products/delete/420'),
(609, 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '567', 'product/leggins-estanpado-foil-bicolor-negro', 'admin/firesale/products/edit/567', 'admin/firesale/products/delete/567'),
(641, 'PANTALONETA ENTRENO 2015', 'Pantaloneta entreno 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '558', 'product/pantaloneta-entreno-2015', 'admin/firesale/products/edit/558', 'admin/firesale/products/delete/558'),
(446, 'BUSO TIPO FLEECE - S', 'Buso en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '422', 'product/', 'admin/firesale/products/edit/422', 'admin/firesale/products/delete/422'),
(448, 'BUSO TIPO FLEECE - M', 'Buso en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '423', 'product/', 'admin/firesale/products/edit/423', 'admin/firesale/products/delete/423'),
(449, 'BUSO TIPO FLEECE - L', 'Buso en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '424', 'product/', 'admin/firesale/products/edit/424', 'admin/firesale/products/delete/424'),
(460, 'CHAQUETA TIPO FILLAT 2015  - S', 'Chaqueta de tipo gaban', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '426', 'product/', 'admin/firesale/products/edit/426', 'admin/firesale/products/delete/426'),
(461, 'CHAQUETA TIPO FILLAT 2015  - M', 'Chaqueta de tipo gaban', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '427', 'product/', 'admin/firesale/products/edit/427', 'admin/firesale/products/delete/427'),
(462, 'CHAQUETA TIPO FILLAT 2015  - L', 'Chaqueta de tipo gaban', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '428', 'product/', 'admin/firesale/products/edit/428', 'admin/firesale/products/delete/428'),
(463, 'CHAQUETA TIPO FILLAT 2015  - XL', 'Chaqueta de tipo gaban', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '429', 'product/', 'admin/firesale/products/edit/429', 'admin/firesale/products/delete/429'),
(330, 'OFICIAL DAMA 2015 - XS', 'Oficial dama 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '430', 'product/', 'admin/firesale/products/edit/430', 'admin/firesale/products/delete/430'),
(331, 'OFICIAL DAMA 2015 - S', 'Oficial dama 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '432', 'product/', 'admin/firesale/products/edit/432', 'admin/firesale/products/delete/432'),
(332, 'OFICIAL DAMA 2015 - M', 'Oficial dama 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '433', 'product/', 'admin/firesale/products/edit/433', 'admin/firesale/products/delete/433'),
(333, 'OFICIAL DAMA 2015 - L', 'Oficial dama 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '434', 'product/', 'admin/firesale/products/edit/434', 'admin/firesale/products/delete/434'),
(334, 'OFICIAL DAMA 2015 - XL', 'Oficial dama 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '435', 'product/', 'admin/firesale/products/edit/435', 'admin/firesale/products/delete/435'),
(335, 'MINIKIT 2015 - 2T', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '436', 'product/', 'admin/firesale/products/edit/436', 'admin/firesale/products/delete/436'),
(336, 'MINIKIT 2015 - 2XS', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '437', 'product/', 'admin/firesale/products/edit/437', 'admin/firesale/products/delete/437'),
(337, 'MINIKIT 2015 - 3T', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '438', 'product/', 'admin/firesale/products/edit/438', 'admin/firesale/products/delete/438'),
(338, 'MINIKIT 2015 - 4T', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '439', 'product/', 'admin/firesale/products/edit/439', 'admin/firesale/products/delete/439'),
(339, 'MINIKIT 2015 - 5T', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '440', 'product/', 'admin/firesale/products/edit/440', 'admin/firesale/products/delete/440'),
(464, 'PANTALONETA OFICIAL 2015 - S', 'Pantaloneta oficial 2105', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '441', 'product/', 'admin/firesale/products/edit/441', 'admin/firesale/products/delete/441'),
(465, 'PANTALONETA OFICIAL 2015 - M', 'Pantaloneta oficial 2105', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '442', 'product/', 'admin/firesale/products/edit/442', 'admin/firesale/products/delete/442'),
(466, 'PANTALONETA OFICIAL 2015 - L', 'Pantaloneta oficial 2105', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '443', 'product/', 'admin/firesale/products/edit/443', 'admin/firesale/products/delete/443'),
(467, 'PANTALONETA OFICIAL 2015 - XL', 'Pantaloneta oficial 2105', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '444', 'product/', 'admin/firesale/products/edit/444', 'admin/firesale/products/delete/444'),
(469, 'PANTALONETA VISITANTE 2015 - S', 'Pantaloneta visitante 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '445', 'product/', 'admin/firesale/products/edit/445', 'admin/firesale/products/delete/445'),
(471, 'PANTALONETA VISITANTE 2015 - M', 'Pantaloneta visitante 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '446', 'product/', 'admin/firesale/products/edit/446', 'admin/firesale/products/delete/446'),
(472, 'PANTALONETA VISITANTE 2015 - L', 'Pantaloneta visitante 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '447', 'product/', 'admin/firesale/products/edit/447', 'admin/firesale/products/delete/447'),
(473, 'PANTALONETA VISITANTE 2015 - XL', 'Pantaloneta visitante 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '448', 'product/', 'admin/firesale/products/edit/448', 'admin/firesale/products/delete/448'),
(348, 'POLO BLANCA 2015 - S', 'Polo blanca 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '449', 'product/ballet-pumps-7-green', 'admin/firesale/products/edit/449', 'admin/firesale/products/delete/449'),
(349, 'POLO BLANCA 2015 - M', 'Polo blanca 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '450', 'product/ballet-pumps-7-green', 'admin/firesale/products/edit/450', 'admin/firesale/products/delete/450'),
(350, 'POLO BLANCA 2015 - L', 'Polo blanca 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '451', 'product/ballet-pumps-7-green', 'admin/firesale/products/edit/451', 'admin/firesale/products/delete/451'),
(351, 'POLO BLANCA 2015 - XL', 'Polo blanca 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '452', 'product/ballet-pumps-7-green', 'admin/firesale/products/edit/452', 'admin/firesale/products/delete/452'),
(353, 'ROMPEVIENTOS 2015 - S', 'Rompevientos 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '453', 'product/ballet-pumps-7-magenta', 'admin/firesale/products/edit/453', 'admin/firesale/products/delete/453'),
(354, 'ROMPEVIENTOS 2015 - M', 'Rompevientos 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '454', 'product/ballet-pumps-7-magenta', 'admin/firesale/products/edit/454', 'admin/firesale/products/delete/454'),
(355, 'ROMPEVIENTOS 2015 - L', 'Rompevientos 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '455', 'product/ballet-pumps-7-magenta', 'admin/firesale/products/edit/455', 'admin/firesale/products/delete/455'),
(356, 'ROMPEVIENTOS 2015 - XL', 'Rompevientos 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '456', 'product/ballet-pumps-7-magenta', 'admin/firesale/products/edit/456', 'admin/firesale/products/delete/456'),
(474, 'SUDADERA ENTRENO 2015 - S', 'Sudadera entreno 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '457', 'product/ballet-pumps-7-purple', 'admin/firesale/products/edit/457', 'admin/firesale/products/delete/457'),
(476, 'SUDADERA ENTRENO 2015 - M', 'Sudadera entreno 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '458', 'product/ballet-pumps-7-purple', 'admin/firesale/products/edit/458', 'admin/firesale/products/delete/458'),
(478, 'SUDADERA ENTRENO 2015 - L', 'Sudadera entreno 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '459', 'product/ballet-pumps-7-purple', 'admin/firesale/products/edit/459', 'admin/firesale/products/delete/459'),
(479, 'SUDADERA ENTRENO 2015 - L', 'Sudadera entreno 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '460', 'product/ballet-pumps-7-purple', 'admin/firesale/products/edit/460', 'admin/firesale/products/delete/460'),
(480, 'SUDADERA PRESENTACION 2015 - S', 'Sudadera presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '461', 'product/ballet-pumps-8-red', 'admin/firesale/products/edit/461', 'admin/firesale/products/delete/461'),
(481, 'SUDADERA PRESENTACION 2015 - M', 'Sudadera presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '462', 'product/ballet-pumps-8-red', 'admin/firesale/products/edit/462', 'admin/firesale/products/delete/462'),
(483, 'SUDADERA PRESENTACION 2015 - L', 'Sudadera presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '464', 'product/ballet-pumps-8-red', 'admin/firesale/products/edit/464', 'admin/firesale/products/delete/464'),
(484, 'SUDADERA PRESENTACION 2015 - XL', 'Sudadera presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '465', 'product/ballet-pumps-8-red', 'admin/firesale/products/edit/465', 'admin/firesale/products/delete/465'),
(365, 'CHAQUETA PRESENTACION 2015 - S', 'Chaqueta presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '466', 'product/ballet-pumps-8-magenta', 'admin/firesale/products/edit/466', 'admin/firesale/products/delete/466'),
(451, 'CHAQUETA PRESENTACION 2015 - M', 'Chaqueta presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '467', 'product/ballet-pumps-8-magenta', 'admin/firesale/products/edit/467', 'admin/firesale/products/delete/467'),
(452, 'CHAQUETA PRESENTACION 2015 - L', 'Chaqueta presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '468', 'product/ballet-pumps-8-magenta', 'admin/firesale/products/edit/468', 'admin/firesale/products/delete/468'),
(368, 'CHAQUETA PRESENTACION 2015 - XL', 'Chaqueta presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '469', 'product/ballet-pumps-8-magenta', 'admin/firesale/products/edit/469', 'admin/firesale/products/delete/469'),
(373, 'CAMISETA OFICIAL NIOS 2015 - S', 'camiseta oficial nios 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '470', 'product/ballet-pumps-8-purple', 'admin/firesale/products/edit/470', 'admin/firesale/products/delete/470'),
(370, 'CAMISETA OFICIAL NIOS 2015 - M', 'camiseta oficial nios 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '471', 'product/ballet-pumps-8-purple', 'admin/firesale/products/edit/471', 'admin/firesale/products/delete/471'),
(371, 'CAMISETA OFICIAL NIOS 2015 - L', 'camiseta oficial nios 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '472', 'product/ballet-pumps-8-purple', 'admin/firesale/products/edit/472', 'admin/firesale/products/delete/472'),
(372, 'CAMISETA OFICIAL NIOS 2015 - XL', 'camiseta oficial nios 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '473', 'product/ballet-pumps-8-purple', 'admin/firesale/products/edit/473', 'admin/firesale/products/delete/473'),
(374, 'PANTALON TIPO YOGA GRIS JASPE - S', 'PANTALON TIPO YOGA GRIS JASPE', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '474', 'product/ballet-pumps-9-red', 'admin/firesale/products/edit/474', 'admin/firesale/products/delete/474'),
(375, 'PANTALON TIPO YOGA GRIS JASPE - M', 'PANTALON TIPO YOGA GRIS JASPE', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '475', 'product/ballet-pumps-9-red', 'admin/firesale/products/edit/475', 'admin/firesale/products/delete/475'),
(376, 'PANTALON TIPO YOGA GRIS JASPE - L', 'PANTALON TIPO YOGA GRIS JASPE', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '476', 'product/ballet-pumps-9-red', 'admin/firesale/products/edit/476', 'admin/firesale/products/delete/476'),
(377, 'PANTALON TIPO YOGA GRIS JASPE - XL', 'PANTALON TIPO YOGA GRIS JASPE', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '477', 'product/ballet-pumps-9-red', 'admin/firesale/products/edit/477', 'admin/firesale/products/delete/477'),
(378, 'T SHIRT OVER CON ESTAMPADO GRIS - S', 'T SHIRT OVER CON ESTAMPADO GRIS', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '478', 'product/ballet-pumps-9-green', 'admin/firesale/products/edit/478', 'admin/firesale/products/delete/478'),
(379, 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - S', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '482', 'product/ballet-pumps-9-magenta', 'admin/firesale/products/edit/482', 'admin/firesale/products/delete/482'),
(380, 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - M', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '483', 'product/ballet-pumps-9-magenta', 'admin/firesale/products/edit/483', 'admin/firesale/products/delete/483'),
(381, 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - L', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '484', 'product/ballet-pumps-9-magenta', 'admin/firesale/products/edit/484', 'admin/firesale/products/delete/484'),
(382, 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - XL', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '485', 'product/ballet-pumps-9-magenta', 'admin/firesale/products/edit/485', 'admin/firesale/products/delete/485'),
(383, 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - S', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '486', 'product/ballet-pumps-9-purple', 'admin/firesale/products/edit/486', 'admin/firesale/products/delete/486'),
(384, 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - M', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '487', 'product/ballet-pumps-9-purple', 'admin/firesale/products/edit/487', 'admin/firesale/products/delete/487'),
(385, 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - L', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '488', 'product/ballet-pumps-9-purple', 'admin/firesale/products/edit/488', 'admin/firesale/products/delete/488'),
(386, 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - XL', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '489', 'product/ballet-pumps-9-purple', 'admin/firesale/products/edit/489', 'admin/firesale/products/delete/489'),
(412, 'ESQUELETO ESTRELLA GRIS - S', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '490', 'product/holiday-shower-gel-talla-38', 'admin/firesale/products/edit/490', 'admin/firesale/products/delete/490'),
(417, 'ESQUELETO ESTRELLA GRIS - M', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '491', 'product/holiday-shower-gel-talla-38', 'admin/firesale/products/edit/491', 'admin/firesale/products/delete/491'),
(418, 'ESQUELETO ESTRELLA GRIS - L', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '492', 'product/holiday-shower-gel-talla-38', 'admin/firesale/products/edit/492', 'admin/firesale/products/delete/492'),
(419, 'ESQUELETO ESTRELLA GRIS - XL', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '493', 'product/holiday-shower-gel-talla-38', 'admin/firesale/products/edit/493', 'admin/firesale/products/delete/493'),
(425, 'ESQUELETO ESTRELLA BLANCO - S', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '494', 'product/', 'admin/firesale/products/edit/494', 'admin/firesale/products/delete/494'),
(422, 'ESQUELETO ESTRELLA BLANCO - M', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '495', 'product/', 'admin/firesale/products/edit/495', 'admin/firesale/products/delete/495'),
(423, 'ESQUELETO ESTRELLA BLANCO - L', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '496', 'product/', 'admin/firesale/products/edit/496', 'admin/firesale/products/delete/496'),
(395, 'ESQUELETO ESTRELLA BLANCO - XL', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '497', 'product/', 'admin/firesale/products/edit/497', 'admin/firesale/products/delete/497'),
(424, 'ESQUELETO ESTRELLA BLANCO - XS', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '498', 'product/', 'admin/firesale/products/edit/498', 'admin/firesale/products/delete/498'),
(397, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - XS', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '504', 'product/', 'admin/firesale/products/edit/504', 'admin/firesale/products/delete/504'),
(398, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - S', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '505', 'product/', 'admin/firesale/products/edit/505', 'admin/firesale/products/delete/505'),
(399, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - M', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '506', 'product/', 'admin/firesale/products/edit/506', 'admin/firesale/products/delete/506'),
(400, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - L', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '507', 'product/', 'admin/firesale/products/edit/507', 'admin/firesale/products/delete/507'),
(401, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - XL', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '508', 'product/', 'admin/firesale/products/edit/508', 'admin/firesale/products/delete/508'),
(485, 'SWEATER CUELLO EN V MUJER MFC - S', 'SWEATER CUELLO EN V MUJER MFC', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '509', 'product/', 'admin/firesale/products/edit/509', 'admin/firesale/products/delete/509'),
(403, 'SWEATER CUELLO EN V MUJER MFC - M', 'SWEATER CUELLO EN V MUJER MFC', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '510', 'product/', 'admin/firesale/products/edit/510', 'admin/firesale/products/delete/510'),
(404, 'SWEATER CUELLO EN V MUJER MFC - L', 'SWEATER CUELLO EN V MUJER MFC', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '511', 'product/', 'admin/firesale/products/edit/511', 'admin/firesale/products/delete/511'),
(405, 'SWEATER CUELLO EN V MUJER MFC - XL', 'SWEATER CUELLO EN V MUJER MFC', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '512', 'product/', 'admin/firesale/products/edit/512', 'admin/firesale/products/delete/512'),
(604, 'CAMISETA OFICIAL NIOS 2015', 'camiseta oficial nios 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '563', 'product/camiseta-oficial-ni-os-2015', 'admin/firesale/products/edit/563', 'admin/firesale/products/delete/563'),
(780, 'CHAQUETA PRESENTACION 2015', 'Chaqueta presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '562', 'product/chaqueta-presentacion-2015', 'admin/firesale/products/edit/562', 'admin/firesale/products/delete/562'),
(804, 'CAMISETA ESTAMPADA MC AZUL REY', 'CAMISETA ESTAMPADA MC AZUL REY', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '572', 'product/camiseta-estampada-m-c-azul-rey', 'admin/firesale/products/edit/572', 'admin/firesale/products/delete/572'),
(810, 'CAMISETA OFICIAL 2015 MANGA LARGA', 'Camiseta Oficial 2015 manga larga talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '545', 'product/camiseta-oficial-2015-manga-larga', 'admin/firesale/products/edit/545', 'admin/firesale/products/delete/545'),
(614, 'POLO AZUL REY 2015', 'Polo azul rey 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '557', 'product/polo-azul-rey-2015', 'admin/firesale/products/edit/557', 'admin/firesale/products/delete/557'),
(798, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '571', 'product/camiseta-estampada-m-c-bicolor-blanco', 'admin/firesale/products/edit/571', 'admin/firesale/products/delete/571'),
(591, 'SUDADERA ENTRENO 2015', 'Sudadera entreno 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '560', 'product/sudadera-entreno-2015', 'admin/firesale/products/edit/560', 'admin/firesale/products/delete/560'),
(783, 'SWEATER CUELLO EN V MUJER MFC', 'SWEATER CUELLO EN V MUJER MFC', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '573', 'product/sweater-cuello-en-v-mujer-mfc', 'admin/firesale/products/edit/573', 'admin/firesale/products/delete/573'),
(514, 'T SHIRT OVER CON ESTAMPADO GRIS', 'T SHIRT OVER CON ESTAMPADO GRIS', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '565', 'product/t-shirt-over-con-estampado-gris', 'admin/firesale/products/edit/565', 'admin/firesale/products/delete/565'),
(515, 'SUETER CAPOTA STYLE GRIS', 'SUETER CAPOTA STYLE GRIS', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '570', 'product/sueter-capota-style-gris', 'admin/firesale/products/edit/570', 'admin/firesale/products/delete/570'),
(697, 'SUDADERA PRESENTACION 2015', 'Sudadera presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '561', 'product/sudadera-presentacion-2015', 'admin/firesale/products/edit/561', 'admin/firesale/products/delete/561'),
(646, 'POLO BLANCA 2015', 'Polo blanca 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '556', 'product/polo-blanca-2015', 'admin/firesale/products/edit/556', 'admin/firesale/products/delete/556'),
(439, 'T SHIRT OVER CON ESTAMPADO CAMPIN - S', 'Camiseta Manga Larga Gris Talla 4', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '398', 'product/', 'admin/firesale/products/edit/398', 'admin/firesale/products/delete/398'),
(450, 'BUSO TIPO FLEECE - XL', 'Buso en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '425', 'product/', 'admin/firesale/products/edit/425', 'admin/firesale/products/delete/425'),
(457, 'PANTALONETA ENTRENO 2015 - S', 'Pantaloneta entreno 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '513', 'product/holiday-shower-gel-rojo', 'admin/firesale/products/edit/513', 'admin/firesale/products/delete/513'),
(458, 'PANTALONETA ENTRENO 2015 - M', 'Pantaloneta entreno 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '515', 'product/holiday-shower-gel-rojo', 'admin/firesale/products/edit/515', 'admin/firesale/products/delete/515'),
(459, 'PANTALONETA ENTRENO 2015 - L', 'Pantaloneta entreno 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '516', 'product/holiday-shower-gel-rojo', 'admin/firesale/products/edit/516', 'admin/firesale/products/delete/516'),
(634, 'PANTALON TIPO YOGA GRIS JASPE', 'PANTALON TIPO YOGA GRIS JASPE', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '564', 'product/pantalon-tipo-yoga-gris-jaspe', 'admin/firesale/products/edit/564', 'admin/firesale/products/delete/564'),
(608, 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '566', 'product/leggins-estanpado-foil-bicolor-azul', 'admin/firesale/products/edit/566', 'admin/firesale/products/delete/566'),
(575, 'PROTECTOR PARA SAMSUNG S4  AURICULARES MFC', 'Protector para Samsung S4 auriculares Mfc', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '579', 'product/protector-para-samsung-s4-auriculares-mfc', 'admin/firesale/products/edit/579', 'admin/firesale/products/delete/579'),
(786, 'CAMISETA ENTRENAMIENTO 2015 BLANCA', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '546', 'product/camiseta-entrenamiento-2015-blanca', 'admin/firesale/products/edit/546', 'admin/firesale/products/delete/546'),
(633, 'OFICIAL DAMA 2015', 'Oficial dama 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '552', 'product/oficial-dama-2015', 'admin/firesale/products/edit/552', 'admin/firesale/products/delete/552'),
(581, 'BUSO SHOP', 'Sweter Estampado Con Mangas en Pu Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '539', 'product/buso-shop', 'admin/firesale/products/edit/539', 'admin/firesale/products/delete/539'),
(652, 'CAMISETA VISITANTE 2015 MANGA LARGA 2015', 'Camiseta Visitante 2015 Manga Larga Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '534', 'product/camiseta-visitante-2015-manga-larga-2015', 'admin/firesale/products/edit/534', 'admin/firesale/products/delete/534'),
(781, 'MINIKIT 2015', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '553', 'product/minikit-2015', 'admin/firesale/products/edit/553', 'admin/firesale/products/delete/553'),
(529, 'AUDIFONOS', 'Audifonos con microfono y switch para contestar y colgar llamadas', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '529', 'product/audifonos', 'admin/firesale/products/edit/529', 'admin/firesale/products/delete/529'),
(746, 'MORRAL MILLONARIOS MFC', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '581', 'product/morral-millonarios-mfc', 'admin/firesale/products/edit/581', 'admin/firesale/products/delete/581'),
(647, 'GORRA MFC 2015', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '582', 'product/gorra-mfc-2015', 'admin/firesale/products/edit/582', 'admin/firesale/products/delete/582'),
(606, 'ESQUELETO ESTRELLA BLANCO', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '569', 'product/esqueleto-estrella-blanco', 'admin/firesale/products/edit/569', 'admin/firesale/products/delete/569'),
(570, 'CUBRELECHO DOBLE', 'COMFORT DOBLE CON DOS FUNDAS', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '525', 'product/cubrelecho-doble', 'admin/firesale/products/edit/525', 'admin/firesale/products/delete/525'),
(619, 'CHAQUETA TIPO FILLAT 2015', 'Chaqueta de tipo gaban', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '549', 'product/chaqueta-tipo-fillat-2015', 'admin/firesale/products/edit/549', 'admin/firesale/products/delete/549'),
(593, 'BUSO TIPO FLEECE', 'Buso en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '548', 'product/buso-tipo-fleece', 'admin/firesale/products/edit/548', 'admin/firesale/products/delete/548'),
(598, 'CAMISETA OFICIAL 2015 MANGA CORTA', 'Camiseta oficial 2015 manga corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '544', 'product/camiseta-oficial-2015-manga-corta', 'admin/firesale/products/edit/544', 'admin/firesale/products/delete/544'),
(585, 'CAMISETA PASION DE MILLONES', 'Camiseta Pasion De Millones Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '543', 'product/camiseta-pasion-de-millones', 'admin/firesale/products/edit/543', 'admin/firesale/products/delete/543'),
(594, 'CAMISETA ENTRENAMIENTO 2015 AZUL', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '547', 'product/camiseta-entrenamiento-2015-azul', 'admin/firesale/products/edit/547', 'admin/firesale/products/delete/547'),
(635, 'CAMISETA VISITANTE 2015 MANGA CORTA', 'Camiseta Visitante 2015 manga Corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '533', 'product/camiseta-visitante-2015-manga-corta', 'admin/firesale/products/edit/533', 'admin/firesale/products/delete/533'),
(776, 'CHAQUETA PRESENTACION 2015 - S', 'Chaqueta presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '666', 'product/', 'admin/firesale/products/edit/666', 'admin/firesale/products/delete/666'),
(737, 'ROMPEVIENTOS 2015', 'Rompevientos 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '559', 'product/rompevientos-2015', 'admin/firesale/products/edit/559', 'admin/firesale/products/delete/559'),
(620, 'CAMISETA OFICIAL 2013 - M', 'Camiseta Oficial 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '706', 'product/', 'admin/firesale/products/edit/706', 'admin/firesale/products/delete/706'),
(621, 'CAMISETA OFICIAL 2013 - L', 'Camiseta Oficial 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '707', 'product/', 'admin/firesale/products/edit/707', 'admin/firesale/products/delete/707'),
(622, 'CAMISETA OFICIAL 2013 - XL', 'Camiseta Oficial 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '708', 'product/', 'admin/firesale/products/edit/708', 'admin/firesale/products/delete/708'),
(623, 'CAMISETA OFICIAL 2013 - M', 'Camiseta Oficial 2013 Manga Larga Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '656', 'product/', 'admin/firesale/products/edit/656', 'admin/firesale/products/delete/656'),
(625, 'CAMISETA OFICIAL 2013 - S', 'Camiseta Oficial 2013 Manga Larga Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '709', 'product/', 'admin/firesale/products/edit/709', 'admin/firesale/products/delete/709'),
(626, 'CAMISETA OFICIAL 2013 - L', 'Camiseta Oficial 2013 Manga Larga Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '710', 'product/', 'admin/firesale/products/edit/710', 'admin/firesale/products/delete/710'),
(627, 'CAMISETA OFICIAL 2013 - XL', 'Camiseta Oficial 2013 Manga Larga Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '711', 'product/', 'admin/firesale/products/edit/711', 'admin/firesale/products/delete/711'),
(751, 'LIBRO LA GRAN HISTORIA', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '715', 'product/libro-la-gran-historia', 'admin/firesale/products/edit/715', 'admin/firesale/products/delete/715'),
(787, 'GORRA MFC 2015 NEGRA', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '716', 'product/gorra-mfc-2015-negra', 'admin/firesale/products/edit/716', 'admin/firesale/products/delete/716'),
(649, 'GORRA MFC BLANCA', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '717', 'product/gorra-mfc-blanca', 'admin/firesale/products/edit/717', 'admin/firesale/products/delete/717'),
(644, 'PANTALONETA VISITANTE 2015', 'Pantaloneta visitante 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '555', 'product/pantaloneta-visitante-2015', 'admin/firesale/products/edit/555', 'admin/firesale/products/delete/555'),
(650, 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - L', 'Camiseta Visitante 2015 Manga Larga Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '732', 'product/running-nike-dart-10-msl-negrorojo-verde', 'admin/firesale/products/edit/732', 'admin/firesale/products/delete/732'),
(651, 'CAMISETA VISITANTE 2015 MANGA LARGA 2015  - XL', 'Camiseta Visitante 2015 Manga Larga Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '733', 'product/running-nike-dart-10-msl-negrorojo-verde', 'admin/firesale/products/edit/733', 'admin/firesale/products/delete/733'),
(655, 'SUDADERA DE ENTRENAMIENTO 2014 - XL', 'Sudadera De entrenamiento Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '602', 'product/', 'admin/firesale/products/edit/602', 'admin/firesale/products/delete/602'),
(656, 'SUDADERA DE ENTRENAMIENTO 2014 - L', 'Sudadera De entrenamiento Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '601', 'product/', 'admin/firesale/products/edit/601', 'admin/firesale/products/delete/601'),
(657, 'SUDADERA DE ENTRENAMIENTO 2014 - M', 'Sudadera De entrenamiento Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '600', 'product/', 'admin/firesale/products/edit/600', 'admin/firesale/products/delete/600');
INSERT INTO `default_search_index` (`id`, `title`, `description`, `keywords`, `keyword_hash`, `module`, `entry_key`, `entry_plural`, `entry_id`, `uri`, `cp_edit_uri`, `cp_delete_uri`) VALUES
(658, 'SUDADERA DE ENTRENAMIENTO 2014 - S', 'Sudadera De entrenamiento Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '599', 'product/', 'admin/firesale/products/edit/599', 'admin/firesale/products/delete/599'),
(659, 'SUDADERA DE PRESENTACION 2014 - S', 'Sudadera Impermeable Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '658', 'product/', 'admin/firesale/products/edit/658', 'admin/firesale/products/delete/658'),
(660, 'SUDADERA DE PRESENTACION 2014 - M', 'Sudadera Impermeable Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '659', 'product/', 'admin/firesale/products/edit/659', 'admin/firesale/products/delete/659'),
(661, 'SUDADERA DE PRESENTACION 2014 - L', 'Sudadera Impermeable Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '660', 'product/', 'admin/firesale/products/edit/660', 'admin/firesale/products/delete/660'),
(662, 'SUDADERA DE PRESENTACION 2014 - XL', 'Sudadera Impermeable Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '661', 'product/', 'admin/firesale/products/edit/661', 'admin/firesale/products/delete/661'),
(663, 'BUSO SHETLAND  - S', 'Buso Tejido Cuello V Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '595', 'product/', 'admin/firesale/products/edit/595', 'admin/firesale/products/delete/595'),
(664, 'BUSO SHETLAND  - M', 'Buso Tejido Cuello V Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '596', 'product/', 'admin/firesale/products/edit/596', 'admin/firesale/products/delete/596'),
(665, 'BUSO SHETLAND  - L', 'Buso Tejido Cuello V Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '597', 'product/', 'admin/firesale/products/edit/597', 'admin/firesale/products/delete/597'),
(666, 'BUSO SHETLAND  - XL', 'Buso Tejido Cuello V Talla M', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '598', 'product/', 'admin/firesale/products/edit/598', 'admin/firesale/products/delete/598'),
(668, 'SWEATER CUELLO EN V MUJER MFC - S', 'SWEATER CUELLO EN V MUJER MFC', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '698', 'product/', 'admin/firesale/products/edit/698', 'admin/firesale/products/delete/698'),
(669, 'SWEATER CUELLO EN V MUJER MFC - M', 'SWEATER CUELLO EN V MUJER MFC', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '699', 'product/', 'admin/firesale/products/edit/699', 'admin/firesale/products/delete/699'),
(670, 'SWEATER CUELLO EN V MUJER MFC - L', 'SWEATER CUELLO EN V MUJER MFC', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '700', 'product/', 'admin/firesale/products/edit/700', 'admin/firesale/products/delete/700'),
(671, 'SWEATER CUELLO EN V MUJER MFC - XL', 'SWEATER CUELLO EN V MUJER MFC', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '701', 'product/', 'admin/firesale/products/edit/701', 'admin/firesale/products/delete/701'),
(672, 'CAMISETA VISITANTE 2015 MANGA CORTA  - S', 'Camiseta Visitante 2015 manga Corta Talla s', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '657', 'product/', 'admin/firesale/products/edit/657', 'admin/firesale/products/delete/657'),
(673, 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - S', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '651', 'product/', 'admin/firesale/products/edit/651', 'admin/firesale/products/delete/651'),
(674, 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - M', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '652', 'product/', 'admin/firesale/products/edit/652', 'admin/firesale/products/delete/652'),
(675, 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - L', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '653', 'product/', 'admin/firesale/products/edit/653', 'admin/firesale/products/delete/653'),
(676, 'CAMISETA ENTRENAMIENTO 2015 BLANCA  - XL', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '654', 'product/', 'admin/firesale/products/edit/654', 'admin/firesale/products/delete/654'),
(677, 'CAMISETA ENTRENAMIENTO 2015 AZUL  - S', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '647', 'product/', 'admin/firesale/products/edit/647', 'admin/firesale/products/delete/647'),
(678, 'CAMISETA ENTRENAMIENTO 2015 AZUL  - M', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '648', 'product/', 'admin/firesale/products/edit/648', 'admin/firesale/products/delete/648'),
(679, 'CAMISETA ENTRENAMIENTO 2015 AZUL  - L', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '649', 'product/', 'admin/firesale/products/edit/649', 'admin/firesale/products/delete/649'),
(680, 'CAMISETA ENTRENAMIENTO 2015 AZUL  - XL', 'Camiseta de entrenamiento AdiZero', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '650', 'product/', 'admin/firesale/products/edit/650', 'admin/firesale/products/delete/650'),
(681, 'BUSO TIPO FLEECE - S', 'Buso en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '643', 'product/', 'admin/firesale/products/edit/643', 'admin/firesale/products/delete/643'),
(682, 'BUSO TIPO FLEECE - M', 'Buso en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '644', 'product/', 'admin/firesale/products/edit/644', 'admin/firesale/products/delete/644'),
(683, 'BUSO TIPO FLEECE - L', 'Buso en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '645', 'product/', 'admin/firesale/products/edit/645', 'admin/firesale/products/delete/645'),
(684, 'BUSO TIPO FLEECE - XL', 'Buso en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '646', 'product/', 'admin/firesale/products/edit/646', 'admin/firesale/products/delete/646'),
(689, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - XS', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '627', 'product/', 'admin/firesale/products/edit/627', 'admin/firesale/products/delete/627'),
(686, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - S', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '628', 'product/', 'admin/firesale/products/edit/628', 'admin/firesale/products/delete/628'),
(688, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - L', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '630', 'product/', 'admin/firesale/products/edit/630', 'admin/firesale/products/delete/630'),
(690, 'CAMISETA ESTAMPADA MC BICOLOR BLANCO - M', 'CAMISETA ESTAMPADA MC BICOLOR BLANCO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '629', 'product/', 'admin/firesale/products/edit/629', 'admin/firesale/products/delete/629'),
(696, 'CAMISETA NIÑO LA PASION', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '734', 'product/camiseta-nino-la-pasion', 'admin/firesale/products/edit/734', 'admin/firesale/products/delete/734'),
(692, 'CAMISETA NIÑO LA PASION - 2/4', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '735', 'product/', 'admin/firesale/products/edit/735', 'admin/firesale/products/delete/735'),
(693, 'CAMISETA NIÑO LA PASION - 6/8', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '736', 'product/', 'admin/firesale/products/edit/736', 'admin/firesale/products/delete/736'),
(694, 'CAMISETA NIÑO LA PASION - 10/12', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '737', 'product/', 'admin/firesale/products/edit/737', 'admin/firesale/products/delete/737'),
(695, 'CAMISETA NIÑO LA PASION - 10/16', '', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '738', 'product/', 'admin/firesale/products/edit/738', 'admin/firesale/products/delete/738'),
(772, 'CHAQUETA PRESENTACION 2015 - XL', 'Chaqueta presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '669', 'product/', 'admin/firesale/products/edit/669', 'admin/firesale/products/delete/669'),
(700, 'ROMPEVIENTOS 2015 - M', 'Rompevientos 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '691', 'product/alcancia-imaginamos-alcancia-pequena', 'admin/firesale/products/edit/691', 'admin/firesale/products/delete/691'),
(701, 'ROMPEVIENTOS 2015 - L', 'Rompevientos 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '692', 'product/alcancia-imaginamos-alcancia-pequena', 'admin/firesale/products/edit/692', 'admin/firesale/products/delete/692'),
(702, 'ROMPEVIENTOS 2015 - XL', 'Rompevientos 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '693', 'product/alcancia-imaginamos-alcancia-pequena', 'admin/firesale/products/edit/693', 'admin/firesale/products/delete/693'),
(704, 'ESQUELETO ESTRELLA GRIS - XS', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '587', 'product/', 'admin/firesale/products/edit/587', 'admin/firesale/products/delete/587'),
(705, 'ESQUELETO ESTRELLA GRIS - S', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '588', 'product/', 'admin/firesale/products/edit/588', 'admin/firesale/products/delete/588'),
(706, 'ESQUELETO ESTRELLA GRIS - M', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '589', 'product/', 'admin/firesale/products/edit/589', 'admin/firesale/products/delete/589'),
(707, 'ESQUELETO ESTRELLA GRIS - L', 'ESQUELETO ESTRELLA', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '590', 'product/', 'admin/firesale/products/edit/590', 'admin/firesale/products/delete/590'),
(708, 'PANTALON TIPO YOGA GRIS JASPE - XS', 'PANTALON TIPO YOGA GRIS JASPE', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '679', 'product/', 'admin/firesale/products/edit/679', 'admin/firesale/products/delete/679'),
(709, 'PANTALON TIPO YOGA GRIS JASPE - S', 'PANTALON TIPO YOGA GRIS JASPE', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '680', 'product/', 'admin/firesale/products/edit/680', 'admin/firesale/products/delete/680'),
(710, 'PANTALON TIPO YOGA GRIS JASPE - M', 'PANTALON TIPO YOGA GRIS JASPE', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '681', 'product/', 'admin/firesale/products/edit/681', 'admin/firesale/products/delete/681'),
(711, 'PANTALON TIPO YOGA GRIS JASPE - L', 'PANTALON TIPO YOGA GRIS JASPE', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '682', 'product/', 'admin/firesale/products/edit/682', 'admin/firesale/products/delete/682'),
(712, 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - S', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '591', 'product/', 'admin/firesale/products/edit/591', 'admin/firesale/products/delete/591'),
(713, 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - M', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '592', 'product/', 'admin/firesale/products/edit/592', 'admin/firesale/products/delete/592'),
(714, 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - L', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '593', 'product/', 'admin/firesale/products/edit/593', 'admin/firesale/products/delete/593'),
(715, 'LEGGINS ESTANPADO FOIL BICOLOR AZUL - XL', 'LEGGINS ESTANPADO FOIL BICOLOR AZUL', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '594', 'product/', 'admin/firesale/products/edit/594', 'admin/firesale/products/delete/594'),
(716, 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - S', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '670', 'product/', 'admin/firesale/products/edit/670', 'admin/firesale/products/delete/670'),
(717, 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - M', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '671', 'product/', 'admin/firesale/products/edit/671', 'admin/firesale/products/delete/671'),
(718, 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - L', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '672', 'product/', 'admin/firesale/products/edit/672', 'admin/firesale/products/delete/672'),
(719, 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO - XL', 'LEGGINS ESTANPADO FOIL BICOLOR NEGRO', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '673', 'product/', 'admin/firesale/products/edit/673', 'admin/firesale/products/delete/673'),
(720, 'POLO BLANCA 2015 - S', 'Polo blanca 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '723', 'product/prueba-s-esqueleto-rojo', 'admin/firesale/products/edit/723', 'admin/firesale/products/delete/723'),
(721, 'POLO BLANCA 2015 - M', 'Polo blanca 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '724', 'product/prueba-s-esqueleto-rojo', 'admin/firesale/products/edit/724', 'admin/firesale/products/delete/724'),
(722, 'POLO BLANCA 2015 - L', 'Polo blanca 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '725', 'product/prueba-s-esqueleto-rojo', 'admin/firesale/products/edit/725', 'admin/firesale/products/delete/725'),
(723, 'POLO BLANCA 2015 - XL', 'Polo blanca 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '726', 'product/prueba-s-esqueleto-rojo', 'admin/firesale/products/edit/726', 'admin/firesale/products/delete/726'),
(724, 'POLO AZUL REY 2015 - S', 'Polo azul rey 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '631', 'product/', 'admin/firesale/products/edit/631', 'admin/firesale/products/delete/631'),
(725, 'POLO AZUL REY 2015 - M', 'Polo azul rey 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '632', 'product/', 'admin/firesale/products/edit/632', 'admin/firesale/products/delete/632'),
(726, 'POLO AZUL REY 2015 - L', 'Polo azul rey 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '633', 'product/', 'admin/firesale/products/edit/633', 'admin/firesale/products/delete/633'),
(727, 'POLO AZUL REY 2015 - XL', 'Polo azul rey 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '634', 'product/', 'admin/firesale/products/edit/634', 'admin/firesale/products/delete/634'),
(728, 'CHAQUETA TIPO FILLAT 2015  - S', 'Chaqueta de tipo gaban', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '702', 'product/prueba-s', 'admin/firesale/products/edit/702', 'admin/firesale/products/delete/702'),
(729, 'CHAQUETA TIPO FILLAT 2015  - M', 'Chaqueta de tipo gaban', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '703', 'product/prueba-s', 'admin/firesale/products/edit/703', 'admin/firesale/products/delete/703'),
(730, 'CHAQUETA TIPO FILLAT 2015  - L', 'Chaqueta de tipo gaban', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '704', 'product/prueba-s', 'admin/firesale/products/edit/704', 'admin/firesale/products/delete/704'),
(731, 'CHAQUETA TIPO FILLAT 2015  - XL', 'Chaqueta de tipo gaban', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '705', 'product/prueba-s', 'admin/firesale/products/edit/705', 'admin/firesale/products/delete/705'),
(732, 'CAMISETA OFICIAL NIOS 2015 - S', 'camiseta oficial nios 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '662', 'product/', 'admin/firesale/products/edit/662', 'admin/firesale/products/delete/662'),
(733, 'CAMISETA OFICIAL NIOS 2015 - M', 'camiseta oficial nios 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '663', 'product/', 'admin/firesale/products/edit/663', 'admin/firesale/products/delete/663'),
(734, 'CAMISETA OFICIAL NIOS 2015 - L', 'camiseta oficial nios 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '664', 'product/', 'admin/firesale/products/edit/664', 'admin/firesale/products/delete/664'),
(735, 'CAMISETA OFICIAL NIOS 2015 - XL', 'camiseta oficial nios 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '665', 'product/', 'admin/firesale/products/edit/665', 'admin/firesale/products/delete/665'),
(738, 'PANTALONETA VISITANTE 2015 - S', 'Pantaloneta visitante 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '739', 'product/', 'admin/firesale/products/edit/739', 'admin/firesale/products/delete/739'),
(739, 'PANTALONETA VISITANTE 2015 - M', 'Pantaloneta visitante 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '740', 'product/', 'admin/firesale/products/edit/740', 'admin/firesale/products/delete/740'),
(740, 'PANTALONETA VISITANTE 2015 - XL', 'Pantaloneta visitante 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '742', 'product/', 'admin/firesale/products/edit/742', 'admin/firesale/products/delete/742'),
(741, 'PANTALONETA VISITANTE 2015 - L', 'Pantaloneta visitante 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '741', 'product/', 'admin/firesale/products/edit/741', 'admin/firesale/products/delete/741'),
(742, 'MINIKIT 2015 - 18/24', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '674', 'product/', 'admin/firesale/products/edit/674', 'admin/firesale/products/delete/674'),
(743, 'MINIKIT 2015 - 2/3', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '675', 'product/', 'admin/firesale/products/edit/675', 'admin/firesale/products/delete/675'),
(744, 'MINIKIT 2015 - 4/5', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '677', 'product/', 'admin/firesale/products/edit/677', 'admin/firesale/products/delete/677'),
(745, 'MINIKIT 2015 - 5/6', 'Minikit 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '678', 'product/', 'admin/firesale/products/edit/678', 'admin/firesale/products/delete/678'),
(752, 'COJIN 45X45', 'Cojin en fleece', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '522', 'product/cojin-45x45', 'admin/firesale/products/edit/522', 'admin/firesale/products/delete/522'),
(756, 'CAMISETAS OFICIALES 2013', 'Camiseta Oficial 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '518', 'product/camiseta-oficial-2013', 'admin/firesale/products/edit/518', 'admin/firesale/products/delete/518'),
(766, 'CHAQUETA PRESENTACION 2015 - M', 'Chaqueta presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '667', 'product/', 'admin/firesale/products/edit/667', 'admin/firesale/products/delete/667'),
(770, 'CAMISETA VISITANTE  2013', 'Camiseta visitante 2013 Manga Corta Talla S', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '517', 'product/camiseta-visitante-2013', 'admin/firesale/products/edit/517', 'admin/firesale/products/delete/517'),
(771, 'CHAQUETA PRESENTACION 2015 - L', 'Chaqueta presentacion 2015', NULL, NULL, 'firesale', 'firesale:product', 'firesale:products', '668', 'product/', 'admin/firesale/products/edit/668', 'admin/firesale/products/delete/668');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_settings`
--

CREATE TABLE IF NOT EXISTS `default_settings` (
  `slug` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` set('text','textarea','password','select','select-multiple','radio','checkbox') COLLATE utf8_unicode_ci NOT NULL,
  `default` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `options` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_required` int(1) NOT NULL,
  `is_gui` int(1) NOT NULL,
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`slug`),
  UNIQUE KEY `unique_slug` (`slug`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `default_settings`
--

INSERT INTO `default_settings` (`slug`, `title`, `description`, `type`, `default`, `value`, `options`, `is_required`, `is_gui`, `module`, `order`) VALUES
('activation_email', 'Activation Email', 'Send out an e-mail with an activation link when a user signs up. Disable this so that admins must manually activate each account.', 'select', '1', '', '0=activate_by_admin|1=activate_by_email|2=no_activation', 0, 1, 'users', 961),
('addons_upload', 'Addons Upload Permissions', 'Keeps mere admins from uploading addons by default', 'text', '0', '1', '', 1, 0, '', 0),
('admin_force_https', 'Force HTTPS for Control Panel?', 'Allow only the HTTPS protocol when using the Control Panel?', 'radio', '0', '', '1=Yes|0=No', 1, 1, '', 0),
('admin_theme', 'Control Panel Theme', 'Select the theme for the control panel.', '', '', 'pyrocms', 'func:get_themes', 1, 0, '', 0),
('akismet_api_key', 'Akismet API Key', 'Akismet is a spam-blocker from the WordPress team. It keeps spam under control without forcing users to get past human-checking CAPTCHA forms.', 'text', '', '', '', 0, 1, 'integration', 981),
('api_enabled', 'API Enabled', 'Allow API access to all modules which have an API controller.', 'select', '0', '0', '0=Disabled|1=Enabled', 0, 0, 'api', 0),
('api_user_keys', 'API User Keys', 'Allow users to sign up for API keys (if the API is Enabled).', 'select', '0', '0', '0=Disabled|1=Enabled', 0, 0, 'api', 0),
('auto_username', 'Auto Username', 'Create the username automatically, meaning users can skip making one on registration.', 'radio', '1', '', '1=Enabled|0=Disabled', 0, 1, 'users', 964),
('cdn_domain', 'CDN Domain', 'CDN domains allow you to offload static content to various edge servers, like Amazon CloudFront or MaxCDN.', 'text', '', '', '', 0, 1, 'integration', 1000),
('ckeditor_config', 'CKEditor Config', 'You can find a list of valid configuration items in <a target="_blank" href="http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.config.html">CKEditor''s documentation.</a>', 'textarea', '', '{{# this is a wysiwyg-simple editor customized for the blog module (it allows images to be inserted) #}}\n$(''textarea#intro.wysiwyg-simple'').ckeditor({\n	toolbar: [\n		[''pyroimages''],\n		[''Bold'', ''Italic'', ''-'', ''NumberedList'', ''BulletedList'', ''-'', ''Link'', ''Unlink'']\n	  ],\n	extraPlugins: ''pyroimages'',\n	width: ''99%'',\n	height: 100,\n	dialog_backgroundCoverColor: ''#000'',\n	defaultLanguage: ''{{ helper:config item="default_language" }}'',\n	language: ''{{ global:current_language }}''\n});\n\n{{# this is the config for all wysiwyg-simple textareas #}}\n$(''textarea.wysiwyg-simple'').ckeditor({\n	toolbar: [\n		[''Bold'', ''Italic'', ''-'', ''NumberedList'', ''BulletedList'', ''-'', ''Link'', ''Unlink'']\n	  ],\n	width: ''99%'',\n	height: 100,\n	dialog_backgroundCoverColor: ''#000'',\n	defaultLanguage: ''{{ helper:config item="default_language" }}'',\n	language: ''{{ global:current_language }}''\n});\n\n{{# and this is the advanced editor #}}\n$(''textarea.wysiwyg-advanced'').ckeditor({\n	toolbar: [\n		[''Maximize''],\n		[''pyroimages'', ''pyrofiles''],\n		[''Cut'',''Copy'',''Paste'',''PasteFromWord''],\n		[''Undo'',''Redo'',''-'',''Find'',''Replace''],\n		[''Link'',''Unlink''],\n		[''Table'',''HorizontalRule'',''SpecialChar''],\n		[''Bold'',''Italic'',''StrikeThrough''],\n		[''JustifyLeft'',''JustifyCenter'',''JustifyRight'',''JustifyBlock'',''-'',''BidiLtr'',''BidiRtl''],\n		[''TextColor'',''BGColor'',''Format'', ''FontSize'', ''Subscript'',''Superscript'', ''NumberedList'',''BulletedList'',''Outdent'',''Indent'',''Blockquote''],\n		[''ShowBlocks'', ''RemoveFormat'', ''Source''],\n	],\n	extraPlugins: ''pyroimages,pyrofiles'',\n	width: ''99%'',\n	height: 400,\n	dialog_backgroundCoverColor: ''#000'',\n	removePlugins: ''elementspath'',\n	defaultLanguage: ''{{ helper:config item="default_language" }}'',\n	language: ''{{ global:current_language }}''\n});', '', 1, 1, 'wysiwyg', 993),
('comment_markdown', 'Allow Markdown', 'Do you want to allow visitors to post comments using Markdown?', 'select', '0', '0', '0=Text Only|1=Allow Markdown', 1, 1, 'comments', 965),
('comment_order', 'Comment Order', 'Sort order in which to display comments.', 'select', 'ASC', 'ASC', 'ASC=Oldest First|DESC=Newest First', 1, 1, 'comments', 966),
('contact_email', 'Contact E-mail', 'All e-mails from users, guests and the site will go to this e-mail address.', 'text', 'brayan.acebo@imaginamos.co', 'jorge.clavijo@imaginamos.com, luis.salazar@imagina.co', '', 1, 1, 'email', 979),
('currency', 'Currency', 'The currency symbol for use on products, services, etc.', 'text', '$', '', '', 1, 1, '', 994),
('dashboard_rss', 'Dashboard RSS Feed', 'Link to an RSS feed that will be displayed on the dashboard.', 'text', 'https://www.getfiresale.org/blog/rss/all.rss', '', '', 0, 1, '', 990),
('dashboard_rss_count', 'Dashboard RSS Items', 'How many RSS items would you like to display on the dashboard?', 'text', '5', '5', '', 1, 1, '', 989),
('date_format', 'Date Format', 'How should dates be displayed across the website and control panel? Using the <a target="_blank" href="http://php.net/manual/en/function.date.php">date format</a> from PHP - OR - Using the format of <a target="_blank" href="http://php.net/manual/en/function.strftime.php">strings formatted as date</a> from PHP.', 'text', 'F j, Y', '', '', 1, 1, '', 995),
('default_theme', 'Default Theme', 'Select the theme you want users to see by default.', '', 'default', 'ecommerce', 'func:get_themes', 1, 0, '', 0),
('enable_comments', 'Enable Comments', 'Enable comments.', 'radio', '1', '1', '1=Enabled|0=Disabled', 1, 1, 'comments', 968),
('enable_profiles', 'Enable profiles', 'Allow users to add and edit profiles.', 'radio', '1', '', '1=Enabled|0=Disabled', 1, 1, 'users', 963),
('enable_registration', 'Enable user registration', 'Allow users to register in your site.', 'radio', '1', '', '1=Enabled|0=Disabled', 0, 1, 'users', 961),
('files_cache', 'Files Cache', 'When outputting an image via site.com/files what shall we set the cache expiration for?', 'select', '480', '480', '0=no-cache|1=1-minute|60=1-hour|180=3-hour|480=8-hour|1440=1-day|43200=30-days', 1, 1, 'files', 986),
('files_cf_api_key', 'Rackspace Cloud Files API Key', 'You also must provide your Cloud Files API Key. You will find it at the same location as your Username in your Rackspace account.', 'text', '', '', '', 0, 1, 'files', 989),
('files_cf_username', 'Rackspace Cloud Files Username', 'To enable cloud file storage in your Rackspace Cloud Files account please enter your Cloud Files Username. <a href="https://manage.rackspacecloud.com/APIAccess.do">Find your credentials</a>', 'text', '', '', '', 0, 1, 'files', 990),
('files_enabled_providers', 'Enabled File Storage Providers', 'Which file storage providers do you want to enable? (If you enable a cloud provider you must provide valid auth keys below', 'checkbox', '0', '0', 'amazon-s3=Amazon S3|rackspace-cf=Rackspace Cloud Files', 0, 1, 'files', 994),
('files_s3_access_key', 'Amazon S3 Access Key', 'To enable cloud file storage in your Amazon S3 account provide your Access Key. <a href="https://aws-portal.amazon.com/gp/aws/securityCredentials#access_credentials">Find your credentials</a>', 'text', '', '', '', 0, 1, 'files', 993),
('files_s3_geographic_location', 'Amazon S3 Geographic Location', 'Either US or EU. If you change this you must also change the S3 URL.', 'radio', 'US', 'US', 'US=United States|EU=Europe', 1, 1, 'files', 991),
('files_s3_secret_key', 'Amazon S3 Secret Key', 'You also must provide your Amazon S3 Secret Key. You will find it at the same location as your Access Key in your Amazon account.', 'text', '', '', '', 0, 1, 'files', 992),
('files_s3_url', 'Amazon S3 URL', 'Change this if using one of Amazon''s EU locations or a custom domain.', 'text', 'http://{{ bucket }}.s3.amazonaws.com/', 'http://{{ bucket }}.s3.amazonaws.com/', '', 0, 1, 'files', 991),
('files_upload_limit', 'Filesize Limit', 'Maximum filesize to allow when uploading. Specify the size in MB. Example: 5', 'text', '5', '5', '', 1, 1, 'files', 987),
('firesale_api', 'Enable FireSale API', 'Our API is available on most core pages, simply append .json or .xml', 'select', '0', '0', '1=Yes|0=No', 1, 1, 'firesale', 0),
('firesale_api_key', 'FireSale API Key', 'The API is public if this isleft blank, once set add ?key=<YOUR KEY> to access it privately', 'text', '', '', '', 0, 1, 'firesale', 0),
('firesale_basic_checkout', 'Basic Checkout View', 'Minimal checkout layout, requires a minimal.html layout in your theme', 'select', '1', '1', '1=Yes|0=No', 1, 1, 'firesale', 0),
('firesale_currency', 'Default Currency Code', 'The currency you accept (ISO-4217 format)', 'select', '1', '1', '1=GBP', 1, 1, 'firesale', 0),
('firesale_currency_key', 'Currency API Key', 'API Key from <a target="_blank" href="https://openexchangerates.org/signup/free">Open Exchange Rates</a>', 'text', '', '', '', 0, 1, 'firesale', 0),
('firesale_currency_updated', 'Currency last update time', 'The last time the currency was updated, api is updated every hour and to keep to rate limits we only check after that', 'text', '', '', '', 0, 0, 'firesale', 0),
('firesale_current_currency', 'Current Currency', 'The current currency in use, used to update existing values if default currency is changed', 'text', 'GBP', 'GBP', '', 0, 0, 'firesale', 0),
('firesale_design_enable', 'Enable Design Options For', '', 'checkbox', 'products,categories', 'products,categories', 'product=Products|category=Categories|brand=Brands', 0, 1, 'firesale', 0),
('firesale_https', '0', '0', 'select', '0', '0', '1=Yes|0=No', 1, 1, 'firesale', 0),
('firesale_login', 'Require login to purchase', 'Ensure a user is logged in before allowing them to buy products', 'select', '0', '0', '1=Yes|0=No', 1, 1, 'firesale', 0),
('firesale_low', 'Low Stock Level', 'The number of products remaining before stock is considered low', 'text', '10', '10', '', 1, 1, 'firesale', 0),
('firesale_mp_recent', 'Maximum Recent Products', 'The maximum number of recent products to track', 'text', '20', '20', '', 1, 1, 'firesale', 0),
('firesale_new', 'New Product Time', 'The time in seconds that a product is considered new', 'text', '86400', '86400', '', 1, 1, 'firesale', 0),
('firesale_perpage', 'Products per Page', 'The number of products to be displayed on category and search result pages', 'text', '15', '15', '', 1, 1, 'firesale', 0),
('firesale_review_accept', 'Moderate reviews?', 'Require reviews to be moderated before they are posted.', 'select', '0', '1', '1=Yes|0=No', 1, 1, 'firesale', 0),
('firesale_show_variations', 'Show Variations', 'Do you want to show variations on listings and search results?', 'select', '0', '0', '1=Yes|0=No', 1, 1, 'firesale', 0),
('firesale_use_assets', 'Use FireSale Assets', 'Include the FireSale CSS & JS on the front-end theme', 'select', '1', '1', '1=Yes|0=No', 1, 1, 'firesale', 0),
('frontend_enabled', 'Site Status', 'Use this option to the user-facing part of the site on or off. Useful when you want to take the site down for maintenance.', 'radio', '1', '', '1=Open|0=Closed', 1, 1, '', 988),
('ga_email', 'Google Analytic E-mail', 'E-mail address used for Google Analytics, we need this to show the graph on the dashboard.', 'text', '', '', '', 0, 1, 'integration', 983),
('ga_password', 'Google Analytic Password', 'This is also needed to show the graph on the dashboard. You will need to allow access to Google to get this to work. See <a href="https://accounts.google.com/b/0/IssuedAuthSubTokens?hl=en_US" target="_blank">Authorized Access to your Google Account</a>', 'password', '', '', '', 0, 1, 'integration', 982),
('ga_profile', 'Google Analytic Profile ID', 'Profile ID for this website in Google Analytics', 'text', '', '', '', 0, 1, 'integration', 984),
('ga_tracking', 'Google Tracking Code', 'Enter your Google Analytic Tracking Code to activate Google Analytics view data capturing. E.g: UA-19483569-6', 'text', '', '', '', 0, 1, 'integration', 985),
('image_background', 'Image Background Colour', 'Hexcode (without #) colour you wish resized image backgrounds to be', 'text', 'ffffff', 'ffffff', '', 1, 1, 'firesale', 0),
('image_square', 'Make Images Square', 'Some themes may require square images to keep layouts consistent', 'select', '0', '1', '1=Yes|0=No', 1, 1, 'firesale', 0),
('mail_line_endings', 'Email Line Endings', 'Change from the standard \\r\\n line ending to PHP_EOL for some email servers.', 'select', '1', '1', '0=PHP_EOL|1=\\r\\n', 0, 1, 'email', 972),
('mail_protocol', 'Mail Protocol', 'Select desired email protocol.', 'select', 'mail', 'smtp', 'mail=Mail|sendmail=Sendmail|smtp=SMTP', 1, 1, 'email', 977),
('mail_sendmail_path', 'Sendmail Path', 'Path to server sendmail binary.', 'text', '', '', '', 0, 1, 'email', 972),
('mail_smtp_host', 'SMTP Host Name', 'The host name of your smtp server.', 'text', '', 'mail.fastproject.co', '', 0, 1, 'email', 976),
('mail_smtp_pass', 'SMTP password', 'SMTP password.', 'password', '', 'y#hsPaQ2GyD8', '', 0, 1, 'email', 975),
('mail_smtp_port', 'SMTP Port', 'SMTP port number.', 'text', '', '25', '', 0, 1, 'email', 974),
('mail_smtp_user', 'SMTP User Name', 'SMTP user name.', 'text', '', 'info@fastproject.co', '', 0, 1, 'email', 973),
('meta_topic', 'Meta Topic', 'Two or three words describing this type of company/website.', 'text', 'Content Management', 'MILLONARIOS FC', '', 0, 1, '', 998),
('moderate_comments', 'Moderate Comments', 'Force comments to be approved before they appear on the site.', 'radio', '1', '1', '1=Enabled|0=Disabled', 1, 1, 'comments', 967),
('profile_visibility', 'Profile Visibility', 'Specify who can view user profiles on the public site', 'select', 'public', '', 'public=profile_public|owner=profile_owner|hidden=profile_hidden|member=profile_member', 0, 1, 'users', 960),
('records_per_page', 'Records Per Page', 'How many records should we show per page in the admin section?', 'select', '25', '', '10=10|25=25|50=50|100=100', 1, 1, '', 992),
('registered_email', 'User Registered Email', 'Send a notification email to the contact e-mail when someone registers.', 'radio', '1', '', '1=Enabled|0=Disabled', 0, 1, 'users', 962),
('rss_feed_items', 'Feed item count', 'How many items should we show in RSS/blog feeds?', 'select', '25', '', '10=10|25=25|50=50|100=100', 1, 1, '', 991),
('server_email', 'Server E-mail', 'All e-mails to users will come from this e-mail address.', 'text', 'admin@localhost', 'info@fastproject.co', '', 1, 1, 'email', 978),
('site_lang', 'Site Language', 'The native language of the website, used to choose templates of e-mail notifications, contact form, and other features that should not depend on the language of a user.', 'select', 'en', 'es', 'func:get_supported_lang', 1, 1, '', 997),
('site_name', 'Site Name', 'The name of the website for page titles and for use around the site.', 'text', 'FireSale Pro', 'MILLONARIOS FC', '', 1, 1, '', 1000),
('site_public_lang', 'Public Languages', 'Which are the languages really supported and offered on the front-end of your website?', 'checkbox', 'en', 'es', 'func:get_supported_lang', 1, 1, '', 996),
('site_slogan', 'Site Slogan', 'The slogan of the website for page titles and for use around the site', 'text', '', 'Tienda Online', '', 0, 1, '', 999),
('unavailable_message', 'Unavailable Message', 'When the site is turned off or there is a major problem, this message will show to users.', 'textarea', 'Sorry, this website is currently unavailable.', '', '', 0, 1, '', 987);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_theme_options`
--

CREATE TABLE IF NOT EXISTS `default_theme_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` set('text','textarea','password','select','select-multiple','radio','checkbox','colour-picker') COLLATE utf8_unicode_ci NOT NULL,
  `default` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `options` text COLLATE utf8_unicode_ci NOT NULL,
  `is_required` int(1) NOT NULL,
  `theme` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=92 ;

--
-- Volcado de datos para la tabla `default_theme_options`
--

INSERT INTO `default_theme_options` (`id`, `slug`, `title`, `description`, `type`, `default`, `value`, `options`, `is_required`, `theme`) VALUES
(1, 'show_breadcrumbs', 'Do you want to show breadcrumbs?', 'Shows breadcrumbs at the top of the page.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'firesale'),
(2, 'slider_width', 'Slider Width', 'Homepage slider full width, narrow or none.', 'select', 'full', 'full', 'full=Full|narrow=Narrow|none=None', 1, 'firesale'),
(3, 'sticky_nav', 'Sticky Navigation', 'Stick the navigation to the top of the page when scrolling down the page. (Only for wide screens)', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'firesale'),
(4, 'modal_buttons', 'Modal Buttons', 'Login and Register buttons & modal windows below the search form in the header.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'firesale'),
(5, 'cart_action', 'Cart Action', 'Cart action: modal window or link to cart page.', 'radio', 'modal', 'modal', 'modal=Modal|link=Link', 1, 'firesale'),
(6, 'contact_address', 'Contact Address', 'Contact address is shown in the footer and on the contact page. This is also used for Google maps if enabled.', 'text', 'Newcastle upon Tyne, Tyne and Wear NE1', 'Cra 18a #90-13 ofi 301', '', 0, 'firesale'),
(7, 'contact_phone', 'Contact Phone Number', 'Contact phone number is shown in the footer and on the contact page.', 'text', '+44 (0123) 1234567', '+ 57 662 27 71', '', 0, 'firesale'),
(8, 'google_map', 'Google Maps', 'Display Google map on contact page. Based on contact address if provided.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 0, 'firesale'),
(9, 'twitter_account', 'Twitter Account', 'Your Twitter account name.', 'text', 'getfiresale', 'getfiresale', '', 0, 'firesale'),
(10, 'facebook_account', 'Facebook Account', 'Your Facebook account name.', 'text', 'pages/FireSale/538201042898530?fref=ts', 'pages/FireSale/538201042898530?fref=ts', '', 0, 'firesale'),
(11, 'google_account', 'Google+ Account', 'Your Google+ account name', 'text', '', '', '', 0, 'firesale'),
(12, 'linkedin_account', 'Linkedin Account', 'Your Linkedin account name.', 'text', 'company/3113165?trk=tyah', 'company/3113165?trk=tyah', '', 0, 'firesale'),
(13, 'github_account', 'Github Account', 'Your Github account name.', 'text', 'firesale', 'firesale', '', 0, 'firesale'),
(14, 'add_this_pubid', 'addthis.com Public ID', 'Insert your addthis.com public id here for product pages.', 'text', 'ra-5199e76e29cb32ec', 'ra-5199e76e29cb32ec', '', 0, 'firesale'),
(15, 'add_this_html', 'addthis.com Share HTML', 'Insert your html from addthis.com for product pages.', 'textarea', '<div class="addthis_toolbox addthis_default_style addthis_32x32_style">\r\n<a class="addthis_button_preferred_1"></a>\r\n<a class="addthis_button_preferred_2"></a>\r\n<a class="addthis_button_preferred_3"></a>\r\n<a class="addthis_button_compact"></a>\r\n</div>', '<div class="addthis_toolbox addthis_default_style addthis_32x32_style">\n<a class="addthis_button_preferred_1"></a>\n<a class="addthis_button_preferred_2"></a>\n<a class="addthis_button_preferred_3"></a>\n<a class="addthis_button_compact"></a>\n</div>', '', 0, 'firesale'),
(16, 'add_this_track', 'addthis.com Tracking', 'Track when users share by copying your URL.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 0, 'firesale'),
(17, 'google_maps_type', 'Google Maps Type', 'Changes the type of google map used throughout the site (contact page and user profile pages).', 'select', 'ROADMAP', 'ROADMAP', 'ROADMAP=Roadmap|TERRAIN=Terrain|SATELLITE=Satellite|HYBRID=Hybrid', 0, 'firesale'),
(18, 'primary_color', 'Primary Site Color', '6 digit hex code with or without #', 'text', '#FB9700', '#FB9700', '', 1, 'firesale'),
(19, 'secondary_color', 'Secondary Site Color', '6 digit hex code with or without #', 'text', '#FFBF00', '#FFBF00', '', 1, 'firesale'),
(20, 'open_graph', 'Enable Open Graph', 'Open Graph metadata for all pages. Products & user profiles have additional properties.', 'select', 'yes', 'yes', 'no=No|yes=Yes', 1, 'firesale'),
(21, 'consumer_key', 'Consumer Key', 'Twitter App Consumer Key', 'text', '', '', '', 0, 'firesale'),
(22, 'consumer_secret', 'Consumer Secret', 'Twitter App Consumer Secret', 'text', '', '', '', 0, 'firesale'),
(23, 'oauth_token', 'oauth Token', 'Twitter App oauth Token', 'text', '', '', '', 0, 'firesale'),
(24, 'oauth_token_secret', 'oauth Token Secret', 'Twitter App oauth Token Secret', 'text', '', '', '', 0, 'firesale'),
(25, 'ios_start_image', 'IOS Web App', 'Toggle on or off start up image for IOS devices', 'select', 'yes', 'yes', 'no=No|yes=Yes', 0, 'firesale'),
(26, 'ios_bar_color', 'IOS Status Bar Color', 'Switch the IOS status bar color', 'select', 'black', 'black', 'default=Default|black=Black|black-translucent=Black Translucent', 0, 'firesale'),
(27, 'pyrocms_recent_comments', 'Recent Comments', 'Would you like to display recent comments on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'pyrocms'),
(28, 'pyrocms_news_feed', 'News Feed', 'Would you like to display the news feed on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'pyrocms'),
(29, 'pyrocms_quick_links', 'Quick Links', 'Would you like to display quick links on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'pyrocms'),
(30, 'pyrocms_analytics_graph', 'Analytics Graph', 'Would you like to display the graph on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'pyrocms'),
(31, 'show_breadcrumbs', 'Show Breadcrumbs', 'Would you like to display breadcrumbs?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'default'),
(32, 'layout', 'Layout', 'Which type of layout shall we use?', 'select', '2 column', '2 column', '2 column=Two Column|full-width=Full Width|full-width-home=Full Width Home Page', 1, 'default'),
(33, 'background', 'Background', 'Choose the default background for the theme.', 'select', 'fabric', 'fabric', 'black=Black|fabric=Fabric|graph=Graph|leather=Leather|noise=Noise|texture=Texture', 1, 'base'),
(34, 'slider', 'Slider', 'Would you like to display the slider on the homepage?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'base'),
(35, 'color', 'Default Theme Color', 'This changes things like background color, link colors etc…', 'select', 'pink', 'pink', 'red=Red|orange=Orange|yellow=Yellow|green=Green|blue=Blue|pink=Pink', 1, 'base'),
(36, 'show_breadcrumbs', 'Do you want to show breadcrumbs?', 'If selected it shows a string of breadcrumbs at the top of the page.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'base'),
(37, 'show_breadcrumbs', 'Do you want to show breadcrumbs?', 'Shows breadcrumbs at the top of the page.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'sportsgear'),
(38, 'slider_width', 'Slider Width', 'Homepage slider full width, narrow or none.', 'select', 'full', 'full', 'full=Full|narrow=Narrow|none=None', 1, 'sportsgear'),
(39, 'sticky_nav', 'Sticky Navigation', 'Stick the navigation to the top of the page when scrolling down the page. (Only for wide screens)', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'sportsgear'),
(40, 'modal_buttons', 'Modal Buttons', 'Login and Register buttons & modal windows below the search form in the header.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'sportsgear'),
(41, 'cart_action', 'Cart Action', 'Cart action: modal window or link to cart page.', 'radio', 'modal', 'modal', 'modal=Modal|link=Link', 1, 'sportsgear'),
(42, 'contact_address', 'Contact Address', 'Contact address is shown in the footer and on the contact page. This is also used for Google maps if enabled.', 'text', 'Newcastle upon Tyne, Tyne and Wear NE1', 'Newcastle upon Tyne, Tyne and Wear NE1', '', 0, 'sportsgear'),
(43, 'contact_phone', 'Contact Phone Number', 'Contact phone number is shown in the footer and on the contact page.', 'text', '+44 (0123) 1234567', '+44 (0123) 1234567', '', 0, 'sportsgear'),
(44, 'google_map', 'Google Maps', 'Display Google map on contact page. Based on contact address if provided.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 0, 'sportsgear'),
(45, 'twitter_account', 'Twitter Account', 'Your Twitter account name.', 'text', 'getfiresale', 'getfiresale', '', 0, 'sportsgear'),
(46, 'facebook_account', 'Facebook Account', 'Your Facebook account name.', 'text', 'pages/FireSale/538201042898530?fref=ts', 'pages/FireSale/538201042898530?fref=ts', '', 0, 'sportsgear'),
(47, 'google_account', 'Google+ Account', 'Your Google+ account name', 'text', 'firesale', 'firesale', '', 0, 'sportsgear'),
(48, 'linkedin_account', 'Linkedin Account', 'Your Linkedin account name.', 'text', 'company/3113165?trk=tyah', 'company/3113165?trk=tyah', '', 0, 'sportsgear'),
(49, 'github_account', 'Github Account', 'Your Github account name.', 'text', 'firesale', 'firesale', '', 0, 'sportsgear'),
(50, 'add_this_pubid', 'addthis.com Public ID', 'Insert your addthis.com public id here for product pages.', 'text', 'ra-5199e76e29cb32ec', 'ra-5199e76e29cb32ec', '', 0, 'sportsgear'),
(51, 'add_this_html', 'addthis.com Share HTML', 'Insert your html from addthis.com for product pages.', 'textarea', '<div class="addthis_toolbox addthis_default_style addthis_32x32_style">\r\n<a class="addthis_button_preferred_1"></a>\r\n<a class="addthis_button_preferred_2"></a>\r\n<a class="addthis_button_preferred_3"></a>\r\n<a class="addthis_button_compact"></a>\r\n</div>', '<div class="addthis_toolbox addthis_default_style addthis_32x32_style">\r\n<a class="addthis_button_preferred_1"></a>\r\n<a class="addthis_button_preferred_2"></a>\r\n<a class="addthis_button_preferred_3"></a>\r\n<a class="addthis_button_compact"></a>\r\n</div>', '', 0, 'sportsgear'),
(52, 'add_this_track', 'addthis.com Tracking', 'Track when users share by copying your URL.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 0, 'sportsgear'),
(53, 'google_maps_type', 'Google Maps Type', 'Changes the type of google map used throughout the site (contact page and user profile pages).', 'select', 'ROADMAP', 'ROADMAP', 'ROADMAP=Roadmap|TERRAIN=Terrain|SATELLITE=Satellite|HYBRID=Hybrid', 0, 'sportsgear'),
(54, 'primary_color', 'Primary Site Color', '6 digit hex code with or without #', 'text', '#FB9700', '#FB9700', '', 1, 'sportsgear'),
(55, 'secondary_color', 'Secondary Site Color', '6 digit hex code with or without #', 'text', '#FFBF00', '#FFBF00', '', 1, 'sportsgear'),
(56, 'open_graph', 'Enable Open Graph', 'Open Graph metadata for all pages. Products & user profiles have additional properties.', 'select', 'yes', 'yes', 'no=No|yes=Yes', 1, 'sportsgear'),
(57, 'consumer_key', 'Consumer Key', 'Twitter App Consumer Key', 'text', '', '', '', 0, 'sportsgear'),
(58, 'consumer_secret', 'Consumer Secret', 'Twitter App Consumer Secret', 'text', '', '', '', 0, 'sportsgear'),
(59, 'oauth_token', 'oauth Token', 'Twitter App oauth Token', 'text', '', '', '', 0, 'sportsgear'),
(60, 'oauth_token_secret', 'oauth Token Secret', 'Twitter App oauth Token Secret', 'text', '', '', '', 0, 'sportsgear'),
(61, 'ios_start_image', 'IOS Web App', 'Toggle on or off start up image for IOS devices', 'select', 'yes', 'yes', 'no=No|yes=Yes', 0, 'sportsgear'),
(62, 'ios_bar_color', 'IOS Status Bar Color', 'Switch the IOS status bar color', 'select', 'black', 'black', 'default=Default|black=Black|black-translucent=Black Translucent', 0, 'sportsgear'),
(63, 'pyrocms_news_feed', 'News Feed', 'Would you like to display the news feed on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'imaginamos'),
(64, 'pyrocms_quick_links', 'Quick Links', 'Would you like to display quick links on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'imaginamos'),
(65, 'pyrocms_analytics_graph', 'Analytics Graph', 'Would you like to display the graph on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'imaginamos'),
(66, 'show_breadcrumbs', 'Do you want to show breadcrumbs?', 'Shows breadcrumbs at the top of the page.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'ecommerce'),
(67, 'slider_width', 'Slider Width', 'Homepage slider full width, narrow or none.', 'select', 'full', 'full', 'full=Full|narrow=Narrow|none=None', 1, 'ecommerce'),
(68, 'sticky_nav', 'Sticky Navigation', 'Stick the navigation to the top of the page when scrolling down the page. (Only for wide screens)', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'ecommerce'),
(69, 'modal_buttons', 'Modal Buttons', 'Login and Register buttons & modal windows below the search form in the header.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'ecommerce'),
(70, 'cart_action', 'Cart Action', 'Cart action: modal window or link to cart page.', 'radio', 'modal', 'modal', 'modal=Modal|link=Link', 1, 'ecommerce'),
(71, 'contact_address', 'Contact Address', 'Contact address is shown in the footer and on the contact page. This is also used for Google maps if enabled.', 'text', 'Newcastle upon Tyne, Tyne and Wear NE1', 'Newcastle upon Tyne, Tyne and Wear NE1', '', 0, 'ecommerce'),
(72, 'contact_phone', 'Contact Phone Number', 'Contact phone number is shown in the footer and on the contact page.', 'text', '+44 (0123) 1234567', '+44 (0123) 1234567', '', 0, 'ecommerce'),
(73, 'google_map', 'Google Maps', 'Display Google map on contact page. Based on contact address if provided.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 0, 'ecommerce'),
(74, 'twitter_account', 'Twitter Account', 'Your Twitter account name.', 'text', 'getfiresale', 'getfiresale', '', 0, 'ecommerce'),
(75, 'facebook_account', 'Facebook Account', 'Your Facebook account name.', 'text', 'pages/FireSale/538201042898530?fref=ts', 'pages/FireSale/538201042898530?fref=ts', '', 0, 'ecommerce'),
(76, 'google_account', 'Google+ Account', 'Your Google+ account name', 'text', 'firesale', 'firesale', '', 0, 'ecommerce'),
(77, 'linkedin_account', 'Linkedin Account', 'Your Linkedin account name.', 'text', 'company/3113165?trk=tyah', 'company/3113165?trk=tyah', '', 0, 'ecommerce'),
(78, 'github_account', 'Github Account', 'Your Github account name.', 'text', 'firesale', 'firesale', '', 0, 'ecommerce'),
(79, 'add_this_pubid', 'addthis.com Public ID', 'Insert your addthis.com public id here for product pages.', 'text', 'ra-5199e76e29cb32ec', 'ra-5199e76e29cb32ec', '', 0, 'ecommerce'),
(80, 'add_this_html', 'addthis.com Share HTML', 'Insert your html from addthis.com for product pages.', 'textarea', '<div class="addthis_toolbox addthis_default_style addthis_32x32_style">\r\n<a class="addthis_button_preferred_1"></a>\r\n<a class="addthis_button_preferred_2"></a>\r\n<a class="addthis_button_preferred_3"></a>\r\n<a class="addthis_button_compact"></a>\r\n</div>', '<div class="addthis_toolbox addthis_default_style addthis_32x32_style">\r\n<a class="addthis_button_preferred_1"></a>\r\n<a class="addthis_button_preferred_2"></a>\r\n<a class="addthis_button_preferred_3"></a>\r\n<a class="addthis_button_compact"></a>\r\n</div>', '', 0, 'ecommerce'),
(81, 'add_this_track', 'addthis.com Tracking', 'Track when users share by copying your URL.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 0, 'ecommerce'),
(82, 'google_maps_type', 'Google Maps Type', 'Changes the type of google map used throughout the site (contact page and user profile pages).', 'select', 'ROADMAP', 'ROADMAP', 'ROADMAP=Roadmap|TERRAIN=Terrain|SATELLITE=Satellite|HYBRID=Hybrid', 0, 'ecommerce'),
(83, 'primary_color', 'Primary Site Color', '6 digit hex code with or without #', 'text', '#FB9700', '#FB9700', '', 1, 'ecommerce'),
(84, 'secondary_color', 'Secondary Site Color', '6 digit hex code with or without #', 'text', '#FFBF00', '#FFBF00', '', 1, 'ecommerce'),
(85, 'open_graph', 'Enable Open Graph', 'Open Graph metadata for all pages. Products & user profiles have additional properties.', 'select', 'yes', 'yes', 'no=No|yes=Yes', 1, 'ecommerce'),
(86, 'consumer_key', 'Consumer Key', 'Twitter App Consumer Key', 'text', '', '', '', 0, 'ecommerce'),
(87, 'consumer_secret', 'Consumer Secret', 'Twitter App Consumer Secret', 'text', '', '', '', 0, 'ecommerce'),
(88, 'oauth_token', 'oauth Token', 'Twitter App oauth Token', 'text', '', '', '', 0, 'ecommerce'),
(89, 'oauth_token_secret', 'oauth Token Secret', 'Twitter App oauth Token Secret', 'text', '', '', '', 0, 'ecommerce'),
(90, 'ios_start_image', 'IOS Web App', 'Toggle on or off start up image for IOS devices', 'select', 'yes', 'yes', 'no=No|yes=Yes', 0, 'ecommerce'),
(91, 'ios_bar_color', 'IOS Status Bar Color', 'Switch the IOS status bar color', 'select', 'black', 'black', 'default=Default|black=Black|black-translucent=Black Translucent', 0, 'ecommerce');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_users`
--

CREATE TABLE IF NOT EXISTS `default_users` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salt` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Registered User Information' AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `default_users`
--

INSERT INTO `default_users` (`id`, `email`, `password`, `salt`, `group_id`, `ip_address`, `active`, `activation_code`, `created_on`, `last_login`, `username`, `forgotten_password_code`, `remember_code`) VALUES
(10, 'luis.salazar@imagina.co', 'b0ea9620d1e1ea77668cf818858929700755d5fb', '17b076', 1, '190.7.131.42', 1, NULL, 1394567200, 1429727569, 'luis', '1303d9f9eddf929053e68eb319ab00347917fa83', 'd6b435b564e3bebe6fdc14bc5b7472d2e65cb62e'),
(13, 'demo@imaginamos.co', '159887af5a77bf5c82cf3860c8088e6887e291d7', '6a3286', 3, '127.0.0.1', 1, NULL, 1399652702, 1429746320, 'demo', NULL, '736779870df071a6ab78fb4783fdb9f263a695c8'),
(17, 'mariaoffc@hotmail.com', '0c0ebbebfd2f6bcbc986b223ceffa9bee8916e68', '3a6770', 2, '186.30.12.150', 0, '175ae13387e2be487e64b5c8e8a5a3e62e301537', 1428552860, 1428552860, 'maria.pedroza', NULL, NULL),
(18, 'lilianaartun@gmail.com', '67bc1a216f3abf8e0e326f6cfaeff7a5a9b8e6f8', 'a5bea0', 2, '190.159.146.87', 1, '', 1428647374, 1428724577, 'liliana_.artunduaga', NULL, NULL),
(20, 'jorge.clavijo@imaginamos.com', 'a8de8c558b66bacb735d7e4173e3e2872cf6eb52', '45d943', 2, '181.135.200.248', 1, '', 1428993281, 1429063797, 'jorge.clavijo', '0', NULL),
(21, 'fabian.riascos@imagina.co', '1b6f3fe84c1cdad66f8df2b1c28268675eb9b799', '1fbd25', 1, '181.130.80.205', 1, 'ddf9ba40e7403e220fba171862c94c4df3ac0509', 1429086053, 1429124591, 'fabian.andres', '185b50751a6c947371205c3dba31c9b31e3d33e3', NULL),
(23, 'luisfer7192@hotmail.com', '9a72305229f0971e6aa388152fbd73e0a5bb3f08', '0043a1', 2, '181.58.168.110', 0, 'a0eb0a5b42ad47d38158291b7eff385d36bad4af', 1429124185, 1429124185, 'luis.salazar', NULL, NULL),
(24, 'coordinadortiendas@millonarios.com.co', 'cb502c3ed5f46b5f3f55686f86166d3ba609e948', '5df635', 2, '190.145.2.43', 0, 'ff78dacb913be83f2fd67f1f4f761c6a2beb0a6e', 1429564461, 1429564461, 'maria_.pedroza', NULL, NULL),
(25, 'm91levesi@hotmail.com', '11288e186f17928009e17130346b61d68f22581e', 'fd2562', 2, '190.145.2.43', 1, '', 1429564991, 1429566063, 'leyner.vente', '0', '2322f57ed364f954799b54cc0226dec1cb13029c');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_variables`
--

CREATE TABLE IF NOT EXISTS `default_variables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_variables`
--

INSERT INTO `default_variables` (`id`, `name`, `data`) VALUES
(1, 'chat_server_url', 'http://localhost/demo-tienda-online/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_widgets`
--

CREATE TABLE IF NOT EXISTS `default_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `version` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `enabled` int(1) NOT NULL DEFAULT '1',
  `order` int(10) NOT NULL DEFAULT '0',
  `updated_on` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `default_widgets`
--

INSERT INTO `default_widgets` (`id`, `slug`, `title`, `description`, `author`, `website`, `version`, `enabled`, `order`, `updated_on`) VALUES
(1, 'google_maps', 'a:10:{s:2:"en";s:11:"Google Maps";s:2:"el";s:19:"Χάρτης Google";s:2:"nl";s:11:"Google Maps";s:2:"br";s:11:"Google Maps";s:2:"pt";s:11:"Google Maps";s:2:"ru";s:17:"Карты Google";s:2:"id";s:11:"Google Maps";s:2:"fi";s:11:"Google Maps";s:2:"fr";s:11:"Google Maps";s:2:"fa";s:17:"نقشه گوگل";}', 'a:10:{s:2:"en";s:32:"Display Google Maps on your site";s:2:"el";s:78:"Προβάλετε έναν Χάρτη Google στον ιστότοπό σας";s:2:"nl";s:27:"Toon Google Maps in uw site";s:2:"br";s:34:"Mostra mapas do Google no seu site";s:2:"pt";s:34:"Mostra mapas do Google no seu site";s:2:"ru";s:80:"Выводит карты Google на страницах вашего сайта";s:2:"id";s:37:"Menampilkan Google Maps di Situs Anda";s:2:"fi";s:39:"Näytä Google Maps kartta sivustollasi";s:2:"fr";s:42:"Publiez un plan Google Maps sur votre site";s:2:"fa";s:59:"نمایش داده نقشه گوگل بر روی سایت ";}', 'Gregory Athons', 'http://www.gregathons.com', '1.0.0', 1, 1, 1422410218),
(2, 'html', 's:4:"HTML";', 'a:10:{s:2:"en";s:28:"Create blocks of custom HTML";s:2:"el";s:80:"Δημιουργήστε περιοχές με δικό σας κώδικα HTML";s:2:"br";s:41:"Permite criar blocos de HTML customizados";s:2:"pt";s:41:"Permite criar blocos de HTML customizados";s:2:"nl";s:30:"Maak blokken met maatwerk HTML";s:2:"ru";s:83:"Создание HTML-блоков с произвольным содержимым";s:2:"id";s:24:"Membuat blok HTML apapun";s:2:"fi";s:32:"Luo lohkoja omasta HTML koodista";s:2:"fr";s:36:"Créez des blocs HTML personnalisés";s:2:"fa";s:58:"ایجاد قسمت ها به صورت اچ تی ام ال";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.0.0', 1, 2, 1422410218),
(3, 'login', 'a:10:{s:2:"en";s:5:"Login";s:2:"el";s:14:"Σύνδεση";s:2:"nl";s:5:"Login";s:2:"br";s:5:"Login";s:2:"pt";s:5:"Login";s:2:"ru";s:22:"Вход на сайт";s:2:"id";s:5:"Login";s:2:"fi";s:13:"Kirjautuminen";s:2:"fr";s:9:"Connexion";s:2:"fa";s:10:"لاگین";}', 'a:10:{s:2:"en";s:36:"Display a simple login form anywhere";s:2:"el";s:96:"Προβάλετε μια απλή φόρμα σύνδεσης χρήστη οπουδήποτε";s:2:"br";s:69:"Permite colocar um formulário de login em qualquer lugar do seu site";s:2:"pt";s:69:"Permite colocar um formulário de login em qualquer lugar do seu site";s:2:"nl";s:32:"Toon overal een simpele loginbox";s:2:"ru";s:72:"Выводит простую форму для входа на сайт";s:2:"id";s:32:"Menampilkan form login sederhana";s:2:"fi";s:52:"Näytä yksinkertainen kirjautumislomake missä vain";s:2:"fr";s:54:"Affichez un formulaire de connexion où vous souhaitez";s:2:"fa";s:70:"نمایش یک لاگین ساده در هر قسمتی از سایت";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.0.0', 1, 3, 1422410218),
(4, 'navigation', 'a:10:{s:2:"en";s:10:"Navigation";s:2:"el";s:16:"Πλοήγηση";s:2:"nl";s:9:"Navigatie";s:2:"br";s:11:"Navegação";s:2:"pt";s:11:"Navegação";s:2:"ru";s:18:"Навигация";s:2:"id";s:8:"Navigasi";s:2:"fi";s:10:"Navigaatio";s:2:"fr";s:10:"Navigation";s:2:"fa";s:10:"منوها";}', 'a:10:{s:2:"en";s:40:"Display a navigation group with a widget";s:2:"el";s:100:"Προβάλετε μια ομάδα στοιχείων πλοήγησης στον ιστότοπο";s:2:"nl";s:38:"Toon een navigatiegroep met een widget";s:2:"br";s:62:"Exibe um grupo de links de navegação como widget em seu site";s:2:"pt";s:62:"Exibe um grupo de links de navegação como widget no seu site";s:2:"ru";s:88:"Отображает навигационную группу внутри виджета";s:2:"id";s:44:"Menampilkan grup navigasi menggunakan widget";s:2:"fi";s:37:"Näytä widgetillä navigaatio ryhmä";s:2:"fr";s:47:"Affichez un groupe de navigation dans un widget";s:2:"fa";s:71:"نمایش گروهی از منوها با استفاده از ویجت";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.2.0', 1, 4, 1422410218),
(5, 'rss_feed', 'a:10:{s:2:"en";s:8:"RSS Feed";s:2:"el";s:24:"Τροφοδοσία RSS";s:2:"nl";s:8:"RSS Feed";s:2:"br";s:8:"Feed RSS";s:2:"pt";s:8:"Feed RSS";s:2:"ru";s:31:"Лента новостей RSS";s:2:"id";s:8:"RSS Feed";s:2:"fi";s:10:"RSS Syöte";s:2:"fr";s:8:"Flux RSS";s:2:"fa";s:19:"خبر خوان RSS";}', 'a:10:{s:2:"en";s:41:"Display parsed RSS feeds on your websites";s:2:"el";s:82:"Προβάλετε τα περιεχόμενα μιας τροφοδοσίας RSS";s:2:"nl";s:28:"Toon RSS feeds op uw website";s:2:"br";s:48:"Interpreta e exibe qualquer feed RSS no seu site";s:2:"pt";s:48:"Interpreta e exibe qualquer feed RSS no seu site";s:2:"ru";s:94:"Выводит обработанную ленту новостей на вашем сайте";s:2:"id";s:42:"Menampilkan kutipan RSS feed di situs Anda";s:2:"fi";s:39:"Näytä purettu RSS syöte sivustollasi";s:2:"fr";s:39:"Affichez un flux RSS sur votre site web";s:2:"fa";s:46:"نمایش خوراک های RSS در سایت";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.2.0', 1, 5, 1422410218),
(6, 'social_bookmark', 'a:10:{s:2:"en";s:15:"Social Bookmark";s:2:"el";s:35:"Κοινωνική δικτύωση";s:2:"nl";s:19:"Sociale Bladwijzers";s:2:"br";s:15:"Social Bookmark";s:2:"pt";s:15:"Social Bookmark";s:2:"ru";s:37:"Социальные закладки";s:2:"id";s:15:"Social Bookmark";s:2:"fi";s:24:"Sosiaalinen kirjanmerkki";s:2:"fr";s:13:"Liens sociaux";s:2:"fa";s:52:"بوکمارک های شبکه های اجتماعی";}', 'a:10:{s:2:"en";s:47:"Configurable social bookmark links from AddThis";s:2:"el";s:111:"Παραμετροποιήσιμα στοιχεία κοινωνικής δικτυώσης από το AddThis";s:2:"nl";s:43:"Voeg sociale bladwijzers toe vanuit AddThis";s:2:"br";s:87:"Adiciona links de redes sociais usando o AddThis, podendo fazer algumas configurações";s:2:"pt";s:87:"Adiciona links de redes sociais usando o AddThis, podendo fazer algumas configurações";s:2:"ru";s:90:"Конфигурируемые социальные закладки с сайта AddThis";s:2:"id";s:60:"Tautan social bookmark yang dapat dikonfigurasi dari AddThis";s:2:"fi";s:59:"Konfiguroitava sosiaalinen kirjanmerkki linkit AddThis:stä";s:2:"fr";s:43:"Liens sociaux personnalisables avec AddThis";s:2:"fa";s:71:"تنظیم و نمایش لینک های شبکه های اجتماعی";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.0.0', 1, 6, 1422410218),
(7, 'archive', 'a:8:{s:2:"en";s:7:"Archive";s:2:"br";s:15:"Arquivo do Blog";s:2:"fa";s:10:"آرشیو";s:2:"pt";s:15:"Arquivo do Blog";s:2:"el";s:33:"Αρχείο Ιστολογίου";s:2:"fr";s:16:"Archives du Blog";s:2:"ru";s:10:"Архив";s:2:"id";s:7:"Archive";}', 'a:8:{s:2:"en";s:64:"Display a list of old months with links to posts in those months";s:2:"br";s:95:"Mostra uma lista navegação cronológica contendo o índice dos artigos publicados mensalmente";s:2:"fa";s:101:"نمایش لیست ماه های گذشته به همراه لینک به پست های مربوطه";s:2:"pt";s:95:"Mostra uma lista navegação cronológica contendo o índice dos artigos publicados mensalmente";s:2:"el";s:155:"Προβάλλει μια λίστα μηνών και συνδέσμους σε αναρτήσεις που έγιναν σε κάθε από αυτούς";s:2:"fr";s:95:"Permet d''afficher une liste des mois passés avec des liens vers les posts relatifs à ces mois";s:2:"ru";s:114:"Выводит список по месяцам со ссылками на записи в этих месяцах";s:2:"id";s:63:"Menampilkan daftar bulan beserta tautan post di setiap bulannya";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.0.0', 1, 7, 1422410217),
(8, 'blog_categories', 'a:8:{s:2:"en";s:15:"Blog Categories";s:2:"br";s:18:"Categorias do Blog";s:2:"pt";s:18:"Categorias do Blog";s:2:"el";s:41:"Κατηγορίες Ιστολογίου";s:2:"fr";s:19:"Catégories du Blog";s:2:"ru";s:29:"Категории Блога";s:2:"id";s:12:"Kateori Blog";s:2:"fa";s:28:"مجموعه های بلاگ";}', 'a:8:{s:2:"en";s:30:"Show a list of blog categories";s:2:"br";s:57:"Mostra uma lista de navegação com as categorias do Blog";s:2:"pt";s:57:"Mostra uma lista de navegação com as categorias do Blog";s:2:"el";s:97:"Προβάλει την λίστα των κατηγοριών του ιστολογίου σας";s:2:"fr";s:49:"Permet d''afficher la liste de Catégories du Blog";s:2:"ru";s:57:"Выводит список категорий блога";s:2:"id";s:35:"Menampilkan daftar kategori tulisan";s:2:"fa";s:55:"نمایش لیستی از مجموعه های بلاگ";}', 'Stephen Cozart', 'http://github.com/clip/', '1.0.0', 1, 8, 1422410217),
(9, 'latest_posts', 'a:8:{s:2:"en";s:12:"Latest posts";s:2:"br";s:24:"Artigos recentes do Blog";s:2:"fa";s:26:"آخرین ارسال ها";s:2:"pt";s:24:"Artigos recentes do Blog";s:2:"el";s:62:"Τελευταίες αναρτήσεις ιστολογίου";s:2:"fr";s:17:"Derniers articles";s:2:"ru";s:31:"Последние записи";s:2:"id";s:12:"Post Terbaru";}', 'a:8:{s:2:"en";s:39:"Display latest blog posts with a widget";s:2:"br";s:81:"Mostra uma lista de navegação para abrir os últimos artigos publicados no Blog";s:2:"fa";s:65:"نمایش آخرین پست های وبلاگ در یک ویجت";s:2:"pt";s:81:"Mostra uma lista de navegação para abrir os últimos artigos publicados no Blog";s:2:"el";s:103:"Προβάλει τις πιο πρόσφατες αναρτήσεις στο ιστολόγιό σας";s:2:"fr";s:68:"Permet d''afficher la liste des derniers posts du blog dans un Widget";s:2:"ru";s:100:"Выводит список последних записей блога внутри виджета";s:2:"id";s:51:"Menampilkan posting blog terbaru menggunakan widget";}', 'Erik Berman', 'http://www.nukleo.fr', '1.0.0', 1, 9, 1422410218),
(10, 'firesale_cart', 's:13:"FireSale Cart";', 's:35:"Display the current cart for a user";', 'Jamie Holdroyd', 'http://www.getfiresale.org', '1.2.0', 1, 10, 1422410218),
(11, 'firesale_categories', 's:19:"FireSale Categories";', 's:44:"Display Categories in a structured tree list";', 'Jamie Holdroyd', 'http://www.getfiresale.org', '1.1.0', 1, 11, 1422410218),
(12, 'firesale_products', 's:17:"FireSale Products";', 's:44:"Display Products based on a range of options";', 'Jamie Holdroyd', 'http://www.getfiresale.org', '1.1.0', 1, 12, 1422410218),
(13, 'logo_pagina', 'a:2:{s:2:"en";s:21:"Logo Página (Widget)";s:2:"es";s:22:"Web site Logo (Widget)";}', 'a:2:{s:2:"en";s:0:"";s:2:"es";s:24:"Configuración del logo.";}', 'Luis Fernando Salazar Buitrago', 'www.imaginamos.com', '1.0', 1, 13, 1422410218),
(14, 'footer_contact_data', 'a:2:{s:2:"en";s:19:"footer_contact_data";s:2:"es";s:39:"Datos de contacto en el footer (Widget)";}', 'a:2:{s:2:"en";s:0:"";s:2:"es";s:60:"Configuración de footer datos con el modulo de contactenos.";}', 'Luis Fernando Salazar Buitrago', 'www.imaginamos.com', '1.0', 1, 14, 1422410218),
(15, 'chat', 'a:2:{s:2:"en";s:13:"Chat (Widget)";s:2:"es";s:13:"Chat (Widget)";}', 'a:2:{s:2:"en";s:12:"Support Chat";s:2:"es";s:15:"Chat de soporte";}', 'Luis Fernando Salazar Buitrago', 'www.imaginamos.com', '1.0', 1, 15, 1422410218),
(16, 'phone_header', 'a:2:{s:2:"en";s:19:"Phone number header";s:2:"es";s:32:"Telefono del encabezado (Widget)";}', 'a:2:{s:2:"en";s:19:"Phone number header";s:2:"es";s:23:"Telefono del encabezado";}', 'Luis Fernando Salazar Buitrago', 'www.imaginamos.com', '1.0', 1, 16, 1422410218),
(17, 'cart_header', 'a:2:{s:2:"en";s:20:"Cart Header (Widget)";s:2:"es";s:20:"Cart header (Widget)";}', 'a:2:{s:2:"en";s:20:"Cart Header (Widget)";s:2:"es";s:20:"Cart Header (Widget)";}', 'Luis Fernando Salazar Buitrago', 'www.imaginamos.com', '1.0', 1, 17, 1422410217),
(18, 'new_items', 'a:2:{s:2:"en";s:18:"New Items (Widget)";s:2:"es";s:18:"New Items (Widget)";}', 'a:2:{s:2:"en";s:18:"New Items (Widget)";s:2:"es";s:18:"New Items (Widget)";}', 'Christian España', 'www.imaginamos.com', '1.0', 1, 18, 1422410218),
(19, 'header_contact_data', 'a:2:{s:2:"en";s:19:"header_contact_data";s:2:"es";s:39:"Datos de contacto en el header (Widget)";}', 'a:2:{s:2:"en";s:0:"";s:2:"es";s:60:"Configuración de footer datos con el modulo de contactenos.";}', 'Luis Fernando Salazar Buitrago', 'www.imaginamos.com', '1.0', 1, 19, 1422410217),
(20, 'term_cond_header', 'a:2:{s:2:"en";s:33:"Term and Condicions Link (Widget)";s:2:"es";s:39:"Link de terminos y condiciones (Widget)";}', 'a:2:{s:2:"en";s:0:"";s:2:"es";s:0:"";}', 'Luis Fernando Salazar Buitrago', 'www.imaginamos.com', '1.0', 1, 20, 1429053179);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_widget_areas`
--

CREATE TABLE IF NOT EXISTS `default_widget_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `default_widget_areas`
--

INSERT INTO `default_widget_areas` (`id`, `slug`, `title`) VALUES
(1, 'sidebar', 'Sidebar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `default_widget_instances`
--

CREATE TABLE IF NOT EXISTS `default_widget_instances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `widget_id` int(11) DEFAULT NULL,
  `widget_area_id` int(11) DEFAULT NULL,
  `options` text COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) NOT NULL DEFAULT '0',
  `created_on` int(11) NOT NULL DEFAULT '0',
  `updated_on` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `default_widget_instances`
--

INSERT INTO `default_widget_instances` (`id`, `title`, `widget_id`, `widget_area_id`, `options`, `order`, `created_on`, `updated_on`) VALUES
(1, 'logo', 13, 1, 'a:0:{}', 1, 1396381843, 0),
(2, 'data footer', 14, 1, 'a:0:{}', 2, 1397577294, 0),
(3, 'Cart header', 17, 1, 'a:0:{}', 3, 1416746458, 0),
(4, 'lo mas nuevo', 18, 1, 'a:0:{}', 4, 1416772027, 0),
(5, 'header', 19, 1, 'a:0:{}', 5, 1422410226, 0),
(6, 'term and condicions', 20, 1, 'a:0:{}', 6, 1429053191, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

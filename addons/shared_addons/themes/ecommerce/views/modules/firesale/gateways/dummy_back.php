<div class="horiz-form dummy-gateway">
<?php echo form_open(); ?>
    <div class="control-group">
      <label class="control-label" for="email">Name on Card:</label>
      <div class="controls">
      <?php echo form_input('name', 'Joe Blogs'); ?>
      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="email">Card Type:</label>
      <div class="controls">
      <?php echo form_dropdown('card_type', $default_cards); ?>
      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="email">Card No:</label>
      <div class="controls">
      <?php echo form_input('card_no', '4111111111111111'); ?>
      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="email">Expiry date:</label>
      <div class="controls select-dates">
      <?php echo form_dropdown('exp_month', $months); ?>
      <?php echo form_dropdown('exp_year', $years); ?>
      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="email">CSC:</label>
      <div class="controls">
      <?php echo form_input('csc', '123'); ?>
      </div>
    </div>

    <div class="form_buttons right-text">
    	<button type="submit" class="btn btn-danger btn-large"><span>Process Dummy Payment</span></button>
	</div>
<?php echo form_close();
echo "</div>";
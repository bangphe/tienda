<section class="title">
    <h4>Home</h4>
</section>
<section class="item">
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-pautas"><span>Imagen Fondo Destacados</span></a></li>
                <li><a href="#page-slider"><span>Productos destacados</span></a></li>
                <li><a href="#page-banner"><span>Slider</span></a></li>
                <li><a href="#page-banner_two"><span>Banner</span></a></li>
            </ul>

            <!-- PAUTAS -->

            <div class="form_inputs" id="page-pautas">
                <fieldset>

                    <?php if (count($pautas) < 1): ?>
                        <?php echo anchor('admin/home/nueva_pauta', '<span title="Maximo 3 Pautas">[+3] Crear Pauta</span>', 'class="btn blue"'); ?>
                    <?php endif; ?>


                    <?php if (!empty($pautas)): ?>
                       <div class="scroll-table-wide">
                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 40%">Imagen</th>
                                    <th style="width: 40%">Link</th>
                                    <th style="width: 20%">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($pautas as $pauta): ?>
                                    <tr>
                                        <td>
                                            <?php if (!empty($pauta->imagen)): ?>
                                                <div style="overflow: hidden"><img src="<?php echo $pauta->imagen; ?>" width="300"></div>
                                            <?php endif; ?>
                                        </td>
                                        <td><a href="<?php echo $pauta->link ?>" target="_blank"><?php echo $pauta->link ?></a></td>
                                        <td>
                                            <?php echo anchor('admin/home/editar_pauta/' . $pauta->id, lang('global:edit'), 'class="btn blue small"'); ?>
                                            <!--<?php echo anchor('admin/home/eliminar_pauta/' . $pauta->id, lang('global:delete'), array('class' => 'confirm btn red small')) ?>-->
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                <?php else: ?>
                    <div class="no_data">No hay Pautas actualmente</div >
                    <?php endif ?>
                </fieldset>
            </div>

            <!-- PRODUCTOS DESTACADOS -->

            <div class="form_inputs" id="page-slider">
                <fieldset>
                    <fieldset id="filters" class="four">
                        <legend>Incluir Producto Destacado</legend>
                        <?php if (count($items) < 1) { ?>
                        <?php echo form_open_multipart(site_url('admin/home/crear_destacado/')); ?>
                        <ul>
                        	<li>
                               <label>Productos</label>
                               <select name="producto_id">
                                   <?php foreach ($productos as $producto): ?>
                                    <option value="<?php echo $producto['id'] ?>"><?php echo ucfirst($producto['title']) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </li>
                        <li>
                          <input type="submit" value="Incluir" title="Incluir Máximo 1 Destacados" class="btn blue">
                      </li>
                  </ul>

                  <?php echo form_close(); ?>
                  <?php }else { ?>
                  <small>No es posible agregar más de 6 productos destacados</small>
                  <?php } ?>
              </fieldset>

              <?php if (!empty($items)): ?>
                <div class="scroll-table-wide">
                    <table border="0" class="table-list" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 5%">Orden</th>
                                <th style="width: 25%">Titulo</th>
                                <th style="width: 30%">Descripción</th>
                                <th style="width: 20%">Precio</th>
                                <th style="width: 20%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="sortable">
                            <?php foreach ($items as $item): ?>
                                <tr>
                                    <td>
                                        <?php echo form_open(site_url('admin/home/orden')); ?>
                                        <select name="orden_nuevo" style="width: 50px" onchange="this.form.submit()">
                                            <?php foreach ($items as $orden): ?>
                                                <option value="<?php echo $orden['item_id'] ?>" <?php echo ($orden['orden'] == $item['orden']) ? 'selected' : null ?>><?php echo $orden['orden'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?php echo form_hidden('orden_actual', $item['orden']) ?>
                                        <?php echo form_hidden('id', $item['item_id']) ?>
                                        <?php echo form_close(); ?>
                                    </td>
                                    <td><?php echo $item['titulo'] ?></td>
                                    <td><?php echo $item['descripcion'] ?></td>
                                    <td>$<?php echo number_format($item['precio']) ?></td>
                                    <td>
                                        <?php echo anchor('admin/firesale/products/edit/' . $item['producto_id'], lang('global:edit'), 'class="btn blue small" target="_blank"'); ?>
                                        <?php echo anchor('admin/home/eliminar_destacado/' . $item['item_id'], lang('global:delete'), array('class' => 'btn red small')) ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            <?php else: ?>
                <p style="text-align: center">No hay Destacados actualmente</p>
            <?php endif ?>
        </fieldset>
    </div>
    
		<!-- BANNER (SLIDER) -->

        <div class="form_inputs" id="page-banner">
            <fieldset>
            	<?php 
                if(empty($banner) || count($banner) < 5)
                {
                	echo anchor('admin/home/edit_banner/', '<span>+ Crear Slide</span>', 'class="btn blue"');
                }
                ?>
                <br>
                <br>
                <?php if (!empty($banner)): ?>

                    <table border="0" class="table-list" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 20%">Imagen</th>
                                <th style="width: 20%">Titulo</th>
                                <th style="width: 20%">Texto</th>
                                <th style="width: 20%">Link</th>
                                <th class="width-10">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                    <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>
                                </td>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($banner as $slide): ?>
                                <tr>
                                    <td>
                                        <?php if (!empty($slide->image)): ?>
                                            <img src="<?php echo site_url($slide->image); ?>" style="width: 139px;">
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo $slide->title ?></td>
                                    <td><?php echo $slide->text ?></td>
                                    <td><a href="<?php echo $slide->link ?>"><?php echo $slide->link ?></a></td>
                                    <td>
                                        <?php echo anchor('admin/home/edit_banner/' . $slide->id, lang('global:edit'), 'class="btn blue small"'); ?>
                                        <?php echo anchor('admin/home/delete_banner/' . $slide->id, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                <?php else: ?>
                    <p style="text-align: center">No hay un slider actualmente</p>
                <?php endif ?>
            </fieldset>
        </div>
        
        <!-- BANNER 2-->

        <div class="form_inputs" id="page-banner_two">
            <fieldset>
                <?php 
                if(empty($banner_two) || count($banner_two) < 3)
                {
                    echo anchor('admin/home/edit_banner_two/', '<span>+ Crear Imagen</span>', 'class="btn blue"');
                }
                ?>
                <br>
                <br>
                <?php if (!empty($banner_two)): ?>

                    <table border="0" class="table-list" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 20%">Imagen</th>
                                <th style="width: 20%">Titulo</th>
                                <th style="width: 20%">Texto</th>
                                <th style="width: 20%">Link</th>
                                <th class="width-10">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                    <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>
                                </td>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($banner_two as $slide): ?>
                                <tr>
                                    <td>
                                        <?php if (!empty($slide->image)): ?>
                                            <img src="<?php echo site_url($slide->image); ?>" style="width: 139px;">
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo $slide->title ?></td>
                                    <td><?php echo $slide->text ?></td>
                                    <td><a href="<?php echo $slide->link ?>"><?php echo $slide->link ?></a></td>
                                    <td>
                                        <?php echo anchor('admin/home/edit_banner_two/' . $slide->id, lang('global:edit'), 'class="btn blue small"'); ?>
                                        <?php echo anchor('admin/home/delete_banner_two/' . $slide->id, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                <?php else: ?>
                    <p style="text-align: center">No hay una imagen actualmente</p>
                <?php endif ?>
            </fieldset>
        </div>


	</div>
</div>
</section>
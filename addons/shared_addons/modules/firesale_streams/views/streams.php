    <section class="title">
        <h4><?php echo lang('firesale:sections:streams'); ?></h4>
    </section>

    <section class="item">
        <div class="content">
        <?php if ( ! empty($streams)): ?>
            <?php echo form_open('admin/firesale_streams/delete'); ?>
            	<div class="scroll-table">
                    <table>
                        <thead>
                            <tr>
                                <th style="width: 15px"><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
                                <th><?php echo lang('firesale:label_name'); ?></th>
                                <th><?php echo lang('firesale:label_slug'); ?></th>
                                <th><?php echo lang('firesale:label_namespace'); ?></th>
                                <th style="width: 225px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($streams as $stream): ?>
                                <tr>
                                    <td><input type="checkbox" name="action_to[]" value="<?php echo $stream['stream_namespace']; ?>" /></td>
                                    <td><?php echo $stream['stream_name']; ?></td>
                                    <td><?php echo $stream['stream_slug']; ?></td>
                                    <td><?php echo $stream['stream_namespace']; ?></td>
                                    <td style="text-align:right">
                                        <?php echo anchor('admin/firesale_streams/fields/' . $stream['id'], lang('firesale:button:fields'), 'class="btn blue small"'); ?>
                                        <?php echo anchor('admin/firesale_streams/delete/' . $stream['stream_namespace'], lang('global:delete'), array('class' => 'btn red small')); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
				</div>
                <div class="table_action_buttons">
                    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )); ?>
                </div>
            <?php echo form_close(); ?>
            <?php else: ?>
                <div class="no_data"><?php echo lang('firesale:brands:none'); ?></div>
            <?php endif;?>
        </div>
    </section>

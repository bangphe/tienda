
<div class="bkg-delta">
    <div class="inner-box">
        <strong class="main-font box-title"><?php echo $datos->titulo ?></strong>
    </div>
</div>
<div class="bkg-delta">
    <div class="inner-box">
        <!-- IMAGEN -->
        <?php if (!empty($datos->imagen)): ?>
            <div>
                <img src="<?php echo upload_url($datos->imagen) ?>" alt="imagen">
            </div>
        <?php endif; ?>
        <!-- VIDEO -->
        <?php if (!empty($datos->video)): ?>
            <div>
                <?php echo htmlspecialchars_decode($datos->video); ?>
            </div>
            <hr>
        <?php endif; ?>
        <!-- TEXTO -->
        <div>
            <?php echo $datos->texto ?>
        </div>
    </div>
</div>
$(function()
{
	$('#applies_to').change(function()
	{
		if ($(this).val() == '0')
		{
			$('#li-products').slideUp('slow');
			$('#li-categories').slideUp('slow');
		}
		else if ($(this).val() == '1')
		{
			$('#li-categories').slideUp('slow', function()
			{
				$('#li-products').slideDown('slow');
			});
		}
		else if ($(this).val() == '2')
		{
			$('#li-products').slideUp('slow', function()
			{
				$('#li-categories').slideDown('slow');
			});
		}
	});

	$('#toggle_type').click(function()
	{
		if ($(this).html() == '%')
		{
			$('[name="type"]').val('fixed');
			$(this).hide().html(currency).fadeIn('fast');
		}
		else
		{
			$('[name="type"]').val('percentage');
			$(this).hide().html('&#37;').fadeIn('fast');
		}
	});
});
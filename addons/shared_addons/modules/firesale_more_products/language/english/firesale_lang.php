<?php

    // General
    $lang['firesale:recent:title'] = 'Recently Viewed Products';

    // Settings
    $lang['firesale:recent:setting'] = 'Maximum Recent Products';
    $lang['firesale:recent:inst']    = 'The maximum number of recent products to track';

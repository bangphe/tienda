<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reviews_m extends MY_Model
{
    public $selected = FALSE;
    public $results  = 0;

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'firesale_reviews';
    }

    public function max_values()
    {

        // Variables
        $tmp     = $this->get_fields();
        $fields  = array();

        // Format fields
        foreach( $tmp as $field ) {
            // Get max review value
            $data = explode(' : ', $field->field_data['choice_data']);
            $fields[$field->field_slug]          = array();
            $fields[$field->field_slug]['max']   = end($data);
            $fields[$field->field_slug]['title'] = $field->field_name;
        }

        return $fields;
    }

    public function get_fields($id = NULL)
    {

        // Variables
        $return = array();
        $fields = $this->streams->streams->get_assignments('firesale_reviews', 'firesale_reviews');
		
        // Loop
        foreach( $fields as &$field ) {

            if( substr($field->field_slug, 0, 6) == 'rating' ) {

                // Check ID if set
                if( $id == NULL or $field->id == $id ) {

                    // Get field data
                    $field->field_data = unserialize($field->field_data);

                    // Get max review value
                    $data = explode(' : ', $field->field_data['choice_data']);
                    $field->field_max = end($data);

                    // Add to return array
                    $return[] = $field;

                    if( $id != NULL ) {
                        return $field;
                    }

                }

            }
        }

        // Send it back
        return $return;
    }

    public function build_field_data($input)
    {

        // Build ratings
        $ratings = '';
        for( $i = 0; $i <= $input['maximum']; ( $i += 0.5 ) ) {
            $i = number_format($i, 0);
            $ratings .= "{$i} : {$i}\n";
        }

        // Build slug
        if( isset($input['id']) ) {
            // Get the slug
            $data = $this->get_fields($input['id']);
            $slug = $data->field_slug;
        } else {
            // Calculate it
            $no   = count($this->get_fields());
            $slug = 'rating_' . ( $no + 1 );
        }

        // Create filed
        $field = array(
                    'namespace'     => 'firesale_reviews',
                    'assign'        => 'firesale_reviews',
                    'title_column'  => FALSE,
                    'required'      => ( isset($input['is_required']) && $input['is_required'] == 'yes' ? true : false ),
                    'unique'        => FALSE,
                    'field_name'    => $input['title'],
                    'name'          => $input['title'],
                    'field_slug'    => $slug,
                    'slug'          => $slug,
                    'field_type'    => 'choice',
                    'type'          => 'choice',
                    'choice_data'   => trim($ratings),
                    'choice_type'   => 'dropdown',
                    'default_value' => $input['default'],
                    'extras'        => array('choice_data' => trim($ratings), 'choice_type' => 'dropdown', 'default_value' => $input['default'])
                 );

        // Send it back
        return $field;
    }

    public function update_status($review, $status)
    {

        if( $this->db->where('id', $review)->update($this->_table, array('status' => $status)) ) {
            return TRUE;
        }

        return FALSE;
    }

    public function delete_review($review)
    {

        if( $this->db->where('id', $review)->delete($this->_table) ) {
            return TRUE;
        }

        return FALSE;
    }

    public function user_has_reviewed($user, $product)
    {

        $query = $this->db->select('id')->where("created_by = '{$user}' AND product = '{$product}'")->get($this->_table);

        if( $query->num_rows() ) {
            return TRUE;
        }

        return FALSE;
    }

    public function review_count($product)
    {

        $count = $this->db->select('id')
                          ->where("product = '{$product}' AND status = '1'")
                          ->from($this->_table)
                          ->count_all_results();

        return $count;
    }

    public function review_rating($product)
    {

        // Get ratings
        $all    = 0;
        $max    = 0;
        $fields = $this->max_values();
        $result = $this->db->where("product = '{$product}' AND status = 1")
                           ->get($this->_table);

        // Check for reviews
        if( $result->num_rows() > 0 ) {

            $result = $result->result_array();

            foreach( $result as $review ) {

                // Incriment count
                $count++;

                // Loop each field
                foreach( $review as $key => $val ) {
                    if( substr($key, 0, 7) == 'rating_' ) {
                        $all += $val;
                        $max += $fields[$key]['max'];
                    }
                }

            }

            return array(
                    'rating' => number_format(( $all / $count ), 0),
                    'max'    => $max,
                    'width'  => floor( ( $all / $max ) * 100 ).'%');
        }

        return array('rating' => 0, 'max' => 0, 'width' => 0);
    }

    public function assign_values(&$reviews, $fields)
    {

        // Loop found reviews
        foreach( $reviews AS $key => &$entry ) {

            // Assign variables
            $entry['rated']   = 0.0;
            $entry['stars']   = 0.0;
            $entry['ratings'] = array();

            foreach( $entry AS $key => $val ) {
                if( substr($key, 0, 7) == 'rating_' ) {
                    $entry['rated']    += number_format($val['value'], 0);
                    $entry['stars']    += number_format(($fields[$key]['max'] > 0 ? $fields[$key]['max'] : 1), 0);
                    $entry['ratings'][] = array('name'   => $fields[$key]['title'],
                                                'rating' => $val['value'],
                                                'max'    => $fields[$key]['max'],
                                                'width'  => floor( ( $val['value'] / ($fields[$key]['max'] > 0 ? $fields[$key]['max'] : 1) ) * 100 ).'%');
                }
            }

            // Add final, formatted values
            $entry['width']   = floor( ( $entry['rated'] / $entry['stars'] ) * 100 ).'%';
            $entry['created_formatted'] = nice_time($entry['created']);
            $entry['review']  = nl2br(trim($entry['review']));
        }

    } 

}

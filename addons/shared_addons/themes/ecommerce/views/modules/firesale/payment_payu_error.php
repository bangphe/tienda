<?php
$id = $this->uri->segment(3);
$msj = '';
if (!empty($id)) {
    if ($id == 4)
        $msj = 'Transacción rechazada por la entidad.';
    if ($id == 5)
        $msj = 'Transacción declinada por la entidad financiera.';
    if ($id == 10004)
        $msj = 'Error en el Ajuste.';
    if ($id == 'indefinido')
        $msj = 'No hubo un proceso exitoso; podrian haber muchas razones tales como: <br><br>-Fondos Insuficientes<br>-Tarjeta Inválida<br>-Tarjeta Vencida/Restringida<br>-Tarjeta no autorizada para realizar compras por internet<br>Entre otros..';
} else {
    redirect();
}
?>
  <div class="clear"></div>

      <!--Titulo-->

      <div class="tittles marco">

        <h1>PROCESO DE PAGO NO EXITOSO </h1>
        <br>
        <p><?php echo $msj; ?></p>

        <br>
        <h2>Resumen de Pedido</h2>
      </div>

      <!--Fin Titulo-->

      <div class="clear"></div>

      <!--bg Carrito-->      

      <section class="bg_carrito">

          <!--Contenidos Sitio-->

          <section class="cont_produt marco">

            <!--Lista Carrito-->

            <article class="large-8 medium-8 small-12 columns ">

              <div class="caja_borde">

                 <ul class="list_carrito">

                    {{ items }}  

                    <li>
                       <a href="{{ firesale:url route='cart' }}/remove/{{ rowid }}" class="delete_prd" type="">x</a>
                       <div class="large-2 medium-2 small-4 columns">
                         {{ if image }}
                          <a href="{{ if parent }}{{ firesale:url route='product' id=parent }}{{ else }}{{ firesale:url route='product' id=id }}{{ endif }}" title="View {{ name }}"><img src="{{ url:site }}files/large/{{ image }}" alt=" {{ name }}"/></a>
                          {{ else }}
                          <div class="cart-left">{{ theme:image file="no-image.png" alt=title }}</div>
                          {{ endif }}
                       </div>
                       <div class="detalle_prod_carrito large-10 medium-10 small-8 columns">
                           <div class="nom_prod large-6 medium-6 small-12 columns">
                              <h2>{{ name }}</h2>
                              <h4>{{ code }}</h4>
                              <h4> Cantidad: {{ qty }}</h4>
                           </div>
                           <div class="nom_prod2 large-6 medium-6 small-12 columns">
                              {{ if orig_price_formatted }}
                                <span>Precio Original:</span> <s><h1>{{ orig_price_formatted }}</h1></s>
                                <span>Precio de descuento:</span> <h1>{{ price }}</h1>
                              {{ else }}
                                <h1>{{ price }}</h1>
                              {{ endif }}
                              <h3>Subtotal {{ total }}</h3>
                              {{ if options }}
                              <div class="options">
                                  <span class="title">Opciones</span><br>
                                  {{ options }}
                                    <span>{{ title }}  {{ value }}</span><br/>
                                  {{ /options }}
                              </div>
                              {{ endif }}
                           </div>
                       </div>

                        <div class="clear"></div>

                    </li>

                     {{ /items }}  

                </ul>

                   <div class="clear"></div>

              </div>

            </article>

            <!--Fin Lista Carrito-->

            <!--Lista de pago-->

            <article class="large-4 medium-4 small-12 columns padding_l">

              <div class="caja_borde">

                 <ul class="tabla_pagos">

                   <li>

                     <div class="large-8 medium-8 small-12 columns">

                          Cantidad de Artículos

                     </div>

                     <div class="large-4 medium-4 small-12 columns valor">

                          {{ firesale:cart }}

                            {{ count }}

                          {{ /firesale:cart }}

                     </div>

                   </li>

                    <form method="post" action="{{ firesale:url route='cart' }}/update" class="cart-view">

                      <li>

                       <div class="large-8 medium-8 small-12 columns">

                            Sub Total

                       </div>

                       <div class="large-4 medium-4 small-12 columns valor price-ship">

                            {{ total }}

                       </div>

                     </li>

                     <li>

                       <div class="large-8 medium-8 small-12 columns">

                            Iva

                       </div>

                       <div class="large-4 medium-4 small-12 columns valor price-ship">

                            {{ price_tax }}

                       </div>

                     </li>

                     <li>

                       <div class="large-8 medium-8 small-12 columns">

                            Envío

                       </div>

                       <div class="large-4 medium-4 small-12 columns valor price-ship">

                             {{ price_ship }}

                       </div>

                     </li>


                      <div class="valor_comp">

                        <div class="large-6 medium-6 small-12 columns">TOTAL</div>

                        <div class="large-6 medium-6 small-12 columns valor_t">{{ price_total }}</div> 

                      </div>
                     

                      <div class="clear"></div>                    

                      <a href="{{ url:base }}" class="btn_gris">VOLVER A LA TIENDA</a>

                    </form>
                 </ul>

              </div>

            </article>

            <div class="clear"></div>
            <br> 
          </section>

          <!--Fin Contenidos Sitio-->      

      </section>

      <!--Fin bg Carrito-->      
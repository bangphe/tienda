<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Title
$lang['fs_discount_codes:sections:discounts'] = 'Codici sconto';
$lang['fs_discount_codes:add_discount_code'] = 'Aggiungi codice sconto';
$lang['fs_discount_codes:edit_discount_code'] = 'Modifica codice sconto';

// Shortcuts
$lang['fs_discount_codes:shortcuts:add'] = 'Aggiungi codice sconto';

// Field labels & instructions
$lang['fs_discount_codes:code'] 		= 'Codice';
$lang['fs_discount_codes:type'] 		= 'Discount type';
$lang['fs_discount_codes:value'] 		= 'Valore';
$lang['fs_discount_codes:value_info'] 	= 'Quanto vale il codice sconto (percentuale o valore fisso, clicca sul simbolo per cambiare il tipo di valore da inserire)'; // Instructions
$lang['fs_discount_codes:desc'] 		= 'Descrizione';
$lang['fs_discount_codes:applies_to']	= 'Applica a';
$lang['fs_discount_codes:products'] 	= 'Prodotti';
$lang['fs_discount_codes:categories']	= 'Categorie';
$lang['fs_discount_codes:usage'] 		= 'Utilizzo';
$lang['fs_discount_codes:start_date'] 	= 'Data di inizio';
$lang['fs_discount_codes:end_date'] 	= 'Data di fine';
$lang['fs_discount_codes:used'] 		= 'Usato';
$lang['fs_discount_codes:code_used'] 	= 'Codice usato';

// Dropdown values
$lang['fs_discount_codes:everything'] 		= 'Per tutto';
$lang['fs_discount_codes:selected_prods'] 	= 'Per i prodotti selezionati';
$lang['fs_discount_codes:selected_cats'] 	= 'Per le categorie selezionate';
$lang['fs_discount_codes:once'] 			= 'Una volta sola';
$lang['fs_discount_codes:once_user'] 		= 'Una volta per ogni utente';
$lang['fs_discount_codes:multiple'] 		= 'Più volte';
$lang['fs_discount_codes:yes'] 				= 'Si';
$lang['fs_discount_codes:no'] 				= 'No';

// Admin messages
$lang['fs_discount_codes:messages:no_codes'] 			= 'Non ci sono ancora codici sconto da mostrare';
$lang['fs_discount_codes:messages:deleted'] 			= 'Codice sconto cancellato con successo';
$lang['fs_discount_codes:messages:deleted_multiple'] 	= 'I codici sconto selezionati sono stati cancellati con successo';


// Frontend messages
$lang['fs_discount_codes:message:discount_applied'] 	= 'Il codice sconto %s è stato applicato con successo al tuo carrello';
$lang['fs_discount_codes:message:login_required'] 		= 'Devi eseguire il login per utilizzare il codice sconto';
$lang['fs_discount_codes:message:already_used'] 		= 'Hai già utilizzato questo codice durante un acquisto precedente';
$lang['fs_discount_codes:message:invalid'] 				= 'Il codice inserito non risulta essere valido';
$lang['fs_discount_codes:message:already_applied'] 		= 'Hai già applicato un codice sconto al tuo carrello';
$lang['fs_discount_codes:message:code_removed'] 		= 'Codice rimosso con successo';
$lang['fs_discount_codes:message:code_removed_fail'] 	= 'Non è stato rimosso il codice sconto';

# Tienda Online

* [Sitio Web](http://tienda.imaginamos.com)
* [Documentación](http://www.getfiresale.org/documentation)
* Versión: 1.1

## Equipo

* [Brayan Acebo](http://brayanacebo.com)
* Adrian Rodriguez
* Luis Salazar

## Descripción

Es una plataforma de comercio electrónico ligero, extensible diseñado y construido en torno a [ PyroCMS ] ( http://www.pyrocms.com ) . Hemos procuró mantenerlo pequeño pero muy funcional, mientras que por lo que es fácil de usar para que usted y sus clientes , lo que le permite obtener su nueva tienda funcionando en poco tiempo . En el núcleo que hemos incluido todo lo necesario para construir una tienda increíble, y los dos módulos adicionales ( búsqueda y envío) están destinados a ser utilizados no sólo como una plantilla para su desarrollo futuro, pero también es clave para la facilidad de uso por defecto . Estas dos características se quedaron fuera del núcleo como sabemos que son las áreas más susceptibles de ser personalizado por muchos clientes y sentía que era necesario para que puedan ser reemplazados fácilmente .

A través de una versión ligeramente modificada del archivo detalles estándar que se encuentra en cualquier módulo PyroCMS hemos elaborado un marco que permite a la base y los complementos para lucir y sentirse de la misma en la parte de administración mientras se interactúa a la perfección a través de todo el paquete. La idea de la extensibilidad era algo que teníamos desde el día uno y todo lo que hemos hecho ha sido en torno a este principio. Hemos objetivo fue permitir el uso de la base como punto de partida y requerir el menor modificación posible y mover todas las personalizaciones de los add- ons. Mientras que los complementos hacen un montón de la personalización también hemos propusimos hacer tanto de las corrientes centrales habilitados como sea posible , con más de lo mismo que se convirtió con el tiempo , lo que significa que puede modificar y cambiar las opciones de cada zona de fácil del módulo .

## Características

* La creación de productos altamente potente y categoría
* Seguimiento de pedido completo
* Variantes de productos y modificadores
* Panel informativo con más información añadida a través de módulos
* Disponible en 6 (y contando) Idiomas
* Control total sobre el diseño de páginas
* Interfaz simple e intuative
* 15 pasarelas de pago, impulsado por CI-Merchant y fácilmente extendidos
* Gestión de la ruta completa y personalizados URLs
* Múltiples opciones de divisas a través de la API
* Casi totalmente extensible a través de módulos
* Totalmente arroyos potencia para facilitar la personalización
* Arrastrar y soltar, archivos integrado, de carga de imágenes
* Completar con el envío y búsqueda

## Requerimientos

Antes de intentar la instalación asegúrese de que cumple los siguientes requisitos y usted tiene:

* PyroCMS Versión 2.1.5 o mayor
* PHP 5.2 o por encima
* MySQL 5.x

# Instalación
Si usted esta viendo esto es por que tiene un minino de permisos de lectura; siendo así puede clonar o descargar el proyecto e iniciar su propia instalación. Dentro encontrara el .sql con la base de datos en un directorio llamado docs y su configuración la puede hacer en system/cms/config/database.php

Instale por favor la base de datos y busque la tabla default_users y edite el primer registro (brayan.acebo@imaginamos.co) con su correo de imaginamos. La clave por defecto es Imaginamos2014; Asi usted tendrá los accesos de desarrollador.

# Feedback y cuestiones
Si usted encuentra cualquier problema o desea proporcionar comentarios que lo agradeceríamos si ha utilizado el gestor de incidencias GitHub y vamos a tratar de hacer frente a ellos tan pronto como nos sea posible.

# Las solicitudes y jalar

Si usted desea enviar sus correcciones de errores, mejoras y traducciones al proyecto, por favor envíelas a sus respectivos repositorio. Cada uno de los módulos de venta rápida en este repositorio es un subárbol de su repositorio padre. Por favor, póngase en contacto con nosotros si le gustaría tener acceso a cualquiera de estos repositorios.
Colaboradores

¡Gracias a todos los que han ayudado en el camino, lo que contribuye no sólo de código, pero el tiempo, las ideas y el café, sin que esto no habría sido posible.
<?php defined('BASEPATH') or exit('No direct script access allowed');

class Plugin_Firesale_digital extends Plugin
{
	public function __construct()
	{
		//parent::__construct();

		$this->load->model('firesale_digital/digital_m');
	}

	public function can_download()
	{
		$order_item_id = $this->attribute('order_item_id', FALSE);

		return $this->digital_m->can_download($order_item_id);
	}

	public function downloads()
	{
		$order_item_id = $this->attribute('order_item_id', FALSE);

		return $this->digital_m->downloads($order_item_id);
	}

	public function total_downloads()
	{
		$id = $this->attribute('id', FALSE);
		$slug = $this->attribute('slug', FALSE);

		$return = FALSE;

		if ($id) $return = $this->digital_m->total_downloads($id);
		if ($slug AND ! $return) $return = $this->digital_m->total_downloads($slug);

		return $return;
	}
}

<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6 no-js" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7 no-js" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="en"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<!-- html5.js for IE less than 9 -->
<!--[if lt IE 9]>  <script src="assets/js/lib/html5.js"></script>  <![endif]-->
<html>
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta http-equiv="content-language" content="es" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title>Imaginamos.com / e-commerce</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="copyright" content="imaginamos.com" />
  <meta name="date" content="2014" />
  <meta name="author" content="diseño web: imaginamos.com" />
  <meta name="robots" content="All" />
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <link rel="author" type="text/plain" href="humans.txt" />
  <!--Style's-->
  <link href="assets/css/tienda.css" rel="stylesheet" type="text/css" />
  <!--Librerias-->
  <script src="assets/js/lib/modernizr.min.js"></script>
</head>

<body>

<section id="preload">
    <div id="preload-container">
      <div id="dot"></div>
        <div class="step" id="s1"></div>
        <div class="step" id="s2"></div>
        <div class="step" id="s3"></div>
    </div>
</section>

<?php include("nav_responsive.php"); ?>  

<section class="content_all">

  <?php include("header.php"); ?> 

  <!--Contenidos Sitio-->
  <section class="barra_int"></section>
  <section class="cont_home">   

      <?php include("slider.php"); ?>       

      <div class="clear"></div>

      <!--Categorias-->
      <section class="bg_categorias_home">
        <div class="marco">
          <ul class="categorias_hor"> 
            <li class="large-4 medium-4 columns bounceInUp wow" data-wow-duration="1s" data-wow-delay="0.1s">
              <a href="">
                <div class="text_cat">
                  <h3>COLECCIONES</h3>
                  <h2>PARA HOMBRES</h2>
                </div> 
                <img src="assets/images/dest1.jpg" alt="">
              </a>
            </li>
            <li class="large-4 medium-4 columns bounceInUp wow" data-wow-duration="1s" data-wow-delay="0.2s">
              <a href="">
                <div class="text_cat">
                  <h3>COLECCIONES</h3>
                  <h2>PARA MUJERES</h2>
                </div>
                <img src="assets/images/dest2.jpg" alt="">
              </a>
            </li>
            <li class="large-4 medium-4 columns bounceInUp wow" data-wow-duration="1s" data-wow-delay="0.3s">
              <a href="">
                <div class="text_cat">
                  <h3>COLECCIONES</h3>
                  <h2>PARA NIÑOS</h2>
                </div>
                <img src="assets/images/dest3.jpg" alt="">
              </a>
            </li>
          </ul> 
        </div>
      </section>
      <!-- Fin Categorias-->

      <!--Marcas-->
      <section class="marcar_home">
        <ul class="carrusel_marcas">
          <li class="item"><img src="assets/images/marcas/1.png" alt=""></li>
          <li class="item"><img src="assets/images/marcas/2.png" alt=""></li>
          <li class="item"><img src="assets/images/marcas/3.png" alt=""></li>
          <li class="item"><img src="assets/images/marcas/4.png" alt=""></li>
          <li class="item"><img src="assets/images/marcas/5.png" alt=""></li>
        </ul>
      </section>
      <!--Fin Marcas-->

      <!--Productos destacados-->
      <section class="bg_destacados">
        <div class="marco">
          <ul class="destacados"> 
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.1s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <img src="assets/images/productos/1.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a>
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.2s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <span class="etiq_new">NUEVO</span>
                    <img src="assets/images/productos/2.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a>
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.3s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>                    
                    <img src="assets/images/productos/3.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a>
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.4s">
              <a href="">
              <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <span class="etiq_desc">50%<i>dscto</i></span>
                    <img src="assets/images/productos/4.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio precio_ant">$120.000</div>
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>                  
                  </a>
                </div>
              </a>
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.5s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <img src="assets/images/productos/5.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a>
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.6s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <img src="assets/images/productos/6.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a> 
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.7s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <img src="assets/images/productos/7.jpg" alt="">
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a> 
            </li>
            <li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.8s">
              <a href="">
                <div class="marco_prod">
                  <div class="img_prod">
                    <div class="mask modal_all">
                       <a href="#modal_preview" data-effect="mfp-3d-unfold">
                       <div class="btn_blanco">Vista rápida</div>
                       </a>
                    </div>
                    <img src="assets/images/productos/8.jpg" alt="">
                    <span class="etiq_desc">50%<i>dscto</i></span>
                  </div>
                  <div class="color">2 Colores</div>
                  <p>Reloj consectetur sagittis tempor molestie</p>  
                  <div class="precio precio_ant">$120.000</div>
                  <div class="precio">$120.000</div>
                   <div class="clear"></div>
                  <a href="detalle.php" title="">
                  <div class="btn_border">
                    Detalle
                  </div>  
                  </a>
                </div>
              </a> 
            </li>
          </ul> 
        </div>  
      </section>   
      <!--Fim Productos destacados-->

      <div class="clear"></div>

      <!--Promociones-->
      <section class="promociones_home">
        <div class="large-6 medium-12 columns">
           <div class="bg_promo">
              <!--Bg Promocion-->
              <div class="bg_img_promo">
                 <img src="assets/images/bg_promo1.jpg" alt="">
              </div>
              <!--Fin Bg Promocion-->
              <div class="img_prod_promo wow zoomIn" data-wow-duration=".5s" data-wow-delay="0.1s"> 
                <img src="assets/images/produc_dest1.png" alt="">             
              </div>
              <div class="text_promo">
                  <h3 class="wow fadeInDown" data-wow-duration=".5s">DESCUENTO 50%</h3>
                  <h2 class="wow fadeIn" data-wow-duration="1s">SOMBRERO ELIT $75.000</h2>
                  <a href="" class="btn_border flipInX wow" data-wow-delay"0.4s">DETALLE</a>
                  <a href="" class="btn_gris flipInX wow" data-wow-delay="0.5s">COMPRAR</a>
              </div>
           </div>
        </div>
        <div class="large-6 medium-12 columns">
          <div class="bg_promo">
              <!--Bg Promocion-->
              <div class="bg_img_promo">
                 <img src="assets/images/bg_promo2.jpg" alt="">
              </div>
              <!--Fin Bg Promocion-->
              <div class="img_prod_promo wow zoomIn" data-wow-duration=".5s" data-wow-delay="0.1s">
                <img src="assets/images/produc_dest2.png" alt="">             
              </div>
              <div class="text_promo">
                  <h3 class="wow fadeInDown" data-wow-duration=".5s">DESCUENTO 50%</h3>
                  <h2 class="wow fadeIn" data-wow-duration="1s">JEANS IPSUM $75.000</h2>
                  <a href="" class="btn_gris flipInX wow" data-wow-delay="0.4s">COMPRAR</a>
                  <a href="" class="btn_border flipInX wow" data-wow-delay="0.5s">DETALLE</a>
              </div>
           </div>
        </div>
        </div> 
      </section>
      <!--Fin Promociones-->

  </section>
  <!--Fin Contenidos Sitio-->

    <div class="clear"></div>

  <?php include("footer.php"); ?> 

</section><!--Content_all-->


</body>
</html>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* This file is part of FireSale, a PHP based eCommerce system built for
* PyroCMS.
*
* Copyright (c) 2013 Moltin Ltd.
* http://github.com/firesale/firesale
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @package firesale/core
* @author FireSale <support@getfiresale.org>
* @copyright 2013 Moltin Ltd.
* @version dev
* @link http://github.com/firesale/firesale
*
*/

class category extends Public_Controller
{
    /**
     * Contains the maximum number of products to show in the
     * front-end category view, also used for pagination.
     *
     * @var integer Number of products to show per-page
     * @access public
     */
    public $perpage = 15;

    /**
     * Loads the parent constructor and gets an
     * instance of CI. Also loads in the language
     * files and required models to perform any
     * required actions.
     *
     *
     * @return void
     * @access public
     */
    public function __construct()
    {

        parent::__construct();

        // Load libraries
        $this->load->driver('Streams');
        $this->load->library('files/files');
        $this->lang->load('firesale');
        $this->load->model('categories_m');
        $this->load->model('products_m');
        $this->load->model('routes_m');
        $this->load->helper('firesale/general');

        // Assign data object
        $this->data = new stdClass;
		
		$this->template
                //->append_js('module::jquery.min.js')
                ->append_js('module::ajax.js');
		
        // Get perpage option
        $this->perpage = $this->settings->get('firesale_perpage');

    }

    /**
     * Builds the initial Category view for the front-end
     * pages, including the specific category or sub-cats,
     * pagination.
     *
     * @param  string $category Category slug to query
     * @param  string $start    (Optional) Either the pagination page or sale
     * @return void
     * @access public
     */
    public function index()
    {
        // Variables
        $args     = func_get_args();
        $args     = explode('/', str_replace(array('.json', '.xml'), '', implode('/', $args)));
        $start    = ( is_numeric(end($args)) ? array_pop($args) : 0 );
        $category = implode('/', $args);

        // Check category
        if ( ( is_int($category) OR is_numeric($category) ) AND $start == 0 ) {
            $start    = $category;
            $category = NULL;
        }

        // Get cookie data
        $order              = $this->input->cookie('firesale_listing_order');
        $this->data->layout = $this->input->cookie('firesale_listing_style') or 'grid';
        $this->data->order  = get_order(( $order != null ? $order : '1' ));

        // Get category details
        if ($category != NULL) {
            $category = cache('categories_m/get_category', $category);
        }

        // Check category exists
        if ($category != FALSE or $category == NULL) {
            // Build Query
            // se cambia el start por que lo unico que se pone es donde empieza (ejemplo pagina 2, empezaba desde el producto 2) mas no se multiplicaba por la cantidad de paginas q hay en cada uno
            $query = $this->categories_m->_build_query($category)
                          ->order_by( $this->data->order['by'], $this->data->order['dir'])
                          ->limit($this->perpage, ($start-1 < 1 ? 0 : $start-1)*$this->perpage);
                          
            // Variables
            $query     = $query->get_compiled_select();
            $cache_key = md5(BASE_URL.$query);
            $products  = array();

            // Get from cache
            if ( ! $ids = $this->cache->get($cache_key) ) {
                $ids = $this->db->query($query)->result_array();
            }
			// cargamos la libreria para traer los datos del carrito
			$this->load->library('fs_cart');
			$carts_items = $this->fs_cart->contents();
			
            // Loop and get products
            foreach ($ids AS $id) {
                $product                = cache('products_m/get_product', $id['id']);
                $product['description'] = strip_tags($product['description']);
                $product['precio_ant'] = number_format($product['price'] - (($product['price'] * $product['descuento']) / 100));
				
				// revisamos cuales productos estan en el carrito
				foreach ($carts_items as $id => $cart)
				{
					if($cart['id'] == $product['id'])
					{
						$product['incart'] = 'si';
					}
					// revisamos si algun modificador del producto esta en el carrito
					foreach ($product['modifiers'] as $id2 => $modif)
					{
						foreach($modif['variations'] as $id3 => $variations)
						{
							if($cart['id'] == $variations['product']['id'])
							{
								$product['incart'] = 'si';
							}
						}
					}
				}
				
				$products[] = $product;
				
            }

            // Assign pagination
            if ( ! empty($products) ) {

                // Variables
                $cat   = ( isset($category['id']) ? $category['id'] : NULL );
                $url   = str_replace('/{{ slug }}', '', uri('category', $cat));
                $total = cache('categories_m/total_products', $cat);

                // Build pagination
                $this->data->pagination = create_pagination($url, $total, $this->perpage, ( 2 + substr_count($url, '/') ));
                $this->data->pagination['shown'] = count($products);
            }
			
            // Breadcrumbs
            $this->categories_m->build_breadcrumbs($category, $this->template);

            // Assign data
            $this->data->category = $category;
            $this->data->products = reassign_helper_vars($products);
            $this->data->ordering = get_order();
            $this->data->parent   = ( isset($category['parent']['id']) && $category['parent']['id'] > 0 ? $category['parent']['id'] : $category['id'] );
			$this->data->text_newsletter = $this->db->limit(1)->get('newsletter_info')->row();

            // cargamos los modificadores para el buscador
            // Add to query
            $attributes = array( 'modifier_1' =>'color', 'modifier_2' => 'talla', 'price' => 'price');
            $this->data->color_filter = $this->db->select('pm.`id`, pm.`title`, pv.title AS title_var')
                        ->from('firesale_product_modifiers AS pm')
                        ->join('firesale_product_variations AS pv', 'pm.id = pv.parent', 'left')
                        ->where('lower(pm.title)', 'color')
                        ->group_by('pv.title')
                        ->get()
                        ->result_array();

            $this->data->talla_filter = $this->db->select('pm.`id`, pm.`title`, pv.title AS title_var')
                        ->from('firesale_product_modifiers AS pm')
                        ->join('firesale_product_variations AS pv', 'pm.id = pv.parent', 'left')
                        ->where('lower(pm.title)', 'talla')
                        ->group_by('pv.title')
                        ->get()
                        ->result_array();

            $this->data->price_filter = $this->data->min_max = cache('products_m/price_min_max');
            
            // Api request
            if ( api($this->data) ) { return; }

            // Set category in session
            $this->session->set_userdata('category', $this->data->category['id']);

            // Build Page
            $this->template->title(( $category != NULL ? $this->data->category['title'] : lang('firesale:cats_all_products') ))
                           ->set($this->data);

            // Assign accessible information
            $this->template->design = 'category';
            $this->template->id     = $this->data->category['id'];

            // Fire events
            $overload = Events::trigger('page_build', $this->template);
			
			$categoryId = 0;
			if(isset($this->data->category['id']))
            {
                $categoryId = $this->data->category['id'];
            }
            
            if(isset($this->data->category['parent']['id']))
			{
				$categoryId = $this->data->category['parent']['id'];
			}
			
			
            // Build page
            $this->template->set('categoryId', $categoryId)->build(( $overload ? $overload : 'category' ));
            return;
        }

        // Not found
        set_status_header(404);
        echo Modules::run('pages/_remap', '404');
    }
	
	/**
     * Ofertas
     *
     * @return void
     * @access public
     */
    public function offers()
    {
        // Variables
        $args     = func_get_args();
        $args     = explode('/', str_replace(array('.json', '.xml'), '', implode('/', $args)));
        $start    = ( is_numeric(end($args)) ? array_pop($args) : 0 );
        $category = implode('/', $args);

        // Check category
        if ( ( is_int($category) OR is_numeric($category) ) AND $start == 0 ) {
            $start    = $category;
            $category = NULL;
        }

        // Get cookie data
        $order              = $this->input->cookie('firesale_listing_order');
        $this->data->layout = $this->input->cookie('firesale_listing_style') or 'grid';
        $this->data->order  = get_order(( $order != null ? $order : '1' ));

        // Check category exists
        if ($category != FALSE or $category == NULL) {
            // Build Query
            // se cambia el start por que lo unico que se pone es donde empieza (ejemplo pagina 2, empezaba desde el producto 2) mas no se multiplicaba por la cantidad de paginas q hay en cada uno
            $query = $this->categories_m->_build_query($category)
                          ->order_by( $this->data->order['by'], $this->data->order['dir'])
                          ->where('descuento >', 0);
                          //->limit($this->perpage, ($start-1 < 1 ? 0 : $start-1)*$this->perpage);
                          
            // Variables
            $query     = $query->get_compiled_select();
            $cache_key = md5(BASE_URL.$query);
            $products  = array();

            // Get from cache
            if ( ! $ids = $this->cache->get($cache_key) ) {
                $ids = $this->db->query($query)->result_array();
            }
            // cargamos la libreria para traer los datos del carrito
            $this->load->library('fs_cart');
            $carts_items = $this->fs_cart->contents();
      
            // Loop and get products
            foreach ($ids AS $id) {
                $product                = $this->products_m->get_product($id['id']);
                $product['description'] = strip_tags($product['description']);
                $product['precio_ant'] = number_format($product['price'] - (($product['price'] * $product['descuento']) / 100));
        
              // revisamos cuales productos estan en el carrito
              foreach ($carts_items as $id => $cart)
              {
                if($cart['id'] == $product['id'])
                {
                  $product['incart'] = 'si';
                }
                // revisamos si algun modificador del producto esta en el carrito
                foreach ($product['modifiers'] as $id2 => $modif)
                {
                  foreach($modif['variations'] as $id3 => $variations)
                  {
                    if($cart['id'] == $variations['product']['id'])
                    {
                      $product['incart'] = 'si';
                    }
                  }
                }
              }
              
              $products[] = $product;
        
            }

            // Assign pagination
            if ( ! empty($products) ) {

                // Variables
                $cat   = ( isset($category['id']) ? $category['id'] : NULL );
                $url   = str_replace('/{{ slug }}', '', uri('category', $cat));
                $total = cache('categories_m/total_products', $cat);

                // Build pagination
                //$this->data->pagination = create_pagination($url, $total, $this->perpage, ( 2 + substr_count($url, '/') ));
                $this->data->pagination = create_pagination($url, $total, 10000, ( 2 + substr_count($url, '/') ));
                $this->data->pagination['shown'] = count($products);
            }
      
            // Breadcrumbs
            $this->categories_m->build_breadcrumbs_offers($category, $this->template);

            // Assign data
            $this->data->category = $category;
            $this->data->products = reassign_helper_vars($products);
            $this->data->ordering = get_order();
            $this->data->parent   = ( isset($category['parent']['id']) && $category['parent']['id'] > 0 ? $category['parent']['id'] : $category['id'] );
            $this->data->text_newsletter = $this->db->limit(1)->get('newsletter_info')->row();

            /*echo "<pre> ";
            print_r( $this->data->products);
            echo "</pre> ";
            exit();*/

            // cargamos los modificadores para el buscador
            // Add to query
            $attributes = array( 'modifier_1' =>'colores', 'modifier_2' => 'tallas', 'price' => 'price');
            $this->data->color_filter = $this->db->select('pm.`id`, pm.`title`, pv.title AS title_var')
                        ->from('firesale_product_modifiers AS pm')
                        ->join('firesale_product_variations AS pv', 'pm.id = pv.parent', 'left')
                        ->where('lower(pm.title)', 'colores')
                        ->group_by('pv.title')
                        ->get()
                        ->result_array();

            $this->data->talla_filter = $this->db->select('pm.`id`, pm.`title`, pv.title AS title_var')
                        ->from('firesale_product_modifiers AS pm')
                        ->join('firesale_product_variations AS pv', 'pm.id = pv.parent', 'left')
                        ->where('lower(pm.title)', 'tallas')
                        ->group_by('pv.title')
                        ->get()
                        ->result_array();

            $this->data->price_filter = $this->data->min_max = cache('products_m/price_min_max');
            
            // Api request
            if ( api($this->data) ) { return; }

            // Set category in session
            $this->session->set_userdata('category', $this->data->category['id']);

            // Build Page
            $this->template->title(( $category != NULL ? $this->data->category['title'] : lang('firesale:cats_all_products') ))
                           ->set($this->data);

            // Assign accessible information
            $this->template->design = 'category';
            $this->template->id     = $this->data->category['id'];

            // Fire events
            $overload = Events::trigger('page_build', $this->template);
      
            $categoryId = 0;
            if(isset($this->data->category['id']))
            {
              $categoryId = $this->data->category['id'];
            }
            
            if(isset($this->data->category['parent']['id']))
            {
              $categoryId = $this->data->category['parent']['id'];
            }

            // Build page
            $this->template->set('categoryId', $categoryId)->build(( $overload ? $overload : 'category' ));
            return;
        }

        // Not found
        set_status_header(404);
        echo Modules::run('pages/_remap', '404');
    }
	
	public function promotions()
    {
        // Variables
        $args     = func_get_args();
        $args     = explode('/', str_replace(array('.json', '.xml'), '', implode('/', $args)));
        $start    = ( is_numeric(end($args)) ? array_pop($args) : 0 );
        $category = implode('/', $args);

        // Check category
        if ( ( is_int($category) OR is_numeric($category) ) AND $start == 0 ) {
            $start    = $category;
            $category = NULL;
        }

        // Get cookie data
        $order              = $this->input->cookie('firesale_listing_order');
        $this->data->layout = $this->input->cookie('firesale_listing_style') or 'grid';
        $this->data->order  = get_order(( $order != null ? $order : '1' ));

        // Get category details
        if ($category != NULL) {
            $category = cache('categories_m/get_category', $category);
        }

        // Check category exists
        if ($category != FALSE or $category == NULL) {
            // Build Query
            // se cambia el start por que lo unico que se pone es donde empieza (ejemplo pagina 2, empezaba desde el producto 2) mas no se multiplicaba por la cantidad de paginas q hay en cada uno
            $query = $this->categories_m->_build_query_promotions($category)
                          ->order_by( $this->data->order['by'], $this->data->order['dir'])
                          ->limit($this->perpage, ($start-1 < 1 ? 0 : $start-1)*$this->perpage);
                          
            // Variables
            $query     = $query->get_compiled_select();
            $cache_key = md5(BASE_URL.$query);
            $products  = array();

            // Get from cache
            if ( ! $ids = $this->cache->get($cache_key) ) {
                $ids = $this->db->query($query)->result_array();
            }
			// cargamos la libreria para traer los datos del carrito
			$this->load->library('fs_cart');
			$carts_items = $this->fs_cart->contents();
			
            // Loop and get products
            foreach ($ids AS $id) {
                $product                = cache('products_m/get_product', $id['id']);
                $product['description'] = strip_tags($product['description']);
                $product['precio_ant'] = number_format($product['price'] - (($product['price'] * $product['descuento']) / 100));
				
				// revisamos cuales productos estan en el carrito
				foreach ($carts_items as $id => $cart)
				{
					if($cart['id'] == $product['id'])
					{
						$product['incart'] = 'si';
					}
					// revisamos si algun modificador del producto esta en el carrito
					foreach ($product['modifiers'] as $id2 => $modif)
					{
						foreach($modif['variations'] as $id3 => $variations)
						{
							if($cart['id'] == $variations['product']['id'])
							{
								$product['incart'] = 'si';
							}
						}
					}
				}
				
				$products[] = $product;
				
            }

            // Assign pagination
            if ( ! empty($products) ) {

                // Variables
                $cat   = ( isset($category['id']) ? $category['id'] : NULL );
                $url   = str_replace('/{{ slug }}', '', uri('category', $cat));
                $total = cache('categories_m/total_products_promotions', $cat);

                // Build pagination
                $this->data->pagination = create_pagination($url, $total, $this->perpage, ( 2 + substr_count($url, '/') ));
                $this->data->pagination['shown'] = count($products);
            }
			
            // Breadcrumbs
            $this->categories_m->build_breadcrumbs($category, $this->template);

            // Assign data
            $this->data->category = $category;
            $this->data->products = reassign_helper_vars($products);
            $this->data->ordering = get_order();
            $this->data->parent   = ( isset($category['parent']['id']) && $category['parent']['id'] > 0 ? $category['parent']['id'] : $category['id'] );
			$this->data->text_newsletter = $this->db->limit(1)->get('newsletter_info')->row();

            // cargamos los modificadores para el buscador
            // Add to query
            $attributes = array( 'modifier_1' =>'color', 'modifier_2' => 'talla', 'price' => 'price');
            $this->data->color_filter = $this->db->select('pm.`id`, pm.`title`, pv.title AS title_var')
                        ->from('firesale_product_modifiers AS pm')
                        ->join('firesale_product_variations AS pv', 'pm.id = pv.parent', 'left')
                        ->where('lower(pm.title)', 'color')
                        ->group_by('pv.title')
                        ->get()
                        ->result_array();

            $this->data->talla_filter = $this->db->select('pm.`id`, pm.`title`, pv.title AS title_var')
                        ->from('firesale_product_modifiers AS pm')
                        ->join('firesale_product_variations AS pv', 'pm.id = pv.parent', 'left')
                        ->where('lower(pm.title)', 'talla')
                        ->group_by('pv.title')
                        ->get()
                        ->result_array();

            $this->data->price_filter = $this->data->min_max = cache('products_m/price_min_max');
            
            // Api request
            if ( api($this->data) ) { return; }

            // Set category in session
            $this->session->set_userdata('category', $this->data->category['id']);

            // Build Page
            $this->template->title(( $category != NULL ? $this->data->category['title'] : lang('firesale:cats_all_products') ))
                           ->set($this->data);

            // Assign accessible information
            $this->template->design = 'category';
            $this->template->id     = $this->data->category['id'];

            // Fire events
            $overload = Events::trigger('page_build', $this->template);
			
			$categoryId = 0;
			if(isset($this->data->category['id']))
            {
                $categoryId = $this->data->category['id'];
            }
            
            if(isset($this->data->category['parent']['id']))
			{
				$categoryId = $this->data->category['parent']['id'];
			}
			
			
            // Build page
            $this->template->set('categoryId', $categoryId)->build(( $overload ? $overload : 'category' ));
            return;
        }

        // Not found
        set_status_header(404);
        echo Modules::run('pages/_remap', '404');
    }

    /**
     * Sets the listing style cookie with a possible value of grid or layout
     *
     * @param  string $type The layout style to set in the cookie
     * @return void
     * @access public
     */
    public function style($type)
    {
        $cookie = array(
            'name'   => 'listing_style',
            'value'  => $type,
            'expire' => '2592000',
            'path'   => '/',
            'prefix' => 'firesale_',
            'secure' => FALSE
        );

        $this->input->set_cookie($cookie);

        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Sets the listing order cookie with a number of possible values as
     * defined in the get_order helper.
     *
     * @param  integer $type The ID of the ordering method to use
     * @return void
     * @access public
     */
    public function order($type)
    {

        $orders = get_order();

        if ( array_key_exists($type, $orders) ) {
            $cookie = array(
                'name'   => 'listing_order',
                'value'  => $type,
                'expire' => '86500',
                'path'   => '/',
                'prefix' => 'firesale_',
                'secure' => FALSE
            );

            $this->input->set_cookie($cookie);
        }

        redirect($_SERVER['HTTP_REFERER']);
    }
	
		
	public function featured_products()
	{
		// Variables
        $args     = func_get_args();
        $args     = explode('/', str_replace(array('.json', '.xml'), '', implode('/', $args)));
        $start    = ( is_numeric(end($args)) ? array_pop($args) : 0 );
        $category = implode('/', $args);

        // Check category
        if ( ( is_int($category) OR is_numeric($category) ) AND $start == 0 ) {
            $start    = $category;
            $category = NULL;
        }

        // Get cookie data
        $order              = $this->input->cookie('firesale_listing_order');
        $this->data->layout = $this->input->cookie('firesale_listing_style') or 'grid';
        $this->data->order  = get_order(( $order != null ? $order : '1' ));
		// Build Query
		// se cambia el start por que lo unico que se pone es donde empieza (ejemplo pagina 2, empezaba desde el producto 2) mas no se multiplicaba por la cantidad de paginas q hay en cada uno
		$query = $this->categories_m->_build_query_featured()
					  ->limit($this->perpage, ($start-1 < 1 ? 0 : $start-1)*$this->perpage);
					  
		// Variables
		$query     = $query->get_compiled_select();
		$cache_key = md5(BASE_URL.$query);
		$products  = array();

		// Get from cache
		if ( ! $ids = $this->cache->get($cache_key) ) {
			$ids = $this->db->query($query)->result_array();
		}
		// cargamos la libreria para traer los datos del carrito
		$this->load->library('fs_cart');
		$carts_items = $this->fs_cart->contents();
		
		// Loop and get products
		foreach ($ids AS $id) {
			$product                = cache('products_m/get_product', $id['id']);
			$product['description'] = strip_tags($product['description']);
			$product['precio_ant'] = number_format($product['price'] - (($product['price'] * $product['descuento']) / 100));
			
			// revisamos cuales productos estan en el carrito
			foreach ($carts_items as $id => $cart)
			{
				if($cart['id'] == $product['id'])
				{
					$product['incart'] = 'si';
				}
				// revisamos si algun modificador del producto esta en el carrito
				foreach ($product['modifiers'] as $id2 => $modif)
				{
					foreach($modif['variations'] as $id3 => $variations)
					{
						if($cart['id'] == $variations['product']['id'])
						{
							$product['incart'] = 'si';
						}
					}
				}
			}
			
			$products[] = $product;
			
		}

		// Assign pagination
		if ( ! empty($products) ) {

			// Variables
			$cat   = ( isset($category['id']) ? $category['id'] : NULL );
			$url   = str_replace('/{{ slug }}', '', uri('category', $cat));
			$total = cache('categories_m/total_products', $cat);

			// Build pagination
			$this->data->pagination = create_pagination($url, $total, $this->perpage, ( 2 + substr_count($url, '/') ));
			$this->data->pagination['shown'] = count($products);
		}
		
		// Breadcrumbs
		$this->categories_m->build_breadcrumbs($category, $this->template);

		// Assign data
		$this->data->category = $category;
		$this->data->products = reassign_helper_vars($products);
		$this->data->ordering = get_order();
		$this->data->parent   = ( isset($category['parent']['id']) && $category['parent']['id'] > 0 ? $category['parent']['id'] : $category['id'] );
		$this->data->text_newsletter = $this->db->limit(1)->get('newsletter_info')->row();

		// cargamos los modificadores para el buscador
		// Add to query
		$attributes = array( 'modifier_1' =>'color', 'modifier_2' => 'talla', 'price' => 'price');
		$this->data->color_filter = $this->db->select('pm.`id`, pm.`title`, pv.title AS title_var')
					->from('firesale_product_modifiers AS pm')
					->join('firesale_product_variations AS pv', 'pm.id = pv.parent', 'left')
					->where('lower(pm.title)', 'color')
					->group_by('pv.title')
					->get()
					->result_array();

		$this->data->talla_filter = $this->db->select('pm.`id`, pm.`title`, pv.title AS title_var')
					->from('firesale_product_modifiers AS pm')
					->join('firesale_product_variations AS pv', 'pm.id = pv.parent', 'left')
					->where('lower(pm.title)', 'talla')
					->group_by('pv.title')
					->get()
					->result_array();

		$this->data->price_filter = $this->data->min_max = cache('products_m/price_min_max');
		
		// Api request
		if ( api($this->data) ) { return; }

		// Set category in session
		$this->session->set_userdata('category', $this->data->category['id']);

		// Build Page
		$this->template->title(( $category != NULL ? $this->data->category['title'] : lang('firesale:cats_all_products') ))
					   ->set($this->data);

		// Assign accessible information
		$this->template->design = 'category';
		$this->template->id     = $this->data->category['id'];

		// Fire events
		$overload = Events::trigger('page_build', $this->template);
		
		$categoryId = 0;
		if(isset($this->data->category['id']))
		{
			$categoryId = $this->data->category['id'];
		}
		
		if(isset($this->data->category['parent']['id']))
		{
			$categoryId = $this->data->category['parent']['id'];
		}
		
		
		// Build page
		$this->template->set('categoryId', $categoryId)->build(( $overload ? $overload : 'category' ));
	}
}

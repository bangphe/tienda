
		<table>
			<thead>
				<th style="width: 100px"><?php echo lang('firesale:label_id'); ?></th>
				<th><?php echo lang('firesale:label_title'); ?></th>
				<th style="width: 190px; text-align: center"><?php echo lang('firesale:label_remdls'); ?></th>
				<th style="width: 190px; text-align: center"><?php echo lang('firesale:label_availuntil'); ?></th>
				<th style="width: 100px"><?php echo lang('firesale:field:download'); ?></th>
			</thead>
			<tbody>
			{{ downloads }}
				<tr>
					<td>{{ code }}</td>
					<td><a href="{{ firesale:url route="product" id=id }}">{{ title }}</a></td>
				{{ if expired == 0 }}
					<td style="text-align: center">{{ remaining }}</td>
					<td style="text-align: center">{{ helper:date string="D M j G:i:s" time=expires }}</td>
					<td><a href="{{ firesale:url route="downloads-file" id=id }}{{ order_item_id }}" class="button"><?php echo lang('firesale:field:download'); ?></a></td>
				{{ else }}
					<td style="text-align: center">-</td>
					<td style="text-align: center">-</td>
					<td style="text-align: center">-</td>
				{{ endif }}
				</tr>
			{{ /downloads }}
			</tbody>
		</table>

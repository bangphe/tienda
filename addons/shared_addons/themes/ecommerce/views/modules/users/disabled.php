<section class="cont_home">
    <section class="marco">
        <!--Titulo-->
        <div class="subtittle titulo_int animated fadeInDown">
            <h1>Registro de Cuenta</h1>
        </div>
          <div class="clear"></div>
        <section class="cont_nosotros">
                
            <div class="bg_log_v elem_blancos animated fadeInUp">               
                <div class="alert-error"><?php echo $this->lang->line('user:registration_disabled') ?></div>                   
            </div>

            <div class="clear"></div>

            {{ theme:partial name="recent_items" }}

        </section>

    </section><!--Marco-->  
</section>

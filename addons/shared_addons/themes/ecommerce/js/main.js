$(function(){
    $('.headerchat').click(function(){
        if($(this).hasClass('activoChat')){
            $('.collapchat').fadeOut(500);
            $(this).removeClass('activoChat');
        }else{
            $('.collapchat').fadeIn(500);
            $(this).addClass('activoChat');
        }
    })
});

function abrir_chat(){
	if ($('#chat').hasClass('chat_close')){
		document.getElementById('chat').className='client-window';
	}else{
		document.getElementById('chat').className='chat_close';
	}
}

function cerrar_chat(){
	if ($('#chat').hasClass('client-window')){
		document.getElementById('chat').className='chat_close';
	}else{
		document.getElementById('chat').className='client-window';
	}
}
$(function() {
	

	/* Fix Contact Button */

	if (! $('.contact-button input').hasClass('btn')) {
		$('.contact-button input').addClass('btn');
	}

    
    function sidebarIn() {
        $(this).find('.sidebar-text-alt').slideDown();
    }

    function sidebarOut() {
        $(this).find('.sidebar-text-alt').slideUp();     
    }

    /* fix ios app mode links */

    if (("standalone" in window.navigator) && window.navigator.standalone) {
      // For iOS Apps
      $('a').on('click', function(e){
        e.preventDefault();
        var new_location = $(this).attr('href');
        if (new_location != undefined && new_location.substr(0, 1) != '#' && $(this).attr('data-method') == undefined){
            if (e.target != "_blank") {
                window.location = new_location;
            }
        }
      });
    }

    /* Sorting Results */
    $('#listing-sort select').change(function(e) {
        $.cookie('firesale_listing_order', $(this).val());
        window.location.reload();
    });

    /* Homepage */

    /* masonry */
    /*var $container_left = $('#masonry');
    
    $container_left.imagesLoaded( function(){
      $container_left.masonry({
        itemSelector: '.masonry-item',
        isAnimated: true,
        isFitWidth: true,
        gutterWidth: 0,
        columnWidth: 1
      });
    });*/

    $('.masonry-item, .listings .span3').hover(
        function() { $(this).siblings().stop().fadeTo(500, 0.3); },
        function() { $(this).siblings().stop().fadeTo(500, 1); }
    );

    /* twitter popup */
    $('.twitter_popup').click(function(event) {
      var width  = 575,
          height = 400,
          left   = ($(window).width()  - width)  / 2,
          top    = ($(window).height() - height) / 2,
          url    = this.href,
          opts   = 'status=1' +
                   ',width='  + width  +
                   ',height=' + height +
                   ',top='    + top    +
                   ',left='   + left;
      
      window.open(url, 'twitter', opts);
    
      return false;
    });

    /* add to wishlist (homepage) */
    $('.wish_modal').click(function(event) {

        var wishlist_url = $('#wishlistModal').data('wishlist-url');
        var item_id = $(this).data('item-id');

        $('#wishlistModal').find('.well').each(function() {
            var wishlist_id = $(this).data('wishlist-id');
            $(this).find('.wishlist_add_link').attr("href", wishlist_url+"/insert/"+wishlist_id+"/"+item_id);
        });

    });

    /* featured items */

//    $(".featured .item").hoverIntent( featuredIn, featuredOut);

    function featuredIn() {
        $(this).find('.go').fadeIn('fast');
        $(this).find('.text').slideDown();
    }

    function featuredOut() {
        $(this).find('.go').fadeOut('fast');
        $(this).find('.text').slideUp();        
    }

    /* Cart */
 /*   $('.tcart button[type=submit]').live('click', function(e) {
        e.preventDefault();
        var o = $(this).parents('form'), c = $(this).parents('.cart-quantity');
        $.ajax({ url: o.attr('action'), type: 'POST', data: c.find('input').attr('name')+'='+c.find('input').val()+'&btnAction=update', beforeSend: function(xhr) { xhr.setRequestHeader('X-Requested-With', { toString: function() { return ''; } } ); }, success: function(data) {
            alert(data);
            $('.tcart').html($(data).find('.tcart').html());
        } });
    });*/

    setTimeout(function() { $('a[href*="olark"]').parent().remove(); },1250);

	/* checkout */

    // returning customer
    $('#login-toggle').click( function(){
        // $('.login-toggle').toggle();
        if ($(this).html() == "Click here to login") {
            $('.login-toggle').stop().fadeIn('fast');
            $(this).html('Close login form'); 
        } else {
            $(this).html('Click here to login');
            $('.login-toggle').stop().hide();
        }
    });

	// shipping address change

	$("select[name='ship_to']").change( function() {
		if ($(this).val() == "new" ) {
			// show new form
			$('.new_shipping_address').show();
			$('.saved_ship_addresses').find('.show_address').hide().removeClass('show_address');
		} else if ($(this).val() == "same_as_billing" ) {
            // hide new form
            $('.new_shipping_address').hide();
            // show the ul class for this address
            $('.saved_ship_addresses').find('.show_address').hide().removeClass('show_address');
            // show same details
            if ($("select[name='bill_to']").val() != "new") {
                $('.saved_ship_addresses .address_'+$("select[name='bill_to']").val()).show().addClass('show_address');                
            }
        } else {
			// hide new form
			$('.new_shipping_address').hide();
            // hide other saved addresses
            $('.saved_ship_addresses .saved_address').hide().removeClass('show_address');
			// show the saved address
			$('.saved_ship_addresses .address_'+$(this).val()).show().addClass('show_address');
		}
	}).change();

	// billing address change
    $("select[name='bill_to']").change( function() {
        if ($(this).val() == "new" ) {
            // show new form
            $('.new_billing_address').show();
            // hide saved addresses
            $('.saved_bill_addresses').find('.show_address').hide().removeClass('show_address');
            // hide same details (shipping)
            if ($("select[name='ship_to']").val() == "same_as_billing") {
                $('.saved_ship_addresses').find('.show_address').hide().removeClass('show_address');
            }
        } else {
            // hide new form
            $('.new_billing_address').hide();
            // hide other saved addresses
            $('.saved_bill_addresses .saved_address').hide().removeClass('show_address');
            // show the saved address
            $('.saved_bill_addresses .address_'+$(this).val()).show().addClass('show_address');
            // show same details (shipping)
            if ($("select[name='ship_to']").val() == "same_as_billing") {
                // hide other saved addresses
                $('.saved_ship_addresses .saved_address').hide().removeClass('show_address');
                // show the ul class for this address
                $('.saved_ship_addresses .address_'+$("select[name='bill_to']").val()).show().addClass('show_address');                
            }
        }
    }).change();

	// shipping methods

	// select on click
    $('ul.shipping-methods li').click(function() {
        $('ul.shipping-methods li').find('input').removeAttr('checked');
        $(this).find('input').attr('checked', 'checked');
        $('ul.shipping-methods li').removeClass('selected_shipping');
        $('ul.shipping-methods .selected-icon').remove();
        $(this).addClass('selected_shipping');
        $(this).html($(this).html()+"<div class='selected-icon'><i class='icon-ok'></i></div>");
        // get shipping total
        var shipping_total = $(this).attr('data-cost');
        // set shipping total
		ship_total2 = Number(shipping_total).format(0, 3, '.', ',');
        $('.price-ship').html(currency+ship_total2);
        // get cart total
        var cart_total = 0;
        $('.product').each(function(index, value) {
            if ($(this).attr('data-cost')) {
                cart_total = cart_total + (Number($(this).attr('data-cost')) * Number($(this).attr('data-queantity')) );
            }
        });
        var price_total = (Number(cart_total) + Number(shipping_total)).format(0, 3, '.', ',');

        // set shipping total + cart total
        $('.price-total').html(currency+price_total);
    });
	
	/**
     * Number.prototype.format(n, x, s, c)
     * 
     * @param integer n: length of decimal
     * @param integer x: length of whole part
     * @param mixed   s: sections delimiter
     * @param mixed   c: decimal delimiter
     */
    Number.prototype.format = function(n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));
        
        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };

    // remove any selected methods on page load
    $('ul.shipping-methods .selected-icon').remove();
    $('ul.shipping-methods li').find('input').removeClass('selected_shipping');


    var ban = true;

    // add selected shipping to the selected shipping option
    $('ul.shipping-methods li').each(function(index, value) {
        var shipping_selected = $(this).find('input').attr('checked');
        if (shipping_selected == "checked") {
        	var ban = false;
            $(this).click();
        }
    });  

    if($('ul.shipping-methods li').length === 0 && $('#luchito').length > 0)
    {
        // set shipping total
        $('.price-ship').html(currency+0);
        // get cart total
        var cart_total = 0;
        $('.product').each(function(index, value) {
            if ($(this).attr('data-cost')) {
                cart_total = cart_total + (Number($(this).attr('data-cost')) * Number($(this).attr('data-queantity')) );
            }
        });
        var price_total = (Number(cart_total)).format(0, 3, '.', ',');

        // set shipping total + cart total
        //$('.price-total').html(currency+price_total);
    }

	// payment methods

	// select on click
    $('ul.payment-gateways li').click(function() {
        $('ul.payment-gateways li').find('input').removeAttr('checked');
        $(this).find('input').attr('checked', 'checked');
        $('ul.payment-gateways li').removeClass('selected_payment');
        $('ul.payment-gateways .selected-icon').remove();
        $(this).addClass('selected_payment');
        $(this).html($(this).html()+"<div class='selected-icon'><i class='icon-ok'></i></div>");
    });

    // remove any selected methods on page load
    $('ul.payment-gateways .selected-icon').remove();
    $('ul.payment-gateways li').find('input').removeClass('selected_payment');

    // add selected payment to the selected payment option
    $('ul.payment-gateways li').each(function(index, value) {
        var payment_selected = $(this).find('input').attr('checked');
        if (payment_selected) {

        }
        if (payment_selected == "checked") {
            $(this).addClass('selected_payment');
            $(this).html($(this).html()+"<div class='selected-icon'><i class='icon-ok'></i></div>");
        }
    }); 

    // select GB if no errors (not posted)
    if (! $('.alert-error').length) {
        $("#bill_country, #ship_country").val('GB').attr('selected',true);
    }
	
	/* Mis scripts */
	
	$(window).resize(function(){
	
		/* Tamaños dinámicos cajas home*/
		var ww = $(window).width();
		
		if(ww >= 979){
			$(".main-btn").removeClass("active");
			$(".main-content").removeClass("no-active");
			$("footer").removeClass("no-active");
			$(".main-nav").removeClass("active");
			$("#toTop").removeClass("no-active");
			$("body").addClass("desktop");
			$(".stick-menu").addClass("active");
		}else{
			$("body").removeClass("desktop");	
			$(".stick-menu").removeClass("active");
		}
		
		/* Altura dinámica del menú lateral*/
		var height_nav = $(".main-nav").height() - 60;
		$(".main-nav header > ul").css({
			'height' : height_nav
		});
		
	})
	
	var ww = $(window).width();
	
	if(ww >= 979){
		$("body").addClass("desktop");
		$(".stick-menu").addClass("active");
	}else{
		$("body").removeClass("desktop");	
		$(".stick-menu").removeClass("active");
	}
	
	$(document).scroll(function(e){
		var altScroll = $(window).scrollTop();
		if(altScroll >= 36){
			$(".stick-menu.active").addClass("fix")
		
		}else{
			$(".stick-menu.active").removeClass("fix")
		
		}
	})
	
	/* Altura dinámica del menú lateral*/
	var height_nav = $(".main-nav").height() - 60;
	$(".main-nav header > ul").css({
		'height' : height_nav
	});
	/* Slide principal */
	var elem = document.getElementById('central-slide');
/*	window.mySwipe = Swipe(elem, {
	  auto: 5000
	});*/
	
	/*var elem_ads = document.getElementById('ads-slide');
	window.mySwipeAds = Swipe(elem_ads, {
	  auto: 50000
	});*/
	
	var elem_home = document.getElementById('home-slide');
	/*window.mySwipeHome = Swipe(elem_home, {
	  auto: 6000
	});
	
	/* Slide producto */
	var elem_prod = document.getElementById('prod-slide');
	/*window.mySwipeProd = Swipe(elem_prod, {
	  auto: 5000
	});
	*/
	var c = 0;
	$(".ads-slide img").each(function(){
		if(c == 0){
			$(this).addClass("first");
			c++;	
		}	
	})
	
	
	/* Menu lateral */
	$(".main-btn").click(function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$(".main-content").removeClass("no-active");
			$("footer").removeClass("no-active");
			$(".main-nav").removeClass("active");
			$("#toTop").removeClass("no-active");
		}else{
			$(this).addClass("active");
			$(".main-content").addClass("no-active");
			$("footer").addClass("no-active");
			$(".main-nav").addClass("active");
			$("#toTop").addClass("no-active");
		}
		
	});
	
	/*Account dropdown*/
	$(".user-menu").hover(function(){
		$(".dropdown-menu", this).show();
	}, function(){
		$(".dropdown-menu", this).hide();
	});
	
	/* Buscador header */
	/*$(".btn-search").click(function(event){
		$(".main-content .form-search").fadeIn();
		$(".main-content .form-search input[type='text']").focus();	
		event.stopPropagation();
	})	
	$('html').click(function() {
	   $(".main-content .form-search").fadeOut();	
	});*/
	
	/* Productos home */
	$(".home-content .prod-item").each(function(){
		var i = $(this).index();
		if(i % 4 == 0){
			$(this).addClass("first");
		}else if(i % 4 == 3){
			$(this).addClass("last");
		}
		
	});
	$(".prod-item").hover(function(){
		if($(".desktop").length){
			$(".rollover", this).stop();
			$(".prod-actions", this).css({
				'display' : 'block'	
			})
			$(".rollover", this).animate({
				'opacity' :'1'
			}, 300)
		}
	}, function(){
		if($(".desktop").length){
			$(".rollover", this).stop();
			$(".prod-actions", this).hide();
			$(".rollover", this).animate({
				'opacity' :'0'
			}, 300)
		}
	});
	
	

	/* Productos categorias */
	$(".category .prod-item").each(function(){
		var i = $(this).index();
		if(i % 3 == 0){
			$(this).addClass("first");
		}else if(i % 3 == 2){
			$(this).addClass("last");
		}
		
	});
	
	$(".search .prod-item").each(function(){
		var i = $(this).index();
		if(i % 3 == 0){
			$(this).addClass("first");
		}else if(i % 3 == 2){
			$(this).addClass("last");
		}
		
	});
	
	
	
	$(".home-content .prod-item").each(function(){
		var i = $(this).index();

		if(i % 4 == 0){
			$(this).addClass("first");
		}else if(i % 4 == 3){
			$(this).addClass("last");
		}
		
	});
	
	/*$(".featured-prods .prod-item").each(function(){
		var i = $(this).index();
		if(i % 4 == 1){
			$(this).addClass("first");
		}else if(i % 4 == 0){
			$(this).addClass("last");
		}
		
	});*/
	
	/* Category list */
	$(".sidebar-items  .cat-list .dropdown .right-caret").each(function(){
		$(this).parent().parent().parent().append($(this));	
	})
	$(".sidebar-items  .cat-list .dropdown .right-caret").click(function(){
		$("> ul", $(this).parent()).slideToggle();
		
		if($(this).hasClass("active")){
			$(this).removeClass("active")
		}else{
			$(this).addClass("active")
		}
	})
	
	jQuery(".category-filter select").eq(0).change(function(){

		var opt = jQuery(this).children(":selected").attr("value");
		
		console.log(opt);
		var str = opt;
		
		 window.location = opt;
		
		
	});
	
	
		
});


// Window load events
$(window).bind("load", function() { 

    var window_height = $(window).height();
    var header_height = $("header").height()+30;
    var content_height = $(".push-footer").height();
    var footer_height = $("#sticky-footer").height();
    
    positionFooter();
       
    function positionFooter() {

        // if header, content and footer height are less than the window height
        if ((header_height+content_height+footer_height) < window_height) {
            // push the footer down by setting the min height of the content_height
            var min_height = (window_height-header_height)-footer_height;
            $(".push-footer").css('min-height', min_height+'px');
        }
    }

    $(window).resize(function() {
        window_height = $(window).height();
        header_height = $("header").height()+30;
        content_height = $(".push-footer").height();
        footer_height = $("#sticky-footer").height();
        
        positionFooter();
    });

    /* hide nav bar onload */

    function hideAddressBar(){
      if(document.documentElement.scrollHeight<window.outerHeight/window.devicePixelRatio)
        document.documentElement.style.height=(window.outerHeight/window.devicePixelRatio)+'px';
      setTimeout(window.scrollTo(1,1),0);
    }
    window.addEventListener("load",function(){hideAddressBar();});
    window.addEventListener("orientationchange",function(){hideAddressBar();});
               
});





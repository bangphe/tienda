<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['firesale:field:digital'] = 'Digital?';
$lang['firesale:field:download'] = 'Descargar';
$lang['firesale:field:downloads'] = 'Descargas Permitidas';
$lang['firesale:field:expires_after'] = 'Expira Después De';

$lang['firesale:instructions:digital'] = 'Los pedidos que contengan sólo los elementos digitales se ajustan automáticamente al enviar.';
$lang['firesale:instructions:download'] = 'El archivo que usuario va tener acceso para comprar.';
$lang['firesale:instructions:downloads'] = 'El numero de veces que el usuario puede descargar el archivo. Blanco es ilimitado.';
$lang['firesale:instructions:expires_after'] = 'Por cuánto tiempo el usuario será capaz de descargar el archivo después de la compra.';

$lang['firesale:interval:hours'] = 'Horas';
$lang['firesale:interval:days'] = 'Días';
$lang['firesale:interval:weeks'] = 'Semanas';
$lang['firesale:interval:months'] = 'Meses';
$lang['firesale:interval:years'] = 'Años';

// Tabs
$lang['firesale:tabs:digital'] = 'Digital';
$lang['firesale:digital:title'] = 'Descargas';

// Labels
$lang['firesale:label_remdls'] = 'Descargas restantes';
$lang['firesale:label_availuntil'] = 'Disponible hasta';
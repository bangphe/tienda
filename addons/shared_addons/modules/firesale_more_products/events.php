<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Events_Firesale_More_Products
{

    protected $ci;

    public function __construct()
    {

        $this->ci =& get_instance();

        $this->ci->load->model('firesale_more_products/more_products_m');

        // register the events
        Events::register('product_viewed', array($this, 'product_viewed'));

        Events::register('clear_cache', array($this, 'clear_cache'));

    }

    public function product_viewed($data)
    {

        // Load required items
        $this->ci->load->model('firesale_more_products/more_products_m');

        // Get current
        $current = $this->ci->more_products_m->get_current();
		
        // Add to array
        if( !in_array($data['id'], $current) ) {
            $current[] = $data['id'];
        }

        // Remove empty results
        $current = array_filter($current);

        // Keep to max size
        if( count($current) > (int)$this->ci->settings->get('firesale_mp_recent', '20') ) {
            array_shift($current);
        }
		
        // Save current
        $this->ci->more_products_m->save_current($current);
    }

    public function clear_cache()
    {
        $this->ci->pyrocache->delete_all('more_products_m');
    }

}

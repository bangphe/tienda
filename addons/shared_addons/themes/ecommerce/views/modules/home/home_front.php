{{ if banner }}
<section class="bg_slider" style="display:inline-block">        
   <div class="tp-banner" >
        <ul>
        {{ banner }}
            <li data-transition="boxslide" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
              <img src="{{image}}" alt="{{title}}"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" />
                <div class="flex-caption">
                    <!-- Textos -->
                    <h3 class="tp-caption title_custom_big_white lft tp-resizeme"
                        data-x="center" data-hoffset="0"
                        data-y="265" data-voffset="-50"
                        data-speed="300"
                        data-start="700"
                        data-easing="Power3.easeInOut"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.1"
                        data-endelementdelay="0.1"
                        data-endspeed="300"
                        style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        {{text}}
                    </h3>
                    <h1 class="tp-caption title_custom_big_white lft tp-resizeme"
                        data-x="center" data-hoffset="0"
                        data-y="130" data-voffset="-50"
                        data-speed="300"
                        data-start="700"
                        data-easing="Power3.easeInOut"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.1"
                        data-endelementdelay="0.1"
                        data-endspeed="300"
                        style="z-index: 2; max-width: 525px; max-height: auto; ">
                        {{title}}
                    </h1>
                    {{ if title }}
                    <div class="tp-caption sfb tp-resizeme"
                        data-x="center" data-hoffset="0"
                        data-y="360" data-voffset="50"
                        data-speed="300"
                        data-start="1200"
                        data-easing="Power3.easeInOut"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.1"
                        data-endelementdelay="0.1"
                        data-endspeed="300">
                      <a href="{{ link }}" class="btn btn-transparent-2 btn-lg btn_gris">IR A LA COLECCIÓN</a>
                    </div>
                    {{ endif }}
                </div>
            </li>
        {{ /banner }}
        </ul>
    </div>
      
</section>
{{ endif }}


<!--Banner dos-->
{{ if banner_two }}
<section class="bg_categorias_home">
  <div class="marco">
    <ul class="categorias_hor">
      {{ banner_two }}
      <li class="large-4 medium-4 small-12 columns bounceInUp wow" data-wow-duration="1s" data-wow-delay="0.1s">
        <a href="{{ link }}">
          <div class="text_cat">
            <h3>{{ text }}</h3>
            <h2>{{ title }}</h2>
          </div> 
          <img src="{{ image }}" alt="">
        </a>
      </li>
      {{ /banner_two }}
    </ul> 
  </div>
</section>
{{endif}}
<!-- Fin Banner -->

<!--Marcas--
<section class="marcar_home">
  <?php if ($brands['total'] > 0 && $brands['total'] < 6): ?>
  <ul class="carrusel_marcas">
    <?php foreach($brands['entries'] as $brand ): ?>
    <li class="item"><img src="<?= ( $brand['image'] ? $brand['image']->path : '' ); ?>" alt="" /></li>
    <?php endforeach; ?>
  </ul>
  <?php endif;?>
</section>
<!--Fin Marcas-->

<div class="clear"></div>

<section class="promociones_home">
<br>
<div class="marco">
  <ul class="destacados">
      {{ firesale:products limit="8" order="created desc" where="p.featured = 1" limit="4" }}
      {{ if entries }}
      {{ entries }}
      <li class="large-3 medium-6 small-12 columns wow fadeInUp category-1" data-wow-delay="0.1s">
          <div class="marco_prod">
            <div class="img_prod">
              <div class="action_share modal_all">
                  <?php if (module_enabled('firesale_wishlist')): ?> 
                      {{ if user:logged_in }} 
                      <a href="#wishlistModal" data-item-id="{{ id }}" data-toggle="modal" data-target="#wishlistModal" data-placement="top" data-original-title="Agregar a mi lista" data-effect="mfp-move-horizontal" class="wish_modal"><i class="fa fa-heart"></i></a> 
                      {{ endif }}
                  <?php endif; ?>
                  <a class="fb-ico facebook_popup link" data-placement="top" data-original-title="Compartir en Facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer.php?s=100&p[title]={{ title }}&p[url]={{ firesale:url route='product' id=id }}{{ if images.1.id }}&p[images][0]={{ url:site uri='files/large' }}/{{ images.1.id }}{{ endif }}">
                    <i class="fa fa-facebook"></i>
                  </a>
                  <a class="tw-ico twitter_popup link" data-placement="top" data-original-title="Compartir en Twitter" href="http://twitter.com/share?url={{ firesale:url route='product' id=id }}&text={{ title }}%20is%20awesome!{{ if {theme:options:twitter_account} }}&via={{ theme:options:twitter_account }}{{ endif }}">
                    <i class="fa fa-twitter"></i>
                  </a>
              </div>
              <div class="mask modal_all">
                 <a href="#modal_preview" data-href="{{url:base}}product/{{slug}}" id="quick_view" data-effect="mfp-3d-unfold">
                 <div class="btn_blanco">Vista rápida</div>
                 </a>
              </div>
              {{ if images }}
                {{ if images.0.id }}
                <img  src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }} alternate" />
                {{ else }}
                <img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                {{ endif }}
                {{ else }}
                {{ theme:image file="no-image.png" alt=title }}
              {{ endif }}
                {{ if new_item AND descuento > '0' }}
                    <span class="etiq_new">NUEVO</span>
                    <span class="etiq_desc desc_bot">{{ descuento }}%<i>dscto</i></span>
                {{ elseif descuento > '0' }}
                   <span class="etiq_desc">{{ descuento }}%<i>dscto</i></span>
                {{ elseif new_item }}
                    <span class="etiq_new">NUEVO</span>
                {{ endif }}
            </div>
            <div class="cont_head_cat color">
              <div class="right">
                  {{ if stock > "0" }}
                    <b>En stock</b>
                  {{ else }}
                    <b>Sin stock</b>
                  {{ endif }}
              </div>
                <div class="clear"></div>
            </div>
            <p>{{ title }}</p>
            {{ if descuento > '0' }}
                <div class="precio precio_ant">${{ price_ant }}</div>
                <div class="precio">{{ price_formatted }}</div>
            {{ else }}
                    <div class="precio">{{ price_formatted }}</div>
            {{ endif }}
             <div class="clear"></div>
            <div class="block"> 
            {{ if stock > 0 }}
              <a href="{{ firesale:url route='product' id=id }}" title="">
                <div class="btn_border">
                  Detalle
                </div>  
              </a>
            {{ else }}
              <a href="{{ firesale:url route='product' id=id }}" title="">
                <div class="btn_border">
                  Detalle
                </div>  
              </a>
            {{ endif }}
            </div>
          </div>
      </li>
      {{ /entries }}
      {{ endif }}
      {{ /firesale:products }}
  </ul>
</div>
</section>
{{ if products }}
<div class="publicidadTienda promo_uno clearfix bg_promo" {{ if pautas.imagen }} style="background-image: url({{pautas.imagen}}) !important;" {{ endif }}>
  {{ products }}
  <div class="marco_promo">
    <section class="row">
      <div class="small-12 medium-7 large-7 columns" data-wow-delay="0.1s">
        <div class="publicidadIndex clearfix">
          <!-- <span>Hombre performance</span> -->
          <b>{{ title }}</b>
          <i>{{ price_formatted }}</i>
          <div class="clear"></div>
        </div>
      </div>
      <div class="small-12 medium-5 large-5 columns" data-wow-delay="0.2s">
        <a href="{{ firesale:url route='product' id=id }}">
          <img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}">             
        </a>
      </div>
    </section>
  </div>
  {{ /products }} 
</div>
{{ endif }}
<div class="clear"></div>

<section class="promociones_home">
<div class="marco">
  <ul class="destacados">
      {{ firesale:products limit="4,4" order="created desc" where="p.featured = 1" }}
      {{ if entries }}
      {{ entries }}
      <li class="large-3 medium-6 small-12 columns wow fadeInUp category-1" data-wow-delay="0.1s">
          <div class="marco_prod">
            <div class="img_prod">
              <div class="action_share modal_all">
                  <?php if (module_enabled('firesale_wishlist')): ?> 
                      {{ if user:logged_in }} 
                      <a href="#wishlistModal" data-item-id="{{ id }}" data-toggle="modal" data-target="#wishlistModal" data-placement="top" data-original-title="Agregar a mi lista" data-effect="mfp-move-horizontal" class="wish_modal"><i class="fa fa-heart"></i></a> 
                      {{ endif }}
                  <?php endif; ?>
                  <a class="fb-ico facebook_popup link" data-placement="top" data-original-title="Compartir en Facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer.php?s=100&p[title]={{ title }}&p[url]={{ firesale:url route='product' id=id }}{{ if images.1.id }}&p[images][0]={{ url:site uri='files/large' }}/{{ images.1.id }}{{ endif }}">
                    <i class="fa fa-facebook"></i>
                  </a>
                  <a class="tw-ico twitter_popup link" data-placement="top" data-original-title="Compartir en Twitter" href="http://twitter.com/share?url={{ firesale:url route='product' id=id }}&text={{ title }}%20is%20awesome!{{ if {theme:options:twitter_account} }}&via={{ theme:options:twitter_account }}{{ endif }}">
                    <i class="fa fa-twitter"></i>
                  </a>
              </div>
              <div class="mask modal_all">
                 <a href="#modal_preview" data-href="{{url:base}}product/{{slug}}" id="quick_view" data-effect="mfp-3d-unfold">
                 <div class="btn_blanco">Vista rápida</div>
                 </a>
              </div>
              {{ if images }}
                {{ if images.0.id }}
                <img  src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }} alternate" />
                {{ else }}
                <img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                {{ endif }}
                {{ else }}
                {{ theme:image file="no-image.png" alt=title }}
              {{ endif }}
                {{ if new_item AND descuento > '0' }}
                    <span class="etiq_new">NUEVO</span>
                    <span class="etiq_desc desc_bot">{{ descuento }}%<i>dscto</i></span>
                {{ elseif descuento > '0' }}
                   <span class="etiq_desc">{{ descuento }}%<i>dscto</i></span>
                {{ elseif new_item }}
                    <span class="etiq_new">NUEVO</span>
                {{ endif }}
            </div>
            <div class="cont_head_cat color">
              <div class="right">
                  {{ if stock > "0" }}
                    <b>En stock</b>
                  {{ else }}
                    <b>Sin stock</b>
                  {{ endif }}
              </div>
                <div class="clear"></div>
            </div>
            <p>{{ title }}</p>
            {{ if descuento > '0' }}
                <div class="precio precio_ant">${{ price_ant }}</div>
                <div class="precio">{{ price_formatted }}</div>
            {{ else }}
                    <div class="precio">{{ price_formatted }}</div>
            {{ endif }}
             <div class="clear"></div>
            <div class="block"> 
            {{ if stock > 0 }}
              <a href="{{ firesale:url route='product' id=id }}" title="">
                <div class="btn_border">
                  Detalle
                </div>  
              </a>
              <!-- {{ if modifiers }}
                <a href="{{ firesale:url route='cart' after='/insert/' }}{{ modifiers.1.variations.1.product.id }}">
                  <div class="btn_border p_cart">
                  Agregar al Carrito
                  </div>
                </a>
                {{ else }}
                <a href="{{ firesale:url route='cart' after='/insert/' }}{{ id }}">
                  <div class="btn_border p_cart">
                  Agregar al Carrito
                  </div>       
                </a>
              {{ endif }}      -->                 
                  
            {{ else }}
              <a href="{{ firesale:url route='product' id=id }}" title="">
                <div class="btn_border">
                  Detalle
                </div>  
              </a>
            {{ endif }}
            </div>
          </div>
      </li>
      {{ /entries }}
      {{ endif }}
      {{ /firesale:products }}
  </ul>
</div>
</section>

    </div>
</div>
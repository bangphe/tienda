
    <?php echo form_open_multipart($this->uri->uri_string(), 'class="crud" id="tabs"'); ?>

        <input type="hidden" name="id" id="id" value="<?php /*echo $id;*/ ?>" />

        <section class="title">
            <h4>Edit Review</h4>
        </section>

        <section class="item form_inputs">
			<div class="content">
           		
                <div class="inline-form">
                	<fieldset>
                        <ul>
        <?php foreach( $fields AS $input ): ?>
                            <li class="<?php echo alternator('even', ''); ?>">
                                <label for="<?php echo $input['input_slug']; ?>"><?php echo ( lang(substr($input['input_title'], 5)) ? lang(substr($input['input_title'], 5)) : $input['input_title'] ); ?> <?php echo $input['required']; ?></label>
                                <div class="input"><?php echo $input['input']; ?></div>
                                <br class="clear">
                            </li>
        <?php endforeach; ?>
                        </ul>
        			</fieldset>
                </div>
                <div class="buttons">
                    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'btn red') )); ?>
                </div>
                    
			</div>
        </section>

    <?php echo form_close(); ?>

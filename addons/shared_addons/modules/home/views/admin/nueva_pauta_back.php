<section class="title">
    <h4>Nueva Pauta</h4>
</section>
<section class="item">
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-pauta"><span>Nueva Pauta</span></a></li>
            </ul>
            <div class="clear"></div>
            <div class="form_inputs" id="page-bibliografia">
                <?php echo form_open_multipart(site_url('admin/home/crear_pauta/'), 'class="crud"'); ?>
                <div class="inline-form">
                    <fieldset>
                        <ul>
                            <li>
                                <label for="name">Imagen
                                    <small>
                                        - Imagen Permitidas gif | jpg | png | jpeg<br>
                                        - Tamaño Máximo 2 MB<br>
                                        - Ancho Máximo 1000px<br>
                                        - Alto Máximo 700px
                                    </small>
                                </label>
                                <div class="input">
                                   <div class="btn-false">
                                    <div class="btn">Examinar</div>
                                    <?php echo form_upload('imagen', '', ' id="imagen"'); ?>
                                </div>
                            </div>
                            <br class="clear">
                        </li>
                        <li>
                            <label for="name">Link <span>*</span></label>
                            <div class="input">

                            <?php echo form_input('link', set_value('link'), ' id="link" style="width: 80%;"'); ?>
                          </div>
                      </li>
                  </ul>
              </fieldset>
          </div>
          <div class="buttons float-right padding-top">
            <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
        </div>

        <?php echo form_close(); ?>
    </div>

</div>
</div>
</section>
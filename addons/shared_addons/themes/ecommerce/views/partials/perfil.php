<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6 no-js" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7 no-js" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="en"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<!-- html5.js for IE less than 9 -->
<!--[if lt IE 9]>  <script src="assets/js/lib/html5.js"></script>  <![endif]-->
<html>
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta http-equiv="content-language" content="es" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title>Imaginamos.com / e-commerce</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="copyright" content="imaginamos.com" />
  <meta name="date" content="2014" />
  <meta name="author" content="diseño web: imaginamos.com" />
  <meta name="robots" content="All" />
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <link rel="author" type="text/plain" href="humans.txt" />
  <!--Style's-->
  <link href="assets/css/tienda.css" rel="stylesheet" type="text/css" />
  <!--Librerias-->
  <script src="assets/js/lib/modernizr.min.js"></script>
</head>

<body class="userlogeado">
<!-- <section id="preload">
    <div id="preload-container">
      <div id="dot"></div>
        <div class="step" id="s1"></div>
        <div class="step" id="s2"></div>
        <div class="step" id="s3"></div>
    </div>
</section>
 -->
<?php include("nav_responsive.php"); ?>  

<section class="content_all">

  <?php include("header.php"); ?> 

  <!--Contenidos Sitio-->
  <section class="barra_int"></section>
  <section class="cont_home">   

      <!--Miga de pan-->
      <div class="bg_miga">
        <ul class="marco miga_pan">
          <a href="index.php"><i class="fa fa-angle-double-right"></i>Inicio</a>
          <a href="#"><i class="fa fa-angle-double-right"></i>Mi Cuenta</a>
        </ul>
          <div class="clear"></div>
      </div>    
      <!--Fin Miga de pan-->

      <div class="clear"></div>
      
      <section class="marco bg_pasos_comp">
          <!--bg Carrito-->    
          <section class="bg_perfil large-3 medium-3 small-12 columns item1">
             <section class="categoria_perfil">
               <ul class="sllider_option marco">
                  <li><a href="#perfil1"><i class="fa fa-user"></i> Mi Perfil</a></li>
                  <li><a href="#perfil2"><i class="fa fa-folder-open"></i> Direcciones Guardadas</a></li>
                  <li><a href="#perfil3"><i class="fa fa-heart"></i> Lista de deseos</a></li>
                  <li><a href="#perfil4"><i class="fa fa-sort-amount-asc"></i> Historial</a></li>
               </ul>
                 <div class="clear"></div>
             </section>
          </section>
          <!--Fin bg Carrito-->      

          <!--Campos Perfil-->
          <section class="large-9 medium-9 small-12 columns item2">
            <section class="cont_cat_perfil" id="perfil1">
                <div class=" relarive elem_blancos animated fadeInUp">
                     <!--Titulo-->
                      <div class="tittles">
                        <h1>MI PERFIL</h1>
                        <div class="btn_edit btn_gris"><i class="fa fa-pencil"></i> Editar</div>
                      </div>
                     <!--Fin Titulo-->                     
                     <div class="large-6 medium-6 small-12 columns item1">
                        <h3>Mi Cuenta</h3>
                        <fieldset> 
                            <label>Nombres</label>                        
                            <input type="text" name="Camilo" id="name_per" placeholder="Lorem" disabled />                                                                       
                        </fieldset> 
                        <fieldset> 
                            <label>Apellidos</label>                        
                            <input type="text" name="Rojo" id="" placeholder="Ipsum" disabled />                                                                       
                        </fieldset> 
                         <fieldset> 
                            <label>Nombre de Usuario</label>                        
                            <input type="text" name="camilo.rojo" id="" placeholder="Lorem Ipsum!" disabled />                                                                       
                        </fieldset> 
                        <fieldset> 
                            <label>Company</label>                        
                            <input type="text" name="Nombres" id="" placeholder="Imaginamos SA" disabled />                                                                       
                        </fieldset>
                        <fieldset> 
                            <label>Sobre mi</label>                        
                            <textarea id="ccomment" name="comment" cols="22" placeholder="Nec elit eleifend augue posuere sollicitudin vitae vel varius blandit cras porta porta felis, non elementum nunc fringilla consectetur " disabled></textarea>                                                                        
                        </fieldset>                         
                        <fieldset class="large-6 medium-6 small-12 columns item1"> 
                            <label>Idioma</label>                        
                            <select placeholder="Español" disabled>
                              <option value="">Español</option>
                              <option value="">Ingles</option>
                            </select>                                                                   
                        </fieldset> 
                        <fieldset class="large-6 medium-6 small-12 columns item2"> 
                            <label>Fecha de nacimiento</label>                        
                            <input type="date" name="Nombres" id="" disabled/>                                                                       
                        </fieldset> 
                        <fieldset class="large-6 medium-6 small-12 columns item1">
                            <label>Sexo</label>                        
                            <select placeholder="Hombre" disabled>
                              <option value="">Hombre</option>
                              <option value="">Mujer</option>
                            </select>  
                        </fieldset> 
                       <fieldset class="large-6 medium-6 small-12 columns item2"> 
                            <label>Teléfono</label>                        
                            <input type="text" name="Nombres" id="" placeholder="7939403" disabled/>                                                                       
                        </fieldset> 
                        <fieldset class="large-6 medium-6 small-12 columns item1">
                            <label>Móvil / Celular</label>                        
                            <input type="text" name="Nombres" id="" placeholder="3168456354" disabled/>                                                                       
                        </fieldset> 
                        <fieldset class="large-6 medium-6 small-12 columns item2"> 
                            <label>Código postal</label>                        
                            <input type="text" name="Nombres" id=""  placeholder="16332" disabled/>                                                                       
                        </fieldset> 
                        <fieldset> 
                            <label>Sitio web</label>                        
                            <input type="text" name="Nombres" id=""  placeholder="imaginamos.com" disabled/>                                                                       
                        </fieldset> 
                     </div> 

                     <div class="large-6 medium-6 small-12 columns item2">
                        <h3>Datos de Acceso</h3>
                        <fieldset> 
                            <label>E-mail</label>                        
                            <input type="text" name="Nombres" id=""   placeholder="pruea@imaginamos.com" disabled/>                                                                       
                        </fieldset> 
                        <fieldset> 
                            <label>Contraseña</label>                        
                            <input type="password" name="password" id=""  value="lorem" disabled/>                                                                       
                        </fieldset> 

                        <div class="contra_new">
                           <div class="alert_form margin_b_aler">Para modificar tu contraseña rellena los siguientes cambios.</div>
                           <fieldset> 
                              <label>Contraseña actual</label>                        
                              <input type="text" name="Nombres" id="" />                                                                       
                          </fieldset>  
                          <fieldset> 
                            <label>Nueva Contraseña</label>                        
                            <input type="password" name="Nombres" id="" />                                                                       
                        </fieldset> 
                        <fieldset> 
                            <label>Confirmar Contraseña</label>                        
                            <input type="password" name="Nombres" id="" />                                                                       
                        </fieldset> 

                        <a href="" title="" class="btn_gris right">CAMBIAR CONTRASEÑA</a>
                           <div class="clear"></div>
                        </div>                        
                     </div>  

                        <div class="clear"></div>   

                       <div class="controles_perfil">
                          <a href="javascript:void(0)" title="" class="btn_gris right margin-l guardar_perfil">GUARDAR CAMBIOS</a>
                          <a href="avascript:void(0)" title="" class="btn_gris right margin-l guardar_perfil">CANCELAR</a>
                          <div class="clear"></div>
                       </div>   

                </div>  
                  <div class="clear"></div>
            </section>
            
            <section class="cont_cat_perfil" id="perfil2">
                <!--Titulo-->
                <div class="tittles">
                  <h1>Mis Direcciones</h1>  
                </div>
                 <!--Fin Titulo-->    

                 <!--List Direcciones-->
                 <section class="mis_dire_cread animated fadeInUp">                       
                   <ul class="cajasdir">
                      <li class="large-4 medium-6 small-12 columns item1">
                        <div class="caja_borde">
                          <h3>Nombre Dirección <div class="btn_edit"><i class="fa fa-pencil"></i></div></h3>
                          <p><i class="fa fa-user"></i> Nombre contacto</p>
                          <p><i class="fa fa-map-marker"></i> Direccion 1</p>
                          <p><i class="fa fa-map-marker"></i> Direccion 2</p>
                          <p><i class="fa fa-building-o"></i> Ciudad</p>
                          <p><i class="fa fa-globe"></i> Pais</p>
                        </div>                      
                      </li>
                      <li class="large-4 medium-6 small-12 columns item1">
                        <div class="caja_borde">
                          <h3>Nombre Dirección <div class="btn_edit"><i class="fa fa-pencil"></i></div></h3>
                          <p><i class="fa fa-user"></i> Nombre contacto</p>
                          <p><i class="fa fa-map-marker"></i> Direccion 1</p>
                          <p><i class="fa fa-map-marker"></i> Direccion 2</p>
                          <p><i class="fa fa-building-o"></i> Ciudad</p>
                          <p><i class="fa fa-globe"></i> Pais</p>
                        </div>                      
                      </li>
                      <li class="large-4 medium-6 small-12 columns item1">
                        <div class="caja_borde">
                          <h3>Nombre Dirección <div class="btn_edit"><i class="fa fa-pencil"></i></div></h3>
                          <p><i class="fa fa-user"></i> Nombre contacto</p>
                          <p><i class="fa fa-map-marker"></i> Direccion 1</p>
                          <p><i class="fa fa-map-marker"></i> Direccion 2</p>
                          <p><i class="fa fa-building-o"></i> Ciudad</p>
                          <p><i class="fa fa-globe"></i> Pais</p>
                        </div>                      
                      </li>
                   </ul>

                     <div class="clear"></div>

                    <div class="controles_perfil2">
                          <a href="javascript:void(0)" title="" class="btn_gris right margin-l btn_new_direc">AGREGAR NUEVA DIRECCIÓN</a>
                          <div class="clear"></div>
                       </div>   
                    <div class="clear"></div>   
                 </section>

                <!--Form Direcciones-->
                <form action="" method="post" class="elem_blancos form_direc animated fadeInUp">
                    <h3>Nueva Dirección</h3>
                  <div class="large-6 medium-6 small-12 columns item1">
                      <label>Nombre de Ubicación <span>*</span></label>
                      <input type="text" name="title" value="" id="title" maxlength="255">                     
                      
                      <label>Nombre <span>*</span></label>
                      <input type="text" name="firstname" value="" id="firstname" maxlength="100">

                      <label>Email <span>*</span></label>
                      <input type="text" name="email" value="" id="email" maxlength="255">

                      <label>Dirección 1 <span>*</span></label>
                      <input type="text" name="address1" value="" id="address1" maxlength="255">

                      <label> Ciudad <span>*</span></label>
                      <input type="text" name="city" value="" id="city" maxlength="255">                                

                      <label>CP</label>
                      <input type="text" name="postcode" value="" id="postcode" maxlength="40">
                  </div>
                        
                  <div class="large-6 medium-6 small-12 columns item2">
                       <label>Empresa <span></span></label>
                       <input type="text" name="company" value="" id="company" maxlength="255">
                    
                       <label>Apellido <span>*</span> </label>
                       <input type="text" name="lastname" value="" id="lastname" maxlength="100">
                       
                       <label>Teléfono <span></span></label>
                       <input type="text" name="phone" value="" id="phone" maxlength="255">
                       
                       <label>Dirección 2 <span></span></label>
                       <input type="text" name="address2" value="" id="address2" maxlength="255">
                       
                       <label>Estado/Depto <span>*</span></label>
                       <input type="text" name="county" value="" id="county" maxlength="255">
                       
                       <label>País <span></span></label>
                       <select name="country" id="country">                       
                          <option value="" selected="selected">-----</option>
                          <option value="AD">Andorra</option>
                          <option value="AE">United Arab Emirates</option>
                          <option value="AF">Afghanistan</option>
                          <option value="AG">Antigua and Barbuda</option>
                          <option value="AI">Anguilla</option>
                          <option value="AL">Albania</option>
                          <option value="AM">Armenia</option>
                          <option value="AN">Netherlands Antilles</option>
                          <option value="AO">Angola</option>
                          <option value="AQ">Antarctica</option>
                          <option value="AR">Argentina</option>
                          <option value="AS">American Samoa</option>
                          <option value="AT">Austria</option>
                          <option value="AU">Australia</option>
                          <option value="AW">Aruba</option>
                          <option value="AX">Aland Islands</option>
                          <option value="AZ">Azerbaijan</option>
                          <option value="BA">Bosnia and Herzegovina</option>
                          <option value="BB">Barbados</option>
                          <option value="BD">Bangladesh</option>
                          <option value="BE">Belgium</option>
                          <option value="BF">Burkina Faso</option>
                          <option value="BG">Bulgaria</option>
                          <option value="BH">Bahrain</option>
                          <option value="BI">Burundi</option>
                          <option value="BJ">Benin</option>
                          <option value="BL">Saint Barthélemy</option>
                          <option value="BM">Bermuda</option>
                          <option value="BN">Brunei</option>
                          <option value="BO">Bolivia</option>
                          <option value="BR">Brazil</option>
                          <option value="BS">Bahamas</option>
                          <option value="BT">Bhutan</option>
                          <option value="BV">Bouvet Island</option>
                          <option value="BW">Botswana</option>
                          <option value="BY">Belarus</option>
                          <option value="BZ">Belize</option>
                          <option value="CA">Canada</option>
                          <option value="CC">Cocos (Keeling) Islands</option>
                          <option value="CD">Congo (Kinshasa)</option>
                          <option value="CF">Central African Republic</option>
                          <option value="CG">Congo (Brazzaville)</option>
                          <option value="CH">Switzerland</option>
                          <option value="CI">Ivory Coast</option>
                          <option value="CK">Cook Islands</option>
                          <option value="CL">Chile</option>
                          <option value="CM">Cameroon</option>
                          <option value="CN">China</option>
                          <option value="CO">Colombia</option>
                          <option value="CR">Costa Rica</option>
                          <option value="CU">Cuba</option>
                          <option value="CW">Curaçao</option>
                          <option value="CV">Cape Verde</option>
                          <option value="CX">Christmas Island</option>
                          <option value="CY">Cyprus</option>
                          <option value="CZ">Czech Republic</option>
                          <option value="DE">Germany</option>
                          <option value="DJ">Djibouti</option>
                          <option value="DK">Denmark</option>
                          <option value="DM">Dominica</option>
                          <option value="DO">Dominican Republic</option>
                          <option value="DZ">Algeria</option>
                          <option value="EC">Ecuador</option>
                          <option value="EE">Estonia</option>
                          <option value="EG">Egypt</option>
                          <option value="EH">Western Sahara</option>
                          <option value="ER">Eritrea</option>
                          <option value="ES">Spain</option>
                          <option value="ET">Ethiopia</option>
                          <option value="FI">Finland</option>
                          <option value="FJ">Fiji</option>
                          <option value="FK">Falkland Islands</option>
                          <option value="FM">Micronesia</option>
                          <option value="FO">Faroe Islands</option>
                          <option value="FR">France</option>
                          <option value="GA">Gabon</option>
                          <option value="GB">United Kingdom</option>
                          <option value="GD">Grenada</option>
                          <option value="GE">Georgia</option>
                          <option value="GF">French Guiana</option>
                          <option value="GG">Guernsey</option>
                          <option value="GH">Ghana</option>
                          <option value="GI">Gibraltar</option>
                          <option value="GL">Greenland</option>
                          <option value="GM">Gambia</option>
                          <option value="GN">Guinea</option>
                          <option value="GP">Guadeloupe</option>
                          <option value="GQ">Equatorial Guinea</option>
                          <option value="GR">Greece</option>
                          <option value="GS">South Georgia and the South Sandwich Islands</option>
                          <option value="GT">Guatemala</option>
                          <option value="GU">Guam</option>
                          <option value="GW">Guinea-Bissau</option>
                          <option value="GY">Guyana</option>
                          <option value="HK">Hong Kong S.A.R., China</option>
                          <option value="HM">Heard Island and McDonald Islands</option>
                          <option value="HN">Honduras</option>
                          <option value="HR">Croatia</option>
                          <option value="HT">Haiti</option>
                          <option value="HU">Hungary</option>
                          <option value="ID">Indonesia</option>
                          <option value="IE">Ireland</option>
                          <option value="IL">Israel</option>
                          <option value="IM">Isle of Man</option>
                          <option value="IN">India</option>
                          <option value="IO">British Indian Ocean Territory</option>
                          <option value="IQ">Iraq</option>
                          <option value="IR">Iran</option>
                          <option value="IS">Iceland</option>
                          <option value="IT">Italy</option>
                          <option value="JE">Jersey</option>
                          <option value="JM">Jamaica</option>
                          <option value="JO">Jordan</option>
                          <option value="JP">Japan</option>
                          <option value="KE">Kenya</option>
                          <option value="KG">Kyrgyzstan</option>
                          <option value="KH">Cambodia</option>
                          <option value="KI">Kiribati</option>
                          <option value="KM">Comoros</option>
                          <option value="KN">Saint Kitts and Nevis</option>
                          <option value="KP">North Korea</option>
                          <option value="KR">South Korea</option>
                          <option value="KW">Kuwait</option>
                          <option value="KY">Cayman Islands</option>
                          <option value="KZ">Kazakhstan</option>
                          <option value="LA">Laos</option>
                          <option value="LB">Lebanon</option>
                          <option value="LC">Saint Lucia</option>
                          <option value="LI">Liechtenstein</option>
                          <option value="LK">Sri Lanka</option>
                          <option value="LR">Liberia</option>
                          <option value="LS">Lesotho</option>
                          <option value="LT">Lithuania</option>
                          <option value="LU">Luxembourg</option>
                          <option value="LV">Latvia</option>
                          <option value="LY">Libya</option>
                          <option value="MA">Morocco</option>
                          <option value="MC">Monaco</option>
                          <option value="MD">Moldova</option>
                          <option value="ME">Montenegro</option>
                          <option value="MF">Saint Martin (French part)</option>
                          <option value="MG">Madagascar</option>
                          <option value="MH">Marshall Islands</option>
                          <option value="MK">Macedonia</option>
                          <option value="ML">Mali</option>
                          <option value="MM">Myanmar</option>
                          <option value="MN">Mongolia</option>
                          <option value="MO">Macao S.A.R., China</option>
                          <option value="MP">Northern Mariana Islands</option>
                          <option value="MQ">Martinique</option>
                          <option value="MR">Mauritania</option>
                          <option value="MS">Montserrat</option>
                          <option value="MT">Malta</option>
                          <option value="MU">Mauritius</option>
                          <option value="MV">Maldives</option>
                          <option value="MW">Malawi</option>
                          <option value="MX">Mexico</option>
                          <option value="MY">Malaysia</option>
                          <option value="MZ">Mozambique</option>
                          <option value="NA">Namibia</option>
                          <option value="NC">New Caledonia</option>
                          <option value="NE">Niger</option>
                          <option value="NF">Norfolk Island</option>
                          <option value="NG">Nigeria</option>
                          <option value="NI">Nicaragua</option>
                          <option value="NL">Netherlands</option>
                          <option value="NO">Norway</option>
                          <option value="NP">Nepal</option>
                          <option value="NR">Nauru</option>
                          <option value="NU">Niue</option>
                          <option value="NZ">New Zealand</option>
                          <option value="OM">Oman</option>
                          <option value="PA">Panama</option>
                          <option value="PE">Peru</option>
                          <option value="PF">French Polynesia</option>
                          <option value="PG">Papua New Guinea</option>
                          <option value="PH">Philippines</option>
                          <option value="PK">Pakistan</option>
                          <option value="PL">Poland</option>
                          <option value="PM">Saint Pierre and Miquelon</option>
                          <option value="PN">Pitcairn</option>
                          <option value="PR">Puerto Rico</option>
                          <option value="PS">Palestinian Territory</option>
                          <option value="PT">Portugal</option>
                          <option value="PW">Palau</option>
                          <option value="PY">Paraguay</option>
                          <option value="QA">Qatar</option>
                          <option value="RE">Reunion</option>
                          <option value="RO">Romania</option>
                          <option value="RS">Serbia</option>
                          <option value="RU">Russia</option>
                          <option value="RW">Rwanda</option>
                          <option value="SA">Saudi Arabia</option>
                          <option value="SB">Solomon Islands</option>
                          <option value="SC">Seychelles</option>
                          <option value="SD">Sudan</option>
                          <option value="SE">Sweden</option>
                          <option value="SG">Singapore</option>
                          <option value="SH">Saint Helena</option>
                          <option value="SI">Slovenia</option>
                          <option value="SJ">Svalbard and Jan Mayen</option>
                          <option value="SK">Slovakia</option>
                          <option value="SL">Sierra Leone</option>
                          <option value="SM">San Marino</option>
                          <option value="SN">Senegal</option>
                          <option value="SO">Somalia</option>
                          <option value="SR">Suriname</option>
                          <option value="ST">Sao Tome and Principe</option>
                          <option value="SV">El Salvador</option>
                          <option value="SY">Syria</option>
                          <option value="SZ">Swaziland</option>
                          <option value="TC">Turks and Caicos Islands</option>
                          <option value="TD">Chad</option>
                          <option value="TF">French Southern Territories</option>
                          <option value="TG">Togo</option>
                          <option value="TH">Thailand</option>
                          <option value="TJ">Tajikistan</option>
                          <option value="TK">Tokelau</option>
                          <option value="TL">Timor-Leste</option>
                          <option value="TM">Turkmenistan</option>
                          <option value="TN">Tunisia</option>
                          <option value="TO">Tonga</option>
                          <option value="TR">Turkey</option>
                          <option value="TT">Trinidad and Tobago</option>
                          <option value="TV">Tuvalu</option>
                          <option value="TW">Taiwan</option>
                          <option value="TZ">Tanzania</option>
                          <option value="UA">Ukraine</option>
                          <option value="UG">Uganda</option>
                          <option value="UM">United States Minor Outlying Islands</option>
                          <option value="US">United States</option>
                          <option value="UY">Uruguay</option>
                          <option value="UZ">Uzbekistan</option>
                          <option value="VA">Vatican</option>
                          <option value="VC">Saint Vincent and the Grenadines</option>
                          <option value="VE">Venezuela</option>
                          <option value="VG">British Virgin Islands</option>
                          <option value="VI">U.S. Virgin Islands</option>
                          <option value="VN">Vietnam</option>
                          <option value="VU">Vanuatu</option>
                          <option value="WF">Wallis and Futuna</option>
                          <option value="WS">Samoa</option>
                          <option value="YE">Yemen</option>
                          <option value="YT">Mayotte</option>
                          <option value="ZA">South Africa</option>
                          <option value="ZM">Zambia</option>
                          <option value="ZW">Zimbabwe</option>
                       </select>

                     </div>

                      <div class="large-12 medium-12 small-12 columns">
                          <a href="javascript:void(0)" class="btn_gris right  margin-l sabe_dir">Guardar</a>
                          <a href="javascript:void(0)" class="btn_gris right  margin-l sabe_dir">Cancelar</a>
                      </div>
                  </form>
                <!--Fin Form Direcciones-->                   

                  <div class="clear"></div>
            </section>

            <section class="cont_cat_perfil" id="perfil3">
                <!--Titulo-->
                <div class="tittles">
                  <h1>MIS LISTAS DE DESEOS</h1>  
                </div>
                 <!--Fin Titulo--> 
                <ul class="cajasdir animated fadeInUp">
                    <li class="large-4 medium-6 small-12 columns item1">
                      <div class="caja_borde">
                        <span class="delete_deseo"><i class="fa fa-times"></i></span>
                        <h3>Nombre Deseo <div class="btn_edit"><i class="fa fa-pencil"></i></div></h3>
                        <p>Artículos: 0</p>
                        <p>Privacidad: Privado</p>
                        <p>Estado: Activo</p>
                      </div>                      
                    </li>
                    <li class="large-4 medium-6 small-12 columns item1">
                      <div class="caja_borde">
                        <span class="delete_deseo"><i class="fa fa-times"></i></span>
                        <h3>Nombre Deseo <div class="btn_edit"><i class="fa fa-pencil"></i></div></h3>
                        <p>Artículos: 0</p>
                        <p>Privacidad: Privado</p>
                        <p>Estado: Activo</p>
                      </div>                      
                    </li>
                    <li class="large-4 medium-6 small-12 columns item1">
                      <div class="caja_borde">
                        <span class="delete_deseo"><i class="fa fa-times"></i></span>
                        <h3>Nombre Deseo <div class="btn_edit"><i class="fa fa-pencil"></i></div></h3>
                        <p>Artículos: 0</p>
                        <p>Privacidad: Privado</p>
                        <p>Estado: Activo</p>
                      </div>                      
                    </li>
                </ul>
                  <div class="clear"></div>

                <!--Titulo-->
                <div class="tittles">
                  <h1>LISTAS DE DESEOS PUBLICAS</h1>  
                </div>
                 <!--Fin Titulo--> 
                 <ul class="cajasdir animated fadeInUp">
                    <li class="large-4 medium-6 small-12 columns item1">
                      <div class="caja_borde">
                        <span class="delete_deseo"><i class="fa fa-times"></i></span>                        
                        <h3>Tecnología <div class="btn_edit"><i class="fa fa-pencil"></i></div></h3>
                        <p>Artículos: 24</p>
                        <p>Privacidad: Publico</p>
                        <p>Estado: Activo</p>
                      </div>                      
                    </li>
                    <li class="large-4 medium-6 small-12 columns item1">
                      <div class="caja_borde">
                        <span class="delete_deseo"><i class="fa fa-times"></i></span>                        
                        <h3>Tecnología <div class="btn_edit"><i class="fa fa-pencil"></i></div></h3>
                        <p>Artículos: 24</p>
                        <p>Privacidad: Publico</p>
                        <p>Estado: Activo</p>
                      </div>                      
                    </li>
                    <li class="large-4 medium-6 small-12 columns item1">
                      <div class="caja_borde">
                        <span class="delete_deseo"><i class="fa fa-times"></i></span>                        
                        <h3>Tecnología <div class="btn_edit"><i class="fa fa-pencil"></i></div></h3>
                        <p>Artículos: 24</p>
                        <p>Privacidad: Publico</p>
                        <p>Estado: Activo</p>
                      </div>                      
                    </li>
                </ul>
                <div class="clear"></div>

                <div class="controles_perfil2">
                    <a href="javascript:void(0)" title="" class="btn_gris right margin-l btn_new_direc">AGREGAR NUEVA DIRECCIÓN</a>
                    <div class="clear"></div>
                 </div>   
                  
                  <div class="clear"></div>
            </section>

            <section class="cont_cat_perfil" id="perfil4">
                <!--Titulo-->
                <div class="tittles">
                  <h1>Hisotria</h1>  
                </div>

                <div class="resultados-busqueda">
              <div class="listabuscador">
                <div class="animated fadeInUp" data-wow-delay=".1s">
                  <div class="row">
                    <div class="small-12  medium-4 large-3 columns item1">
                      <img src="assets/images/productos/1.jpg" alt="">
                    </div>
                    <div class="small-12  medium-8 large-9 columns item2">
                      <h2>Consectetur adipiscing phasellus</h2>                      
                      <div class="clear"></div>                      
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores sapiente blanditiis culpa repudiandae.  unde? Accusantium ad doloremque, dolore. Quaerat, totam.
                      </p>                      
                      <div class="clear"></div>                      
                    </div>
                  </div>
                </div>
                <div class="animated fadeInUp" data-wow-delay=".1s">
                  <div class="row">
                    <div class="small-12  medium-4 large-3 columns item1">
                      <img src="assets/images/productos/2.jpg" alt="">
                    </div>
                    <div class="small-12  medium-8 large-9 columns item2">
                      <h2>Consectetur adipiscing phasellus</h2>                      
                      <div class="clear"></div>                      
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores sapiente blanditiis culpa repudiandae.  unde? Accusantium ad doloremque, dolore. Quaerat, totam.
                      </p>                      
                      <div class="clear"></div>                      
                    </div>
                  </div>
                </div>
                <div class="animated fadeInUp" data-wow-delay=".1s">
                  <div class="row">
                    <div class="small-12  medium-4 large-3 columns item1">
                      <img src="assets/images/productos/3.jpg" alt="">
                    </div>
                    <div class="small-12  medium-8 large-9 columns item2">
                      <h2>Consectetur adipiscing phasellus</h2>                      
                      <div class="clear"></div>                      
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores sapiente blanditiis culpa repudiandae.  unde? Accusantium ad doloremque, dolore. Quaerat, totam.
                      </p>                      
                      <div class="clear"></div>                      
                    </div>
                  </div>
                </div>
                <div class="animated fadeInUp" data-wow-delay=".1s">
                  <div class="row">
                    <div class="small-12  medium-4 large-3 columns item1">
                      <img src="assets/images/productos/5.jpg" alt="">
                    </div>
                    <div class="small-12  medium-8 large-9 columns item2">
                      <h2>Consectetur adipiscing phasellus</h2>                      
                      <div class="clear"></div>                      
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores sapiente blanditiis culpa repudiandae.  unde? Accusantium ad doloremque, dolore. Quaerat, totam.
                      </p>                      
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

                 <!--Fin Titulo-->   
                  <div class="clear"></div>
            </section>

          </section>
          <!--Fin Campos Perfil-->
      </section>


  </section>
  <!--Fin Contenidos Sitio-->

    <div class="clear"></div>

  <?php include("footer.php"); ?> 

</section><!--Content_all-->

</body>
</html>

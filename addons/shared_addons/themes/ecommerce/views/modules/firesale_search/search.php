<section class="marco">
   <article class="large-12 medium-12 small-12 columns relative lista_products">             
      <div class="destacados container">
       {{ if products }}
        {{ products }}
          <li class="large-3 medium-6 small-12 columns mix category-1" >
              <div class="marco_prod">
                <div class="img_prod">
                  <div class="action_share modal_all">
                      <?php if (module_enabled('firesale_wishlist')): ?> 
                        {{ if user:logged_in }} 
                        <a href="#wishlistModal" data-item-id="{{ id }}" data-toggle="modal" data-target="#wishlistModal" data-placement="top" data-original-title="Agregar a mi lista" data-effect="mfp-move-horizontal" class="wish_modal"><i class="fa fa-heart"></i></a> 
                        {{ endif }}
                      <?php endif; ?>
                      <a class="fb-ico facebook_popup link" data-placement="top" data-original-title="Compartir en Facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer.php?s=100&p[title]={{ title }}&p[url]={{ firesale:url route='product' id=id }}{{ if images.1.id }}&p[images][0]={{ url:site uri='files/large' }}/{{ images.1.id }}{{ endif }}">
                        <i class="fa fa-facebook"></i>
                      </a>
                      <a class="tw-ico twitter_popup link" data-placement="top" data-original-title="Compartir en Twitter" href="http://twitter.com/share?url={{ firesale:url route='product' id=id }}&text={{ title }}%20is%20awesome!{{ if {theme:options:twitter_account} }}&via={{ theme:options:twitter_account }}{{ endif }}">
                        <i class="fa fa-twitter"></i>
                      </a>
                  </div>
                  <div class="mask modal_all">
                     <a href="#modal_preview" data-href="{{url:base}}product/{{slug}}" id="quick_view" data-effect="mfp-3d-unfold">
                     <div class="btn_blanco">Vista rápida</div>
                     </a>
                  </div>
                  {{ if images }}
                    {{ if images.0.id }}
                    <img  src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }} alternate" />
                    {{ else }}
                    <img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                    {{ endif }}
                    {{ else }}
                    {{ theme:image file="no-image.png" alt=title }}
                  {{ endif }}
                    {{ if new_item AND descuento > '0' }}
                        <span class="etiq_new">NUEVO</span>
                        <span class="etiq_desc desc_bot">{{ descuento }}%<i>dscto</i></span>
                    {{ elseif descuento > '0' }}
                       <span class="etiq_desc">{{ descuento }}%<i>dscto</i></span>
                    {{ elseif new_item }}
                        <span class="etiq_new">NUEVO</span>
                    {{ endif }}
                </div>
                <div class="cont_head_cat color">
                    <div class="clear"></div>
                </div>
                <p>{{ title }}</p>
                {{ if descuento > '0' }}
                    <div class="precio precio_ant">${{ price_ant }}</div>
                    <div class="precio">{{ price_formatted }}</div>
                {{ else }}
                        <div class="precio">{{ price_formatted }}</div>
                {{ endif }}
                 <div class="clear"></div>
                <a href="{{ firesale:url route='product' id=id }}" title="">
                <div class="btn_border">
                  Detalle
                </div>  
                </a>
              </div>
          </li>
        {{ /products }}
        {{ else }}
        <div class="marco">
          <div class="large-12">
            <h2>No hay Productos</h2>
            <p class="center-text">(No te rindas - comprueba la ortografía o prueba con términos menos específicos)</p>
          </div>
        </div>
        {{ endif }}
        <div class="gap"></div>
        <div class="gap"></div>
        <div class="large-12">
            <!-- Pagination -->
            <div class="paging pagination-container">
                {{ pagination.links }}
            </div>
        </div>  

      </div>

  </article>
</section>
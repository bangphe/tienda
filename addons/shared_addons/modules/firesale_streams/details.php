<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Module_Firesale_streams extends Module
{
    public $version = '1.0';
    public $language_file = 'firesale_streams/firesale';

    public function __construct()
    {
        parent::__construct();

        // Load in the FireSale library
        $this->load->library('firesale/firesale');
        $this->lang->load($this->language_file);
    }

    public function info()
    {
        $info = array(
            'name' => array(
                'en' => 'FireSale Streams'
            ),
            'description' => array(
                'en' => 'Easily add/edit/remove fields from FireSale streams'
            ),
            'frontend'      => false,
            'backend'       => true,
            'author'        => 'Chris Harvey',
            'sections'      => array(
                'streams'   => array(
                    'name'      => 'firesale:sections:streams',
                    'uri'       => 'admin/firesale_streams',
                    'shortcuts' => array(
                        array(
                            'name'  => 'firesale:shortcuts:add_stream',
                            'uri'   => 'admin/firesale_streams/create',
                            'class' => 'add'
                        )
                    )
                )
            )
        );

        if ($this->uri->segment(3) == 'fields') {
            $stream = $this->uri->segment(4);

            $info['sections']['streams']['shortcuts'][0] = array(
                'name'  => 'firesale:shortcuts:add_field',
                'uri'   => "admin/firesale_streams/add_field/{$stream}",
                'class' => 'add'
            );
        }

        return $info;
    }

    public function admin_menu(&$menu)
    {
        $menu['lang:firesale:title']['lang:firesale:sections:streams'] = 'admin/firesale_streams';
    }

    public function install()
    {
        // We don't really need any install logic, oh the joys.
        if ($this->firesale->is_installed()) {
            return true;
        } else {
            return false;
        }
    }

    public function uninstall()
    {
        // We don't need any uninstall logic, oh the joys.
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        return "Some Help Stuff";
    }

}

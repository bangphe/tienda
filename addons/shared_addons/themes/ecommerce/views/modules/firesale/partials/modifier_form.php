<?php echo form_open("{{ firesale:url route='cart' base='no' }}/insert", 'id="modifier_form"'); ?> 
<input type="hidden" name="prd_code[0]" value="<?php echo $product['id']; ?>" />
<fieldset>
    <?php if (!empty($modifiers)): 
        $i = 1;
        ?>

        <div class="row detall_pop detalle_prod all_detalle_prod">
        <?php $f= 1; foreach ($modifiers as $modifier): ?>
          <div class="large-12 medium-12 small-12 ">
            <h2>
                <?php echo $modifier['title']; ?>
                <small><?php echo $modifier['instructions']; ?></small>
            </h2>                   
            <?php  if ($modifier['type']['key'] != '2'): ?>
                <?php if ($type == 'select'): ?>
                    <select name="options[0][<?php echo $modifier['id']; ?>]" data-id="<?php echo $modifier['id']; ?>" class="modifier_ajax modifier_<?php echo $i; ?>"> 
                    <?php
                    $i++;
                    endif; ?>
                     <?php if ($type == 'radio'): ?>
                        <ul class="tallas tallas<?php echo $f ?>" >
                    <?php endif; ?>
                    <?php foreach ($modifier['variations'] as $variation): ?>
                    
                        <?php if ($type == 'radio'): ?>
                            
                                <li>
                                    <input type="radio" name="options[0][<?php echo $modifier['id']; ?>]" id="options_<?php echo $variation['id']; ?>" value="<?php echo $variation['id']; ?>" <?php echo $variation['selected']; ?>/>
                                    <label for="options_<?php echo $variation['id']; ?>">
                                        <?php if (!empty($variation['image'])): ?>
                                        <img src="{{ url:site }}files/thumb/<?php echo $variation['image']; ?>/50/50/fit" title="<?php echo $variation['title']; ?>" class="img_ac"/>
                                        <?php else: ?>
                                           <div class="img_ac title_atribute"><?php echo $variation['title']; ?></div>
                                        <?php endif; ?>
                                        <?php if (preg_replace('/[^A-Za-z0-9]/', ' ', $variation['difference']) > 0): ?>
                                            (<?php echo $variation['difference']; ?>)
                                        <?php endif; ?>
                                    </label>
                                </li>
                           
                        <?php else: ?>
                            <option <?php echo $variation['selected']; ?> value="<?php echo $variation['id']; ?>"><?php echo $variation['title']; ?>
                                <?php if (preg_replace('/[^A-Za-z0-9]/', ' ', $variation['difference']) > 0): ?>
                                    (<?php echo $variation['difference']; ?>)
                                <?php endif; ?>
                            </option>
                        <?php endif; ?>
                    <?php  endforeach; ?>
                    <?php if ($type == 'radio'): ?>
                        </ul>
                    <?php endif; ?>
                    <?php if ($type == 'select'): ?>
                    </select>
                <?php endif; ?>
            <?php else: ?>
                <div class="large-6 medium-6 small-12 columns">
                    <textarea name="options[0][<?php echo $modifier['id']; ?>]"></textarea>
                </div>
            <?php endif; ?>
             <div class="clear"></div>
          </div>            
        <?php $f= $f+1; endforeach; ?>
        </div> 

            <div class="clear"></div>

    <?php endif; ?>

    <div class="row btns_red_agreg">       
        <div class="large-6  mediu6-4 small-12 columns padding_r">
            <h2  class="large-5  medium-4 small-6 columns padding_t">CANTIDAD</h2>
            <span  class="large-3  medium-3 small-6 columns">
                <input class="input-mini" name="qty[0]" size="3" value="1" type="text" />
            </span>
        </div> 
        <div class="large-6  medium-12 small-12 columns">
			<?php if($stock): ?>
                <button class="btn_border_blan btn_agregar" type="submit" name="btnAction" value="cart"><?php echo lang('firesale:product:label_add_to_cart'); ?></button>
            <?php else: ?>

            <?php endif; ?>
        </div>                    
    </div>

        <div class="clear"></div>
</fieldset>
<?php echo form_close(); ?>



	<h1>{{ list.title }}</h1>

	{{ if list.items }}
	<ul>
	{{ list.items }}
		<li>
			<a href="{{ firesale:url route='product' id=id }}">{{ title }} @ {{ price_formatted }} x {{ helper:number_format value=quantity }} = {{ price_total }}</a> |
			<a href="{{ firesale:url route='wishlist' }}/insert/{{ parent }}/{{ id }}/{{ plus }}">+</a> |
			<a href="{{ firesale:url route='wishlist' }}/insert/{{ parent }}/{{ id }}/{{ minus }}">-</a> |
			<a href="{{ firesale:url route='wishlist' }}/remove/{{ parent }}/{{ id }}">remove</a> |
			<a href="{{ firesale:url route='cart' }}/insert/{{ id }}/{{ quantity }}">Add to cart</a>
		</li>
	{{ /list.items }}
	</ul>
	{{ else }}
		<div class="alert notice"><?php echo sprintf(lang('firesale:wish:item:none'), '{{ firesale:url route="category" id="1" }}'); ?></div>
	{{ endif }}

	<br />
	<a href="{{ firesale:url route='wishlist' }}"><?php echo lang('firesale:wish:back'); ?></a>

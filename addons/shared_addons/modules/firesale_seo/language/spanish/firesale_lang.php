<?php defined('BASEPATH') or exit('No direct script access allowed');

    // General
    $lang['firesale:seo:title']    = 'Posicionamiento en buscadores';
    $lang['firesale:tab_metadata'] = 'Meta Data';

    // Labels
    $lang['firesale:seo:label_title']    = 'Meta Titulo';
    $lang['firesale:seo:label_desc']     = 'Meta Descripción';
    $lang['firesale:seo:label_keywords'] = 'Meta Palabras Clave';

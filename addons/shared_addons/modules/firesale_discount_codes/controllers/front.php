<?php defined('BASEPATH') or exit('No direct script access allowed');

class Front extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load the streams driver
        $this->load->driver('Streams');

        // Load the cart class
        $this->load->library('firesale/fs_cart');

        // Load the required models
        $this->load->model(array('codes_m', 'firesale/cart_m', 'firesale/orders_m', 'firesale/routes_m'));

        // Where in the world is the cart?
        $this->cart_route = $this->routes_m->build_url('cart');
    }

    public function apply_discount()
    {

        if ( ! $this->session->userdata('discount_code')) {

            $code = $this->input->post('code');

            $query = $this->db->get_where('firesale_discount_codes', array('code' => $code, 'used' => 'n'));

            if ($query->num_rows()) {

                $discount = $query->row();
                $entry_id = $discount->id;

                if ($discount->usage == 'once_user' AND empty($this->current_user)) {
                    $this->session->set_flashdata('error', lang('fs_discount_codes:message:login_required'));
                } elseif ($discount->start <= time() AND $discount->end >= time()) {
                    if ( ! empty($this->current_user)) {
                        $already_used = $this->db->get_where('firesale_orders', array(
                            'code_used' => $discount->id,
                            'created_by' => $this->current_user->id
                        ))->num_rows();
                    } else {
                        $already_used = FALSE;
                    }


                    if ($discount->usage != 'once_user' OR ! $already_used) {
                        $this->codes_m->build_original();
                        $this->codes_m->apply_discount($code);

                        // Does the cart have an order attached?
                        if ($this->cart_m->cart_has_order()) {
                            // Yup, update the order
                            $this->db->update('firesale_orders', array(
                                'code_used' => $code
                            ), array(
                                'id' => $this->session->userdata('order_id')
                            ));
                        }

                        Events::trigger('discount_applied', $discount);

                        $this->session->set_userdata('discount_code', $code);
                        $this->session->set_flashdata('success', sprintf(lang('fs_discount_codes:message:discount_applied'), $code));
                    } else {
                        $this->session->set_flashdata('error', lang('fs_discount_codes:message:already_used'));
                    }
                }

                redirect($this->cart_route);

            } else {
                $this->session->set_flashdata('error', lang('fs_discount_codes:message:invalid'));
            }

        } else {
            $this->session->set_flashdata('error', lang('fs_discount_codes:message:already_applied'));
        }

        redirect($this->cart_route);
    }

    public function remove_discount()
    {

        if ($this->session->userdata('discount_code')) {
            // Get cart contents
            $contents = $this->fs_cart->contents();

            foreach ($contents as $row_id => $item) {
                if (isset($item['orig_price']) AND isset($item['discount_code'])) {
                    // Remove product
                    $this->fs_cart->remove($row_id);

                    // Rebuild old information
                    $item['subtotal'] = $item['orig_price'] * $item['qty'];
                    $item['price'] = $item['orig_price'];

                    // Remove unwanted items
                    unset($item['orig_price']);
                    unset($item['orig_price_formatted']);
                    unset($item['discount_code']);

                    // Push back into cart
                    $this->fs_cart->insert($item);
                }
            }

            // Remove session data
            $this->session->unset_userdata('discount_code');

            // Does the cart have an order attached?
            if ($this->cart_m->cart_has_order()) {
                // Yup, remove the code from the order
                $this->db->update('firesale_orders', array(
                    'code_used' => NULL
                ), array(
                    'id' => $this->session->userdata('order_id')
                ));
            }

            // Set flash
            $this->session->set_flashdata('success', lang('fs_discount_codes:message:code_removed'));

        } else {
            $this->session->set_flashdata('error', lang('fs_discount_codes:message:code_removed_fail'));
        }

        redirect($this->cart_route);
    }
}

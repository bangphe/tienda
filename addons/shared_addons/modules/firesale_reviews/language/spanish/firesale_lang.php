<?php

    // Navigation
    $lang['firesale:sections:reviews'] = 'Calificaciones';
    $lang['firesale:reviews:ratings']  = 'Administrar valoración';

    // Labels
    $lang['firesale:reviews:title'] 	 = 'Calificación del productos';
    $lang['firesale:label_viewmore']	 = 'Ver mas';
    $lang['firesale:label_author'] 		 = 'Autor';
    $lang['firesale:label_product'] 	 = 'Productos';
    $lang['firesale:label_status']		 = 'Estado';
    $lang['firesale:label_rating']		 = 'Valoración';
    $lang['firesale:label_review']		 = 'Calificar';
    $lang['firesale:label_review_title'] = 'Titulo del comentario';
    $lang['firesale:label_filterprod']	 = 'Filtrar por producto';
    $lang['firesale:label_filterpsel']	 = 'Seleccione un producto';

    // Form
    $lang['firesale:reviews:form_title'] 	= 'Deja un comentario para el producto: %s';
    $lang['firesale:reviews:form_name'] 	= 'Nombre';
    $lang['firesale:reviews:form_email'] 	= 'Dirección de correo';
    $lang['firesale:reviews:form_comment'] 	= 'Comentario';
    $lang['firesale:reviews:form_submit']	= 'AGREGAR COMENTARIO';

    // Message
    $lang['firesale:reviews:none']				= 'No se encontraron comentarios';
    $lang['firesale:reviews:add_success']		= 'Comentario añadido con éxito';
    $lang['firesale:reviews:add_error']			= 'Fallo al agregar el comentario';
    $lang['firesale:reviews:add_login_error']	= 'Fallo al agregar el comentario por problemas de login';
    $lang['firesale:reviews:update_success']    = 'Comentario actualizado correctamente';
    $lang['firesale:reviews:update_error']      = 'Error actualizando comentario';
    $lang['firesale:reviews:status_success']	= 'Estado actualizado correctamente';
    $lang['firesale:reviews:status_error']		= 'Estado fallo al actualizar';
    $lang['firesale:reviews:delete_success']	= 'Comentario eliminado correctamente';
    $lang['firesale:reviews:delete_error']		= 'Comentario fallo al borrar';
    $lang['firesale:reviews:previous']			= 'Usted tiene una calificación previa de este producto';
    $lang['firesale:reviews:nouser']			= 'Debe estar registrado para comentar este producto';

    // Create items
    $lang['firesale:reviews:ratings:create'] = 'Crear nueva valoración';

    // Edit items
    $lang['firesale:reviews:ratings:edit'] = 'Editar valoración';


    // Dashboard
    $lang['firesale:reviews:latest_title']	= 'Ultimos comentarios';

    // Settings
    $lang['firesale:reviews:setting_accept'] = 'Comentarios moderados?';
    $lang['firesale:reviews:desc_accept']    = 'Requerir opiniones que deben ser moderadas antes de su publicación.';

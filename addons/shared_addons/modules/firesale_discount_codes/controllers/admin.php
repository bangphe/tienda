<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends Admin_Controller
{
    public $section = 'discounts';

    public function __construct()
    {
        parent::__construct();

        // Load in ze language file
        $this->lang->load('firesale_discount_codes/firesale');

        // We need ze streams driver
        $this->load->driver('Streams');

        // Load ze codes model
        $this->load->model('codes_m');

        // Me a lovea talkin' alikea zis
    }

    public function index()
    {

        $params = array(
            'stream' => 'firesale_discount_codes',
            'namespace' => 'firesale_discount_codes',
            'paginate' => 'yes',
            'page_segment' => 4
        );

        $data['codes'] = $this->streams->entries->get_entries($params);
        $data['codes'] = $data['codes']['entries'];

        foreach ($data['codes'] as &$code) {
            if ($code['applies_to']['key'] == '1') {
                $products = $this->db->select('firesale_products.id, firesale_products.title')
                                     ->join('firesale_products', 'firesale_products.id = firesale_discount_codes_firesale_products.firesale_products_id', 'left')
                                     ->get_where('firesale_discount_codes_firesale_products', array(
                                            'firesale_discount_codes_firesale_products.row_id' => $code['id']
                                     ));

                $code['products'] = array();

                foreach ($products->result() as $product) {
                    $code['products'][$product->id] = $product->title;
                }
            } elseif ($code['applies_to']['key'] == '2') {
                $categories = $this->db->select('firesale_categories.id, firesale_categories.title')
                                       ->join('firesale_categories', 'firesale_categories.id = firesale_discount_codes_firesale_categories.firesale_categories_id', 'left')
                                       ->get_where('firesale_discount_codes_firesale_categories', array(
                                            'firesale_discount_codes_firesale_categories.row_id' => $code['id']
                                        ));

                $code['categories'] = array();

                foreach ($categories->result() as $category) {
                    $code['categories'][$category->id] = $category->title;
                }
            }
        }

        $this->template->title(lang('firesale:title') . ' ' . lang('firesale:discounts:title'))
                       ->build('admin/index', $data);

    }

    public function create()
    {
        $stream = $this->streams->streams->get_stream('firesale_discount_codes', 'firesale_discount_codes');
        $fields = $this->streams_m->get_stream_fields($stream->id);

        // Prepare the form validation rules
        $rules = $this->fields->set_rules($fields, 'new', array(), TRUE);

        // Set the form validation rules
        $this->form_validation->set_rules($rules);

        // Run validation
        if ($this->form_validation->run()) {
            $this->streams->entries->insert_entry($this->input->post(), $stream, 'discount_codes');

            $this->session->set_flashdata('success', 'The discount code was created successfully');

            redirect('admin/firesale_discount_codes');
        } else {
            $built_fields = $this->fields->build_form($stream, 'new', $this->input->post(), FALSE, FALSE, array('used'));

            $data = $this->codes_m->build_fields($built_fields);

            $this->load->model('firesale/currency_m');
            $currency = $this->currency_m->get();

            $this->template->append_metadata('<script type="text/javascript">var currency = "' . $currency->symbol . '"</script>')
                           ->append_js('module::codes.js')
                           ->append_css('module::codes.css')
                           ->build('admin/form', $data);
        }
    }

    /**
      * @todo Merge this with the create method
      */
    public function edit($id)
    {
        $stream = $this->streams->streams->get_stream('firesale_discount_codes', 'firesale_discount_codes');
        $fields = $this->streams_m->get_stream_fields($stream->id);

        // Prepare the form validation rules
        $rules = $this->fields->set_rules($fields, 'edit', array(), TRUE, $id);

        // Set the form validation rules
        $this->form_validation->set_rules($rules);

        // Run validation
        if ($this->form_validation->run()) {
            $this->streams->entries->update_entry($id, $this->input->post(), $stream, 'discount_codes');

            $this->session->set_flashdata('success', 'The discount code was edited successfully.');

            redirect('admin/firesale_discount_codes');
        } else {
            $row = $this->row_m->get_row($id, $stream, FALSE);

            $built_fields = $this->fields->build_form($stream, 'edit', $row, FALSE, FALSE, array('used'));

            $data = $this->codes_m->build_fields($built_fields);

            $this->load->model('firesale/currency_m');
            $currency = $this->currency_m->get();

            $this->template->append_metadata('<script type="text/javascript">var currency = "' . $currency->symbol . '"</script>')
                           ->append_js('module::codes.js')
                           ->append_css('module::codes.css')
                           ->build('admin/form', $data);
        }
    }

    public function delete($id = NULL)
    {
        if (empty($id) AND $this->input->post('action_to')) {
            foreach ($this->input->post('action_to') as $id)
                $this->_delete($id);

            $this->session->set_flashdata('success', lang('fs_discount_codes:message:deleted_multiple'));
        } else {
            $this->_delete($id);
            $this->session->set_flashdata('success', lang('fs_discount_codes:message:deleted'));
        }

        redirect('admin/firesale_discount_codes');
    }

    private function _delete($id)
    {
        return $this->db->delete('firesale_discount_codes', array(
            'id' => $id
        ));
    }
}

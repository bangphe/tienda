FireSale Reviews
================

* Website: http://www.getfiresale.org
* Documentation: http://docs.getfiresale.org
* License: Included with release
* Version: 1.0

Add amazing product reviews and management tools simply and quickly, just install and add the required tags and it's done! On the front-end of the site we have provided a number of Plugin tags to allow for developers and designers alike to quickly and easily include not only the reviews but also display star ratings on products, categories, etc. and include the form to add a review with simple LEX tags.

### Installation

1. Create a new folder in your chosen addons directory named "firesale_reviews"
2. Drop in the files included with this release
3. Navigate to your control panel and goto add-ons
4. Enable the FireSale Reviews module
5. Start adding lex tags to your theme!

### The Review Form

Out of the box the LEX tags build the default form for you (along with any additional fields added via streams) or it can be customised within your theme to match any layout you may desire. The slider for the ratings starts life as a dropdown and is then altered via JavaScript to be more user friendly, if you like you could also turn it into a star-rating system or leave it as a dropdown selection - that's up to you.

You will also be given a number of options on the administration side to allow users to review without being logged in - which is disabled by default. If you aren't logged in additional fields for name and email are displayed, as well as an option to automatically display reviews or have them placed in draft mode for an admin to approve before being counted and displayed - which is required by default.

To add the form, use the following LEX tag

	{{ firesale_reviews:form }}

### Reviews

Once a review has been made another LEX tag then starts to do its' work and that is to display the reviews on the page, we provide all the information we can about the user if they're logged in as well as the product itself. This was done to give as much information as may be required by a client so you don't need to go editing anything if the information you want isn't displayed already. After a review has been made they will be shown using the following tags (with an example layout):

	{{ firesale_reviews:reviews width="75" }}
              <article class="review">
                <header>
                  <div class="left"><strong><a href="/profile/{{ created_by.username }}">{{ created_by.username }}</a></strong><time> {{ created }}</time></div>
                  <div class="right"><span class="stars"><span style="width: {{ width }}px"></span></span><strong>{{ rating.value }} out of {{ stars }}</strong></div>
                </header>
                <p>{{ review }}</p>
              </article>
	{{ /firesale_reviews:reviews }}

The width variable is the width of your star rating so we can figure out how wide it should be for the given rating.

### Stars

These are the star ratings viewed on various areas of the sites and can be used with any product or on a category page. They use the same format as above for the reviews and allow you to choose the product and width for your css:

	{{ firesale_reviews:product_rating width=75 }}<span class="stars"><span style="width: {{ width }}px"></span>{{ rating }}</span>{{ /firesale_reviews:product_rating }}

### Feedback and Issues

If you have any issues please email (support@getfiresale.org)[support@getfiresale.org] and either report your issue or request access to the GitHub repo and we will consider the issue/request as soon as we can.
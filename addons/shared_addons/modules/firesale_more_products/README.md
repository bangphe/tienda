FireSale More Products
======================

* Website: http://www.getfiresale.org
* Documentation: http://docs.getfiresale.org
* License: Included with release
* Version: 1.1

More products is an additional module for [FireSale](http://www.getfiresale.org) that has a number of great features rolled in to one to show additional products to customers. It is designed to entice them to buy/view more products and provides all of this to you and your theme on almost any page through the use of a few very simple lex tags via the plugin. These features include:

* Related Products
* Recently Viewed
* Also Bought
* Just Added to Cart (Coming in version 1.2)

### Installation

1. Create a new folder in your chosen addons directory named "firesale_more_products"
2. Drop in the files included with this release
3. Navigate to your control panel and goto add-ons
4. Enable the FireSale More Products module
5. Start adding lex tags to your theme!

### Related Products

Related products uses a number of aspects of a given product to generate a list of possible products that are related to this product. By default it tracks around 20 products but can be altered in the settings in your admin.

To use this in your theme, you can use the following LEX tags:

	{{ firesale_more_products:related_products product="PRODUCT ID" [use_cat="true|false"] }}

		Within these tags you have a complete product object to use, including images, categories, etc.
		To add these to your layout you can use {{ title }}, {{ price }}, {{ category }} {{ /category }}, etc.

	{{ /firesale_more_products:related_products }}

The use_cat paramater is optional and will only show products with related categories, this makes the results more accurate but can reduce the number of results greatly.

### Recently Viewed

This tracks the last 20 (again can be changed in settings) products that the user has viewed, they are stored in a JSON string stored in session and can be removed by the user at any time. After installation of the module these will automatically be tracked regardless of tags being included or not and can be shown on any page you wish.

To show the recently viewed products you can use the following LEX tags:

	{{ firesale_more_products:recently_viewed }}

		Again you are given a complete product object to display

	{{ /firesale_more_products:recently_viewed }}

When creating your layout, if you want to allow users to remove a specific product from their viewed list you can include the following link:

	{{ url:base }}firesale_more_products/front/remove_recent/{{ id }}

You can also install the following route to make the link a little nicer

	$route['remove/([0-9]+)'] = 'firesale_more_products/front/remove_recent/$1';

	Making the link required:

	{{ url:base }}remove/{{ id }}

### Also Bought

Also bought displays the products that were also purchased by customers at the same time as the given product. To use it in your theme use the following tags:

	{{ firesale_more_products:also_bought product="PRODUCT ID" [limit="NUMERIC"] }}

		Again, full product object

	{{ /firesale_more_products:albso_bought }}

### Just Added to Cart

This adds a new page that you are automatically sent to following the addition of a product to the cart, this gives you a potentially last chance to show your customers products from the above features that they may be interested in before they checkout. This feature is not currently live, but will be coming in the first update very soon - with the option to turn it on or off in settings. This page will be fully customisable with it's own view and will provide the current product object that has been added to display.

### Feedback and Issues

If you have any issues please email (support@getfiresale.org)[support@getfiresale.org] and either report your issue or request access to the GitHub repo and we will consider the issue/request as soon as we can.

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Brayan Acebo
 */
class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('quienes_somos');
        $this->template
                ->append_js('module::developer.js')
                ->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));
        $this->load->model(array('quienes_somos_m'));
    }

    /**
     * List all domains
     */
    public function index() {
        $result = $this->quienes_somos_m->get_all();
        $post = array();

        if (count($result) > 0) {
            $post = $result[0];
        }

        $this->template->set('data', $post)
                ->build('admin/quienes_somos_back');
    }

    public function edit() {

        $this->form_validation->set_rules('titulo', 'Titulo', 'required|max_length[455]');
        $this->form_validation->set_rules('video', 'Video', '');
        $this->form_validation->set_rules('texto', 'Texto', 'required');

        if ($this->form_validation->run() === TRUE) {

            $post = (object) $this->input->post();

            $data = array(
                'titulo' => $post->titulo,
                'video' => $post->video,
                'texto' => html_entity_decode($post->texto)
            );

            $config['upload_path'] = UPLOADSFOLDER . 'quienes_somos';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2050;
            $config['max_width'] = 1000;
            $config['max_height'] = 700;
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);

            $img = $_FILES['imagen']['name'];

            if (!empty($img)) {
                if ($this->upload->do_upload('imagen')) {
                    $datos = array('upload_data' => $this->upload->data());
                    $path = str_replace(UPLOADSFOLDER, '', $datos['upload_data']['full_path']);
                    $img = array('imagen' => $path);
                    $data = array_merge($data, $img);
                    $obj = $this->db->where('id', $post->id)->get('quienes_somos')->row();
                    @unlink(UPLOADSFOLDER . $obj->imagen);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    return $this->index();
                }
            }

            if ($this->quienes_somos_m->update_all($data)) {
                // insert ok, so
                $this->session->set_flashdata('success', lang('quienes_somos:success_message'));
                redirect('admin/quienes_somos/');
            } else {
                $this->session->set_flashdata('error', lang('quienes_somos:error_message'));
                redirect('admin/quienes_somos/');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            return $this->index();
        }
    }

}

	<h1>{{ list.title }}</h1>

	{{ if list.items }}
	<ul>
	{{ list.items }}
		<li>
			<a href="{{ firesale:url route='product' id=id }}">{{ title }} @ {{ price_formatted }} x {{ helper:number_format value=quantity }} = {{ price_total }}</a> |
			<a href="{{ firesale:url route='wishlist' }}/insert/{{ parent }}/{{ id }}/{{ plus }}">+</a> |
			<a href="{{ firesale:url route='wishlist' }}/insert/{{ parent }}/{{ id }}/{{ minus }}">-</a> |
			<a href="{{ firesale:url route='wishlist' }}/remove/{{ parent }}/{{ id }}">remove</a> |
			<a href="{{ firesale:url route='cart' }}/insert/{{ id }}/{{ quantity }}">Add to cart</a>
		</li>
	{{ /list.items }}
	</ul>
	{{ else }}
		<div class="alert notice"><?php echo sprintf(lang('firesale:wish:item:none'), '{{ firesale:url route="category" id="1" }}'); ?></div>
	{{ endif }}

	<br />
	<a href="{{ firesale:url route='wishlist' }}"><?php echo lang('firesale:wish:back'); ?></a>

	<div class="sixteen columns alpha omega wishlist">
		<div class="title">{{ list.title }}</div>
		<div class="table">
			<table>
				<thead>
					<tr class="headers">
						<td class="quantity">Quantity</td>
						<td class="details">Item Details</td>
						<td class="price">Price</td>
						<td class="total"></td>
					</tr>
				</thead>
				<tbody>
				{{ list.items }}
					<tr class="items">
						<td class="quantity"><input type="text" name="qty[{{ id }}]" value="{{ quantity }}" /></td>
						<td class="details">
							<img class="product" src="img/product1.jpg" alt="">
							<div class="productname"><p>{{ title }}</p></div>
							<a href="{{ firesale:url route='wishlist' }}/remove/{{ parent }}/{{ id }}">{{ theme:image file="trash.png" alt="Remove" }}</a>
						</td>
						<td class="price">{{ price_formatted }}</td>
						<td class="total"><a href="{{ firesale:url route='cart' }}/insert/{{ id }}/{{ quantity }}" class="btn">Add to Bag</a></td>
					</tr>
				{{ /list.items }}
				</tbody>
			</table>
		</div>
	</div>
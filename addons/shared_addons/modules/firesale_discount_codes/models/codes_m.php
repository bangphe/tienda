<?php defined('BASEPATH') or exit('No direct script access allowed');

class Codes_m extends MY_Model
{

    public $fired = FALSE;

    public function __construct()
    {
        parent::__construct();

        // We need ze streams driver
        $this->load->driver('Streams');

        // We also need currency
        $this->load->model('firesale/currency_m');
    }

    public function current_id()
    {
        if ($code = $this->session->userdata('discount_code')) {
            return (string) $this->db->get_where('firesale_discount_codes', array(
                'code' => $code
            ))->row()->id;
        }

        return FALSE;
    }

    public function get_code($id, $return = 'array')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);
        } else {
            $this->db->where('code', $id);
        }

        $query = $this->db->get('firesale_discount_codes');

        return $return == 'array' ? $query->row_array() : $query->row();
    }

    public function build_original()
    {
        if ($this->fs_cart->total_items()) {
            $this->load->model('firesale/taxes_m');
            $this->fs_cart->orig_total = 0;

            // Build the pricing
            foreach ($this->fs_cart->contents() as $item) {
                $price = (isset($item['orig_price']) ? $item['orig_price'] : 1) * $item['qty'];
                $this->fs_cart->orig_total += $price;

                $percentage = $this->taxes_m->get_percentage($item['tax_band']);
				if(isset($this->fs_cart->orig_tax))
				{
					$this->fs_cart->orig_tax += ($price / 100) * $percentage;
				}
            }
			if(isset($this->fs_cart->orig_subtotal))
			{
            	$this->fs_cart->orig_subtotal = $this->fs_cart->orig_total - $this->fs_cart->orig_tax;
			}

        } else {
            $this->fs_cart->orig_total = '0.00';
            $this->fs_cart->orig_tax = '0.00';
            $this->fs_cart->orig_subtotal = '0.00';
        }

    }

    public function build_fields($fields)
    {
        $data['fields'] = $fields;

        foreach ($data['fields'] as $key => &$field) {
            if ($field['input_slug'] == 'type') {
                unset($data['fields'][$key]);
                $type_field = $field;
            }

            if ($field['input_slug'] == 'value')
                $field['type_field'] = $type_field;

            if ($field['input_slug'] == 'applies_to')
                $data['applies_to'] = $field['value'];
        }

        return $data;
    }

    public function apply_discount($code)
    {

        foreach ($this->fs_cart->contents() as $row_id => $item) {

            // Remove the item
            $this->fs_cart->remove($row_id);

            $discount = $this->db->get_where('firesale_discount_codes', array('code' => $code, 'used' => 'n'))->row();

            $products = $this->db->get_where('firesale_discount_codes_firesale_products', array(
                'firesale_products_id' => $item['id'],
                'row_id' => $discount->id
            ));


            $this->db->join('firesale_products_firesale_categories', 'firesale_products_firesale_categories.firesale_categories_id = firesale_discount_codes_firesale_categories.firesale_categories_id', 'left');
            $categories = $this->db->get_where('firesale_discount_codes_firesale_categories', array(
                'firesale_products_firesale_categories.row_id'       => $item['id'],
                'firesale_discount_codes_firesale_categories.row_id' => $discount->id
            ));

            if ($discount->applies_to == '0' OR ($discount->applies_to == '1' AND $products->num_rows()) OR ($discount->applies_to == '2' AND $categories->num_rows())) {
                // Set the old price (before discount)
                $item['orig_price'] = isset($item['orig_price']) ? $item['orig_price'] : $item['price'];

                if ($discount->type == 'percentage') {
                    // Take off the percentage
                    $item['price'] = round($item['orig_price'] - ($item['orig_price'] * ($discount->value / 100)), 2);
                } else {
                    // Take off a fixed amount
                    $item['price'] = $item['orig_price'] - $discount->value;

                    // Don't allow the price to go below zero (but allow zero)
                    if ($item['price'] < 0)
                        $item['price'] = 0;
                }

                if ($item['price'] != $item['orig_price'])
                    $item['discount_code'] = $code;

                //1.2 compatibility
                $item['modified'] = true;
            }

            $item['orig_price_formatted'] = $this->currency_m->format_string($item['orig_price'], $this->fs_cart->currency());

            // We will not be using the subtotal here, this will be calculated automatically
            unset($item['subtotal']);

            $this->fs_cart->insert($item);
        }
    }

    public function cart_updated()
    {
        if ($code = $this->session->userdata('discount_code')) {
            $this->apply_discount($code);
        } else {
            return FALSE;
        }
    }

    public function page_build($template)
    {

        if ($this->uri->segment(1) == 'basket' AND ! $this->fired) {

            $this->build_original();

            $data = array(
                'orig_total'    => number_format($this->fs_cart->orig_total, 0),
                'orig_tax'	    => number_format($this->fs_cart->orig_tax, 0),
                'orig_subtotal' => number_format($this->fs_cart->orig_subtotal, 0),

                'orig_total_formatted'    => $this->currency_m->format_string($this->fs_cart->orig_total, $this->fs_cart->currency()),
                'orig_tax_formatted'      => $this->currency_m->format_string($this->fs_cart->orig_tax, $this->fs_cart->currency()),
                'orig_subtotal_formatted' => $this->currency_m->format_string($this->fs_cart->orig_subtotal, $this->fs_cart->currency())
            );

            $template->set($data);

            $this->fired = TRUE;

        }

    }

}

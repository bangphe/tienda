<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* This file is part of FireSale, a PHP based eCommerce system built for
* PyroCMS.
*
* Copyright (c) 2013 Moltin Ltd.
* http://github.com/firesale/firesale
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @package firesale/design
* @author FireSale <support@getfiresale.org>
* @copyright 2013 Moltin Ltd.
* @version master
* @link http://github.com/firesale/firesale
*
*/

    // General
    $lang['firesale:design:title'] = 'Diseño Firesale';
    $lang['firesale:tabs:design']  = 'Diseño';

    // Pages
    $lang['firesale:design:pages:products']   = 'Productos';
    $lang['firesale:design:pages:categories'] = 'Categorias';
    $lang['firesale:design:pages:brands']     = 'Marcas';

    // Settings
    $lang['firesale:design:settings:enable'] = 'Activar Opciones de Diseño Para';

    // Labels
    $lang['firesale:design:label_enable']  = 'Activar Controles de Diseño';
    $lang['firesale:design:label_element'] = 'Elemento';
    $lang['firesale:design:label_type']    = 'Tipo';
    $lang['firesale:design:label_layout']  = 'Disposición';
    $lang['firesale:design:label_view']    = 'Vista';

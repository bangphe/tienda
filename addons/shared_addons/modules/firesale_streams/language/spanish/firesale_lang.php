<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['firesale:sections:streams']     = 'Streams';

$lang['firesale:title:add_field']      = 'Agregar Campo';
$lang['firesale:title:edit_field']     = 'Editar Campo';

$lang['firesale:shortcuts:add_stream'] = 'Agregar Stream';
$lang['firesale:shortcuts:add_field']  = 'Add Campo';
$lang['firesale:button:fields']        = 'Campos';

$lang['firesale:label_streamname']     = 'Nombre del Stream';
$lang['firesale:label_streamslug']     = 'Slug del Stream';
$lang['firesale:label_streamprefix']   = 'Este será automáticamente el prefijo "firesale_"';
$lang['firesale:label_name']           = 'Nombre';
$lang['firesale:label_namespace']      = 'Nombre de espacio';
$lang['firesale:label_type']           = 'Tipo de Campo';
$lang['firesale:label_required']       = 'Requerido?';
$lang['firesale:label_unique']         = 'Unico?';
$lang['firesale:label_fieldsin']       = 'Campos en "%s"';

$lang['firesale:streams:no_fields']    = 'Actualmente no hay campos en el stream';
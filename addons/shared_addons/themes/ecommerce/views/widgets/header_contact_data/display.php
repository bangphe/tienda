<li class="redes_head">
{{ if data:facebook }}
  <a href="{{data:facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
{{ endif }}
{{ if data:twitter }}
  <a href="{{data:twitter}}" target="_blank"><i class="fa fa-twitter"></i></a>
{{ endif }}
{{ if data:linkedin }}
  <a href="{{data:linkedin}}" target="_blank"><i class="fa fa-instagram"></i></a>
{{ endif }}
{{ if data:youtube }}
  <a href="{{data:youtube}}" target="_blank"><i class="fa fa-youtube-play"></i></a>
{{ endif }}
</li>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <div class="redes hide">  
        {{ if data:facebook }}        
          <a class="face" href="" target="_blank"><i class="fa fa-facebook"></i></a> 
        {{ endif }}   
        {{ if data:twitter }}        
          <a class="twitter" href="" target="_blank"><i class="fa fa-twitter"></i></a>
        {{ endif }}
        {{ if data:google }} 
          <a class="google" href="" target="_blank"><i class="fa fa-google-plus"></i></a>
        {{ endif }}
        {{ if data:linkedin }}
          <a class="linkedin" href="" target="_blank"><i class="fa fa-linkedin"></i></a>
        {{ endif }}
          <a class="intagram" href="" target="_blank"><i class="fa fa-instagram"></i></a>
        </div>
         <div class="clear"></div>
       {{ if data:adress }} {{data:adress}} {{ endif }}  {{ if data:phone }} - {{ data:phone }} {{ endif }} {{ if data:email }} - <a href="mailto:{{ data:email }}">{{ data:email }}</a> {{ endif }}  
		{{ if terms_cond }} - <a target="_blank" href="{{ url:site }}{{terms_cond}}">Términos y condiciones</a>{{ endif }}
</div>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* Este modelo es para convertir una consulta de la base de datos y mandarla a un excel
*
*/

class to_excel_m extends MY_Model
{
    /**
     * Loads the parent constructor and gets an
     * instance of CI.
     *
     * @return void
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        // cargamos la libreria de excel
        $this->load->library('firesale/to_excel');
    }

    // pasar un query sql de codeigniter a excel
    function to_excel($datosSql, $filename='exceloutput')
    {
         $encabezadoExcel = '';  // just creating the var for field headers to append to below
         $datosExcel = '';  // just creating the var for field data to append to below
         
         $obj = & get_instance();
         
         if ($datosSql->num_rows() == 0)
         {
              echo '<p>La tabla no tiene datos.</p>';
         }
         else 
         {
            foreach($datosSql->result_array() as $row => $value)
            {
                $encabezadoExcel = '';
                //echo $row.'----'.$value.'<br/>';
                foreach($value as $row2 => $value2)
                {
                    //echo $row2.'----'.$value2.'<br/>';
                    $encabezadoExcel .= $row2."\t";
                }
            }
         
            foreach ($datosSql->result() as $row)
            {
                $lineaExcel = '';
                foreach($row as $value)
                {                                            
                    if ((!isset($value)) OR ($value == ""))
                    {
                        $value = "\t";
                    }
                    else
                    {
                        $value = str_replace('"', '""', $value);
                        $value = '"' . $value . '"' . "\t";
                    }
                    $lineaExcel .= $value;
               }
               $datosExcel .= trim($lineaExcel)."\n";
            }
              
            $datosExcel = str_replace("\r","",$datosExcel);
            
            // pasar los datos de utf-8 a ansi
            $datosExcel = iconv('UTF-8', 'ISO-8859-1', $datosExcel);
            $encabezadoExcel = iconv('UTF-8', 'ISO-8859-1', $encabezadoExcel);
            
            //header("Content-type: application/x-msdownload; charset=utf-8"");
            //header("Content-type: application/vnd.ms-excel; charset=utf-8");  //for Excel 5/2003 formats
            header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");  // for Excel 2007
            //header('Content-type: text/csv; charset=ISO-8859-1');
            header("Content-Disposition: attachment; filename=$filename.xls");
            echo "$encabezadoExcel\n$datosExcel";
         }
    }
	
	function order_to_excel($orders, $filename='order')
    {
		$datosExcel = '';
		foreach($orders AS $order)
		{
			// Colocamos los titulos
			$value = 'Numero pedido '."\t".' Nick '."\t".' Nombre '."\t".' Correo '."\t".' Estado '."\t".' Metodo '."\t".' Sub Total '."\t".' Envio '."\t".' Total '."\t".' Código Usado '."\t\n".'';
			$value = str_replace('"', '""', $value);
			$datosExcel .= $value;
			// Organizamos los datos del pedido
			$datosExcel .= str_replace('"', '""', $order['id']."\t");
			if(!empty($order['created_by']))
			{
				$datosExcel .= str_replace('"', '""', $order['created_by']['username']."\t");
				$datosExcel .= str_replace('"', '""', $order['created_by']['display_name']."\t");
				$datosExcel .= str_replace('"', '""', $order['created_by']['email']."\t");
			}
			else
			{
				$datosExcel .= str_replace('"', '""',"\t \t \t");
			}
			if(!empty($order['order_status']))
			{
				$datosExcel .= str_replace('"', '""', $order['order_status']['value']);
			}
			// siguiente columna
			$datosExcel .= str_replace('"', '""',"\t");
			if(!empty($order['gateway']))
			{
				$datosExcel .= str_replace('"', '""', $order['gateway']['name']);
			}
			$datosExcel .= str_replace('"', '""',"\t");
			$datosExcel .= str_replace('"', '""', $order['price_sub']);
			$datosExcel .= str_replace('"', '""',"\t");
			$datosExcel .= str_replace('"', '""', $order['price_ship']);
			$datosExcel .= str_replace('"', '""',"\t");
			$datosExcel .= str_replace('"', '""', $order['price_total']);
			$datosExcel .= str_replace('"', '""',"\t");
			if(!empty($order['code_used']))
			{
				$datosExcel .= str_replace('"', '""', $order['code_used']['code']);
			}
			// salto de linea
			$datosExcel .= str_replace('"', '""',"\t\n");
			$datosExcel .= str_replace('"', '""',"\t\n");
			
			// consultamos los productos y les damos formato
            $products = cache('orders_m/order_products', $order['id']);
			if(!empty($products['products']))
			{
				// titulos del producto
				$value = 'Producto '."\t".' Código '."\t".' Precio Unt. '."\t".' Cantidad '."\t".' Total '."\t\n".'';
				$value = str_replace('"', '""', $value);
				$datosExcel .= $value;
				foreach ($products['products'] AS $product)
				{
					$datosExcel .= str_replace('"', '""', $product['name']);
					$datosExcel .= str_replace('"', '""',"\t");
					$datosExcel .= str_replace('"', '""', $product['code']);
					$datosExcel .= str_replace('"', '""',"\t");
					$datosExcel .= str_replace('"', '""', $product['price']);
					$datosExcel .= str_replace('"', '""',"\t");
					$datosExcel .= str_replace('"', '""', $product['qty']);
					$datosExcel .= str_replace('"', '""',"\t");
					$datosExcel .= str_replace('"', '""', $product['price'] * $product['qty']);
					$datosExcel .= str_replace('"', '""',"\t\n");
				}
			}
			
			// salto de linea
			$datosExcel .= str_replace('"', '""',"\t\n");
			$datosExcel .= str_replace('"', '""',"\t\n");
		}
		
		$datosExcel = str_replace("\r","",$datosExcel);
        
		// pasar los datos de utf-8 a ansi
		$datosExcel = iconv('UTF-8', 'ISO-8859-1', $datosExcel);
		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");  // for Excel 2007
		header("Content-Disposition: attachment; filename=$filename.xls");
		echo "$datosExcel";
    }

    public function order_to_excel_servientrega($orders, $filename='order')
    {
    	$datosExcel = '';
    	// Colocamos los titulos
		$value = 'Numero de referencia '."\t".' Ciudad/Cod DANE de Origen '."\t".' Numero de Guia '."\t".' Tiempo de Entrega '."\t".' Generar Sobreporte '."\t".' Documento de Identificacion '."\t".' Nombre del Destinatario '."\t".' Direccion '."\t".' Ciudad/Cod DANE de destino '."\t".' Departamento '."\t".' Telefono '."\t".' Correo Electronico Destinatario '."\t".' Celular '."\t".' Departamento de Origen '."\t".' Direccion Remitente '."\t".' Nombre de la Unidad de Empaque '."\t".' Dice Contener '."\t".' Valor declarado '."\t".' Numero de Piezas '."\t".' Cantidad '."\t".' Remision '."\t".' Alto '."\t".' Ancho '."\t".' Largo '."\t".' Peso '."\t".' Producto '."\t".' Empaque y Embalaje '."\t".' Forma de Pago '."\t".' Medio de Transporte '."\t".' Campo personalizado 1 '."\t".' Generar Cajaporte '."\t".' Contenido Sobreporte o Cajaporte '."\t".' Valor Declarado Sobreporte o cajaporte '."\t".' Bolsa de seguridad '."\t".' Precinto '."\t".' Identificador de Archivo Origen '."\t".' Unidad de longitud '."\t".' Unidad de peso '."\t".' Factura '.' Codigo de Facturacion '."\t\n".'';
		$value = str_replace('"', '""', $value);
		$datosExcel .= $value;
		foreach($orders AS $order)
		{
			/*echo "<pre> ";
			print_r($order);
			echo "</pre> ";
			exit();*/
			// Organizamos los datos del pedido
			// llenamos campos vacios o default
			$datosExcel .= str_replace('"', '""',"\t"); // Número de referencia
			$datosExcel .= str_replace('"', '""',"\t"); // Ciudad/Cód DANE de Origen
			$datosExcel .= str_replace('"', '""',"\t"); // Número de Guia
			$datosExcel .= str_replace('"', '""', '1'."\t"); // Tiempo de Entrega
			$datosExcel .= str_replace('"', '""', '0'."\t"); // Generar Sobreporte
			
			// los datos del cliente
			if(!empty($order['bill_to']))
			{
				$datosExcel .= str_replace('"', '""', $order['bill_to']['cedule']."\t"); // Documento de Identificación
				$datosExcel .= str_replace('"', '""', $order['bill_to']['firstname'].' '.$order['bill_to']['lastname']."\t"); // Nombre del Destinatario
				$datosExcel .= str_replace('"', '""', $order['bill_to']['address1']."\t"); // Dirección
				// buscamos la ciudad
				if(!empty($order['bill_to']['id_city']))
				{
					$data_base = $this->db->select('name')
						->from('firesale_city')
						->where('id',$order['bill_to']['id_city'])->get()->row();
					$datosExcel .= str_replace('"', '""', $data_base->name."\t"); // Ciudad/Cód DANE de destino
				}
				else
				{
					$datosExcel .= str_replace('"', '""',"\t"); // Ciudad/Cód DANE de destino
				}
				// buscamos el departamento
				if(!empty($order['bill_to']['id_deparment']))
				{
					$data_base = $this->db->select('name')
						->from('firesale_department')
						->where('id',$order['bill_to']['id_deparment'])->get()->row();
					$datosExcel .= str_replace('"', '""', $data_base->name."\t"); // Departamento
				}
				else
				{
					$datosExcel .= str_replace('"', '""',"\t"); // Departamento
				}
				$datosExcel .= str_replace('"', '""', $order['bill_to']['phone']."\t"); // Teléfono
				$datosExcel .= str_replace('"', '""', $order['bill_to']['email']."\t"); // Correo Electrónico Destinatario
			}
			else
			{
				$datosExcel .= str_replace('"', '""',"\t \t \t \t \t \t \t");
			}
			// llenamos campos vacios o default
			$datosExcel .= str_replace('"', '""',"\t"); // Celular
			$datosExcel .= str_replace('"', '""',"\t"); // Departamento de Origen
			$datosExcel .= str_replace('"', '""',"\t"); // Direccion Remitente
			$datosExcel .= str_replace('"', '""', 'GENERICA'."\t"); // Nombre de la Unidad de Empaque
			$datosExcel .= str_replace('"', '""', 'PRENDAS DEPORTIVAS'."\t"); // Dice Contener

			$datosExcel .= str_replace('"', '""', $order['price_sub']."\t"); // Valor declarado

			$datosExcel .= str_replace('"', '""', '1'."\t"); // Número de Piezas
			$datosExcel .= str_replace('"', '""', '1'."\t"); // Cantidad
			$datosExcel .= str_replace('"', '""', 'opcional'."\t"); // Remisión
			$datosExcel .= str_replace('"', '""', '10'."\t"); // Alto
			$datosExcel .= str_replace('"', '""', '10'."\t"); // Ancho
			$datosExcel .= str_replace('"', '""', '10'."\t"); // Largp
			$datosExcel .= str_replace('"', '""', '10'."\t"); // Peso
			//$datosExcel .= str_replace('"', '""', '2'."\t"); /*$datosExcel .= str_replace('"', '""', $order['count']."\t");*/ // Producto
			$datosExcel .= str_replace('"', '""', $order['shipping']['type_packaging']."\t");
			$datosExcel .= str_replace('"', '""',"\t"); // Empaque y Embalaje
			$datosExcel .= str_replace('"', '""', '2'."\t"); // Forma de Pago
			$datosExcel .= str_replace('"', '""', '1'."\t"); // Medio de Transporte

			if(!empty($order['bill_to']))
			{
				$datosExcel .= str_replace('"', '""', $order['bill_to']['observation']."\t"); // Campo personalizado 1
			}
			else
			{
				$datosExcel .= str_replace('"', '""',"\t"); // Campo personalizado 1
			}

			$datosExcel .= str_replace('"', '""', '0'."\t"); // Generar Cajaporte
			$datosExcel .= str_replace('"', '""',"\t"); // Contenido Sobreporte o Cajaporte
			$datosExcel .= str_replace('"', '""',"\t"); // Valor Declarado Sobreporte o cajaporte
			$datosExcel .= str_replace('"', '""',"\t"); // Bolsa de seguridad
			$datosExcel .= str_replace('"', '""',"\t"); // Precinto
			$datosExcel .= str_replace('"', '""',"\t"); // Identificador de Archivo Origen
			$datosExcel .= str_replace('"', '""', 'CM'."\t"); // Unidad de longitud
			$datosExcel .= str_replace('"', '""', 'KG'."\t"); // Unidad de peso
			$datosExcel .= str_replace('"', '""','SER94829'."\t"); // Factura
			$datosExcel .= str_replace('"', '""',"\t"); // Codigo de Facturación

			// salto de linea
			$datosExcel .= str_replace('"', '""',"\t\n");
		}
		
		$datosExcel = str_replace("\r","",$datosExcel);
        
		// pasar los datos de utf-8 a ansi
		$datosExcel = iconv('UTF-8', 'ISO-8859-1', $datosExcel);
		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");  // for Excel 2007
		header("Content-Disposition: attachment; filename=$filename.xls");
		echo "$datosExcel";
    }
}

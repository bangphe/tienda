<?php

class htmltopdf
{
    
	var $_htmlToPdf = NULL;
	
	function __construct()
	{
		require_once("html2pdf/html2pdf.class.php");
		if(is_null($this->_htmlToPdf))
		{
			$this->_htmlToPdf = new HTML2PDF();
		}
	}
	
	function convert_html_to_pdf($htmlReporte, $pdf_nombreArchivo ='', $tamanoPaginaPdf) 
	{		
		$this->_htmlToPdf = new HTML2PDF('P', $tamanoPaginaPdf, 'es', true, 'UTF-8',array(20, 10, 20, 15));
        $this->_htmlToPdf->pdf->SetDisplayMode('fullpage');
		$this->_htmlToPdf->setDefaultFont('arial');
        $this->_htmlToPdf->writeHTML($htmlReporte);
        $this->_htmlToPdf->Output($pdf_nombreArchivo);
	}
}
?>
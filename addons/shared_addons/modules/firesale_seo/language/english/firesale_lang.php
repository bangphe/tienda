<?php defined('BASEPATH') or exit('No direct script access allowed');

    // General
    $lang['firesale:seo:title']    = 'Search Engine Optimisation';
    $lang['firesale:tab_metadata'] = 'Meta Data';

    // Labels
    $lang['firesale:seo:label_title']    = 'Meta Title';
    $lang['firesale:seo:label_desc']     = 'Meta Description';
    $lang['firesale:seo:label_keywords'] = 'Meta Keywords';

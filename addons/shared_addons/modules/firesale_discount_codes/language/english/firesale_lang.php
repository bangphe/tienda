<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Title
$lang['fs_discount_codes:sections:discounts'] = 'Discount Codes';
$lang['fs_discount_codes:add_discount_code'] = 'Add Discount Code';
$lang['fs_discount_codes:edit_discount_code'] = 'Edit Discount Code';

// Shortcuts
$lang['fs_discount_codes:shortcuts:add'] = 'Add Discount Code';

// Field labels & instructions
$lang['fs_discount_codes:code'] = 'Code';
$lang['fs_discount_codes:type'] = 'Discount type';
$lang['fs_discount_codes:value'] = 'Value';
$lang['fs_discount_codes:value_info'] = 'How much is this discount code worth (percentage or fixed rate)'; // Instructions
$lang['fs_discount_codes:desc'] = 'Description';
$lang['fs_discount_codes:applies_to'] = 'Applies to';
$lang['fs_discount_codes:products'] = 'Products';
$lang['fs_discount_codes:categories'] = 'Categories';
$lang['fs_discount_codes:usage'] = 'Usage';
$lang['fs_discount_codes:start_date'] = 'Start date';
$lang['fs_discount_codes:end_date'] = 'End date';
$lang['fs_discount_codes:used'] = 'Used';
$lang['fs_discount_codes:code_used'] = 'Code used';

// Dropdown values
$lang['fs_discount_codes:everything'] = 'Everything';
$lang['fs_discount_codes:selected_prods'] = 'Selected products';
$lang['fs_discount_codes:selected_cats'] = 'Selected categories';
$lang['fs_discount_codes:once'] = 'Once';
$lang['fs_discount_codes:once_user'] = 'Once per user';
$lang['fs_discount_codes:multiple'] = 'Multiple';
$lang['fs_discount_codes:yes'] = 'Yes';
$lang['fs_discount_codes:no'] = 'No';

// Admin messages
$lang['fs_discount_codes:messages:no_codes'] = 'There are currently no discount codes to display';
$lang['fs_discount_codes:messages:deleted'] = 'Discount code deleted successfully';
$lang['fs_discount_codes:messages:deleted_multiple'] = 'The selected discount codes were deleted successfully';


// Frontend messages
$lang['fs_discount_codes:message:discount_applied'] = 'The discount code %s was successfully applied to your basket';
$lang['fs_discount_codes:message:login_required'] = 'You must be logged in to apply that discount code';
$lang['fs_discount_codes:message:already_used'] = 'You have already used this code on a previous order';
$lang['fs_discount_codes:message:invalid'] = 'The code you entered does not appear to be valid';
$lang['fs_discount_codes:message:already_applied'] = 'You already have a discount code applied to your cart';
$lang['fs_discount_codes:message:code_removed'] = 'Code successfully removed';
$lang['fs_discount_codes:message:code_removed_fail'] = 'No discount code to remove';

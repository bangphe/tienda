 <div id="sortable">

    <?php if( isset($items) AND ! empty($items) ): ?>
    <?php foreach( $items as $item ): ?>
    <?php if($item['id'] != "reviews"): ?>

    <?php
        $class = ($item['id'] == "sales") ? "full-width" : "one_half";
    ?>

        <div class="<?php echo $class ?>" id="<?php echo $item['id']; ?>">
            <section class="draggable title">
                <h4><?php echo $item['title']; ?></h4>
                <a class="toggle" title="Toggle this element"></a>
            </section>
            <section class="item"<?php echo ( isset($item['hidden']) && $item['hidden'] == true ? ' style="height: auto; display: none"' : '' ); ?>>
                <div class="content">
                    <?php echo $item['content']; ?>
                </div>
            </section>
        </div>

    <?php endif; ?>
    <?php endforeach; ?>
    <?php endif; ?>
    </div>
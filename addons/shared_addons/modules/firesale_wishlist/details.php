<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Firesale_wishlist extends Module
{
    public $version = '0.0.1';
    public $language_file = 'firesale_wishlist/firesale';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('firesale/firesale');
        $this->lang->load($this->language_file);
    }

    public function info()
    {

        $info = array(
            'name' => array(
                'en' => 'FireSale Wishlists'
            ),
            'description' => array(
                'en' => 'Wishlist creation and management',
            ),
            'frontend' 		=> false,
            'backend' 		=> true,
            'author' 		=> 'Jamie Holdroyd',
            'shortcuts'     => array(
                array(
                    'name'  => 'firesale:wish:add',
                    'uri'   => 'admin/firesale_wishlist/create',
                    'class' => 'add'
                )
            )
        );

        return $info;
    }

    public function admin_menu(&$menu)
    {
        $menu['lang:firesale:title']['lang:firesale:wish:title'] = 'admin/firesale_wishlist';
    }

    public function install()
    {

        // Load required items
        $this->load->driver('Streams');

        ###############
        ## WISHLISTS ##
        ###############

        // Create wishlists stream
        if( !$this->streams->streams->add_stream(lang('firesale:wish:title'), 'firesale_wishlists', 'firesale_wishlists', NULL, NULL) ) return FALSE;

        // Add fields
        $fields   = array();
        $template = array('namespace' => 'firesale_wishlists', 'assign' => 'firesale_wishlists', 'type' => 'text', 'title_column' => false, 'required' => false, 'unique' => false);
        $fields[] = array('name' => 'lang:firesale:wish:label:title', 'slug' => 'title', 'type' => 'text', 'extra' => array('max_length' => 255), 'unique' => true);
        $fields[] = array('name' => 'lang:firesale:wish:label:status', 'slug' => 'status', 'type' => 'choice', 'extra' => array('choice_data' => "0 : lang:firesale:label_draft\n1 : lang:firesale:label_live", 'choice_type' => 'dropdown', 'default_value' => 0));
        $fields[] = array('name' => 'lang:firesale:wish:label:privacy', 'slug' => 'privacy', 'type' => 'choice', 'extra' => array('choice_data' => "0 : lang:firesale:wish:label:public\n1 : lang:firesale:wish:label:private", 'choice_type' => 'dropdown', 'default_value' => 0));
        $fields[] = array('name' => 'lang:firesale:wish:label:desc', 'slug' => 'description', 'type' => 'wysiwyg', 'extra' => array('editor_type' => 'simple'), 'required' => false);

        $this->add_stream_fields($fields, $template);

        #############################
        ## WISHLIST PRODUCT LINKUP ##
        #############################

        // Create lookup table
        $this->db->query("CREATE TABLE `" . SITE_REF . "_firesale_wishlists_firesale_products` (
                            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                            `row_id` int(11) NOT NULL,
                            `firesale_wishlists_id` int(11) NOT NULL,
                            `firesale_products_id` int(11) NOT NULL,
                            `firesale_products_qty` int(11) NOT NULL,
                            PRIMARY KEY (`id`)
                          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;");

        ############
        ## ROUTES ##
        ############
        
        $this->routes('add');

        // Return
        return true;
    }

    public function uninstall()
    {
        // Load required items
        $this->load->driver('Streams');

        // Drop streams tables
        $this->streams->utilities->remove_namespace('firesale_wishlists');

        // Drop manual tables
        $this->dbforge->drop_table('firesale_wishlists_firesale_products');

        // Remove routes
        $this->routes('remove');

        // Return
        return true;
    }

    public function upgrade($old_version)
    {
        // Your Upgrade Logic
        return true;
    }

    public function help()
    {
        return "Some Help Stuff";
    }

    public function routes($action)
    {

        // Load model
        $this->load->model('firesale/routes_m');

        // Routes
        $routes   = array();
        $routes[] = array('is_core' => 1, 'title' => lang('firesale:wish:title'), 'slug' => 'wishlist', 'table' => '', 'map' => 'users/wishlist{{ any }}', 'route' => 'users/wishlist(:any)?', 'translation' => 'firesale_wishlist/front$1', 'https' => '0');

        // Perform
        foreach ($routes AS $route) {
            // Check action
            if ($action == 'add') {
                $this->routes_m->create($route);
            } elseif ($action == 'remove') {
                $this->routes_m->delete($route['slug']);
            }
        }

        // Return
        return true;
    }

    public function add_stream_fields($fields, $template)
    {
        // Combine
        foreach ($fields AS &$field) { $field = array_merge($template, $field); }

        // Add fields to stream
        $this->streams->fields->add_fields($fields);
    }

}

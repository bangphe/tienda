<div class="container">
    <div class="row">

        <div class="span3">
            <strong>Categorias</strong>
            <li class="dropdown">

                <ul class="dropdown">
                    {{ firesale:categories order="ordering_count asc" parent="0" }}
                    {{ if children > 0 }}
                    <li>
                        <a href="{{ firesale:url route='category' id=id }}"><span>{{ title }}</span></a>
                    </li>
                    {{ else }}
                    <li>
                        <a href="{{ firesale:url route='category' id=id }}">{{ title }}</a>
                    </li>
                    {{ endif }}
                    {{ /firesale:categories }}
                    <li><a href="#">Ver Más</a></li>
                </ul>
            </li>
        </div>

        <div class="span3 pull-right">
            <?php foreach ($pautas as $pauta): ?>
                <div><a href="<?php echo $pauta->link ?>" target="_blank"><img src="<?php echo upload_url($pauta->imagen) ?>"></a></div>
            <?php endforeach; ?>
        </div>
        <div class="span6 pull-right">

            <?php foreach ($items as $item): ?>
                <div>
                    <a href="product/<?php echo $item['slug'] ?>">
                        <div style="width: 160px;height: 160px;overflow: hidden">
                            <img src="<?php echo upload_url($item['imagen']) ?>" width="160">
                        </div>
                        <strong><?php echo $item['titulo'] ?></strong>
                    </a>
                    <p><?php echo $item['descripcion'] ?></p>
                    <small>$<?php echo $item['precio'] ?></small>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div id="masonry">
        {{ firesale:products limit="4" order="featured desc" }}
        {{ if entries }}
        {{ entries }}
        <div class="masonry-item col1">
            <div class="item-flex">

                <a class="item-image" href="{{ firesale:url route='product' id=id }}">
                    {{ if images }}
                    {{ if images.1.id }}
                    <img class="first" src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                    <img class="rollover" src="{{ url:site uri='files/thumb' }}/{{ images.1.id }}/360" alt="{{ title }} alternate" />
                    {{ else }}
                    <img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                    {{ endif }}
                    {{ else }}
                    {{ theme:image file="no-image.png" alt=title }}
                    {{ endif }}
                    <span class="name">{{ title }}</span>
                </a>

                <div class="sharing">

                    <a class="facebook_popup link" data-original-title="Share this on Facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer.php?s=100&p[title]={{ title }}&p[url]={{ firesale:url route='product' id=id }}{{ if images.1.id }}&p[images][0]={{ url:site uri='files/large' }}/{{ images.0.id }}{{ endif }}"><i class="icon-facebook"></i></a>

                    <a class="twitter_popup link" data-original-title="Share this on Twitter" href="http://twitter.com/share?url={{ firesale:url route='product' id=id }}&text={{ title }}%20is%20awesome!{{ if {theme:options:twitter_account} }}&via={{ theme:options:twitter_account }}{{ endif }}"><i class="icon-twitter"></i></a>

                    <?php if (module_enabled('firesale_wishlist')): ?>
                        {{ if user:logged_in }}
                        <a href="#" class="add_to_wishlist link wish_modal" data-item-id="{{ id }}" data-original-title="Agregar a la lista de deseos" type="button" data-toggle="modal" data-target="#wishlistModal"><i class="icon-heart"></i></a>
                        {{ endif }}
                    <?php endif; ?>

                </div>

                <div class="right-text">
                    <div class="item-price pull-left">{{ price_formatted }}</div>

                    <a href="{{ firesale:url route='product' id=id }}"><span class="btn btn-inverse">View</span></a>

                    {{ if modifiers }}
                    <a class="btn btn-warning" href="{{ firesale:url route='cart' after='/insert/' }}{{ modifiers.1.variations.1.product.id }}">Add to Cart</a>
                    {{ else }}
                    <a class="btn btn-warning" href="{{ firesale:url route='cart' after='/insert/' }}{{ id }}">Add to Cart</a>
                    {{ endif }}

                </div>

            </div>
        </div>
        {{ /entries }}
        {{ endif }}
        {{ /firesale:products }}
    </div>
</div>
<!-- Wishlist modal 
<?php if (module_enabled('firesale_wishlist')): ?>
    {{ if user:logged_in }}
    {{ theme:partial name="wishlist_modal" }}
    {{ endif }}
<?php endif; ?>-->
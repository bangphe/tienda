<section class="title">
    <h4>Importar Productos</h4>
</section>
<section class="item">
    <div class="content">
        <fieldset id="filters" class="four">
            <legend></legend>
                <h4>Pasos para importar los productos:</h4>
                <p>Paso 1: Se deben subir las imagenes de los producto, para esto existe un modulo llamado archivos, que lo encontraras en el menu container->archivos, en este encontraras una carpeta llamada products_main_images
                en la cual se deben subir las imagenes en lo posible sin repetir el nombre
                </p>
                <p>Paso 2: Exportar en excel las categorias y las marcas, en este encontraras los ID's que son los que se incluyen en el archivo excel</p>
                <p>Paso 3: Descargar el archivo de ejemplo</p>
                <p>
                    Paso 4: Los campos son los siguientes 
                    <br/>
                    <br/>
                    Código del producto <br/>
                    Titulo <br/>
                    Precio de referencia en el mercado con iva <br/>
                    Precio de referencia en el mercado sin iva <br/>
                    Precio con iva <br/>
                    Precio sin iva <br/>
                    Descuento <br/>
                    Stock <br/>
                    Categorias: (Se pueden agregar varios ID'S de la categoria separados por coma (,))<br/>
                    Imagen: El nombre de la imagen con la que se subio a la carpeta del modulo.<br/>
                    Marcas: Se coloca el id de la marca (Es una sola marca)<br/>
                    Descripción <br/>
                </p>
                <br/>
                <p>Nota: <br/>
                    La estructura del excel no puede ser modificada. <br/>
                    Tanto en el titulo como en la descripción se borraran los caracteres especiales que alteren la base de datos.
                </p>
                <br/>
                <br/>
                <br/>
            <ul>
                <li>
                    <center>
                        <p>
                            Exportar la lista de categorias para los productos.

                            Los productos necesitan tener la referencia de las categorias donde van a estar ubicadas, Se deben crear las categorias primero para poder tener su id como referencia para que este se vincule
                        </p>
                        <a class="btn" href="<?php echo site_url('admin/firesale/products/export_categories'); ?>">Exportar lista de categorias</a>
                    </center>
                </li>
                <li>
                    <center>
                        <p>
                            Exportar la lista de marcas para los productos.

                            Los productos necesitan tener la referencia de las marcas donde van a estar ubicadas, Se deben crear las marcas primero para poder tener su id como referencia para que este se vincule
                        </p>
                        <a class="btn" href="<?php echo site_url('admin/firesale/products/export_brands'); ?>">Exportar lista de marcas</a>
                    </center>
                </li>
                <li>
                    <center>
                        <?php echo form_open_multipart(site_url('admin/firesale/products/read_excel_products/'), 'class="crud"'); ?>
                            <fieldset>
                                <ul>
                                    <li>
                                        <label for="name">Archivo de ejemplo
                                            <small>
                                                - Archivo de excel formato .xls (Libro de excel 97 - 2003)
                                            </small>
                                        </label>
                                        <div class="btn-false">
                                            <a href="<?php echo site_url('docs/example_excel_import_products.xls') ?>" target="_blank">Descargar Archivo de Ejemplo</a>
                                        </div>
                                    </li>
                                    <li>
                                        <label for="name">Importar Archivo
                                            <small>
                                                - Se importa el Excel en un formato .xls (Libro de excel 97 - 2003)
                                            </small>
                                        </label>
                                        <div class="btn-false">
                                            <div class="btn">Examinar</div>
                                            <?php echo form_upload('file', set_value('file'), ' id="file"'); ?>
                                        </div>
                                    </li>
                                </ul>
                                <?php
                                    $this->load->view('admin/partials/buttons', array('buttons' => array('save')));
                                ?>
                            </fieldset>
                        <?php echo form_close(); ?>
                    </center>
                </li>
            </ul>
        </fieldset>
    </div>
</section>
<section class="title">
    <h4>Cambiar precios de los productos</h4>
</section>

<section class="item">
    <div class="content">
        <fieldset id="filters" class="four">
            <legend></legend>
            <ul>
                <li>
                    <center>
                        <p>
                            Con el siguiente botón podrás exportar los productos de la base de datos a un archivo excel con la siguiente estructura
                            <br/><br/>
                            ID, Codigo, Producto, Precio_Distribuidor_Con_Iva, Precio_Distribuidor_Sin_Iva, Precio_Con_Iva , Precio_Sin_Iva, Descuento, Stock
                            <br/><br/>
                            Todos los campos son editables se modificaran en caso tal que se cambien algunos de sus atributos, el único campo que no se debe cambiar es el ID
                            <br/><br/>
                            Se debe tener en cuenta que el programa no va a calcular el IVA del producto, por esta razón en el archivo excel debe estar tanto con IVA como sin IVA.
                            <br/><br/>
                            Nota:
                            <br/>
                            No pueden estar los campos con formulas, ya que esto afectaria la base de datos.
                        </p>
                    </center>
                </li>
                <li>
                    <center><a class="btn" href="<?php echo site_url('admin/firesale/products/export_prices'); ?>">Exportar lista de productos</a></center>
                </li>
                <li>
                    <center>
                        <?php echo form_open_multipart(site_url('admin/firesale/products/read_excel_prices/'), 'class="crud"'); ?>
                            <fieldset>
                                <ul>
                                    <li>
                                        <label for="name">Importar Archivo
                                            <small>
                                                - Se importa el Excel que se exporto anteriormente, Este producto debera ser guardado en un formato diferente, este formato es un .xls (Libro de excel 97 - 2003)
                                            </small>
                                        </label>
                                        <div class="btn-false">
                                            <div class="btn">Examinar</div>
                                            <?php echo form_upload('file', set_value('file'), ' id="file"'); ?>
                                        </div>
                                    </li>
                                    <li>
                                        <label for="date">Código </label>
                                        <br/>
                                        <small>
                                            - Puedes seleccionar que campos son los que desea cambiar con el archivo excel
                                        </small>
                                        <div class="input"><?php echo form_checkbox('code', '1', FALSE); ?></div>
                                    </li>
                                    <li>
                                        <label for="date">Nombre Del Producto </label>
                                        <div class="input"><?php echo form_checkbox('title', '1', FALSE); ?></div>
                                    </li>
                                    <li>
                                        <label for="date">Precios Distribuidores</label>
                                        <br/>
                                        <small>
                                            - Con este campo se selecciona tanto el precio sin iva como con iva, (Su diferencia depende del iva, este lo debes calcular y dejar el campo sin formula)
                                        </small>
                                        <div class="input"><?php echo form_checkbox('distributor', '1', TRUE); ?></div>
                                    </li>
                                    <li>
                                        <label for="date">Precios Clientes</label>
                                        <br/>
                                        <small>
                                            - Con este campo se selecciona tanto el precio sin iva como con iva, (Su diferencia depende del iva, este lo debes calcular y dejar el campo sin formula)
                                        </small>
                                        <div class="input"><?php echo form_checkbox('client', '1', TRUE); ?></div>
                                    </li>
                                    <li>
                                        <label for="date">Descuento </label>
                                        <div class="input"><?php echo form_checkbox('descuento', '1', TRUE); ?></div>
                                    </li>
                                    <li>
                                        <label for="date">Stock </label>
                                        <div class="input"><?php echo form_checkbox('stock', '1', TRUE); ?></div>
                                    </li>
                                </ul>
                                <?php
                                    $this->load->view('admin/partials/buttons', array('buttons' => array('save')));
                                ?>
                            </fieldset>
                        <?php echo form_close(); ?>
                    </center>
                </li>
            </ul>
        </fieldset>
    </div>
</section>
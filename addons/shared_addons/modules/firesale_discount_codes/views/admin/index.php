
        <section class="title">
            <h4><?php echo lang('fs_discount_codes:sections:discounts'); ?></h4>
        </section>

        <section class="item">
            <div class="content">
                <?php if ( ! empty($codes)): ?>
                    <?php echo form_open('admin/firesale_discount_codes/delete'); ?>
                    <div class="scroll-table">
                        <table>
                            <thead>
                                <tr>
                                    <th style="width: 15px"><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
                                    <th><?php echo lang('fs_discount_codes:code'); ?></th>
                                    <th><?php echo lang('fs_discount_codes:value'); ?></th>
                                    <th><?php echo lang('fs_discount_codes:applies_to'); ?></th>
                                    <th><?php echo lang('fs_discount_codes:usage'); ?></th>
                                    <th><?php echo lang('fs_discount_codes:start_date'); ?></th>
                                    <th><?php echo lang('fs_discount_codes:end_date'); ?></th>
                                    <th><?php echo lang('fs_discount_codes:used'); ?></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($codes as $code): ?>
                                    <tr>
                                        <td><input type="checkbox" name="action_to[]" value="<?php echo $code['id']; ?>" /></td>
                                        <td><?php echo $code['code']; ?></td>
                                        <td><?php if ($code['type']['key'] == 'fixed'): ?><?php echo $this->settings->get('currency'); ?><?php endif; ?><?php echo $code['value']; ?><?php if ($code['type']['key'] == 'percentage'): ?>%<?php endif; ?></td>
    
                                        <td>
                                            <?php if ($code['applies_to']['key'] == '0'): ?>
                                                <?php echo $code['applies_to']['value']; ?>
                                            <?php elseif ($code['applies_to']['key'] == '1'): ?>
                                                <?php foreach ($code['products'] as $id => $title): ?>
                                                    <a href="<?php echo site_url('admin/firesale/products/edit/' . $id); ?>"><?php echo $title; ?></a><br />
                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <?php foreach ($code['categories'] as $id => $title): ?>
                                                    <a href="<?php echo site_url('admin/firesale/categories/edit/' . $id); ?>"><?php echo $title; ?></a><br />
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </td>
    
                                        <td><?php echo $code['usage']['value']; ?></td>
                                        <td><?php echo date('d M Y', $code['start']); ?></td>
                                        <td><?php echo date('d M Y', $code['end']); ?></td>
                                        <td><?php echo $code['used']['value']; ?></td>
                                        <td class="actions">
                                            <a href="<?php echo site_url('admin/firesale_discount_codes/edit/' . $code['id']); ?>" class="btn blue small"><?php echo lang('global:edit'); ?></a>
                                            <a href="<?php echo site_url('admin/firesale_discount_codes/delete/' . $code['id']); ?>" class="btn red small"><?php echo lang('global:delete'); ?></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="9">
                                        <div class="table_action_buttons">
                                            <button type="submit" name="btnAction" value="delete" class="btn red confirm"><span><?php echo lang('global:delete'); ?></span></button>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                <?php else: ?>
                    <div class="no_data"><?php echo lang('fs_discount_codes:messages:no_codes'); ?></div>
                <?php endif; ?>
            </div>

        </section>


    <?php echo form_open($this->uri->uri_string(), 'id="wish" class="crud wrap"'); ?>
        
        </ul>
        <?php foreach( $fields AS $input ): ?>
            <li class="<?php echo alternator('even', ''); ?>">
                <label for="<?php echo $input['input_slug']; ?>">
                    <?php echo lang(substr($input['input_title'], 5)) ? lang(substr($input['input_title'], 5)) : $input['input_title']; ?> <?php echo $input['required']; ?>
                    <small><?php echo lang(substr($input['instructions'], 5)) ? lang(substr($input['instructions'], 5)) : $input['instructions']; ?></small>
                </label>
                <div class="input"><?php echo $input['input']; ?></div>
                <br class="clear" />
            </li>
        <?php endforeach; ?>
        </ul>

        <div class="buttons">
            <button type="submit" name="btnAction" value="save" class="btn blue"><span><?php echo lang('save_label'); ?></span></button>            
        <?php if ( isset($_SERVER['HTTP_REFERER']) ): ?>
            <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn gray cancel"><?php echo lang('cancel_label'); ?></a>
        <?php endif; ?>
        </div>

    <?php echo form_close(); ?>

<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends Admin_Controller
{

	public $tabs    = array('_items' => array());
    public $section = 'wishlists';
    public $perpage = 30;

    public function __construct()
    {
        parent::__construct();

        // Load required items
        $this->lang->load('firesale');
        $this->load->model('wishlists_m');

        // Add initial items
        $this->data   = new stdClass();
        $this->stream = $this->streams->streams->get_stream('firesale_wishlists', 'firesale_wishlists');
    }

	public function index($start = 0)
	{
        // Actions
        if ( $this->input->post('btnAction') == 'delete' ) {
            $this->delete();
        }

        // Variables
        $pagination = array('uri_segment' => 3, 'per_page' => $this->perpage, 'base_url' => 'admin/firesale_wishlist/');
        $params     = array(
            'stream'      => $this->stream->stream_slug,
            'namespace'   => $this->stream->stream_namespace,
            'limit'       => $this->perpage,
            'offset'      => $start,
            'order_by'    => 'ordering_count',
            'sort'        => 'asc',
            'paginate'    => 'yes',
            'pag_segment' => $pagination['uri_segment']
        );

        // Assign data
        $this->data = $this->streams->entries->get_entries($params, $pagination);

        // Format data
        foreach ( $this->data['entries'] as &$entry ) {
            $entry = $this->pyrocache->model('wishlists_m', 'get_list', array($entry['id']), $this->firesale->cache_time);
        }

        // Build page
        $this->template->title(lang('firesale:wish:title'))
                       ->append_css('module::module.css')
                       ->build('admin/index', $this->data);
	}

    public function create()
    {
        $this->_form();
    }

    public function edit($id)
    {
        // Get the row
        $row = $this->streams->entries->get_entry($id, $this->stream->stream_namespace, $this->stream->stream_slug);
        $this->_form($row);
    }

    public function delete($id = 0)
    {
        // Variables
        $delete = true;
        $items  = $this->input->post('action_to');

        // Delete multiple
        if ( $this->input->post('btnAction') == 'delete' and ! empty($items) ) {

            foreach ( $items as $id ) {
                if ( ! $this->wishlists_m->list_delete($id) ) {
                    $delete = false;
                }
            }

        // Delete single
        } elseif ( $id > 0 ) {
            if ( ! $this->wishlists_m->list_delete($id) ) {
                $delete = false;
            }
        }

        // Set flashdata
        if ( $delete ) {
            $this->session->set_flashdata('success', lang('firesale:wish:delete:success'));
        } else {
            $this->session->set_flashdata('error', lang('firesale:wish:delete:error'));
        }

        // Send back
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function _form($row = false)
    {
    	// Variables
        $stream = $this->streams->streams->get_stream($this->stream->stream_namespace, $this->stream->stream_slug);
        $skip  = array('btnAction');
        $extra = array(
            'return'          => false,
            'success_message' => lang('firesale:wish:'.($row ? 'edit' : 'add').':success'),
            'failure_message' => lang('firesale:wish:'.($row ? 'edit' : 'add').':error'),
            'title'           => lang('firesale:wish:'.($row ? 'edit' : 'add'))
        );

        // Get fields
        $fields = $this->fields->build_form($stream, $row ? 'edit' : 'new', $row ? $row : $this->input->post(), false, false, $skip, $extra);

        // Check for posted
        if ( ! is_array($fields)) {

            // Success, clear all the cache!
            Events::trigger('clear_cache');

            // Redirect
            if ( $this->input->post('btnAction') == 'save_exit' ) {
                redirect('admin/firesale_wishlist');
            } else {
                redirect('admin/firesale_wishlist/edit/' . $fields);
            }
        }

        // Pass some data to the view
        $data['id']     = $row ? $row->id : null;
        $data['type']   = $row ? 'edit' : 'add';
        $data['fields'] = fields_to_tabs($fields, $this->tabs);
        $data['tabs']   = array_keys($data['fields']);
        $data['items']  = $row ? $this->wishlists_m->list_items($row->id) : array();

        // Build page
        $this->template->title(lang('firesale:wish:'.$data['type']))
                       //->append_js('module::module.js')
        			   ->append_css('module::module.css')
        			   ->build('admin/form', $data);
    }

	public function add_items($list_id)
    {
        // Variables
        $list     = array();
        $products = $this->db->select('id, title')->get('firesale_products')->result_array();

        // Format
        foreach ( $products as $product ) {
            $list[$product['id']] = $product['title'];
        }
		
        // Build page
        $this->template->set_layout(false)
                       ->build('admin/list', array('list' => $list, 'list_id' => $list_id));
    }

    //public function insert_items($list_id, $product, $quantity = 1)
    public function insert_items($list_id)
    {
    	/*
    	echo "<pre> ";
		print_r($_POST);
		echo "</pre> ";
		return;
		*/
		
		if(isset($_POST['product']) && isset($_POST['quantity']))
		{
			$product = $_POST['product'];
			$quantity = $_POST['quantity'];
		}
		else {
			$this->session->flashdata('error', lang('firesale:wish:item:add:error'));
        	redirect($this->input->server('HTTP_REFERER'));
		}
		
        // Add it
        $this->wishlists_m->list_item_add($list_id, $product, $quantity);

        // Clear cache
        $this->pyrocache->delete_all('wishlists_m');

        // Set flashdata
        $this->session->flashdata('success', lang('firesale:wish:item:add:success'));
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function update_items()
    {
        // Variables
        $id = $this->input->post('id');

        // Loop quantity
        foreach ( $this->input->post('quantity') as $product => $quantity ) {
            // Remove
            if ( $quantity <= 0 ) {
                $this->wishlists_m->list_item_remove($id, $product);

            // Update
            } else {
                $this->wishlists_m->list_item_add($id, $product, $quantity);
            }
        }

        // Clear cache
        $this->pyrocache->delete_all('wishlists_m');

        // Set flashdata
        $this->session->flashdata('success', lang('firesale:wish:item:edit:success'));
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function delete_items($idproduc, $idwhishlist)
    {
    	$this->wishlists_m->list_item_remove($idwhishlist, $idproduc);
		/*
        // Variables
        $id = $this->input->post('id');

        // Loop quantity
        foreach ( $this->input->post('action_to') as $product ) {
            // Remove
            $this->wishlists_m->list_item_remove($id, $product);
        }
		*/
        // Clear cache
        $this->pyrocache->delete_all('wishlists_m');
        // Set flashdata
        $this->session->flashdata('success', lang('firesale:wish:item:delete:success'));
        redirect($this->input->server('HTTP_REFERER'));
    }

}

<div id="carga"></div>
<div class="hide" id="baseurl"><?php echo base_url(); ?></div>
<div class="hide" id="pdfVista"><?php echo (isset($pdfVista) ? $pdfVista : '0'); ?></div>
<div class="hide" id="esMovil"><?php echo ($this->agent->is_mobile() ? '1' : '0' ); ?></div>
<div class="hide" id="esIOS"><?php echo ($this->agent->is_mobile('iphone') || $this->agent->is_mobile('ipad') || $this->agent->is_mobile('ipod') ? '1' : '0' ); ?></div>
<a href="#sinergiaModal" role="button" class="btn hide" id="btn-show-modal" data-toggle="modal">Launch demo modal</a>
<div id="sinergiaModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div id="cargarinternomodal"></div>
	<div id="modalcontent">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel">Vista Ampliada</h3>
		</div>
		<div class="modal-body">
			<p></p>
		</div>
	  	<div class="modal-footer">
	  		<button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
		</div>
	</div>
</div>	
<div id="contenedorCanvas" class="hide">
	<canvas id="canvas" width="2500" height="2000"></canvas>
</div>
<div id="css-body-vistaAmpliada" class="hide">
	<div class="css-body-widget">
		<div class="idWidget hide"></div>
		<div class="css-graficoWidget" id="graficoContenedorVistaAmpliada">
			<svg></svg>
		</div>
		<div id="css-tabla-vistaAmpliada">
			
		</div>
	</div>
</div>
<div id="css-grupoWidgets" <?php if(!isset($modulosWidgets)){ echo 'class="sortableWidgets"'; }?> >
		<?php if(isset($url_buscar)) {?>
			<form method="POST" id="cambiarFechaSession" action="<?php echo base_url().$url_buscar; ?>" class="form-search">
				<div class="input">
					<span class="add-on">Fecha General</span>
					<div class="input-append date">
						<?php
						echo form_input(array('name' => 'fechaGeneralSession', 'id' => 'fechaGeneralSession', 'disabled' => 'disabled', 'class' => 'input-medium', 'value' => (isset($anoactual)) ? $anoactual.'-'.$mesactual.'-'.$diaactual : set_value('fechaGeneralSession') ));
						?>
						<span class="add-on"><i class="icon-calendar"></i></span>
					</div>
					<button type="submit" name="enviar" class="btn btn-inverse" >Actualizar</button>
				</div>
			</form>
		<?php } ?>
    	<?php
    		if(!empty($widgetsUsuario))
			{
				foreach ($widgetsUsuario->result() AS $datosArray)
				{
					// calculamos el ancho del widget segun si es para volverse pdf o no (los pdf solo admiten un ancho de 2 como maximo por el tamaño)
					$undAnchoWidget = $datosArray->undsancho;
					// si vamos a crear el pdf los widgets deben de tener un tamaño de 660 de lo contrario creamos los widgets con el tamaño normal para el usuario
					if(isset($pdfVista) && $datosArray->tipografico != 5)
					{
						$anchoWidget = 660;
					}
					else
					{
						$undsAnchoWidget = $undAnchoWidget;
                   		$anchoWidget = (300*$undsAnchoWidget) + (18*($undsAnchoWidget-1)) + ($undsAnchoWidget - ($undsAnchoWidget == 1 ? 1 : 2));
					}
					$posIzqLoader = ($anchoWidget/2) - 16;
				    
					
					// el id de la clase widget se utiliza para el sortable, y idwiusu interno es para recorrer el widget
					// Nota: no debemos dejar espacios entre los divs, pues al utilizar inline-block se generan pixels adicionales
					echo '<div class="css-widget" style="width:'.$anchoWidget.'px" id="'.($datosArray->idwiusu != 0 ? $datosArray->idwiusu : $datosArray->idwidge).'">
							<div class="css-opcioneswidget pull-left controlesWidget hide">
								<a href="#" class="css-menu-opcioneswidget"><i class="icon-cog icon-white"> </i></a>
							</div>
							<div class="pull-right css-ayudaWidget">
							    <a class="btnAyudaWidget" title="'.$datosArray->descripcion.'" data-toggle="tooltip"><i class="icon-question-sign icon-white"> </i></a>
                            </div>
						    <div class="css-loaderWidget" style="left:'.$posIzqLoader.'px;"></div>
							<span class="idwidge hide">'.$datosArray->idwidge.'</span>
							<span class="idwiusu hide">'.$datosArray->idwiusu.'</span>
							<span class="posicion hide">'.$datosArray->posicion.'</span>
							<span class="undsancho hide">'.$undAnchoWidget.'</span>
							<span class="tipografico hide">'.$datosArray->tipografico.'</span>
							<span class="tipotabla hide">'.$datosArray->tipotabla.'</span>
							<span class="minrefresco hide">'.$datosArray->minrefresco.'</span>
							<span class="nomwidge hide">'.$datosArray->nomwidge.'</span>
							<span class="tituloejex hide">'.$datosArray->tituloejex.'</span>
							<span class="tituloejey hide">'.$datosArray->tituloejey.'</span>
							<span class="multiserie hide">'.$datosArray->multiserie.'</span>
							<span class="divisorWidget hide">'.$datosArray->divisor.'</span>
							<div class="css-header-widget">'.$datosArray->nomwidge.'
								
							</div>
							<div class="css-footer-widget">
							<a href="#" class="zoomWidget controlesWidget hide pull-right"> <i class="icon-search"></i></a>
							
							<div class="fechaWidgetDiv">
								<form method="POST">
									<span class="periodoWidgetTitulo">Periodo:</span>
									<div class="input-append date periodoWidget">';
									echo form_input(array('name' => 'fechaWidget', 'id' => 'fechaWidget', 'disabled' => 'disabled', 'class' => 'periodoWidgetInput', 'value' => (isset($anoactual)) ? $anoactual.'-'.$mesactual.'-'.$diaactual : set_value('fechaWidget') ));
									echo '<span class="add-on periodoWidgetAddon"><i class="icon-calendar"></i></span>
									</div>
								</form>
							</div>                            
								
							</div>
							<div class="css-body-widget">
								<div class="css'.(isset($pdfVista) ? '-pdf' : '').'-graficoWidget" id="graficoContenedor'.($datosArray->idwiusu != 0 ? $datosArray->idwiusu : $datosArray->idwidge).'">
									<svg style="width:'.($anchoWidget-24).'px"></svg>
								</div>
								
								<div class="datosWidget'.($datosArray->idwiusu != 0 ? $datosArray->idwiusu : $datosArray->idwidge).' hide">
									<div class="datosGrafico"></div>
									<div class="datosTabla"></div>
								</div>
							</div>
						</div>';
				}
			}
		?>
</div>

<div class="limpiar20"> </div>
<script type="text/javascript" charset="utf-8">
    $(window).load(function()
    {
        //$('#css-grupoWidgets').masonry({itemSelector:'.css-widget',columnWidth:1,gutter:10,transitionDuration:'0.5s',opacity:1,transform:'scale(1)', isResizable:true});
        $('#css-grupoWidgets').masonry({itemSelector: '#css-grupoWidgets', isResizable: true, columnWidth: 1, gutter:10, transitionDuration:'0.5s'});
    });
</script>
<section class="cont_home m_b_20">   

  <!--Miga de pan-->
  <div class="bg_miga">
    <ul class="marco miga_pan">
      <a href="{{url:base}}"><i class="fa fa-angle-double-right"></i>Inicio</a>
      <a href="#"><i class="fa fa-angle-double-right"></i>Mi Cuenta</a>
    </ul>
      <div class="clear"></div>
  </div>    
  <!--Fin Miga de pan-->

  <div class="clear"></div>
  
  <section class="marco bg_pasos_comp">
      <!--bg Carrito-->    
      {{ theme:partial name="account_nav" }}
      <!--Fin bg Carrito--> 

       <section class="large-9 medium-9 small-12 columns item2">
       <section class="cont_cat_perfil" id="perfil1">
          <div class=" relarive elem_blancos  animated fadeInUp form_edit">
             <div class="tittles">
                <h1>EDITAR PERFIL</h1>
              </div>
               <?php if (validation_errors()):?>
                  <div class="alert-error">
                    <?php echo validation_errors();?>
                  </div>
               <?php endif;?>

       
              <?php echo form_open_multipart('', array('class'=>'user_form form-horizontal input-w'));?>

                <div class="large-12 medium-12 small-12 columns dats_editar">
                  <h3>Datos de Acceso</h3>
                    <div class="large-6 medium-6 small-12 columns"> 
                      <label class="col-sm-2 control-label" for="email"><?php echo lang('global:email') ?></label>
                      <div class="col-sm-10 input_b">
                        <?php echo form_input('email', $_user->email) ?>
                      </div>
                    </div>
                    <div class="large-6 medium-6 small-12 columns"> 
                      <label class="col-sm-2 control-label" for="password"><?php echo lang('global:password') ?></label>
                      <div class="col-sm-10 input_b">
                        <?php echo form_password('password', '', 'autocomplete="off"') ?>
                      </div>
                    </div>
                </div>


                <div class="large-12 medium-12 small-12 columns">

                    <h3>Mi Cuenta</h3>   

                    <div class="dats_editar">
                      
                      <div class="large-6 medium-6 small-12 columns">                  
                        <label ><?php echo lang('profile_display_name') ?></label>
                        <?php echo form_input(array('name' => 'display_name', 'id' => 'display_name', 'value' => set_value('display_name', $display_name))) ?>
                      </div>

                      <?php foreach($profile_fields as $field): ?> 

                        <?php if($field['input']): ?>
                          <div class="large-6 medium-6 small-12 columns">  
                            <label>
                              <?php echo (lang($field['field_name'])) ? lang($field['field_name']) : $field['field_name'];  ?>
                              <?php if ($field['required']) echo '<span>*</span>' ?>
                            </label>

                            <?php if($field['instructions']) echo '<p class="instructions">'.$field['instructions'].'</p>' ?>
                            
                            <div class="ss">
                              <?php echo $field['input'] ?>
                            </div>
                          </div>     
                      <?php endif ?>
                      <?php endforeach ?> 

                    </div>

                      <div class="clear"></div>
                
                    <input class="btn_gris" type="submit" value="<?php echo lang('profile_save_btn') ?>" name="">
                       
          <?php echo form_close() ?>

        </div>  

        <div class="clear"></div>
      </section>
    </section>


  <!--Fin Campos Perfil-->
  </section>

</section>
<section class="cont_home m_b_20">  
  
  <section class="marco bg_pasos_comp">
      <!--bg Carrito-->    
      {{ theme:partial name="account_nav" }}
      <!--Fin bg Carrito--> 

      <section class="large-9 medium-9 small-12 columns item2">

        <section class="cont_cat_perfil">

          <div class=" relarive elem_blancos  animated fadeInUp form_edit">

             <div class="tittles">
                <h1>Mis Direcciones</h1>  
             </div>
              
             <section class="mis_dire_cread animated fadeInUp">     
                {{ if addresses }}                  
                <ul class="cajasdir">               

                  {{ addresses }}
                  <li class="large-4 medium-6 small-12 columns item1">
                    <div class="caja_borde">
                      <h3>{{ title }} 
                        <div class="btn_edit"><a href="{{ firesale:url route='addresses' }}/edit/{{ id }}">
                          <span  class="left_tip" data-tips="Editar">
                          <i class="fa fa-pencil-square-o"></i>
                          </span>
                        </a></div>
                      </h3>
                      <p><i class="fa fa-user"></i> {{ firstname }} {{ lastname }}</p>
                      <p><i class="fa fa-map-marker"></i> {{ address1 }}</p>
                      <p><i class="fa fa-building-o"></i> {{ email }}</p>
                      <p><i class="fa fa-building-o"></i> {{ phone }}</p>
                      <p><i class="fa fa-globe"></i> {{ id_deparment.name }}</p>
                      <p><i class="fa fa-globe"></i> {{ id_city.name }}</p>
                    </div>                      
                  </li>
                  {{ /addresses }}
           
                  {{ else }}
                      <div >No Hay Direcciones Guardadas</div>
                  {{ endif }}

                <ul>

                  <div class="clear"></div>

                <div class="controles_perfil2 m_t_20">
                  <a href="{{ firesale:url route="addresses" }}/create" title="" class="btn_gris right margin-l btn_new_direc">AGREGAR NUEVA DIRECCIÓN</a>
                  <div class="clear"></div>
                </div>   

                  <div class="clear"></div>  
             </section>

          </div>

        </section>

      </section>  

  <!--Fin Campos Perfil-->
  </section>

</section>


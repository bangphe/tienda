<a href="javascript:void(0)"><span></span> ({{ if ! count }}0{{ else }}{{ count }}{{ endif }})</a>

<div class="carrito_m">
  <h5>Tienes <strong class="cont_cart_h">{{ if ! count }}0{{ else }}{{ count }}{{ endif }}</strong>  artículo(s) en tu carrito. <div class="close_cart">x</div></h5>
  <ul class="cont_car">
    
    {{ products }}
    <li>
      <a href="javascript:void(0)" class="delete_prd">x</a>
      <a href="{{ if parent }}{{ firesale:url route='product' id=parent }}{{ else }}{{ firesale:url route='product' id=id }}{{ endif }}" title="View {{ name }}">
        {{ if image }}
        <img src="{{ url:site }}files/large/{{ image }}" alt=" {{ name }}"/>
        {{ else }}
        <div class="cart-left">{{ theme:image file="no-image.png" alt=title }}</div>
        {{ endif }}
        <h2>{{ name }}</h2>
        <h4>{{ price_formatted }}</h4>
        
      </a>
    </li>
    {{ /products }}
    <div class="clear"></div>
  </ul>
  <div class="costo_car">
    <div class="large-6 columns animated">
      <h6>TOTAL</h6>
      <h1>{{ total }}</h1>
    </div>
    <div class="large-6 columns animated">
      <a href="{{ url:site }}cart" title="" class="btn_border_blan">PAGAR AHORA</a>
    </div>
    <div class="clear"></div>
  </div>
  
  <div class="clear"></div>
  
</div>
<script>
  $('.close_cart').click(function(event) {
    $(".carrito_m").toggleClass('carrito_m_open');    
    $(".large-6").addClass('zoomIn');  
    $(".carrito_m li img").addClass('flipInX');    
    $("body").animate({ scrollTop : $( $('header') ).offset().top}, 200);        
  });
</script>
<?php

    // Navigation
    $lang['firesale:sections:reviews'] = 'Reviews';
    $lang['firesale:reviews:ratings']  = 'Manage Ratings';

    // Labels
    $lang['firesale:reviews:title'] 	 = 'Product Reviews';
    $lang['firesale:label_viewmore']	 = 'View More';
    $lang['firesale:label_author'] 		 = 'Author';
    $lang['firesale:label_product'] 	 = 'Product';
    $lang['firesale:label_status']		 = 'Status';
    $lang['firesale:label_rating']		 = 'Rating';
    $lang['firesale:label_review']		 = 'Review';
    $lang['firesale:label_review_title'] = 'Review Title';
    $lang['firesale:label_filterprod']	 = 'Filter by Product';
    $lang['firesale:label_filterpsel']	 = 'Select a Product';

    // Form
    $lang['firesale:reviews:form_title'] 	= 'Leave a Review for %s';
    $lang['firesale:reviews:form_name'] 	= 'Name';
    $lang['firesale:reviews:form_email'] 	= 'Email Address';
    $lang['firesale:reviews:form_comment'] 	= 'Comment';
    $lang['firesale:reviews:form_submit']	= 'Add your review';

    // Message
    $lang['firesale:reviews:none']				= 'No Reviews Found';
    $lang['firesale:reviews:add_success']		= 'Added review successfully';
    $lang['firesale:reviews:add_error']			= 'Adding of review failed';
    $lang['firesale:reviews:add_login_error']	= 'Adding of review failed due to login issue';
    $lang['firesale:reviews:update_success']    = 'Review updated successfully';
    $lang['firesale:reviews:update_error']      = 'Error updating review';
    $lang['firesale:reviews:status_success']	= 'Status update successful';
    $lang['firesale:reviews:status_error']		= 'Status update failed';
    $lang['firesale:reviews:delete_success']	= 'Review delete successful';
    $lang['firesale:reviews:delete_error']		= 'Review delete failed';
    $lang['firesale:reviews:previous']			= 'You have previously reviewed this product';
    $lang['firesale:reviews:nouser']			= 'You must be logged in to review this product';

    // Create items
    $lang['firesale:reviews:ratings:create'] = 'Create New Rating';

    // Edit items
    $lang['firesale:reviews:ratings:edit'] = 'Edit Rating';


    // Dashboard
    $lang['firesale:reviews:latest_title']	= 'Latest Reviews';

    // Settings
    $lang['firesale:reviews:setting_accept'] = 'Moderate reviews?';
    $lang['firesale:reviews:desc_accept']    = 'Require reviews to be moderated before they are posted.';

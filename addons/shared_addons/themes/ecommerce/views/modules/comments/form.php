                        
                        <div class="respond well">
                            <div class="title"><h4><?php echo lang('comments:your_comment') ?></h4></div>
                            <div class="form">
                                <?php echo form_open("comments/create/{$module}", 'class="form-horizontal" id="comment-form"') ?>
                                    <noscript><?php echo form_input('d0ntf1llth1s1n', '', 'style="display:none"') ?></noscript>
                                    <?php echo form_hidden('entry', $entry_hash) ?>
                                <?php if ( ! is_logged_in()): ?>
                                    <div class="control-group">
                                        <label class="control-label" for="name"><?php echo lang('comments:name_label') ?></label>
                                        <div class="controls">
                                            <input type="text" class="input-large" id="name" name="name" class="required" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="email"><?php echo lang('global:email') ?></label>
                                        <div class="controls">
                                            <input type="text" class="input-large" id="email" name="email" class="required" />
                                        </div>
                                    </div>
                                <?php endif; ?>
                                    <div class="control-group">
                                        <label class="control-label" for="comment"><?php echo lang('comments:message_label') ?></label>
                                        <div class="controls">
                                            <textarea class="input-xlarge" id="comment" name="comment" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-alpha" id="submitform"><?php echo lang('comments:send_label'); ?></button>
                                    </div>
                                <?php echo form_close() ?>
                            </div>
                        </div>

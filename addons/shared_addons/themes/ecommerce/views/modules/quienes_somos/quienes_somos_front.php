<div class="bkg-delta">
    <div class="inner-box">
        <strong class="main-font box-title"><?php echo $datos->titulo ?></strong>
    </div>
</div>
<div class="bkg-delta about">
    <div class="inner-box">
        <!-- IMAGEN -->
        <?php if (!empty($datos->imagen)): ?>
        	<img src="<?php echo upload_url($datos->imagen) ?>" alt="imagen" class="about-media">
        <?php endif; ?>
        <!-- VIDEO -->
        <?php if (!empty($datos->video)): ?>
        	<span class="about-media"><?php echo htmlspecialchars_decode($datos->video); ?></span>
        <?php endif; ?>
        <!-- TEXTO -->
        <?php echo $datos->texto ?>
        
        <div class="clear"></div>
        
    </div>
</div>
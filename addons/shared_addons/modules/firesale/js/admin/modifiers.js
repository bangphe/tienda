$(function() {

	sorting();

	$("button[type=submit]").live('click', function() {
	    $("button", $(this).parents("form")).removeAttr("clicked");
	    $(this).attr("clicked", "true");
	});

	$('.modifiers th .mod-min').live('click', function(e) {
		e.preventDefault();
		$('.modifiers td .mod-min').click();
	});
		
	$('.modifiers td .mod-min').live('click', function(e) {
		e.preventDefault();
		$(this).parents('tr').find('table').slideToggle(250);
		$(this).toggleClass('show');
	});

	$('#mod_form').live('submit', function(e) {
		e.preventDefault();
		var action = $(this).find('button[clicked=true]').val();
		$.post($(this).attr('action'), $(this).serialize()+'&btnAction='+action, function(response) {
			build_alert(response);
			if( $(response).find('#modifiers').size() > 0 ) {
				$('#modifiers').html($(response).find('#modifiers').html());
				$('a.modifier.modal').colorbox.close("Modifier added successfully");
				sorting();
			} else {
				$('#mod_form').html(response);
			}
		});
	});

	$('#var_form').live('submit', function(e) {
		e.preventDefault();
		var action = $(this).find('button[clicked=true]').val();
		$.post($(this).attr('action'), $(this).serialize()+'&btnAction='+action, function(response) {
			build_alert(response);
			if( $(response).find('#modifiers').size() > 0 ) {
				$('#modifiers').html($(response).find('#modifiers').html());
				$('a.modifier.modal').colorbox.close("Variation added successfully");
				sorting();
			} else {
				$('#var_form').html(response);
			}
		});
	});
	
	// Luis Fernando Salazar B.
	var base_url = $('#baseurl').html();  // seleccionamos la base url de un div
	
	$(document).on("click", ".update_modifier_ajax", function(event)
	{
		event.preventDefault();
		//alert($(this).attr('href'));
		$.ajax({
			type: "POST",
			url: $(this).attr('href'),
			beforeSend: function () {
			    $("#cboxContent").html('<img src="'+base_url+'uploads/default/loading.gif" width="28" height="28" class="img_loadig" />');
			},
			success: function(html){
				$("#cboxContent").html(html);
			},
			error: function(err)
	        {
	        	alert("Ocurrió un error. Por favor inténtelo de nuevo.");
	        }
		});

	});
	
	$(document).on("submit", ".form_modifier_ajax", function(event)
	{
		event.preventDefault();
		element = $(this);
		$.ajax({
			url: element.attr('action'),
			type: 'POST',
			dataType: "json",
			data:  element.serialize(),
			beforeSend: function () 
			{
			    //$("#cboxContent").html('<img src="'+base_url+'uploads/default/loading.gif" width="28" height="28"/>');
			},
			success: function(data)
			{
				if(data.status != 'error')
				{
					location.reload(true);
				}
				else
				{
					alert(data.msg);
				}
			},
			error: function(err)
	        {
	        	alert("Ocurrió un error. Por favor inténtelo de nuevo.");
	        }
		});
	});
});

function sorting() {

	$('.modifiers tbody').sortable({
		handle: 'span.mod-mover',
		update: function() {
			$('.modifiers tbody > tr').removeClass('alt');
			$('.modifiers tbody > tr:nth-child(even)').addClass('alt');
			var order = [];
			$('.modifiers tbody > tr').each(function() { order.push(this.id); });
			order = order.join(',');
			$.post(SITE_URL + 'admin/firesale/ajax/modifiers_order', { order: order });
		}
	});

	$('.modifiers tbody table tbody').sortable({
		handle: 'span.var-mover',
		update: function() {
			$('.modifiers table tbody > tr').removeClass('alt');
			$('.modifiers table tbody > tr:nth-child(even)').addClass('alt');
			var order = [];
			$('.modifiers table tbody > tr').each(function() { order.push(this.id); });
			order = order.join(',');
			$.post(SITE_URL + 'admin/firesale/ajax/variations_order', { order: order });
		}
	});

}
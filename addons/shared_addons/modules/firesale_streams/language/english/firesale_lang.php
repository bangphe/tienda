<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['firesale:sections:streams']     = 'Streams';

$lang['firesale:title:add_field']      = 'Add Field';
$lang['firesale:title:edit_field']     = 'Edit Field';

$lang['firesale:shortcuts:add_stream'] = 'Add Stream';
$lang['firesale:shortcuts:add_field']  = 'Add Field';
$lang['firesale:button:fields']        = 'Fields';

$lang['firesale:label_streamname']     = 'Stream Name';
$lang['firesale:label_streamslug']     = 'Stream Slug';
$lang['firesale:label_streamprefix']   = 'This will automatically be prefixed with "firesale_"';
$lang['firesale:label_name']           = 'Name';
$lang['firesale:label_namespace']      = 'Namespace';
$lang['firesale:label_type']           = 'Field Type';
$lang['firesale:label_required']       = 'Required?';
$lang['firesale:label_unique']         = 'Unique?';
$lang['firesale:label_fieldsin']       = 'Fields in "%s"';

$lang['firesale:streams:no_fields']    = 'There are currently no fields in this stream';
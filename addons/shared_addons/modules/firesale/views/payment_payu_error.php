<?php
$id = $this->uri->segment(3);
$msj = '';
if (!empty($id)) {
    if ($id == 4)
        $msj = 'Transacción rechazada por la entidad.';
    if ($id == 5)
        $msj = 'Transacción declinada por la entidad financiera.';
    if ($id == 10004)
        $msj = 'Error en el Ajuste.';
    if ($id == 'indefinido')
        $msj = 'No hubo un proceso exitoso; podrian haber muchas razones tales como: <br><br>-Fondos Insuficientes<br>-Tarjeta Inválida<br>-Tarjeta Vencida/Restringida<br>-Tarjeta no autorizada para realizar compras por internet<br>Entre otros..';
} else {
    redirect();
}
?>
<div class="firesale width-full confirmation">

    <h2>Mensaje de Sistema</h2>
    <p><?php echo $msj; ?></p>

    <br class="clear" />
    <br />

    <h2>Resumen del pedido</h2>

    <table class="firesale standard orders" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <th><?php echo lang('firesale:label_product'); ?></th>
            <th><?php echo lang('firesale:product:label_model'); ?></th>
            <th><?php echo lang('firesale:cart:label_quantity'); ?></th>
            <th><?php echo lang('firesale:cart:label_unit_price'); ?></th>
            <th width="130"><?php echo lang('firesale:cart:label_total'); ?></th>
        </tr>
        {{ items }}
        <tr>
            <td class="align-left"><strong>{{ name }}</strong></td>
            <td>{{ code }}</td>
            <td>{{ qty }}</td>
            <td>{{ price }}</td>
            <td>{{ total }}</td>
        </tr>
        {{ /items }}
        <tr>
            <td class="align-right" colspan="4"><strong><?php echo lang('firesale:cart:label_sub_total'); ?>:</strong></td>
            <td>{{ price_sub }}</td>
        </tr>
        <tr>
            <td class="align-right" colspan="4"><strong><?php echo lang('firesale:tabs:shipping'); ?>:</strong></td>
            <td>{{ price_ship }}</td>
        </tr>
        <tr class="last">
            <td class="large align-right" colspan="4"><strong><?php echo lang('firesale:cart:label_total'); ?>:</strong></td>
            <td class="large price">{{ price_total }}</td>
        </tr>
    </table>

    <br class="clear" />
    <br />

    <h2><?php echo lang('firesale:orders:label_order_status'); ?></h2>

    <table class="firesale standard orders" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <th width="150"><?php echo lang('firesale:label_status'); ?></th>
            <th><?php echo lang('firesale:orders:label_message'); ?></th>
        </tr>
        <tr>
            <td>{{ status.value }}</td>
            <td>{{ status.message }}</td>
        </tr>
    </table>

</div>

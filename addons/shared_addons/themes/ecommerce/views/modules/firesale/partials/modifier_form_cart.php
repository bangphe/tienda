<?php echo form_open("{{ firesale:url route='cart' base='no' }}/insert_in_cart", 'id="modifier_form"'); ?>
<input type="hidden" name="prd_code[0]" value="<?php echo $product['id']; ?>" />
<input type="hidden" name="id_encript" value="<?php echo $id_encript; ?>" />
<section class="form_prodcut_carrrito elem_blancos">
    <?php if (!empty($modifiers)): 
        $i = 1;
        ?>

        <?php foreach ($modifiers as $modifier): ?>
        
            <fieldset class="large-4 medium-4 small-4 columns"> 
                <label>
                    <?php echo $modifier['title']; ?>
                    <small><?php echo $modifier['instructions']; ?></small>
                </label>
                <div class="controls">
                   
                    <?php if ($modifier['type']['key'] != '2'): ?>
                        <?php if ($type == 'select'): ?>
                            <select name="options[0][<?php echo $modifier['id']; ?>]" data-id="<?php echo $modifier['id']; ?>" class="modifier_ajax modifier_<?php echo $i; ?>"> 
                            <?php
                            $i++;
                            endif; ?>
                            <?php foreach ($modifier['variations'] as $variation): ?>
                            
                                <?php if ($type == 'radio'): ?>
                                    <input type="radio" name="options[0][<?php echo $modifier['id']; ?>]" id="options_<?php echo $variation['id']; ?>" value="<?php echo $variation['id']; ?>" <?php echo $variation['selected']; ?>/>
                                    <label for="options_<?php echo $variation['id']; ?>"><?php echo $variation['title']; ?>
                                        <?php if (preg_replace('/[^A-Za-z0-9]/', ' ', $variation['difference']) > 0): ?>
                                            (<?php echo $variation['difference']; ?>)
                                        <?php endif; ?>
                                    </label>
                                <?php else: ?>
                                    <option <?php echo $variation['selected']; ?> value="<?php echo $variation['id']; ?>"><?php echo $variation['title']; ?>
                                        <?php if (preg_replace('/[^A-Za-z0-9]/', ' ', $variation['difference']) > 0): ?>
                                            (<?php echo $variation['difference']; ?>)
                                        <?php endif; ?>
                                    </option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            
                            <?php if ($type == 'select'): ?>
                            </select>
                        <?php endif; ?>
                    <?php else: ?>
                        <textarea name="options[0][<?php echo $modifier['id']; ?>]"></textarea>
                    <?php endif; ?>

                   
                </div>
            </fieldset>
        <?php endforeach; ?>

    <?php endif; ?>

 
    <fieldset class="large-4 medium-4 small-4 columns">
        <label>Cantidad</label>
        <input class="input-mini cant_prod" name="qty[0]" size="3" value="1" type="text" />     
   </fieldset>
     <div class="clear"></div>
   <fieldset class="large-12  medium-12 small-12 columns padding_l">
        <button class="btn_gris" type="submit" name="btnAction" value="cart"><span>Actualizar</span>  </button>
    </fieldset> 


</section>
<?php echo form_close(); ?>


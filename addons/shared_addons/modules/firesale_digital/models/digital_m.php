<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Digital_m extends MY_Model
{

	public function get_downloads($user_id)
	{

		// Load required libraries
		$this->load->library('files/files');

		// Perform initial query
		$query = $this->db->select('p.*, i.`id` AS `order_item_id`, i.`created` AS `ordered`')
						  ->where('p.`download` IS NOT ', 'NULL', FALSE)
						  ->where_in('`order_status`', array(2,3,4), FALSE)
						  ->where('p.`digital`', "'y'", FALSE)
						  ->where('o.`created_by`', $this->current_user->id, FALSE)
						  ->from('firesale_orders_items AS i')
						  ->join('firesale_products AS p', 'i.product_id = p.id')
						  ->join('firesale_orders AS o', 'i.order_id = o.id')
						  ->get();

		// Check for results before continuing
		if( $query->num_rows() )
		{

			// Get results
			$results = $query->result_array();

			// Loop and filter
			foreach( $results AS &$result )
			{

				// Check if access is still allowed
				if( ! $this->can_download($result['order_item_id']) )
				{
					$result['expired'] = '1';
				}
				else
				{

					// Set 
					$result['expired'] = '0';
					$result['expires'] = strtotime("+ {$result['expires_after']} {$result['expires_interval']}", strtotime($result['ordered']));
					$result['expires_in'] = $result['expires'] - time();
					
					// Add downloads remaining
					$downloads      = $this->db->get_where('firesale_digital_downloads', array('order_item_id' => $result['order_item_id']));
					$download_count = 0;

					if ($downloads->num_rows())
					{
						$download_count = $downloads->row()->downloads;
					}

					$unlimited = empty($result['downloads']) ? TRUE : FALSE;
					$result['remaining'] = ! $unlimited ? $result['downloads'] - $download_count : 'Unlimited';

				}

			}

			// Return
			return $results;
		}

		// Nothing found
		return FALSE;
	}

	public function get_product($order_item_id)
	{
		// Load the products model
		$this->load->model('firesale/products_m');

		$order_item = $this->db->select('product_id')->get_where('firesale_orders_items', array(
			'id' => $order_item_id
		));

		return $order_item->num_rows()
			? $this->products_m->get_product($order_item->row()->product_id)
			: false;
	}

	public function can_download($order_item_id)
	{
		$this->db->select('firesale_products.*, firesale_orders_items.created AS ordered')
				 ->where('firesale_products.download IS NOT NULL', NULL)
				 ->where($this->db->dbprefix('firesale_orders.order_status') . ' IN ', '(2,3,4)', FALSE)
				 ->where('firesale_orders.created_by', $this->current_user->id)
				 ->join('firesale_products', 'firesale_orders_items.product_id = firesale_products.id')
				 ->join('firesale_orders', 'firesale_orders_items.order_id = firesale_orders.id');

		$query = $this->db->get_where('firesale_orders_items',array(
			'firesale_orders_items.id' => $order_item_id,
			'firesale_products.digital' => 'y',
		));

		if ($query->num_rows())
		{
			$product = $query->row();

			$downloads = $this->db->get_where('firesale_digital_downloads', array(
				'order_item_id' => $order_item_id
			));

			$download_count = $downloads->num_rows() ? $downloads->row()->downloads : 0;

			$unlimited = empty($product->downloads) ? TRUE : FALSE;
			$never_expire = empty($product->expires_interval) ? TRUE : FALSE;

			// Figure out when the product expires.
			$expires = strtotime("+{$product->expires_after} {$product->expires_interval}", strtotime($product->ordered));

			if (($download_count < $product->downloads OR $unlimited)
				AND ($expires > time() OR $never_expire))
			{
				return TRUE;
			}
		}

		return FALSE;
	}

	public function increment($order_item_id)
	{
		// Only increment if the user can download the file
		if ($this->can_download($order_item_id))
		{
			$downloads = $this->db->get_where('firesale_digital_downloads', array(
				'order_item_id' => $order_item_id
			));

			if ($downloads->num_rows())
			{
				$downloads = (int) $downloads->row()->downloads;
				$downloads++;

				$this->db->update('firesale_digital_downloads', array(
					'downloads' => $downloads
				), array(
					'order_item_id' => $order_item_id
				));
			}
			else
			{
				$downloads = 1;

				$this->db->insert('firesale_digital_downloads', array(
					'downloads' => $downloads,
					'order_item_id' => $order_item_id
				));
			}
		}
	}

	public function total_downloads($id_slug)
	{
		if (is_numeric($id_slug))
		{
			$this->db->where('firesale_products.id', $id_slug);
		}
		else
		{
			$this->db->where('firesale_products.slug', $id_slug);
		}

		$downloads = $this->db->select('SUM(firesale_digital_downloads.downloads) AS downloads')
							  ->join('firesale_orders_items', 'firesale_digital_downloads.order_item_id = firesale_orders_items.id')
							  ->join('firesale_products', 'firesale_orders_items.product_id = firesale_products.id');

		return (int) $downloads->row()->downloads;
	}

	public function downloads($order_item_id)
	{
		$downloads = $this->db->get_where('firesale_digital_downloads', array(
			'order_item_id' => $order_item_id
		));

		return $downloads->num_rows() ? (int) $downloads->row()->downloads : 0;
	}

}
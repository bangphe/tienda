<section class="title">
    <h4>
        <?php
            if ($this->uri->segment(3) == 'create'):
                echo lang('fs_discount_codes:add_discount_code');
            else:
                echo lang('fs_discount_codes:edit_discount_code');
            endif;
        ?>
    </h4>
</section>

<section class="item">
    <div class="content">
        <?php echo form_open(uri_string(), 'id="page-form"'); ?>
        <?php echo form_hidden('parent_id', empty($page->parent_id) ? 0 : $page->parent_id); ?>
            <!-- Content tab -->
            <div class="form_inputs" id="page-content">
            	<div class="inline-form">
                	<fieldset>
                        <ul>
                            <?php foreach ($fields as $field): ?>
                                <?php if ($field['input_slug'] == 'value'): ?>
                                    <li>
                                        <label for="title">
                                            <?php echo lang(substr($field['input_title'], 5)); ?> <?php echo $field['required']; ?>
                                            <small><?php echo lang(substr($field['instructions'], 5)); ?></small>
                                        </label>
                                        <div class="input">
                                            <?php echo $field['input']; ?>
                                            <button id="toggle_type" type="button" class="btn gray"><?php if (set_value('type', $field['type_field']['value']) == 'percentage'): ?>&#37;<?php else: ?>&pound;<?php endif; ?></button>
                                            <?php echo form_hidden('type', set_value('type', $field['type_field']['value']), 'id="discount_type"'); ?>
                                        </div>
                                        <br class="clear">
                                    </li>
                                <?php elseif ($field['input_slug'] == 'products' OR $field['input_slug'] == 'categories'): ?>
                                    <li id="li-<?php echo $field['input_slug']; ?>" style="<?php if ($applies_to == '0' OR ($applies_to == '1' AND $field['input_slug'] == 'categories') OR ($applies_to == '2' AND $field['input_slug'] == 'products')): ?>display: none;<?php endif; ?>">
                                        <label for="title">
                                            <?php echo lang(substr($field['input_title'], 5)); ?> <?php echo $field['required']; ?>
                                            <small><?php echo lang(substr($field['instructions'], 5)); ?></small>
                                        </label>
                                        <div class="input"><?php echo $field['input']; ?></div>
                                        <br class="clear">
                                    </li>
                                <?php else: ?>
                                    <li id="li-<?php echo $field['input_slug']; ?>">
                                        <label for="title">
                                            <?php echo lang(substr($field['input_title'], 5)); ?> <?php echo $field['required']; ?>
                                            <small><?php echo lang(substr($field['instructions'], 5)); ?></small>
                                        </label>
                                        <div class="input"><?php echo $field['input']; ?></div>
                                        <br class="clear">
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </fieldset>
                </div>
                
            </div>

        <div class="buttons align-right padding-top">
            <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
        </div>

        <?php echo form_close(); ?>
    </div>
</section>

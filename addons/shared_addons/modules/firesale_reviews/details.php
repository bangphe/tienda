<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Firesale_Reviews extends Module {

	public $version = '1.1.0';
	public $language_file = 'firesale_reviews/firesale';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('firesale/firesale');
		$this->lang->load($this->language_file);
	}

	public function info()
	{

		$info = array(
			'name' => array(
				'en' => 'FireSale Reviews'
			),
			'description' => array(
				'en' => 'Product reviews and management'
			),
			'frontend' 		=> FALSE,
			'backend' 		=> TRUE,
			'author' 		=> 'Jamie Holdroyd',
			/*'shortcuts' => array(
				'ratings'	=> array(
					'name'	=> 'firesale:reviews:ratings',
					'uri'	=> 'admin/firesale_reviews/ratings'
				)
			)*/
		);

		return $info;
	}

    public function admin_menu(&$menu)
    {
        $menu['lang:firesale:title']['lang:firesale:sections:reviews'] = 'admin/firesale_reviews';
    }
	
	public function install()
	{

		// Variables
		$_return = TRUE;
		$_rating = 5; // Maximum initial rating
		
		// Load required items
		$this->load->driver('Streams');
		$this->lang->load($this->language_file);
	
		#############
		## REVIEWS ##
		#############
		
		// Create reviews stream
		if( !$this->streams->streams->add_stream(lang('firesale:reviews:title'), 'firesale_reviews', 'firesale_reviews', NULL, NULL) ) return FALSE;
	
		// Get stream data
		$products = end($this->streams->streams->get_streams('firesale_products'));
		
		// Build ratings
		$ratings = '';
		for( $i = 0; $i <= $_rating; ( $i += 0.5 ) ) { $i = number_format($i, 0); $ratings .= "{$i} : {$i}\n"; }
	
		// Add fields
		$fields   = array();
		$template = array('namespace' => 'firesale_reviews', 'assign' => 'firesale_reviews', 'type' => 'text', 'title_column' => FALSE, 'required' => TRUE, 'unique' => FALSE);
		$fields[] = array('name' => lang('firesale:label_product'), 'slug' => 'product', 'type' => 'relationship', 'required' => FALSE, 'extra' => array('max_length' => 6, 'choose_stream' => $products->id));
		$fields[] = array('name' => lang('firesale:label_status'), 'slug' => 'status', 'type' => 'choice', 'required' => FALSE, 'extra' => array('choice_data' => "0 : lang:firesale:label_draft\n1 : lang:firesale:label_live", 'choice_type' => 'dropdown', 'default_value' => 0));
		$fields[] = array('name' => lang('firesale:label_review_title'), 'slug' => 'title', 'type' => 'text', 'extra' => array('max_length' => 160), 'required' => FALSE, 'title_column' => TRUE);
		$fields[] = array('name' => lang('firesale:label_review'), 'slug' => 'review', 'type' => 'wysiwyg', 'extra' => array('editor_type' => 'simple'));
		$fields[] = array('name' => lang('firesale:label_rating'), 'slug' => 'rating_1', 'type' => 'choice', 'extra' => array('choice_data' => trim($ratings), 'choice_type' => 'dropdown', 'default_value' => ( $_rating / 2 )));

		// Apply defaults
		foreach( $fields AS &$field ) { $field = array_merge($template, $field); }
		
		// Add fields to stream
		$this->streams->fields->add_fields($fields);

		// Add settings
		$this->settings('add');

		// Add routes
		$this->routes('add');

		// Return
		return $_return;
	}

	public function uninstall()
	{

		// Variables
		$_return = TRUE;
		
		// Load required items
		$this->load->driver('Streams');
		
		// Remove streams
		$this->streams->utilities->remove_namespace('firesale_reviews');

		// Remove settings
		$this->settings('remove');

		// Remove routes
		$this->routes('remove');

		// Return
		return $_return;
	}

	
	public function settings($action)
	{
	
		// Variables
		$return     = TRUE;
		$settings   = array();

		// Require login to purchase
		$settings[] = array(
			'slug' 		  	=> 'firesale_review_accept',
			'title' 	  	=> lang('firesale:reviews:setting_accept'),
			'description' 	=> lang('firesale:reviews:desc_accept'),
			'default' 		=> '0',
			'value' 		=> '0',
			'type' 			=> 'select',
			'options' 		=> '1=Yes|0=No',
			'is_required' 	=> 1,
			'is_gui' 		=> 1,
			'module' 		=> 'firesale'
		);

		// Perform	
		if( $action == 'add' )
		{
			if( !$this->db->insert_batch('settings', $settings) )
			{
				$return = FALSE;
			}
		}
		elseif( $action == 'remove' )
		{
			foreach ($settings as $setting)
			{
				if( !$this->db->delete('settings', array('slug' => $setting['slug'])) )
				{
					$return = FALSE;
				}
			}
		}
		
		return $return;	
	}

	public function routes($action)
	{

		// Load required items
		$this->load->model('firesale/routes_m');

		// Routes
		$routes   = array();
		$routes[] = array('is_core' => 0, 'title' => 'Reviews', 'slug' => 'reviews', 'table' => '', 'map' => 'reviews/create/{{ id }}', 'route' => 'reviews/create/([0-9]+)', 'translation' => 'firesale_reviews/front/create/$1');

		// Perform
		foreach( $routes AS $route )
		{

			// Check action
			if( $action == 'add' )
			{
				// Add
				$this->routes_m->create($route);
			}
			else if( $action == 'remove' )
			{
				// Remove
				$this->routes_m->delete($route['slug']);
			}

		}

	}

	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		return TRUE;
	}
	
	public function help()
	{

		return '<iframe src="https://www.getfiresale.org/documentation/modules/reviews" style="width: 1100px; height: 650px; border-radius: 4px 4px 0 0"></iframe>';
	}

}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Theme_Ecommerce extends Theme
{
    public $name = 'Ecommerce';
    public $author = 'Imaginamos SAS';
    public $author_frontend = 'William Villalba';
    public $author_website = 'http://imaginamos.com';
    public $website = 'http://imaginamos.com';
    public $description = 'Tema Ecommerce Imaginamos 2014';
    public $version = '1.0';
    
}
 /* End of file theme.php */
<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Title
$lang['fs_discount_codes:sections:discounts'] = 'Códigos de descuento';
$lang['fs_discount_codes:add_discount_code'] = 'Nuevo código de descuento';
$lang['fs_discount_codes:edit_discount_code'] = 'Editar código de descuento';

// Shortcuts
$lang['fs_discount_codes:shortcuts:add'] = 'Nuevo código de descuento';

// Field labels & instructions
$lang['fs_discount_codes:code'] = 'Nombre del Código';
$lang['fs_discount_codes:type'] = 'Tipo de descuento';
$lang['fs_discount_codes:value'] = 'Valor';
$lang['fs_discount_codes:value_info'] = 'Cuanto es el valor del código de descuento (porcentaje o tasa fija)'; // Instructions
$lang['fs_discount_codes:desc'] = 'Descripción';
$lang['fs_discount_codes:applies_to'] = 'Aplicar a';
$lang['fs_discount_codes:products'] = 'productos';
$lang['fs_discount_codes:categories'] = 'Categorias';
$lang['fs_discount_codes:usage'] = 'Uso';
$lang['fs_discount_codes:start_date'] = 'Fecha de inicio';
$lang['fs_discount_codes:end_date'] = 'Fecha final';
$lang['fs_discount_codes:used'] = 'Usado';
$lang['fs_discount_codes:code_used'] = 'Código usado';

// Dropdown values
$lang['fs_discount_codes:everything'] = 'Todo';
$lang['fs_discount_codes:selected_prods'] = 'Productos seleccionados';
$lang['fs_discount_codes:selected_cats'] = 'Categorias Seleccionados';
$lang['fs_discount_codes:once'] = 'Una vez';
$lang['fs_discount_codes:once_user'] = 'Una vez por usuario';
$lang['fs_discount_codes:multiple'] = 'Múltiple';
$lang['fs_discount_codes:yes'] = 'Si';
$lang['fs_discount_codes:no'] = 'No';

// Admin messages
$lang['fs_discount_codes:messages:no_codes'] = 'Actualmente no hay códigos de descuento para mostrar';
$lang['fs_discount_codes:messages:deleted'] = 'Código de descuento eliminado correctamente';
$lang['fs_discount_codes:messages:deleted_multiple'] = 'Los códigos de descuento seleccionados fueron borrados exitosamente';


// Frontend messages
$lang['fs_discount_codes:message:discount_applied'] = 'El código de descuento se aplicó con éxito a tu carrito';
$lang['fs_discount_codes:message:login_required'] = 'Usted debe estar conectado para aplicar ese código de descuento';
$lang['fs_discount_codes:message:already_used'] = 'Usted ya ha utilizado este código en un pedido anterior';
$lang['fs_discount_codes:message:invalid'] = 'El código que escribió no parece ser válido';
$lang['fs_discount_codes:message:already_applied'] = 'Usted ya tiene un código de descuento aplicado a su carrito';
$lang['fs_discount_codes:message:code_removed'] = 'Código de descuento removido';
$lang['fs_discount_codes:message:code_removed_fail'] = 'No hay código de descuento para remover';

<?php echo form_open_multipart(site_url('admin/firesale/products/edit_modifier_ajax/'.(isset($data) ? $data->id : '')), 'class="crud form_modifier_ajax"'); ?>
<fieldset>
	<ul>
		<li>
			<label for="name">Precio Actual</label>
			<div class="input"><?php echo form_input('price', (isset($data->price)) ? $data->price : set_value('price'), 'class="dev-input-price" style="width:30%"'); ?></div>
		</li>
		<li>
			<label for="name">Nivel de Stock</label>
			<div class="input"><?php echo form_input('stock', (isset($data->stock)) ? $data->stock : set_value('stock'), 'class="dev-input-title" style="width:15%"'); ?></div>
		</li>
		<li>
			<label for="name">Descuento %</label>
			<div class="input"><?php echo form_input('descuento', (isset($data->descuento)) ? $data->descuento : set_value('descuento'), 'class="dev-input-title" style="width:10%"'); ?></div>
		</li>
	</ul>
	<?php
		$this->load->view('admin/partials/buttons', array('buttons' => array('save')));
	?>
</fieldset>
<?php echo form_close(); ?>
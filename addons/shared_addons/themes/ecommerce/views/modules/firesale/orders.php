<section class="cont_home m_b_20">  

  
  <section class="marco bg_pasos_comp">
      <!--bg Carrito-->    
      {{ theme:partial name="account_nav" }}
      <!--Fin bg Carrito--> 

      <section class="large-9 medium-9 small-12 columns item2">

        <section class="cont_cat_perfil">

          <div class=" relarive elem_blancos  animated fadeInUp form_edit">

             <div class="tittles">
               <h1>Historial de Pedidos</h1>  
             </div>


             <div class="resultados-busqueda">

               {{ if orders }}
                <div class="listabuscador">
                  {{ orders }}
                  <div class="animated fadeInUp" data-wow-delay=".1s">
                    <div class="row">
                      <div class="small-12  medium-8 large-12 columns">
                        <h2 class="small-12 medium-12 large-12 columns m_b_10">Pedido #{{ id }} </h2>                      
                        <div class="clear"></div>                      
                        <p class="small-12 medium-6 large-3 columns">Fecha: {{ helper:date timestamp=created format="d/m/Y" }}</p>                      
                        <!-- <p  class="small-12 medium-6 large-2 columns">Articulos: {{ count }}</p>                       -->
                        <p  class="small-12 medium-6 large-3 columns">Precio: {{ price_total_formatted }}</p>                      
                        <p  class="small-12 medium-6 large-3 columns">
                          Estado:  {{ if order_status }}
                            {{ order_status.value }}
                            {{ else }}
                            Solicitud de Pedido Enviada
                            {{ endif }}
                        </p>
                        <div class="small-12 medium-6 large-3 columns">
                          <a href="{{ firesale:url route='orders-single' id=id }}" class="btn_gris right"><i class="fa fa-search m_r_10"></i>Ver</a>
                        </div>                                             
                        <div class="clear"></div>                      
                      </div>                      
                    </div>
                  </div>
                  {{ /orders }}

                  {{ else }}
                        <div class="aler_modal_p">
                          <p>No hay pedidos Actualmente</p>
                          <i class="fa fa-warning"></i>
                        </div>
                   {{ endif }}

                </div>
              </div>
           

             <div class="clear"></div>
          </div>

            <div class="clear"></div>
        </section>

      </section>  

  <!--Fin Campos Perfil-->
  </section>

</section>


               
                
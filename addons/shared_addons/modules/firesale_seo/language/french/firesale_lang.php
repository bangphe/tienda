<?php defined('BASEPATH') or exit('No direct script access allowed');

    // FireSale SEO v 1.0

    // General
    $lang['firesale:seo:title']    = 'Référencement';
    $lang['firesale:tab_metadata'] = 'Balises Méta';

    // Labels
    $lang['firesale:seo:label_title']    = 'Balise Titre';
    $lang['firesale:seo:label_desc']     = 'Balise Description';
    $lang['firesale:seo:label_keywords'] = 'Balise Mots-Clés';

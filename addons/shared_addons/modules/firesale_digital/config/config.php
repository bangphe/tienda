<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Upload Path
|--------------------------------------------------------------------------
|
| Where would you like to upload your digital products to?
| NO TRAILING SLASH
|
|	UPLOAD_PATH . 'firesale_digital';
|
| It is recommended that this path is outside of your document root for
| security reasons.
|
| There is no need to create this folder, it will be created automatically
| along with a .htaccess file to protect your files.
|
*/
$config['fs_digital_upload_path'] = UPLOAD_PATH . 'firesale_digital';
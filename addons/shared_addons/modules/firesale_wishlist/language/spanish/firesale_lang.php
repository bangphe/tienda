<?php defined('BASEPATH') OR exit('No direct script access allowed');

	// General
    $lang['firesale:wish:title']               = 'Lista de deseos';

    // Tabs
    $lang['firesale:wish:tabs:general']        = 'General';
    $lang['firesale:wish:tabs:items']          = 'Artículos';

    // Lists
	$lang['firesale:wish:add']                 = 'Crear nueva lista de deseos';
	$lang['firesale:wish:add:success']         = 'Lista de deseo creada correctamente';
	$lang['firesale:wish:add:error']           = 'Error creando la lista de deseo';
	$lang['firesale:wish:edit']                = 'Editar lista de deseo';
	$lang['firesale:wish:edit:success']        = 'Lista de deseo editada correctamente';
	$lang['firesale:wish:edit:error']          = 'Error editando lista de deseo';
	$lang['firesale:wish:delete']              = 'Borrar lista de deseo';
	$lang['firesale:wish:delete:success']      = 'Lista de deseo eliminada correctamente';
	$lang['firesale:wish:delete:error']        = 'Error borrando la lista de deseo';
	$lang['firesale:wish:none']                = 'No se encontro una lista de deseo, Click <a href="%s">Acá</a> para crear una';
	$lang['firesale:wish:back']                = 'Volver a la lista de deseos';

	// Items
	$lang['firesale:wish:item:add']            = 'Agregar Aatículo a %s';
	$lang['firesale:wish:item:add:short']      = 'Agregar artículo';
	$lang['firesale:wish:item:add:success']    = 'Artículo agregado correctamente';
	$lang['firesale:wish:item:add:error']      = 'Error agregando el artículo';
	$lang['firesale:wish:item:edit']           = 'Editar artículo';
	$lang['firesale:wish:item:edit:short']     = 'Actualizar artículo';
	$lang['firesale:wish:item:edit:success']   = 'artículo actualizado correctamente';
	$lang['firesale:wish:item:edit:error']     = 'Error editando el artículo';
	$lang['firesale:wish:item:delete']         = 'Eliminarlo de %s';
	$lang['firesale:wish:item:delete:short']   = 'Remover artículos';
	$lang['firesale:wish:item:delete:success'] = 'Artículo removido correctamente';
	$lang['firesale:wish:item:delete:error']   = 'Error al remover el artículo';
	$lang['firesale:wish:item:none']           = 'No se encontro un artículo, Click <a href="%s">Acá</a> para encontrar más';

	// Defaults
	$lang['firesale:wish:default:name']        = 'Lista de deseo por defecto';
	$lang['firesale:wish:default:desc']        = 'Su lista de deseo por defecto';

    // Labels
	$lang['firesale:wish:label:title']         = 'Nombre';
	$lang['firesale:wish:label:user']          = 'Usuario';
	$lang['firesale:wish:label:status']        = 'Estado';
	$lang['firesale:wish:label:privacy']       = 'privacidad';
	$lang['firesale:wish:label:public']        = 'Publico';
	$lang['firesale:wish:label:private']       = 'Privado';
	$lang['firesale:wish:label:link']          = 'Visible con Enlace';
	$lang['firesale:wish:label:desc']          = 'Descripción';
	$lang['firesale:wish:label:product']       = 'Producto';
	$lang['firesale:wish:label:quantity']      = 'Cantidad';
	$lang['firesale:wish:label:items']         = 'Artículos';
	$lang['firesale:wish:label:value']         = 'Valor';

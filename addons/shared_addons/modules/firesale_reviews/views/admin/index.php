
    <form action="<?php echo site_url(); ?>admin/firesale_reviews/delete" method="post" accept-charset="utf-8">

        <section class="title">
            <h4><?php echo lang('firesale:reviews:title'); ?></h4>
            <a class="tooltip-s show-filter" original-title="Show Filters"></a>
        </section>

        <section class="item">
			<div class="content">
                <fieldset id="filters" style="display: none">
                    <legend><?php echo lang('global:filters'); ?></legend>
                    <ul>
                        <li class="<?php echo alternator('even', ''); ?>">
                            <div class="input"><?php echo form_dropdown('filter', $products, $product, 'id="filter"'); ?></div>
                        </li>
                    </ul>
                </fieldset>
    
    <?php if( count($reviews) <= 0 ): ?>
                <div class="no_data"><?php echo lang('firesale:reviews:none'); ?></div>
    
                </section>
    
    <?php else: ?>
                <div class="scroll-table">
                    <table id="reviews_table">
                        <thead>
                            <tr>
                                <!--<th style="width: 15px"><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>-->
                                <th style="width: 180px"><?php echo lang('firesale:label_title'); ?></th>
                            <?php foreach( $fields as $t => $s ): ?>
                                <th class="rating"><?php echo $t; ?></th>
                            <?php endforeach; ?>
                                <th style="width: 90px"><?php echo lang('firesale:label_status'); ?></th>
                                <th style="width: 180px"></th>
                            </tr>
                            
                        </thead>
                        <tbody>
        <?php foreach( $reviews AS $review ): ?>
                            <tr>
                                <!--<td><input type="checkbox" name="action_to[]" value="<?php echo $review['id']; ?>"  /></td>-->
                                <td><?php echo $review['product']['title']; ?></td>
                            <?php foreach( $fields as $t => $s ): ?>
                                <td class="rating"><?php echo $review[$s]['value']; ?></td>
                            <?php endforeach; ?>
                                <!--<td><?php echo form_dropdown('status_' . $review['id'], array('0' => lang('firesale:label_draft'), '1' => lang('firesale:label_live')), set_value('status', $review['status']['key']), 'id="status_' . $review['id'] . '"'); ?></td>-->
								<td><?php echo ($review['status']['key'] == 1 ? 'Activo' : 'Inactivo'); ?></td>
                                <td><center>
                                    <!--<a href="<?php echo site_url(); ?>admin/firesale_reviews/status/<?php echo $review['id']; ?>" class="btn green small">Update</a>-->
                                    <a href="<?php echo site_url(); ?>admin/firesale_reviews/edit/<?php echo $review['id']; ?>" class="btn blue small">Edit</a>
                                    <a href="<?php echo site_url(); ?>admin/firesale_reviews/delete/<?php echo $review['id']; ?>" class="btn red small">Delete</a>
                                </center></td>
                            </tr>
        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <br />
                <div class="table_action_buttons">
                    <!--<button type="submit" name="btnAction" value="delete" class="btn red confirm"><span>Delete</span></button>-->
                </div>
			</div>
        </section>

<?php endif; ?>
    </form>

<?php if( isset($pagination) ): ?>
    <?php echo $pagination['links']; ?>
<?php endif; ?>

<section class="item">
    <div class="image">
        <h2>Home/Otros</h2>
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-principal"><span><?php echo $titulo; ?></span></a></li>
            </ul>

            <div class="inline-form" id="page-principal">
                <?php echo form_open_multipart(site_url('admin/home/edit_others_vid/'.(isset($item) ? $item->id : '')), 'class="crud"'); ?>
                <fieldset>
                    <ul>
                        <li>
                            <label for='content'>Video <span>*</span><small>- Ancho Máximo 262px <br>- Alto Máximo 202px</small></label><div class='input'>
                            <textarea name='content' rows='10' class='dev-input-textarea'><?= (isset($item->content))?$data[0]->content:'' ?></textarea></div>
                        </li>
                        <li>
                            <label for="text">Texto</label>
                            <div class="input"><?php echo form_input('text', (isset($item->text)) ? $item->text : set_value('text'), 'class="dev-input-title" style="width:100%"'); ?></div>
                        </li>
                    </ul>
                    <?php
                        $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel')));
                    ?>
                </fieldset>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Brayan Acebo
 */
class Home extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $models = array(
            'home_pautas_m',
            'home_slider_m',
            'home_banner_m',
            'home_banner_two_m',
            'home_others_m'
        );
        $this->load->library('files/files');
        $this->load->model($models);

        // Initialise data
        $this->data = new stdClass();

        $this->load->driver('Streams');
        // Get the stream
        $this->stream = $this->streams->streams->get_stream('firesale_brands', 'firesale_brands');
    }

    // -----------------------------------------------------------------

    public function index()
    {	
        // Variables
        $params = array(
            'stream'       => 'firesale_brands',
            'namespace'    => 'firesale_brands',
            'paginate'     => 'yes',
            'page_segment' => 4,
            'where' => "featured = 1",
        );

        // Assign brands
        $this->data->brands = $this->streams->entries->get_entries($params);

        // Add images
        foreach( $this->data->brands['entries'] AS &$brand ) {

            // Assign images
            $folder = get_file_folder_by_slug($brand['slug'], 'brand-images');
            $images = Files::folder_contents($folder->id);

            $brand['image'] = $images['data']['file'][0];

            // Assign categories
            $brand['categories'] = $this->pyrocache->model('brands_m', 'get_categories', array($brand['id']), $this->firesale->cache_time);
        }

        // Add page data
        $this->template->title(lang('firesale:title') . ' ' . lang('firesale:sections:brands'))
                       ->set($this->data);

		// Banner
        $banner = $this->home_banner_m->get_all();

        // Banner 2
        $banner_two = $this->home_banner_two_m->get_all();
		
        // Pautas
        $pautas = $this->home_pautas_m->limit(1)
                ->get_all();

        // Slider
        $slider = $this->home_slider_m
                ->order_by('orden','ASC')
                ->get_all();



        $products = array();

        $others = $this->home_others_m->order_by('type','DESC')->get_all();
        foreach($others AS $item){
            if($item->type != 1){

                $videoJson = json_decode(file_get_contents('http://www.youtube.com/oembed?url=' . $item->content . '&format=json'));
                $item->content_img = $videoJson->thumbnail_url;
                preg_match('/src="([^"]+)"/', $videoJson->html, $match);
                $url = $match[1];
                $item->content = $url;
            }
        }
		
		// llamamos el modelo de productos de firesale
		$this->load->model('firesale/products_m');
        foreach ($slider as &$product) {
            $product_item = cache('products_m/get_product', $product->producto_id);
            if(!empty($product_item))
            {
                $products[] = $product_item;
            }
        }
		
        
		Asset::add_path('newsletter', 'addons/shared_addons/modules/newsletter/');
		
        $this->template
				->append_js('newsletter::jquery_validate.js')
				->append_js('newsletter::formvalidate.js')
                ->set('pautas', $pautas[0])
                ->set('products', $products)
				->set('banner', $banner)
                ->set('banner_two', $banner_two)
                ->set('others', $others)
                ->build('home_front');
    }

}
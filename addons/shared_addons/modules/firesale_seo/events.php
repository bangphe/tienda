<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Events_Firesale_seo
{

    protected $ci;
    protected $uri = array('products', 'categories');

    public function __construct()
    {

        $this->ci =& get_instance();

        // register the events
        Events::register('product_updated',    array($this, 'product_updated'));
        Events::register('page_build',         array($this, 'page_build'));
        Events::register('form_build',         array($this, 'form_build'));

    }

    public function product_updated($input)
    {

        // Load required items
        $this->ci->load->model('redirects/redirect_m');
        $this->ci->load->model('firesale/categories_m');
        $this->ci->load->model('firesale/products_m');

        // Get current product
        $product = $this->ci->products_m->get_product($input['id']);

        // Set up automatic redirects
        if( $product['slug'] != $input['slug'] ) {

            // Build redirect data
            $data = array(
                        'type' => '301',
                        'from' => 'product/' . $product['slug'],
                        'to'   => 'product/' . $input['slug']
                    );

            // Check this from exists
            if( $this->ci->redirect_m->check_from($data['from']) > 0 ) {
                // Update it
                $from = $this->ci->redirect_m->get_from($data['from']);
                $this->ci->redirect_m->update($site->id, $data);
            } else {
                // Insert it
                $this->ci->redirect_m->insert($data);
            }

            // Ensure we're not going to get stuck in a loop
            $where = array('from' => $data['to'], 'to' => $data['from']);
            if( $this->ci->db->where($where)->get('redirects')->num_rows() ) {
                $this->ci->db->where($where)->delete('redirects');
            }

        }

    }

    public function page_build($template)
    {

        // Build Front-end products page
        if( in_array($this->ci->uri->rsegment('1'), array('front_product', 'front_category')) ) {

            // Get current item
            if( $this->ci->uri->rsegment('1') == 'front_product' ) {
                $item = $this->ci->pyrocache->model('products_m', 'get_product', array($template->id), $this->ci->firesale->cache_time);
            } else if( $this->ci->uri->rsegment('1') == 'front_category' ) {
                $item = $this->ci->pyrocache->model('categories_m', 'get_category', array($template->id), $this->ci->firesale->cache_time);
            }

            // Alter meta title
            if( isset($item['meta_title']) AND strlen(trim($item['meta_title'])) > 0 ) {
                $template->title($item['meta_title']);
            }

            // Alter meta description
            if( isset($item['meta_description']) AND strlen(trim($item['meta_description'])) > 0 ) {
                $template->append_metadata('<meta name="description" content="' . $item['meta_description'] . '"/>');
            } else {
                $item['meta_description'] = substr($product['description'], 0, 160);
                $template->append_metadata('<meta name="description" content="' . $item['meta_description'] . '"/>');
            }

            // Alter meta keywords
            if( isset($item['meta_keywords']) AND strlen(trim($item['meta_keywords'])) > 0 ) {
                $template->append_metadata('<meta name="keywords" content="' . $item['meta_keywords'] . '"/>');
            }

        }
        // Append required assets to product creation/edit
        else if( in_array($this->ci->uri->segment('3'), $this->uri) ) {

            // Find path
            if( file_exists(SHARED_ADDONPATH . 'modules/firesale_seo/details.php') ) {
                $dir = BASE_URL . 'addons/shared_addons/modules/';
            } elseif ( file_exists('addons/' . SITE_REF . '/modules/firesale_seo/details.php') ) {
                $dir = BASE_URL . 'addons/' . SITE_REF . '/modules/';
            } else {
                $dir = (defined('PYROPATH') ? PYROPATH : APPPATH) . 'modules/';
            }

            // Add asset path
            Asset::add_path('firesale_seo', $dir . 'firesale_seo/');

            // Append metadata
            $template->append_js('firesale_seo::jquery.tagsinput.js');
            $template->append_js('firesale_seo::seo.js');
            $template->append_css('firesale_seo::jquery.tagsinput.css');
            $template->append_css('firesale_seo::seo.css');
        }
        // Add google analytics tracking
        else if( $this->ci->uri->segment('2') == 'payment' AND $this->ci->settings->get('ga_tracking') ) {

            // Build metadata
            $metadata = $this->ci->parser->parse('firesale_seo/analytics', $template->order, true);

            // Add to metadata
            $template->append_metadata($metadata);
        }

    }

    public function form_build($controller)
    {

        if( in_array($controller->section, $this->uri) ) {

            // Remove images (needs to be last)
            unset($controller->tabs['_images']);

            // Add metadata to tabs
            $controller->tabs['metadata'] = array('meta_title', 'meta_description', 'meta_keywords');

            // Add images back in
            $controller->tabs['_images'] = array();

        }

    }

}

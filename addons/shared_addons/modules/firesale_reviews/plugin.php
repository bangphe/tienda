<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Plugin_Firesale_reviews extends Plugin
{

    private $product;
    private $stream;
    private $count = 0;

    public function __construct()
    {

        // Load libraries
        $this->lang->load('firesale_reviews/firesale');
        $this->load->model('firesale/products_m');
        $this->load->helper('firesale/general');
        $this->load->model('reviews_m');

        // Get from cache
        if ( ! $this->stream  = $this->cache->get(md5(BASE_URL.'reviews_stream')) ) {
            $this->stream = $this->streams->streams->get_stream('firesale_reviews', 'firesale_reviews');
            $this->cache->save(md5(BASE_URL.'reviews_stream'), $this->stream, $this->firesale->cache_time);
        }
    }

    public function product_rating()
    {

        // Variables
        $product = $this->attribute('product');
        $rating  = $this->pyrocache->model('reviews_m', 'review_rating', array($product), $this->firesale->cache_time);

        // Return
        return $rating;
    }

    public function get()
    {

        // Variables
        $product = $this->attribute('product');
        $reviews = $this->pyrocache->model('reviews_m', 'get_reviews_by_product', array($product), $this->firesale->cache_time);

        // Check for reviews
        if( $reviews != FALSE ) {

            // Get max field values
            $fields = $this->pyrocache->model('reviews_m', 'max_values', array(), $this->firesale->cache_time);

            // Add rating values
            $this->reviews_m->assign_values($reviews, $fields);

            return $reviews;
        }

        return array();
    }

    public function get_count()
    {

        // Variables
        $product = $this->attribute('product');
        
        $count = $this->pyrocache->model('reviews_m', 'review_count', array($product), $this->firesale->cache_time);

        // Return count
        return $this->content()
            ? array('count' => $count)
            : $count;
    }

    public function reviews()
    {

        // Variables
        $product    = $this->attribute('product');
        $product    = $this->pyrocache->model('products_m', 'get_product', array($product), $this->firesale->cache_time);
        $cache_key  = md5(BASE_URL.'reviews'.$this->attribute('product'));
		
        // Get from cache
        if ( !($reviews = $this->cache->get($cache_key)) || empty($reviews['entries']))
        {
            // Set query paramaters
            $params  = array(
                        'stream'    => 'firesale_reviews',
                        'namespace' => 'firesale_reviews',
                        'order_by'  => 'id',
                        'sort'      => 'desc',
                        'where'     => "product = '{$product['id']}' AND default_firesale_reviews.status = '1'"
                       );

            // Get entries
            $reviews = $this->streams->entries->get_entries($params);
            $fields  = $this->pyrocache->model('reviews_m', 'max_values', array(), $this->firesale->cache_time);

            // Set count
            $this->count = $reviews['total'];

            // Add rating values
            $this->reviews_m->assign_values($reviews['entries'], $fields);

            $this->cache->save($cache_key, $reviews, $this->firesale->cache_time);
        }
        
        if(!empty($reviews['entries']))
        {
            $i = 0;
            $total = 0;
            foreach($reviews['entries'] AS $item)
            {

                $total += $item['rating_1']['value'];
                $i++;
            }
            $rating = $total/$i;
            $rating = (int)$rating;
        }
		
        // return reviews
        return array(array('rating' => $rating));
    }

    public function form()
    {

        // Variables
        $product   = $this->attribute('product');
        $cache_key = md5(BASE_URL.'form'.$this->attribute('product'));

        // Get from cache
        if ( ! $data = $this->cache->get($cache_key) ) {

            // Get the stream fields
            $fields = $this->fields->build_form($this->stream, 'new');

            // Remove unwanted front-end options
            for( $i = 0; $i < count($fields); $i++ ) {
                if( in_array($fields[$i]['input_slug'], array('product', 'status')) ) {
                    unset($fields[$i]);
                }
            }

            // Assign Data
            $data->product  = $this->pyrocache->model('products_m', 'get_product', array($product), $this->firesale->cache_time);
            $data->fields   = $fields;
            $data->reviewed = $this->pyrocache->model('reviews_m', 'user_has_reviewed', array($this->current_user->id, $data->product['id']), $this->firesale->cache_time);

            $this->cache->save($cache_key, $data, $this->firesale->cache_time);
        }

        // Return view
        return $this->module_view('firesale_reviews', 'form', $data, true);
    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author 	Luis Fernando Salazar Buitrago
 * @package 	PyroCMS
 * @subpackage 	newsletter Module
 * @category 	Modulos
 * @license 	Apache License v2.0
 */

class Newsletter extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('newsletter_m'));
    }

    public function insert_email()
	{
		$this->form_validation->set_rules('email', 'Correo', 'required|trim|valid_email');
		$statusJson = 'error';
		$msgJson = 'Ocurrio un error intentelo de nuevo';
		if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion
		{
			if(validation_errors() != "")
			{
				$title = 'Advertencia';
				$msgJson = validation_errors();
			}
		}
		else // si el formulario ha sido enviado con éxito se procede
		{
			unset($_POST['btnAction']);
			unset($_POST['termCond']);
            $data = $_POST;
			
			$email = $this->newsletter_m
				->where('email', $_POST['email'])
		        ->get_all();
			
			if(!empty($email))
			{
				$title = 'Advertencia';
				$msgJson = 'Tu correo ya se encuentra registrado, muchas gracias.';
			}
			else
			{
	            if($this->newsletter_m->insert($data))
	            {
	            	$statusJson = '';
	            	$title = 'SUBSCRIPCIÓN EXITOSA!';
					$msgJson = 'Gracias por tu subscripción, disfrutaras de la mejor información semanal mente, recuerda recomendar 
								TU TIENDA 
								a tus amigos!';
	            }
			}
		}
		echo json_encode(array('status' => $statusJson, 'msg' => $msgJson, 'title' => $title));

	}

}


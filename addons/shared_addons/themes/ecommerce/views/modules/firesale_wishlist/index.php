<section class="cont_home m_b_20">  

  
  <section class="marco bg_pasos_comp">
      <!--bg Carrito-->    
      {{ theme:partial name="account_nav" }}
      <!--Fin bg Carrito--> 

      <section class="large-9 medium-9 small-12 columns item2">

        <section class="cont_cat_perfil">

          <div class=" relarive elem_blancos  animated fadeInUp form_edit">

             <div class="tittles">
                 <h1>MIS LISTAS DE DESEOS</h1> 
             </div>

              <!--Fin Titulo--> 
              <ul class="cajasdir animated fadeInUp">
                {{ if lists.entries }}
                  {{ lists.entries }}
                    <li class="large-4 medium-6 small-12 columns item1">
                      <div class="caja_borde">
                        <span class="delete_deseo">
                          <a href="{{ firesale:url route='wishlist' }}/delete/{{ id }}">
                            <span class="left_tip" data-tips="Eliminar">
                              <i class="fa fa-times"></i>
                            </span>  
                          </a>
                        </span>
                        <h3>
                          {{ title }} 
                          <div class="btn_edit"><a href="{{ firesale:url route='wishlist' }}/edit/{{ id }}">
                            <span  class="left_tip" data-tips="Editar">
                            <i class="fa fa-pencil-square-o"></i>
                            </span>
                          </a></div>
                        </h3>

                        <p>Artículos: {{ total }}</p>
                        <p>Privacidad: {{ privacy.value }}</p>
                        <p>Estado: {{ status.value }}</p>
                        <span class="bgn_ver_m">
                          <a href="{{ firesale:url route='wishlist' }}/view/{{ id }}" class="btn_border">VER</a>
                        </span>
                      </div>     
                     </li>
                    {{ /lists.entries }} 
                        <div class="clear"></div>
                  {{ else }}
                    <div>No se encontró ninguna lista de deseos</div>
                  {{ endif }}
                </ul>
                
                <div class="clear"></div>

                <div class="clear"></div>

                <div class="controles_perfil2 m_t_20">
                    <a href="{{ firesale:url route='wishlist' }}/create" title="" class="btn_gris right">AGREGAR NUEVA LISTA</a>
                    <div class="clear"></div>
                </div>   

             <div class="clear"></div>
          </div>

            <div class="clear"></div>
        </section>

      </section>  

  <!--Fin Campos Perfil-->
  </section>

</section>







		
      		
      		
	      	
<div id="baseurl" class="hide" style="display:none">{{ url:site }}</div>

<div id="json_markers" class="hide" style="display:none">{{ json_markers }}</div>

<div id="json_info_content" class="hide" style="display:none">{{ json_info_content }}</div>





<section class="cont_home m_b_30"> 

  <section class="marco">

    <!--Titulo-->

    <div class="subtittle titulo_int animated fadeInDown">

        <h1>Cont&aacute;ctanos</h1>

    </div>

      <div class="clear"></div>

    

    <ul class="cont_nosotros cont_contact"> 

      <li class="large-6 medium-6 small-12 columns animated fadeInUp elem_blancos">



        <?php echo form_open(site_url('contact_us/send'), 'class="crud form_contac_ajax"'); ?>        

            

            <div id="loading_contacts"></div>           

            <fieldset class="fila">

                <!--<label class="control-label">Nombre y apellido *</label>-->

                <input type="text" class="form-control" name="name" placeholder="Nombre y apellido *" value="<?php echo set_value('nombre') ?>">

            </fieldset>

            <fieldset class="fila">

                <!--<label class="control-label">Correo Electrónico *</label>-->

                <input type="text" class="form-control" name="email" placeholder="Correo Electrónico *" value="<?php echo set_value('correo') ?>">

            </fieldset>

            <fieldset class="fila">

                <!--<label class="control-label">Teléfono</label>-->

                <input type="text" class="form-control" name="phone" placeholder="Teléfono" value="<?php echo set_value('telefono') ?>">

            </fieldset>

           <fieldset class="fila">

                <!--<label class="control-label">Celular</label>-->

                <input type="text" class="form-control" name="cell" placeholder="Celular" value="<?php echo set_value('cell') ?>">

            </fieldset>

            <fieldset class="fila">

                <!--<label class="control-label">Empresa/Organización</label>-->

                <input type="text" class="form-control" name="company" placeholder="Empresa/Organización" value="<?php echo set_value('empresa') ?>">

            </fieldset>

            <!-- <fieldset class="fila">

                <label class="control-label">Municipio/Departamento</label>

                <input type="text" class="form-control" name="city" value="<?php echo set_value('municipio') ?>">

            </fieldset> -->

            <fieldset class="fila">

                <!--<label class="control-label">Mensaje *</label>-->

                <textarea class="form-control" rows="5" name="message" placeholder="Mensaje *"><?php echo set_value('mensaje') ?></textarea>

            </fieldset>

            <div align="right" class="form-group">

                <button type="submit" value="Enviar" class="btn_gris" name="btnAction">Enviar</button>  

            </div>

        <?php echo form_close(); ?>

      </li>

      <li class="large-6 medium-6 small-12 columns animated fadeInUp elem_blancos">

       

        <p for="path" class="text_select">Encuéntranos </p>

        <?php echo form_dropdown('id_google_map', $selectGoogleMaps, (isset($id_google_map) ? $id_google_map : set_value('id_google_map')), 'id="changeGoogleMap"'); ?>

        

        <!-- Mapa de google -->

        <div id="map_loader"></div>

        <div id="map_wrapper">

            <div id="map_canvas"></div>

        </div>

      </li>



    </ul>  



 </section> 

</section>




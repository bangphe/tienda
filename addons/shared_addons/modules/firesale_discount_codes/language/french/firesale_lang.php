<?php defined('BASEPATH') OR exit('No direct script access allowed');

// FireSale Discount Codes v 1.0

// Title
$lang['fs_discount_codes:sections:discounts'] = 'Codes de réduction';
$lang['fs_discount_codes:add_discount_code'] = 'Add Discount Code'; #translate
$lang['fs_discount_codes:edit_discount_code'] = 'Edit Discount Code'; #translate

// Shortcuts
$lang['fs_discount_codes:shortcuts:add'] = 'Ajouter un code de réduction';

// Field labels & instructions
$lang['fs_discount_codes:code'] = 'Code';
$lang['fs_discount_codes:type'] = 'Type de réduction';
$lang['fs_discount_codes:value'] = 'Valeur';
$lang['fs_discount_codes:value_info'] = 'Valeur du code (Pourcentage ou Taux fixe)'; // Instructions
$lang['fs_discount_codes:desc'] = 'Description';
$lang['fs_discount_codes:applies_to'] = 'Appliquer à';
$lang['fs_discount_codes:products'] = 'Produits';
$lang['fs_discount_codes:categories'] = 'Catégories';
$lang['fs_discount_codes:usage'] = 'Utilisation';
$lang['fs_discount_codes:start_date'] = 'Date de début';
$lang['fs_discount_codes:end_date'] = 'Date de fin';
$lang['fs_discount_codes:used'] = 'Utilisé';
$lang['fs_discount_codes:code_used'] = 'Code utilisé';

// Dropdown values
$lang['fs_discount_codes:everything'] = 'Tous';
$lang['fs_discount_codes:selected_prods'] = 'Produits sélectionnés';
$lang['fs_discount_codes:selected_cats'] = 'Catégories sélectionnées';
$lang['fs_discount_codes:once'] = 'Une seule fois';
$lang['fs_discount_codes:once_user'] = 'Une seule fois par utilisateur';
$lang['fs_discount_codes:multiple'] = 'Multiple';
$lang['fs_discount_codes:yes'] = 'Oui';
$lang['fs_discount_codes:no'] = 'Non';

// Admin messages
$lang['fs_discount_codes:messages:no_codes'] = 'Il n\'y a actuellement aucun code à afficher';
$lang['fs_discount_codes:messages:deleted'] = 'Le code a été supprimé';
$lang['fs_discount_codes:messages:deleted_multiple'] = 'Les codes ont été supprimés';


// Frontend messages
$lang['fs_discount_codes:message:discount_applied'] = 'Le code de réduction %s a bien été appliqué à votre panier';
$lang['fs_discount_codes:message:login_required'] = 'Vous devez être connecté pour utiliser le code de réduction';
$lang['fs_discount_codes:message:already_used'] = 'Vous avez déjà utilisé ce code pour une commande précédente';
$lang['fs_discount_codes:message:invalid'] = 'Le code de réduction saisi est invalide';
$lang['fs_discount_codes:message:already_applied'] = 'Votre panier bénéficie déjà d\'un code de réduction';
$lang['fs_discount_codes:message:code_removed'] = 'Code de réduction annulé';
$lang['fs_discount_codes:message:code_removed_fail'] = 'Aucun code de réduction à annuler';

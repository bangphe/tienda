/*function toggleMenu() {
	var subMenu = document.getElementById("usr-submenu");
	subMenu.classList.toggle("showSubMenu");
}*/
/*Ocultar/mostrar sub-menu*/
$('#usrmenu').click(function()
{
	$('#usr-submenu').toggle(100);
	$('#usr-submenu').addClass("showSubMenu");
});
/*
function delSubMenu(){
	document.getElementById("usr-submenu").classList.remove("showSubMenu");
}*/
$('#mainmenu').click(function()
{
	$('#usr-submenu').hide();
});
/*Activar los tooltips de bootstrap*/
$('.btnAyudaWidget').tooltip({placement:'bottom'});
/*Activar los pop overs de bootstrap*/
$('.pover').popover();
/*Ocultar/mostrar menu principal*/
$('#mainmenudisp').click(function()
{
	$('#mainmenu').toggle(100);
	if ($('#icomainmenu').hasClass('icon-chevron-down'))
	{
		$('#icomainmenu').removeClass('icon-chevron-down').addClass('icon-chevron-up');
	}
	else
	{
		$('#icomainmenu').removeClass('icon-chevron-up').addClass('icon-chevron-down');
	}
});

;(function($){
	$.fn.datepicker.dates['es'] = {
		days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
		daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
		daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
		months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		today: "Hoy"
	};
}(jQuery));

$('.input-append.date').datepicker({
    format: "yyyy-mm-dd",
    language: "es",
    autoclose: true,
    todayHighlight: true
});
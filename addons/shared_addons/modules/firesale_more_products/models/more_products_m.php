<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class More_products_m extends MY_Model {

    public $track = 20;
    public $type = 'cookie';
    public $time = 0;

    public function __construct() {

        parent::__construct();

        $this->track = (int) $this->settings->get('firesale_mp_recent', '20');
        $this->time = ( time() + ( 7 * 24 * 60 * 60 ));
    }

    public function get_current() {

        // Get the product data
        if ($this->type == 'cookie') {
            $current = $this->input->cookie($this->config->item('cookie_prefix') . 'recently_viewed', '');
        } else {
            $current = $this->session->userdata(SITE_REF . '_recently_viewed', '');
        }

        if ($current) {
            // Format the data
            return explode('|', base64_decode($current));
        }

        return false;
    }

    public function save_current($current) {

        // Variables
        $data = base64_encode(implode('|', $current));

        // Set it back into cookie/session
        if ($this->type == 'cookie') {
            $this->input->set_cookie('recently_viewed', $data, $this->time);
        } else {
            $this->session->set_userdata(SITE_REF . '_recently_viewed', $data);
        }
    }

    public function also_bought($id, $limit = 20) {

        // Format values
        $id = ( 0 + $id );
        $limit = ( 0 + $limit );

        // Build query

        $sql = "SELECT i.`product_id`, i.`order_id`, SUM(i.`qty`) AS `count`
                FROM `" . SITE_REF . "_firesale_orders_items` AS i
                INNER JOIN `" . SITE_REF . "_firesale_products` AS p ON i.`product_id` = p.`id`
                WHERE ( SELECT `product_id` FROM `" . SITE_REF . "_firesale_orders_items` WHERE `order_id` = `order_id` AND `product_id` = {$id} LIMIT 1 ) > 0
                AND p.`id` != {$id}
                GROUP BY p.`slug`
                ORDER BY `count` DESC
                LIMIT {$limit}";

        // Run query
        $result = $this->db->query($sql);

        // Check for results
        if ($result->num_rows()) {
            // Retrieve results
            $results = $result->result_array();
            return $results;
        }

        // Nothing?
        return FALSE;
    }

    public function related_products($product, $use_cat = FALSE, $limit = 10) {

        // Get results
        if ($products = $this->_perform_search($product, $use_cat, $limit)) {

            // Get products
            foreach ($products AS $key => $product) {
                $products[$key] = $this->pyrocache->model('products_m', 'get_product', array($product['id']), $this->firesale->cache_time);
            }

            // Return
            return $products;
        }

        // Nothing?
        return FALSE;
    }

    protected function _perform_search($product, $use_cat = FALSE, $limit = 10) {

        // Variables
        $search = $this->db->escape(trim(strip_tags($product['description'])));

        // Build query
        $query = $this->db->select('p.id')
                //->select("MATCH(`title`, `description`) AGAINST({$search}) AS `weight`", FALSE)
                ->from('firesale_products AS p')
                ->join('default_firesale_products_firesale_categories AS c', 'c.row_id = p.id', 'inner')
                ->where('p.id != ' . $product['id'])
                ->where('status', '1')
                //->having('weight > ', '0')
                // ->where('price >= ' . $product['price'] * 0.95)
                // ->where('price <= ' . $product['price'] * 2.00)
                //->order_by('weight', 'DESC')
                ->group_by('p.id, p.slug')
                ->limit($limit);

        // Ignore variations
        if (!(bool) $this->settings->get('firesale_show_variations')) {
            $query->where('is_variation', '0');
        }

        // Add categegory to search terms?
        // - more accurate but greatly reduces results
        if ($use_cat) {

            // Build array of categories
            $cats = array();
            foreach ($product['category'] AS $category) {
                if (!in_array($category['id'], $cats)) {
                    $cats[] = $category['id'];
                }
            }

            // Add to query
            $query->where_in('c.firesale_categories_id', $cats);
        }

        // Run query
        $results = $query->get();

        // Check for results
        if ($results->num_rows()) {
            $results = $results->result_array();
            return $results;
        }

        // Nothing?
        return FALSE;
    }

}

<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
* This file is part of FireSale, a PHP based eCommerce system built for
* PyroCMS.
*
* Copyright (c) 2013 Moltin Ltd.
* http://github.com/firesale/firesale
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @package firesale/brands
* @author FireSale <support@getfiresale.org>
* @copyright 2013 Moltin Ltd.
* @version master
* @link http://github.com/firesale/firesale
*
*/

    // Sections
    $lang['firesale:sections:brands'] = 'Marcas';

    // Shortcuts
    $lang['firesale:shortcuts:brand_create'] = 'Nueva Marca';

    // Brands
    $lang['firesale:brands:title']          = 'Marca';
    $lang['firesale:brands:new']            = 'Agregar a nueva marca';
    $lang['firesale:brands:add_success']    = 'Nueva marca agregada correctamente';
    $lang['firesale:brands:add_error']      = 'Error agregando la marca';
    $lang['firesale:brands:edit']           = 'Editar Marca';
    $lang['firesale:brands:edit_success']   = 'Marca editada correctamente';
    $lang['firesale:brands:edit_error']     = 'Error editando la marca';
    $lang['firesale:brands:not_found']      = 'La marca seleccionada no ha sido encontrada';
    $lang['firesale:brands:none']           = 'No hay marcas';
    $lang['firesale:brands:delete_success'] = 'Marca removida correctamente';
    $lang['firesale:brands:delete_error']   = 'error removiendo marca';

    // Labels
    $lang['firesale:label_brand'] = 'Marca';

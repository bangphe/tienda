<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends Public_Controller
{

    public function __construct()
    {
        parent::__construct();

        // Load required items
        $this->lang->load('firesale');
        $this->load->model('wishlists_m');
        $this->load->model('firesale/routes_m');
        $this->load->helper('firesale/general');

        // Ensure the user is logged in
        if ( ! isset($this->current_user->id) ) {
        	$this->session->set_flashdata('error', 'You must be logged in to view this page');
        	redirect('users/login');
        }

        // Add initial items
        $this->data    = new stdClass();
        $this->stream  = $this->streams->streams->get_stream('firesale_wishlists', 'firesale_wishlists');
        $this->user_id = $this->current_user->id;
        $this->base    = $this->pyrocache->model('routes_m', 'build_url', array('wishlist'), $this->firesale->cache_time);

        // Check ownership if a list ID is available
        /* Se comenta por que para colocar las listas publicas no se deben de sacar de la vista
		 * if ( is_numeric($this->uri->rsegment(3)) and $this->uri->rsegment(3) > 0 ) {
            if ( ! $this->pyrocache->model('wishlists_m', 'owner', array($this->uri->rsegment(3), $this->user_id), $this->firesale->cache_time) ) {
                show_404();
                return;
            }
        }*/
    }

	public function index()
	{


        // Assign variables
        $this->data->lists = $this->pyrocache->model('wishlists_m', 'user_lists', array($this->user_id,), $this->firesale->cache_time);
		
		$this->data->public_list = $this->pyrocache->model('wishlists_m', 'user_lists', array($this->user_id, null, 0), $this->firesale->cache_time);
		
        // Build page
        $this->template->title(lang('firesale:wish:title'))
                       ->set_breadcrumb(lang('firesale:wish:title'))
        			   ->build('index', $this->data);
	}

	public function view($list)
	{
        if ($list) {
        // Assign variables
        $this->data->list = $this->pyrocache->model('wishlists_m', 'get_list', array($list), $this->firesale->cache_time);

        // Check list
        if ( ! $this->data->list ) {
            show_404();
            return;
        }

        // echo '<pre>';
        // var_dump($this->data);die;

        // Build page
        $this->template->title($this->data->list['title'])
                       ->set_breadcrumb(lang('firesale:wish:title'), $this->base)
                       ->set_breadcrumb($this->data->list['title'])
        			   ->build('view', $this->data);
        } else {
            redirect('404');
        }
	}

    public function create()
    {
        $this->_form();
    }

    public function edit($list)
    {
        if ($list) {
            $row = $this->streams->entries->get_entry($list, $this->stream->stream_namespace, $this->stream->stream_slug);
            $this->_form($row);
        } else {
            redirect('404');
        }
    }

    public function delete($list)
    {
        // Delete
        if ( ! $this->wishlists_m->list_delete($list) ) {
            $this->session->set_flashdata('error', lang('firesale:wish:delete:error'));
        } else {
            $this->pyrocache->delete_all('wishlists_m');
        	$this->session->set_flashdata('success', lang('firesale:wish:delete:success'));
        }

        // Send back
        redirect($this->input->server('HTTP_REFERER'));
    }

	public function insert($list, $item, $quantity = 1)
	{
        // Create default list
        if ( $list <= 0 ) {
            $list = $this->wishlists_m->create(lang('firesale:wish:default:name'), lang('firesale:wish:default:desc'), $this->user_id);
        }

        // Check quantity
        if ( $quantity <= 0 ) {
            $this->remove($list, $item);
        }

        // Add to list
        if ( ! $this->wishlists_m->list_item_add($list, $item, $quantity) ) {
            $this->session->set_flashdata('error', lang('firesale:wish:item:add:error'));
        } else {
            $this->pyrocache->delete_all('wishlists_m');
        	$this->session->set_flashdata('success', lang('firesale:wish:item:add:success'));
        }

        // Send back
        redirect($this->input->server('HTTP_REFERER'));
	}

    public function cart($list)
    {
        // Create default list
        if ( $list <= 0 ) {
            $list = $this->wishlists_m->create(lang('firesale:wish:default:name'), lang('firesale:wish:default:desc'), $this->user_id);
        }

        // Load required items
        $this->load->library('firesale/fs_cart');

        // Variables
        $contents = $this->fs_cart->contents();
        
        // Check contents
        if ( ! empty($contents) ) {

            // Loop and add items
            foreach ( $contents as $item ) {
                $this->wishlists_m->list_item_add($list, $item['id'], $item['qty']);
            }

            // Clear cache
            $this->pyrocache->delete_all('wishlists_m');

            // Set flashdata
            $this->session->set_flashdata('success', lang('firesale:wish:item:add:success'));
        } else {
            $this->session->set_flashdata('error', lang('firesale:wish:item:add:error'));
        }

        // Send back
        redirect($this->input->server('HTTP_REFERER'));
    }

	public function remove($list, $item)
	{
        // Add to list
        if ( ! $this->wishlists_m->list_item_remove($list, $item) ) {
            $this->session->set_flashdata('error', lang('firesale:wish:item:delete:error'));
        } else {
            $this->pyrocache->delete_all('wishlists_m');
        	$this->session->set_flashdata('success', lang('firesale:wish:item:delete:success'));
        }

        // Send back
        redirect($this->input->server('HTTP_REFERER'));
	}

	public function _form($row = false)
	{
    	// Variables
        $stream = $this->streams->streams->get_stream($this->stream->stream_namespace, $this->stream->stream_slug);
        $skip  = array('btnAction');
        $extra = array(
            'return'          => false,
            'success_message' => lang('firesale:wish:'.($row ? 'edit' : 'add').':success'),
            'failure_message' => lang('firesale:wish:'.($row ? 'edit' : 'add').':error'),
            'title'           => lang('firesale:wish:'.($row ? 'edit' : 'add'))
        );

        // Get fields
        $fields = $this->fields->build_form($stream, $row ? 'edit' : 'new', $row ? $row : $this->input->post(), false, false, $skip, $extra);

        // Check for posted
        if ( ! is_array($fields)) {

            // Success, clear all the cache!
            Events::trigger('clear_cache');

            // Redirect
            if ( $this->input->post('btnAction') == 'save_exit' ) {
                redirect('users/wishlist');
            } else {
                redirect('users/wishlist/edit/'.$fields);
            }
        }

        // Pass some data to the view
        $this->data->type   = $row ? 'edit' : 'add';
        $this->data->fields = $fields;

        // Build page
        $this->template->title(lang('firesale:wish:'.$this->data->type))
                       ->set_breadcrumb(lang('firesale:wish:title'), $this->base)
                       ->set_breadcrumb(lang('firesale:wish:'.$this->data->type))
        			   ->build('form', $this->data);
	}

}
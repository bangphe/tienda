  <section class="cont_home">   
      <!--Titulo-->
      <div class="tittles marco">
        <h1>Confirmar Pedido</h1>
      </div>
      <!--Fin Titulo-->

      <div class="clear"></div>

      <!--Pasos Compra-->
      <section class="bg_pasos_comp marco">
        <div class="elem_blancos">
         <form action="" method="get">
           <article class="col1_p large-6 medium-6 small-12 columns relative item1">
              <h3>DETALLES DE FACTURACIÓN</h3>    
                  <ul class="datos_user_comp">
                    <li><p>{{ bill_to.firstname }} {{ bill_to.lastname }}</p></li>
                    {{ if bill_to.cedule }}<li><p>{{ bill_to.cedule }}</p></li>{{ endif }}
                    {{ if bill_to.email }}<li><p>{{ bill_to.email }}</p></li>{{ endif }}
                    {{ if bill_to.phone }}<li><p>{{ bill_to.phone }}</p></li>{{ endif }}
                    {{ if bill_to.address1 }}<li><p>{{ bill_to.address1 }}</p></li>{{ endif }}
                    {{ if bill_to.deparment_name }}<li><p>{{ bill_to.deparment_name }}</p></li>{{ endif }}
                    {{ if bill_to.city_name }}<li><p>{{ bill_to.city_name }}</p></li>{{ endif }}
					{{ if bill_to.observation }}<li><p>{{ bill_to.observation }}</p></li>{{ endif }}
                  </ul>     
           </article>

           <article class="col2_p large-6 medium-6 small-12 columns relative item2">
              <h3>DATOS DEL ENVÍO</h3>
                  <ul class="datos_user_comp">
                    <li><p>{{ ship_to.firstname }} {{ ship_to.lastname }}</p></li>
                    {{ if ship_to.cedule }}<li><p>{{ ship_to.cedule }}</p></li>{{ endif }}
                    {{ if ship_to.email }}<li><p>{{ ship_to.email }}</p></li>{{ endif }}
                    {{ if ship_to.phone }}<li><p>{{ ship_to.phone }}</p></li>{{ endif }}
                    {{ if ship_to.address1 }}<li><p>{{ ship_to.address1 }}</p></li>{{ endif }}
                    {{ if ship_to.deparment_name }}<li><p>{{ ship_to.deparment_name }}</p></li>{{ endif }}
                    {{ if ship_to.city_name }}<li><p>{{ ship_to.city_name }}</p></li>{{ endif }}
					{{ if ship_to.observation }}<li><p>{{ ship_to.observation }}</p></li>{{ endif }}
                  </ul> 
           </article>

             <div class="clear"></div>
         </form>
        </div> 
      </section>  
      <!--Pasos Compra-->
        
        <div class="clear"></div>

      <!--bg Carrito-->      
      <section class="bg_carrito">
          <div class="tittles marco">
            <h1>REVISIÓN DEL CARRITO ({{ firesale:cart }}{{ count }}{{ /firesale:cart }})</h1>
          </div>
          <!--Contenidos Sitio-->
          <section class="cont_produt marco">
            <!--Lista Carrito-->
            <article class="large-8 medium-8 small-12 columns ">
              <div class="caja_borde">
                 <ul class="list_carrito">
                  {{ items }}
                    <li>
                      <!--<a href="{{ firesale:url route='cart' }}/remove/{{ rowid }}" class="delete_prd" type="">x</a>-->
                       <div class="large-2 medium-2 small-4 columns">
                          <!-- <ul class="colores">                
                             <a href="javascript:void(0)" class="cafe"></a>
                           </ul>  
                            -->
                         {{ if image }}
                          <a href="{{ if parent }}{{ firesale:url route='product' id=parent }}{{ else }}{{ firesale:url route='product' id=id }}{{ endif }}" title="View {{ name }}"><img src="{{ url:site }}files/large/{{ image }}" alt=" {{ name }}"/></a>
                          {{ else }}
                          <div class="cart-left">{{ theme:image file="no-image.png" alt=title }}</div>
                          {{ endif }}
                       </div>
                       <div class="detalle_prod_carrito large-10 medium-10 small-8 columns">
                           <div class="nom_prod large-6 medium-6 small-12 columns">
                              <h2>{{ name }}</h2>
                              <br>
                              <!--<h4>Ref. 3309/137   <!--<span class="dis">Artículo disponible</span></h4>-->
                              <!--Productos no disponibles <h4>Ref. 3309/137   <span class="no_dis">Artículo disponible</span></h4> -->
                           </div>
                           <div class="nom_prod2 large-6 medium-6 small-12 columns">
                              <h1>{{ price }}</h1>
                              <h3>Subtotal {{ total }}</h3>
							  <h3>Cantidad {{ qty }}</h3>
                              <!--Productos no disponibles <h4>Ref. 3309/137   <span class="no_dis">Artículo disponible</span></h4> -->
                           </div>
                       </div>
                        <div class="clear"></div>
                    </li>
                     {{ /items }}
                 </ul>
                   <div class="clear"></div>
              </div>
            </article>
            <!--Fin Lista Carrito-->
            <!--Lista de pago-->
            <article class="large-4 medium-4 small-12 columns padding_l">
              <div class="caja_borde">
                 <ul class="tabla_pagos">
                   <li>
                     <div class="large-8 medium-8 small-12 columns">
                          Cantidad de Artículos
                     </div>
                     <div class="large-4 medium-4 small-12 columns valor">
                          {{ firesale:cart }}
                            {{ count }}
                          {{ /firesale:cart }}
                     </div>
                   </li>
                   <li>
                     <div class="large-8 medium-8 small-12 columns">
                          Envío estimado
                     </div>
                     <div class="large-4 medium-4 small-12 columns valor">
                          {{ price_ship }}<span>*</span>
                     </div>
                   </li>
                   <div class="valor_comp">
                      <div class="large-6 medium-6 small-12 columns">TOTAL</div>
                      <div class="large-6 medium-6 small-12 columns valor_t"> {{ price_total }}</div> 
                   </div>
                     <div class="clear"></div>
					 <div class="titulo_del_pag">
                     DETALLES DEL PAGO
					 
                   </div>  
                      <div class="clear"></div>                       
					           {{ payment }}
                   <!--<div class="term_compra2">
                     <p>
                        Se enviarán correos de notificación y el administrador únicamente recibirá la solicitud. Posteriormente se pondrá en contacto contigo.
                     </p>                  
                   </div> 
				   <a href="carrito_paso4.php" class="btn_gris">ENVIAR SOLICITUD</a>-->
				   <div class="clear"></div>  
                 </ul>
              </div>
            </article>
            <!--Fin Lista de pago--> 
             <div class="clear"></div>    
            <!--Codigos Descuentos-->
            <article class="large-12 medium-12 small-12 columns">
              <div class="codigo_dest">
                  <div class="large-2 medium-2 small-12 columns right">
                      <!--<button type="submit" name="btnAction" value="pay" class="btn_blanco" >CONTINUAR</button>-->
                  </div>
              </div>
            </article>
            <!--Fin Codigos Descuentos-->  
               <div class="clear"></div>
          </section>
          <!--Fin Contenidos Sitio-->      
      </section>
      <!--Fin bg Carrito-->      
  </section>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Widget_Phone_Header extends Widgets
{
    // The widget title,  this is displayed in the admin interface
    public $title = array(
        'en' => 'Phone number header',
        'es' => 'Telefono del encabezado (Widget)'
    );
    public $description = array(
        'en' => 'Phone number header',
        'es' => 'Telefono del encabezado'
    );
 
    // The author's name
    public $author = 'Luis Fernando Salazar Buitrago';
 
    // The authors website for the widget
    public $website = 'www.imaginamos.com';
 
    //current version of your widget
    public $version = '1.0';
	
    public function run()
    {
    	$data = $this->db->get($this->db->select('phone')->dbprefix.'contact_us')->row();
    	return array('data' => $data->phone);
    }
}

    <?php echo form_open_multipart(site_url('admin/firesale_wishlist/insert_items/'.$list_id), 'class="crud form_inputs" id="item_add"'); ?>

        <fieldset>
            <ul>
                <li class="<?php echo alternator('even', ''); ?>">
                    <label for="product">
                        <?php echo lang('firesale:wish:label:product'); ?>
                    </label>
                    <div class="input"><?php echo form_dropdown('product', $list, 'id="product"'); ?></div>
                </li>
                <li class="<?php echo alternator('even', ''); ?>">
                    <label for="quantity">
                        <?php echo lang('firesale:wish:label:quantity'); ?>
                    </label>
                    <div class="input">
    					<div class="increment">
    						<!--<a href="#" class="btn blue minus">-</a>-->
    						<input type="text" name="quantity" id="quantity" value="1" />
    						<!--<a href="#" class="btn blue plus">+</a>-->
    					</div>
                    </div>
                </li>
            </ul>
        </fieldset>

        <div class="buttons">
            <button type="submit" name="btnAction" value="save" class="btn blue"><span><?php echo lang('save_label'); ?></span></button>
            <a href="#" class="btn gray cancel"><?php echo lang('cancel_label'); ?></a>
        </div>

        <div class="modal_spacer"></div>

    <?php echo form_close(); ?>

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * CI-Merchant Library
 *
 * Copyright (c) 2011-2012 Adrian Macneil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Merchant PayU Class
 *
 * Payment processing using PayU.in
 *
 * @author Daksh H. Mehta ( @dakshhmehta )
 * @author Brayan Acebo
 */
class Merchant_payu extends Merchant_Driver {

    private $hash_data = array();
    private $hash_sequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
    protected $settings = array();

    public function __construct() {
        parent::__construct();
        $this->settings = $this->settings();
    }

    public function default_settings() {
        return array(
            'merchantId' => '500238',
            'ApiKey' => '6u39nqhq8ftd0hlvnjfs66eh8c',
            'test' => true,
            'currency' => 'COP',
            'accountId' => 500537
        );
    }

    public function purchase() {
		
        $merchantId = $this->settings['merchantId'];
        $ApiKey = $this->settings['ApiKey'];
        $referenceCode = date('Y-m-d H:i:s');
        $amount = $this->param('amount');
        $currency = $this->settings['currency'];

        $firmacadena = "$ApiKey~$merchantId~$referenceCode~$amount~$currency";
        $signature = md5($firmacadena);

        $test = ($this->settings['test'] == 1) ? 1 : 0;

        $this->hash_data = array(
            'merchantId' => $merchantId,
            'ApiKey' => $ApiKey,
            'accountId' => $this->settings['accountId'],
            'referenceCode' => $referenceCode,
            //'amount' => $this->param('amount'),
			'amount' => $amount,
            'currency' => $currency,
            'tax' => 0,
            'taxReturnBase' => 0,
            'description' => $this->param('description'),
            'buyerEmail' => $this->param('email'),
            'test' => $test,
            'signature' => $signature,
            'responseUrl' => site_url('cart/register_payu')
        );

        $url = ($test == 1) ? $this->_url_test() : $this->_url_real();
		
        $this->post_redirect($url, $this->hash_data);
    }

    protected function _hash() {
        $hash_string = '';
        $hashSeq = explode('|', $this->hash_sequence);
        foreach ($hashSeq as $hash_var) {
            $hash_string .= isset($this->hash_data[$hash_var]) ? $this->hash_data[$hash_var] : '';
            $hash_string .= '|';
        }
        $hash_string .= $this->settings['salt'];
        $hash = strtolower(hash('sha512', $hash_string));
        return $hash;
    }

    public function purchase_return() {
        $payu_response = $this->CI->input->post(null, true);
        if ($payu_response['status'] == 'success') {
            // You can perform reverse hash check for more security.
            $response = new Merchant_payu_response(Merchant_response::COMPLETE);
        } else {
            $response = new Merchant_payu_response(Merchant_response::FAILED);
        }
        return $response;
    }

    protected function _url_test() {
        return 'https://stg.gateway.payulatam.com/ppp-web-gateway';
    }

    protected function _url_real() {
        return 'https://gateway.payulatam.com/ppp-web-gateway/';
    }

}

class Merchant_payu_response extends Merchant_response {

    protected $_response;

    public function __construct($response) {
        $ci = & get_instance();
        $this->_response = $response;
        $this->_status = $response;
        $this->_reference = (string) $ci->input->post('txnid');
    }

}

/* End of file ./libraries/merchant/drivers/merchant_payu.php */

<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Events_Firesale_digital
{
	protected $ci;
	
	public function __construct()
	{

		$this->ci =& get_instance();
		
		// register the events
		Events::register('form_build', array($this, 'form_build'));
		Events::register('page_build', array($this, 'page_build'));	
		Events::register('order_complete', array($this, 'order_complete'));	
		Events::register('clear_cache', array($this, 'clear_cache'));	
	}

    public function form_build($controller)
    {
    	if ($this->ci->uri->segment(3) != 'products') return;

		// Remove images (needs to be last)
	    unset($controller->tabs['_images']);

	    // Add metadata to tabs
	    $controller->tabs['digital'] = array('digital', 'download', 'downloads', 'expires_after', 'expires_interval');

	    // Add images back in
	    $controller->tabs['_images'] = array();
    }

	public function page_build($template)
	{
		if ($this->ci->uri->segment(3) != 'products') return;
		
		// Find path
        if(file_exists(SHARED_ADDONPATH . 'modules/firesale_digital/details.php')) {
            $dir = BASE_URL . 'addons/shared_addons/modules/';
        } elseif (file_exists('addons/' . SITE_REF . '/modules/firesale_digital/details.php')) {
            $dir = BASE_URL . 'addons/' . SITE_REF . '/modules/';
        } else {
            $dir = (defined('PYROPATH') ? PYROPATH : APPPATH) . 'modules/';
        }

		// Add asset path
		Asset::add_path('firesale_digital', $dir . 'firesale_digital/');

		// Append metadata
		$template->append_css('firesale_digital::digital.css');
	}

	public function order_complete($order)
	{
		$this->ci->load->model('firesale/orders_m');
		
		$non_digital = 0;

		foreach ($order['items'] as $item)
		{
			if ($item['digital'] == 'n') $non_digital++;
		}

		if ( ! $non_digital) $this->ci->orders_m->update_status($order['id'], 3);
	}

	public function clear_cache()
    {
        $this->ci->pyrocache->delete_all('digital_m');
    }

}

</div>
{{ if banner }}
	<div id="home-slide">
	    <div class='swipe-wrap'>
	    	{{ banner }}
		        <div class="slider-item" style="display: block;">
		            <img src="{{image}}" />
		        </div>
		    {{ /banner }}
	    </div>
	</div>
	
	<div class="home-slide-btn">
	    <div class="slide-btn">
	        <a onclick='mySwipeHome.prev()' class="btn-prev"></a> 
	        <a onclick='mySwipeHome.next()' class="btn-next"></a>
	    </div>
	</div>
{{ endif }}
            
<div class="container home-content">

    <div class="clearfix">

        <div class="quarter bkg-alpha box-home">
        	<div class="inner-box">
                <strong class="main-font box-title">Categorias</strong>
    
                <ul class="cat-list">
                    {{ firesale:categories order="ordering_count asc" parent="0" }}
                    {{ if children > 0 }}
                    <li>
                        <a href="{{ firesale:url route='category' id=id }}"><span>{{ title }}</span></a>
                        <ul>
                            {{ firesale:sub_categories order="ordering_count asc" parent=id }}
                            {{ if children > 0 }}
                            <li class="dropdown"><a href="{{ firesale:url route='category' id=id }}"><span>{{ title }} <b class="right-caret hide-phone"></b></span></a>
                                <ul>
                                    {{ firesale:sub_sub_categories order="ordering_count asc" parent=id }}
                                    	<li><a href="{{ firesale:url route='category' id=id }}">{{ title }}</a></li>
                                    {{ /firesale:sub_sub_categories }}
                                </ul>
                            </li>
                            {{ else }}
                            <li><a href="{{ firesale:url route='category' id=id }}">{{ title }}</a></li>
                            {{ endif }}
                            {{ /firesale:sub_categories }}
                        </ul>
                    </li>
                    {{ else }}
                    <li>
                        <a href="{{ firesale:url route='category' id=id }}">{{ title }}</a>
                    </li>
                    {{ endif }}
                    {{ /firesale:categories }}
                    <li><a href="#">Ver Más</a></li>
                </ul>
            </div>
        </div>

        <div class="half bkg-beta box-home" id="central-slide">
			<div class='swipe-wrap'>
				{{items}}
					<div class="slider-item" style="display: block;">
	                    <div class="half">
	                    	<div class="inner-box">
	                            <a href="product/{{ slug  }}">
	                                <strong class="main-font box-title">{{ titulo }}</strong>
	                            </a>
	                            <p>{{ descripcion }}</p>
	                            <div class="price main-font">${{ precio }}</div>
	                        </div>
	                        <div class="arrow-right bkg-beta"> </div>
	                    </div>
	                    <a href="product/{{ slug  }}">
	                        <div class="half bkg-delta">
	                            <img src="{{ imagen  }}" width="160">
	                        </div>
	                    </a>
	                    <div class="clear"></div>
	                </div>
				{{/items}}
            </div>
            <div class="slide-btn">
                <a onclick='mySwipe.prev()' class="btn-prev"></a> 
                <a onclick='mySwipe.next()' class="btn-next"></a>
            </div>
        </div>
        
       
                
        <div class="quarter bkg-beta box-home ads-slide">
        	<div class="inner-box">
                <strong class="main-font box-title">Publicidad</strong>
            </div>
            {{pautas}}
            	<a href="{{link}}" target="_blank"><img src="{{imagen}}"></a>
            {{/pautas}}
        </div>
    </div>

    <div class="clearfix featured-prods">
        {{ firesale:products limit="4" order="featured desc" }}
        {{ if entries }}
        {{ entries }}
        <div class="quarter prod-item">
            <div class="prod-box">
				<div class="prod-actions">
                    <div class="actions-btn">
                        <?php if (module_enabled('firesale_wishlist')): ?>
		                    {{ if user:logged_in }} 
		                    	<a class="add_to_wishlist whish-ico link wish_modal" data-item-id="{{ id }}" data-toggle="modal" data-target="#wishlistModal" data-placement="top" data-original-title="Agregar a la whishlist"></a>
		                    {{ endif }}
	                    <?php endif; ?>
                        <a class="fb-ico facebook_popup link" data-placement="top" data-original-title="Share this on Facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer.php?s=100&p[title]={{ title }}&p[url]={{ firesale:url route='product' id=id }}{{ if images.1.id }}&p[images][0]={{ url:site uri='files/large' }}/{{ images.0.id }}{{ endif }}"></a>
                        <a class="tw-ico twitter_popup link" data-placement="top" data-original-title="Share this on Twitter" href="http://twitter.com/share?url={{ firesale:url route='product' id=id }}&text={{ title }}%20is%20awesome!{{ if {theme:options:twitter_account} }}&via={{ theme:options:twitter_account }}{{ endif }}"></a>
                    </div>
                </div>
                <a class="item-image" href="{{ firesale:url route='product' id=id }}">
                    {{ if images }}
                    {{ if images.1.id }}
                    <img class="first" src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                    <img class="rollover" src="{{ url:site uri='files/thumb' }}/{{ images.1.id }}/360" alt="{{ title }} alternate" />
                    {{ else }}
                    <img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                    {{ endif }}
                    {{ else }}
                    {{ theme:image file="no-image.png" alt=title }}
                    {{ endif }}
                    
                </a>
               
				<div class="inner-box">
                	<a href="{{ firesale:url route='product' id=id }}">
                    	<span class="product-title">{{ title }}</span>
                    </a>
                    <div class="price main-font">{{ price_formatted }}</div>
                    {{ if modifiers }}
                    <a class="btn btn-alpha" href="{{ firesale:url route='product' id=id }}"><span>VER</span></a>
                    <!--<a class="btn btn-alpha" href="{{ firesale:url route='cart' after='/insert/' }}{{ modifiers.1.variations.1.product.id }}"><span>AGREGAR AL CARRITO</span></a>-->
                    {{ else }}
                    <a class="btn btn-alpha" href="{{ firesale:url route='cart' after='/insert/' }}{{ id }}"><span>AGREGAR AL CARRITO</span></a>
                    {{ endif }}  
                </div>

            </div>
        </div>
        {{ /entries }}
        {{ endif }}
        {{ /firesale:products }}
    </div>
</div>
<!-- Wishlist modal 
<?php /*nif (module_enabled('firesale_wishlist')): ?>
    {{ if user:logged_in }} 
    {{ theme:partial name="wishlist_modal" }}
    {{ endif }}
<?php endif; */?>-->
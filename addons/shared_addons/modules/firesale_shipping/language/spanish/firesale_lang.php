<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
* This file is part of FireSale, a PHP based eCommerce system built for
* PyroCMS.
*
* Copyright (c) 2013 Moltin Ltd.
* http://github.com/firesale/firesale
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @package firesale/shipping
* @author FireSale <support@getfiresale.org>
* @copyright 2013 Moltin Ltd.
* @version master
* @link http://github.com/firesale/firesale
*
*/

    // General
    $lang['firesale:shortcuts:band_create'] = 'Crear nuevo transporte';

    // Sections
    $lang['firesale:sections:shipping'] = 'Envio';
    $lang['firesale:shipping:title'] 	= 'Opciones de envio';
    $lang['firesale:shipping:create'] 	= 'Crear nuevo transporte de envio';
    $lang['firesale:shipping:edit'] 	= 'Editar transporte de envio';
    $lang['firesale:shipping:none']		= 'No se encontraron opciones de envio';

    // Labels
    $lang['firesale:label_free']           = 'Gratis';
	$lang['firesale:type_packaging']       = 'Tipo de Empaque';
    $lang['firesale:label_save']           = 'Guardar';
    $lang['firesale:label_courier']        = 'portador del envío';
    $lang['firesale:label_price_min']      = 'Precio (Minimo)';
    $lang['firesale:label_price_max']      = 'Precio (Máximo)';
    $lang['firesale:label_price_min_max']  = 'Precio (Min a Max)';
    $lang['firesale:label_weight_min_max'] = 'Peso (Min a Max)';
    $lang['firesale:label_weight_min']     = 'Peso (Minimo)';
    $lang['firesale:label_weight_max']     = 'Peso (Máximo)';
    $lang['firesale:label_weight_kg']      = 'Peso del producto (kg)';
    $lang['firesale:label_height_cm']      = 'Alto del producto (cm)';
    $lang['firesale:label_width_cm']       = 'Ancho del producto (cm)';
    $lang['firesale:label_depth_cm']       = 'Profundidad del producto (cm)';
    $lang['firesale:label_up_to']          = 'Subir a';

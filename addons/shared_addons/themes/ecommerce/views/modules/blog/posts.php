
    <div class="content blog">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                {{ if posts }}
                    <h1 class="title hide">Blog</h1>
                    <div class="posts">
                    
                    <div class="paging">
                        {{ pagination }}
                    </div>

                    {{ posts }}
                        <div class="entry">
                            <h2><a href="{{ url }}">{{ title }}</a></h2>
							<img src="{{image:thumb}}" />
                            <div class="metadata">
                               <i class="icon-calendar"></i> {{ helper:date format='j M y' timestamp=created_on }}
                               <i class="icon-user"></i> <a href='{{ url:site }}user/{{ created_by.user_id }}'>{{ created_by.display_name }}</a>
                               {{ if keywords }}<i class="icon-folder-open"></i>{{ keywords }} <a href="{{ url:site }}blog/tagged/{{ keyword }}">{{ keyword }}</a>{{ /keywords }}{{ endif }}
                            </div>
                            {{ if cover_image.image }}
                                <div class="thumb">
                                    <a href="{{ url }}"><img src="{{ cover_image.image }}" alt="{{ title }}" width="100%" /></a>
                                </div>
                            {{ endif }}
                            {{ intro }}
                            <div class="button"><a href="{{ url }}" class="btn btn-alpha"><span>Ver Más</span></a></div>
                        </div>
                    {{ /posts }}

                    <div class="paging">
                        {{ pagination }}
                    </div>
                        <div class="clearfix"></div>
                              
                    </div>
                {{ else }}
                    <h1>NO POSTS!</h1>
                {{ endif }}
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="sidebar">
                        {{ theme:partial name="sidebar" }}
                    </div>                                                
                </div>

            </div>
        </div>
    </div>

                        <div id="comments" class="comments well">
                        <?php if ($comments): ?>
                            <div class="title"><h4><?php echo ( count($comments).' Comment'.( count($comments) != 1 ? 's' : '' )); ?></h4></div>
                            <ul class="comment-list">
                            <?php foreach ($comments as $item): ?>
                                <li class="comment">
                                    <a class="pull-left" href="{{ url:site }}user/<?php echo $item->user_id; ?>">
                                        <img src="<?php echo gravatar($item->user_email, 64, 'g', true); ?>" class="avatar" alt="Avatar" />
                                    </a>
                                    <div class="comment-author"><a href="{{ url:site }}user/<?php echo $item->user_id; ?>"><span><?php echo $item->display_name ?></span></a></div>
                                    <div class="cmeta">Commented on <?php echo format_date($item->created_on) ?></div>
                                    <?php if (Settings::get('comment_markdown') and $item->parsed) { echo $item->parsed; } else { echo '<p>'.nl2br($item->comment).'</p>'; } ?>
                                    <div class="clearfix"></div>
                                </li>
                            <?php endforeach ?>
                            </ul>
                        <?php else: ?>
                            <p><?php echo lang('comments:no_comments') ?></p>
                        <?php endif ?>
                        </div>

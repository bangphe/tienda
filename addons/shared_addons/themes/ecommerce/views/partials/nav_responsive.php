<nav class="menu_nav elem_negros">
  <div class="buscador">
        <form class="buscar" id="buscarForm">   
          <input type="text" name="busqueda" id="busqueda" placeholder="Buscar en tienda..." class="tex_busc" />  
          <a href="buscador.php" class="btn_gris"><i class="fa fa-search"></i></a>               
        </form>
  </div>
  <ul>
    <li><a href="index.php" class="animated flipInX" data-wow-delay="0.2s" data-hover="INICIO">INICIO</a></li>
    <li><a href="javascript:void(0)" class="animated flipInX" data-wow-delay="0.2s" data-hover="HOMBRES">HOMBRES <i class="fa fa-chevron-down"></i></a>
      <ul class="sub_nav_resp">
         <li><a href="javascript:void(0)" data-hover="Ropa">Ropa <i class="fa fa-chevron-down"></i></a>
            <ul class="subcateg">
               <a href="lista_productos.php">Pashminas, Sombreros y Más</a>
               <a href="lista_productos.php">Cinturones</a>
               <a href="lista_productos.php">Gafas de Sol</a>
               <a href="lista_productos.php">Bisutería</a>
               <a href="lista_productos.php">Llaveros, Charms y Más</a>
               <a href="lista_productos.php" class="all_categ">» Todos</a>
            </ul>
         </li>                    
         <li><a href="javascript:void(0)" data-hover="Marroquinería">Marroquinería</a></li>                    
         <li><a href="javascript:void(0)" data-hover="Accesorios">Accesorios</a></li>                    
         <li><a href="javascript:void(0)" data-hover="Calzado">Calzado</a></li>                    
         <li><a href="javascript:void(0)" data-hover="Relojes y Joyería">Relojes y Joyería</a></li>                    
         <li><a href="javascript:void(0)" data-hover="Outdoor">Outdoor</a></li>                    
         <li><a href="javascript:void(0)" data-hover="El Regalo Perfecto">El Regalo Perfecto</a></li>  
      </ul>
    </li>
    <li><a href="javascript:void(0)" class="animated flipInX" data-wow-delay="0.3s" data-hover="NIÑOS">NIÑOS <i class="fa fa-chevron-down"></i></a>
      <ul class="sub_nav_resp">
         <li><a href="javascript:void(0)" data-hover="Ropa">Ropa <i class="fa fa-chevron-down"></i></a>
            <ul class="subcateg">
               <a href="lista_productos.php">Pashminas, Sombreros y Más</a>
               <a href="lista_productos.php">Cinturones</a>
               <a href="lista_productos.php">Gafas de Sol</a>
               <a href="lista_productos.php">Bisutería</a>
               <a href="lista_productos.php">Llaveros, Charms y Más</a>
               <a href="lista_productos.php" class="all_categ">» Todos</a>
            </ul>
         </li>                    
         <li><a href="javascript:void(0)" data-hover="Marroquinería">Marroquinería</a></li>                    
         <li><a href="javascript:void(0)" data-hover="Accesorios">Accesorios</a></li>                    
         <li><a href="javascript:void(0)" data-hover="Calzado">Calzado</a></li>                    
         <li><a href="javascript:void(0)" data-hover="Relojes y Joyería">Relojes y Joyería <i class="fa fa-chevron-down"></i></a>
         <ul class="subcateg">
               <a href="lista_productos.php">Pashminas, Sombreros y Más</a>
               <a href="lista_productos.php">Cinturones</a>
               <a href="lista_productos.php">Gafas de Sol</a>
               <a href="lista_productos.php">Bisutería</a>
               <a href="lista_productos.php">Llaveros, Charms y Más</a>
               <a href="lista_productos.php" class="all_categ">» Todos</a>
            </ul>
         </li>                    
         <li><a href="javascript:void(0)" data-hover="Outdoor">Outdoor <i class="fa fa-chevron-down"></i></a>
         <ul class="subcateg">
               <a href="lista_productos.php">Pashminas, Sombreros y Más</a>
               <a href="lista_productos.php">Cinturones</a>
               <a href="lista_productos.php">Gafas de Sol</a>
               <a href="lista_productos.php">Bisutería</a>
               <a href="lista_productos.php">Llaveros, Charms y Más</a>
               <a href="lista_productos.php" class="all_categ">» Todos</a>
            </ul>
         </li>                    
         <li><a href="javascript:void(0)" data-hover="El Regalo Perfecto">El Regalo Perfecto <i class="fa fa-chevron-down"></i></a>
         <ul class="subcateg">
               <a href="lista_productos.php">Pashminas, Sombreros y Más</a>
               <a href="lista_productos.php">Cinturones</a>
               <a href="lista_productos.php">Gafas de Sol</a>
               <a href="lista_productos.php">Bisutería</a>
               <a href="lista_productos.php">Llaveros, Charms y Más</a>
               <a href="lista_productos.php" class="all_categ">» Todos</a>
            </ul>
         </li> 
      </ul>
    </li>
    <li><a href="javascript:void(0)" class="animated flipInX" data-wow-delay="0.5s" data-hover="UNISEX">UNISEX <i class="fa fa-chevron-down"></i></a>
      <ul class="sub_nav_resp">
         <li><a href="javascript:void(0)" data-hover="Ropa">Ropa <i class="fa fa-chevron-down"></i></a>
            <ul class="subcateg">
               <a href="lista_productos.php">Pashminas, Sombreros y Más</a>
               <a href="lista_productos.php">Cinturones</a>
               <a href="lista_productos.php">Gafas de Sol</a>
               <a href="lista_productos.php">Bisutería</a>
               <a href="lista_productos.php">Llaveros, Charms y Más</a>
               <a href="lista_productos.php" class="all_categ">» Todos</a>
            </ul>
         </li>                    
         <li><a href="javascript:void(0)" data-hover="Marroquinería">Marroquinería <i class="fa fa-chevron-down"></i></a>
         <ul class="subcateg">
               <a href="lista_productos.php">Pashminas, Sombreros y Más</a>
               <a href="lista_productos.php">Cinturones</a>
               <a href="lista_productos.php">Gafas de Sol</a>
               <a href="lista_productos.php">Bisutería</a>
               <a href="lista_productos.php">Llaveros, Charms y Más</a>
               <a href="lista_productos.php" class="all_categ">» Todos</a>
            </ul>
         </li>                   
         <li><a href="javascript:void(0)" data-hover="Accesorios">Accesorios <i class="fa fa-chevron-down"></i></a>
         <ul class="subcateg">
               <a href="lista_productos.php">Pashminas, Sombreros y Más</a>
               <a href="lista_productos.php">Cinturones</a>
               <a href="lista_productos.php">Gafas de Sol</a>
               <a href="lista_productos.php">Bisutería</a>
               <a href="lista_productos.php">Llaveros, Charms y Más</a>
               <a href="lista_productos.php" class="all_categ">» Todos</a>
            </ul>
         </li>                   
         <li><a href="javascript:void(0)" data-hover="Calzado">Calzado <i class="fa fa-chevron-down"></i></a>
         <ul class="subcateg">
               <a href="lista_productos.php">Pashminas, Sombreros y Más</a>
               <a href="lista_productos.php">Cinturones</a>
               <a href="lista_productos.php">Gafas de Sol</a>
               <a href="lista_productos.php">Bisutería</a>
               <a href="lista_productos.php">Llaveros, Charms y Más</a>
               <a href="lista_productos.php" class="all_categ">» Todos</a>
            </ul>
         </li>                    
         <li><a href="javascript:void(0)" data-hover="Relojes y Joyería">Relojes y Joyería</a></li>                    
         <li><a href="javascript:void(0)" data-hover="Outdoor">Outdoor</a></li>                    
         <li><a href="javascript:void(0)" data-hover="El Regalo Perfecto">El Regalo Perfecto</a></li>  
      </ul>
    </li>
    <li><a href="javascript:void(0)" class="animated flipInX" data-wow-delay="0.6s" data-hover="ACCESORIOS">ACCESORIOS</a></li>
    <li><a href="javascript:void(0)" class="animated flipInX" data-wow-delay="0.7s" data-hover="LO MÁS NUEVO">LO MÁS NUEVO</a></li>
  </ul>

  <!-- <div class="newsll elem_blancos">
    <h3>Suscríbete a nuestro</h3>
    <h1>NEWSLETTER</h1>
    <p>Recibe descuentos, promociones exclusivas y se el primero en descubrir nuestras nuevas ofertas.</p>
    <form method="get" class="modal_all">
       <input type="text" name="" value="Correo Electrónico">
       <a href="#modal_nwslletter" data-effect="mfp-move-horizontal" class="btn_gris block">SUSCRIBIRME</a>  
    </form>
  </div> -->
</nav> 
<section class="item">
    <div class="content">
    	<h2>Home/Banner</h2>
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-principal"><span><?php echo $titulo; ?></span></a></li>
            </ul>

            <div class="inline-form" id="page-principal">
                <?php echo form_open_multipart(site_url('admin/home/edit_banner_two/'.(isset($banner_two) ? $banner_two->id : '')), 'class="crud"'); ?>
                <fieldset>
                    <ul>
                        <li>
                            <label for="name">Imagen
                                <small>
                                    - Imagen Permitidas gif | jpg | png | jpeg<br>
                                    - Tamaño Máximo 2 MB<br>
                                    - Ancho  1366px<br>
                                    - Alto 547px
                                    - Todas las imagenes deben ser del mismo tamaño
                                </small>
                            </label>
                            <?php if (!empty($banner_two->image)): ?>
                                <div>
                                    <img src="<?php echo site_url($banner_two->image) ?>" width="298">
                                </div>
                            <?php endif; ?>
                            <div class="btn-false">
                                <div class="btn">Examinar</div>
                                <?php echo form_upload('image', set_value('image'), ' id="image"'); ?>
                            </div>
                        </li>
                        <li>
                            <label for="name">Titulo</label>
                            <div class="input"><?php echo form_input('title', (isset($banner_two->title)) ? $banner_two->title : set_value('title'), 'class="dev-input-title" style="width:100%"'); ?></div>
                        </li>
                        <li>
                            <label for="name">Texto</label>
                            <div class="input"><?php echo form_input('text', (isset($banner_two->text)) ? $banner_two->text : set_value('text'), 'class="dev-input-title" style="width:100%"'); ?></div>
                        </li>
                        <li>
                            <label for="name">Link</label>
                            <div class="input"><?php echo form_input('link', (isset($banner_two->link)) ? $banner_two->link : set_value('link'), 'class="dev-input-title" style="width:100%"'); ?></div>
                        </li>
                    </ul>
                    <?php
                    	$this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel')));
                    ?>
                </fieldset>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>
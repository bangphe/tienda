<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This file is part of FireSale, a PHP based eCommerce system built for
 * PyroCMS.
 *
 * Copyright (c) 2013 Moltin Ltd.
 * http://github.com/firesale/firesale
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package firesale/core
 * @author FireSale <support@getfiresale.org>
 * @copyright 2013 Moltin Ltd.
 * @version dev
 * @link http://github.com/firesale/firesale
 *
 */
class products extends Admin_Controller {

    public $stream = NULL;
    public $perpage = 30;
    public $section = 'products';
    public $tabs = array('description' => array('description'),
        '_modifiers' => array(),
        '_images' => array());

    public function __construct() {

        parent::__construct();

        // Add data object
        $this->data = new stdClass;

        // Button events
        Events::trigger('button_build', $this->template);

        // Load libraries, drivers & models
        $this->load->driver('Streams');
        $this->load->model(array(
            'routes_m',
            'products_m',
            'modifier_m',
            'categories_m',
            'taxes_m',
            'streams_core/row_m',
            'firesale_products_m',
            'firesale/to_excel_m',
        ));

        $this->load->library('streams_core/fields');
        $this->load->library('files/files');
        $this->load->helper('general');

        // Add metadata
        $this->template->append_css('module::admin/products.css')
                ->append_js('module::admin/jquery.tablesort.js')
                ->append_js('module::admin/jquery.metadata.js')
                ->append_js('module::admin/jquery.tablesort.plugins.js')
                ->append_js('module::admin/upload.js')
                ->append_js('module::admin/products.js')
                ->append_js('module::admin/modifiers.js')
                ->append_metadata('<script type="text/javascript">' .
                        "\n  var currency = '" . cache('currency_m/get_symbol') . "';" .
                        "\n  var tax_rate = '" . cache('taxes_m/get_percentage', 1, 1) . "';" .
                        "\n</script>");

        // Get the stream
        $this->stream = $this->streams->streams->get_stream('firesale_products', 'firesale_products');
    }

    public function index($start = 0) {

        // Check for btnAction
        if ($action = $this->input->post('btnAction')) {
            $this->$action();
        }

        // Get product IDs
        $products = cache('products_m/get_products', array(), $start, $this->perpage);

        // Build product data
        foreach ($products AS &$product) {
            $product = cache('products_m/get_product', $product['id']);
        }

        // Assign variables
        $this->data->products = $products;
        $this->data->count = cache('products_m/get_products', ( isset($filter) ? $filter : array()), 0, 0);
        $this->data->count = ( $this->data->count ? count($this->data->count) : 0 );
        $this->data->pagination = create_pagination('/admin/firesale/products/', $this->data->count, $this->perpage, 4);
        $this->data->categories = array('-1' => lang('firesale:label_filtersel')) + cache('categories_m/dropdown_values');
        $this->data->status = cache('products_m/status_dropdown', ( $type == 'status' ? $value : -1));
        $this->data->stock_status = cache('products_m/stock_status_dropdown', ( $type == 'stock_status' ? $value : -1));
        $this->data->min_max = cache('products_m/price_min_max');
        $this->data->buttons = ( $this->template->buttons ? $this->template->buttons : '' );

		
        // Add page data
        $this->template->title(lang('firesale:title') . ' ' . lang('firesale:sections:products'))
                ->set($this->data);

        // Fire events
        Events::trigger('page_build', $this->template);

        // Build page
        $this->template->build('admin/products/index');
    }

    public function create($id = NULL, $row = NULL, $modifier = null) {
        // Variables
        $input = FALSE;
        $skip = array();
        $extra = array();
        $id2 = $id;
        // Check for post data
        if (substr($this->input->post('btnAction'), 0, 4) == 'save') {
        	// para evitar un error de copiar y pegar de otras paginas en la creación
        	if($id == NULL)
			{
				$_POST['description'] = html_entity_decode($_POST['texto'], ENT_COMPAT, 'UTF-8');
			}
            // Variables
            $input = $this->input->post();
			// los slug los ponemos en minusculas
			$input['slug'] = strtolower($input['slug']);
			// quitamos los espacios en blanco
			$input['slug'] = str_replace(' ', '', $input['slug']);
			
            $skip = array('btnAction');
            $extra = array(
                'return' => FALSE,
                'success_message' => lang('firesale:prod_' . ( $id == NULL ? 'add' : 'edit' ) . '_success'),
                'error_message' => lang('firesale:prod_' . ( $id == NULL ? 'add' : 'edit' ) . '_error')
            );

            // Editing
            if ($id !== NULL) {

                // Just incase
                Events::trigger('pre_product_updated', $id);
                //$input = $this->input->post();
				
                // Elimina categorias actuales para evitar duplicados en la actualizacion
                $input['category'] = $_POST['category'] = $this->products_m->category_fix($id, $input['category']);

                // Esto permite que el wisywig incluya estilos
                $_data = array(
                    'description' => html_entity_decode($input['texto'], ENT_COMPAT, 'UTF-8')
                );

                $this->db->where('id', $id);
                $this->db->update('firesale_products', $_data);

            }
        }
		
		//var_dump($this->streams_m->get_stream_fields($this->stream->id));

        // Get the stream fields
        $fields = $this->fields->build_form($this->stream, ( $id == NULL ? 'new' : 'edit'), ( $id == NULL ? $input : $row), FALSE, FALSE, $skip, $extra);
        if(!is_array($fields))
            $this->categories_m->category_orden($fields,$input['category']);

        // Posted
        if (substr($this->input->post('btnAction'), 0, 4) == 'save')
        {
            // Got an ID back
            if (is_string($fields) OR is_integer($fields)) {

                // Esto permite que el wisywig incluya estilos
                $_data = array(
                    'description' => html_entity_decode($input['texto'], ENT_COMPAT, 'UTF-8')
                );

                $this->db->where('id', $id);
                $this->db->update('firesale_products', $_data);

                // Assign ID
                $id = $fields;


                // Update image folder?
                if (!empty($row)) {

                    // Update Folder Slug
                    if ($row->slug != $input['slug']) {
                        $this->products_m->update_folder_slug($row->slug, $input['slug']); 
                    }

                    // Update variation
                    if ($row->price != $input['price']) {
                        $this->modifier_m->edit_price($row, $input['price'], $input['rrp']); 
                    } 



                    // Fire event
                    $data = array_merge(array('id' => $id, 'stream' => 'firesale_products'), $input);
                    
                    Events::trigger('product_updated', $data);
                }

                // Everything went well, clear cache for front-end update
                Events::trigger('clear_cache');

                // Add to search
                $product = cache('products_m/get_product', $id);
                $this->products_m->search($product, true);

                // Redirect
                redirect('admin/firesale/products' . ( $input['btnAction'] != 'save_exit' ? '/edit/' . $id.'/'.$modifier : '' ));
            }
        }
        // Fire build event
        Events::trigger('form_build', $this);

        // Get edit variables
        if ($row) {

            // Add row
            $this->data = $row;

            // Get modifiers and variants
            $this->data->modifiers = cache('modifier_m/get_modifiers', $row->id);
            $this->data->variations = cache('modifier_m/get_products', $row->id);

            // Get images
            $folder = get_file_folder_by_slug($row->slug, 'product-images');
            $images = Files::folder_contents($folder->id);
            $this->data->images = $images['data']['file'];
        }

        // Assign variables
        $this->data->id = $id;
        $this->data->fields = fields_to_tabs($fields, $this->tabs);
        $this->data->tabs = array_keys($this->data->fields);
        $this->data->symbol = cache('currency_m/get_symbol');
		$this->data->is_modifier = (!empty($modifier) ? TRUE : FALSE);
		//var_dump($this->data->fields);
		
        $obj = $this->db->get_where('firesale_products', array('id' => $id))->row();

        $this->data->texto = $obj->description;

        // Add metadata
        $this->template->append_js('module::admin/jquery.filedrop.js')
                ->append_js('module::admin/upload.js')
                ->append_metadata($this->load->view('fragments/wysiwyg', NULL, TRUE));

        // Grab all the taxes
        $taxes = cache('taxes_m/taxes_for_currency', 1);

        $tax_string = '<script type="text/javascript">' .
                "\n var taxes = new Array();\n";

        foreach ($taxes as $tax)
            $tax_string .= "taxes[" . $tax->id . "] = " . $tax->value . ";\n";

        $tax_string .= '</script>';

        $this->template->append_metadata($tax_string);

        // Add page data
        $this->template->title(lang('firesale:title') . ' ' . lang('firesale:prod_title_' . ( $id == NULL ? 'create' : 'edit' )))
                ->set($this->data)
                ->enable_parser(true);
		
		/*
		echo "<pre> ";
		print_r($this->data);
		echo "</pre> ";
		return;
		*/
		
        // Fire events
        Events::trigger('page_build', $this->template);

        // Build page
        $this->template->build('admin/products/create');
    }

    public function edit($id, $modifier = null) {

        // Get row
        if ($row = cache('row_m/get_row', $id, $this->stream, false)) {
            // Load form
            $this->create($id, $row, $modifier);
        } else {
            $this->session->set_flashdata('error', lang('firesale:prod_not_found'));
            redirect('admin/firesale/products/create');
        }
    }

    public function delete($prod_id = null) {

        $delete = true;
        $products = $this->input->post('action_to');

        if ($this->input->post('btnAction') == 'delete' and !empty($products)) {

            for ($i = 0; $i < count($products); $i++) {

                if (!$this->products_m->delete_product($products[$i])) {
                    $delete = false;
                }

                // Remove from search
                if ($delete === true) {
                    $this->products_m->search(array('id' => $products[$i]));
                }
            }
        } elseif ($prod_id !== null) {

            if (!$this->products_m->delete_product($prod_id)) {
                $delete = false;
            }

            // Remove from search
            if ($delete === true) {
                $this->products_m->search(array('id' => $prod_id));
            }
        }

        if ($delete) {

            // Deleted, clear cache!
            Events::trigger('clear_cache');

            $this->session->set_flashdata('success', lang('firesale:prod_delete_success'));
        } else {
            $this->session->set_flashdata('error', lang('firesale:prod_delete_error'));
        }

        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function edit_modifier_ajax($idItem = null)
	{
		$this->form_validation->set_rules('price', 'PRECIO ACTUAL', 'required|numeric|max_length[15]|trim');
        $this->form_validation->set_rules('stock', 'NIVEL DE STOCK', 'required|numeric|max_length[11]|trim');
        $this->form_validation->set_rules('descuento', 'DESCUENTO', 'numeric|max_length[2]|trim');
		
		$statusJson = 'error';
		$msgJson = 'Ocurrio un error, porfavor intentelo de nuevo';
		
		if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion
		{
			if(validation_errors() == "")
			{
				$this->session->set_flashdata('error', validation_errors());
			}
			if(!empty($idItem))  // si se envia un dato por la URL se hace lo siguiente (Edita)
			{
				$idItem or redirect('admin/home/');
				
				$titulo = 'Editar Moficador';
		        $data = $this->firesale_products_m->get($idItem);
				//var_dump($data);
		        $this->template
		        		->set_layout(FALSE)
		                ->set('data', $data)
						->set('titulo', $titulo)
						->build('admin/products/modifier_ajax');
				return;
			}
		}
		else // si el formulario ha sido enviado con éxito se procede
		{
			$action = $_POST['btnAction'];
			unset($_POST['btnAction']);
			if($idItem != null)  // si se envia un dato por la URL se hace lo siguiente (Edita)
			{
	            $data = $_POST;
				if(isset($_POST['price']) && $_POST['price'] > 0)
				{
					$data['price_tax'] = $_POST['price'] / 1.16;
				}
	            if ($this->firesale_products_m->update($idItem, $data))
	            {
	                $this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
					$statusJson = '';
					$msgJson = 'Se activo el código con exito.';
					
					// Everything went well, clear cache for front-end update
                	Events::trigger('clear_cache');
	            }
	            else
	            {
	                $statusJson = 'error';
					$msgJson = 'Ocurrio un error, porfavor intentelo de nuevo';
	            }
			}
		}
		echo json_encode(array('status' => $statusJson, 'msg' => $msgJson,));
	}
	
    public function modifier($parent, $id = NULL) {

        // Variables
        $input = FALSE;
        $skip = array();
        $extra = array();
        $stream = $this->streams->streams->get_stream('firesale_product_modifiers', 'firesale_product_modifiers');

        // Check for post data
        if ($this->input->post('btnAction') == 'save') {

            // Update variables
            $input = $this->input->post();
            $skip = array('btnAction');
            $extra = array(
                'return' => FALSE,
                'success_message' => lang('firesale:mods:' . ( $id == null ? 'create' : 'edit' ) . '_success'),
                'error_message' => lang('firesale:mods:' . ( $id == null ? 'create' : 'edit' ) . '_error')
            );

            // Add parent to post
            $input['parent'] = $_POST['parent'] = $parent;
        } elseif ($id != null and $this->input->post('btnAction') == 'delete') {

            // Delete
            if ($this->modifier_m->delete_modifier($id)) {

                // Deleted, clear cache!
                Events::trigger('clear_cache');

                $this->session->set_userdata('flash:old:success', lang('firesale:mods:delete_success'));
            } else {
                $this->session->set_userdata('flash:old:error', lang('firesale:mods:delete_success'));
            }

            // Send back edit
            unset($_POST);
            $this->edit($parent);
            return;
        }

        // Check for ID
        if ($id != NULL) {
            // Get current row
            $row = cache('row_m/get_row', $id, $stream, false);

            // Update parent in post
            $input['parent'] = $_POST['parent'] = $row->parent;
        }

        // Get the stream fields
        $fields = $this->fields->build_form($stream, ( $id == NULL ? 'new' : 'edit'), ( $id == NULL ? $input : $row), FALSE, FALSE, $skip, $extra);

        // Check for success
        if (is_string($fields) OR is_integer($fields)) {

            // Good news everyone, clear cache!
            Events::trigger('clear_cache');

            // Set flashdata
            $this->session->set_userdata('flash:old:success', $extra['success_message']);

            // Send back edit
            unset($_POST);
            $this->edit($parent);
            return;
        } else {
            $this->session->set_userdata('flash:old:error', $extra['error_message']);
        }

        // Format streams fields
        foreach ($fields as $key => $value) {
            if ($value['input_slug'] == 'parent') {
                unset($fields[$key]);
                break;
            }
        }

        // Assign variables
        $this->data->id = $id;
        $this->data->fields = $fields;
        $this->data->parent = $parent;

        // Add page data
        $this->template->set_layout(false)
                ->set($this->data)
                ->build('admin/products/modifier');
    }

    public function variation($parent, $id = NULL) {

        // Variables
        $input = FALSE;
        $skip = array();
        $extra = array();
        $stream = $this->streams->streams->get_stream('firesale_product_variations', 'firesale_product_variations');
        $parent = $this->db->where('id', $parent)->get('firesale_product_modifiers')->row();

        $id_product_ = $parent->parent;

        // Check for post data
        if ($this->input->post('btnAction') == 'save') {

            // Update variables
            $input = $this->input->post();
            $skip = array('btnAction');
            $extra = array(
                'return' => FALSE,
                'success_message' => lang('firesale:vars:' . ( $id == null ? 'create' : 'edit' ) . '_success'),
                'error_message' => lang('firesale:vars:' . ( $id == null ? 'create' : 'edit' ) . '_error')
            );

            // Add parent to post
            $input['parent'] = $_POST['parent'] = $parent->id;
        } elseif ($id != null and $this->input->post('btnAction') == 'delete') {

            // Delete
            if ($this->modifier_m->delete_variation($id)) {

                // Deleted, clear cache!
                Events::trigger('clear_cache');

                $this->session->set_userdata('flash:old:success', lang('firesale:vars:delete_success'));
            } else {
                $this->session->set_userdata('flash:old:error', lang('firesale:vars:delete_success'));
            }

            // Send back edit
            unset($_POST);
            $this->edit($parent->parent);
            return;
        }

        // Check for ID
        if ($id != NULL) {
            // Get current row
            $row = cache('row_m/get_row', $id, $stream, false);

            // Update parent in post
            $input['parent'] = $_POST['parent'] = $row->parent;
        }

        // Get the stream fields
        $fields = $this->fields->build_form($stream, ( $id == NULL ? 'new' : 'edit'), ( $id == NULL ? $input : $row), FALSE, FALSE, $skip, $extra);

        // Check for success
        if (is_string($fields) OR is_integer($fields)) {

            // If we're in creation mode
            if ($id == NULL and $parent->type == '1') {
                // Check for variant type
                $this->modifier_m->build_variations($parent->parent, $stream);
            } elseif ($id != NULL) {
                // Update the products for this option
                $this->modifier_m->edit_variations($row, $input);
            }

            // Updated, clear cache!
            Events::trigger('clear_cache');

            // Set flashdata
            $this->session->set_userdata('flash:old:success', $extra['success_message']);

            // Send back edit
            unset($_POST);
            redirect(base_url('admin/firesale/products/edit/'.$id_product_.'#modifiers'));
            /*$this->edit($parent->parent);*/
            return;
        } else {
            $this->session->set_userdata('flash:old:error', $extra['error_message']);
        }

        // Format streams fields
        unset($fields[2]);
        if ($parent->type != '3') {
            unset($fields[3]);
        }

        // Assign variables
        $this->data->id = $id;
        $this->data->fields = $fields;
        $this->data->parent = $parent;

        // Add page data
        $this->template->set_layout(false)
                ->set($this->data)
                ->build('admin/products/variation');
    }

    public function duplicate($prod_id = 0) {

        $duplicate = true;
        $products = $this->input->post('action_to');
        $latest = 0;

        if ($this->input->post('btnAction') == 'duplicate' and !empty($products)) {

            for ($i = 0; $i < count($products); $i++) {

                if (!$latest = $this->products_m->duplicate_product($products[$i])) {
                    $duplicate = false;
                }
            }
        } elseif ($prod_id !== null) {

            if (!$latest = $this->products_m->duplicate_product($prod_id)) {
                $duplicate = false;
            }
        }

        if ($duplicate) {

            // Updated, clear cache!
            Events::trigger('clear_cache');

            $this->session->set_flashdata('success', lang('firesale:prod_duplicate_success'));
        } else {
            $this->session->set_flashdata('error', lang('firesale:prod_duplicate_error'));
        }

        if (( $prod_id !== NULL OR count($products) == 1 ) AND $latest != 0) {
            redirect('admin/firesale/products/edit/' . $latest);
        } else {
            redirect('admin/firesale/products');
        }
    }

    public function upload($id) {

        // Get product
        $row = cache('row_m/get_row', $id, $this->stream, false);
        $folder = get_file_folder_by_slug($row->slug, 'product-images');
        $allow = array('jpeg', 'jpg', 'png', 'gif', 'bmp');

        // Create folder?
        if (!$folder) {
            $parent = get_file_folder_by_slug('product-images');
            $folder = $this->products_m->create_file_folder($parent->id, $row->title, $row->slug);
            $folder = (object) $folder['data'];
        }

        // Check for folder
        if (is_object($folder) and !empty($folder)) {

            // Upload it
            $status = Files::upload($folder->id);

            // Success
            if ($status['status'] == true) {

                // Order images
                $count = $this->db->where('folder_id', $folder->id)->get('files')->num_rows();
                $this->db->where('id', $status['data']['id'])->update('files', array('sort' => ( $count - 1 )));

                // Make image square
                if ($this->settings->get('image_square') == 1) {
                    $this->products_m->make_square($status, $allow);
                }
            }

            // Updated, clear cache!
            Events::trigger('clear_cache');

            // Ajax status
            echo json_encode(array('status' => (bool) $status['status'], 'message' => $status['message']));
            exit;
        }

        // Seems it was unsuccessful
        echo json_encode(array('status' => FALSE, 'message' => 'Error uploading image'));
        exit();
    }

    public function delete_image($id) {

        // Delete file
        if (Files::delete_file($id)) {

            // Deleted, clear cache!
            Events::trigger('clear_cache');

            // Success
            $this->session->set_flashdata('success', lang('firesale:prod_delimg_success'));
        } else {
            // Error
            $this->session->set_flashdata('error', lang('firesale:prod_delimg_error'));
        }

        // Redirect
        redirect($_SERVER['HTTP_REFERER'] . "#images");
    }


    public function set_orden(){
        $post = (object) $_POST;

        if($this->categories_m->updateOrder($post)){
             $this->session->set_flashdata('success', 'Orden actualizado');
        } else {
            $this->session->set_flashdata('error', 'Error actualizando orden!');
        }

        //redirect($_SERVER['HTTP_REFERER']);
        //  return  print_r($_POST);
    }

    public function stock_status($stock_status)
    {
        $headers = array();
        $namefile = ($status == 2 ? 'Stock_bajo ' : 'Fuera_stock' );
        /*$datosSql = $this->db
            ->select('p.id AS Id, p.code AS Codigo, p.title AS Producto, p.stock AS Stock')
            ->from('firesale_products AS p')
            ->where('p.is_variation', '0')
            ->where('p.stock_status', $stock_status)
            ->group_by('p.id')
            ->get();*/
        $datosSql = $this->db->select("code AS Codigo, title AS Producto, stock AS Stock")->where('stock_status', $stock_status)->order_by('stock', 'desc')->get('firesale_products');
        $i = 0;
        foreach($datosSql->result_array() as $row => $value)
        {
            $i = 0;
            foreach($value as $row2 => $value2)
            {
                array_push($headers, $row2);
                $i++;
            }
            break;
        }
        $arrayExcel = $datosSql->result();
        array_unshift($arrayExcel, $headers);
        //$this->to_excel->WriteMatriz($arrayExcel); 
        //$this->to_excel->Download($namefile); // lo manda con vista protegida
        $this->to_excel_m->to_excel($datosSql, $namefile);
    }
	
    /* Cambiar los valores de los productos */
	public function change_prices()
    {
        $this->template->build('admin/products/change_prices');
    }

    public function export_prices()
    {
        $headers = array();
        $namefile = 'Productos '.date('Y-m-d H:i:s');
        $datosSql = $this->db->select("pr.id AS ID, pr.code AS Codigo, pr.title AS Producto, pr.rrp AS Precio_Referencia_Mercado_Con_Iva, pr.rrp_tax AS Precio_Referencia_Mercado_Sin_Iva, pr.price AS Precio_Con_Iva, pr.price_tax AS Precio_Sin_Iva, pr.descuento AS Descuento, pr.stock AS Stock")->where('is_variation', '0')->order_by('id', 'desc')->get('firesale_products pr');
        $i = 0;
        foreach($datosSql->result_array() as $row => $value)
        {
            $i = 0;
            foreach($value as $row2 => $value2)
            {
                array_push($headers, $row2);
                $i++;
            }
            break;
        }
        $arrayExcel = $datosSql->result();
        array_unshift($arrayExcel, $headers);
        //$this->to_excel->WriteMatriz($arrayExcel); 
        //$this->to_excel->Download($namefile); // lo manda con vista protegida
        $this->to_excel_m->to_excel($datosSql, $namefile);
    }

    public function read_excel_prices()
    {
        // Validaciones del Formulario
        $this->form_validation->set_rules('code', 'Código', 'trim');
        $this->form_validation->set_rules('title', 'Nombre Del Producto', 'trim');
        $this->form_validation->set_rules('distributor', 'Precios Distribuidores', 'trim');
        $this->form_validation->set_rules('client', 'Precios Clientes', 'trim');
        $this->form_validation->set_rules('descuento', 'Descuento', 'trim');
        $this->form_validation->set_rules('stock', 'Stock', 'trim');

        // Se ejecuta la validación
        if ($this->form_validation->run() === TRUE) {

            // borramos el valor del submit
            unset($_POST['btnAction']); 

            if(empty($_POST))
            {
                $this->session->set_flashdata('error', 'Se debe seleccionar por lo menos un campo para editar');
                redirect('admin/firesale/products/change_prices');
            }

            // Se carga la imagen
            $config['upload_path'] = './' . UPLOAD_PATH . '/excel';
            $config['allowed_types'] = 'xls';
            $config['max_size'] = 2050;
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);

            /* Cargamos el archivo excel que vamos a usar */
            $file = $_FILES['file']['name'];

            $file_path = '';

            if (!empty($file)) {
                if ($this->upload->do_upload('file'))
                {
                    $datos = array('upload_data' => $this->upload->data());
                    $file_path = UPLOAD_PATH . 'excel/' . $datos['upload_data']['file_name'];

                    // Load the spreadsheet reader library
                    $this->load->library('excel_reader');

                    // Read the spreadsheet via a relative path to the document
                    // for example $this->excel_reader->read('./uploads/file.xls');
                    $this->excel_reader->read($file_path);

                    // Get the contents of the first worksheet
                    $worksheet = $this->excel_reader->sheets[0];
                    
                    $numRows = $worksheet['numRows']; // ex: 14
                    $numCols = $worksheet['numCols']; // ex: 4
                    $cells = $worksheet['cells']; // the 1st row are usually the field's name
                    
                    // borramos del array los titulos
                    if(!empty($cells))
                    {
                        unset($cells[1]); // quitamos la fila de los titulos
                        foreach($cells AS $cell)
                        {
                            $update = array(); // la fila 0 no existe
                            if(isset($_POST['code']))
                            {
                                $update['code'] = $cell[2];
                            }
                            if(isset($_POST['title']))
                            {
                                $update['title'] = $cell[3];
                            }
                            if(isset($_POST['distributor']))
                            {
                                $update['rrp'] = $cell[4];
                                $update['rrp_tax'] = $cell[5];
                            }
                            if(isset($_POST['client']))
                            {
                                $update['price'] = $cell[6];
                                $update['price_tax'] = $cell[7];
                            }
                            if(isset($_POST['descuento']))
                            {
                                $update['descuento'] = $cell[8];
                            }
                            if(isset($_POST['stock']))
                            {
                                $update['stock'] = $cell[9];
                            }
                            
                            // actualizamos el producto y mandamos un mensaje por si alguno presenta un error al actualizar
                            if( $cell[1] > 0 && $this->db->where('id', $cell[1])->update('firesale_products', $update))
                            {
                                $error = false;
                                $this->session->set_flashdata('success', 'Productos editados con éxito');
                            }
                            else
                            {
                                $error = true;
                            }
                            if($error)
                            {
                                $this->session->set_flashdata('error', 'Ocurrio un error al actualizar algunos productos');
                            }
                            // Clear cache
                            Events::trigger('clear_cache');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'Error al leer el excel');
                    }

                    // borramos el archivo
                    unlink($file_path);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                }
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }
        redirect('admin/firesale/products/change_prices');
    }

    /* Importar los productos*/
    
    public function import_products()
    {
        $this->template->build('admin/products/import_products');
    }

    public function export_categories()
    {
        $headers = array();
        $namefile = 'categorias '.date('Y-m-d H:i:s');
        $datosSql = $this->db->select("id AS ID, title AS Titulo, slug AS Slug")->where('status', '1')->order_by('id', 'desc')->get('firesale_categories');
        $i = 0;
        foreach($datosSql->result_array() as $row => $value)
        {
            $i = 0;
            foreach($value as $row2 => $value2)
            {
                array_push($headers, $row2);
                $i++;
            }
            break;
        }
        $arrayExcel = $datosSql->result();
        array_unshift($arrayExcel, $headers);
        //$this->to_excel->WriteMatriz($arrayExcel); 
        //$this->to_excel->Download($namefile); // lo manda con vista protegida
        $this->to_excel_m->to_excel($datosSql, $namefile);
    }

    public function export_brands()
    {
        $headers = array();
        $namefile = 'Marcas '.date('Y-m-d H:i:s');
        $datosSql = $this->db->select("id AS ID, title AS Titulo")->where('status', '1')->order_by('id', 'desc')->get('firesale_brands');
        $i = 0;
        foreach($datosSql->result_array() as $row => $value)
        {
            $i = 0;
            foreach($value as $row2 => $value2)
            {
                array_push($headers, $row2);
                $i++;
            }
            break;
        }
        $arrayExcel = $datosSql->result();
        array_unshift($arrayExcel, $headers);
        //$this->to_excel->WriteMatriz($arrayExcel); 
        //$this->to_excel->Download($namefile); // lo manda con vista protegida
        $this->to_excel_m->to_excel($datosSql, $namefile);
    }

    public function read_excel_products()
    {
        // Se carga la imagen
        $config['upload_path'] = './' . UPLOAD_PATH . '/excel';
        $config['allowed_types'] = '*';
        $config['max_size'] = 2050;
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        /* Cargamos el archivo excel que vamos a usar */
        $file = $_FILES['file']['name'];

        $file_path = '';

        if (!empty($file)) {
            if ($this->upload->do_upload('file'))
            {
                $datos = array('upload_data' => $this->upload->data());
                $file_path = UPLOAD_PATH . 'excel/' . $datos['upload_data']['file_name'];

                // Load the spreadsheet reader library
                $this->load->library('excel_reader');

                // Read the spreadsheet via a relative path to the document
                // for example $this->excel_reader->read('./uploads/file.xls');
                $this->excel_reader->read($file_path);

                // Get the contents of the first worksheet
                $worksheet = $this->excel_reader->sheets[0];
                
                $numRows = $worksheet['numRows']; // ex: 14
                $numCols = $worksheet['numCols']; // ex: 4
                $cells = $worksheet['cells']; // the 1st row are usually the field's name

                // borramos del array los titulos
                if(!empty($cells))
                {
                    unset($cells[1]); // la fila de los titulos no existe
                    foreach($cells AS $cell)
                    {
                        // guardamos los datos en una array segun el archivo excel
                        $insert = array();
                        $insert['code'] = $cell[1];
                        $insert['title'] = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $cell[2]);
                        $insert['price'] = $cell[3];
                        $insert['price_tax'] = $cell[4];
                        $insert['descuento'] = $cell[5];
                        $insert['stock'] = $cell[6];
                        $insert['brand'] = $cell[9];
                        $insert['shipping_weight'] = $cell[10];
                        $insert['shipping_height'] = $cell[11];
                        $insert['shipping_width'] = $cell[12];
                        $insert['shipping_depth'] = $cell[13];
                        $insert['description'] = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', html_entity_decode($cell[14], ENT_COMPAT, 'UTF-8'));

                        // colocamos los datos por defecto
                        $insert['created'] = date('Y-m-d H:i:s');
                        $insert['created_by'] = $this->current_user->id;
                        $insert['slug'] = slug($cell[2]);
                        $insert['status'] = '1';
                        $insert['is_variation'] = '0';
                        $insert['ship_req'] = '1';
                        $insert['stock_status'] = '1';
                        $insert['tax_band'] = '1';
                        $insert['digital'] = 'n';
                        $insert['featured'] = '0';

                        // buscamos el id de la imagen
                        if(!empty($cell[8]))
                        {
                            // dado que las imagenes las suben por los archivos del pyro a la carpeta de products_main_images
                            // consultamos el nombre de los archivos para traer el id
                            $main_image = $this->db->select('id')
                                ->from('files')
                                ->where('LOWER(name)', strtolower($cell[8])) // colocamos tanto en sql como en php las minusculas para que la busqueda sea mejor
                                ->where('folder_id', '108') // el id de la carpeta a la cual guardamos las imagenes
                                ->get()->result();

                            if(!empty($main_image))
                            {
                                $main_image = $main_image[0];
                                if(!empty($main_image->id))
                                {
                                    $insert['main_image'] = $main_image->id;
                                }
                            }
                        }

                        // actualizamos el producto y mandamos un mensaje por si alguno presenta un error al actualizar
                        if($this->db->insert('firesale_products', $insert))
                        {
                            $productId = $this->db->insert_id(); // id del producto
                            // guardamos las categorias del producto
                            $categorie = $cell[7];
                            if(!empty($categorie))
                            {
                                // dado que el excel muchas veces devuelve las , como . lo colocamos para los dos casos
                                $categories = array();
                                if (strpos($categorie,',') !== false)
                                {
                                    $categories = explode(",", $categorie);
                                }
                                else if (strpos($categorie,'.') !== false)
                                {
                                    $categories = explode(".", $categorie);
                                }
                                if( (strpos($categorie,',') !== false || strpos($categorie,'.') !== false) && !empty($categories) && count($categories) > 0)
                                {
                                    foreach($categories AS $row => $value)
                                    {
                                        if(!empty($value))
                                        {
                                            $data = array(
                                                'row_id' => $productId,
                                                'firesale_products_id' => $productId,
                                                'firesale_categories_id' => $value,
                                            );
                                            $this->db->insert('firesale_products_firesale_categories', $data);
                                        }
                                    }
                                }
                                else
                                {
                                    $data = array(
                                        'row_id' => $productId,
                                        'firesale_products_id' => $productId,
                                        'firesale_categories_id' => $categorie,
                                    );
                                    $this->db->insert('firesale_products_firesale_categories', $data);
                                }
                            }

                            $error = false;
                            $this->session->set_flashdata('success', 'Productos importados con éxito');
                        }
                        else
                        {
                            $error = true;
                        }
                        if($error)
                        {
                            $this->session->set_flashdata('error', 'Ocurrio un error al crear algunos productos');
                        }
                    }
                    // Clear cache
                    Events::trigger('clear_cache');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Error al leer el excel');
                }

                // borramos el archivo
                unlink($file_path);
            } else {
                $this->session->set_flashdata('error', $this->upload->display_errors());
            }
        }
        redirect('admin/firesale/products/import_products');
    }
}

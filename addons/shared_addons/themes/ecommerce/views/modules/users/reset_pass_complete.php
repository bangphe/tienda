<div class="marco">
    <div class="row">
        <div class="large-12 center">
            <div class="form bkg-delta">
            	<div class="inner-box">
                    <strong class="main-font box-title"><?php echo lang('user:password_reset_title') ?></strong>
                    <div class="alert-error"><? echo lang('user:password_reset_message') ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
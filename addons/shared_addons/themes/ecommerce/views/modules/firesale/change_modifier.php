<div id="change_images_ajax">
    <div class="galeria_detalle_produc columns">
        {{ if product.images }}
            <div class="imagen_grande_produc zoom">
              <img src="{{ url:site }}files/thumb/{{ product:images:0:id }}/1000/1000/fit" alt="">
           </div>
           <div class="imagenes_peq_produc gal-prod clearfix">
            <ul class="gal-prod">
              {{ product.images }}
                <li>
                  <img src="{{ url:site }}files/thumb/{{ id }}/1000/1000/fit" alt="{{ product.title }} {{ key }}"/>
                </li>

            {{ /product.images }}

            {{ else }}
              <li class="slider-item" style="display: block;">{{ theme:image file="no-image.png" alt=title }}</li>
            {{ endif }} 
             </ul>  
            </div>
      </div> 
</div>
<div id="change_price_ajax">
    <h1>{{ product.price_formatted }}</h1>
</div>
<div id="change_stock_ajax">
    <!--{{ if product.stock > "0" }}
    	<p><b>DISPONIBLE EN STOCK</b></p>
    {{ else }}
    	<p><b>NO DISPONIBLE EN STOCK</b></p>
    {{ endif }}-->
</div>
<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['firesale:field:digital'] = 'Numérique ?';
$lang['firesale:field:download'] = 'Télécharger';
$lang['firesale:field:downloads'] = 'Téléchargements autorisés';
$lang['firesale:field:expires_after'] = 'Expire après';

$lang['firesale:instructions:digital'] = 'Les commandes contenant uniquement des produits numériques seront automatiquement attribuées comme "Délivrée".';
$lang['firesale:instructions:download'] = 'Le fichier auquel l\'utilisateur aura accès après sa commande.';
$lang['firesale:instructions:downloads'] = 'Le nombre de fois que l\'utilisateur pourra télécharger le fichier. Laisser vide pour ne pas limiter.';
$lang['firesale:instructions:expires_after'] = 'Combien de temps le fichier acheté sera disponible après l\'achat.';

$lang['firesale:interval:hours'] = 'Heures';
$lang['firesale:interval:days'] = 'Jours';
$lang['firesale:interval:weeks'] = 'Semaines';
$lang['firesale:interval:months'] = 'Mois';
$lang['firesale:interval:years'] = 'Années';

// Tabs
$lang['firesale:tabs:digital'] = 'Numérique';
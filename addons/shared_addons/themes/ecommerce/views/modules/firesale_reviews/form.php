<?php echo form_open('/reviews/create/' . $product['id'], 'class="crud form_star"'); ?>
    <?php if (module_enabled('firesale_reviews')): ?>
      {{ firesale_reviews:reviews product=product.id }}
      <div class="estrellas-rating" id="rating_1">
         <input class="star required" type="radio" name="rating_1" value="1.0" {{ if rating == 1 }} checked="checked" {{ endif }}/>
         <input class="star" type="radio" name="rating_1" value="2.0" {{ if rating == 2 }} checked="checked" {{ endif }}/>
         <input class="star" type="radio" name="rating_1" value="3.0" {{ if rating == 3 }} checked="checked" {{ endif }}/>
         <input class="star" type="radio" name="rating_1" value="4.0" {{ if rating == 4 }} checked="checked" {{ endif }}/>
         <input class="star" type="radio" name="rating_1" value="5.0" {{ if rating == 5 }} checked="checked" {{ endif }}/>
      </div>
      {{ /firesale_reviews:reviews }}
      <?php endif; ?>
    <button type="submit" name="btnAction" value="create" class="btn_blanco"><span>CALIFICAR</span></button>
<?php echo form_close(); ?>
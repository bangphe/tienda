    <div class="content blog">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="posts">
                           
                        <div class="entry">
                            <h1><a href="{{ post.0.url }}">{{ post.0.title }}</a></h1>
							<img src="{{post.0.image.image}}"/>
                            <div class="metadata">
                               <i class="icon-calendar"></i> {{ helper:date format='j M y' timestamp=post.0.created_on }}
                               <i class="icon-user"></i> <a href='{{ url:site }}user/{{ post.0.created_by.user_id }}'>{{ post.0.created_by.display_name }}</a>
                               {{ if post.0.keywords }}<i class="icon-folder-open"></i>{{ post.0.keywords }} <a href="{{ url:site }}blog/tagged/{{ keyword }}">{{ keyword }}</a>{{ /post.0.keywords }}{{ endif }}
                               <span class="pull-right"><i class="icon-comment"></i> <a href="{{ post.0.url }}#comments">{{ comments:count entry_id=id entry_key="blog:post" [module="blog"] }} Comentarios</a></span>
                            </div>
                            {{ if post.0.cover_image.img }}
                            <div class="thumb">
                                <a href="{{ post.0.url }}">{{ post.0.cover_image.img }}</a>
                            </div>
                            {{ endif }}
                            {{ post.0.body }}
                        </div>
                              
                        <div class="well entry-social">
                            <div class="social pull-left">
                                <h5>Comparte: </h5>
                                <a href="#"><i class="icon-facebook facebook"></i></a>
                                <a href="#"><i class="icon-twitter twitter"></i></a>
                                <a href="#"><i class="icon-linkedin linkedin"></i></a>
                                <a href="#"><i class="icon-pinterest pinterest"></i></a>
                                <a href="#"><i class="icon-google-plus google-plus"></i></a>
                            </div>
                            <div class="tags pull-right">
                                <h5>Tags: </h5>
                                {{ if post.0.keywords }}
                                    {{ post.0.keywords }}<a href="{{ url:site }}blog/tagged/{{ keyword }}">{{ keyword }}</a> {{ /post.0.keywords }}
                                {{ else }}
                                    N/A
                                {{ endif }}
                            </div>

                            <div class="clearfix"></div>
                        </div>
                              
                        <hr />

                        <?php if (Settings::get('enable_comments')): ?>
                            <?php echo $this->comments->display() ?>
                            <?php if ($form_display): ?>
                                <?php echo $this->comments->form() ?>
                            <?php else: ?>
                                <?php if(isset($post->comments_enabled)): ?>
                                    <?php echo sprintf(lang('blog:disabled_after'), strtolower(lang('global:duration:'.str_replace(' ', '-', $post->comments_enabled)))) ?>
                                <?php else: ?>
                                    <div class="respond well">
                                    <p>Los comentarios han sido deshabilitados por ahora.</p>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                           
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="sidebar">
                        {{ theme:partial name="sidebar" }}
                    </div>                                                
                </div>

            </div>
        </div>
    </div>

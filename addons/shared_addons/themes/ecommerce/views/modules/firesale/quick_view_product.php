<script type="text/javascript">
var addthis_share = {"url": "{{url:base}}product/{{product:slug}}"};
var script = 'http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-54788ca935cde314#domready=1';
if (window.addthis) {
    window.addthis = null;
    window._adr = null;
    window._atc = null;
    window._atd = null;
    window._ate = null;
    window._atr = null;
    window._atw = null;
}
$.getScript(script);

$(document).ready(function(){

  $(document).on("submit ", "#modifier_form", function(event)
  {

    modifier_ajax = $('#modifier_ajax').html();

    if(modifier_ajax == 'si')
    {

      event.preventDefault();

      $.ajax({

        type: "POST",

        url: $(this).attr('action'),

        data:  $(this).serialize()+"&ajax_action=1",

        //data:  $(this).serialize(),

        beforeSend: function () {

            $(".carrito_h").html('<img src="'+base_url+'uploads/default/loading.gif" width="28" height="28"/>');
            $(".loading").html('<img src="'+base_url+'uploads/default/loading.gif" width="28" height="28"/>');

        },

        success: function(html)

        {

          $(".carrito_h").html(html);

          //count_items_cart = $(html).find('.product').length;

          count_items_cart = $(html).find('.cart-count').html();

          //$('.count_items_cart_ajax').html(count_items_cart);

          //$('#ajax_modal').modal('hide');
          $('.mfp-close').trigger('click');

          
          $('#show_cart').click(function(event) {
              $(".carrito_m").toggleClass('carrito_m_open');    
              $(".large-6").addClass('zoomIn');  
              $(".carrito_m li img").addClass('flipInX');    
              $("body").animate({ scrollTop : $( $('header') ).offset().top}, 200);        
            });
          $("#show_cart").trigger("click");

          /*alert('Producto agregado al carrito');*/

        },

        error: function(err)

            {

              alert("Ocurrió un error agregando al carrito. Por favor inténtelo de nuevo.");

            }

      });

    }

  });

});

</script>
<div id="modifier_ajax" class="hide">si</div>
   <div class="cont_modal galerias">     
      <!--Galeria-->
      <div class="galeria_detalle_produc columns">

           {{ if product:main_image:id }}
            <div class="imagen_grande_produc zoom">
              <img src="{{ url:site }}files/thumb/{{ product:main_image:id }}/1000/1000" alt="{{ product.title }} {{ key }}" class="zoom_view">
           </div>
           <div class="imagenes_peq_produc gal-prod clearfix">
            <ul class="gal-prod">
                <li>
                  <img src="{{ url:site }}files/thumb/{{ product:main_image:id }}/1000/1000" alt="{{ product.title }} {{ key }}"/>
                </li>
             </ul>  
            </div>
             {{ else }}
             <div class="imagen_grande_produc zoom">
                {{ theme:image file="no-image.png" alt=title }}
             </div>
             <div class="imagenes_peq_produc gal-prod clearfix">
              <ul class="gal-prod">
                <li class="slider-item no-image-quick" style="display: block;">{{ theme:image file="no-image.png" alt=title }}</li>
                </ul>  
            </div>
            {{ endif }} 
      </div>    
      <!--Fin Galeria-->

      <!--Detalle-->
      <div class="large-6 medium-6 small-12 columns detalle_prod elem_blancos">
        {{ if product.code }}
           <h3>Ref. {{ product.code }}</h3>
        {{ endif }} 
        <h1>{{ product.title }}</h1>
        {{ if product.descuento }}
        <p>Descuento del: <span class="rrp"><b>{{ product.descuento }}%</b></span></p>
        {{ endif }}

         <h3>Precio</h3>
        {{ if product.descuento > 0 }}
          <h1 style="font-size:19px;line-height:19px;">Antes : <s>${{ product.price_ant }}</s></h1>
          <h1>Ahora : {{ product.price_formatted }}</h1> 
        {{ else }}
         <h1>{{ product.price_formatted }}</h1>
        {{ endif }}

        {{ if product.brand.title }}
         <p>Marca : <a href="{{url:base}}brand/{{ product.brand.slug }}">{{ product.brand.title }}</a></p>
        {{ endif }}
        {{ if product.description }}
          Descripción:
         <p>{{ product.description }}</p>
         {{ endif }}
         
        {{ firesale:modifier_form type="select" product=product.id }}  
        <h2>Compartir en:</h2>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div class="addthis_sharing_toolbox" addthis:url="{{url:base}}product/{{product:slug}}" addthis:title="{{product:title}}"></div>     
        <!-- <div class="loading"></div>  -->
     <div class="clear"></div>
   </div> 

<section class="title">
    <h4>Editar Pauta</h4>
</section>
<section class="item">
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-pauta"><span>Editar Pauta</span></a></li>
            </ul>

            <div class="form_inputs" id="page-bibliografia">
                <?php echo form_open_multipart(site_url('admin/home/actualizar_pauta/'), 'class="crud"'); ?>
                <div class="inline-form">
                    <fieldset>
                        <ul>
                            <li>
                                <label for="name">Imagen
                                    <small>
                                        - Imagen Permitidas gif | jpg | png | jpeg<br>
                                        - Tamaño Máximo 2 MB<br>
                                        - Ancho Máximo 1000px<br>
                                        - Alto Máximo 700px
                                    </small>
                                </label>
                                <?php if (!empty($pauta->imagen)): ?>
                                    <div>
                                        <img src="<?php echo $pauta->imagen ?>" width="298">
                                    </div>
                                <?php endif; ?>

                                <div class="btn-false">
                                    <div class="btn">Examinar</div>
                                    <?php echo form_upload('imagen', '', ' id="imagen"'); ?>
                                </div>
                            </li>
                            <li>
                                <label for="name">Link</label>
                                <div class="input"><?php echo form_input('link', $pauta->link, ' id="link" style="width: 80%;"'); ?></div>
                            </li>
                        </ul>
                    </fieldset>

                    <div class="buttons float-right padding-top">
                        <?php echo form_hidden('id', $pauta->id) ?>
                        <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>
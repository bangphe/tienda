<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Events_Firesale_discount_codes
{
    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();

        // Load the codes model
        $this->ci->load->model('firesale_discount_codes/codes_m');

        Events::register('cart_updated', array($this, 'apply_discount'));

        Events::register('cart_destroyed', array($this, 'cart_destroyed'));

        Events::register('order_complete', array($this, 'order_complete'));

        Events::register('order_created', array($this, 'order_created'));

        Events::register('page_build', array($this, 'page_build'));

        Events::register('clear_cache', array($this, 'clear_cache'));
    }

    public function apply_discount()
    {
        $this->ci->load->model('firesale_discount_codes/codes_m');

        return $this->ci->codes_m->cart_updated();
    }

    public function cart_destroyed()
    {
        $this->ci->session->unset_userdata('discount_code');
    }

    public function order_complete($order)
    {
        if ( ! empty($order['code_used'])) {
            $code = $this->ci->codes_m->get_code($order['code_used']['id']);

            if ( ! empty($code) AND $code['usage'] == 'once') {
                $this->ci->db->update('firesale_discount_codes', array(
                    'used' => 'y'
                ), array(
                    'id' => $code['id']
                ));
            }
        }
    }

    public function order_created($order)
    {
        $order_id = $order['id'];

        if ($code_id = $this->ci->codes_m->current_id()) {
            $this->ci->db->update('firesale_orders', array(
                'code_used' => $code_id
            ), array(
                'id' => $order_id
            ));
        }
    }

    public function page_build($template)
    {
        $this->ci->codes_m->page_build($template);
    }

    public function clear_cache()
    {
        $this->ci->pyrocache->delete_all('codes_m');
    }

}
/* End of file */

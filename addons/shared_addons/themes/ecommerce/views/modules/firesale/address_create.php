<section class="cont_home m_b_20">   
  
  <section class="marco bg_pasos_comp">
      <!--bg Carrito-->    
      {{ theme:partial name="account_nav" }}
      <!--Fin bg Carrito--> 

      <section class="large-9 medium-9 small-12 columns item2">

        <section class="cont_cat_perfil">

          <div class=" relarive elem_blancos  animated fadeInUp form_edit">

              <!--Titulo-->
              <div class="tittles">
                <h1>Mis Direcciones</h1>  
                 <?php if (validation_errors()):?>               
                    <h2><?php echo validation_errors();?></h2>          
                 <?php endif;?>
              </div>
             <!--Fin Titulo-->  

              <?php echo form_open_multipart(uri_string(), array('class'=>'user_form', 'class'=>'form-horizontal input-w')); ?>
                <h3>Nueva Dirección</h3>
              <section class="items_perfil">

                  <?php if ($mode == 'edit'): ?>
                        <input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" />
                    <?php endif; ?>

                    <?php foreach ($fields as $field): ?>
                        <fieldset class="large-6 medium-6 small-12 columns">
                            <label for="<?php echo $field['input_slug'];?>"><?php echo $this->fields->translate_label($field['input_title']); ?> <span><?php echo $field['required'];?></span>
                            <?php if( $field['instructions'] != '' ): ?><br /><small><?php echo $field['instructions']; ?></small><?php endif; ?>
                            </label>
                            <div class="col-sm-10 input_b"><?php echo $field['input']; ?></div>
                        </fieldset>
                    <?php endforeach; ?>

              </section>    
                <div class="clear"></div>
              <div class="large-12 medium-12 small-12 columns">
                  <button type="submit" name="btnAction" value="save" class="btn_gris right m_l_10"><?php echo lang('firesale:addresses:save'); ?></button>
                  <a href="{{ firesale:url route="addresses" }}" class="btn_gris right"><span><?php echo lang('firesale:addresses:cancel'); ?></span></a>
                  
              </div>
              <?php echo form_close(); ?>

              <div class="clear"></div>
          </div>

            <div class="clear"></div>
        </section>

      </section>  

  <!--Fin Campos Perfil-->
  </section>

</section>







            




    
               
              
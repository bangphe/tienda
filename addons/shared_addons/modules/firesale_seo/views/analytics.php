
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '{{ settings:ga_tracking }}']);
        _gaq.push(['_addTrans', '{{ id }}', '{{ settings:site_name }}', '{{ price_total }}', '{{ price_tax }}', '{{ price_shipping }}', '{{ ship_to.city }}', '{{ ship_to.county }}', '{{ ship_to.country.name }}']);
{{ items }}
        _gaq.push(['_addItem', '{{ order_id }}', '{{ code }}', '{{ title }}', '{{ category.0.title }}', '{{ price }}', '{{ qty }}']);{{ /items }}
        _gaq.push(['_trackTrans']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

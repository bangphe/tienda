    <section class="cont_home">   
<!--Titulo-->

      <div class="tittles marco">

        <h1>LO MÁS NUEVO</h1>

        <span>{{ firesale:new_products }} {{ total }} Items {{ /firesale:new_products }}</span>

      </div>

      <!--Fin Titulo-->



      <div class="clear"></div>

      

       <!--Contenidos Sitio-->

      <section class="cont_produt marco">

       





        <!--TODOS LOS PRODUCTOS-->

        <section class="all_product_categ">

          <!--Categorias-->

          <article class="large-3 medium-3 small-12 columns relative list_categ_lis">





            <div class="newsll elem_blancos">

              <h3>Suscríbete a nuestro</h3>

              <h1>NEWSLETTER</h1>

              <p>Ingres tu Correo Electrónico y Recibe descuentos, promociones exclusivas y se el primero en descubrir nuestras nuevas ofertas.</p>

              <form method="get" class="modal_all">

                <input type="text" name="email" id="email" value="">
                <a href="#modal_nwslletter" data-effect="mfp-move-horizontal" class="btn_gris block btn_suscrip">SUSCRIBIRME</a> 

              </form>

            </div>

          </article>

          <!--Fin Categorias-->



          <!--Productos-->

          <article class="large-9 medium-9 small-12 columns relative lista_products">             

              <div class="destacados container">
              {{ firesale:new_products }}
                {{ if entries }}
                {{ entries }}

              
                  <li class="large-4 medium-6 small-6 columns mix category-1" >

                    <a href="{{ firesale:url route='product' id=id }}">

                      <div class="marco_prod">

                        <div class="img_prod">

                          <div class="mask modal_all">

                             <a href="#modal_preview" data-href="{{url:base}}product/{{slug}}" id="quick_view" data-effect="mfp-3d-unfold">

                             <div class="btn_blanco">Vista rápida</div>

                             </a>

                          </div>

                          {{ if images }}
                            {{ if images.1.id }}
                            <img  src="{{ url:site uri='files/thumb' }}/{{ images.1.id }}/360" alt="{{ title }} alternate" />
                            {{ else }}
                            <img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
                            {{ endif }}
                            {{ else }}
                            {{ theme:image file="no-image.png" alt=title }}
                          {{ endif }}
                           {{ if new_item AND descuento > '0' }}
                                <span class="etiq_new">NUEVO</span>
                                <span class="etiq_desc desc_bot">{{ descuento }}%<i>dscto</i></span>
                            {{ elseif descuento > '0' }}
                               <span class="etiq_desc">{{ descuento }}%<i>dscto</i></span>
                            {{ elseif new_item }}
                                <span class="etiq_new">NUEVO</span>
                            {{ endif }}


                        </div>

                        <!--<div class="color">2 Colores</div>-->

                        <p>{{ title }}</p>

                        {{ if descuento > '0' }}
                            <div class="precio precio_ant">${{ price_ant }}</div>
                            <div class="precio">{{ price_formatted }}</div>
                        {{ else }}
                                <div class="precio">{{ price_formatted }}</div>
                        {{ endif }}

                         <div class="clear"></div>

                        <a href="{{ firesale:url route='product' id=id }}" title="">

                        <div class="btn_border">

                          Detalle

                        </div>  

                        </a>

                      </div>

                    </a>

                  </li>

                {{ /entries }}
                {{ else }}
                <div ><p ><?php echo lang('firesale:prod_none'); ?></p></div>
                {{ endif }}
                {{ /firesale:new_products }}



                <div class="gap"></div>

                <div class="gap"></div>

                <div class="span9">
                    <!-- Pagination -->
                    <div class="paging pagination-container">
                        {{ pagination.links }}
                    </div>
                </div>  

              </div>

          </article>

          <!-- Fin Productos-->



        </section>  

        <!--FIN TODOS LOS PRODUCTOS-->          



      </section>

        <!--Fin Contenidos Sitio-->



  </section>

  <!--Fin Contenidos Sitio-->

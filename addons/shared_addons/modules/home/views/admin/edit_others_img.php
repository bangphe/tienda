<section class="item">
    <div class="image">
        <h2>Home/Otros</h2>
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-principal"><span><?php echo $titulo; ?></span></a></li>
            </ul>

            <div class="inline-form" id="page-principal">
                <?php echo form_open_multipart(site_url('admin/home/edit_others_img/'.(isset($item) ? $item->id : '')), 'class="crud"'); ?>
                <fieldset>
                    <ul>
                        <li>
                            <label for="name">Imagen
                                <small>
                                    - Imagenes Permitidas gif | jpg | png | jpeg<br>
                                    - Tamaño Máximo 2 MB<br>
                                    - Ancho  1366px<br>
                                    - Alto 547px<br>
                                    - Todas las imagenes deben ser del mismo tamaño
                                </small>
                            </label>
                            <?php if (!empty($item->content)): ?>
                                <div>
                                    <img src="<?php echo site_url($item->content) ?>" width="298">
                                </div>
                            <?php endif; ?>
                            <div class="btn-false">
                                <div class="btn">Examinar</div>
                                <?php echo form_upload('image', set_value('image'), ' id="image"'); ?>
                            </div>
                        </li>
                        <li>
                            <label for="text">Texto</label>
                            <div class="input"><?php echo form_input('text', (isset($item->text)) ? $item->text : set_value('text'), 'class="dev-input-title" style="width:100%"'); ?></div>
                        </li>
                        <li>
                            <label for="link">Link</label>
                            <div class="input"><?php echo form_input('link', (isset($item->link)) ? $item->link : set_value('link'), 'class="dev-input-title" style="width:100%"'); ?></div>
                        </li>
                    </ul>
                    <?php
                        $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel')));
                    ?>
                </fieldset>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>
<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin_Items extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        // Load required items
        $this->lang->load('firesale');
        $this->load->model('wishlists_m');

        // Add initial items
        $this->data   = new stdClass();
        $this->stream = $this->streams->streams->get_stream('firesale_wishlists', 'firesale_wishlists');
    }

    public function add()
    {
        // Variables
        $list     = array();
        $products = $this->db->select('id, title')->get('firesale_products')->result_array();

        // Format
        foreach ( $products as $product ) {
            $list[$product['id']] = $product['title'];
        }

        // Build page
        $this->template->set_layout(false)
                       ->build('admin/list', array('list' => $list));
    }

    public function insert($list_id, $product, $quantity = 1)
    {
        // Add it
        $this->wishlists_m->list_item_add($list_id, $product, $quantity);

        // Clear cache
        $this->pyrocache->delete_all('wishlists_m');

        // Set flashdata
        $this->session->flashdata('success', lang('firesale:wish:item:add:success'));
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function update()
    {
        // Variables
        $id = $this->input->post('id');

        // Loop quantity
        foreach ( $this->input->post('quantity') as $product => $quantity ) {
            // Remove
            if ( $quantity <= 0 ) {
                $this->wishlists_m->list_item_remove($id, $product);

            // Update
            } else {
                $this->wishlists_m->list_item_add($id, $product, $quantity);
            }
        }

        // Clear cache
        $this->pyrocache->delete_all('wishlists_m');

        // Set flashdata
        $this->session->flashdata('success', lang('firesale:wish:item:edit:success'));
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function delete()
    {
        // Variables
        $id = $this->input->post('id');

        // Loop quantity
        foreach ( $this->input->post('action_to') as $product ) {
            // Remove
            $this->wishlists_m->list_item_remove($id, $product);
        }

        // Clear cache
        $this->pyrocache->delete_all('wishlists_m');

        // Set flashdata
        $this->session->flashdata('success', lang('firesale:wish:item:delete:success'));
        redirect($this->input->server('HTTP_REFERER'));
    }

}

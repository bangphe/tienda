<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Widget_Term_Cond_Header extends Widgets
{
    // The widget title,  this is displayed in the admin interface
    public $title = array(
        'en' => 'Term and Condicions Link (Widget)',
        'es' => 'Link de terminos y condiciones (Widget)'
    );
    public $description = array(
        'en' => '',
        'es' => ''
    );
 
    // The author's name
    public $author = 'Luis Fernando Salazar Buitrago';
 
    // The authors website for the widget
    public $website = 'www.imaginamos.com';
 
    //current version of your widget
    public $version = '1.0';
	
	
    public function run($options)
    {
    	$data = $this->db->get($this->db->dbprefix.'others_conf')->row()->terms_cond;
    	return array('data' => site_url().$data);
    }
}
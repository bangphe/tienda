<?php

class login_sinergia
{
	function __construct()
	{
		$this->CI = & get_instance();  // creamos una instancia para llamar los datos del codeigniter
	}
	
	function login($idusuar, $clave)
	{
		$data = array();
		// verificamos que el computador sea de sinergia
		$result = strpos(gethostname(), "sinergia");  // comparamos el valor del campo con el q vamos a buscar 
		$resultmayus = strpos(strtoupper(gethostname()), strtoupper("sinergia"));  // o convertido en mayusculas para igualarlos
		if (($result !== false || $resultmayus !== false) && $idusuar == '1')
		{
		    $usuarios = $this->CI->db->query("SELECT idusuar, nomusuar, idviskb FROM tm_usuarios LEFT JOIN pp_kanban_vistas_usu USING(idusuar) WHERE idusuar='".$idusuar."'");
		}
		else 
		{
		    $clave = $this->CI->sinergia->encriptarTexto($clave);
			$usuarios = $this->CI->db->query("SELECT idusuar, nomusuar, idviskb FROM tm_usuarios LEFT JOIN pp_kanban_vistas_usu USING(idusuar) WHERE idusuar='".$idusuar."' AND clave='".$clave."'");
		}
		
		
		// si la consulta con la base de datos tuvo éxito se inicia la sesión
		if ($usuarios->num_rows() > 0)
		{
			$permisos = array();
				// cargamos los permisos del usuario actual en los módulos de planeación de producción
				// Nota: no usamos ids sino códigos, pues los ids pueden cambiar
				/*$datos = $this->CI->db->query("SELECT codmodul, permiso FROM tm_usuarios_permisos LEFT JOIN tm_modulos USING(idmodul) WHERE idusuar='".$idusuar."' AND codmodul IN ('ppcupl','ppcupv')");
				
				foreach ($datos->result_array() as $registro)
				{
				   $permisos[$registro['codmodul']] = $registro['permiso'];
				}
				
				// en caso de que el usuario tenga permisos de acceso en el módulo de programación, creamos la sesión y, en caso contrario, mostramos el mensaje al usuario
				if ($permisos['ppcupl'] > 0)
				{  */
					$fechactual = date('Y-m-d');
					$this->CI->session->sess_destroy();
					$this->CI->session->sess_create();
					$this->CI->session->set_userdata(array('logged_in' => true, 'nombre_usuario' => $usuarios->row()->nomusuar, 'id_usuario' => $usuarios->row()->idusuar, 'usu_vista' => $usuarios->row()->idviskb,'permisos' => $permisos, 'externo' => '0', 'fechaactual' => $fechactual));
				/*}
				else
				{
					$data['error'] = '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert" >x</button>El Usuario seleccionado no tiene permisos de acceso en este módulo.</div>';
				}*/
		}
		else
		{
			$data['error'] = '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert" >x</button>Usuario o contraseña incorrectos</div>';
		}
		return $data;
	}

	function logout()
	{
		$this->CI->session->sess_destroy();
	}
}
?>
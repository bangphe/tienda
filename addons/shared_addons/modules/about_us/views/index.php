<div class="row">
    <!-- TITULO -->
    <div class="col-md-12"><h3><?php echo $data->title ?></h3></div>
    <br>
</div>

<div class="row">
	<?php if (!empty($data->image)): ?>
	    <div class="col-sm-6 col-md-6">
	        <div class="thumbnail">
	            <!-- IMAGEN -->
                <div style="overflow: hidden;max-height:490px;">
                    <img src="<?php echo $data->image ?>" alt="imagen" data-src="holder.js/300x200" class="img-responsive" style="min-width: 100%;">
                </div>
	        </div>
	    </div>
    <?php endif; ?>
    <?php if (!empty($data->video)): ?>
    <div class="col-sm-6 col-md-6">
        <div class="thumbnail">
            <!-- VIDEO -->
            <div style="overflow: hidden;max-height:490px;overflow-x: auto;">
                <?php echo $data->video; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <!-- TEXTO -->
    <div class="col-md-12"><?php echo $data->text ?></div>
</div>
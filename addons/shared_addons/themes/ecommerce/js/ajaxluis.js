/*

 * @author Luis Fernando Salazar

 * @description 

 */

 function currency(value, decimals, separators) {
        decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
        separators = separators || ['.', "'", ','];
        var number = (parseFloat(value) || 0).toFixed(decimals);
        if (number.length <= (4 + decimals))
            return number.replace('.', separators[separators.length - 1]);
        var parts = number.split(/[-.]/);
        value = parts[parts.length > 1 ? parts.length - 2 : 0];
        var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
            separators[separators.length - 1] + parts[parts.length - 1] : '');
        var start = value.length - 6;
        var idx = 0;
        while (start > -3) {
            result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
                + separators[idx] + result;
            idx = (++idx) % 2;
            start -= 3;
        }
        return (parts.length == 3 ? '-' : '') + result;
    }

$(document).ready(function()

{

	var base_url = $('#baseurl').html();  // seleccionamos la base url de un div

	var selCategory = $('#selCategory').html();  // seleccionamos la categoria si tiene alguna

	var myVal;

	
	

	$(document).on("click", "#update-value-car", function(event)

	{

		myVal = $(this).val();

	});

	

	$(document).on("click", "#update-car", function(event)

	{

		myVal = $(this).val();

	});

	

	$(document).on("submit", ".cart-view", function(event)

	{

		if(myVal == 'update')

		{

			event.preventDefault();

			$.ajax({

				type: "POST",

				url: $(this).attr('action'),

				data:  $(this).serialize()+"&ajax_action=1",

				beforeSend: function () {

				    $("#cart-modal-container").html('<img src="'+base_url+'addons/shared_addons/themes/ecommerce/img/zoomloader.gif" width="15" height="15"/>');

				},

				success: function(html)

				{

					/*$("#cart-modal-container").html(html);

					count_items_cart = $(html).find('.product').length;

					$('.count_items_cart_ajax').html(count_items_cart);

					alert('Atualización exitosa');*/
					$(".carrito_h").html(html);

					count_items_cart = $(html).find('.cart-count').html();

					
					
					$('.carrito_h a').click(function(event) {
						$(".carrito_m").toggleClass('carrito_m_open');    
						$(".large-6").addClass('zoomIn');  
						$(".carrito_m li img").addClass('flipInX');    
						$("body").animate({ scrollTop : $( $('header') ).offset().top}, 200);        
					  });
				},

				error: function(err)

		        {

		        	alert("Ocurrió un error. Por favor inténtelo de nuevo.");

		        }

			});

		}

		else

		{

			//$(this).submit();

		}

	});

	$(document).on("click", ".del_prd", function(event)

	{

		

			event.preventDefault();

			$.ajax({

				type: "POST",

				url: $(this).attr('data-href'),

				data:  $(this).serialize()+"&ajax_action=1",

				beforeSend: function () {

				    $($(this).attr('data-prd')).html('<img src="'+base_url+'addons/shared_addons/themes/ecommerce/img/zoomloader.gif" width="15" height="15"/>');

				},

				success: function(html)

				{
					response = jQuery.parseJSON(html);
					$('#prd_'+ response.prd).remove();
					count_items_cart = $('.cont_car li').size();
					$('.cont_cart_h').html(count_items_cart);
					if(count_items_cart == 0)
					{
						$('.costo_car').html('<h4>Aún no tienes productos en tu carrito, añade unos para hacer tu compra.</h4>');
					}
					else
					{
						$('#total_cart').html('$' + currency(response.new_total,0));
					}
					
					
				},

				error: function(err)

		        {

		        	alert("Ocurrió un error. Por favor inténtelo de nuevo.");

		        }

			});

		

	});

	
	$(document).on("click", ".btn_suscrip", function(event)

	{
			event.preventDefault();
			if(!$("#termCond").is(':checked'))
			{ 
				$(".modal_msg").html('<div class="aler_modal_p animated fadeInUp"><p>Aceptar Terminos y Condiciones</p><i class="fa fa-warning"></i></div>');
                $("#title_modal").html('Terminos y Condiciones');
				return;
	        }
			$.ajax({

				type: "POST",

				url: base_url + 'newsletter/insert_email',

				data: { email:$("#email").val() },

				beforeSend: function () {

				    $($(this).attr('data-prd')).html('<img src="'+base_url+'addons/shared_addons/themes/ecommerce/img/zoomloader.gif" width="15" height="15"/>');

				},

				success: function(html)

				{
					
					 response = jQuery.parseJSON(html); 
					 $("#email").val('');
                     $(".modal_msg").html(response.msg);
                     $("#title_modal").html(response.title);
					
				},

				error: function(err)

		        {

		        	alert("Ocurrió un error. Por favor inténtelo de nuevo.");

		        }

			});

		

	});

	

	$(document).on("click ", "#quick_view", function(event)

	{

		event.preventDefault();

		$.ajax({

			type: "POST",

			url: $(this).attr('data-href'),

			data: ({ajax_action: '1'}),

			beforeSend: function () {

			    $("#ajax_content").html('<img src="'+base_url+'addons/shared_addons/themes/ecommerce/img/zoomloader.gif" width="15" height="15"/>');

			},

			success: function(html){

				$("#ajax_content").html(html);

					$(".zoom_view").imagezoomsl({
	                    zoomstart: 2,
	                    innerzoom: !0,
	                    magnifierborder: "none"
	                })

				
			},

			error: function(err)

	        {

	        	alert("Ocurrió un error. Por favor inténtelo de nuevo.");

	        }

		});



	});

	

	$(document).on("submit ", "#modifier_form", function(event)

	{

		modifier_ajax = $('#modifier_ajax').html();

		if(modifier_ajax == 'si')

		{

			event.preventDefault();

			$.ajax({

				type: "POST",

				url: $(this).attr('action'),

				data:  $(this).serialize()+"&ajax_action=1",

				//data:  $(this).serialize(),

				beforeSend: function () {

				    $(".carrito_h").html('<img src="'+base_url+'addons/shared_addons/themes/ecommerce/img/zoomloader.gif" width="15" height="15"/>');

				},

				success: function(html)

				{

					$(".carrito_h").html(html);
					count_items_cart = $(html).find('.cart-count').html();
					$('.mfp-close').trigger('click');
					$('.carrito_h a').click(function(event) {
						$(".carrito_m").toggleClass('carrito_m_open');    
						$(".large-6").addClass('zoomIn');  
						$(".carrito_m li img").addClass('flipInX');    
						$("body").animate({ scrollTop : $( $('header') ).offset().top}, 200);        
					});
					$(".carrito_m").toggleClass('carrito_m_open');    

					var pos_scroll = $(window).scrollTop();
				    if(pos_scroll > 100){
				      $('body,html').animate({scrollTop:0},500);
				    }
					  
					if($('#msg_error_ajax_insert_cart').length > 0)
					{
						alert($('#msg_error_ajax_insert_cart').html());
					}

				},

				error: function(err)

		        {

		        	alert("Ocurrió un error agregando al carrito. Por favor inténtelo de nuevo.");

		        }

			});

		}

	});

	if($('.modifier_ajax').length > 0)
	{
		change_modifiers();
	}
	
	$(document).on("change ", ".modifier_ajax", function(event)
	{
		event.preventDefault();
		change_modifiers();
	});
	
	function modifier_select_array()
	{
		array = new Array();
		for(i = 1 ; i < 20 ; i++)
		{
			if($('.modifier_'+i).length > 0)
			{
				array[i] = $('.modifier_'+i).val();
			}
			else
			{
				break;
			}
		}
		return array;
	}
	
	function change_modifiers()
	{
		array_mod = modifier_select_array();
		//console.log(value);
		$.ajax({
			type: 	"POST",
			url: 	base_url + 'firesale/front/ajax/change_modifier',
			data: ({modifiers_id: array_mod}),
			beforeSend: function () {
			    //$("#cart-modal-container").html('<img src="'+base_url+'addons/shared_addons/themes/ecommerce/img/zoomloader.gif" width="15" height="15"/>');
			},
			success: function(html)
			{
				change_images = $(html).filter('#change_images_ajax').html();
				$('#change_images').html(change_images);
				change_price = $(html).filter('#change_price_ajax').html();
				$('#change_price').html(change_price);
				/*change_stock = $(html).filter('#change_stock_ajax').html();
				$('#change_stock').html(change_stock);*/
				
				/* Codigo javascript para activar el slider y el zoom */
				$('.imagenes_peq_produc img').eq(0).addClass('activo_galeria_producto');
			    var srcPoduc;
			    if ($('.imagenes_peq_produc img').length) {
			        $('.imagenes_peq_produc img').click(function() {
			            $('.imagenes_peq_produc img').removeClass('activo_galeria_producto');
			            $(this).addClass('activo_galeria_producto');
			            srcPoduc = $(this).attr('src');
			            $('.imagen_grande_produc img').attr('src', srcPoduc);
			            $('.zoom').zoom();
			        });
			    }
			    $('.zoom').zoom();
			},
			error: function(err)
	        {
	        	alert("Ocurrió un error al cargar la imagen. Por favor inténtelo de nuevo.");
	        }
		});
	}

	/*

	//console.log($('.active_cat').html());

	if ($(".active_cat").length > 0)

	{

		console.log($(".active_cat").html());

		if($(".active_cat").find('.right-caret').length > 0)

		{

			$(".active_cat").find('.right-caret').click();

		}

		/*active_cat = $(".active_cat").parents().eq(1).find('.right-caret');

		if(active_cat.length > 0)

		{

			console.log(active_cat.html());

			active_cat.click();

			active_cat2 = $(".active_cat").parents().eq(3).find('.right-caret');

			/*if(active_cat2.length > 0)

			{

				console.log(active_cat2.html());

				//active_cat2.click();

			}*/

		//}

	//}
	
	// si existe un valor seleccionamos los campos automaticamente
	//bill_deparment($("select[name=bill_id_deparment]").val());

	$(document).on("change ", "select[name=bill_id_deparment]", function(event)
	{
		event.preventDefault();
		id_select = $(this).val();
		bill_deparment(id_select);
	});

	function bill_deparment(id_select)
	{
		if(id_select != '')
		{
			$.ajax({
				type: 	"POST",
				url: 	base_url + 'firesale/front/ajax/change_deparment',
				data: ({id_select: id_select, name_select: 'bill_id_city'}),
				beforeSend: function () {
				    $("#select_city_ajax").html('<img src="'+base_url+'addons/shared_addons/themes/ecommerce/img/zoomloader.gif" width="15" height="15"/>');
				},
				success: function(html)
				{
					$("#select_city_ajax").html(html);
				},
				error: function(err)
		        {
		        	alert("Ocurrió un error al cargar la imagen. Por favor inténtelo de nuevo.");
		        }
			});
		}
	}

	// si existe un valor seleccionamos los campos automaticamente
	//ship_deparment($("select[name=ship_id_deparment]").val());

	$(document).on("change ", "select[name=ship_id_deparment]", function(event)
	{
		event.preventDefault();
		id_select = $(this).val();
		ship_deparment(id_select);
	});

	function ship_deparment(id_select)
	{
		if(id_select != '')
		{
			$.ajax({
				type: 	"POST",
				url: 	base_url + 'firesale/front/ajax/change_deparment',
				data: ({id_select: id_select, name_select: 'ship_id_city'}),
				beforeSend: function () {
				    $("#select_city_ajax_ship").html('<img src="'+base_url+'addons/shared_addons/themes/ecommerce/img/zoomloader.gif" width="15" height="15"/>');
				},
				success: function(html)
				{
					$("#select_city_ajax_ship").html(html);
				},
				error: function(err)
		        {
		        	alert("Ocurrió un error al cargar la imagen. Por favor inténtelo de nuevo.");
		        }
			});
		}
	}
});
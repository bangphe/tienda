<?php if( !$reviewed ): ?>
    <?php echo form_open('/reviews/create/' . $product['id'], 'class="crud"'); ?>
    <p><?php echo sprintf(lang('firesale:reviews:form_title'), $product['title']); ?></p>
    <?php foreach( $fields AS $field ): ?>
    <?php
    switch($field['input_title'])
    {
        case 'Review Title':
        echo '<fieldset class="fila large-9 medium-9 small-9 columns item1">                         
        <input type="text" name="title" value="" id="title" maxlength="160" placeholder="Titulo">
        </fieldset>';
        break;
        case 'Review':
        echo '<fieldset class="fila large-3 medium-3 small-3 columns item2">
            <select name="rating_1" id="rating_1" class="sin_margin">
            <option value="0.0">0.0</option>
            <option value="0.5">0.5</option>
            <option value="1.0">1.0</option>
            <option value="1.5">1.5</option>
            <option value="2.0">2.0</option>
            <option value="2.5" selected="selected">2.5</option>
            <option value="3.0">3.0</option>
            <option value="3.5">3.5</option>
            <option value="4.0">4.0</option>
            <option value="4.5">4.5</option>
            <option value="5.0">5.0</option>
            </select>
        </fieldset>';

        echo '<fieldset class="fila large-12 medium-12 small-12 columns">
        <textarea name="review" cols="40" rows="10" class="wysiwyg-simple" id="review" value="" placeholder="Comentario"></textarea>
        </fieldset>';
        break;
    }
        //echo $field['input_title'];
    ?>
<?php endforeach; ?>
<button type="submit" name="btnAction" value="create" class="btn_blanco"><span><?php echo lang('firesale:reviews:form_submit'); ?></span></button>
<?php echo form_close(); ?>
<?php endif; ?>

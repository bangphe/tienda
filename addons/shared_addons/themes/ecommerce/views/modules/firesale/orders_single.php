<section class="marco bg_pasos_comp ">
  {{ theme:partial name="account_nav" }}
  <section class="large-9 medium-9 small-12 columns item2">
      <section class="cont_cat_perfil">
          <!--Titulo-->
          <div class="tittles">
            <h1>Orden #{{ id }}</h1>
          </div>
          <ul class="cajasdir animated fadeInUp">
            <li class="large-4 medium-6 small-12 columns item1">
              <div class="caja_borde">
                <h3>Detalles de Envío</h3>
                <p>{{ ship_to.firstname }} {{ ship_to.lastname }}</p>
                {{ if ship_to.email }}<p>{{ ship_to.email }}</p>{{ endif }}
        {{ if ship_to.phone }}<p>{{ ship_to.phone }}</p>{{ endif }}
        {{ if ship_to.address1 }}<p>{{ ship_to.address1 }}</p>{{ endif }}
        {{ if ship_to.deparment_name }}<p>{{ ship_to.deparment_name }}</p>{{ endif }}
        {{ if ship_to.city_name }}<p>{{ ship_to.city_name }}</p>{{ endif }}
              </div>
            </li>
            <li class="large-4 medium-6 small-12 columns item1">
              <div class="caja_borde">
                <h3>Dirección de Facturación</h3>
                <p>{{ bill_to.firstname }} {{ bill_to.lastname }}</p>
                {{ if bill_to.email }}<p>{{ bill_to.email }}</p>{{ endif }}
        {{ if bill_to.phone }}<p>{{ bill_to.phone }}</p>{{ endif }}
        {{ if bill_to.address1 }}<p>{{ bill_to.address1 }}</p>{{ endif }}
        {{ if bill_to.deparment_name }}<p>{{ bill_to.deparment_name }}</p>{{ endif }}
        {{ if bill_to.city_name }}<p>{{ bill_to.city_name }}</p>{{ endif }}
              </div>
            </li>
            <li class="large-4 medium-6 small-12 columns item1">
              <div class="caja_borde">
                <h3>Estado de Pedido</h3>
                <p>Estado:  {{ if order_status }}{{ order_status.value }}{{ else }}Solicitud de Pedido Enviada{{ endif }}</p>  
                <br><br>
                <h3>
                  <b>Envio: </b>{{ price_ship }}
                </h3>
                <h3 style="margin-bottom: 6px;">
                  <b>Total: </b>{{ price_total }}
                </h3>
              </div>
            </li>
            <li class="large-12 medium-12 small-12 columns item1 mtop10">
              <h3><?php echo lang('firesale:orders:label_products'); ?></h3>
            </li>
            {{ items }}
            <li class="large-6 medium-6 small-12 columns item1">
              <div class="caja_borde">
                <div class="item_left">
                    <a href="{{ if modifiers:0:parent }}{{ firesale:url route='product' id=modifiers:0:parent }}{{ else }}{{ firesale:url route='product' id=id }}{{ endif }}">
                      {{ if image }}
                       <img src="{{ url:site uri='files/thumb' }}/{{ image }}/200/200/fit" alt="{{ title }}" />
                      {{ else }}
                        {{ theme:image file="no-image.png" alt=title }}
                      {{ endif }}
                    </a>
                </div>
                <div class="text_wislist">
                <h3>{{ name }}</h3>
                <p><b>Precio Por Unidad:</b> {{ price_formatted }}</p>
                <p><b>Cantidad:</b> {{ qty }}</p>
                <!--{{ if options }}
                <p>
                <h3>Opciones</h3>
                {{ options }}
                <b>{{ title }}: </b>{{ value }}
                {{ /options }}
                </p>
                {{ endif }}-->
        {{ if modifiers }}
          <p>
            <h3>Modificadores</h3>
            {{ modifiers }}
              <b>{{ title }}: </b>{{ var_title }}<br/>
            {{ /modifiers }}
          </p>
        {{ endif }}
                {{ if download:filename }}
                  <a href="{{ url:site }}uploads/default/files/{{ download:filename }}" target="_blank" class="btn_gris">Descargar Archivo</a>
                  <div class="clear"></div>
                {{ endif }}
                </div>
              </div>
            </li>
            {{ /items }}
          </ul>
      </section>
  </section>
</section>


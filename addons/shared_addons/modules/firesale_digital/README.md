# FireSale Digital #
Easily add digital products and downloads to your FireSale/PyroCMS store. You can limit the number of times a user can download your product and also set expiry dates.

With a custom downloads area, your users can see their available downloads, how many downloads they have remaining and when they will expire.
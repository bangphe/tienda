
    <?php echo form_open($this->uri->uri_string(), 'class="crud"'); ?>

        <section class="title">
            <h4><?php echo lang('firesale:shortcuts:add_stream'); ?></h4>
        </section>

        <section class="item form_inputs">
            <div class="content">

                <div class="form_inputs">
                	<div class="inline-form">
                        <fieldset>
                            <ul>
                                <li class="even">
                                    <label for="name">
                                        <?php echo lang('firesale:label_streamname'); ?> <span>*</span>
                                    </label>
                                    <div class="input"><input type="text" name="name" id="name" value="<?php echo set_value('name'); ?>" /></div>
                                </li>
                                <li>
                                    <label for="slug">
                                        <?php echo lang('firesale:label_streamslug'); ?> <span>*</span>
                                        <small><?php echo lang('firesale:label_streamprefix'); ?></small>
                                    </label>
                                    <div class="input"><input type="text" name="slug" id="slug" value="<?php echo set_value('slug'); ?>" /></div>
                                </li>
                            </ul>
                        </fieldset>
                    </div>
                </div>

                <div class="buttons">
                    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'btn red') )); ?>
                </div>

            </div>
        </section>

    <?php echo form_close(); ?>

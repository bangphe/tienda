    
    <input type="hidden" name="list_id" value="<?php echo $id; ?>" />

    <?php if ( ! empty($items) ): ?>
		<div class="scroll-table">
            <table class="items">
                <thead>
                    <tr>
                        <th style="width: 15px"><center><input type="checkbox" name="action_to_all" value="" class="check-all" /></center></th>
                        <th style="width: 32px"></th>
                        <th><?php echo lang('firesale:label_title'); ?></th>
                        <th><?php echo lang('firesale:label_quantity'); ?></th>
                        <th><?php echo lang('firesale:label_price'); ?></th>
                        <th><?php echo lang('firesale:label_price_total'); ?></th>
                        <th style="width: 15px">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                foreach ( $items as $item ): ?>
                    <tr>
                        <td><center><input type="checkbox" name="action_to[]" value="<?php echo $item['id']; ?>" /></center></td>
                        <td><center><img src="<?php echo ( $item['image'] != FALSE ? site_url('files/thumb/' . $item['image'] . '/32/32') : '' ); ?>" alt="Product Image" /></center></td>
                        <td><?php echo $item['title']; ?></td>
                        <td>
                        	<?php echo $item['quantity']; ?>
                            <!--<div class="increment">
                                <a href="#" class="btn blue minus">-</a>
                                <input type="text" name="quantity[<?php echo $item['id']; ?>]" value="<?php echo $item['quantity']; ?>" />
                                <a href="#" class="btn blue plus">+</a>
                            </div>-->
                        </td>
                        <td><?php echo $item['price_formatted']; ?></td>
                        <td><?php echo $item['price_total']; ?></td>
                        <td><a href="<?php echo site_url('admin/firesale_wishlist/delete_items/'.$item['id'].'/'.$id); ?>" class="btn red">Eliminar</a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>

    <?php else: ?>
        <div class="no_data">No Items Found</div>
    <?php endif; ?>

    <div class="item_add">
        <a href="<?php echo site_url('admin/firesale_wishlist/add_items/'.$id); ?>" class="btn green modal"><?php echo lang('firesale:wish:item:add:short'); ?></a>
        <!--<button type="submit" name="btnAction" value="update" class="btn blue"><?php echo lang('firesale:wish:item:edit:short'); ?></button>
        <button type="submit" name="btnAction" value="delete" class="btn red"><?php echo lang('firesale:wish:item:delete:short'); ?></button>-->
    </div>

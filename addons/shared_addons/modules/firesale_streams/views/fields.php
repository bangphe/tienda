    <section class="title">
        <h4><?php echo sprintf(lang('firesale:label_fieldsin'), $stream_name); ?></h4>
    </section>

    <section class="item">
        <div class="content">
        <?php if ( ! empty($fields)): ?>
            <?php echo form_open('admin/firesale_streams/' . $this->uri->segment(4) . '/delete'); ?>
            	<div class="scroll-table">
                    <table id="streams_fields">
                        <thead>
                            <tr>
                                <th style="width: 25px"></th>
                                <th style="width: 15px"><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
                                <th><?php echo lang('firesale:label_name'); ?></th>
                                <th><?php echo lang('firesale:label_slug'); ?></th>
                                <th><?php echo lang('firesale:label_type'); ?></th>
                                <th><?php echo lang('firesale:label_required'); ?></th>
                                <th><?php echo lang('firesale:label_unique'); ?></th>
                                <th style="width: 125px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($fields as $field): ?>
                                <tr id="field_<?php echo $field->field_id; ?>">
                                    <td><span class="mover"></span></td>
                                    <td><input type="checkbox" name="action_to[]" value="<?php echo $field->field_slug; ?>" /></td>
                                    <td><?php echo lang(substr($field->field_name, 5)) ? lang(substr($field->field_name, 5)) : $field->field_name; ?></td>
                                    <td><?php echo $field->field_slug; ?></td>
                                    <td><?php echo $field->field_type; ?></td>
                                    <td><?php echo ucfirst($field->is_required) ?></td>
                                    <td><?php echo ucfirst($field->is_unique) ?></td>
                                    <td>
                                        <?php echo anchor('admin/firesale_streams/fields/' . $field->stream_id . '/edit/' . $field->assign_id, lang('global:edit'), 'class="btn blue small"'); ?>
                                        <?php echo anchor('admin/firesale_streams/fields/' . $field->stream_id . '/delete/' . $field->field_slug, lang('global:delete'), array('class' => 'btn red small')); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
				</div>
                <div class="table_action_buttons">
                    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('btn red') )); ?>
                </div>
            <?php echo form_close(); ?>
            <?php else: ?>
                <div class="no_data"><?php echo lang('firesale:streams:no_fields'); ?></div>
            <?php endif;?>
        </div>
    </section>

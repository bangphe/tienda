<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {

    public $section = 'streams';

    public function index() {
        $this->db->where("LEFT(stream_slug, 9) = 'firesale_'", NULL, FALSE);
        $streams = $this->db->get('data_streams');

        $data['streams'] = $streams->result_array();

        foreach ($data['streams'] as &$stream) {
            // Decode the language key, if any.
            if (substr($stream['stream_name'], 0, 5) == 'lang:') {
                $stream['stream_name'] = lang(substr($stream['stream_name'], 5));
            }
        }

        $this->template->build('streams', $data);
    }

    public function create() {
        $this->form_validation->set_rules('name', 'stream name', 'required')
                ->set_rules('slug', 'stream slug', 'required|is_unique[data_streams.stream_slug]');

        if ($this->form_validation->run()) {
            $data = $this->input->post();

            if (substr($data['slug'], 0, 9) != 'firesale_') {
                $data['slug'] = 'firesale_' . $data['slug'];
            }

            $add = $this->streams->streams->add_stream($data['name'], $data['slug'], $data['slug']);

            if ($add) {
                $this->session->set_flashdata('success', 'Stream created successfully');
            } else {
                $this->session->set_flashdata('error', 'Unable to create stream');
            }

            redirect('admin/firesale_streams');
        } else {
            $this->template->append_js('module::fs_streams.js')->build('form');
        }
    }

    public function delete($namespace = NULL, $redirect = TRUE) {
        if (is_null($namespace) AND $this->input->post('action_to')) {
            foreach ($this->input->post('action_to') as $namespace) {
                $this->delete($namespace, FALSE);
            }

            $this->session->set_flashdata('success', 'The selected streams were deleted successfully');
        } elseif (substr($namespace, 0, 9) == 'firesale_') {
            $this->streams->utilities->remove_namespace($namespace);
            $this->session->set_flashdata('success', 'Stream deleted successfully');
        }

        if ($redirect)
            redirect('admin/firesale_streams');
    }

    public function fields($stream = NULL, $action = NULL, $param = NULL) {
        if (!is_null($stream)) {
            $stream = $this->db->get_where('data_streams', array(
                        'id' => $stream
                    ))->row();

            if (is_null($action)) {
                $data = $stream;
                $data->fields = $this->streams->streams->get_assignments($stream->stream_slug, $stream->stream_namespace);

                if (!empty($data->fields)) {
                    foreach ($data->fields as &$field) {

                        // Check for language
                        if (substr($field->field_name, 0, 5) == 'lang:') {
                            $field->field_name = lang(substr($field->field_name, 5)) or $field->field_name;
                        }

                        // Lets get the real field name
                        $field->field_type = lang('streams:' . $field->field_type . '.name') ? lang('streams:' . $field->field_type . '.name') : $field->field_type;
                    }
                }

                $this->template->append_css('module::fs_streams.css')
                        ->append_js('module::fs_streams.js')
                        ->build('fields', $data);
            } elseif ($action == 'edit' AND !is_null($param)) {
                $this->streams->cp->field_form($stream->stream_slug, $stream->stream_namespace, 'edit', 'admin/firesale_streams/fields/' . $stream->id, $param, array(), TRUE, array(
                    'title' => lang('firesale:title:edit_field')
                ));
            } elseif ($action == 'delete') {
                $delete = $this->streams->fields->delete_field($param, $stream->stream_namespace);

                redirect('admin/firesale_streams/fields/' . $stream->id);
            }
        } else {
            redirect('admin/firesale_streams');
        }
    }

    public function add_field($stream = NULL) {
        if (!is_null($stream)) {
            $stream = $this->db->get_where('data_streams', array(
                        'id' => $stream
                    ))->row();

            $this->streams->cp->field_form($stream->stream_slug, $stream->stream_namespace, 'new', 'admin/firesale_streams', NULL, array(), TRUE, array(
                'title' => lang('firesale:title:add_field')
            ));
        }
    }

    public function ajax_order() {
        if ($this->input->is_ajax_request()) {

            $order = explode(',', $this->input->post('order'));

            $i = 1;

            foreach ($order as $field) {
                $this->db
                        ->where('id', str_replace('field_', '', $field))
                        ->update('data_field_assignments', array('sort_order' => $i));

                $i++;
            }

            exit('ok');
        }

        show_404();
    }

}

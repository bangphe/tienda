  <section class="marco bg_pasos_comp ">
        {{ theme:partial name="account_nav" }}
        <section class="large-9 medium-9 small-12 columns item2">
            <section class="cont_cat_perfil">
                <div class="tittles">
                    <h1>{{ list.title }}</h1>  
                </div>
                 {{ if list.description }}
                    <h3>{{ list.description }}</h3>
                {{ endif }}
                <div class="row">
                    {{ if list.items }}
                    <ul class="cajasdir animated fadeInUp">
                        {{ list.items }}
                       <li class="large-6 medium-6 small-12 columns item1">
                            <div class="caja_borde">
                                <span class="delete_deseo"><a href="{{ firesale:url route='wishlist' }}/remove/{{ parent }}/{{ id }}"><i class="fa fa-times"></i></a></span>
                                <div class="item_left">
                                    <a href="{{ firesale:url route='product' id=id }}">
                                      {{ if image }}
                                        <img src="{{ url:site uri='files/thumb' }}/{{ image }}/200/200/fit" alt="{{ title }}" />
                                      {{ else }}
                                        {{ theme:image file="no-image.png" alt=title }}
                                      {{ endif }}
                                    </a>
                                </div>
                                <div class="text_wislist">
                                    <h3>{{ title }}</h3>
                                    <!--<div class="wishlist-quantity">
                                        Cantidad:
                                        <span>{{ helper:number_format value=quantity }}</span>
                                        <a class="btn minus-quantity" href="{{ firesale:url route='wishlist' }}/insert/{{ parent }}/{{ id }}/{{ minus }}">-</a>
                                        <a class="btn plus-quantity" href="{{ firesale:url route='wishlist' }}/insert/{{ parent }}/{{ id }}/{{ plus }}">+</a>
                                    </div>-->
                                    <!-- <p>Cantidad: {{ helper:number_format value=quantity }}</p> -->
                                    <small><b style="color:red">Importante:</b> Es posible que el precio varié según las características del producto.</small>
                                    <p>Precio Por Unidad: {{ price_formatted }}</p>
                                    <div class="modal_all">
                                        <a href="#modal_preview" data-href="{{url:base}}product/{{slug}}" id="quick_view" data-effect="mfp-3d-unfold">
                                            <div class="btn btn_gris">Vista rápida</div>
                                        </a>
                                    </div>
                                   <!--  <a class="btn btn_gris" href="{{ firesale:url route='cart' }}/insert/{{ id }}/{{ quantity }}">Añadir al carrito</a> -->
                                </div>
                            </div>
                       </li>
                       {{ /list.items }}
                   </ul>    
                    {{ else }}
                    <div class="alert notice"><?php echo sprintf(lang('firesale:wish:item:none'), '{{ firesale:url route="category" id="1" }}'); ?></div>
                    {{ endif }}
                    <a class="btn btn_gris" href="{{ firesale:url route='wishlist' }}">Volver</a>
                </div>
                   
            </section>
        </section>

</section>


FireSale SEO
============

* Website: http://www.getfiresale.org
* Documentation: http://www.getfiresale.org/documentation
* License: Included with release
* Version: 1.2.0

Search Engine Optimisation is a key part of any store and with this module you get just that with enable-and-use ease, simply enable the module and everything else will appear as and where it should for you to fill in and nothing more. The module will automatically detect your new page title, description and keywords and fill them in on the front-end.

### Installation

1. Create a new folder in your chosen addons directory named "firesale_seo"
2. Drop in the files included with this release
3. Navigate to your control panel and goto add-ons
4. Enable the FireSale SEO module
5. New fields are available on product creation to fill in

### Usage

As stated as soon as the new module is enabled a new tab is added to products and categories named "Metadata", this gives you three new fields with a fitting UI that makes the job of adding data easy and efficient. We've added a handy little guide to your meta description to show your character count as defined by Googles limits to allow you to avoid the "..." on the search page. Once filled in, just save and the front-end will be updated instantly!

### Feedback and Issues

If you have any issues please email (support@getfiresale.org)[support@getfiresale.org] and either report your issue or request access to the GitHub repo and we will consider the issue/request as soon as we can.
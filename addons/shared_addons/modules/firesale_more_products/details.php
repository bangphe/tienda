<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Firesale_More_Products extends Module {

	public $version = '1.1.0';
	public $language_file = 'firesale_more_products/firesale';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('firesale/firesale');
		$this->lang->load($this->language_file);
	}

	public function info()
	{

		$info = array(
			'name' => array(
				'en' => 'More Products',
				'it' => 'Prodotti visti di recente'
			),
			'description' => array(
				'en' => 'Automatic tracking of recently viewed, also bought and related products',
				'it' => 'Tracciamento automatico dei prodotti visti di recente, prodotti comprati e correlati'
			),
			'frontend' 		=> FALSE,
			'backend' 		=> FALSE,
			'author' 		=> 'Jamie Holdroyd'
		);

		return $info;
	}

	
	public function install()
	{

		// Add recently viewed settings
		$viewed = array('slug' => 'firesale_mp_recent', 'title' => lang('firesale:recent:setting'), 'description' => lang('firesale:recent:inst'), 'default' => '20', 'value' => '20', 'type' => 'text', 'options' => '', 'is_required' => 1, 'is_gui' => 1, 'module' => 'firesale');
		$this->db->insert('settings', $viewed);

		// Return
		return TRUE;
	}

	public function uninstall()
	{

		// Remove settings
		$this->db->delete('settings', array('slug' => 'firesale_mp_recent'));

		// Return
		return TRUE;
	}

	public function upgrade($old_version)
	{

		if( $old_version < '1.1.0' )
		{

			// Add recently viewed settings
			$viewed = array('slug' => 'firesale_mp_recent', 'title' => lang('firesale:recent:setting'), 'description' => lang('firesale:recent:inst'), 'default' => '20', 'value' => '20', 'type' => 'text', 'options' => '', 'is_required' => 1, 'is_gui' => 1, 'module' => 'firesale');
			$this->db->insert('settings', $viewed);

		}

		return TRUE;
	}
	
	public function help()
	{
		return "Some Help Stuff";
	}

}
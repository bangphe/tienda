<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Firesale_SEO extends Module
{
    public $version = '1.2.0';
    public $language_file = 'firesale_seo/firesale';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('firesale/firesale');
        $this->lang->load($this->language_file);
    }

    public function info()
    {

        $info = array(
            'name' => array(
                'en' => 'SEO',
                'fr' => 'SEO',
                'it' => 'SEO'
            ),
            'description' => array(
                'en' => 'Search Engine Optimisation for products, categories, etc.',
                'fr' => 'Optimisation du référencement pour les produits, les catégories, etc.',
                'it' => 'Ottimizzazione per i motori di ricerca per prodotti, categorie etc.'
            ),
            'frontend' 		=> false,
            'backend' 		=> false,
            'menu'	   		=> 'FireSale',
            'author' 		=> 'Jamie Holdroyd'
        );

        return $info;
    }

    public function install()
    {

        // Load required items
        $this->load->driver('Streams');

        ###########################
        ## SEO FIELDS - PRODUCTS ##
        ###########################

        // Get stream data
        $products = end($this->streams->streams->get_streams('firesale_products', TRUE, 'firesale_products'));

        // Add fields
        $fields   = array();
        $template = array('namespace' => 'firesale_products', 'assign' => 'firesale_products', 'type' => 'text', 'title_column' => FALSE, 'required' => FALSE, 'unique' => FALSE);
        $fields[] = array('name' => 'lang:firesale:seo:label_title', 'slug' => 'meta_title', 'type' => 'text', 'extra' => array('max_length' => 255), 'unique' => TRUE);
        $fields[] = array('name' => 'lang:firesale:seo:label_desc', 'slug' => 'meta_description', 'type' => 'textarea');
        $fields[] = array('name' => 'lang:firesale:seo:label_keywords', 'slug' => 'meta_keywords', 'type' => 'text');

        // Combine
        foreach( $fields AS &$field ) {
            $field = array_merge($template, $field);
        }

        // Add fields to stream
        $this->streams->fields->add_fields($fields);

        #############################
        ## SEO FIELDS - CATEGORIES ##
        #############################

        // Format
        foreach( $fields AS &$field ) {
            $field['namespace'] = 'firesale_categories';
            $field['assign']    = 'firesale_categories';
        }

        // Add fields to stream
        $this->streams->fields->add_fields($fields);

        // Return
        return TRUE;
    }

    public function uninstall()
    {

        // Load required items
        $this->load->driver('Streams');

        // Drop fields from products
        $this->streams->fields->delete_field('meta_title', 'firesale_products');
        $this->streams->fields->delete_field('meta_description', 'firesale_products');
        $this->streams->fields->delete_field('meta_keywords', 'firesale_products');

        // Drop fields from categories
        $this->streams->fields->delete_field('meta_title', 'firesale_categories');
        $this->streams->fields->delete_field('meta_description', 'firesale_categories');
        $this->streams->fields->delete_field('meta_keywords', 'firesale_categories');

        // Return
        return TRUE;
    }

    public function upgrade($old_version)
    {
        // Your Upgrade Logic
        return TRUE;
    }

    public function help()
    {
        return "Some Help Stuff";
    }

}

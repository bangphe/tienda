<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class front extends Public_Controller {

    public $stream;

    public function __construct() {

        parent::__construct();

        // Load libraries, drivers & models
        $this->load->driver('Streams');
        $this->load->library('streams_core/fields');
        $this->load->model('reviews_m');
        $this->load->model('firesale/categories_m');
        $this->load->model('firesale/products_m');
        $this->load->model('firesale/routes_m');

        // Get the stream
        $this->stream = $this->streams->streams->get_stream('firesale_reviews', 'firesale_reviews');
    }

    public function create($product = NULL) {

        // Get review data
        $review = $this->input->post();
        $product = $this->products_m->get_product($product);

        // Check for post data
        if ($product != FALSE AND $this->input->post('btnAction') == 'create') {

            // Check logged in
            if (isset($this->current_user->id) AND $this->current_user->id > 0) {

                // Ensure user has not reviewed this product
                if (!$this->reviews_m->user_has_reviewed($this->current_user->id, $product['id'])) {

                    // Add extras
                    $review['created_by'] = $this->current_user->id;
                    $review['product'] = $product['id'];

                    // Is moderation required?
                    $review['status'] = ( $this->settings->get('firesale_review_accept', '1') ? 0 : 1 );

                    // Append to post
                    $_POST = $review;

                    // Variables
                    $skip = array('btnAction');
                    $extra = array(
                        'return' => false,
                        'success_message' => lang('firesale:review_add_success'),
                        'error_message' => lang('firesale:review_add_error')
                    );

                    $this->pyrocache->delete_all('reviews_m');
                    $this->pyrocache->delete_all('codeigniter');
					
					$this->session->set_flashdata('error', 'Calificación agregada con éxito, en espera de aprobación');
					
                    // Get the stream fields to update
                    $this->fields->build_form($this->stream, 'new', $review, FALSE, FALSE, $skip, $extra);
					
                } else {
                    // Error flash
                    $this->session->set_flashdata('error', lang('firesale:reviews:previous'));
                }
            } else {
                // Error flash
                $this->session->set_flashdata('error', lang('firesale:reviews:nouser'));
            }

            // Redirect
            redirect($this->routes_m->build_url('product', $product['id']));
        }

        // Disallow direct access
        redirect('');
    }

}

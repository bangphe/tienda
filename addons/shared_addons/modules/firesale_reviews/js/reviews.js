$(function() {

	// Dashboard
	$('a.show-filter').click(function() { $('#filters').slideToggle(1); });
	$('#filter').change(function() { if( $(this).val() > 0 ) { window.location = SITE_URL+'admin/firesale_reviews/index/' + $(this).val(); } });
	
	$('a.update').click(function(e) {
		e.preventDefault();
		var val = $(this).parents('tr').find('select').val();
		$.getJSON($(this).attr('href') + '/' + val, function(response) {
			pyro.add_notification(response.message);
		});
	});

});
<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * FireSale Digital Field Type
 *
 * @package		FireSale
 * @subpackage  Digital Field Type
 * @author		Chris Harvey
 */
class Field_firesale_digital
{
	public $field_type_slug			= 'firesale_digital';
	
	public $db_col_type				= 'varchar';

	public $custom_parameters		= array();

	public $version					= '1.0';

	public $author					= array('name'=>'Chris Harvey', 'url'=>'http://www.getfiresale.org');
	
	public $input_is_file			= true;

	// --------------------------------------------------------------------------

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->config('firesale_digital/config');
	}

	/**
	 * Output form input
	 *
	 * @param	array
	 * @param	array
	 * @return	string
	 */
	public function form_output($params)
	{	
		// Get the file
		if ($params['value'])
		{
			$current_file = $params['value'];
		}
		else
		{
			$current_file = NULL;
		}

		$out = '';
		
		if ($current_file)
		{
			$out .= $this->_output_link($current_file).'<br />';
		}
		
		// Output the actual used value
		if ($params['value'])
		{
			$out .= form_hidden($params['form_slug'], $params['value']);
		}
		else
		{
			$out .= form_hidden($params['form_slug'], 'dummy');
		}

		$options['name'] 	= $params['form_slug'];
		$options['name'] 	= $params['form_slug'].'_file';
		
		return $out .= form_upload($options);
	}

	// --------------------------------------------------------------------------

	/**
	 * Process before saving to database
	 *
	 * @access	public
	 * @param	array
	 * @param	obj
	 * @return	string
	 */
	public function pre_save($input, $field)
	{	
		// Only go through the pre_save upload if there is a file ready to go
		if (isset($_FILES[$field->field_slug.'_file']['name']) && $_FILES[$field->field_slug.'_file']['name'] != '')
		{
			// Do nothing
		}	
		else
		{
			// If we have a file already just return that value
			if (is_numeric($this->CI->input->post($field->field_slug)))
			{
				return $this->CI->input->post($field->field_slug);
			}
			else
			{
				return null;
			}
		}	
		
		// Set upload data
		$upload_config['upload_path'] = realpath(FCPATH . $this->CI->config->item('fs_digital_upload_path'));
		$upload_config['allowed_types'] = '*';

		// Do the upload
		$this->CI->load->library('upload', $upload_config);

		if ( ! $this->CI->upload->do_upload($field->field_slug . '_file'))
		{
			// @todo - languagize
			$this->CI->session->set_flashdata('notice', lang('streams:firesale:upload_errors') . $this->CI->upload->display_errors());	
			
			return null;
		}
		else
		{
			$file = $this->CI->upload->data();
			
			// Return the file name here
			
			// Return the ID
			return $file['file_name'];
		}
	}

	// --------------------------------------------------------------------------

	/**
	 * Process before outputting
	 *
	 * @access	public
	 * @param	array
	 * @return	mixed - null or string
	 */	
	public function pre_output($input, $params)
	{
		if ( ! $input) return null;
		
		if ($db_obj->num_rows() > 0)
		{
			return $this->_output_link($db_obj->row(), false);
		}
	}

	// --------------------------------------------------------------------------

	/**
	 * Process before outputting for the plugin
	 *
	 * This creates an array of data to be merged with the
	 * tag array so relationship data can be called with
	 * a {field.column} syntax
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @param	array
	 * @return	mixed - null or array
	 */
	public function pre_output_plugin($input, $params)
	{
		if ( ! $input) return null;

		$file_data['path']		= realpath(FCPATH . $this->CI->config->item('fs_digital_upload_path') . '/' . $input);
		$file_data['filename']	= $input;
		$file_data['mimetype']	= null;

		return $file_data;
	}

	// --------------------------------------------------------------------------
	
	/**
	 * Output link
	 *
	 * Used mostly for the back end
	 *
	 * @access	private
	 * @param	obj
	 * @return	string
	 */
	private function _output_link($file)
	{
		return '<a href="'.$this->CI->config->item('files:path').$file->filename.'" target="_blank">'.$file->filename.'</a><br />';
	}
}
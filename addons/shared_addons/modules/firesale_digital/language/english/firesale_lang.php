<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['firesale:field:digital'] = 'Digital?';
$lang['firesale:field:download'] = 'Download';
$lang['firesale:field:downloads'] = 'Allowed Downloads';
$lang['firesale:field:expires_after'] = 'Expires After';

$lang['firesale:instructions:digital'] = 'Orders containing only digital items will be automatically set to dispatched.';
$lang['firesale:instructions:download'] = 'The file that the user will have access to on purchase.';
$lang['firesale:instructions:downloads'] = 'The number of times the user can download this file. Blank for unlimited.';
$lang['firesale:instructions:expires_after'] = 'How long the user will be able to download the file after purchase.';

$lang['firesale:interval:hours'] = 'Hours';
$lang['firesale:interval:days'] = 'Days';
$lang['firesale:interval:weeks'] = 'Weeks';
$lang['firesale:interval:months'] = 'Months';
$lang['firesale:interval:years'] = 'Years';

// Tabs
$lang['firesale:tabs:digital'] = 'Digital';
$lang['firesale:digital:title'] = 'Downloads';

// Labels
$lang['firesale:label_remdls'] = 'Remaining Downloads';
$lang['firesale:label_availuntil'] = 'Available Until';
$(function() {

    pyro.generate_slug($('#name'), $('#slug'), '-');

	$('#streams_fields tbody').sortable({
		handle: 'span.mover',
		update: function() {
			var order = [];
			$('#streams_fields tbody > tr').removeClass('alt');
			$('#streams_fields tbody > tr:nth-child(even)').addClass('alt');
			$('#streams_fields tbody > tr').each(function() { order.push(this.id); });
			order = order.join(',');
			$.post(SITE_URL + 'admin/firesale_streams/ajax_order', { order: order });
		}
	});

});
<?php

    // General
    $lang['firesale:recent:title'] = 'Productos Vistos Recientemente';

    // Settings
    $lang['firesale:recent:setting'] = 'Máximo Productos Recientes';
    $lang['firesale:recent:inst']    = 'El número máximo de productos recientes para rastrear';

<section class="cont_home m_b_20">   

      <!--Miga de pan-->
      <div class="bg_miga">
        <ul class="marco miga_pan">
          <a href="{{url:base}}"><i class="fa fa-angle-double-right"></i>Inicio</a>
          <a href="#"><i class="fa fa-angle-double-right"></i>Mi Cuenta</a>
        </ul>
          <div class="clear"></div>
      </div>    
      <!--Fin Miga de pan-->

      <div class="clear"></div>
      
      <section class="marco bg_pasos_comp">
          <!--bg Carrito-->    
          {{ theme:partial name="account_nav" }}
          <!--Fin bg Carrito-->      

          <!--Campos Perfil-->
           <section class="large-9 medium-9 small-12 columns item2">
            <section class="cont_cat_perfil" id="perfil1">
                <div class=" relarive elem_blancos animated fadeInUp">
                     <!--Titulo-->
                      <div class="tittles">
                        <h1>MI PERFIL</h1>
                        <a href="{{ url:site}}edit-profile" class="btn_edit btn_gris">
                          <i class="fa fa-pencil"></i> Editar 
                        </a>
                      </div>
                     <!--Fin Titulo--> 
                     <div class="large-12 medium-12 small-12 columns items_perfil">
                        <h3>Mi Cuenta</h3>
                        <div class="cont_user_img">{{ theme:image file="avatar.png" alt="avatar" }}</div>
                        {{ user:profile_fields user_id= _user:id }}
                          {{ if user:id === _user:id or user:group === 'admin' or slug != 'email' and slug != 'first_name' and slug != 'last_name' and slug != 'username' and value }}                           
                            <fieldset class="large-6 medium-6 small-12 columns"> 
                            <label>{{ name }}</label>                        
                            <input type="text" name="Camilo" id="name_per" placeholder="{{ if value }}{{ value }}{{ else }}&nbsp;{{ endif }}" disabled />                                                                       
                            </fieldset> 
                          {{ endif }}
                  
                        {{ /user:profile_fields }}

                     </div> 

                </div>  

                  <div class="clear"></div>

            </section>
        </section>

   
          <!--Fin Campos Perfil-->
      </section>


  </section>



      

      
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Module_Home extends Module {

    public $version = '1.0';

    public function info() {
        return array(
            'name' => array(
                'es' => 'Home',
                'en' => 'Home'
            ),
            'description' => array(
                'es' => 'Home Basico de la Tienda Online',
                'en' => 'Basic Home'
            ),
            'frontend' => true,
            'backend' => true,
            'menu' => 'content',
        );
    }

    public function install() 
    {
    	// creamos la base de datos del banner
        $this->dbforge->drop_table('home_banner');

        // creamos un array con los campos de la base de datos
        $banner = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true
            ),
            'image' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
            'text' => array(
                'type' => 'TEXT',
                'constraint' => '0',
                'null' => true
            ),
            'link' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
        );

        // creamos la llave primaria
        $this->dbforge->add_field($banner);
        $this->dbforge->add_key('id', true);

        // creamos la carpeta en la ruta especificada
        $this->dbforge->create_table('home_banner') AND is_dir($this->upload_path . 'home_banner/') OR @mkdir($this->upload_path . 'home_banner/', 0777, TRUE);

        // creamos la base de datos del banner 2
        $this->dbforge->drop_table('home_banner_two');

        // creamos un array con los campos de la base de datos
        $banner = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true
            ),
            'image' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
            'text' => array(
                'type' => 'TEXT',
                'constraint' => '0',
                'null' => true
            ),
            'link' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
        );

        // creamos la llave primaria
        $this->dbforge->add_field($banner);
        $this->dbforge->add_key('id', true);

        // creamos la carpeta en la ruta especificada
        $this->dbforge->create_table('home_banner_two') AND is_dir($this->upload_path . 'home_banner_two/') OR @mkdir($this->upload_path . 'home_banner_two/', 0777, TRUE);

        // creamos la base de datos de el video e imagen adicionales
        $this->dbforge->drop_table('home_others');

        // creamos un array con los campos de la base de datos
        $home_others = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true
            ),
            'content' => array(
                'type' => 'VARCHAR',
                'constraint' => '455'
            ),
            'type' => array(
                'type' => 'INT',
                'constraint' => '2',
            ),
            'text' => array(
                'type' => 'TEXT',
                'constraint' => '0',
                'null' => true
            ),
            'link' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
        );

        // creamos la llave primaria
        $this->dbforge->add_field($home_others);
        $this->dbforge->add_key('id', true);

        // creamos la carpeta en la ruta especificada
        $this->dbforge->create_table('home_others') AND is_dir($this->upload_path . 'home_others/') OR @mkdir($this->upload_path . 'home_banner_two/', 0777, TRUE);



        /* Este instalador no sirve nuevamente para el modulo */

        $this->dbforge->drop_table('home_pautas');

        $field = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true
            ),
            'imagen' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            ),
            'link' => array(
                'type' => 'VARCHAR',
                'constraint' => '455',
                'null' => true
            )
        );

        $this->dbforge->add_field($field);
        $this->dbforge->add_key('id', true);
		
		$this->dbforge->create_table('home_pautas') AND is_dir($this->upload_path . 'home_pautas/') OR @mkdir($this->upload_path . 'home_pautas/', 0777, TRUE);

        /*$dir = UPLOADSFOLDER . 'home/';

        if (!is_dir($dir))
        {
            @mkdir($dir, 0777);
        }*/

        $this->dbforge->drop_table('home_slider');

        $field = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => true
            ),
            'producto_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => true
            ),
            'orden' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => true
            )
        );

        $this->dbforge->add_field($field);
        $this->dbforge->add_key('id', true);

        if (!$this->dbforge->create_table('home_slider')) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        $this->dbforge->drop_table('home_pautas');
		$this->dbforge->drop_table('home_slider');
		$this->dbforge->drop_table('home_banner');
        @rmdir($this->upload_path . 'home_pautas');
		@rmdir($this->upload_path . 'home_banner');
        return true;
    }

    public function upgrade($old_version) {
        return true;
    }

    public function help() {
        return "Pagina inicial del sitio desarrollada a la medida.";
    }

}
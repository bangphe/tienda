<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @author  Brayan Acebo
 */

class reportesModel extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    // funciones para manipular los array de los graficos
	public function generarArrayBarrasSimple($datosSql, $divisor)
	{
		$datosArray = array();
		if(!empty($datosSql))
		{
			foreach ($datosSql as $key => $value)
			{
				if(!isset($datosArray[$value['codcategoria']]))
				{
					$datosArray[$value['codcategoria']]['valor'] = 0;
					$datosArray[$value['codcategoria']]['valor'] = '';
				}
				$datosArray[$value['codcategoria']]['valor'] += $value['valor'] / ($divisor > 0 ? $divisor : 1); // el valor estaba dividido segun lo asignado en la base de datos
				$datosArray[$value['codcategoria']]['nomcategoria'] = $value['nomcategoria'];
			}
		}
		return $datosArray;
	}
	
	public function generarArrayMultiBarChar($datosSql, $divisor)
	{
		// reiniciamos el contador
		$i = 0;
		$maxOrdenSerie = 0;
		$sumar = FALSE;
		$datosArray = array();  // vaciamos el array
		$arraySeries = array();
		if(!empty($datosSql))
		{
			// buscamos el valor mas alto en el campo ordenserie
			foreach ($datosSql AS $datoArray)
			{
				$maxOrdenSerie = max($maxOrdenSerie, $datoArray['ordenserie']);
				if(!isset($arraySeries[$datoArray['ordenserie']]))
				{
					$arraySeries[$datoArray['ordenserie']] = $datoArray['nomserie'];
				}
			}
			
			foreach ($datosSql AS $arrayValores)
			{
				if(!isset($datosArray[$arrayValores['codcategoria']]))
				{
					for ($j = 1; $j <= $maxOrdenSerie; $j++)
					{
						$datosArray[$arrayValores['codcategoria']]['valores'][$j]['valor'] = 0;
						$datosArray[$arrayValores['codcategoria']]['valores'][$j]['nomserie'] = $arraySeries[$j];
					}
					$datosArray[$arrayValores['codcategoria']]['orden'] = $i;
					$datosArray[$arrayValores['codcategoria']]['nomcategoria'] = $arrayValores['nomcategoria'];
					$sumar = TRUE;
				}
				$datosArray[$arrayValores['codcategoria']]['valores'][$arrayValores['ordenserie']]['valor'] = $arrayValores['valor'] / ($divisor > 0 ? $divisor : 1);
				if($sumar)
				{
					$i++;
					$sumar = FALSE;
				}
			}
		}
		$datosGrafica['datosArray'] = $datosArray;
		$datosGrafica['arraySeries'] = $arraySeries;
		
		return $datosGrafica;
	}
	
	// funciones para manipular los array de las tablas
	public function tablaGraficoArray($datosSql, $multiSerie, $divisor)
	{
		$datosArray = array();
		$i = 0;
		$maxOrdenSerie = 0;
		$sumar = FALSE;
		$arraySeries = array();
		if(!empty($datosSql))
		{
			switch ($multiSerie)
			{
				case '0':
					foreach ($datosSql AS $fila => $arrayValores)
					{
						$datosArray[$fila]['codcategoria'] = $arrayValores['codcategoria'];
						$datosArray[$fila]['nomcategoria'] = $arrayValores['nomcategoria'];
						$datosArray[$fila]['valor'] = $arrayValores['valor'] / ($divisor > 0 ? $divisor : 1);
						$datosArray[$fila]['orden'] = $i;
						$i++;
					}
					break;
				
				case '1':
					foreach ($datosSql AS $datoArray)
					{
						$maxOrdenSerie = max($maxOrdenSerie, $datoArray['ordenserie']);
						if(!isset($arraySeries[$datoArray['ordenserie']]))
						{
							$arraySeries[$datoArray['ordenserie']] = $datoArray['nomserie'];
						}
					}
					
					foreach ($datosSql AS $arrayValores)
					{
						if(!isset($datosArray[$arrayValores['codcategoria']]))
						{
							for ($j = 1; $j <= $maxOrdenSerie; $j++)
							{
								$datosArray[$arrayValores['codcategoria']]['valores'][$j]['valor'] = 0;
								$datosArray[$arrayValores['codcategoria']]['valores'][$j]['nomserie'] = $arraySeries[$j];
							}
							$datosArray[$arrayValores['codcategoria']]['orden'] = $i;
							$datosArray[$arrayValores['codcategoria']]['nomcategoria'] = $arrayValores['nomcategoria'];
							$sumar = TRUE;
						}
						$datosArray[$arrayValores['codcategoria']]['valores'][$arrayValores['ordenserie']]['valor'] = $arrayValores['valor'] / ($divisor > 0 ? $divisor : 1);
						if($sumar)
						{
							$i++;
							$sumar = FALSE;
						}
					}
					break;
			}
		}
		return $datosArray;
	}
	
	public function tablaSimpleMultiSerie($datosTabla, $datosGrafica, $tituloEjeX, $tituloEjeY, $tipoTabla)
	{
		$nuevaTabla = array();
		$titulos = array();
		$datosNuevaTabla = array();
		$valoresSerie = array();
		$maxSerie = 0;
		
		// creamos los titulos
		array_push($titulos, "Código");
		array_push($titulos, $tituloEjeX);
		
		foreach($datosGrafica AS $nombre => $datos)
		{
			if($nombre != 'datosArray')
			{
				foreach($datos AS $ordenSerie => $datos2)
				{
					array_push($titulos, $datos2);
					$valoresSerie[$ordenSerie] = 0;
					$maxSerie = max($maxSerie, $ordenSerie);
				}
			}
		}
		array_push($titulos, "Total");
		if($tipoTabla == 2)
		{
			array_push($titulos, "%");
		}
		
		$total = 0;
		foreach($datosTabla AS $row => $datos)
		{
			$totalFila = 0;
			foreach ($datos['valores'] as $ordenSerie => $datos2)
			{
				$totalFila += $datos2['valor'];
			}
			$total += $totalFila;
		}
		
		// llenamos los datos
		foreach($datosTabla AS $row => $datos)
		{
			$totalFila = 0;
			$arrayFila = array();
			array_push($arrayFila, $row);
			array_push($arrayFila, $datos['nomcategoria']);
			foreach($datos['valores'] AS $ordenSerie => $datos2)
			{
				array_push($arrayFila, $datos2['valor']);
				$totalFila += $datos2['valor'];
				$valoresSerie[$ordenSerie] += $datos2['valor'];
			}
			array_push($arrayFila, $totalFila);
			if($tipoTabla == 2)
			{
				array_push($arrayFila, (($totalFila)/total)*100);
			}
			array_push($datosNuevaTabla, $arrayFila);
		}
		
		// colocamos los totales
		$arrayFila = array();
		array_push($arrayFila, "");
		array_push($arrayFila, "Total");
		
		foreach( $valoresSerie AS  $ordenSerie => $datos2 )
		{
			if(!empty($datos2))
			{
				array_push($arrayFila, $datos2);
			}
		}
		array_push($datosNuevaTabla, $arrayFila);
		$nuevaTabla['titulos'] = $titulos;
		$nuevaTabla['datos'] = $datosNuevaTabla;
		return $nuevaTabla;
	}

	public function tablaSimpleUnaSerie($datosTabla, $datosGrafica, $tituloEjeX, $tituloEjeY, $tipoTabla)
	{
		$nuevaTabla = array();
		$titulos = array();
		$datosNuevaTabla = array();
		
		// llenamos los titulos
		$titulos[0] = 'codigo';
		$titulos[1] = $tituloEjeX;
		$titulos[2] = $tituloEjeY;
		if($tipoTabla == 2)
		{
			$titulos[3] = '%';
		}
		
		// sacamos el valor total desde antes para poder sacar el valor de participación
		$total = 0;
        for($i=0; $i < count($datosTabla); $i++)
		{
			$total += $datosTabla[$i]["valor"];
		}
		
		// llenamos los datos
		for($i=0; $i < count($datosTabla); $i++)
		{
			$arrayFila = array();
			array_push($arrayFila, $datosTabla[$i]["codcategoria"]);
			array_push($arrayFila, $datosTabla[$i]["nomcategoria"]);
			array_push($arrayFila, $datosTabla[$i]["valor"]);
			if($tipoTabla == 2)
			{
				array_push($arrayFila, ($datosTabla[$i]["valor"]/total)*100);
			}
			array_push($datosNuevaTabla, $arrayFila);
		}
		
		// colocamos los totales
		$arrayFila = array();
		array_push($arrayFila, "");
		array_push($arrayFila, "Total");
		array_push($arrayFila, $total);
		if($tipoTabla == 2)
		{
			array_push($arrayFila, "100%");
		}
		array_push($datosNuevaTabla, $arrayFila);
		$nuevaTabla['titulos'] = $titulos;
		$nuevaTabla['datos'] = $datosNuevaTabla;
		return $nuevaTabla;
	}
	
	public function tablaVariacionesMultiSerie($datosTabla, $datosGrafica, $tituloEjeX, $tituloEjeY, $tipoTabla)
	{
		$nuevaTabla = array();
		$titulos = array();
		$datosNuevaTabla = array();
		$valoresSerie = array();
		$maxSerie = 0;
		
		// creamos los titulos
		array_push($titulos, "Código");
		array_push($titulos, $tituloEjeX);
		
		foreach($datosGrafica AS $nombre => $datos)
		{
			if($nombre != 'datosArray')
			{
				$i=1;
				foreach($datos AS $ordenSerie => $datos2)
				{
					array_push($titulos, $datos2);
					$valoresSerie[$ordenSerie] = 0;
					$maxSerie = max($maxSerie, $ordenSerie);
					if($i >= 4)
			        {
			        	array_push($titulos, "VARIACIÓN");
						array_push($titulos, "%");
			        }
			        $i++;
				}
			}
		}
		
		// llenamos los datos
		foreach($datosTabla AS $row => $datos)
		{
			$valorAnt = 0;
			$i = 1;
			$arrayFila = array();
			foreach($datos['valores'] AS $ordenSerie => $datos2)
			{
				array_push($arrayFila, $datos2['valor']);
				$valoresSerie[$ordenSerie] += $datos2['valor'];
				if($i >= 2)
				{
					array_push($arrayFila, $datos2['valor']-$valorAnt);
					array_push($arrayFila, (((($datos2['valor'])/valorAnt)-1)*100).'%');
				}
				$valorAnt = $datos2['valor'];
				$i++;
			}
			array_push($datosNuevaTabla, $arrayFila);
		}
		
		// colocamos los totales
		$arrayFila = array();
		array_push($arrayFila, "");
		array_push($arrayFila, "Total");
		
		$i = 1;
		$valorAnt = 0;
		foreach( $valoresSerie AS  $ordenSerie => $datos2 )
		{
			if(!empty($datos2))
			{
				array_push($arrayFila, $datos2);
				if($i >= 2)
				{
					array_push($arrayFila, $datos2-$valorAnt);
					array_push($arrayFila, (((($datos2)/$valorAnt)-1)*100));
				}
				$valorAnt = $datos2;
				$i++;
			}
		}
		array_push($datosNuevaTabla, $arrayFila);
		$nuevaTabla['titulos'] = $titulos;
		$nuevaTabla['datos'] = $datosNuevaTabla;
		return $nuevaTabla;
	}
	
	public function tablaCumplimientoMultiSerie($datosTabla, $datosGrafica, $tituloEjeX, $tituloEjeY, $tipoTabla)
	{
		$nuevaTabla = array();
		$titulos = array();
		$datosNuevaTabla = array();
		$valoresSerie = array();
		$maxSerie = 0;
		
		// creamos los titulos
		array_push($titulos, "Código");
		array_push($titulos, $tituloEjeX);
		
		foreach($datosGrafica AS $nombre => $datos)
		{
			if($nombre != 'datosArray')
			{
				foreach($datos AS $ordenSerie => $datos2)
				{
					array_push($titulos, $datos2);
					$valoresSerie[$ordenSerie] = 0;
					$maxSerie = max($maxSerie, $ordenSerie);
				}
			}
		}
		array_push($titulos, "Cumplimiento");
		
		// llenamos los datos
		foreach($datosTabla AS $row => $datos)
		{
			$real = 0;
			$presupuesto = 0;
			$primerPaso = true;
			$arrayFila = array();
			array_push($arrayFila, $row);
			array_push($arrayFila, $datos['nomcategoria']);
			foreach($datos['valores'] AS $ordenSerie => $datos2)
			{
				array_push($arrayFila, $datos2['valor']);
				if($primerPaso)
				{
					$presupuesto = $datos2['valor'];
					$primerPaso = false;
				}
				else
				{
					$real = $datos2['valor'];
				}
				$valoresSerie[$ordenSerie] += $datos2['valor'];
			}
			array_push($arrayFila, ($presupuesto > 0 ? ($real/$presupuesto)*100: "0,0").'%');
			array_push($datosNuevaTabla, $arrayFila);
		}
		
		// colocamos los totales
		$arrayFila = array();
		array_push($arrayFila, "");
		array_push($arrayFila, "Total");
		
		$real = 0;
		$presupuesto = 0;
		$primerPaso = true;
		foreach( $valoresSerie AS  $ordenSerie => $datos2 )
		{
			if(!empty($datos2))
			{
				array_push($arrayFila, $datos2);
				if($primerPaso)
				{
					$presupuesto = $datos2;
					$primerPaso = false;
				}
				else
				{
					$real = $datos2;
				}
			}
		}
		array_push($arrayFila, ($presupuesto > 0 ? ($real/$presupuesto)*100: "0,0").'%');
		array_push($datosNuevaTabla, $arrayFila);
		$nuevaTabla['titulos'] = $titulos;
		$nuevaTabla['datos'] = $datosNuevaTabla;
		return $nuevaTabla;
	}
	
	function reemplazarAnoString($string, $fechaWidget)
	{
		// creamos variables de fechas para las consultas
		$arrayDatos = array();
		$fechaactual = ($fechaWidget == null ? $this->session->userdata('fechaactual') : $fechaWidget);
		list($anoactual, $mesactual, $diaactual) = explode('-', $fechaactual);
		
		$anoanterior = $anoactual - 1;
		$mesanterior = $mesactual - 1;
		$messiguiente = $mesactual + 1;
		
		$arrayDatos['fechaDinam'] = (strpos($string, "[anoactual") !== false ? true : false);
		
		
		$string2 = '';
		if(strpos($string, "[anoactual-") !== false)
		{
			if(strpos($string, "[anoactual") !== false)
			{
				$stringPartes = explode("[anoactual", $string);
				foreach ($stringPartes AS $posicion => $datos)
				{
					$resta = substr($datos, 0, 1);
					$string3 = '';
					if($resta == "-")
					{
						$cantidad = substr($datos, 1, 2);  // bcd
						$string3 = strstr($datos, ']');
						$string3 = str_replace("]", '', $string3);
						$string3 = ($anoactual - $cantidad).$string3;
					}
					else 
					{
						$string3 = str_replace("]", $anoactual, $datos);
					}
					$string2 .=  $string3;
				}
			}
		}
		else
		{
			$string2 = str_replace("[anoactual]", $anoactual, $string);
		}
		
		$string = $string2;
		$string = str_replace("[mesactual]", $mesactual, $string);
		$string = str_replace("[diaactual]", $diaactual, $string);
		$string = str_replace("[anoanterior]", $anoanterior, $string);
		$string = str_replace("[mesanterior]", $mesanterior, $string);
		$string = str_replace("[messiguiente]", $messiguiente, $string);
		$arrayDatos['string'] = $string;
		return $arrayDatos;
	}
}

	<ul>
	{{ lists.entries }}
		<li><a href="{{ firesale:url route='wishlist' }}/view/{{ id }}">{{ title }} - {{ helper:number_format value=total }} Item{{ if total != 1 }}s{{ endif }} - worth {{ price }}</a></li>
	{{ /lists.entries }}
	</ul>

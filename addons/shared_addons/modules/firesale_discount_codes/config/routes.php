<?php defined('BASEPATH') or exit('No direct script access allowed');

$route['firesale_discount_codes/admin(/:any)?'] = 'admin$1';
$route['firesale_discount_codes(/:any)?'] = 'front$1';

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* This file is part of FireSale, a PHP based eCommerce system built for
* PyroCMS.
*
* Copyright (c) 2013 Moltin Ltd.
* http://github.com/firesale/firesale
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @package firesale/core
* @author FireSale <support@getfiresale.org>
* @copyright 2013 Moltin Ltd.
* @version dev
* @link http://github.com/firesale/firesale
*
*/

class Imaginamos_m extends MY_Model
{
    /**
     * Loads the parent constructor and gets an
     * instance of CI.
     *
     * @return void
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
    }

    function sqlFormSelect($DB_tabla, $BD_codCampo, $BD_campo, $none, $nom_where = null, $value_where = null)
	{
		$ninguno = array('' => '');
		if($nom_where != null && $value_where != null)
		{
			$this->db->where($nom_where, $value_where);
		}
		$datosSql = $this->db->get($DB_tabla);
		if($datosSql->num_rows() > 0)
		{
			$datosArray = array();
			foreach ($datosSql->result_array() as $row)
			{
				$datosArray[$row[$BD_codCampo]] = $row[$BD_campo];
			}
			if($none)
			{
				$datosArray = $ninguno + $datosArray;
			}
			
			return $datosArray;
		}
		else
		{
			return array();
		}
	}
}

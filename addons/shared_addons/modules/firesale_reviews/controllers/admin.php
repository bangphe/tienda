<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class admin extends Admin_Controller
{
    public $section = "reviews";
    public $perpage = 30;
    public $stream  = NULL;

    public function __construct()
    {

        parent::__construct();

        // Add data
        $this->data = new stdClass();

        // Load libraries
        $this->load->driver('Streams');
        $this->lang->load('firesale_reviews/firesale');
        $this->load->model('firesale/products_m');
        $this->load->model('reviews_m');

        // Get the stream
        $this->stream = $this->streams->streams->get_stream('firesale_reviews', 'firesale_reviews');
    }

    public function index($product = 0, $start = 0)
    {

        // Set query paramaters
        $params	 = array(
                    'stream' 	=> 'firesale_reviews',
                    'namespace'	=> 'firesale_reviews',
                    'limit'		=> $this->perpage,
                    'offset'	=> $start,
                    'order_by'	=> 'created',
                    'sort'		=> 'desc'
                   );

        // Get by category if set
        if( $product > 0 ) {
            $params['where'] = 'product=' . $product;
        }

        // Get entries
        $reviews = $this->streams->entries->get_entries($params);

        // Get fields
        $tmp    = $this->reviews_m->get_fields();
        $fields = array();

        foreach( $tmp as &$field ) {
            $fields[$field->field_name] = $field->field_slug;
        }

        // Assign Variables
        $this->data->products   = $this->products_m->build_dropdown();
        $this->data->fields     = $fields;
        $this->data->product	= $product;
        $this->data->reviews 	= $reviews['entries'];
        $this->data->count		= $reviews['total'];
        $this->data->pagination = $reviews['pagination'];

        // Build page
        $this->template->append_css('module::reviews.css')
                       ->append_js('module::reviews.js')
                       ->title(lang('firesale:title') . ' ' . lang('firesale:reviews:title'))
                       ->build('admin/index', $this->data);

    }

    public function edit($review)
    {

        // Variabls
        $skip  = array();
        $extra = array();

        // Check for post data
        if( substr($this->input->post('btnAction'), 0, 4) == 'save' ) {
            // Variables
            $skip	= array('btnAction');
            $extra 	= array(
                        'return' 			=> '/admin/firesale_reviews' . ( $this->input->post('btnAction') == 'save' ? '/edit/-id-' : '' ),
                        'success_message'	=> lang('firesale:reviews:update_success'),
                        'error_message'		=> lang('firesale:reviews:update_error')
                      );
        }
		
		$this->pyrocache->delete_all('reviews_m');
        $this->pyrocache->delete_all('codeigniter');

        // Get the current row
        $row = $this->row_m->get_row($review, $this->stream, FALSE);

        // Get the stream fields
        $fields = $this->fields->build_form($this->stream, 'edit', $row, FALSE, FALSE, $skip, $extra);

        // Assign variables
        $this->data->review = $row;
        $this->data->id		= $review;
        $this->data->fields = $fields;

        // Build page
        $this->template->append_css('module::reviews.css')
                       ->append_js('module::reviews.js')
                       ->title(lang('firesale:title') . ' ' . lang('firesale:reviews:title'))
                       ->build('admin/edit', $this->data);
    }

    public function delete($review = 0)
    {

        $reviews = $this->input->post('action_to');
        $status  = TRUE;

        if( is_array($reviews) AND count($reviews) > 0 ) {

            foreach( $reviews AS $review ) {
                if( !$this->reviews_m->delete_review($review) ) {
                    $status = FALSE;
                }
            }

            if( $status == TRUE ) {
                $this->session->set_flashdata('success', lang('firesale:reviews:delete_success'));
            } else {
                $this->session->set_flashdata('error', lang('firesale:reviews:delete_error'));
            }

        } else if( $review > 0 ) {
            if( $this->reviews_m->delete_review($review) ) {
                $this->session->set_flashdata('success', lang('firesale:reviews:delete_success'));
            } else {
                $this->session->set_flashdata('error', lang('firesale:reviews:delete_error'));
            }
        }
        $this->pyrocache->delete_all('reviews_m');
        $this->pyrocache->delete_all('codeigniter');
        redirect('/admin/firesale_reviews');
    }

    public function status($review, $status = 0)
    {
        $this->pyrocache->delete_all('reviews_m');

        header('Content-type: application/json');

        if( $review > 0 AND ( $status == 0 OR $status == 1 ) ) {
            if( $this->reviews_m->update_status($review, $status) ) {
                echo json_encode(array('status' => 'success', 'message' => '<div class="alert success">' . lang('firesale:reviews:status_success') . '</div>'));
                exit();
            }
        }

        echo json_encode(array('status' => 'error', 'message' => '<div class="alert error">' . lang('firesale:reviews:status_error') . '</div>'));
        exit();
    }

}

  <div class="clear"></div>
<div id="modifier_ajax" class="hide">si</div>

<?php //print_r($product); exit(); ?>
      <!--Controles Productos-->
      <div class="controls_top marco">       

       {{ if product.prev.0.slug }} <a href="{{ url:site }}product/{{ product.prev.0.slug }}" class="atras"><i class="fa fa-angle-left"></i> Producto anterior</a>{{ endif }}

          <h2>{{ product.title }}</h2>

        {{ if product.next.0.slug }}<a href="{{url:site}}product/{{ product.next.0.slug }}" class="siguiente">Producto siguiente <i class="fa fa-angle-right"></i></a>{{ endif }}

      </div>

      <!--Fin Controles Productos-->



      <div class="clear"></div>

 <!--Detalle Producto-->

      <div class="bg_detalle_product">

        <div class="marco elem_negros bounceInUp animated">
          <!--Fin Detalle Galeria-->            

           <div class="large-5 medium-5 small-12 columns galerias galeria_detalle item2" id="change_images">

              <!--Galeria-->
              <section class="galeria_detalle_produc">
                {{ if images }}
                <div class="cont_zoom imagen_grande_produc">
                  <div class="cont_img_zomm">
                    <img class="zoomimg-container" src="{{ url:site }}files/thumb/{{ first_image:id }}/1000/1000"  data-large="{{ url:site }}files/thumb/{{ first_image:id }}/1000/1000" title="">
                    {{ if product:video:html }}<div class="videoimg-container">{{ product:video:html }}</div>{{ endif}}
                  </div>
                  <ul class="thumb_zoom gal-prod imagenes_peq_produc">
                    {{ images }}
                    <li><img class="zoomimg" src="{{ url:site }}files/thumb/{{ id }}/1000/1000"  data-large="{{ url:site }}files/thumb/{{ id }}/1000/1000" title=""></li>
                    {{ /images }}
                    {{ if product:video:html }}<li><img class="videoimg" src="{{ product:video:thumbnail_url }}" title=""></li>{{ endif}}
                  </ul>
                  {{ else }}
                    {{ theme:image file="no-image.png" alt=title }}
                  {{ endif }} 
                </div>
              </section>
              <!--Fin Galeria-->

           </div>

           <!--Fin Detalle Galeria-->
           <!--Detalle-->

           <div class="large-7 medium-7 small-12 columns datos_detalle">
            <div class="cont_text_prod">
               <h1>{{ product.title }}</h1>
               
        			 {{ if product.descuento > 0 }}
        				<p>Descuento del: <span class="rrp"><b>{{ product.descuento }}%</b></span></p>
        				{{ endif }}
              {{ if product.descuento > 0 }}
                <h1> <span>${{ product.price_ant }}</span></h1>
              {{ endif }}
               <h1>{{ product.price_formatted }}</h1> 

               {{ if product.brand.title }}
                <h3>Marca: <a href="{{url:base}}brand/{{ product.brand.slug }}">{{ product.brand.title }}</a></h3>
                {{ endif }}
                {{ if product.code }}
                 <h4>Ref. {{ product.code }}</h4>
               {{ endif }} 
            </div>

               <div class="row btns_red_agreg">
                <div class="descrip_cal">
                  <div class="clear"></div>
                    <h2 style="padding-top: 4px;">DESCRIPCIÓN</h2> 
                    {{ firesale_reviews:form product=product.id }}
                    
                     <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <section class="descrip cont_tab_det m_t_20" id="descripcion_pro">
                    <div class="des_pro">{{ product.description }}</div>
               </section>
                <div class="clear"></div>

                {{ firesale:modifier_form type="radio" product=product.id }}

                   

                            

               </div>

				

               <!--Tab Comentario -->

               <!-- <section class="tab_detalle">

                 <!-- <a href="#descripcion_pro" class="active_detalle">DESCRIPCIÓN</a> --
        				<?php if (module_enabled('firesale_reviews')): ?>
        					<!-- <a href="#comment_pro">CALIFICACIÓN</a> --
                  
        					<div class="btn_blanco comentar"><i class="fa fa-pencil"></i> CALIFICAR</div>

        				<?php endif; ?>
                   <div class="clear"></div>

               </section>    -->

               
				
				
      				{{ if product.attributes }}
                <h2 style="padding-top: 4px;">ESPECIFICACIONES</h2> 

      				{{ endif }}
                {{ if product.attributes }}
                    <!-- Product Attributes -->
                    <section>
                        <div class="inner-box">
                            <table class="table table-striped tcart">
                                <tbody>
                                    {{ product.attributes }}
                                    <tr>
                                        <td><strong>{{ title }}</strong></td>
                                        <td>{{ value }}</td>
                                    </tr>
                                    {{ /product.attributes }}
                                </tbody>
                            </table>
                        </div>
                    </section>
                {{ endif }}

                <div class="large-12 medium-12 small-12 columns">          
                  <h2>Compartir en:</h2>
                  <!-- Go to www.addthis.com/dashboard to customize your tools -->
                  <div class="addthis_sharing_toolbox" addthis:url="{{url:base}}product/{{product:slug}}" addthis:title="{{product:title}}"></div>     
                </div> 


               <!-- Fin Tab Comentario -->

                 <div class="clear"></div>

           </div>

           <!--Fin Detalle-->



           

        </div>



        <!--Next y Pev Proyect-->
        {{ if product.next.0.slug }}
        <a href="{{ url:site }}product/{{ product.next.0.slug }}" class="next_proyectW">

          <div class="img_pyW"><img src="{{ url:site }}files/thumb/{{ product.next.0.image }}/200/200/fit" alt="">

            <span>siguiente PRODUCTO</span>

          </div>

        </a>
        {{ endif }}
        {{ if product.prev.0.slug }}
        <a href="{{ url:site }}product/{{ product.prev.0.slug }}" class="prev_proyectW">

          <div class="img_pyW"><img src="{{ url:site }}files/thumb/{{ product.prev.0.image }}/200/200/fit" alt="">

            <span>ANTERIOR PRODUCTO</span>

          </div>

        </a>
        {{ endif }}
        <!--Fin Next y Pev Proyect-->

      <div class="clear"></div>

      </div>

      <!--Fin Detalle Producto-->

       <section class="bg_destacados">

        <div class="marco">

          <div class="subtittle">

              <h1>PRODUCTOS DESTACADOS</h1>

          </div>          

          <ul class="destacados"> 
		  
			{{ firesale:products limit="4" order="featured desc" where="p.featured = 1" }}
				{{ if entries }}
				{{ entries }}
				<li class="large-3 medium-6 columns wow fadeInUp" data-wow-delay="0.5s">
						<div class="marco_prod">

							<div class="img_prod">
                <div class="action_share modal_all">
                    <?php if (module_enabled('firesale_wishlist')): ?> 
                        {{ if user:logged_in }} 
                        <a href="#wishlistModal" data-item-id="{{ id }}" data-toggle="modal" data-target="#wishlistModal" data-placement="top" data-original-title="Agregar a mi lista" data-effect="mfp-move-horizontal" class="wish_modal"><i class="fa fa-heart"></i></a> 
                        {{ endif }}
                    <?php endif; ?>
                    <a class="fb-ico facebook_popup link" data-placement="top" data-original-title="Compartir en Facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer.php?s=100&p[title]={{ title }}&p[url]={{ firesale:url route='product' id=id }}{{ if images.1.id }}&p[images][0]={{ url:site uri='files/large' }}/{{ images.1.id }}{{ endif }}">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <a class="tw-ico twitter_popup link" data-placement="top" data-original-title="Compartir en Twitter" href="http://twitter.com/share?url={{ firesale:url route='product' id=id }}&text={{ title }}%20is%20awesome!{{ if {theme:options:twitter_account} }}&via={{ theme:options:twitter_account }}{{ endif }}">
                      <i class="fa fa-twitter"></i>
                    </a>
                </div>
								<div class="mask modal_all">
									<a href="#modal_preview" data-href="{{url:base}}product/{{slug}}" id="quick_view" data-effect="mfp-3d-unfold">
										<div class="btn_blanco">Vista rápida</div>
									</a>
								</div>
								{{ if images }}
									{{ if images.1.id }}
										<img class="first" src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
									{{ else }}
										<img src="{{ url:site uri='files/thumb' }}/{{ images.0.id }}/360" alt="{{ title }}" />
									{{ endif }}
								{{ else }}
									{{ theme:image file="no-image.png" alt=title }}
								{{ endif }}
								<!-- <img src="assets/images/productos/5.jpg" alt=""> -->
								{{ if descuento > '0' }}
									<span class="etiq_desc">{{ descuento }}%<i>dscto</i></span>
								{{ endif }}  
							</div>
							<div class="cont_head_cat color">
                <div class="right">
                    {{ if stock > "0" }}
                      <b>En stock</b>
                    {{ else }}
                      <b>Sin stock</b>
                    {{ endif }}
                </div>
                  <div class="clear"></div>
              </div>
							<p>{{ title }}</p>
							{{ if descuento > '0' }}
								<div class="precio precio_ant">${{ price_ant }}</div>
								<div class="precio">{{ price_formatted }}</div>
							{{ else }}
                                <div class="precio">{{ price_formatted }}</div>
							{{ endif }}
						   <div class="clear"></div>
							<a href="{{ firesale:url route='product' id=id }}" title="Detalle">
								<div class="btn_border">
									Detalle
								</div>  
							</a>
						</div>
				</li>
				{{ /entries }}
				{{ endif }}
				{{ /firesale:products }}
          </ul> 

        </div>  

      </section>  


        <div class="clear"></div>



<script>
$(document).ready(function()
{
 var base_url = $('#baseurl').html();  // seleccionamos la base url de un div
 $( ".pyro-image" ).each(function( index ) {
  console.log( index + ": " + $( this ).text() );
  src = $(this).attr('src');
  src = src.split("}}");
  if(src.length > 0)
  {
   $(this).attr('src', base_url + src[1]);
  }
 });
});
</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Plugin_Firesale_wishlist extends Plugin
{

	public $version     = '1.0.0';
	public $name        = array( 'en' => 'FireSale Wishlists' );
	public $description = array( 'en' => 'Wishlist creation and management' );

    public function __construct()
    {
    	// Load required items
    	$this->load->model('firesale_wishlist/wishlists_m');
    }

    public function lists()
    {
    	// Variables
        $user_id = $this->attribute('user_id', $this->current_user->id);
        $privacy = $this->attribute('privacy', null);

        // Check for user
        if ( $user_id <= 0 ) {
            return false;
        }

    	// Get the users lists
    	$lists = $this->pyrocache->model('wishlists_m', 'user_lists', array($user_id, $privacy), $this->firesale->cache_time);
       
        return array('total' => count($lists), 'entries' => $lists);
    }

    public function in_list()
    {
        // User check
        if ( $this->current_user->id <= 0 ) {
            return false;
        }

    	// Variables
    	$list = $this->attribute('list');
    	$item = $this->attribute('item');

    	// Get list info
    	return $this->pyrocache->model('wishlists_m', 'in_list', array($list, $item), $this->firesale->cache_time);
    }

}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* This file is part of FireSale, a PHP based eCommerce system built for
* PyroCMS.
*
* Copyright (c) 2013 Moltin Ltd.
* http://github.com/firesale/firesale
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @package firesale/search
* @author FireSale <support@getfiresale.org>
* @copyright 2013 Moltin Ltd.
* @version master
* @link http://github.com/firesale/firesale
*
*/

class Plugin_Firesale_Search extends Plugin
{
	public $version = '1.0.0';
	public $autor = 'Luis Fernando Salazar';
	
	public function __construct()
    {
        /*$this->load->model('categories_m');
        $this->load->helper('general');*/
		$this->load->model('firesale/products_m');
    }
	
	public function filter_data()
    {
        // Variables
        $attributes = $this->attributes();
        $cache_key  = md5(BASE_URL.implode('|', $attributes));

        if ( ! $filter_data = $this->cache->get($cache_key) )
		{
			$filter_data = array();

            // Add to query
            foreach ($attributes AS $key => $val) {
				/*echo $key.'-'.$val.'<br/>';*/
                switch (true) {
                    case stristr($key,'modifier'):
						$filter_data[$val] = $this->db->select('pm.`id`, pm.`title`, pv.title AS title_var')
						->from('firesale_product_modifiers AS pm')
						->join('firesale_product_variations AS pv', 'pm.id = pv.parent', 'left')
						->where('lower(pm.title)', $val)
						->group_by('pv.title')
						->get()
						->result_array();
                    break;

                    case stristr($key,'price'):
						$filter_data[$val] = $this->data->min_max = cache('products_m/price_min_max');
                    break;
                }
            }

            // Add to cache
            $this->cache->save($cache_key, $filter_data, $this->firesale->cache_time);

        }
		
		/*echo '<pre>';
		print_r($filter_data);
		echo '</pre>';*/
		
        if ($filter_data) {
            //return array('filter_data' => $filter_data);
            return $filter_data;
        }

        // Nothing?
        return FALSE;
    }

}

<section class="cont_home m_b_20">  

  
  <section class="marco bg_pasos_comp">
      <!--bg Carrito-->    
      {{ theme:partial name="account_nav" }}
      <!--Fin bg Carrito--> 

      <section class="large-9 medium-9 small-12 columns item2">

        <section class="cont_cat_perfil">

          <div class=" relarive elem_blancos  animated fadeInUp form_edit">

             <div class="tittles">
                 <h1><?php echo lang('firesale:wish:'.$type); ?></h1> 
             </div>

             <?php if (validation_errors()):?>
                <div class="alert-error">
                  <?php echo validation_errors();?>
                </div>
             <?php endif;?>

             <div class="clear"></div>

             <?php echo form_open_multipart('', array('class'=>'user_form form-horizontal input-w '));?>
                
                <?php foreach( $fields AS $input ): 
                  $input['input'] = str_replace('multiple="multiple"', '', $input['input']);
                ?>
                    <div class="large-12 medium-12 small-12 columns"> 
                        <label class="col-sm-2 control-label" for="<?php echo $input['input_slug']; ?>">
                            <?php echo lang(substr($input['input_title'], 5)) ? lang(substr($input['input_title'], 5)) : $input['input_title']; ?> <?php echo $input['required']; ?>
                            <small><?php echo lang(substr($input['instructions'], 5)) ? lang(substr($input['instructions'], 5)) : $input['instructions']; ?></small>
                        </label>
                        <div class="col-sm-10 input_b">
                            <?php echo $input['input']; ?>
                        </div>
                    </div>
                <?php endforeach; ?>


                <div class="btn-block m_t_10">
                 <?php if ( isset($_SERVER['HTTP_REFERER']) ): ?>
                    <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn_gris"><span>Cancelar</span></a>
                  <?php endif; ?>                    
                    <button type="submit" name="btnAction" value="save" class="btn_gris m_l_10"><span>Guardar</span></button>            
                    <button type="submit" name="btnAction" value="save_exit" class="btn_gris"><span>Guardar y Salir</span></button> 
                </div>


            <?php echo form_close(); ?>

             <div class="clear"></div>
          </div>

            <div class="clear"></div>
        </section>

      </section>  

  <!--Fin Campos Perfil-->
  </section>

</section>


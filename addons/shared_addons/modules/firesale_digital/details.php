<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Module_Firesale_digital extends Module
{
	public $version = '1.0.0';
	public $language_file = 'firesale_digital/firesale';
	
	public function __construct()
	{
		parent::__construct();
		
		// Load in the FireSale library
		$this->load->library('firesale/firesale');

		// Load Streams... Awesome!
		$this->load->driver('Streams');

		// Load the language file
		$this->lang->load($this->language_file);

		// Add our field type path
        $core_path = defined('PYROPATH') ? PYROPATH : APPPATH;
        
        if (is_dir(SHARED_ADDONPATH.'modules/firesale_digital/field_types')) {
            $this->type->addon_paths['firesale_digital'] = SHARED_ADDONPATH.'modules/firesale_digital/field_types/';
        } elseif (is_dir($core_path.'modules/firesale_digital/field_types')) {
            $this->type->addon_paths['firesale_digital'] = $core_path.'modules/firesale_digital/field_types/';
        } else {
            $this->type->addon_paths['firesale_digital'] = ADDONPATH.'modules/firesale_digital/field_types/';
        }

        $this->type->gather_types();
	}

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Digital',
				'it' => 'Prodotti Digitali'
			),
			'description' => array(
				'en' => 'Sell digital products and downloads',
				'fr' => 'Vente de produits numériques en téléchargement',
				'it' => 'Vendi prodotti digitali e permetti il download'
			),
			'frontend'		=> TRUE,
			'backend'		=> FALSE,
			'firesale_core'	=> FALSE,
			'author'   		=> 'Chris Harvey'
		);
		
		return $info;
	}
	
	public function install()
	{
		if ($this->firesale->is_installed())
		{
			$this->config->load('firesale_digital/config');
			$this->load->model('firesale/routes_m');

			// Create the FireSale digital folder
			@mkdir($this->config->item('fs_digital_upload_path'), 0777);

			// CHMOD the folder just in case.
			@chmod($this->config->item('fs_digital_upload_path'), 0777);

			// Create the .htaccess file to block access if the path is public
			@file_put_contents($this->config->item('fs_digital_upload_path') . '/.htaccess', 'Deny from all', LOCK_EX);
			
			// Add fields
			$fields = array(
				array(
					'name'          => 'lang:firesale:field:digital',
					'slug'          => 'digital',
					'type'          => 'choice',
					'instructions'  => 'lang:firesale:instructions:digital',
					'extra'         => array(
						'choice_data'   => 'y : Yes
											n : No',
						'choice_type'   => 'radio',
						'default_value' => 'n'
					),
					'namespace'     => 'firesale_products',
					'assign'        => 'firesale_products',
					'required'      => true
				),
				array(
					'name'          => 'lang:firesale:field:download',
					'slug'          => 'download',
					'type'          => 'firesale_digital',
					'instructions'  => 'lang:firesale:instructions:download',
					'namespace'     => 'firesale_products',
					'assign'        => 'firesale_products',
					'required'      => false
				),
				array(
					'name'          => 'lang:firesale:field:downloads',
					'slug'          => 'downloads',
					'type'          => 'integer',
					'extra'         => array(
						'max_length' => 11
					),
					'instructions'  => 'lang:firesale:instructions:downloads',
					'namespace'     => 'firesale_products',
					'assign'        => 'firesale_products',
					'required'      => false
				),
				array(
					'name'          => 'lang:firesale:field:expires_after',
					'slug'          => 'expires_after',
					'type'          => 'integer',
					'extra'         => array(
						'max_length' => 11
					),
					'instructions'  => 'lang:firesale:instructions:expires_after',
					'namespace'     => 'firesale_products',
					'assign'        => 'firesale_products',
					'required'      => false
				),
				array(
					'name'          => 'expires_interval',
					'slug'          => 'expires_interval',
					'type'          => 'choice',
					'extra'         => array(
						'choice_data' => 'hours : lang:firesale:interval:hours
										  days : lang:firesale:interval:days
										  weeks : lang:firesale:interval:weeks
										  months : lang:firesale:interval:months
										  years : lang:firesale:interval:years',
						'choice_type' => 'dropdown'
					),
					'namespace'     => 'firesale_products',
					'assign'        => 'firesale_products',
					'required'      => false
				),
			);
			
			// Add fields to stream
			$this->streams->fields->add_fields($fields);

			$this->db->query("
				CREATE TABLE IF NOT EXISTS `" . $this->db->dbprefix('firesale_digital_downloads') . "` (
				  `order_item_id` int(11) NOT NULL,
				  `downloads` int(11) unsigned NOT NULL default '1'
				) ENGINE=MyISAM DEFAULT CHARSET=latin1;
			");

			// Add routes
			$downloads = array(
							'title' => 'Downloads',
							'slug'  => 'downloads',
							'table' => '',
							'map'   => 'users/downloads',
							'route' => 'users/downloads',
							'translation' => 'firesale_digital/front_digital/index'
						);

			$download_file = array(
								'title' => 'Downloads (File)',
								'slug'  => 'downloads-file',
								'table' => '',
								'map'   => 'users/downloads/file/{{ any }}',
								'route' => 'users/downloads/file/([0-9]+)',
								'translation' => 'firesale_digital/front_digital/download/$1'
							 );

			$this->routes_m->create($downloads);
			$this->routes_m->create($download_file);

			return TRUE;
		}
	}

	public function uninstall()
	{
		$this->config->load('firesale_digital/config');

		$this->load->helper('firesale_digital/digital');

		// CH: Recursively remove the directory
		rmdirr($this->config->item('fs_digital_upload_path'));

		// FireSale will remove the fields automatically if it has been uninstalled
		if ($this->firesale->is_installed())
		{
			// Remove Product additions
			$this->streams->fields->delete_field('digital', 'firesale_products');
			$this->streams->fields->delete_field('download', 'firesale_products');
			$this->streams->fields->delete_field('expires_after', 'firesale_products');
			$this->streams->fields->delete_field('expires_interval', 'firesale_products');
			$this->streams->fields->delete_field('downloads', 'firesale_products');
		}

		// Return
		return TRUE;
	}

	public function upgrade($old_version)
	{
		return TRUE;
	}

	public function help()
	{
		return "Some Help Stuff";
	}
}

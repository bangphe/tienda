<section class="cont_home">
    <section class="marco">
        <!--Titulo-->
        <div class="subtittle titulo_int animated fadeInDown">
            <h1>Registrar nueva cuenta</h1>
        </div>
          <div class="clear"></div>
        <section class="cont_nosotros">
                
            <div class="bg_log_v elem_blancos animated fadeInUp">
              
              <?php if ( ! empty($error_string)):?>
                <div class="alert-error">
                  <?php echo $error_string;?>
                </div>
                <?php endif;?>

                <?php echo form_open('register', array('class'=>'user_form form-horizontal')) ?>       
                  <?php foreach($profile_fields as $field) { if($field['required'] and $field['field_slug'] != 'display_name' ) { ?>
                  <fieldset>
                    <label class="col-sm-3 control-label" for="<?php echo $field['field_slug'] ?>" ><?php echo (lang($field['field_name'])) ? lang($field['field_name']) : $field['field_name'];  ?></label>
                    <div class="col-sm-9" ><?php echo $field['input']?></div>
                  </fieldset>
                  <?php } } ?>
                   <?php if ( ! Settings::get('auto_username')): ?>
                  <fieldset>
                    <label class="col-sm-3 control-label" for="username"><?php echo lang('user:username') ?></label>                    
                      <input type="text" name="username" maxlength="100" value="<?php echo $_user->username ?>"  class="form-control" />                    
                  </fieldset>
                  <?php endif ?>
                  <fieldset>
                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo lang('global:email') ?></label>                    
                      <input type="text" name="email" maxlength="100" value="<?php echo $_user->email ?>"  class="form-control"/>                    
                    <?php echo form_input('d0ntf1llth1s1n', ' ', 'class="default-form" style="display:none"') ?>
                  </fieldset>
                  <fieldset>
                    <label for="password" class="col-sm-3 control-label"><?php echo lang('global:password') ?></label>                    
                      <input type="password" class="form-control" id="password" placeholder="Password" name="password" maxlength="100">                    
                  </fieldset>
                  <div class="confirm checkbox">
                    <input type="checkbox" id="termCondnewsletter" name="termCondnewsletter"/>
                    <label for="termCondnewsletter">Acepto <a href="{{ widgets:instance id="6"}}" data-effect="mfp-move-horizontal" target="_blank">Términos y Condiciones.</a>
                    </label>
                  </div> 
                  <fieldset>
                    <div class="text-center">
                      <button type="submit" class="btn_gris"value="<?php echo lang('user:register_btn'); ?>" name="btnSubmit">Regístrate</button>
                    </div>
                  </fieldset>
                <?php echo form_close() ?> 

            </div>

        </section>

    </section><!--Marco-->  
</section>



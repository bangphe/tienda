<section id="content" class="mtop30">
<section class="marco">
<section class="one">
<section class="one">

	<div class="tittles"><h1>Noticias Avanzadas</h1></div>

	{{ advance_news }}
	
	<div class="not_adv">

		<section class="marco cont_nosotros">
	        <div class="large-6 medium-6 small-12 columns p_r_20 m_b_20 wow fadeInUp" data-wow-delay="0.1s">
				<h2 class="title_sec"><a href="{{ urlDetail }}">{{ title }}</a></h2>
			</div>
		<div class="media-not_adv">	
			{{ if type == "2" }}
				<div class="text-not_adv">
					<p><b>"</b>{{ title_said }}<b>"</b></p>
					<span class="date">{{ autor_said }}</span>
				</div>
			{{ endif }}
			{{ if type == "3" }}			
				<div id="owl-slider" class="owl-carousel owl-theme">
	 				{{images}}
			        <div class="item"><img src="{{ path }}" /></div>
			        {{/images}}
				</div>
			{{ endif }}
			{{ if type == "4" }}
				{{ video_audio }}
			{{ endif }}
			{{ if type == "5" }}
				{{ video_audio }}
			{{ endif }}
			{{ if type == "1" }}
				<a href="{{ urlDetail }}" class="post-featured-img"><img src="{{ image }}" alt=""></a>
			{{ endif }}
		</div>


		<span><b>Autor:</b> {{ autor }}</span><br />
		<span class="date"><b>Fecha:</b> {{ date }}</span>
		<br /><br />
		<p>{{ introduction }}</p>
		<br />
		<p><a href="{{ urlDetail }}" class="btn-primary">Ver más</a></p>
	</div>
	{{ /advance_news }}
	{{ pagination }}

</section>
</section>
</section>
</section>
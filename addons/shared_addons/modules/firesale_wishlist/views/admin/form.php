
    <section class="title">
        <h4><?php echo lang('firesale:wish:'.$type); ?></h4>
    </section>

    <section class="item form_inputs">
        <div class="content">

            <?php echo form_open($this->uri->uri_string(), 'id="wish" class="crud"'); ?>
                
                <div class="tabs">

                    <ul class="tab-menu">
                    <?php foreach( $fields AS $tab => $inputs ): ?>
                    <?php if( ( substr($tab, 0, 1) == '_' && $type == 'edit' ) || substr($tab, 0, 1) != '_' ): ?>
                        <li><a href="#<?php echo str_replace('_', '', $tab); ?>"><span><?php echo lang('firesale:wish:tabs:'.str_replace('_', '', $tab)); ?></span></a></li>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    </ul>

                <?php foreach( $fields AS $tab => $inputs ): ?>
                <?php if ( $tab == '_items' && $type == 'edit' ): ?>
                    <div id="items">
                        <fieldset>

                            <?php $this->load->view('admin/add', array('id' => $id, 'items' => $items)); ?>

                        </fieldset>
                    </div>
                <?php elseif( substr($tab, 0, 1) != '_' ): ?>
                    <div id="<?php echo $tab; ?>">
                    	<div class="inline-form">
                            <fieldset>
                                <ul>
                                <?php foreach( $inputs AS $input ): ?>
                                    <li class="<?php echo alternator('even', ''); ?>">
                                        <label for="<?php echo $input['input_slug']; ?>">
                                            <?php echo lang(substr($input['input_title'], 5)) ? lang(substr($input['input_title'], 5)) : $input['input_title']; ?> <?php echo $input['required']; ?>
                                            <small><?php echo lang(substr($input['instructions'], 5)) ? lang(substr($input['instructions'], 5)) : $input['instructions']; ?></small>
                                        </label>
                                        <div class="input"><?php echo $input['input']; ?></div>
                                        <br class="clear" />
                                    </li>
                                <?php endforeach; ?>
                                </ul>
                            </fieldset>
                        </div>
                    </div>
                <?php endif; ?>
                <?php endforeach; ?>

                </div>

                <div class="buttons">
                    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'btn red') )); ?>
                </div>

            <?php echo form_close(); ?>

        </div>
    </section>

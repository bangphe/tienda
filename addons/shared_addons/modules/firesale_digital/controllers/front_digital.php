<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Front_digital extends Public_Controller
{

	public $user_id;

	public function __construct()
	{

		parent::__construct();

		$this->load->model(array('firesale_digital/digital_m', 'firesale/routes_m'));
		$this->load->language(array('firesale/firesale', 'firesale_digital/firesale'));

		$this->user_id = isset($this->current_user->id) ? $this->current_user->id : NULL;

		if( $this->user_id == NULL )
		{
			// Set data and redirect
			$this->session->set_flashdata('error', lang('firesale:cart:login_required'));
			$this->session->set_userdata('redirect_to', current_url());
			redirect('users/login');
		}

	}

	public function index()
	{

		// Variables
		$data = array();
		$data['downloads'] = $this->digital_m->get_downloads($this->user_id);
		$data['count']     = ( $data['downloads'] == FALSE || empty($data['downloads']) ? 0 : count($data['downloads']) );
		
		// Build page
		$this->template->title(lang('firesale:digital:title'))
					   ->set_breadcrumb(lang('firesale:digital:title'), $this->routes_m->build_url('downloads'))
					   ->build('downloads', $data);
	}

	public function download($order_item_id)
	{

		$product = $this->digital_m->get_product($order_item_id);

		if ($product)
		{
			if ( ! empty($this->current_user))
			{
				if ($this->digital_m->can_download($order_item_id))
				{
					$this->load->config('firesale_digital/config');
					$this->load->helper('download');

					// increment the counter
					$this->digital_m->increment($order_item_id);

					// Read the file's contents
					$data = file_get_contents($product['download']['path']);

					$name = $product['download']['filename'];

					force_download($name, $data);
				}
				else
				{
					redirect($this->routes_m->build_url('product') . $product['slug']);
				}
			}
			else
			{
				// Redirect to the product where the user can purchase it.
				redirect($this->routes_m->build_url('product') . $product['slug']);
			}
		}
		else
		{
			// Not product, show 404.
			show_404();
		}
	}

}
/* End of file */